<!DOCTYPE html>
<html lang="en">

<head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700,800,900&display=swap"
            rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('css/plugin.min.css') }}">
      <link rel="stylesheet" href="{{ asset('css/admin_novo/main.css') }}">
      <title>Login - Admin YetzCards</title>
</head>

<body>

      <section class="s-login">
            <div class="container">
                  <div class="container-form">
                        <div class="container-logo">
                              <img src="/img/logo/logo-login.png" alt="">
                        </div>

				<form method="POST" action="/sistema/efetuar-login">
					<div class="container-img-pc">
						<img src="/img/logo/pc.png" alt="">
					</div>
                              <div class="form-group">
                                    <label for="">Login</label>
                                    <input id="login" name="login" type="text" required>
                              </div>
                              <div class="form-group">
                                    <label for="">Senha</label>
                                    <input id="password" name="password" type="password" required>
                              </div>
                              <div class="form-group">
                                    <button type="submit">entrar</button>
                              </div>
                              @csrf
                        </form>

{{--                        <div class="esqueci-senha">--}}
{{--                              <a href="">--}}
{{--                                    <img src="/img/logo/cadeado.png" alt="">--}}
{{--                                    Esqueci minha senha--}}
{{--                              </a>--}}
{{--                        </div>--}}

                        <div class="infs">
                              <p>® YETZ MARKETING, INCENTIVO E RELACIONAMENTO LTDA.
                                    2017 | Todos os direitos reservados / CNPJ: 28.325.166/0001-05.</p>

                              <p>
                                    Este site é melhor visualizado em: Internet Explorer 9, Firefox 2.0.0.2, Safari
                                    3.1.2, Google Chrome 1.0 ou suas versões mais recentes.
                              </p>
				</div>

				<div class="site-seguro">
					<a href="">
						<img src="/img/logo/site-seguro.png" alt="">
					</a>

					<a href="">
						<img src="/img/logo/logo-site-seguro.png" alt="">
					</a>
				</div>

				<div class="copy">
					<span>Todos os direitos reservados 2019 – Yetz</span>
				</div>
                  </div>
		</div>

		<div class="container-img-fundo">
			<img src="/img/logo/fundo.png" alt="">
		</div>
      </section>




      <script src="{{ asset('js/plugins.js') }}"></script>
      <script src="{{ asset('js/admin_novo/admin.min.js') }}"></script>
</body>

</html>
