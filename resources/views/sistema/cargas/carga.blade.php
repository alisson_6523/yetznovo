@extends('sistema.layout.admin-layout')

@section('titulo', ('Carga de Pontos' . (!isset($temporaria) ? ' ' : ' Temporária')))

@section('conteudo')

    <section class="s-carga-pts">
        <div class="container">
            <div class="info-geral-carga">
                <div class="texto-esq">
                    <h1>SUA CARGA DE PONTOS {{ !isset($temporaria) ? '' : 'TEMPORÁRIA ' }}FOI RECEBIDA</h1>
                    <div class="nome-arquivo input-esq">
                        <div class="info">
                            <span>Nome do Arquivo :</span>
                            <strong>{{ $nome_arquivo }}</strong>
                            <input type="hidden" id="nome-arquivo" value="{{ $nome_arquivo }}">
                        </div>
                        <br/>
                        <div class="info">
                            <input type="text" id="titulo-carga" placeholder="Insira o título da carga" required value="{{ isset($protocolo) ? $protocolo->titulo : '' }}">
                        </div>
                    </div>
                    <div class="info-criado">
                        <div class="esq">
                            <div class="item">
                                <i class="icone icone-cliente"></i>
                                <i class="seta icone-arrow-right"></i>
                                <p><strong>CLIENTE</strong> - {{ $campanha->cliente->nome }}</p>
                            </div>
                            <div class="item">
                                <i class="icone icone-campanha"></i>
                                <i class="seta icone-arrow-right"></i>
                                <p><strong>CAMPANHA</strong> - {{ $campanha->nome_campanha }}</p>
                                <input type="hidden" id="cod-campanha" value="{{ $campanha->cod_campanha }}">
                            </div>
                            <div class="item">
                                <i class="icone icone-pdv"></i>
                                <i class="seta icone-arrow-right"></i>
                                <div class="pdvs">
                                    @foreach($campanha->campanha_pdvs as $pdv)
                                        <span>{{ $pdv->nome_pdv }}</span>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="dir">
                            @if(!isset($protocolo))
                                <div class="item">
                                    <i class="icone icone-user"></i>
                                    <i class="seta icone-arrow-right"></i>
                                    <div class="info">
                                        <p><strong>CRIADOR</strong> - {{ \Auth::user()->nome }}</p>
                                        <ul>
                                            <li>Data - {{ date('d/m/Y', strtotime(\Carbon\Carbon::now())) }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @else
                                <div class="item">
                                    <i class="icone icone-user"></i>
                                    <i class="seta icone-arrow-right"></i>
                                    <div class="info">
                                        <p><strong>CRIADOR</strong> - {{ \App\Models\Admin::find($protocolo->cod_adm_criador)->nome }}</p>
                                        <ul>
                                            <li>Data - {{ date('d/m/Y', strtotime($protocolo->created_at)) }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endif
                            @if(!empty($protocolo->data_efetivacao))
                            <div class="item">
                                <i class="icone icone-user"></i>
                                <i class="seta icone-arrow-right"></i>
                                <div class="info">
                                    <p><strong>EFETIVADOR</strong> - {{ \App\Models\Admin::find($protocolo->cod_adm_efetivador)->nome }}</p>
                                    <ul>
                                        <li>Data - {{ date('d/m/Y', strtotime($protocolo->data_efetivacao)) }}</li>
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                @if($estatistica->erros > 0 || (isset($estatistica->nao_encontrados) && $estatistica->nao_encontrados > 0))
                <div class="box-white erro">
                    <img src="{{ asset('img/icons/icone-atencao.svg') }}" alt="">
                    <h2 class="text-red">SEU AQUIVO POSSUI ERROS</h2>
                    <p>Verifique os dados antes de enviar essa carga.</p>
                    <div class="btns">
                        <a href="{{ URL::previous() }}" class="btn-cancelar">cancelar</a>
                    </div>
                </div>
                @else
                <div class="box-white" >
                    <img src="{{ asset('img/icons/icone-sucesso.svg') }}" alt="">
                    <h2 class="text-green">SEU AQUIVO NÃO POSSUI ERROS</h2>
                    @if(!isset($protocolo))
                        <p>Verifique os dados antes de enviar essa carga.</p>
                    @elseif(!empty($protocolo->data_efetivacao))
                        <p>Esta carga já foi efetivada!</p>
                    @else
                        <p>Esta carga já foi enviada!</p>
                    @endif
                    <div class="btns">
                        @if(isset($protocolo))
                            <a href="{{ URL::previous() }}" class="btn-cancelar">Voltar</a>
                        @else
                            <a href="{{ URL::previous() }}" class="btn-cancelar">cancelar</a>
                            <a style="cursor:pointer;" onclick="carga.salvar({{ isset($temporaria) ? 1 : '' }})" class="btn-enviar">enviar <i class="icone-arrow-right"></i></a>
                        @endif
                    </div>
                </div>
                @endif
            </div>
            <div class="info-geral-pts">
                <ul>
                    @if(!isset($temporaria))<li>Usuários Adicionados: <span>{{ $estatistica->adicionados }}</span></li>@endif
                    <li>Usuários Atualizados: <span>{{ $estatistica->atualizados }}</span></li>
                    <li>Usuários com mudança de cargo: <span>{{ $estatistica->mudanca_cargo }}</span></li>
                    <li>Usuários com Mudança de PDV: <span>{{ $estatistica->mudanca_pdv }}</span> </li>
                    <li>Usuários com mudança de Ilha: <span>{{ $estatistica->mudanca_ilha }}</span></li>
                    <li>Usuários com Mudança de Coordenador: <span>{{ $estatistica->mudanca_coordenador }}</span></li>
                    <li>Usuários com Mudança de Supervisor: <span>{{ $estatistica->mudanca_supervisor }}</span></li>
                </ul>
                <div class="pts">
                    <i class="icone icone-circulo-estrela"></i>
                    <i class="seta icone-arrow-right"></i>
                    <h2>{{ number_format($estatistica->total_pontos, 0, '', '.') }}</h2>
                </div>
                @if($estatistica->erros > 0 || (isset($estatistica->nao_encontrados) && $estatistica->nao_encontrados > 0))
                    <div class="info-status erro">
                        <img src="{{ asset('img/icons/icone-danger.svg') }}" alt="">
                        <i class="seta icone-arrow-right"></i>
                        <p>
                            Registros não aceitos: {{ isset($estatistica->nao_encontrados) ? $estatistica->erros + $estatistica->nao_encontrados : $estatistica->erros }}
                            <br/>Erros: {{ !empty($estatistica->erros) ? 'PDV não encontrado' : '' }} <br/>
                            {{ !empty($estatistica->nao_encontrados) ? 'Usuário não encontrado' : '' }}
                        </p>
                    </div>
                @endif
            </div>

            <table>
                <tr>
                    <th>CHAVE-PDV*</th>
                    <th>{{ isset($temporaria) ? 'ID' : 'CPF/RE' }}*</th>
                    <th>COLABORADOR*</th>
                    <th>CARGO</th>
                    <th>ILHA</th>
                    <th>COORDENADOR</th>
                    <th>SUPERVISOR</th>
                    <th>CARGA</th>
                    <th>REFERÊNCIA</th>
                </tr>

                @foreach($usuarios as $usuario)
                    <tr class="{{ $usuario->erro == 1 ? 'erro-linha' : '' }} {{ isset($usuario->mudanca_pdv) && $usuario->mudanca_pdv == 1 ? 'mudanca-pdv' : '' }}">
                        <td>{{ $usuario->registro }}</td>
                        <td>{{ isset($temporaria) ? $usuario->id : $usuario->login }}</td>
                        <td>{{ empty($usuario->nome) ? '' : $usuario->nome  }}</td>
                        <td>{{ $usuario->cargo }}</td>
                        <td>{{ $usuario->ilha }}</td>
                        <td>{{ $usuario->coordenador }}</td>
                        <td>{{ $usuario->supervisor }}</td>
                        <td>{{ number_format($usuario->carga, 0, '', '.') }}</td>
                        <td>{{ $usuario->referencia }}</td>
                    </tr>
                @endforeach

            </table>
        </div>
    </section>

@stop
