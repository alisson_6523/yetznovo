@extends('sistema.layout.admin-layout')

@section('titulo', 'Usuários')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="area-conteudo-modulo" id="rota-usuarios">
            <div class="topo">
                  <ul class="nav-tabs">
                        <li>
                              <span id="title-toggle" data-user="usu">
                                    <i class="icone-user"></i>
                              </span>
                        </li>
                        <li class="active">
                              <span id="title-toggle" data-user="adm">

                              </span>
                        </li>
                  </ul>


                  <div class="acoes">
{{--                        <div class="pesquisa">--}}
{{--                              <button type="button"><i class="icone-lupa"></i></button>--}}
{{--                        </div>--}}
                        <!-- <a href="/sistema/usuarios/novo-administrador" class="btn">
                              adicionar administrador <i class="icone-arrow-right"></i>
                        </a> -->
                  </div>
            </div>

            <div class="todos-usuarios active">
                  @foreach ($admins as $a)
                  <a href="/sistema/usuarios/editar-administrador/{{ $a->id }}/{{ limpaString($a->nome) }}" class="box-usuario" status="{{ $a->status }}">
                        <h3>{{ $a->nome }}</h3>
                        <ul>
                              <li>
                                    <span>{{ $a->perfil->nome }}</span>
                              </li>
                              <li>
                                    <span>{{ $a->criacao }}</span>
                              </li>
                              <li>
                                    <span>{{ $a->status }}</span>
                              </li>
                        </ul>
                  </a>
                  @endforeach
            </div>


            <div class="todos-usuarios-sites">
                  <div class="filtros">
                        <div class="form-group">
                              <label for="">PDV</label>
                              <div class="select-custom">
                                    <input type="hidden" id="filtro-usuarios-pdv">
                                    <a href="" class="item-selected">
                                          <span>Todos</span>
                                    </a>
                                    <div class="dropdown">
                                          <ul>
                                                <li><a href="" cod-opcao="">Todos</a></li>
                                                @foreach($pdvs as $pdv)
                                                <li><a href="" cod-opcao="{{ $pdv->cod_pdv }}">{{ $pdv->nome_pdv }}</a>
                                                </li>
                                                @endforeach
                                          </ul>
                                    </div>
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="">Ilha</label>
                              <div class="select-custom">
                                    <input type="hidden" id="filtro-usuarios-ilha">
                                    <a href="" class="item-selected">
                                          <span>Todos</span>
                                    </a>
                                    <div class="dropdown">
                                          <ul>
                                                <li><a href="" cod-opcao="">Todas</a></li>
                                                @foreach($ilhas as $ilha)
                                                <li><a href=""
                                                            cod-opcao="{{ $ilha->ilha }}">{{ str_replace("_", " ", $ilha->ilha) }}</a>
                                                </li>
                                                @endforeach
                                          </ul>
                                    </div>
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="">Status</label>
                              <div class="select-custom">
                                    <input type="hidden" id="filtro-usuarios-status">
                                    <a href="" class="item-selected">
                                          <span>Todos</span>
                                    </a>
                                    <div class="dropdown">
                                          <ul>
                                                <li><a href="" cod-opcao="">Todos</a></li>
                                                <li><a href="" cod-opcao="1">Ativo</a></li>
                                                <li><a href="" cod-opcao="0">Bloqueado</a></li>
                                          </ul>
                                    </div>
                              </div>
                        </div>
                        <div class="form-group">
                              <label for="">Cadastro</label>
                              <div class="select-custom">
                                    <input type="hidden" id="filtro-usuarios-cadastro">
                                    <a href="" class="item-selected">
                                          <span>Todos</span>
                                    </a>
                                    <div class="dropdown">
                                          <ul>
                                                <li><a href="" cod-opcao="">Todos</a></li>
                                                <li><a href="" cod-opcao="1">Concluído</a></li>
                                                <li><a href="" cod-opcao="0">Não Concluído</a></li>
                                          </ul>
                                    </div>
                              </div>
                        </div>
                        <div class="input-search">
                              <input type="text" id="filtro-usuarios-busca">
                              <button type="button">
                                    <img src="{{ asset('img/icons-adm/lupa.svg') }}" alt="">
                              </button>
                        </div>
                  </div>
                  <div>
                        <div>
                              <div class="loader" style="display: none;">
                                    <i class="fa fa-spinner fa-spin"></i>
                              </div>
                        </div>
                        <div id="todos-usuarios-filtrado">


                        </div>
                  </div>
            </div>
      </section>

</main>

@stop
