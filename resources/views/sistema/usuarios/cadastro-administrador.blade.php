@extends('sistema.layout.admin-layout')

@section('titulo', 'Cadastrar Administrador')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-cadastro-administrador">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/usuarios">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>CADASTRAR ADMINISTRADOR</h2>
                </div>
                <div class="form-group">
                    <label for="">Nome *</label>
                    <input type="text" name="nome" id="cadastro-administrador-nome">
                </div>
                <div class="form-group">
                    <label for="">E-mail *</label>
                    <input type="email" name="email" id="cadastro-administrador-email">
                </div>
                <div class="form-group">
                    <label for="">Login *</label>
                    <input type="text" name="login" id="cadastro-administrador-login">
                </div>
                <div class="form-group">
                    <label for="">Senha *</label>
                    <input type="password" name="senha" id="cadastro-administrador-senha">
                </div>
                <div class="form-group">
                    <label for="">Confirmar Senha *</label>
                    <input type="password" name="confirmar_senha" id="cadastro-administrador-confirmar-senha">
                </div>
                <div class="form-group">
                    <label for="">Cliente</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="cadastro-administrador-cliente">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul id='clientes'>
                                <li><a href="" cod-opcao="">Todos</a></li>
                                @foreach($clientes as $cliente)
                                    <li><a href="" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Perfil *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_perfil" id="cadastro-administrador-perfil">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul id='perfis'>
                                @foreach($perfis as $perfil)
                                    <li><a href="" cod-opcao="{{ $perfil->id }}">{{ $perfil->nome }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="cadastro-administrador-div-campanhas" style="display: none">
                    <label for="">Campanhas *</label>
                    <div id="cadastro-administrador-todas-campanhas" class="todos-pdvs">

                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar este administrador.
                    </p>
                    <button type="submit">finalizar administrador <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DE ADMINISTRADOR', 'link' => '/sistema/usuarios/novo-administrador'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE ADMINISTRADOR REALIZADO <br> COM SUCESSO!', 'link' => '/sistema/usuarios'])
@stop

