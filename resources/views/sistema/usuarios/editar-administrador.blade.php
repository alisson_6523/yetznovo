@extends('sistema.layout.admin-layout')

@section('titulo', 'Editar Administrador')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-editar-administrador">
            <input type="hidden" id="editar-administrador-id" value="{{ $admin->id }}">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/usuarios">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>EDITAR ADMINISTRADOR</h2>
                </div>
                <div class="form-group">
                    <label for="">Nome *</label>
                    <input type="text" name="nome" id="editar-administrador-nome" value="{{ $admin->nome }}">
                </div>
                <div class="form-group">
                    <label for="">E-mail *</label>
                    <input type="email" name="email" id="editar-administrador-email" value="{{ $admin->email }}">
                </div>
                <div class="form-group">
                    <label for="">Login *</label>
                    <input type="text" name="login" id="editar-administrador-login" value="{{ $admin->login }}">
                </div>
                <div class="form-group">
                    <label for="">Senha</label>
                    <input type="password" name="senha" id="editar-administrador-senha">
                </div>
                <div class="form-group">
                    <label for="">Confirmar Senha</label>
                    <input type="password" name="confirmar_senha" id="editar-administrador-confirmar-senha">
                </div>
                <div class="form-group">
                    <label for="">Cliente</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="editar-administrador-cliente" value="{{ !empty($admin->cliente_id) ? $admin->cliente_id : '' }}">
                        <a href="" class="item-selected">
                            <span>{{ !empty($admin->cliente_id) ? $admin->cliente->nome : 'Todos' }}</span>
                        </a>
                        <div class="dropdown">
                            <ul id='clientes'>
                                <li><a href="" cod-opcao="">Todos</a></li>
                                @foreach($clientes as $cliente)
                                    <li><a href="" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Perfil *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_perfil" id="editar-administrador-perfil" value="{{ $admin->perfil_id }}">
                        <a href="" class="item-selected">
                            <span>{{ $admin->perfil->nome }}</span>
                        </a>
                        <div class="dropdown">
                            <ul id='perfis'>
                                @foreach($perfis as $perfil)
                                    <li><a href="" cod-opcao="{{ $perfil->id }}">{{ $perfil->nome }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para atualizar este administrador.
                    </p>
                    <button type="submit">atualizar <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR O ADMINISTRADOR', 'link' => '/sistema/usuario/editar-administrador/'. $admin->id . '/' . limpaString($admin->nome)])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'ADMINISTRADOR ATUALIZADO COM SUCESSO!', 'link' => '/sistema/usuarios'])
@stop

