@extends('sistema.layout.admin-layout')

@section('titulo', 'Usuário')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="s-detalhe-usuario">
            <div class="sidebar-usuario">

                  <div class="voltar-campanha">
                        <img src="{{ asset('img/icons-adm/detalhe-usuario-01.png') }}" alt="">
                        <a href="/sistema/usuarios">VOLTAR / Usuários</a>
                  </div>

                  <div class="container-usuario">
                        <div class="img-usuario">
                              <img src="{{ $usuario->foto_usuario }}" alt="">
                        </div>

                        <div class="text-nome-usuario">
                              <p>{{ $usuario->nome_usuario }}</p>
                              <span class="open-modal">alterar senha</span>
                        </div>

                        <div class="toggle-usuario">
{{--                           <img src="{{ asset('img/icons-adm/detalhe-usuario-02.png') }}" alt="">--}}
                        </div>
                  </div>

                  @if($usuario->campanhas->count() == 1)
                      <div class="saldo-atual">
                            <p>saldo atual de pontos</p>

                            <div class="container-img-pontos">
                                  <img src="{{ asset('img/icons-adm/detalhe-usuario-03.png') }}" alt="">
                                  <img src="{{ asset('img/icons-adm/detalhe-usuario-04.png') }}" alt="">
                            </div>

                            <h3>{{ $usuario->pontos()->first()->pontos }} Yetz</h3>
                      </div>
                  @endif

                  <div class="dados-usuarios">
                        <h2>dados do usuários</h2>
                        <input type="hidden" id="detalhe-usuario-id" value="{{ $usuario->id }}">
                        <ul>
                              <li><span>RE: {{ $usuario->login }}</span></li>
                              <li><span>Data de Nascimento: {{ date('d.m.Y', strtotime($usuario->data_nascimento)) }}</span></li>
                              <li><span>Gênero: {{ $usuario->sexo == 'm' ? 'Masculino' : 'Feminino' }}</span></li>
                              <li><span>E-mail: {{ $usuario->email }}</span></li>
                              <li><span>Telefone: {{ $usuario->telefone }}</span></li>
                              <li><span>Celular: {{ $usuario->celular }}</span></li>
                              <li><span>PDV: {{ $usuario->pdv->nome_pdv}}</span></li>
                              <li>
                                    <span>Campanhas:</span>
                                    <ul>
                                          @foreach($usuario->campanhas as $campanha_usuario)
                                          <li>
                                                <a href="/sistema/campanhas/{{ $campanha_usuario->cod_campanha }}/{{ limpaString($campanha_usuario->nome_campanha) }}/usuario/{{ $usuario->id }}/{{ limpaString($usuario->nome_usuario) }}"><p>{{ $campanha_usuario->nome_campanha }}</p></a>
                                          </li>
                                          @endforeach
                                    </ul>
                              </li>
                        </ul>
                  </div>
                  @if($usuario->notificacao_celular)
                  <div class="msg-whats">
                        <p>Aceito receber mensagens via SMS/WhatsApp</p>
                  </div>
                  @endif
{{--                  <div class="acessar-como">--}}
{{--                        <img src="/img/icons-adm/detalhe-usuario-05.png" alt="">--}}
{{--                        <p>acessar como usuário</p>--}}
{{--                  </div>--}}
            </div>



            <div class="table-usuario">
                  <div class="tab-pane-usuario">
                        <div class="filtros">
                              <div class="form-group">
                                    <label for="">Tipo</label>
                                    <div class="select-custom">
                                          <input type="hidden" name="" id="filtro-campanhas-detalhes-usuario-ilha">
                                          <a href="" class="item-selected">
                                                <span>Todas</span>
                                          </a>
                                          <div class="dropdown">
                                                <ul>
                                                      <li data-cod_opcao="0" ><a href="" cod-opcao="">Todos</a></li>
                                                      @foreach(\App\Models\TipoMovimentacao::all() as $tipo)
                                                            <li data-cod_opcao="{{ $tipo->cod_tipo }}"><a href="" cod-opcao="{{ $tipo->cod_tipo }}">{{ $tipo->descricao_tipo }}</a></li>
                                                      @endforeach
                                                </ul>
                                          </div>
                                    </div>
                              </div>

{{--                              <div class="form-group">--}}
{{--                                    <label for="">Status</label>--}}
{{--                                    <div class="select-custom">--}}
{{--                                          <input type="hidden" name="" id="filtro-campanhas-detalhes-usuario-status">--}}
{{--                                          <a href="" class="item-selected">--}}
{{--                                                <span>Todos</span>--}}
{{--                                          </a>--}}
{{--                                          <div class="dropdown">--}}
{{--                                                <ul>--}}
{{--                                                      <li><a href="" cod-opcao="">Todos</a></li>--}}
{{--                                                      <li><a href="" cod-opcao="1">Aguardando</a></li>--}}
{{--                                                      <li><a href="" cod-opcao="2">Aprovado</a></li>--}}
{{--                                                      <li><a href="" cod-opcao="3">À Caminho</a></li>--}}
{{--                                                      <li><a href="" cod-opcao="4">Entregue</a></li>--}}
{{--                                                      <li><a href="" cod-opcao="5">Cancelado</a></li>--}}
{{--                                                </ul>--}}
{{--                                          </div>--}}
{{--                                    </div>--}}
{{--                              </div>--}}

                              <div class="input-search">
                                    <input type="text" id="filtro-campanhas-detalhes-usuario-busca">
                                    <button type="button">
                                          <img src="{{ asset('img/icons-adm/lupa.svg') }}" alt="">
                                    </button>
                              </div>
                        </div>
                  </div>

                  <div class="container-line-user">
                        @php
                             if(isset($campanha))
                                $movimentacoes = $usuario->movimentacoes()->where('cod_campanha', $campanha->cod_campanha)->orderBy('cod_movimentacao', 'DESC')->get();
                             else
                                $movimentacoes = $usuario->movimentacoes()->orderBy('cod_movimentacao', 'DESC')->get();
                        @endphp

                        @foreach($movimentacoes as $movimentacao)
                            <ul data-cod_movimentacao="{{ $movimentacao->tipo_movimentacao->cod_tipo }}">
                                @if($movimentacao->tipo_movimentacao->cod_tipo == 1)
                                    <li>
                                        <span>{{ $movimentacao->protocolo }}</span>
                                        <div class="container-item">
                                            <div class="title">
                                                <i class="icone-user"></i>
                                                <span>inf. da entrega</span>
                                            </div>
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                CEP - {{ $usuario->pdv->cep }} / {{ $usuario->pdv->estado }}
                                            </p>
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                {{ $usuario->pdv->cidade }} - {{ $usuario->pdv->estado }}
                                            </p>
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                {{ $usuario->pdv->bairro }}
                                            </p>
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                {{ $usuario->pdv->logradouro }},
                                                {{ $usuario->pdv->numero }}{{ ' ('. $usuario->pdv->complemento . ')' }}
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span>Data - {{ date('d/m/Y', strtotime($movimentacao->created_at)) }} . {{ date('H:i:s', strtotime($movimentacao->created_at)) }}</span>
                                        <div class="container-item">
                                            <div class="title">
                                                <i class="icone-resgate"></i>
                                                <span>INF. D0 RESGATE</span>
                                            </div>
                                            @foreach(($movimentacao->premio->carrinho->produtos ?? []) as $produto)
                                                <p>
                                                    <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                    {{ $produto->nome_produto }} - R${{ $produto->valor_reais }} ({{ $produto->produto_tipo->nome_tipo }})
                                                </p>
                                            @endforeach
                                        </div>
                                    </li>

                                    <li>
                                        <span>{{ $movimentacao->tipo_movimentacao->descricao_tipo }}</span>
                                        <div class="container-item">
                                            <div class="title">
                                                <i class="icone-cartao"></i>
                                                <span>STATUS DA ENTREGA</span>
                                            </div>
                                            <p>
                                                {{$movimentacao->premio->status_carrinho->descricao_status}}
                                            </p>
                                        </div>
                                    </li>


                                    <li>
                                        <div class="pts-detalhe-usuario">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-07.png') }}" alt="">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                            <p>{{ $movimentacao->valor }} pts</p>
                                        </div>
                                    </li>
                                @elseif($movimentacao->tipo_movimentacao->cod_tipo == 2)
                                    <li>
                                        <span>{{ $movimentacao->protocolo }}</span>
                                        <div class="container-item">
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                {{ $movimentacao->descricao }}
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span>Data - {{ date('d/m/Y', strtotime($movimentacao->created_at)) }} . {{ date('H:i:s', strtotime($movimentacao->created_at)) }}</span>
                                        <div class="container-item">
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                Referência: {{ $movimentacao->referencia }}
                                            </p>
                                        </div>
                                    </li>




                                    <li>
                                        <span>{{ $movimentacao->tipo_movimentacao->descricao_tipo }}</span>
                                        <div class="container-item">
                                            @if(!empty(\App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()))
                                                <p title="{{ strlen(\App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()->arquivo) > 50 ? \App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()->arquivo : '' }}">
                                                    <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
								    Nome do Arquivo: {{ str_replace("_", " ", strlen(\App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()->arquivo) > 50 ? substr(\App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()->arquivo, 0, 50) . '...' : \App\Models\CampanhaProtocolo::where('protocolo', $movimentacao->protocolo)->first()->arquivo)  }}
                                                </p>
                                            @endif
                                        </div>
                                    </li>


                                    <li>
                                        <div class="pts-detalhe-usuario">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-07.png') }}" alt="">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                            <p>{{ $movimentacao->valor }} pts</p>
                                        </div>

                                        <div class="container-item">

                                        </div>
                                    </li>
                                @elseif($movimentacao->tipo_movimentacao->cod_tipo == 3)
                                    <li>
                                        <span>{{ $movimentacao->protocolo }}</span>
                                        <div class="container-item">
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                {{ $movimentacao->descricao }}
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span>Data - {{ date('d/m/Y', strtotime($movimentacao->created_at)) }} . {{ date('H:i:s', strtotime($movimentacao->created_at)) }}</span>
                                        <div class="container-item">
                                            <p>
                                                <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                                @php
                                                    $item    = App\Models\ItemCarrinho::find($movimentacao->referencia);
                                                    $produto = App\Models\Produto::find($item['cod_produto']);
                                                @endphp
                                                {{ $produto['nome_produto'] }} - R${{ $produto['valor_reais'] }}
                                            </p>
                                        </div>
                                    </li>

                                    <li>
                                        <span>{{ $movimentacao->tipo_movimentacao->descricao_tipo }}</span>
                                        <div class="container-item">

                                        </div>
                                    </li>


                                    <li>
                                        <div class="pts-detalhe-usuario">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-07.png') }}" alt="">
                                            <img src="{{ asset('img/icons-adm/detalhe-usuario-08.png') }}" alt="">
                                            <p>{{ $movimentacao->valor }} pts</p>
                                        </div>

                                        <div class="container-item">

                                        </div>
                                    </li>
                                @elseif($movimentacao->tipo_movimentacao->cod_tipo == 4)

                                @endif

                            </ul>
                        @endforeach

                  </div>
            </div>
      </section>


      <div class="modal-alterar-senha">
            <div class="container-modal-form">
                  <form id="alterar-senha-detalhe-usuario{{ Request::is('sistema/campanhas/*') ? '-campanha' : '' }}">
                        <div class="container-form">
                              <input type="text" name="detalhe_usuario_nova_senha" id="detalhe-usuario-nova-senha" placeholder="Nova Senha">
                        </div>
                        <div class="btn-alterar-senha">
                              <button class="btn-alterar-senha" type="submit">alterar</button>
                        </div>
                  </form>
            </div>
      </div>
</main>

@stop
