<!DOCTYPE html>
<html lang="pt-Br">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700,800,900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/plugin.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin_novo/main.css') }}">
        <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" type="image/png" />
        @yield('css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('titulo') | Admin YetzCards</title>
    </head>

    <body>
        @yield('conteudo')

        <script src="{{ asset('js/plugins.js') }}"></script>
        <script src="{{ asset('js/admin_novo/admin.min.js') }}"></script>
        @yield('scripts')
        @yield('onload')
    </body>
</html>