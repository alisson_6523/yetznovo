@extends('sistema.layout.admin-layout')

@section('titulo', 'Relatórios')

@section('conteudo')
    <label for="">Data inicial</label>
    <input type="date" name="data-inicial" id="data-inicial">
    <br>
    <label for="">Data final</label>
    <input type="date" name="data-final" id="data-final">
    <br>
    <br>
    <label for="">Clientes</label>
    <select name="" id="clientes">
        <option value="0">Selecione</option>

        @foreach($clientes as $cliente)
            @if(!empty(\Auth::user()->cliente_id))
                <option value="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</option>
            @else
                <option value="{{ $cliente->id }}">{{ $cliente->nome }}</option>
            @endif
        @endforeach
    </select>
    <br>
    <label for="">PDVs</label>
    <select name="" id="pdvs">
        <option value="0">Selecione</option>

        @foreach($pdvs as $pdv)
            <option value="{{ $pdv->cod_pdv }}">{{ $pdv->nome_pdv }}</option>
        @endforeach
    </select>
    <br>
    <label for="">Campanhas</label>
    <select name="" id="campanhas">
        <option value="0">Selecione</option>

        @foreach($campanhas as $campanha)
            <option value="{{ $campanha->cod_campanha }}">{{ $campanha->nome_campanha }}</option>
        @endforeach
    </select>
    <br>
    <br>
    <p id="usuarios"></p>
    <p id="novos-usuarios"></p>
    <p id="usuarios-recorrentes"></p>
    <p id="numero-sessoes"></p>
    <p id="sessoes-usuarios"></p>
    <p id="visualizacoes-paginas"></p>
    <p id="paginas-sessao"></p>
    <p id="duracao-sessao"></p>
    <br>
    <h3>Páginas</h3>
    <ul id="ranking-paginas"></ul>
    <br>
    <h3>Categorias</h3>
    <ul id="ranking-categorias"></ul>
    <br>
    <h3>Produtos</h3>
    <ul id="ranking-produtos"></ul>
    <br>
    <h3>Produtos Resgatados</h3>
    <ul id="ranking-produtos-resgatados"></ul>
    <br>
    <h4>Homens</h4>
    <ul id="ranking-produtos-resgatados-homens"></ul>
    <br>
    <h4>Mulheres</h4>
    <ul id="ranking-produtos-resgatados-mulheres"></ul>
    <br>
    <h3>Usuários</h3>
    <p id="usuarios-cadastrados"></p>
    <p id="usuarios-cadastro-completo"></p>
    <p id="usuarios-cadastro-incompleto"></p>
    <p id="usuarios-notificacao-permitida"></p>
    <p id="usuarios-notificacao-bloqueada"></p>
    <br>
    <h3>Usuários Ativos</h3>
    <br>
    <h4>Mês</h4>
    <p id="usuarios-ativos-mes"></p>
    <br>
    <h4>Semana</h4>
    <p id="usuarios-ativos-semana"></p>
    <br>
    <h4>Dia</h4>
    <p id="usuarios-ativos-dia"></p>
    <br>
    <h3>Horários com mais acesso</h3>
    <ul id="acessos-por-horario"></ul>
@stop