@extends('sistema.layout.admin-layout')

@section('titulo', 'Editar Cliente')

@section('conteudo')
    <div class="area-cadastro">
        <form id="editar-cliente">
            <input type="hidden" id="cliente-id" value="{{ $cliente->id }}">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/clientes/{{ $cliente->id }}/{{ limpaString($cliente->nome_fantasia != null ? $cliente->nome_fantasia : $cliente->razao_social) }}">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>editar cliente</h2>
                </div>
                <div class="form-group">
                    <label for="cliente-razao">Razão Social *</label>
                    <input type="text" id="cliente-razao" name="cliente_razao" value="{{ $cliente->razao_social }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-fantasia">Nome Fantasia</label>
                    <input type="text" id="cliente-fantasia" name="cliente_fantasia" value="{{ $cliente->nome_fantasia }}">
                </div>
                <div class="form-group">
                    <label for="cliente-estadual">Inscrição Estadual</label>
                    <input type="text" id="cliente-estadual" name="cliente_estadual" value="{{ $cliente->inscricao_estadual }}">
                </div>
                <div class="form-group">
                    <label for="cliente-municipal">Inscrição Municipal</label>
                    <input type="text" id="cliente-municipal" name="cliente_municipal" value="{{ $cliente->inscricao_municipal }}">
                </div>
                <div class="form-group">
                    <label for="cliente-cnpj">CNPJ</label>
                    <input type="text" mask="cnpj" id="cliente-cnpj" name="cliente_cnpj" value="{{ $cliente->cnpj }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-cep">CEP *</label>
                    <input type="text" mask="cep" id="cliente-cep" name="cliente_cep" value="{{ $cliente->cep }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-logradouro">Logradouro*</label>
                    <input type="text" id="cliente-logradouro" name="cliente_loogradouro" value="{{ $cliente->logradouro }}" required>
                </div>
                <div class="double-form">
                    <div class="form-group">
                        <label for="cliente-complemento">Complemento</label>
                        <input type="text" id="cliente-complemento" name="cliente_complemento" value="{{ $cliente->complemento }}">
                    </div>
                    <div class="form-group">
                        <label for="cliente-numero">Número *</label>
                        <input type="text" id="cliente-numero" name="cliente_numero" value="{{ $cliente->numero }}" required oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                </div>
                <div class="form-group">
                    <label for="cliente-bairro">Bairro *</label>
                    <input type="text" id="cliente-bairro" name="cliente_bairro" value="{{ $cliente->bairro }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-cidade">Cidade *</label>
                    <input type="text" id="cliente-cidade" name="cliente_cidade" value="{{ $cliente->cidade }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-estado">Estado *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cliente_estado" id="cliente-estado" value="{{ $cliente->estado }}">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="AC">Acre</a></li>
                                <li><a href="" cod-opcao="AL">Alagoas</a></li>
                                <li><a href="" cod-opcao="AP">Amapá</a></li>
                                <li><a href="" cod-opcao="AM">Amazonas</a></li>
                                <li><a href="" cod-opcao="BA">Bahia</a></li>
                                <li><a href="" cod-opcao="CE">Ceará</a></li>
                                <li><a href="" cod-opcao="DF">Distrito Federal</a></li>
                                <li><a href="" cod-opcao="ES">Espírito Santo</a></li>
                                <li><a href="" cod-opcao="GO">Goiás</a></li>
                                <li><a href="" cod-opcao="MA">Maranhão</a></li>
                                <li><a href="" cod-opcao="MT">Mato Grosso</a></li>
                                <li><a href="" cod-opcao="MS">Mato Grosso do Sul</a></li>
                                <li><a href="" cod-opcao="MG">Minas Gerais</a></li>
                                <li><a href="" cod-opcao="PA">Pará</a></li>
                                <li><a href="" cod-opcao="PB">Paraíba</a></li>
                                <li><a href="" cod-opcao="PR">Paraná</a></li>
                                <li><a href="" cod-opcao="PE">Pernambuco</a></li>
                                <li><a href="" cod-opcao="PI">Piauí</a></li>
                                <li><a href="" cod-opcao="RJ">Rio de Janeiro</a></li>
                                <li><a href="" cod-opcao="RN">Rio Grande do Norte</a></li>
                                <li><a href="" cod-opcao="RS">Rio Grande do Sul</a></li>
                                <li><a href="" cod-opcao="RO">Rondônia</a></li>
                                <li><a href="" cod-opcao="RR">Roraima</a></li>
                                <li><a href="" cod-opcao="SC">Santa Catarina</a></li>
                                <li><a href="" cod-opcao="SP">São Paulo</a></li>
                                <li><a href="" cod-opcao="SE">Sergipe</a></li>
                                <li><a href="" cod-opcao="TO">Tocantins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="cliente-telefone">Tel. *</label>
                    <input type="text" mask="telefone" id="cliente-telefone" name="cliente_telefone" value="{{ $cliente->telefone }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-email">E-mail *</label>
                    <input type="email" id="cliente-email" name="cliente_email" value="{{ $cliente->email }}" required>
                </div>
                <div class="form-group">
                    <label for="cliente-site">Site</label>
                    <input type="text" id="cliente-site" name="cliente_site" value="{{ $cliente->site }}">
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Enviar logo</label>
                    <div class="box">
                        <input type="file" name="file-7[]" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"/>
                        <label for="file-7"><span>{{ $cliente->logo }}</span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para atualizar seu cliente.
                    </p>
                    <button type="submit">atualizar <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>


    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR O CLIENTE', 'link' => '/sistema/clientes/adicionar'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CLIENTE ATUALIZADO COM SUCESSO!', 'link' => '/sistema/clientes'])

@stop