@extends('sistema.layout.admin-layout')

@section('titulo', 'Clientes')

@section('conteudo')

    <main>
        @include('sistema.includes.menu-admin')

        <section class="area-conteudo-modulo">
            <div class="topo">
                <h2 class="title">
                    <i class="icone-arrow-right"></i>
                    CLIENTES
                </h2>
                <div class="acoes">
                    <div class="pesquisa">
                        <input id="cliente-busca" type="text"/>
                        <button type="button"><i class="icone-lupa"></i></button>
                    </div>
                    <a href="/sistema/clientes/adicionar" class="btn">
                        adicionar cliente <i class="icone-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="area-todos-clientes">

                @foreach($clientes as $cliente)
                    <a href="/sistema/clientes/{{ $cliente->id }}/{{ limpaString($cliente->nome_fantasia != null ? $cliente->nome_fantasia : $cliente->razao_social) }}" class="box-cliente">
                        <div class="info">
                            <h3>{{ $cliente->nome }}</h3>
                            <ul>
                                <li>
                                    <div class="square"></div>
                                    <span>CNPJ-{{ $cliente->cnpj }}</span>
                                </li>
                                <li>
                                    <div class="square"></div>
                                    <span>{{ date('m/Y', strtotime($cliente->created_at)) }}</span>
                                </li>
                                <li>
                                    <div class="square {{ $cliente->deleted_at == null ? 'green' : 'yellow' }}"></div>
                                    <span>{{ $cliente->deleted_at == null ? 'ATIVO' : 'INATIVO' }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="geral">
                            <div class="item">
                                <i class="icone icone-campanha"></i>
                                <i class="icone-arrow-down"></i>
                                <span>{{ $cliente->campanhas->count() }}</span>
                            </div>
                            <div class="item">
                                <i class="icone icone-pdv"></i>
                                <i class="icone-arrow-down"></i>
                                <span>{{ $cliente->cliente_pdvs->count() }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach

            </div>
        </section>
    </main>
@stop