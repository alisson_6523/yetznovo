@extends('sistema.layout.admin-layout')

@section('titulo', $cliente->razao_social)

@section('conteudo')

    <main>
        @include('sistema.includes.menu-admin')

        <section class="area-geral-campanha">
            <aside>
                <div class="cont">
                    <a href="/sistema/clientes" class="btn-voltar">
                        <img src="{{ asset('img/icons/circulo-laranja.svg') }}" alt="">
                        <span>voltar</span>
                    </a>
                    <div class="title">
                        <h1>{{ $cliente->nome_fantasia }}</h1>
                        <button type="button">
                            <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                        </button>
                        <div class="dropdown">
                            <ul>
                                <li>
                                    <a href="/sistema/clientes/editar/{{ $cliente->id }}/{{ limpaString($cliente->nome_fantasia) }}">
                                        <i class="icone-lapis"></i>
                                        <span>editar</span>
                                    </a>
                                </li>
                                @if(empty($cliente->trashed()))
                                <li>
                                    <a onclick="clientes.excluir({{ $cliente->id }})" class="btn-excluir" style="cursor:pointer;">
                                        <img src="{{ asset('img/icons/excluir.svg') }}" alt="">
                                        <span>desativar</span>
                                    </a>
                                </li>
                                @else
                                    <li>
                                        <a onclick="clientes.ativar({{ $cliente->id }})" class="btn-excluir" style="cursor:pointer;">
                                            <i class="icone-lapis"></i>
                                            <span>ativar</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>
                        </div>

                    </div>
                    @if($cliente->trashed())
                        <div class="list">
                            <h3 style="color: red;">cliente inativo</h3>
                        </div>
                    @endif
                    <div class="numbers">
                        <div class="item">
                            <span>N. CAMP.</span>
                            <div class="dados">
                                <i class="icone icone-campanha"></i>
                                <i class="seta icone-arrow-right"></i>
                                <h3>{{ $cliente->campanhas->count() }}</h3>
                            </div>
                        </div>
                        <div class="item">
                            <span>N. PDVS</span>
                            <div class="dados">
                                <i class="icone icone-pdv"></i>
                                <i class="seta icone-arrow-right"></i>
                                <h3>{{ $cliente->cliente_pdvs->count() }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="list">
                        <h3>dados do cliente</h3>
                        <ul>
                            <li>
                                <span>Razão Social: {{ $cliente->razao_social }}</span>
                            </li>
                            <li>
                                <span>CNPJ: {{ $cliente->cnpj }}</span>
                            </li>
                            <li>
                                <span>Telefone: {{ $cliente->telefone }}</span>
                            </li>
                            <li>
                                <span>E-mail: {{ $cliente->email }}</span>
                            </li>
                        </ul>
                    </div>
                    <div class="list">
                        <h3>endereço</h3>
                        <ul>
                            <li>
                                <span>{{ $cliente->logradouro }}</span>
                            </li>
                            <li>
                                <span>Bairro: {{ $cliente->bairro }}</span>
                            </li>
                            <li>
                                <span>Estado: {{ $cliente->estado }}</span>
                            </li>
                            @if(!empty($cliente->complemento))
                                <li>
                                    <span>Complemento: {{ $cliente->complemento }}</span>
                                </li>
                            @endif
                            <li>
                                <span>Número: {{ $cliente->numero }}</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </aside>
            <div class="cont-geral">
                <div class="topo-abas">
                    <a href="" class="btn-recolher-sidebar">
                        <i class="icone-arrow-left"></i>
                    </a>
                    <ul class="nav-tabs">
                        <li class="active">
                            <a href="" id="tab-pdv">
                                <i class="icone-pdv"></i>
                            </a>
                        </li>
                        <li>
                            <a href="" id="tab-campanha">
                                <i class="icone-campanha"></i>
                            </a>
                        </li>
                    </ul>
                    <a href="/sistema/clientes/{{$cliente->id}}/{{ limpaString($cliente->nome_fantasia != null ? $cliente->nome_fantasia : $cliente->razao_social) }}/pdv/adicionar" class="btn-add">
                        <img src="{{ asset('img/icons/icone-plus.svg') }}">
                    </a>
                </div>
                <div class="tab-pane" id="pdvs">
                    <div class="todos-pdvs">
                        @if(count($cliente->cliente_pdvs()->withTrashed()->get()) > 0)
                            @foreach($cliente->cliente_pdvs()->withTrashed()->get() as $pdv)
                                <div class="area-box">
                                    <div class="dropdown">
                                        <ul>
                                            @if(!$pdv->trashed())
                                                <li>
                                                    <a href="/sistema/clientes/{{ $cliente->id }}/{{ limpaString($cliente->nome_fantasia != null ? $cliente->nome_fantasia : $cliente->razao_social) }}/pdv/{{ $pdv->cod_pdv }}/{{ limpaString($pdv->nome_pdv) }}/editar">
                                                        <i class="icone-lapis"></i>
                                                        <span>editar</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a onclick="cad_pdvs.excluir({{ $pdv->cod_pdv }})" class="btn-excluir" style="cursor: pointer;">
                                                        <img src="{{ asset('img/icons/excluir.svg') }}" alt="">
                                                        <span>desativar</span>
                                                    </a>
                                                </li>
                                            @elseif($pdv->trashed() && !$cliente->trashed())
                                                <li>
                                                    <a onclick="cad_pdvs.ativar({{ $pdv->cod_pdv }})" class="btn-excluir" style="cursor: pointer;">
                                                        <i class="icone-lapis"></i>
                                                        <span>ativar</span>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>

                                    <div class="box-pdv">
                                        <div class="title">
                                            <div class="text">
                                                <button type="button" class="btn-acoes">
                                                    <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                                                </button>
                                                <h3>{{ $pdv->nome_pdv }} {{ $pdv->trashed() ? '(Inativo)' : '' }}</h3>
                                            </div>
                                            <i class="icone-arrow-down"></i>
                                        </div>
                                        <ul class="list-itens">
                                            <li><span>Registro: {{ $pdv->registro }}</span></li>
                                            <li><span>Cidade: {{ $pdv->cidade }}</span></li>
                                            <li><span>Estado: {{ $pdv->estado }}</span></li>
                                            <li><span>CEP: {{ $pdv->cep }}</span></li>
                                            <li><span>Logradouro: {{ $pdv->logradouro }}</span></li>
                                            <li><span>Bairro: {{ $pdv->bairro }}</span></li>
                                            @if(!empty($pdv->complemento))
                                                <li><span>Complemento: {{ $pdv->complemento }}</span></li>
                                            @endif
                                            <li><span>Número: {{ $pdv->numero }}</span></li>
                                            <li><span>Campanhas</span></li>
                                        </ul>
                                        @if(count($pdv->campanhas) > 0)
                                            <div class="campanhas">
                                                @foreach($pdv->campanhas as $campanha_pdv)
                                                    <span>{{ $campanha_pdv->nome_campanha }}</span>
                                                @endforeach
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
                <div class="tab-pane" id="campanhas">
                    <div class="todos-pdvs">

                        @foreach($cliente->campanhas()->withTrashed()->get() as $campanha)
                            <div class="area-box">
                                <div class="dropdown">
                                    <ul>
                                        <li>
                                            <a href="/sistema/campanhas/editar/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">
                                                <i class="icone-lapis"></i>
                                                <span>editar</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="box-pdv box-campanha">
                                    <div class="title">
                                        <div class="text">
                                            <button type="button" class="btn-acoes">
                                                <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                                            </button>
                                            <h3>{{ $campanha->nome_campanha }}</h3>
                                        </div>
                                        <i class="icone-arrow-down"></i>
                                    </div>
                                    <ul class="list-itens">
                                        <li>
                                            <span>PDVs </span>
                                            <div class="itens-sub">
                                                @foreach($campanha->campanha_pdvs as $pdv)
                                                    <span>{{ $pdv->nome_pdv }}</span>
                                                @endforeach
                                            </div>
                                        </li>
                                        <li><span>Inicio: {{ date('d/m/Y', strtotime($campanha->dt_inicio)) }}</span></li>
                                        <li><span>Fim: {{ date('d/m/Y', strtotime($campanha->dt_fim)) }}</span></li>
                                        <li class="{{ $campanha->trashed() ? '' : 'ativo' }}"><span>{{ $campanha->trashed() ? 'Inativa' : 'Ativa' }}</span></li>
                                    </ul>
                                    <div class="info-campanha">
                                        <div class="item-info">
                                            <i class="icone icone-user"></i>
                                            <i class="seta icone-arrow-right"></i>
                                            <h3>{{ $campanha->usuarios_campanha()->withTrashed()->count() }}</h3>
                                        </div>
                                        <div class="item-info">
                                            <i class="icone icone-circulo-estrela"></i>
                                            <i class="seta icone-arrow-right"></i>
                                            <h3>{{ number_format($campanha->campanha_protocolos->sum('total_pontos'), 0, '', '.') }}</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </section>

    </main>

@stop
