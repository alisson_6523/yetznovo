<section class="adicionar-arquivos">
      <div class="area-cadastro">
        <form id="form-cadastro-arquivo">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/uploads">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>envio de novo arquivo</h2>
                </div>
                <div class="form-group">
                  <label for="">Título do Arquivo</label>
                  <input type="text" id="cadastro-arquivo-titulo" name="cadastro_arquivo_titulo">
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Arquivo</label>
                    <div class="box">
                        <input type="file" name="anexo[]" id="anexo" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="anexo"><span></span> <strong>Selecionar Arquivo</strong></label>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para enviar o arquivo.
                    </p>
                    <button type="submit">enviar arquivo <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Todos os campos são obrigatórios.</span>
            </div>
        </form>
    </div>
</section>