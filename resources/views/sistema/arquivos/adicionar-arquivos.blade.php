@extends('sistema.layout.admin-layout')

@section('titulo', 'Adicionar Campanha')

@section('conteudo')
      @include('sistema.arquivos.adicionar')

      @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO ENVIO DO ARQUIVO', 'link' => '/sistema/uploads/adicionar'])

      @include('sistema.includes.modal-sucesso', ['mensagem' => 'ENVIO DE ARQUIVO REALIZADO<br/>COM SUCESSO!', 'link' => '/sistema/uploads'])
@stop