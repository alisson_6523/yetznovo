@extends('sistema.layout.admin-layout')

@section('titulo', 'Usuários')

@section('conteudo')

      <main>
            @include('sistema.includes.menu-admin')

            <section class="area-conteudo-modulo">
                  <div class="topo">
                        <h2 class="title">
                              <i class="icone-arrow-right"></i>envio de arquivos
                        </h2>

                        <div class="filtro-arquivo">
                              <input type="text">
                              <img src="/img/icons-adm/arquivo-18.png" alt="">
                        </div>

                        <div class="btn-add-arquivo">
                              <a href="/sistema/uploads/adicionar"><img src="/img/icons-adm/arquivo-19.png" alt=""></a>
                        </div>
                  </div>

                  @include('sistema.arquivos.list-arquivos')
            </section>
      </main>

      @section('scripts')
            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
      @stop

@stop