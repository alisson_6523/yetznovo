<div class="list-arquivos">

      @foreach($arquivos as $arquivo)
            <div class="container-arquivos">
                  <div class="text-name-arquivo">
                        <h3>{{ $arquivo->titulo }}</h3>
                  </div>

                  <div class="text-user">
                        <h3>{{ $arquivo->historico()->where('operacao', 'Enviou')->get()->first()->admin->nome }}</h3>
                  </div>

                  <div class="date-time">
                        <h3>{{ date('d.m.Y - H:i:s', strtotime($arquivo->historico()->where('operacao', 'Enviou')->get()->first()->created_at)) }}</h3>
                  </div>

                  <div class="download">
                        <img src="/img/icons-adm/arquivos-05.png" alt="">
                        <a href="/sistema/uploads/baixar-arquivo/{{ $arquivo->hash_arquivo }}/{{ limpaString($arquivo->titulo) }}"><h2>download</h2></a>
                  </div>

                  <div class="excluir">
                        <img src="/img/icons-adm/arquivo-06.png" alt="" style="cursor:pointer;" onclick="arquivos.excluir({{ $arquivo->id }})">
                  </div>
            </div>
      @endforeach

</div>
