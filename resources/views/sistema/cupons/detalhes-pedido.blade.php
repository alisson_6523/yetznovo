@extends('sistema.layout.admin-layout')

@section('titulo', 'Cupons')

@section('conteudo')

<main>
    @include('sistema.includes.menu-admin')

    <section class="s-area-portal s-area-cupons area-conteudo-modulo" id="cupons-detalhe">
        <div class="topo">
            <h2 class="title">
                <i class="icone-arrow-right"></i>
                {{ $pedido->nome }} - {{ !empty($pedido->cliente_id) ? $pedido->cliente->nome_fantasia : 'Todos os Clientes' }} - {{ !empty($pedido->campanha_id) ? $pedido->campanha->nome_campanha : 'Todas as Campanhas' }} - {{ $pedido->quantidade }} cartões - {{ $pedido->usados }} usados @if($pedido->trashed()) &nbsp;<span style="color: red">(BLOQUEADO)</span> @endif
            </h2>
            <div class="acoes">
                <div class="form-group" id="form-group-cod-status-cupom" style="display: none">
                    <div class="select-custom">
                        <input type="hidden" id="cod-status-cupom" name="cod_status_cupom">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" class="option-cod-cupom" cod-opcao="1">Ativar</a></li>
                                <li><a href="" class="option-cod-cupom" cod-opcao="0">Bloquear</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="cupons">
            <div class="tabela-cupons">
                <ul class="head">
                    <li>
                        <div class="check-square" id="check-all-cupons"></div>
                    </li>
                    <li>
                        <span>Sequêncial</span>
                    </li>
                    <li>
                        <span>Código</span>
                    </li>
                    <li>
                        <span>Valor</span>
                    </li>
                    <li>
                        <span>Usado por</span>
                    </li>
                    <li>
                        <span>PDV</span>
                    </li>
                    <li>
                        <span>Usado em</span>
                    </li>
                </ul>
                <div class="body" id='linhas-resgates'>
                    <div>
                        <div class="loader" style="display: none;">
                            <i class="fa fa-spinner fa-spin"></i>
                        </div>
                    </div>
                    @foreach ($pedido->cupons()->withTrashed()->get() as $c)
                        <div class="item-table">
                                <ul class="dados">
                                    <li>
                                        <div class="check-square" id="checkbox-cupom-unitario" data-cupom_id="{{ $c->id }}"></div>
                                    </li>
                                    <li>
                                        <span>{{ $c->sequencial }} @if($c->trashed()) <span style="color: red">(Bloqueado)</span> @endif</span>
                                    </li>
                                    <li>
                                        <span>{{ $c->codigo }} </span>
                                    </li>
                                    <li>
                                        <span>{{ $c->pontos }}</span>
                                    </li>
                                    <li>
                                        <span>{{ $c->user->nome_usuario ?? '-' }}</span>
                                    </li>
                                    <li>
                                        <span>{{ $c->user->pdv->nome_pdv ?? '-' }}</span>
                                    </li>
                                    <li>
                                        <span>{{ $c->usado ?? '-' }}</span>
                                    </li>
                                </ul>
                        </div>
                    @endforeach
                </div>
            </div>



            {{-- <div class="todos-cartoes">
                @foreach ($pedido->cupons as $c)
                <a href="" class="item">
                    <div class="n-lote">
                        <button type="button" class="btn-detalhes-lote">
                            <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                        </button>
                        <strong>id. cupom - {{ $c->id }}</strong>
                    </div>
                    <div class="n-lote">
                        <strong>{{ $c->codigo }}</strong>
                    </div>
                    <strong>Valor: {{ $c->valor }}</strong>
                    <strong>Usado por: {{ $c->user->nome_usuario ?? '-' }}
                        ({{ $c->user->pdv->nome_pdv ?? '-' }})</strong>
                    <span>{{ $c->usado ?? '-' }}</span>
                </a>
                @endforeach
            </div> --}}
        </div>
    </section>

</main>

@stop
