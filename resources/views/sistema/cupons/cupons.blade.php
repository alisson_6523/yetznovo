@extends('sistema.layout.admin-layout')

@section('titulo', 'Cupons')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="s-area-portal" id=rota-cupons>
            <div class="topo">
                  <ul class="nav-tabs">
                        <li class="active">
                              <span href="" id="tab-cupons">
                                    <i class="icone-cartao"></i>
                              </span>
                        </li>
                        <li>
                              <a href="" id="tab-lote">
                                    <i class="icone-lote"></i>
                              </a>
                        </li>
                  </ul>
                  <a href="/sistema/cupons/cadastra-lote" class="btn">
                        <img src="{{ asset('img/plus.svg') }}" alt="">
                  </a>
            </div>
            <div class="tab-pane" id="cupons">
                  <div class="todos-cartoes">
                      <div>
                          <div class="loader" style="display: none;">
                              <i class="fa fa-spinner fa-spin"></i>
                          </div>
                      </div>
                        @foreach ($cupons as $cupom)
                        <div class="container-itens">
                              <a href="/sistema/cupons/{{ $cupom->id }}" class="item itens-cupons">
                                    <div class="n-lote">
                                          <button type="button" class="btn-detalhes-lote">
                                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                                          </button>
                                          <strong>{{ $cupom->nome }} @if($cupom->trashed()) <span style="color: red">(Bloqueado)</span> @endif</strong>
                                    </div>
                                    <strong>{{ !empty($cupom->cliente_id) ? $cupom->cliente->nome_fantasia : 'Todos os Clientes' }}/{{ !empty($cupom->campanha_id) ? $cupom->campanha->nome_campanha : 'Todas as Campanhas' }}</strong>
                                    <strong>Utilizados - {{ $cupom->usados }}/{{ $cupom->cupons->count() }}</strong>
                                    <span>Criador - {{ $cupom->admin->nome }}</span>
                                    <span>Data de Criação - {{ $cupom->solicitacao }}</span>
                              </a>

                              <div class="dropdown">
                                    <ul>
                                          @if($cupom->trashed())
                                          <li>
                                                <a style="cursor: pointer" onclick="cupons.ativarLote({{ $cupom->id }})">
                                                      <i class="icone-lapis"></i>
                                                      <span>ativar</span>
                                                </a>
                                          </li>
                                          @else
                                          <li>
                                                <a style="cursor: pointer" onclick="cupons.bloquearLote({{ $cupom->id }})" class="btn-excluir">
                                                      <img src="{{ asset('img/icons-adm/excluir.svg') }}" alt="">
                                                      <span>bloquear</span>
                                                </a>
                                          </li>
                                        @endif
                                         <li>
                                             <a style="cursor: pointer" onclick="cupons.exportar({{ $cupom->id }})">
                                                 <i class="icone-lapis"></i>
                                                 <span>exportar</span>
                                             </a>
                                         </li>
                                    </ul>
                              </div>
                        </div>
                        @endforeach
                  </div>
            </div>
      </section>

</main>

@stop
