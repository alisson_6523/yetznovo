@extends('sistema.layout.admin-layout')

@section('titulo', 'Adicionar Lote de Cupons')

@section('conteudo')

<div class="area-cadastro">
    <form id="form-cadastro-lote-cupom">
        <div class="s-esq">
            <div class="title">
                <a href="/sistema/cupons">
                    <div class="circle">
                        <i class="icone-arrow-left"></i>
                    </div>
                </a>
                <h2>CADASTRAR Lote de Cupons</h2>
            </div>
            <div class="form-group">
                <label for="">Nome do Pedido *</label>
                <input type="text" name="nome" id="nome" required>
            </div>
            <div class="form-group">
                <label for="">Cliente *</label>
                <div class="select-custom">
                    <input type="hidden" id="cadastro-cupom-cliente" name="cliente_id">
                    <a href="" class="item-selected">
                        <span>Selecione</span>
                    </a>
                    <div class="dropdown">
                        <ul>
                            <li><a href="" id="cadastro-cupom-option-cliente" cod-opcao="">Todos</a></li>
                            @if(!empty(\Auth::user()->cliente_id))
                                <li><a href="" id="cadastro-cupom-option-cliente" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                            @else
                                @foreach(\App\Models\Cliente::all() as $cliente)
                                    <li><a href="" id="cadastro-cupom-option-cliente" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Campanha</label>
                <div class="select-custom">
                    <input type="hidden" name="cod_campanha" id="cadastro-cupom-campanha">
                    <a href="" class="item-selected">
                        <span>Selecione</span>
                    </a>
                    <div class="dropdown">
                        <ul id="cadastro-cupom-select-campanha">

                        </ul>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">Prefixo *</label>
                <input type="text" name="prefixo" id="prefixo" required>
            </div>

            <div class="form-group">
                <label for="">Quantidade *</label>
                <input type="text" name="quantidade" id="quantidade" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" required>
            </div>

            <div class="form-group">
                <label for="">Pontos *</label>
                <input type="text" name="pontos" id="pontos" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" required>
            </div>

        </div>
        <div class="s-dir">
            <div class="box-white">
                <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                <p>
                    Preencha os campos ao lado e clique no botão abaixo para cadastrar os cupons.
                </p>
                <button type="submit" id="btn-cadastro-lote-cupom">finalizar lote <i class="icone-arrow-right"></i></button>
            </div>
            <span class="alert">Campos com * são obrigatórios.</span>
        </div>
    </form>
</div>

@include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DE CUPOM', 'link' => '/sistema/cupons/cadastra-lote'])

@include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE CUPOM REALIZADO COM SUCESSO!', 'link' => '/sistema/cupons'])

@stop
