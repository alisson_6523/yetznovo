@extends('sistema.layout.admin-layout')

@section('titulo', 'Plataforma')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="s-area-portal">
            <div class="topo">
                  <ul class="nav-tabs">
                        <li class="active">
                              <span id="tab-banner">
                                    <i class="icone-banner"></i>
                              </span>

                              <div class="container-filtro">
                                    <a href="/sistema/plataforma/adicionar-banner" class="btn">
                                          adicionar banner<i class="icone-arrow-right"></i>
                                    </a>

                                    <div class="form-group">
                                          <input type="text" id="filtro-busca-banner">
                                          <button type="button">
                                                <i class="icone-lupa"></i>
                                          </button>
                                    </div>
                              </div>

                        </li>

                        <li>
                              <span id="tab-categoria">
                                    <i class="icone-categoria"></i>
                              </span>

                              <!-- <div class="container-filtro">
                                    <a href="" class="btn">
                                          adicionar categoria<i class="icone-arrow-right"></i>
                                    </a>

                                    <div class="form-group">
                                          <input type="text">
                                          <button type="button">
                                                <i class="icone-lupa"></i>
                                          </button>
                                    </div>
                              </div> -->
                        </li>

                        <li>
                              <span id="tab-cartoes">
                                    <i class="icone-cartao"></i>
                              </span>

                              <!-- <div class="container-filtro">
                                    <a href="" class="btn">
                                          adicionar cartões<i class="icone-arrow-right"></i>
                                    </a>

                                    <div class="form-group">
                                          <input type="text">
                                          <button type="button">
                                                <i class="icone-lupa"></i>
                                          </button>
                                    </div>
                              </div> -->
                        </li>
                  </ul>
            </div>
            <div class="tab-pane" id="banners">
                  <div class="todos-banners">
                        @foreach ($banners as $b)
                        <div class="box-banner">
                              <button type="button" class="">
                                    <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                                    <div class="dropdown">
                                        <ul>
                                            <li>
                                                <a href="/sistema/plataforma/editar-banner/{{ $b->cod_banner }}/{{ !empty($b->titulo) ? limpaString($b->titulo) : 'sem-titulo' }}">
                                                    <i class="icone-lapis"></i>
                                                    <span>editar</span>
                                                </a>
                                            </li>
                                            @if(!$b->trashed())
                                                <li>
                                                    <a onclick="banner.inativar({{ $b->cod_banner }})" class="btn-excluir"
                                                       style="cursor:pointer;">
                                                        <img src="{{ asset('img/icons-adm/excluir.svg') }}" alt="">
                                                        <span>desativar</span>
                                                    </a>
                                                </li>
                                            @else
                                                <li>
                                                    <a onclick="banner.ativar({{ $b->cod_banner }})" class="btn-excluir"
                                                       style="cursor:pointer;">
                                                        <i class="icone-lapis"></i>
                                                        <span>ativar</span>
                                                    </a>
                                                </li>
                                            @endif
                                        </ul>
                                    </div>
                              </button>



                              <div class="info">
                                    <div class="thumb">
                                          <img src="{{ $b->link }}" alt="">
                                    </div>
                                    <ul>
                                          @if(!empty($b->titulo))<li><span>Título: {{ $b->titulo }}</span></li>@endif
                                          @if(!empty($b->cod_campanha))<li><span>Campanha: {{ $b->campanha->nome_campanha }}</span></li>@endif
                                          <li><span>D.E- {{ $b->entrada }}</span></li>
                                          <li><span>D.S- {{ $b->saida }}</span></li>
                                          <li><span>{{ $b->arquivo }}</span></li>
                                          <li status="{{ $b->trashed() ? : 'ativo' }}"><span>{{ $b->trashed() ? 'Inativo' : 'Ativo' }}</span></li>
                                    </ul>
                              </div>
                        </div>
                        @endforeach
                  </div>
            </div>

            <div class="tab-pane" id="categorias" style="display:none">
                  <div class="todas-categorias">
                        @foreach ($categorias as $c)
                        <div class="box-categoria">
                              <button type="button">
                                    <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
{{--                                    <div class="dropdown">--}}
{{--                                          <ul>--}}
{{--                                                <li>--}}
{{--                                                      <a href="">--}}
{{--                                                            <i class="icone-lapis"></i>--}}
{{--                                                            <span>editar</span>--}}
{{--                                                      </a>--}}
{{--                                                </li>--}}

{{--                                                <li>--}}
{{--                                                      <a onclick="" class="btn-excluir"--}}
{{--                                                            style="cursor:pointer;">--}}
{{--                                                            <img src="{{ asset('img/icons-adm/excluir.svg') }}" alt="">--}}
{{--                                                            <span>desativar</span>--}}
{{--                                                      </a>--}}
{{--                                                </li>--}}

{{--                                                <li>--}}
{{--                                                      <a onclick="" class="btn-excluir"--}}
{{--                                                            style="cursor:pointer;">--}}
{{--                                                            <i class="icone-lapis"></i>--}}
{{--                                                            <span>ativar</span>--}}
{{--                                                      </a>--}}
{{--                                                </li>--}}
{{--                                          </ul>--}}
{{--                                    </div>--}}
                              </button>
                              <div class="info">
                                    <h3>{{ $c->categoria }}</h3>
                                    <ul>
                                          <li>{{ $c->produtos->count() }} Produtos</li>
                                          <li status="ativo">Ativo</li>
                                    </ul>
                              </div>
                              <i class="icone icone-{{ $c->icone_categoria }}"></i>
                        </div>
                        @endforeach

                  </div>
            </div>

            <div class="tab-pane" id="cartoes" style="display:none;">
                  <div class="dados-cartoes">
                        @foreach ($produtos as $p)
                        <div class="item" status="ativo">
                              <ul class="item-head">
                                    <li>
                                          <button type="button" class="btn-acoes">
                                                <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
{{--                                                <div class="dropdown">--}}
{{--                                                      <ul>--}}
{{--                                                            <li>--}}
{{--                                                                  <a href="">--}}
{{--                                                                        <i class="icone-lapis"></i>--}}
{{--                                                                        <span>editar</span>--}}
{{--                                                                  </a>--}}
{{--                                                            </li>--}}

{{--                                                            <li>--}}
{{--                                                                  <a onclick="" class="btn-excluir"--}}
{{--                                                                        style="cursor:pointer;">--}}
{{--                                                                        <img src="{{ asset('img/icons-adm/excluir.svg') }}"--}}
{{--                                                                              alt="">--}}
{{--                                                                        <span>desativar</span>--}}
{{--                                                                  </a>--}}
{{--                                                            </li>--}}

{{--                                                            <li>--}}
{{--                                                                  <a onclick="" class="btn-excluir"--}}
{{--                                                                        style="cursor:pointer;">--}}
{{--                                                                        <i class="icone-lapis"></i>--}}
{{--                                                                        <span>ativar</span>--}}
{{--                                                                  </a>--}}
{{--                                                            </li>--}}
{{--                                                      </ul>--}}
{{--                                                </div>--}}
                                          </button>
                                          <span> {{ $p->nome_produto }} - R$ {{ $p->valor_reais }}</span>
                                    </li>
                                    <li>
                                          <span>{{ $p->produto_tipo->nome_tipo }}</span>
                                    </li>
                                    <li>
                                          <ul class="list-categorias">

                                                @foreach($p->categorias as $c)
                                                <li>
                                                      <i class="icone-{{ $c->icone_categoria }}"></i>
                                                </li>
                                                @endforeach

                                          </ul>
                                    </li>
                                    <li>
                                          <span>Valores em Yetz - {{ $p->valor_produto }}</span>
                                    </li>
                                    <li>
                                          <span>Ativo</span>
                                    </li>
                                    <i class="seta icone-arrow-down"></i>
                              </ul>
                              <ul class="item-body">
                                    <li>
                                          <div class="cartao">
                                                <div class="thumb">
                                                      <img src="{{ $p->link_foto }}" alt="">
                                                </div>
                                                <span>Validade do Produto: {{ $p->validade_produto }}</span>
                                          </div>
                                    </li>
                                    <li>
                                          <div class="tipo-cartao">
                                                @foreach($p->selos_adm as $s)
                                                <div class="selo {{ $s['class'] }}">
                                                      <img src="{{ $s['icone'] }}" alt="">
                                                      <span>{{ $s['descricao'] }}</span>
                                                </div>
                                                @endforeach
                                          </div>
                                    </li>
                                    <li>
                                          <label for="">Descrição do Produto:</label>
                                          <p>
                                                {{ $p->descricao_produto }}
                                          </p>
                                    </li>
                                    <li>
                                          <label for="">Detalhes do Produto:</label>
                                          <p>
                                                {{ $p->detalhes_produto }}
                                          </p>
                                    </li>
                                    <li>
                                          <label for="">Informações Importantes:</label>
                                          <p>
                                                {{ $p->informacoes_importantes }}
                                          </p>
                                    </li>
                              </ul>
                        </div>
                        @endforeach
                  </div>
            </div>
      </section>
</main>
<script>
    var banners = @json($banners);
</script>

@stop
