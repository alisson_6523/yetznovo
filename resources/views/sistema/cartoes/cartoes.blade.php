@extends('sistema.layout.admin-layout')

@section('titulo', 'Cartões')

@section('conteudo')

    <main>
    @include('sistema.includes.menu-admin')

        <section class="s-area-portal">
            <div class="topo">
                <ul class="nav-tabs">
                    <li class="active">
                        <a href="" id="tab-cartoes">
                            <i class="icone-cartao"></i>
                        </a>
                    </li>
                    <li>
                        <a href="" id="tab-lote">
                            <i class="icone-lote"></i>
                        </a>
                    </li>
                </ul>
                <a href="" class="btn">
                    <img src="{{ asset('img/plus.svg') }}" alt="">
                </a>
            </div>
            <div class="tab-pane" id="cartoes">
                <div class="todos-cartoes">
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Lote - 1</strong>
                        </div>
                        <strong>N. de Cartões - 999</strong>
                        <strong>Utilizados - 999</strong>
                        <span>Solicitante - André Xavier</span>
                        <span>Data de Solicitação - 05.04.2019</span>
                    </a>
                </div>
            </div>
            <div class="tab-pane" id="lote">
                <div class="todos-cartoes">
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Pedido - 1</strong>
                        </div>
                        <strong>Nome do Pedido - CARDS CAT/ITM ABR19</strong>
                        <strong>Solicitante - André Xavier</strong>
                        <span>Data de Solicitação - 05.04.2019</span>
                        <span>Status - Liberado</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Pedido - 1</strong>
                        </div>
                        <strong>Nome do Pedido - CARDS CAT/ITM ABR19</strong>
                        <strong>Solicitante - André Xavier</strong>
                        <span>Data de Solicitação - 05.04.2019</span>
                        <span>Status - Liberado</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Pedido - 1</strong>
                        </div>
                        <strong>Nome do Pedido - CARDS CAT/ITM ABR19</strong>
                        <strong>Solicitante - André Xavier</strong>
                        <span>Data de Solicitação - 05.04.2019</span>
                        <span>Status - Liberado</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Pedido - 1</strong>
                        </div>
                        <strong>Nome do Pedido - CARDS CAT/ITM ABR19</strong>
                        <strong>Solicitante - André Xavier</strong>
                        <span>Data de Solicitação - 05.04.2019</span>
                        <span>Status - Liberado</span>
                    </a>
                    <a href="" class="item">
                        <div class="n-lote">
                            <button type="button" class="btn-detalhes-lote">
                                <img src="{{ asset('img/icons/bar-cinza-adm.svg') }}" alt="">
                            </button>
                            <strong>N. Pedido - 1</strong>
                        </div>
                        <strong>Nome do Pedido - CARDS CAT/ITM ABR19</strong>
                        <strong>Solicitante - André Xavier</strong>
                        <span>Data de Solicitação - 05.04.2019</span>
                        <span>Status - Liberado</span>
                    </a>
                </div>
            </div>
        </section>

    </main>

@stop