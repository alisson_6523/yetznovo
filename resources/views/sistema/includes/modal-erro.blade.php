<div class="modal" id="modal-erro">
    <div class="overlay"></div>
    <div class="box-white">
        <img src="{{ asset('img/icons/icone-erro.svg') }}" alt="">
        <h2>{{ $mensagem }}</h2>
        <a href="{{ $link }}" class="btn btn-voltar">
            voltar
            <i class="icone-arrow-right"></i>
        </a>
    </div>
</div>