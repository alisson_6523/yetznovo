<div class="modal" id="modal-sucesso">
    <div class="overlay"></div>
    <div class="box-white">
        <img src="{{ asset('img/icons/icone-check-adm.svg') }}" alt="">
        <h2>{!! $mensagem !!}</h2>
        <a href="{{ $link }}" class="btn btn-finalizar">
            finalizar
            <i class="icone-arrow-right"></i>
        </a>
    </div>
</div>