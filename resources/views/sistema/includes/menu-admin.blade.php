<nav>
    <a href="/sistema">
        <div class="logo">
            <i class="icone-yetz"></i>
        </div>
    </a>
    <ul>
    
        @foreach(\Auth::user()->perfil->permissoes as $permissao)
            <li class="{{ request()->is('*/'. limpaString($permissao->pagina->nome)) || request()->is('*/'. limpaString($permissao->pagina->nome) .'/*') ? 'active' : '' }}" title="{{ ucfirst($permissao->pagina->nome) }}">
                <a href="/sistema/{{ limpaString($permissao->pagina->nome) }}">
                    <i class="icone-{{ $permissao->pagina->icone }}"></i>
                </a>
            </li>
        @endforeach

    </ul>
</nav>