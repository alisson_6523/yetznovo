@extends('sistema.layout.admin-layout')

@section('titulo', 'Resgates')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="s-resgates">
            <div class="sidebar-resgate">
                  <form id="form-filtro-resgates">
                        <div class="cont" id="form-pesquisa-inicial">
                              <div class="titulo">
                                    <h1><i class="icone-arrow-right"></i> resgates</h1>
                                    <a style="cursor: pointer" onclick="filtroResgates.exportar()">
                                          <img src="{{ asset("img/icons-adm/icone-exportar.svg") }}" alt="">
                                    </a>
                              </div>
                              <div class="form-group">
                                    <input type="text" id="pesquisa-resgates" name='pesquisa'>
                                    <img src="{{ asset("img/icons-adm/lupa-roxa.svg") }}" alt="">
                              </div>
                              <div class="form-group">
                                    <label for="">Data Inicial</label>
                                    <input type="text" mask="date" name="data_inicial" value="{{ date('d/m/Y', strtotime(\Carbon\Carbon::now()->subMonth())) }}">
                              </div>
                              <div class="form-group">
                                    <label for="">Data Final</label>
                                    <input type="text" mask="date" name="data_final" value="{{ date('d/m/Y', strtotime(\Carbon\Carbon::now())) }}">
                              </div>
                              <div class="form-group">
                                  <label for="">Tipo de Produto</label>
                                  <div class="select-custom">
                                      <input type="hidden" name="tipo_produto" id="pesquisa-tipo-produto">
                                      <a href="" class="item-selected">
                                          <span>Selecione</span>
                                      </a>
                                      <div class="dropdown">
                                          <ul>
                                              <li><a href="" class="resgates-option-cliente" cod-opcao="">Todos</a></li>
                                              @foreach(\App\Models\ProdutoTipo::all() as $tipo)
                                                <li><a href="" class="resgates-option-cliente" cod-opcao="{{ $tipo->cod_tipo }}">{{ $tipo->nome_tipo }}</a></li>
                                              @endforeach
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              <div class="form-group" style="display: block;">
                                    <label for="">Status da Entrega </label>
                                    <div class="todos-pdvs" id="todosStatus">
                                          @foreach (\App\Models\StatusResgate::orderBy('descricao_status', 'ASC')->get() as $s)
                                                <div class="check {{ $s->cod_status != 4 ? 'checked' : '' }}" data-cod="{{ $s->cod_status }}">
                                                      <div class="square"></div>
                                                      <span>{{ $s->descricao_status }}</span>
                                                </div>
                                          @endforeach
                                    </div>
                              </div>

                              <div class="form-group">
                                    <label for="">Cliente</label>
                                    <div class="select-custom">
                                          <input type="hidden" name="cod_cliente">
                                          <a href="" class="item-selected">
                                                <span>Selecione</span>
                                          </a>
                                          <div class="dropdown">
                                                <ul>
                                                    <li>
                                                        <a href="" class="resgates-option-cliente" cod-opcao="">Todos</a>
                                                    </li>
                                                      @foreach (\App\Models\Cliente::all() as $c)
                                                      <li>
                                                            <a href="" class="resgates-option-cliente"
                                                                  cod-opcao="{{ $c->id }}">
                                                                  {{ $c->nome_fantasia }}
                                                            </a>
                                                      </li>
                                                      @endforeach
                                                </ul>
                                          </div>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <label for="">Campanha</label>
                                    <div class="select-custom" id="select-campanha">
                                          <input type="hidden" id="hidden-cod-campanha" name="cod_campanha">
                                          <a href="" class="item-selected">
                                                <span>Selecione</span>
                                          </a>
                                          <div class="dropdown">
                                                <ul id="campanhas-cliente">
                                                </ul>
                                          </div>
                                    </div>
                              </div>
                              <div class="form-group" id="todos-pdvs">
                                    <label for="">PDVS</label>
                                    <div class="todos-pdvs" id='todosPdvs'>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <button type="button" id="btn-filtrar-resgates">filtrar<i class="icone-arrow-right"></i></button>
                              </div>
                        </div>
                        <div class="cont" id="status-lote">
                              <div class="titulo">
                                    <h1><i class="icone-arrow-right"></i> resgates</h1>
                              </div>
                              <h4><span>00</span> linha(s) selecionada(s) </h4>
                              <div class="form-group">
                                    <label for="">Status da Entrega</label>
                                    <div class="select-custom">
                                          <input type="hidden" name="cod_status">
                                          <a href="" class="item-selected">
                                                <span>Selecione</span>
                                          </a>
                                          <div class="dropdown">
                                                <ul>
                                                      @foreach (\App\Models\StatusResgate::orderBy('descricao_status', 'ASC')->get() as $s)
                                                      <li>
                                                            <a href="" cod-opcao="{{ $s->cod_status }}">{{ $s->descricao_status }}</a>
                                                      </li>
                                                      @endforeach
                                                </ul>
                                          </div>
                                    </div>
                              </div>
                              <a style="cursor: pointer" onclick="filtroResgates.exportar()">
                                    <img src="{{ asset("img/icons-adm/icone-exportar.svg") }}" alt="">
                              </a>
                        </div>
                  </form>
            </div>
            <div class="tabela-dados-resgate" id="tabela-resgate">
                    <div class="loader">
                        <i class="fa fa-spinner fa-spin"></i>
                    </div>
                  <ul class="head">
                        <li>
                              <div class="check-square check-all"></div>
                        </li>
                        <li>
                              <span>nome</span>
                        </li>
                        <li>
                              <span>re</span>
                        </li>
                        <li>
                              <span>data-hora</span>
                        </li>
                        <li>
                              <span>Cartão</span>
                        </li>
                        <li>
                              <span>campanha</span>
                        </li>
                        <li>
                              <span>pdv</span>
                        </li>
                        <li>
                              <span>status</span>
                        </li>
                        <li>
                              <span>pontos</span>
                        </li>
                  </ul>
                  <div class="body" id='linhas-resgates'>

                  </div>
            </div>
      </section>

</main>

@stop



@section('onload')
<script>
$(document).ready(function() {
    filtroResgates.filtrar();

    $(document).on("click", "#todosStatus .check", function() {
        $(this).toggleClass("checked");
    });
})
</script>
@stop
