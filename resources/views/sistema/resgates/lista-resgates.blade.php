@foreach ($resgates as $item)
        <div class="item-table item-resgate">
            <ul class="dados">
                <li>
                    <div class="check-square" data-cod_resgate={{ $item->cod_item }}></div>
                </li>
                <li>
                    <span>{{ preg_replace("/_/", " ", $item->carrinho->resgates->first()->user->nome_usuario) }}</span>
                </li>
                <li>
                    <span>{{ $item->carrinho->resgates->first()->user->login }}</span>
                </li>
                <li>
                    <span>{{ $item->carrinho->resgates->first()->data_resgate }}</span>
                </li>
                <li>
                    <span>{{ $item->produto->nome_produto }} - R${{ $item->produto->valor_reais }}</span>
                </li>
                <li>
                    <span>{{ $item->carrinho->resgates->first()->campanha->nome_campanha }}</span>
                </li>
                <li>
                    <span>{{ $item->carrinho->resgates->first()->user->pdv->nome_pdv }}</span>
                </li>
                <li>
                    <span>{{ $item->status_resgate->descricao_status }}</span>
                </li>
                <li>
                    <div class="pts">
                        <i class="icone-circulo-estrela"></i>
                        <i class="seta icone-arrow-right"></i>
                        <span>{{ $item->produto->valor_produto }}</span>
                    </div>
                </li>
                <i class="icone-arrow-down"></i>
            </ul>
            <div class="dados-gerais">
                <div class="item-dados">
                    <div class="title">
                        <i class="icone-user"></i>
                        <span>inf. do usuários</span>
                    </div>
                    <ul>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->nome_usuario }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->celular }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->email }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>Login - {{ $item->carrinho->resgates->first()->user->login }}</span>
                        </li>
                    </ul>
                </div>
                <div class="item-dados">
                    <div class="title">
                        <i class="icone-user"></i>
                        <span>inf. da entrega</span>
                    </div>
                    <ul>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>CEP - {{ $item->carrinho->resgates->first()->user->pdv->cep }} / {{ $item->carrinho->resgates->first()->user->pdv->estado }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->pdv->cidade }} - {{ $item->carrinho->resgates->first()->user->pdv->estado }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->pdv->bairro }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->carrinho->resgates->first()->user->pdv->logradouro }},
                                {{ $item->carrinho->resgates->first()->user->pdv->numero }}{{ ' ('. $item->carrinho->resgates->first()->user->pdv->complemento . ')' }}</span>
                        </li>
                    </ul>
                </div>
                <div class="item-dados">
                    <div class="title">
                        <i class="icone-resgate"></i>
                        <span>INF. D0 RESGATE</span>
                    </div>
                    <ul>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->produto->nome_produto }} - R${{ $item->produto->valor_reais }}</span>
                        </li>
                        <li>
                            <i class="icone-arrow-right"></i>
                            <span>{{ $item->produto->produto_tipo->nome_tipo }}</span>
                        </li>
{{--                        @if ($item->produto->tipo == 2)--}}
{{--                        <li>--}}
{{--                            <i class="icone-arrow-right"></i>--}}
{{--                            <a href="#"><span>(Enviar voucher)</span></a>--}}
{{--                        </li>--}}
{{--                        @endif--}}
                    </ul>
                </div>
                <div class="item-dados">
                    <div class="title">
                        <i class="icone-cartao"></i>
                        <span>STATUS DA ENTREGA</span>
                    </div>
                    <div class="form-group">
                        <div class="select-custom" cod-item='{{ $item->cod_item }}' >
                            <a href="" class="item-selected">
                                <span>{{ $item->status_resgate->descricao_status }}</span>
                            </a>
                            <div class="dropdown">
                                <ul>
                                    @foreach (\App\Models\StatusResgate::all() as $s)
                                        <li><a href="" class="option-status" cod-opcao="{{ $s->cod_status }}">{{ $s->descricao_status }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endforeach
