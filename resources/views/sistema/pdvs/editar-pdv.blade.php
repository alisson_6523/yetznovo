@extends('sistema.layout.admin-layout')

@section('titulo', 'Atualizar Pdv')

@section('conteudo')
    <div class="area-cadastro">
        <form id="editar-pdv">
            <input type="hidden" id="pdv-id" value="{{ $pdv->cod_pdv }}">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/clientes/{{ $pdv->cliente->id }}/{{ limpaString($pdv->cliente->nome_fantasia != null ? $pdv->cliente->nome_fantasia : $pdv->cliente->razao_social) }}">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>atualizar pdv</h2>
                </div>
                <div class="form-group">
                    <div class="codigo">
                        <span>Código do PDV *</span>
                        <i class="icone-arrow-right"></i>
                        <div class="cod-pdv"><span>
                                <input type="text" id="pdv-registro" disabled name="pdv_registro" value="{{ $pdv->registro }}" oninput="this.value = this.value.replace(/[^a-z0-9_]/gi, '').toUpperCase();">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pdv-nome">Nome do PDV *</label>
                    <input type="text" id="pdv-nome" name="pdv_nome" value="{{ $pdv->nome_pdv }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-razao">Razão Social</label>
                    <input type="text" id="pdv-razao" name="pdv_razao" value="{{ $pdv->razao_social }}">
                </div>
                <div class="form-group">
                    <label for="pdv-estaduao">Inscrição Estadual</label>
                    <input type="text" id="pdv-estadual" name="pdv_estadual" value="{{ $pdv->inscricao_estadual }}">
                </div>
                <div class="form-group">
                    <label for="pdv-municipal">Inscrição Municipal</label>
                    <input type="text" id="pdv-municipal" name="pdv_municipal" value="{{ $pdv->inscricao_municipal }}">
                </div>
                <div class="form-group">
                    <label for="pdv-cnpj">CNPJ *</label>
                    <input type="text" mask="cnpj" id="pdv-cnpj" name="pdv_cnpj" value="{{ $pdv->cnpj }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-cep">CEP *</label>
                    <input type="text" mask="cep" id="pdv-cep" name="pdv_cep" value="{{ $pdv->cep }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-estado">Estado *</label>
                    <div class="select-custom">
                        <input type="hidden" name="pdv_estado" id="pdv-estado" value="{{ $pdv->estado }}">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="AC">Acre</a></li>
                                <li><a href="" cod-opcao="AL">Alagoas</a></li>
                                <li><a href="" cod-opcao="AP">Amapá</a></li>
                                <li><a href="" cod-opcao="AM">Amazonas</a></li>
                                <li><a href="" cod-opcao="BA">Bahia</a></li>
                                <li><a href="" cod-opcao="CE">Ceará</a></li>
                                <li><a href="" cod-opcao="DF">Distrito Federal</a></li>
                                <li><a href="" cod-opcao="ES">Espírito Santo</a></li>
                                <li><a href="" cod-opcao="GO">Goiás</a></li>
                                <li><a href="" cod-opcao="MA">Maranhão</a></li>
                                <li><a href="" cod-opcao="MT">Mato Grosso</a></li>
                                <li><a href="" cod-opcao="MS">Mato Grosso do Sul</a></li>
                                <li><a href="" cod-opcao="MG">Minas Gerais</a></li>
                                <li><a href="" cod-opcao="PA">Pará</a></li>
                                <li><a href="" cod-opcao="PB">Paraíba</a></li>
                                <li><a href="" cod-opcao="PR">Paraná</a></li>
                                <li><a href="" cod-opcao="PE">Pernambuco</a></li>
                                <li><a href="" cod-opcao="PI">Piauí</a></li>
                                <li><a href="" cod-opcao="RJ">Rio de Janeiro</a></li>
                                <li><a href="" cod-opcao="RN">Rio Grande do Norte</a></li>
                                <li><a href="" cod-opcao="RS">Rio Grande do Sul</a></li>
                                <li><a href="" cod-opcao="RO">Rondônia</a></li>
                                <li><a href="" cod-opcao="RR">Roraima</a></li>
                                <li><a href="" cod-opcao="SC">Santa Catarina</a></li>
                                <li><a href="" cod-opcao="SP">São Paulo</a></li>
                                <li><a href="" cod-opcao="SE">Sergipe</a></li>
                                <li><a href="" cod-opcao="TO">Tocantins</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="pdv-cidade">Cidade *</label>
                    <input type="text" id="pdv-cidade" name="pdv_cidade" value="{{ $pdv->cidade }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-logradouro">Logradouro *</label>
                    <input type="text" id="pdv-logradouro" name="pdv_logradouro" value="{{ $pdv->logradouro }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-bairro">Bairro *</label>
                    <input type="text" id="pdv-bairro" name="pdv_bairro" value="{{ $pdv->bairro }}" required>
                </div>
                <div class="double-form">
                    <div class="form-group">
                        <label for="pdv-complemento">Complemento</label>
                        <input type="text" id="pdv-complemento" name="pdv_complemento" value="{{ $pdv->complemento }}">
                    </div>
                    <div class="form-group">
                        <label for="pdv-numero">Número *</label>
                        <input type="text" id="pdv-numero" name="pdv_numero" value="{{ $pdv->numero }}" required oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
                    </div>
                </div>
                <div class="form-group">
                    <label for="pdv-telefone">Tel. *</label>
                    <input type="text" mask="telefone" id="pdv-telefone" name="pdv_telefone" value="{{ $pdv->telefone }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-email">E-mail *</label>
                    <input type="email" id="pdv-email" name="pdv_email" value="{{ $pdv->email }}" required>
                </div>
                <div class="form-group">
                    <label for="pdv-site">Site</label>
                    <input type="text" id="pdv-site" name="pdv_site" value="{{ $pdv->site }}">
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Enviar logo</label>
                    <div class="box">
                        <input type="file" name="file-7[]" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"/>
                        <label for="file-7"><span>{{ $pdv->logo }}</span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="nome-cliente">
                    <i class="icone-cliente"></i>
                    <input type="hidden" id="cliente-id" value="{{ $pdv->cliente->id }}">
                    <p><strong>cliente</strong> - {{ $pdv->cliente->nome_fantasia != null ? $pdv->cliente->nome_fantasia : $pdv->cliente->razao_social }}</p>
                </div>
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para atualizar o PDV.
                    </p>
                    <button type="submit">atualizar <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>


    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR O PDV', 'link' => '/sistema/clientes/' . $pdv->cliente->id . '/' . limpaString($pdv->cliente->nome_fantasia != null ? $pdv->cliente->nome_fantasia : $pdv->cliente->razao_social)])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'PDV ATUALIZADO COM SUCESSO!', 'link' => '/sistema/clientes/' . $pdv->cliente->id . '/' . limpaString($pdv->cliente->nome_fantasia != null ? $pdv->cliente->nome_fantasia : $pdv->cliente->razao_social)])

@stop
