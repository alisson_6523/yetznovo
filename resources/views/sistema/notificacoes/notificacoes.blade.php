@extends('sistema.layout.admin-layout')

@section('titulo', 'Notificações')

@section('conteudo')

    <main>
        @include('sistema.includes.menu-admin')

        <section class="area-conteudo-modulo">
            <div class="topo">
                <h2 class="title">
                    <i class="icone-arrow-right"></i>
                    NOTIFICAÇÕES
                </h2>
                <div class="filtro">
{{--                    <div class="form-group">--}}
{{--                        <label for="">Tipo</label>--}}
{{--                        <div class="select-custom">--}}
{{--                            <input type="hidden" name="cod_cliente">--}}
{{--                            <a href="" class="item-selected">--}}
{{--                                <span>Selecione</span>--}}
{{--                            </a>--}}
{{--                            <div class="dropdown">--}}
{{--                                <ul>--}}
{{--                                    <li><a href="" cod-opcao="1"></a></li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="">Cliente</label>--}}
{{--                        <div class="select-custom">--}}
{{--                            <input type="hidden" name="cod_cliente" id="filtro-notificacoes-cliente">--}}
{{--                            <a href="" class="item-selected">--}}
{{--                                <span>Selecione</span>--}}
{{--                            </a>--}}
{{--                            <div class="dropdown">--}}
{{--                                <ul>--}}
{{--                                    @if(!empty(\Auth::user()->cliente_id))--}}
{{--                                        <li><a href="" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>--}}
{{--                                    @else--}}
{{--                                        @foreach($clientes as $cliente)--}}
{{--                                            <li><a href="" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>--}}
{{--                                        @endforeach--}}
{{--                                    @endif--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="">Data de inicio</label>--}}
{{--                        <input type="text" mask="date" id="filtro-notificacoes-dt-inicio">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <label for="">Data Fim</label>--}}
{{--                        <input type="text" mask="date" id="filtro-notificacoes-dt-fim">--}}
{{--                    </div>--}}
{{--                    <div class="form-group">--}}
{{--                        <input type="text" id="filtro-notificacoes-busca">--}}
{{--                        <button type="button">--}}
{{--                            <i class="icone-lupa"></i>--}}
{{--                        </button>--}}
{{--                    </div>--}}
                </div>
                <div class="acoes">
                    <a href="/sistema/notificacoes/adicionar" class="btn">
                        adicionar notificação <i class="icone-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="area-todas-notificacoes">
                @foreach ($notificacoes as $n)

                    <div class="area-box">
                        <div class="box-notificacao">
                            <div class="info-geral">
                                <button type="button">
                                    <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                                </button>
                                <div class="info">
                                    <h2>{{ $n->titulo }}</h2>
                                    <div class="dados">
                                        <ul class="list">
                                            <li>Notificação Web</li>
                                            <li>{{ $n->campanha->cliente->nome_fantasia ?? '-' }}</li>
                                            <li>{{ $n->campanha->nome_campanha ?? '-' }}</li>
                                            <li>D.E - {{ $n->data_envio }}</li>
                                        </ul>
                                        <ul class="list-numbers">
                                            <li>
                                                {{-- enviados --}}
                                                <img src="{{ asset('img/icone-envelope.svg') }}" alt="">
                                                <i class="seta icone-arrow-right"></i>
                                                <span>{{ $n->enviados }}</span>
                                            </li>

                                            <li>
                                                {{-- clickados(link) --}}
                                                <img src="{{ asset('img/cursor.svg') }}" alt="">
                                                <i class="seta icone-arrow-right"></i>
                                                <span>-</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="area-mensagem">
                                <div class="topo-msg">
                                    <h4>Mensagem</h4>
                                    <i class="seta icone-arrow-down"></i>
                                </div>
                                <div class="desc">
                                    <p>
                                        {{$n->mensagem }}
                                    </p>
                                    <div class="pdvs">
                                        <div class="item">
                                            <div class="name">
                                                <span>ILHAS</span>
                                            </div>
                                            <div class="todos">
                                                @foreach(json_decode($n->pdvs) as $ilha)
                                                    <ul>{{ $ilha }}</ul>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </section>
    </main>
@stop
