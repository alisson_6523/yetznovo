@extends('sistema.layout.admin-layout')

@section('titulo', 'Adicionar Notificação')

@section('conteudo')

<div class="area-cadastro">
      <form id="form-cadastro-notificacao">
            {{ csrf_field() }}
            <div class="s-esq">
                  <div class="title">
                        <a href="/sistema/notificacoes">
                              <div class="circle">
                                    <i class="icone-arrow-left"></i>
                              </div>
                        </a>
                        <h2>CADASTRAR NOTIFICAÇÃO</h2>
                  </div>
                  {{-- <div class="form-group">
                    <label for="">Tipo</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="1">E-mail MKT</a></li>
                                <li><a href="" cod-opcao="2">E-mail MKT</a></li>
                            </ul>
                        </div>
                    </div>
                </div> --}}
                  <div class="form-group">
                        <label for="">Categoria</label>
                        <div class="select-custom">
                              <input type="hidden" id='hidden-cod-categoria' name="cod_categoria">
                              <a href="" class="item-selected">
                                    <span>Selecione</span>
                              </a>
                              <div class="dropdown">
                                    <ul>
                                          @foreach(\App\Models\NotificacaoCategorium::all() as $n)
                                          <li>
                                                <a href="" class="option-categoria" cod-opcao="{{ $n->cod_categoria }}">
                                                      {{ $n->nome_categoria }}
                                                </a>
                                          </li>
                                          @endforeach
                                    </ul>
                              </div>
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="">Titulo</label>
                        <input type="text" name="titulo" id="titulo">
                  </div>

                  <div class="form-group">
                        <label for="">Mensagem</label>
                        <textarea name="mensagem" cols="30" rows="5" id="mensagem"></textarea>
                  </div>
                  {{-- <div class="form-group">
                    <label for="" class="label-enviar-logo">Escolha a Imagem</label>
                    <div class="box">
                        <input type="file" name="file-7[]" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="file-7"><span></span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                </div> --}}
                  <div class="form-group">
                        <label for="">Cliente *</label>
                        <div class="select-custom">
                              <input type="hidden" name="cod_cliente" id="cadastro-notificacao-cliente">
                              <a href="" class="item-selected">
                                    <span>Selecione</span>
                              </a>
                              <div class="dropdown">
                                    <ul>
                                          @if(!empty(\Auth::user()->cliente_id))
                                          <li><a href="" id="cadastro-notificacao-option-cliente"
                                                      cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a>
                                          </li>
                                          @else
                                          @foreach($clientes as $cliente)
                                          <li><a href="" id="cadastro-notificacao-option-cliente"
                                                      cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                          @endforeach
                                          @endif
                                    </ul>
                              </div>
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="">Campanha *</label>
                        <div class="select-custom">
                              <input type="hidden" name="cod_campanha" id="cadastro-notificacao-campanha">
                              <a href="" class="item-selected">
                                    <span>Selecione</span>
                              </a>
                              <div class="dropdown">
                                    <ul id="cadastro-notificacao-select-campanha">

                                    </ul>
                              </div>
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="">PDVs / Ilhas *</label>
                        <div class="box-ilhas" id="cadastro-notificacao-pdv-ilhas">
                              Selecione um Cliente e uma Campanha.
                        </div>
                  </div>
            </div>

            <div class="s-dir">
                  <div class="box-white">
                        <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                        <p>
                              Preencha os campos ao lado e clique no botão abaixo para cadastrar a notificação.
                        </p>
                        <button type="submit" id="btn-finalizar-notificacao">finalizar notificação <i class="icone-arrow-right"></i></button>
                  </div>

                  <span class="alert">Campos com * são obrigatórios.</span>

                  <div class="previa-notificacao">
                        <div class="container-img">
                              <img src="/img/icones-notificacoes/novo_cartao.svg" alt="">
                        </div>

                        <div class="title-descricao">
                              <h3></h3>
                              <p></p>
                        </div>
                  </div>

            </div>
      </form>
</div>

@include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DA NOTIFICAÇÃO', 'link' =>
'/sistema/notificacoes/adicionar'])

@include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE NOTIFICAÇÃO REALIZADO COM SUCESSO!', 'link' =>
'/sistema/notificacoes'])
@stop
