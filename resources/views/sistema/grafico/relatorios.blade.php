<div class="container-relatorio-acesso">
      <div class="relatorio-acesso">
            <div class="text-img-relatorio">
                  <img src="/img/icons-adm/relatorio-01.png" alt="">
                  <h1>relatório de acesso</h1>
            </div>

            <form action="">
            <div class="container-form">
                        <label for="">Datas</label>
                        <input id="range" type="text">
                  </div>

                  <!-- <div class="container-form">
                        <label for="">Data de inicio</label>
                        <input id="data-inicial" type="date">
                  </div>

                  <div class="container-form">
                        <label for="">Data de Fim</label>
                        <input id="data-final" type="date">
                  </div> -->
            </form>

      </div>

      <div class="relatorio-graficos-acesso">
            <div class="container-esq">

                  <div class="container-resumo-acesso">

                        <div class="itens-acesso">
                              <img src="/img/icons-adm/relatorio-02.png" alt="">
                              <img src="/img/icons-adm/relatorio-03.png" alt="">
                              <div class="text-acesso">
                                    <h1 id="usuarios-que-logaram"></h1>
                                    <p>usuários</p>
                              </div>
                        </div>

                        <div class="itens-acesso">
                              <img src="/img/icons-adm/relatorio-02.png" alt="">
                              <img src="/img/icons-adm/relatorio-03.png" alt="">
                              <div class="text-acesso">
                                    <h1 id="usuarios-recorrentes"></h1>
                                    <p>usuários recorrentes</p>
                              </div>
                        </div>

                        <div class="itens-acesso">
                              <img src="/img/icons-adm/relatorio-04.png" alt="">
                              <img src="/img/icons-adm/relatorio-03.png" alt="">
                              <div class="text-acesso">
                                    <h1 id="total-logins"></h1>
                                    <p>total de sessões</p>
                              </div>
                        </div>

                        <div class="itens-acesso">
                              <img src="/img/icons-adm/relatorio-05.png" alt="">
                              <img src="/img/icons-adm/relatorio-03.png" alt="">
                              <div class="text-acesso">
                                    <h1 id=sessoes-por-usuario></h1>
                                    <p>n. de sessões por usuários</p>
                              </div>
                        </div>
                        
                        <div class="itens-acesso">
                              <img src="/img/icons-adm/relatorio-06.png" alt="">
                              <img src="/img/icons-adm/relatorio-03.png" alt="">
                              <div class="text-acesso">
                                    <h1 id="paginas-por-sessao"></h1>
                                    <p>páginas por sessão</p>
                              </div>
                        </div>

                  </div>

                  <div class="visualiacao-paginas">
                        <h1>visualização de paginas</h1>
                        <div id="line_top_x"></div>
                        <div class="text-paginas">
                              <div class="visitas-text">
                                    <p>páginas visitadas</p>
                                    <h3 id="paginas-visitadas"></h3>
                              </div>

                              <div class="visitas-text">
                                    <p>tempo médio na página</p>
                                    <h3 id=tempo-medio-pagina>-</h3>
                              </div>
                        </div>
                  </div>

                  <div class="visualizacao-usuarios">
                        <h1>usuários por horário do dia</h1>
                        <div id="series_chart_div" style="height: 282px;"></div>   
                  </div>

                  <div class="container-sessoes-acesso">
                        <h3>sessões por dispositivos</h3>

                        <div class="sessoes-acesso">

                              <div class="itens-sessoes">
                                    <div class="itens-img-sessoes">
                                          <img src="/img/icons-adm/relatorio-07.png" alt="">
                                          <img src="/img/icons-adm/relatorio-08.png" alt="">
                                    </div>    

                                    <div class="itens-text-sessoes">
                                          <h1>celular</h1>
                                          
                                          <h2 id="acessos-celular">0%</h2>
                                    </div>
                              </div>

                              <div class="itens-sessoes">
                                    <div class="itens-img-sessoes">
                                          <img src="/img/icons-adm/relatorio-09.png" alt="">
                                          <img src="/img/icons-adm/relatorio-08.png" alt="">
                                    </div>    

                                    <div class="itens-text-sessoes">
                                          <h1>computador</h1>
                                          
                                          <h2 id="acessos-computador">0%</h2>
                                    </div>
                              </div>

                              <div class="itens-sessoes">
                                    <div class="itens-img-sessoes">
                                          <img src="/img/icons-adm/relatorio-10.png" alt="">
                                          <img src="/img/icons-adm/relatorio-08.png" alt="">
                                    </div>    

                                    <div class="itens-text-sessoes">
                                          <h1>tablet</h1>
                                          
                                          <h2 id="acessos-tablet">0%</h2>
                                    </div>
                              </div>

                        </div>
                  </div>

            </div>

            <div class="container-dir">
                  <div class="container-paginas-acesso" data-start="true">
                        <div class="paginas-mais-acessadas">
                              <h1>páginas mais acessadas</h1>

                              <div id="ranking-paginas"></div>
                              
                              <!-- <div class="loader-acessos">
                                    <h3>1-home</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="85"></span>
                                          <p class="porcentagem-acessos" data-porc="85">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>2 - pedidos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="65"></span>
                                          <p class="porcentagem-acessos" data-porc="65">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>3 - movimentação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="10"></span>
                                          <p class="porcentagem-acessos" data-porc="10">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>4 - notificação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="35"></span>
                                          <p class="porcentagem-acessos" data-porc="35">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>5 - regulamento</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="33"></span>
                                          <p class="porcentagem-acessos" data-porc="33">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>6 - resgate</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="75"></span>
                                          <p class="porcentagem-acessos" data-porc="75">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>7 - - lista de desejos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="11"></span>
                                          <p class="porcentagem-acessos" data-porc="11">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>8 - videos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="23"></span>
                                          <p class="porcentagem-acessos" data-porc="23">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>9 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="77"></span>
                                          <p class="porcentagem-acessos" data-porc="77">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>10 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="59"></span>
                                          <p class="porcentagem-acessos" data-porc="59">0%</p>
                                    </div>
                              </div> -->
                        </div>

                        <div class="categorias-mais-acessadas">
                              <h1>categorias mais acessadas</h1>

                              <div  id="ranking-categorias"></div>
                              
                              <!-- <div class="loader-acessos">
                                    <h3>1-home</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="85"></span>
                                          <p class="porcentagem-acessos" data-porc="85">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>2 - pedidos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="65"></span>
                                          <p class="porcentagem-acessos" data-porc="65">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>3 - movimentação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="10"></span>
                                          <p class="porcentagem-acessos" data-porc="10">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>4 - notificação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="35"></span>
                                          <p class="porcentagem-acessos" data-porc="35">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>5 - regulamento</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="33"></span>
                                          <p class="porcentagem-acessos" data-porc="33">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>6 - resgate</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="75"></span>
                                          <p class="porcentagem-acessos" data-porc="75">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>7 - - lista de desejos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="11"></span>
                                          <p class="porcentagem-acessos" data-porc="11">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>8 - videos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="23"></span>
                                          <p class="porcentagem-acessos" data-porc="23">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>9 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="77"></span>
                                          <p class="porcentagem-acessos" data-porc="77">0%</p>
                                    </div>
                              </div>
                              
                              <div class="loader-acessos">
                                    <h3>10 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="59"></span>
                                          <p class="porcentagem-acessos" data-porc="59">0%</p>
                                    </div>
                              </div> -->
                        </div>

                        <div class="produtos-mais-acessadas">
                              <h1>produtos mais acessados</h1>

                              <div id="ranking-produtos"></div>
                              
                              <!-- <div class="loader-acessos">
                                    <h3>1-home</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="85"></span>
                                          <p class="porcentagem-acessos" data-porc="85">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>2 - pedidos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="65"></span>
                                          <p class="porcentagem-acessos" data-porc="65">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>3 - movimentação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="10"></span>
                                          <p class="porcentagem-acessos" data-porc="10">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>4 - notificação</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="35"></span>
                                          <p class="porcentagem-acessos" data-porc="35">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>5 - regulamento</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="33"></span>
                                          <p class="porcentagem-acessos" data-porc="33">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>6 - resgate</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="75"></span>
                                          <p class="porcentagem-acessos" data-porc="75">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>7 - - lista de desejos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="11"></span>
                                          <p class="porcentagem-acessos" data-porc="11">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>8 - videos</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="23"></span>
                                          <p class="porcentagem-acessos" data-porc="23">0%</p>
                                    </div>
                              </div>

                              <div class="loader-acessos">
                                    <h3>9 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="77"></span>
                                          <p class="porcentagem-acessos" data-porc="77">0%</p>
                                    </div>
                              </div>
                              
                              <div class="loader-acessos">
                                    <h3>10 - assai - R$ 50</h3>
                                    <p>5 acessos</p>

                                    <div class="container-width">
                                          <span></span>
                                          <span class="span-width" data-width="59"></span>
                                          <p class="porcentagem-acessos" data-porc="59">0%</p>
                                    </div>
                              </div> -->
                        </div>
                        
                  </div>

                  <div class="container-app-acesso">
                        <h1>app yetz cards</h1>
                        <div class="container-porcentagem-mobile">
                              <div class="apple">
                                    <div class="apple-text">
                                          <h3>IOS</h3>
                                          <p id="acessos-ios">-</p>
                                    </div>

                                    <div class="apple-img">
                                          <img src="/img/icons-adm/relatorio-11.png" alt="">
                                          <img src="/img/icons-adm/relatorio-12.png" alt="">
                                    </div>
                              </div>

                              <div class="donuts-apple-android">
                                    <div id="donutchart" style="width: 170px; height: 170px;"></div> 
                                    <div class="img-mobile-acesso">
                                          <img src="/img/icons-adm/relatorio-17.png" alt="">
                                    </div>
                              </div>

                              <div class="android">
                                    <div class="android-img">
                                          <img src="/img/icons-adm/relatorio-13.png" alt="">
                                          <img src="/img/icons-adm/relatorio-14.png" alt="">
                                    </div>    
                                    
                                    <div class="android-text">
                                          <h3>ANDROID</h3>
                                          <p id="acessos-android">-</p>
                                    </div>
                              </div>
                              
                              <div class="total-acesso-usuario">
                                    <div class="total-acesso">
                                          <img src="/img/icons-adm/relatorio-15.png" alt="">
                                          <img src="/img/icons-adm/relatorio-18.png" alt="">
                                          <p>total de acessos</p>
                                          <h3 id="total-acessos-app">-</h3>
                                    </div>

                                    <div class="total-usuario">
                                          <img src="/img/icons-adm/relatorio-16.png" alt="">
                                          <img src="/img/icons-adm/relatorio-18.png" alt="">
                                          <p>total de usuario</p>
                                          <h3 id="total-usuarios-app">-</h3>
                                    </div>
                              </div>
                        </div>
                  </div>      
            </div>
      </div>
</div>