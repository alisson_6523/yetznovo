<div class="container-card-resgatados">
      <!-- resumo de pts -->
      <div class="card-resumo">
            <div class="container-card-resumo">
                  <div class="pts-distribuidos">
                        <div class="toggle toggle-resumo">
                              <span></span>
                        </div>

                        <div class="img-estrela">
                              <img src="/img/icons-adm/card-01.png" alt="">
                        </div>      

                        <div class="img-seta-baixo">
                              <img src="/img/icons-adm/card-02.png" alt="">
                        </div>

                        <div class="text-pts-distribuidos">
                              <h1 id="pts-distribuidos"></h1>
                              <p>pts. distribuídos.</p>
                        </div>
                  </div>
                  
                  <div class="pts-resgatados">
                        <div class="img-pts-resgatados">
                              <img src="/img/icons-adm/card-03.png" alt="">
                              <img src="/img/icons-adm/card-04.png" alt="">
                        </div>

                        <div class="text-pts-resgatados">
                              <h1 id="pts-resgatados"></h1>
                              <p>pts. resgatados</p>
                        </div>
                  </div>
                  
                  <div class="pts-resgatados">
                        <div class="img-pts-resgatados">
                              <img src="/img/icons-adm/card-03.png" alt="">
                              <img src="/img/icons-adm/card-04.png" alt="">
                        </div>

                        <div class="text-pts-resgatados">
                              <h1 id="pts-disponiveis"></h1>
                              <p>pts. disponíveis</p>
                        </div>
                  </div>
            </div>

            <div class="container-table">
                  <div class="table-topo">
                        <h1>resumo de pontos mensal</h1>
                        <p>2019</p>
                  </div>          
                  <table>
                        <tr class="table-header">
                              <th>jan</th>
                              <th>fev</th> 
                              <th>mar</th>
                              <th>abr</th>
                              <th>mai</th> 
                              <th>jun</th>
                              <th>jul</th>
                              <th>ago</th> 
                              <th>set</th>
                              <th>out</th>
                              <th>nov</th> 
                              <th>dez</th>
                        </tr>

                        <tr class="table-row">
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                        </tr>

                        <tr class="table-resgatados">
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                              <td>-</td>
                              <td>-</td> 
                              <td>-</td>
                        </tr>

                        <tr class="table-empty">
                        </tr>
                  </table>
            </div>
      </div>

      <!-- usuarios -->
      <div class="card-usuarios">
            <div class="container-card-usuarios">
                  <div class="pts-distribuidos">
                        <div class="toggle toggle-usuario">
                              <span></span>
                        </div>

                        <div class="img-estrela">
                              <img src="/img/icons-adm/card-07.png" alt="">
                        </div>      

                        <div class="img-seta-baixo">
                              <img src="/img/icons-adm/card-02.png" alt="">
                        </div>

                        <div class="text-pts-distribuidos">
                              <h1 id="total-usuarios"></h1>
                              <p>total de usuários</p>
                        </div>
                  </div>
                  
                  <div class="pts-resgatados">
                        <div class="img-pts-resgatados">
                              <img src="/img/icons-adm/card-06.png" alt="">
                              <img src="/img/icons-adm/card-04.png" alt="">
                        </div>

                        <div class="text-pts-resgatados">
                              <h1 id="usuarios-completos"></h1>
                              <p>completos</p>
                        </div>
                  </div>
                  
                  <div class="pts-resgatados">
                        <div class="img-pts-resgatados">
                              <img src="/img/icons-adm/card-13.png" alt="">
                              <img src="/img/icons-adm/card-04.png" alt="">
                        </div>

                        <div class="text-pts-resgatados">
                              <h1 id="usuarios-incompletos"></h1>
                              <p>incompletos</p>
                        </div>
                  </div>
            </div>

            <div class="container-card-ilha" data-start="true">
                  <h1>cadastros por ilha</h1>

                  <div id="usuarios-ilhas"></div>

                  <!-- <h1>inscrições por ilha</h1>

                  <div class="loader-user">
                        <span>cc credicard ret at</span>
                        <p>41 / 32 usu</p>

                        <div class="container-width">
                              <span></span>
                              <span class="span-width" data-width="90"></span>
                              <p class="porcentagem-user" data-porc="90">%</p>
                        </div>
                  </div>

                  <div class="loader-user">
                        <span>cc credicard ret at</span>
                        <p>41 / 32 usu</p>

                        <div class="container-width">
                              <span></span>
                              <span class="span-width" data-width="70"></span>
                              <p class="porcentagem-user" data-porc="70">%</p>
                        </div>
                  </div>

                  <div class="loader-user">
                        <span>cc credicard ret at</span>
                        <p>41 / 32 usu</p>

                        <div class="container-width">
                              <span></span>
                              <span class="span-width" data-width="50"></span>
                              <p class="porcentagem-user" data-porc="50,37">%</p>
                        </div>
                  </div>

                  <div class="loader-user">
                        <span>cc credicard ret at</span>
                        <p>41 / 32 usu</p>

                        <div class="container-width">
                              <span></span>
                              <span class="span-width" data-width="100"></span>
                              <p class="porcentagem-user" data-porc="100">%</p>
                        </div>
                  </div> -->
            </div>
      </div>

      <!-- permissão -->
      <div class="card-permissao">
            <div class="card-img-text">
                  <img src="/img/icons-adm/card-08.png" alt="">
                  <p>envio de sms e whatsapp</p>
            </div>

            <div class="pts-resgatados">
                  <div class="img-pts-resgatados">
                        <img src="/img/icons-adm/card-09.png" alt="">
                        <img src="/img/icons-adm/card-04.png" alt="">
                  </div>

                  <div class="text-pts-resgatados">
                        <h1 id="notificacao-autorizada"></h1>
                        <p>autorizam</p>
                  </div>
            </div>

            <div class="pts-resgatados">
                  <div class="img-pts-resgatados">
                        <img src="/img/icons-adm/card-15.png" alt="">
                        <img src="/img/icons-adm/card-04.png" alt="">
                  </div>

                  <div class="text-pts-resgatados">
                        <h1 id="notificacao-nao-autorizada"></h1>
                        <p>não autorizam</p>
                  </div>
            </div>

      </div>

      <!-- resgates  -->
      <div class="card-resgates">
            <div class="container-card-resgates">
                  <div class="toggle toggle-resgates">
                        <span></span>
                  </div>

                  <div class="total-resg">
                        <div class="container-img-resg">
                              <img src="/img/icons-adm/card-11.png" alt="">
                              <div class="container-seta-lado">
                                    <img src="/img/icons-adm/card-12.png" alt="">
                              </div>
                        </div>

                        <div class="container-text-resg">
                              <h1 id="total-resgates"><span>resg.</span></h1>
                        </div>
                  </div>

                  <h2>mais resgatados</h2>
                  
                  <div class="mais-resg">
                        <div class="container-img-mais">
                              <img src="/img/icons-adm/card-14.png" alt="">
                              <p>
                                    <span>1</span>
                              </p>
                        </div>

                        <div class="text-mais">
                              <h4>outback - R$ 120</h4>
                        </div>
                  </div>

                  <div class="mais-resg">
                        <div class="container-img-mais">
                              <img src="/img/icons-adm/card-14.png" alt="">
                              <p>
                                    <span>1</span>
                              </p>
                        </div>

                        <div class="text-mais">
                              <h4>outback - R$ 120</h4>
                        </div>
                  </div>

                  <div class="mais-resg">
                        <div class="container-img-mais">
                              <img src="/img/icons-adm/card-14.png" alt="">
                              <p>
                                    <span>1</span>
                              </p>
                        </div>

                        <div class="text-mais">
                              <h4>outback - R$ 120</h4>
                        </div>
                  </div>
            </div>

            <div class="percentual-men-women" data-start="true">
                  <div class="container-wonem">
                        <div class="text-img">
                              <div class="img-wonem">
                                    <img src="/img/icons-adm/card-17.png" alt="">
                                    <img src="/img/icons-adm/card-12.png" alt="">
                              </div>

                              <div class="text-wonem">
                                    <h1 id="total-produtos-resgatados-mulheres"></h1>
                              </div>
                        </div>  

                        <div id="produtos-resgatados-mulheres"></div>
                        
                        <!-- <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="70"></span>
                                    <p class="porcentagem-user" data-porc="70">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="50"></span>
                                    <p class="porcentagem-user" data-porc="50,37">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="100"></span>
                                    <p class="porcentagem-user" data-porc="100">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="50"></span>
                                    <p class="porcentagem-user" data-porc="50,37">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="100"></span>
                                    <p class="porcentagem-user" data-porc="100">%</p>
                              </div>
                        </div> -->
                  </div>

                  <div class="container-men">
                        <div class="text-img">
                              <div class="img-wonem">
                                    <img src="/img/icons-adm/card-18.png" alt="">
                                    <img src="/img/icons-adm/card-12.png" alt="">
                              </div>

                              <div class="text-wonem">
                                    <h1 id="total-produtos-resgatados-homens"></h1>
                              </div>
                        </div>  

                        <div id="produtos-resgatados-homens"></div>
                        
                        <!-- <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="70"></span>
                                    <p class="porcentagem-user" data-porc="70">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="50"></span>
                                    <p class="porcentagem-user" data-porc="50,37">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="100"></span>
                                    <p class="porcentagem-user" data-porc="100">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="50"></span>
                                    <p class="porcentagem-user" data-porc="50,37">%</p>
                              </div>
                        </div>

                        <div class="loader-user">
                              <span>cc credicard ret at</span>
                              <p>41 / 32 usu</p>

                              <div class="container-width">
                                    <span></span>
                                    <span class="span-width" data-width="100"></span>
                                    <p class="porcentagem-user" data-porc="100">%</p>
                              </div>
                        </div> -->
                  </div> 
            </div>
      </div>

      <!-- ativos -->
      <div class="card-ativos">
            <div class="card-img-text">
                  <img src="/img/icons-adm/card-08.png" alt="">
                  <p>usuários ativos</p>
            </div>

            <div class="pts-resgatados">
                  <div class="img-pts-resgatados">
                        <img src="/img/icons-adm/card-10.png" alt="">
                        <img src="/img/icons-adm/card-04.png" alt="">
                  </div>

                  <div class="text-pts-resgatados">
                        <h1 id="usuarios-ativos-mensal">-</h1>
                        <p>mensal</p>
                  </div>
            </div>

            <div class="pts-resgatados">
                  <div class="img-pts-resgatados">
                        <img src="/img/icons-adm/card-19.png" alt="">
                        <img src="/img/icons-adm/card-04.png" alt="">
                  </div>

                  <div class="text-pts-resgatados">
                        <h1 id="usuarios-ativos-semanal">-</h1>
                        <p>semanal</p>
                  </div>
            </div>

            <div class="pts-resgatados">
                  <div class="img-pts-resgatados">
                        <img src="/img/icons-adm/card-20.png" alt="">
                        <img src="/img/icons-adm/card-04.png" alt="">
                  </div>

                  <div class="text-pts-resgatados">
                        <h1 id="usuarios-ativos-diario">-</h1>
                        <p>diario</p>
                  </div>
            </div>

      </div>
</div>