@extends('sistema.layout.admin-layout')

@section('titulo', 'Relatórios')

@section('conteudo')

      <main class="{{ request()->is('*/grafico') ? 'grafico' : '' }}">
            <input id="csrf_token" type="hidden" name="csrf_token" value="{{ csrf_token() }}">
            @include('sistema.includes.menu-admin')

            <section class="area-conteudo-modulo">
                  <div class="loader">
                        <i class="fa fa-spinner fa-spin"></i>
                  </div>

                  <div class="topo" style="display: none;">
                        <h2 class="title">
                              <i class="icone-arrow-right"></i>RELATÓRIO GERAL
                        </h2>

                        <div class="acoes">

                              <div class="container-form" data-id="cliente">
                                    <label for="">Cliente</label>

                                    @if(count($clientes) == 1)
                                          <div class="container-text-img">
                                                <p id='cliente-selecionado' data-cliente-id="{{ $clientes[0]->id }}">{{ $clientes[0]->nome }}</p>
                                          </div>
                                    @else
                                          <div class="container-text-img">
                                                <p id='cliente-selecionado' data-cliente-id="0">Todos</p>
                                                <img src="/img/icons-adm/icon-seta-baixo.png" alt="">
                                          </div>

                                          <ul id="cliente" class="teste asasd" data-id="filtro-relatorio-cliente">
                                                <li data-cliente-id="0">
                                                      <p>Todos</p>
                                                </li>

                                                @foreach($clientes as $cliente)
                                                      <li data-cliente-id="{{ $cliente->id }}">
                                                            <p>{{ $cliente->nome }}</p>
                                                      </li>
                                                @endforeach
                                          </ul>
                                    @endif

                                    <input type="hidden" id="filtro-relatorio-cliente">
                              </div>
                              
                              <div class="container-form" data-id="campanhas">
                                    <label for="">Campanhas</label>

                                    <div class="container-text-img">
                                          <p id='campanha-selecionada' data-campanha-id="0">Todos</p>
                                          <img src="/img/icons-adm/icon-seta-baixo.png" alt="">
                                    </div>

                                    <ul id="campanhas" data-id="filtro-relatorio-campanhas">
                                          <li data-campanha-id="0">
                                                <p>Todos</p>
                                          </li>

                                          @foreach($campanhas as $campanha)
                                                <li data-campanha-id="{{ $campanha->cod_campanha }}" data-cliente-id="{{ $campanha->cliente_id }}">
                                                      <p>{{ $campanha->nome_campanha }}</p>
                                                </li>
                                          @endforeach
                                    </ul>

                                    <input type="hidden" id="filtro-relatorio-campanhas">
                              </div>

                              <div class="container-form" data-id="pdv">
                                    <label for="">PDVs</label>

                                    <div class="container-text-img">
                                          <p id='pdv-selecionado' data-pdv-id='0'>Todos</p>
                                          <img src="/img/icons-adm/icon-seta-baixo.png" alt="">
                                    </div>

                                    <ul id="pdv" data-id="filtro-relatorio-pdv">
                                          <li data-campanhas-id="[0]" data-cliente-id="0">
                                                <p>Todos</p>
                                          </li>

                                          @foreach($pdvs as $pdv)
                                                <li data-pdv-id="{{ $pdv->cod_pdv }}" data-campanhas-id="{{ $pdv->campanhas->pluck('cod_campanha') }}">
                                                      <p>{{ $pdv->nome_pdv }}</p>
                                                </li>
                                          @endforeach
                                    </ul>

                                    <input type="hidden" id="filtro-relatorio-pdv">
                              </div>
                        </div>
                  </div>

                  <section class="todos-cards" style="display: none;">
                        @include('sistema.grafico.cards')
                  </section>

                  <section class="todos-relatorios" style="display: none;">
                        @include('sistema.grafico.relatorios')
                  </section>
            </section>
      </main>

      @section('scripts')
            <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

            <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
            <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
            <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

            <script>
                  $(document).ready(function() {
                        $("#range").daterangepicker({
                              "minDate": "02/08/2019",
                              "maxDate": new Date(),
                              "startDate": gerarData(new Date(), -7),
                              "endDate": gerarData(new Date(), 0),
                              "locale": {
                                    "format": "DD/MM/YYYY",
                                    "separator": " - ",
                                    "applyLabel": "Aplicar",
                                    "cancelLabel": "Cancelar",
                                    "daysOfWeek": [
                                        "Dom",
                                        "Seg",
                                        "Ter",
                                        "Qua",
                                        "Qui",
                                        "Sex",
                                        "Sáb"
                                    ],
                                    "monthNames": [
                                        "Janeiro",
                                        "Fevereiro",
                                        "Março",
                                        "Abril",
                                        "Maio",
                                        "Junho",
                                        "Julho",
                                        "Agosto",
                                        "Setembro",
                                        "Outubro",
                                        "Novembro",
                                        "Dezembro"
                                    ],
                                    "firstDay": 1
                              },
                              "opens": "left"
                        }, function(start, end) {
                              let filtro = Object.assign({}, query);

                              filtro['data.formatted'] = { 
                                    '$gte': start.format('YYYY-MM-DD'),
                                    '$lte': gerarData(end.format('YYYY-MM-DD'), 1)
                              };

                              $("main .area-conteudo-modulo .topo").css('display', 'none');
                              $(".todos-cards").css('display', 'none');
                              $(".todos-relatorios").css('display', 'none');
                              $(".area-conteudo-modulo .loader").css('display', 'block');

                              carregarLogins(filtro);
                        });

                        geral({!! $filtro !!});
                  });
            </script>

        @stop

@stop