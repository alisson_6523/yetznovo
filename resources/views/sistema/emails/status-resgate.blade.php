<html>
    <head>
        <title>E-mail Marketing - Entrega</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <!-- Save for Web Slices (aviso_de_pontos.psd) -->
        <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td width="600" height="136" valign="top">
                    <img src="https://www.yetzcards.com.br/img/mmkt-antigo/topo-mkt.jpg" alt="" style="display: block;">
                </td>
            </tr>
            <tr>
                <td width="600" height="162" valign="top">
                    <img src="https://www.yetzcards.com.br/img/mmkt-antigo/mudanca-status.jpg" alt="" style="display: block;">
                </td>
            </tr>
            <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif;" width="600" height="525" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" style="margin: 50px 0px;">
                        <tbody>
                            <tr>
                                <td width='600'>
                                    <table border="0" cellpadding="0" cellspacing="0" width="600">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p style="margin-bottom: 30px;font-size: 20px; text-align: center;"><strong>Olá {{ $resgate->user->apelido }}, algum(ns) item(ns) do seu pedido teve o status atualizado!</strong></p>
                                                    <p style="text-align: center;margin-bottom: 10px;"><strong style="color: #b10833;">Número do Resgate:</strong></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center"><img src="https://www.yetzcards.com.br/img/mmkt-antigo/seta_baixo_amarela.jpg" style="display:block;margin-bottom:10px"></td>                                                       
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellpadding="0" cellspacing="0" width="500" align="center" style="border: 1px solid #bebebe;">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <p style="background: #f3f3f3;color: #1c8e3f;font-size: 39px;line-height: 82px;width: 100%; text-align: center; margin-bottom: 25px;"><strong>{{ $resgate->registro }}</strong></p>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h2 style="font-size: 15px;color: #5b5b5f;margin-top: 40px;margin-bottom: 45px;text-align: center;width: 382px;">Produto(s) / Serviço(s) Resgatado(s): </h2>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:380px;">
                                                        <tbody>
                                                            @foreach($resgate->carrinho->itens as $item)
                                                                <tr>
                                                                    <td align="left">
                                                                        <img src="{{ $item->produto->link_foto }}" style="display:block; width: 157px;">
                                                                    </td>
                                                                    <td align="left">
                                                                        <h2 style="margin: 0;font-size: 14px;color: #801836;width: 160px;margin-bottom: 20px;">{{ $item->produto->nome_produto }}</h2>
                                                                        <h2 style="margin: 0;font-size: 12px;color: #801836;width: 160px;margin-bottom: 20px;">{{ "(". (isset($alteracoes[$item->cod_item]) ? $alteracoes[$item->cod_item] : $item->status_resgate->descricao_status) .")" }}</h2>
                                                                        <span style=" border-top: 4px solid #32b101;width: 123px;display: block;margin-bottom: 15px;"></span>
                                                                        <h1 style="margin: 0;font-size: 20px;color: #32b101;">{{ $item->produto->pontos_visual }} pts</h1>
                                                                    </td>
                                                                </tr>
                                                                <tr style="height: 60px;"></tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="background: #f3f3f3;">
                                                <td>
                                                    <h2 style="font-size: 16px;color: #5b5b5f;padding-top: 30px;padding-left: 60px;border-top: 1px solid #bebebe;">Local de Entrega</h2>
                                                </td>
                                            </tr>
                                            <tr style="background: #f3f3f3;">
                                                <td>
                                                    <p style="font-size: 16px;color: #5b5b5f;padding-left: 60px;">Empresa: {{ $resgate->user->pdv->cliente->nome_fantasia }} </p>
                                                    <p style="font-size: 16px;color: #5b5b5f;padding-left: 60px;">PDV: {{ $resgate->user->pdv->nome_pdv }}</p>
                                                    <p style="font-size: 16px;color: #5b5b5f;padding-left: 60px;">Rua: {{ $resgate->user->pdv->endereco }}</p>
                                                    <p style="font-size: 16px;color: #5b5b5f;padding-left: 60px;margin-bottom: 30px;">CEP: {{ $resgate->user->pdv->cep }} – {{ $resgate->user->pdv->cidade }} – {{ $resgate->user->pdv->estado }} </p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="text-align: center; margin-top: 50px; margin-bottom: 20px;">
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; margin: 0;">Esta é uma mensagem gerada automaticamente, portanto não pode ser respondida.</p>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; margin: 0;">Consulte em  nossos <a href="https://www.yetzcards.com.br/loja/termos-condicoes" target="_blank">Termos de Uso</a> ou <a href="https://www.yetzcards.com.br/loja/contato" target="_blank">Dúvidas Frequentes</a> para mais informações.</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td  width="600" height="113" >  
                    <img src="https://www.yetzcards.com.br/img/mmkt-antigo/rodape-mkt.jpg" alt="" style="display: block;">
                </td>
            </tr>
        </table>
        <!-- End Save for Web Slices -->
    </body>
</html>