<html>
<head>
    <title>E-mail Marketing - Resgate Finalizado</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (aviso_de_pontos.psd) -->
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="600" height="136" valign="top">
            <img src="{{ asset('img/resgate/topo-mkt.jpg') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td width="600" height="162" valign="top">
            <img src="{{ asset('img/resgate/img-resgate-finalizado.png') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family:Arial, tahoma, sans-serif;" width="600" height="525" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" style="margin: 50px 0px;">
                <tbody>
                <tr>
                    <td width="600">
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="margin-top: 30px; margin-bottom: 30px;font-size: 20px; text-align: center; font-family:Arial, tahoma, sans-serif;">Olá <strong> {{ $dados['usuario']->nome_usuario }} </strong>, recebemos seu pedido.</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p style="text-align: center; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;">Parabéns! Você acaba de resgatar um prêmio! Confira as informações do seu pedido: </p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="500" align="center" style="border: 1px solid #bebebe;">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="background: #f3f3f3;color: #1c8e3f;font-size: 39px;line-height: 82px;width: 100%; text-align: center; margin-bottom: 25px; font-family:Arial, tahoma, sans-serif; margin-top:0px;"><strong>{{ $dados['resgate']->cod_resgate }}</strong></p>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="font-size: 15px;color: #5b5b5f;margin-top: 40px;margin-bottom: 45px;text-align: center;width: 382px; font-family:Arial, tahoma, sans-serif;">Produto(s) / Serviço(s) Resgatado(s): </h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    @php
                                        $voucher = 0;
                                        $cartao  = 0;
                                    @endphp
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:380px;">
                                        <tbody>
                                        @foreach($dados['itens'] as $item)
                                            @if($item->first()->tipo == 1)
                                                @php
                                                    $cartao++;
                                                @endphp
                                            @else
                                                @php
                                                    $voucher++;
                                                @endphp
                                            @endif
                                            <tr>
                                                <td align="left">
                                                    <img width="157"!important src="{{ $item->first()->link_foto }}" style="display:block; width: 157px;">
                                                </td>
                                                <td align="left">
                                                    <h2 style="margin: 0;font-size: 14px;color: #801836;width: 160px;margin-bottom: 20px; font-family:Arial, tahoma, sans-serif; margin-top: 0px;">{{ $item->first()->nome_produto . (count($item) > 1 ? ' (' . count($item) . ' Cartões) - R$ ' . $item->first()->valor_reais : ' - R$ ' . $item->first()->valor_reais) }}</h2>
                                                    <span style=" border-top: 4px solid #32b101;width: 123px;display: block;margin-bottom: 15px; font-family:Arial, tahoma, sans-serif;"></span>
                                                    <h1 style="margin: 0;font-size: 20px;color: #32b101; font-family:Arial, tahoma, sans-serif;">{{ number_format(count($item) * $item->first()->valor, 2, ',', '.') }} YETZ</h1>
                                                </td>
                                            </tr>
                                            <tr style="height: 60px;"></tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px;">
                                    <h2 style="font-size: 16px;color: #5b5b5f;padding-top: 30px;border-top: 1px solid #bebebe; font-family:Arial, tahoma, sans-serif;">Atenção!</h2>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px; padding-top: 10px;">
                                    @if($cartao > 0)<div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Seu cartão físico será entregue em seu local de trabalho em até 30 dias.</div>@endif
                                    @if($voucher > 0)<div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Seu voucher digital será entregue em até 15 dias e estará disponível na área de VOUCHER DIGITAL da Plataforma.</div>@endif
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px;">
                                    <h2 style="font-size: 16px;color: #5b5b5f;padding-top: 30px;border-top: 1px solid #bebebe; font-family:Arial, tahoma, sans-serif;">Local de Entrega</h2>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px; padding-top: 10px;">
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Empresa: {{ $dados['usuario']->nome_usuario }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Área: {{ $dados['pdv']->nome_pdv }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">{{ $dados['pdv']->logradouro . ',' . $dados['pdv']->numero }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;">CEP: {{ $dados['pdv']->cep . ' – ' . $dados['pdv']->cidade . ' – ' . $dados['pdv']->estado }}</div>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px;">
                                    <h2 style="font-size: 16px;color: #5b5b5f;padding-top: 30px;border-top: 1px solid #bebebe; font-family:Arial, tahoma, sans-serif;">EM FUNÇÃO DAS FESTAS DE FINAL DE ANO, RESGATES DE CARTÕES FÍSICOS OU VOUCHERS DIGITAIS SOLICITADOS A PARTIR DE 28/11 SERÃO ENTREGUES EM DIFERENTES PRAZOS, CONFIRA:</h2>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px; padding-top: 10px;">
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">DE 28/11 a 19/12/2019 – ENTREGUE  13/01/2020</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">DE 20/12 a 06/01/2020 – ENTREGUE 03/02/2020</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;">A PARTIR DE 07/01/2020 – FLUXO NORMAL</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <p style="text-align: center; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif; margin-top: 0px;">Seu saldo após esse resgate é de: {{ number_format(round($dados['usuario']->saldo_usuario), 2, ',', '.') }} Pontos Yetz. </p>
        <p style="text-align: center; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;">Você pode acompanhar o andamento de seu pedido acessando menu "Acompanhar Resgates".</p>
    </tr>
    <tr>
        <td  width="600" height="113" >
            <img src="{{ asset('img/resgate/rodape-mkt.jpg') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td>
            <p style="text-align: center;margin:30px 0px; font-family:Arial, tahoma, sans-serif;">NÃO RESPONDER ESTE E-MAIL.</p>
        </td>
    </tr>
</table>
</body>
</html>
