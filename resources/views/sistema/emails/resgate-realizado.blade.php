<html>
<head>
    <title>E-mail Marketing - Resgate Finalizado</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<!-- Save for Web Slices (aviso_de_pontos.psd) -->
<table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="600" height="136" valign="top">
            <img src="{{ asset('img/resgate/topo-mkt.jpg') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td width="600" height="162" valign="top">
            <img src="{{ asset('img/resgate/img-resgate-finalizado.png') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td align="center" style="font-family:Arial, tahoma, sans-serif;" width="600" height="525" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" style="margin: 50px 0px;">
                <tbody>
                <tr>
                    <td width="600">
                        <table border="0" cellpadding="0" cellspacing="0" width="600">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="margin-top: 30px; margin-bottom: 30px;font-size: 20px; text-align: center; font-family:Arial, tahoma, sans-serif;">Olá <strong>Administrador</strong>, um novo resgate foi realizado no site YETZCARDS.</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="font-size: 15px;color: #5b5b5f;margin-top: 40px;margin-bottom: 45px;text-align: center; font-family:Arial, tahoma, sans-serif;">{{ $dados['usuario']->nome_usuario }}</h2>
{{--                                    <p style="text-align: center; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;"><a href="https://www.yetzcards.com.br/administrador/gerencia-resgates.php">CLIQUE AQUI</a> para acessar o gerenciador de resgates.</p>--}}
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" width="500" align="center" style="border: 1px solid #bebebe;">
                            <tbody>
                            <tr>
                                <td>
                                    <p style="background: #f3f3f3;color: #1c8e3f;font-size: 39px;line-height: 82px;width: 100%; text-align: center; margin-bottom: 25px; font-family:Arial, tahoma, sans-serif; margin-top:0px;"><strong>{{ $dados['resgate']->cod_resgate }}</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2 style="font-size: 15px;color: #5b5b5f;margin-top: 40px;margin-bottom: 45px;text-align: center;width: 382px; font-family:Arial, tahoma, sans-serif;">Produto(s) / Serviço(s) Resgatado(s): </h2>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:380px;">
                                        <tbody>
                                        @foreach($dados['itens'] as $item)
                                            <tr>
                                                <td align="left">
                                                    <img width="157"!important src="{{ $item->first()->link_foto }}" style="display:block; width: 157px;">
                                                </td>
                                                <td align="left">
                                                    <h2 style="margin: 0;font-size: 14px;color: #801836;width: 160px;margin-bottom: 20px; font-family:Arial, tahoma, sans-serif; margin-top: 0px;">{{ $item->first()->nome_produto . (count($item) > 1 ? ' (' . count($item) . ' Cartões) - R$ ' . $item->first()->valor_reais : ' - R$ ' . $item->first()->valor_reais) }}</h2>
                                                    <span style=" border-top: 4px solid #32b101;width: 123px;display: block;margin-bottom: 15px; font-family:Arial, tahoma, sans-serif;"></span>
                                                    <h1 style="margin: 0;font-size: 20px;color: #32b101; font-family:Arial, tahoma, sans-serif;">{{ count($item) * $item->first()->valor_produto }} YETZ</h1>
                                                </td>
                                            </tr>
                                            <tr style="height: 60px;"></tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px;">
                                    <h2 style="font-size: 16px;color: #5b5b5f;padding-top: 30px;border-top: 1px solid #bebebe; font-family:Arial, tahoma, sans-serif;">Local de Entrega</h2>
                                </td>
                            </tr>
                            <tr style="background: #f3f3f3;">
                                <td style="padding-left: 60px; padding-top: 10px;">
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Empresa: {{ $dados['usuario']->nome_usuario }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">Área: {{ $dados['pdv']->nome_pdv }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; font-family:Arial, tahoma, sans-serif;">{{ $dados['pdv']->logradouro . ',' . $dados['pdv']->numero }}</div>
                                    <div style="padding-bottom: 7px; font-size: 16px;color: #5b5b5f; margin-bottom: 30px; font-family:Arial, tahoma, sans-serif;">CEP: {{ $dados['pdv']->cep . ' – ' . $dados['pdv']->cidade . ' – ' . $dados['pdv']->estado }}</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td  width="600" height="113" >
            <img src="{{ asset('img/resgate/rodape-mkt.jpg') }}" alt="" style="display: block;">
        </td>
    </tr>
    <tr>
        <td>
            <p style="text-align: center;margin:30px 0px; font-family:Arial, tahoma, sans-serif;">NÃO RESPONDER ESTE E-MAIL.</p>
        </td>
    </tr>
</table>
</body>
</html>