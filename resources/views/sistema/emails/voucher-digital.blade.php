<html>
<head>
    <meta charset="utf-8">
</head>

<body>
<table style="background-color: #F7A81E" width="750px" border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>
    <tr>
        <td>
            <img style='display:block;' src="https://s3-sa-east-1.amazonaws.com/yetzcards/email-mkt/topo-logo.png" width="238" height="auto" alt="">
        </td>
        <td style="padding: 10 20 10 20;" width="512" height="auto">
            <p style="font-size: 14pt; color: #FFFFFF; text-align: right; font-family: sans-serif;">VOUCHER DIGITAL</p>
        </td>
    </tr>
</table>

<table style="font-family: sans-serif; padding: 30px 80px 10px 80px; background-color: #f7f7f7;" width="750px" border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>
    <tr>
        <td style="text-align: center">
            <p><font style="font-size: 16pt; color: #FF6F00; font-weight: bold">Resgate de Voucher Digital</font><br><font style="color:#848484; font-size: 10pt; line-height: 20pt">Resgate realizado com Sucesso!</font></p>
            <img style='display:block; margin-left: auto; margin-right: auto; margin-top: 10px' src="https://s3-sa-east-1.amazonaws.com/yetzcards/email-mkt/seta.png" width="20" height="10" alt="">
        </td>
    </tr>
</table>

<table style="font-family: sans-serif; padding: 20px 30px 30px 30px; background-color: #f7f7f7;" width="750px" border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>
    <tr>
        <td style="text-align: center">
            <p><font style="font-size: 16pt; color: #FF6F00; font-weight: bold">Parabéns!</font><br><font style="color:#848484; font-size: 10pt; line-height: 15pt">Você acaba de receber o voucher digital solicitado dentre as<br>inúmeras opções de prêmios da Plataforma YETZ CARDS. </font></p>
        </td>
    </tr>
</table>

<!--CARD-->

<table style="background-color: #f7f7f7; font-family: sans-serif; padding: 10px 54px 10px 54px;" width="750px" border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>
    <tr>
        <td style=" vertical-align: top; text-align: left; padding:40px 20px 40px 40px; background-color: #f7f7f7;border-top: 1px solid white; border-bottom: 1px solid white; border-left: 1px solid white;border-color:#DCDCDC;" width="321px" colspan="1">
            <img style='display:block; margin-left: auto; margin-right: auto;' src="https://s3-sa-east-1.amazonaws.com/yetzcards/email-mkt/{{ $dados['item']->produto->foto_exemplo }}" width="271" height="auto" alt="">
        </td>
        <td style="text-align: left; background-color: #f7f7f7;border-right: 1px solid white; border-top: 1px solid white; border-bottom: 1px solid white;border-color:#DCDCDC; vertical-align: middle;" width="321px" colspan="2">
            <p style="font-size: 18pt; color:#111111"><strong>{{ $dados['item']->produto->nome_produto }}</strong><br></p>
            <a style="text-decoration: none;" href="{{ $dados['item']->link_voucher }}" target="_blank"><img style='display:block;' src="https://s3-sa-east-1.amazonaws.com/yetzcards/email-mkt/botao-voucher.png" width="230" height="auto" alt=""></a>
        </td>
    </tr>
</table>



<!--RODAPÉ-->


<table style="background-color: #f7f7f7; font-family: sans-serif; padding: 50px 0px 10px 0px;" width="750px" border='0' align='center' cellpadding='0' cellspacing='0' bgcolor='#FFFFFF'>
    <tr>
        <td style=" vertical-align: top; text-align: center; padding:40px; background-color: #f7f7f7;border-top: 1px solid white; border-color:#DCDCDC;">
            <img style='display:block; margin-left: auto; margin-right: auto; padding-bottom: 10px' src="https://s3-sa-east-1.amazonaws.com/yetzcards/email-mkt/yetz.png" width="130" height="auto" alt="">

            <p><font style="color:#848484; font-size: 9pt; line-height: 15pt">Esta é uma mensagem gerada automaticamente, portanto não poderá ser respondida.</font></p>
        </td>
    </tr>
</table>
</body>
</html>