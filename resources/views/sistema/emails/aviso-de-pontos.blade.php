<html>
    <head>
        <title>E-mail Marketing - Aviso de Pontos</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
        <table bgcolor="#FFFFFF" border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
                <td width="600" height="136" valign="top">
                    <img src="https://yetzcards.com.br/img/mmkt-antigo/topo-mkt.jpg" alt="" style="display: block;">
                </td>
            </tr>
            <tr>
                <td width="600" height="250" valign="top">
                    <img src="https://yetzcards.com.br/img/mmkt-antigo/aviso_pontos.jpg" alt="" style="display: block;">
                </td>
            </tr>
            <tr>
                <td align="center" style="font-family:Arial, Helvetica, sans-serif;" width="600" height="550" valign="top">
                    <table border="0" cellpadding="0" cellspacing="0" style="margin-top: 60px;">
                        <tbody>
                        <tr>
                            <td width='600'>
                                <table border="0" cellpadding="0" cellspacing="0" width="600">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <p style="margin-bottom: 30px;text-align: center;"><strong>Olá {{ $dados['usuario']->nome_usuario }}, prepare-se!</strong></p>
                                            <p style="text-align: center;margin-bottom: 20px;">Seus Pontos YETZ já estão disponíveis para resgate<br>de vários prêmios!!! </p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <table border="0" cellpadding="0" cellspacing="0" width="588" align="center">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <h1 style="text-align: center;font-size: 24px;color: #b10833;margin: 20px 0px 10px 0px;">SEU SALDO ATUAL</h1>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center"><img src="https://yetzcards.com.br/img/mmkt-antigo/seta_baixo_amarela.jpg" style="display:block;margin-bottom:30px"></td>
                                    </tr>
                                    <tr align="center">
                                        <td>
                                            <h1 style="text-align: center;background: #f3f3f3;font-size: 57px;color: #1c8e3f;padding: 20px;border: 1px solid #bebebe; width: 472px;">{{ $dados['usuario']->saldo_usuario }} YETZ</h1>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <p style="text-align: center;">Você pode escolher o que resgatar entre diversas categorias,<br/> como Alimentação, Eletroeletrônicos, Brinquedos e Games,<br/> Viagens e muito mais...</p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align='center'>
                                            <img src="https://yetzcards.com.br/img/mmkt-antigo/img_categorias.jpg" style="margin: 30px 0px;">
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <div style="margin-top: 50px; margin-bottom: 20px; text-align: center;">
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; margin: 0;">Esta é uma mensagem gerada automaticamente, portanto não pode ser respondida.</p>
                        <p style="font-family:Arial, Helvetica, sans-serif; font-size:12px; margin: 0;">Consulte em  nossos <a href="https://www.yetzcards.com.br/loja/termos-condicoes" target="_blank">Termos de Uso</a> ou <a href="https://www.yetzcards.com.br/loja/contato" target="_blank">Dúvidas Frequentes</a> para mais informações.</p>
                    </div>
                </td>
            </tr>
            <tr>
                <td  width="600" height="113" >
                    <img src="https://yetzcards.com.br/img/mmkt-antigo/rodape-mkt.jpg" alt="" style="display: block;">
                </td>
            </tr>
        </table>
    </body>
</html>
