@extends('sistema.layout.admin-layout')

@section('titulo', 'Editar Banner')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-editar-banner">
            <input type="hidden" id="cod-banner" value="{{ $banner->cod_banner }}">
            {{ csrf_field() }}
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/plataforma">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>EDITAR BANNER</h2>
                </div>

                <div class="form-group">
                    <label for="">Título *</label>
                    <input type="text" name="titulo_banner" id="titulo" value="{{ $banner->titulo }}">
                </div>

                <div class="form-group">
                    <label for="">Data de Entrada *</label>
                    <input type="text" mask="date" name="entrada_banner" maxlength="10" id="dt-entrada" value="{{ date('d/m/Y', strtotime($banner->data_entrada)) }}">
                </div>
                <div class="form-group">
                    <label for="">Data de Saída</label>
                    <input type="text" mask="date" name="saida_banner" maxlength="10" id="dt-saida" value="{{ !empty($banner->data_saida) ? date('d/m/Y', strtotime($banner->data_saida)) : '' }}">
                </div>

                <div class="form-group">
                    <label for="" class="label-enviar-logo">Escolha a Imagem *</label>
                    <div class="box">
                        <input type="file" name="foto" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"
                               accept="image/x-png,image/gif,image/jpeg" />
                        <label for="file-7"><span>{{ $banner->arquivo }}</span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                    <small>O tamanho da imagem deve ser de 1920x440px.</small><br/>
                    <a href="{{ $banner->link }}" target="_blank">
                        Ver Imagem
                    </a>
                </div>
                @if(empty(\Auth::user()->cliente_id))
                    <div class="form-group">
                        <div class="check check-banner-geral {{ !empty($banner->cod_campanha) ? : 'checked' }}">
                            <div class="square"></div>
                            <span>Banner Geral</span>
                        </div>
                    </div>
                @endif
                <div class="form-group" id="cadastro-banner-div-cliente" style="{{ !empty($banner->cod_campanha) ? : 'display: none;' }}">
                    <label for="">Cliente *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="cadastro-banner-cliente" value="{{ !empty($banner->campanha) ? $banner->campanha->cliente->id : '' }}">
                        <a href="" class="item-selected">
                            <span>{{ !empty($banner->campanha) ? $banner->campanha->cliente->nome : 'Selecione' }}</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                @if(!empty(\Auth::user()->cliente_id))
                                    <li><a href="" id="cadastro-banner-option-cliente" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                                @else
                                    @foreach($clientes as $cliente)
                                        <li><a href="" id="cadastro-banner-option-cliente" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="cadastro-banner-div-campanha" style="{{ !empty($banner->cod_campanha) ? : 'display: none;' }}">
                    <label for="">Campanha *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_campanha" id="cadastro-banner-campanha" value="{{ !empty($banner->campanha) ? $banner->cod_campanha : '' }}">
                        <a href="" class="item-selected">
                            <span>{{ !empty($banner->campanha) ? $banner->campanha->nome_campanha : 'Selecione' }}</span>
                        </a>
                        <div class="dropdown">
                            <ul id="cadastro-banner-select-campanha">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar o banner.
                    </p>
                    <button type="submit">atualizar<i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR BANNER', 'link' => '/sistema/plataforma'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'BANNER ATUALIZADO COM SUCESSO!', 'link' => '/sistema/plataforma'])
@stop
