@extends('sistema.layout.admin-layout')

@section('titulo', 'Editar Regulamento Geral')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-editar-regulamento-geral">
            <input type="hidden" id="id-campanha" value="{{ $campanha->cod_campanha }}">
            {{ csrf_field() }}
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>REGULAMENTO GERAL DA CAMPANHA {{ $campanha->nome_campanha }}</h2>
                </div>

                <div class="form-group">
                    <label for="">Título *</label>
                    <input type="text" name="titulo_regulamento" id="titulo" value="{{ $campanha->titulo_regulamento }}">
                </div>

                <div class="form-group">
                    <label for="" class="label-enviar-logo">Escolha o Arquivo *</label>
                    <div class="box">
                        <input type="file" name="foto" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"/>
                        <label for="file-7"><span>{{ $campanha->regulamento }}</span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                    <div>
                        <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}/baixar-regulamento" target="_blank">Baixar regulamento: {{ $campanha->regulamento }}</a>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para atualizar o regulamento.
                    </p>
                    <button type="submit">atualizar<i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR REGULAMENTO', 'link' => '/sistema/campanhas/' . $campanha->cod_campanha . '/' . limpaString($campanha->nome_campanha)])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'REGULAMENTO ATUALIZADO COM SUCESSO!', 'link' => '/sistema/campanhas/' . $campanha->cod_campanha . '/' . limpaString($campanha->nome_campanha)])
@stop
