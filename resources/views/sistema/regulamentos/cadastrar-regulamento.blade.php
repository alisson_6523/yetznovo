@extends('sistema.layout.admin-layout')

@section('titulo', 'Cadastrar Regulamento')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-cadastrar-regulamento">
            <input type="hidden" id="cod-campanha" value="{{ $campanha->cod_campanha }}">
            <input type="hidden" id="ilha" value="{{ $ilha }}">
            {{ csrf_field() }}
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>REGULAMENTO DA ILHA {{ preg_replace("/_/", " ", $ilha) }}</h2>
                </div>

                <div class="form-group">
                    <label for="">Título *</label>
                    <input type="text" name="titulo_regulamento" id="titulo">
                </div>

                <div class="form-group">
                    <label for="" class="label-enviar-logo">Escolha o Arquivo *</label>
                    <div class="box">
                        <input type="file" name="foto" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"/>
                        <label for="file-7"><span></span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar o regulamento.
                    </p>
                    <button type="submit">finalizar regulamento <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DE REGULAMENTO', 'link' => '/sistema/campanhas/' . $campanha->cod_campanha . '/' . limpaString($campanha->nome_campanha)])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE REGULAMENTO <br/>REALIZADO COM SUCESSO!', 'link' => '/sistema/campanhas/' . $campanha->cod_campanha . '/' . limpaString($campanha->nome_campanha)])
@stop
