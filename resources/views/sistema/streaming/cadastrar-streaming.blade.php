@extends('sistema.layout.admin-layout')

@section('titulo', 'Cadastrar Streaming')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-cadastro-streaming">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/streaming">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>CADASTRAR STREAMING</h2>
                </div>
                <div class="form-group">
                    <label for="">Titulo *</label>
                    <input type="text" name="titulo" id="cadastro-streaming-titulo">
                </div>
                <div class="form-group">
                    <label for="">Descrição *</label>
                    <textarea name="descricao" id="cadastro-streaming-descricao"></textarea>
                </div>
                <div class="data">
                    <div class="form-group">
                        <label for="">Data de Disparo *</label>
                        <input type="text" name="dt_disparo" mask="date" id="cadastro-streaming-dt-disparo">
                    </div>
                    <div class="form-group">
                        <label for="">Data de Término *</label>
                        <input type="text" name="dt_termino" mask="date" id="cadastro-streaming-dt-termino">
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Mídia *</label>
                    <div class="box">
                        <input type="file" name="file-7[]" id="cadastro-streaming-midia" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"
                               accept="audio/* video/*"/>
                        <label for="cadastro-streaming-midia"><span></span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                    <small>Somente arquivos .mp3 e .mp4 abaixo de 400Mb.</small>
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Foto de Capa *</label>
                    <div class="box">
                        <input type="file" name="file-7[]" id="cadastro-streaming-capa" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="cadastro-streaming-capa"><span></span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Cliente *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="cadastro-streaming-cliente">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                @if(!empty(\Auth::user()->cliente_id))
                                    <li><a href="" id="cadastro-streaming-option-cliente" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                                @else
                                    @foreach($clientes as $cliente)
                                        <li><a href="" id="cadastro-streaming-option-cliente" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Campanha *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_campanha" id="cadastro-streaming-campanha">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul id="cadastro-streaming-select-campanha">

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">PDVs / Ilhas *</label>
                    <div class="box-ilhas" id="cadastro-streaming-pdv-ilhas">
                        Selecione um Cliente e uma Campanha.
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar este streaming.
                    </p>
                    <button type="submit">finalizar streaming <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DA STREAMING', 'link' => '/sistema/streaming/adicionar'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE STREAMING REALIZADO <br> COM SUCESSO!', 'link' => '/sistema/streaming'])
@stop

