@extends('sistema.layout.admin-layout')

@section('titulo', 'Streaming')

@section('conteudo')

    <main>
        @include('sistema.includes.menu-admin')

        <section class="area-conteudo-modulo">
            <div class="topo">
                <h2 class="title">
                    <i class="icone-arrow-right"></i>
                    STREAMING
                </h2>
                <div class="filtro">
                    <div class="form-group">
                        <label for="">Cliente</label>
                        <div class="select-custom">
                            <input type="hidden" name="cod_cliente" id="filtro-streaming-cliente">
                            <a href="" class="item-selected">
                                <span>Selecione</span>
                            </a>
                            <div class="dropdown">
                                <ul>
                                    <li><a href="" cod-opcao="">Todos</a></li>
                                    @if(!empty(\Auth::user()->cliente_id))
                                        <li><a href="" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                                    @else
                                        @foreach($clientes as $cliente)
                                            <li><a href="" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">A Partir De</label>
                        <input type="text" mask="date" id="filtro-streaming-dt-inicio">
                    </div>
                    <div class="form-group">
                        <label for="">Até</label>
                        <input type="text" mask="date" id="filtro-streaming-dt-fim">
                    </div>
                    <div class="form-group">
                        <input type="text" id="filtro-streaming-busca">
                        <button type="button">
                            <i class="icone-lupa"></i>
                        </button>
                    </div>
                </div>
                <div class="acoes">
                    <a href="/sistema/streaming/adicionar" class="btn">
                        adicionar streaming <i class="icone-arrow-right"></i>
                    </a>
                </div>
            </div>
            <div class="todos-midias">
                @foreach ($videos as $v)
                @php
                    $v["pdvs"] = $v->vinculo()->with('pdv')->get()->pluck('pdv')->pluck('nome_pdv')->implode(', ');
                    $v["tipo"] = $v->tipo;
                @endphp
                <div class="area-box">
                    <div class="box-midia">
                        <div class="info-midia">
                            <button type="button">
                                <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                            </button>
                            <div class="dados">
                                <h3>{{ $v->titulo }}</h3>
                                <div class="info">
                                    <div class="thumb">
                                        <div class="img">
                                            <img src="{{ $v->foto_capa }}" alt="">
                                        </div>
                                        <div class="tipo">
                                            <img src="{{ asset('img/icone-midia.svg') }}" alt="">
                                            <i class="seta icone-arrow-right"></i>
                                            <span>{{ $v->tipo }}</span>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Cliente: {{ !empty($v->cod_cliente) ? $v->cliente->nome : 'Todos'}}</li>
                                        <li>Campanha: {{ !empty($v->cod_campanha) ? $v->campanha->nome_campanha : 'Todas'}}</li>
                                        <li>PDV: {{ !empty($v->pdvs) ? $v->pdvs : 'Todos' }}</li>
                                        <li>D.E: - {{ date('d/m/Y', strtotime($v->data_entrada)) }}</li>
                                        <li>D.F: - {{ date('d/m/Y', strtotime($v->data_saida)) }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="area-desc">
                            <div class="topo-desc" onclick="toggleInfoStreaming(this)">
                                <h3>Descrição</h3>
                                <i class="seta icone-arrow-down"></i>
                            </div>
                            <div class="info-desc">
                                <p>
                                    {{ $v->descricao }}
                                </p>
                                <div class="arquivo">
                                    <span>Arquivo</span>
                                    <a href="{{ $v->link }}">{{ $v->link_video }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </section>
    </main>
    <script>
        var videos = @json($videos);
    </script>

@stop
