@extends('sistema.layout.admin-layout')

@section('titulo', 'Editar Popup')

@section('conteudo')

    <div class="area-cadastro">
        <form id="form-editar-popup">
            <input type="hidden" id="cod-popup" value="{{ $popup->cod_popup }}">
            {{ csrf_field() }}
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/popups">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>EDITAR POPUP</h2>
                </div>

                <div class="form-group">
                    <label for="">Título *</label>
                    <input type="text" name="titulo_popup" id="titulo" value="{{ $popup->titulo_popup }}">
                </div>

                <div class="form-group">
                    <label for="">Data de Entrada</label>
                    <input type="text" mask="date" name="entrada_popup" maxlength="10" id="dt-entrada" value="{{ date('d/m/Y', strtotime($popup->entrada_popup)) }}">
                </div>
                <div class="form-group">
                    <label for="">Data de Saída</label>
                    <input type="text" mask="date" name="saida_popup" maxlength="10" id="dt-saida" value="{{ date('d/m/Y', strtotime($popup->saida_popup)) }}">
                </div>

                <div class="form-group">
                    <label for="" class="label-enviar-logo">Escolha a Imagem</label>
                    <div class="box">
                        <input type="file" name="foto" id="file-7" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected"
                               accept="image/x-png,image/gif,image/jpeg" />
                        <label for="file-7"><span>{{ $popup->foto_popup }}</span> <strong>Selecionar o arquivo</strong></label>
                    </div>
                    <a href="{{ $popup->link_foto_popup }}" target="_blank">
                        Ver Imagem
                    </a>
                </div>
                <div class="form-group">
                    <label for="">Cliente *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="cadastro-popup-cliente" value="{{ $popup->cod_cliente }}">
                        <a href="" class="item-selected">
                            <span>{{ $popup->cliente->nome_fantasia }}</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                @if(!empty(\Auth::user()->cliente_id))
                                    <li><a href="" id="cadastro-popup-option-cliente" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                                @else
                                    @foreach($clientes as $cliente)
                                        <li><a href="" id="cadastro-popup-option-cliente" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Campanha *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_campanha" id="cadastro-popup-campanha" value="{{ $popup->cod_campanha }}">
                        <a href="" class="item-selected">
                            <span>{{ !empty($popup->campanha->nome_campanha) ? $popup->campanha->nome_campanha : 'Selecione' }}</span>
                        </a>
                        <div class="dropdown">
                            <ul id="cadastro-popup-select-campanha">

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">PDVs / Ilhas *</label>
                    <div class="box-ilhas" id="cadastro-popup-pdv-ilhas">
                        Selecione um Cliente e uma Campanha.
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar o popup.
                    </p>
                    <button type="submit">atualizar <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>

    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR POPUP', 'link' => '/sistema/popups/adicionar'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'POPUP ATUALIZADA COM SUCESSO!', 'link' => '/sistema/popups'])
@stop
