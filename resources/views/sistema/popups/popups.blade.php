@extends('sistema.layout.admin-layout')

@section('titulo', 'Popups')

@section('conteudo')

<main>
      @include('sistema.includes.menu-admin')

      <section class="s-area-portal" id="rota-popups" >
            <div class="topo">
                  <ul class="nav-tabs">

                        <a style="cursor: pointer; margin-right: 130px;" onclick="cadastroPopups.limpar()"
                              class="btn-limpar-popups" title="Limpar Popups">
                              Limpar Popups
                        </a>
                  </ul>

                  <a href="/sistema/popups/adicionar" class="btn" title="Adicionar Popup">
                        <img src="{{ asset('img/plus.svg') }}" alt="">
                  </a>
            </div>
            <div class="tab-pane" id="banners">
                  <div class="todos-banners">
                        @foreach ($popups as $p)
                        <div class="box-banner">
                              <button type="button" class="">
                                    <img src="{{ asset('img/icons/bar-cinza.svg') }}" alt="">
                                    <div class="dropdown">
                                          <ul>
                                                <li>
                                                      <a href="/sistema/popups/editar/{{ $p->cod_popup }}/{{ !empty($p->titulo_popup) ? limpaString($p->titulo_popup) : 'sem-titulo' }}">
                                                            <i class="icone-lapis"></i>
                                                            <span>editar</span>
                                                      </a>
                                                </li>

{{--                                                <li>--}}
{{--                                                      <a onclick="" class="btn-excluir" style="cursor:pointer;">--}}
{{--                                                            <img src="{{ asset('img/icons-adm/excluir.svg') }}" alt="">--}}
{{--                                                            <span>desativar</span>--}}
{{--                                                      </a>--}}
{{--                                                </li>--}}

{{--                                                <li>--}}
{{--                                                      <a onclick="" class="btn-excluir" style="cursor:pointer;">--}}
{{--                                                            <i class="icone-lapis"></i>--}}
{{--                                                            <span>ativar</span>--}}
{{--                                                      </a>--}}
{{--                                                </li>--}}
                                          </ul>
                                    </div>
                              </button>
                              <div class="info">
                                    <div class="thumb">
                                          <img src="{{ $p->link_foto_popup }}" alt="">
                                    </div>
                                    <ul>
                                          {{-- <li><span>D.E- {{ $b->entrada }}</span></li>
                                          <li><span>D.S- {{ $b->saida }}</span></li>
                                          <li><span>{{ $b->link_banner }}</span></li> --}}
                                          <li status="ativo"><span>Ativo</span></li>
                                    </ul>
                              </div>
                        </div>

                        @endforeach
                  </div>
            </div>
      </section>
</main>

@stop
