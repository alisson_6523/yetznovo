@foreach ($resgates as $item)
    <div class="item-table item-resgate expand-item">
        <ul class="dados">
            <li>
                <div class="check-square" data-cod_resgate={{ $item->cod_item }}></div>
            </li>
            <li>
                <span>{{ preg_replace("/_/", " ", $item->carrinho->resgates->first()->user->nome_usuario) }}</span>
            </li>
            <li>
                <span>{{ $item->carrinho->resgates->first()->user->login }}</span>
            </li>
            <li>
                <span>{{ $item->carrinho->resgates->first()->data_resgate }}</span>
            </li>
            <li>
                <span>{{ $item->produto->nome_produto }} - R${{ $item->produto->valor_reais }}</span>
            </li>
            <li>
                <span>{{ $item->carrinho->resgates->first()->campanha->nome_campanha }}</span>
            </li>
            <li>
                <span>{{ $item->carrinho->resgates->first()->user->pdv->nome_pdv }}</span>
            </li>
            <li>
                <span>{{ $item->status_resgate->descricao_status }}</span>
            </li>
            <li>
                <div class="pts">
                    <i class="icone-circulo-estrela"></i>
                    <i class="seta icone-arrow-right"></i>
                    <span>{{ $item->produto->valor_produto }}</span>
                </div>
            </li>
            <i class="icone-arrow-down"></i>
        </ul>
        <div class="dados-gerais">

            <div class="item-dados">
                <div class="title">
                    <i class="icone-user"></i>
                    <span>inf. do usuários</span>
                </div>
                <ul>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>{{ $item->carrinho->resgates->first()->user->nome_usuario }}</span>
                    </li>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>{{ $item->carrinho->resgates->first()->user->celular }}</span>
                    </li>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>{{ $item->carrinho->resgates->first()->user->email }}</span>
                    </li>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>Login - {{ $item->carrinho->resgates->first()->user->login }}</span>
                    </li>
                </ul>
            </div>

            <div class="item-dados">
                <div class="title">
                    <i class="icone-resgate"></i>
                    <span>INF. D0 RESGATE</span>
                </div>
                <ul>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>Nº: {{ $item->carrinho->resgates->first()->registro }}</span>
                    </li>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>{{ $item->produto->nome_produto }} - R${{ $item->produto->valor_reais }}</span>
                    </li>
                    <li>
                        <i class="icone-arrow-right"></i>
                        <span>{{ $item->status_resgate->descricao_status }}</span>
                    </li>
                </ul>
            </div>

            <div class="item-dados">
                <div class="title">
                    <i class="icone-resgate"></i>
                    <span>LINK DO VOUCHER</span>
                </div>
                <ul>
                    <li>
                        <div class="form-group">
                            <input type="text" {{ $item->cod_status >= 4 ? 'disabled' : '' }} value="{{ !empty($item->link_voucher) ? $item->link_voucher : '' }}">
                        </div>
                        @if($item->cod_status < 4)
                            <a>
                                <button class="btn-enviar btn-voucher-{{ !empty($item->link_voucher) ? 'editar' : 'salvar' }}-link btn-salvar-voucher" data-resgate="{{ $item->cod_item }}">{{ !empty($item->link_voucher) ? 'Editar' : 'Salvar' }}</button>
                            </a>
                        @endif
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endforeach
