@extends('sistema.layout.admin-layout')

@section('titulo', 'Campanhas')

@section('conteudo')

    <main>
        @include('sistema.includes.menu-admin')

        <section class="area-conteudo-modulo">
            <div class="topo">
                <h2 class="title">
                    <i class="icone-arrow-right"></i>
                    CAMPANHAS
                </h2>
                <div class="acoes">
                    <div class="pesquisa">
                        @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                            <input id="campanha-busca" type="text"/>
                            <button type="button"><i class="icone-lupa"></i></button>
                        @endif
                    </div>
                    @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                        <a href="/sistema/campanhas/adicionar" class="btn">
                            adicionar campanha <i class="icone-arrow-right"></i>
                        </a>
                    @endif
                </div>
            </div>
            <div class="area-todos-clientes">
                @foreach($campanhas as $campanha)
                    <a href="{{ \Auth::user()->perfil_id != 3 ? '/sistema/campanhas/' . $campanha->cod_campanha . '/' . limpaString($campanha->nome_campanha) : '' }}" class="box-cliente box-campanha">
                        <div class="info">
                            <h3>{{ $campanha->nome_campanha }}</h3>
                            <ul>
                                <li>
                                    <div class="square"></div>
                                    <span>{{ $campanha->cliente->nome_fantasia != null ? $campanha->cliente->nome_fantasia : $campanha->cliente->razao_social }}</span>
                                </li>
                                <li>
                                    <div class="square"></div>
                                    <span>Início: {{ date('d/m/Y', strtotime($campanha->dt_inicio)) }}</span>
                                </li>
                                <li>
                                    <div class="square"></div>
                                    <span>Fim: {{ date('d/m/Y', strtotime($campanha->dt_fim)) }}</span>
                                </li>
                                <li>
                                    <div class="square {{ $campanha->trashed() ? 'yellow' : 'green' }}"></div>
                                    <span>{{ $campanha->trashed() ? 'Inativa' : 'Ativa' }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="geral">
                            <div class="item">
                                <i class="icone icone-user"></i>
                                <i class="icone-arrow-down"></i>
                                <span>{{ $campanha->usuarios_campanha()->withTrashed()->count() }}</span>
                            </div>
                            <div class="item">
                                <i class="icone icone-circulo-estrela"></i>
                                <i class="icone-arrow-down"></i>
                                <span>{{ number_format($campanha->campanha_protocolos->sum('total_pontos'), 0, '', '.') }}</span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </section>
    </main>

@stop
