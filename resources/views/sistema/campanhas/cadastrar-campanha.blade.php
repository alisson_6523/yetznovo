@extends('sistema.layout.admin-layout')

@section('titulo', 'Adicionar Campanha')

@section('conteudo')

    <div class="area-cadastro">
        <form id="cadastro-campanha">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/campanhas">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>cadastrar nova campanha</h2>
                </div>
                <div class="form-group">
                    <label for="">Cliente *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="campanha-cliente-id">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                @if(!empty(\Auth::user()->cliente_id))
                                    <li><a href="" class="option-cod-clinte" cod-opcao="{{ \Auth::user()->cliente_id }}">{{ \Auth::user()->cliente->nome }}</a></li>
                                @else
                                    @foreach($clientes as $cliente)
                                        <li><a href="" class="option-cod-clinte" cod-opcao="{{ $cliente->id }}">{{ $cliente->nome }}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">PDVs *</label>
                    <div id="campanha-pdvs" class="todos-pdvs">
                        <small>Selecione um Cliente</small>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Nome da Campanha *</label>
                    <input type="text" id="campanha-nome" name="campanha_nome" required>
                </div>
                <div class="form-group">
                    <label for="">Tipo de Campanha *</label>
                    <div class="select-custom">
                        <input type="hidden" name="campanha_tipo" id="campanha-tipo">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="0">Yetz Cards</a></li>
                                <li><a href="" cod-opcao="1">Yetz Super</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Chave da Campanha *</label>
                    <input type="text" id="campanha-chave" name="campanha_chave" required oninput="this.value = this.value.replace(/[^a-z0-9_]/gi, '').toUpperCase();">
                </div>
                <div class="data">
                    <div class="form-group">
                        <label for="">Data de Início *</label>
                        <input type="text" mask="date" name="campanha_inicio" id="campanha-inicio" required>
                    </div>
                    <div class="form-group">
                        <label for="">Data Final *</label>
                        <input type="text" mask="date" name="campanha_fim" id="campanha-fim" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Validade dos Pontos *</label>
                    <div class="select-custom">
                        <input type="hidden" name="validade_pontos" id="validade-pontos">
                        <a href="" class="item-selected">
                            <span>Selecione</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="30">30 dias</a></li>
                                <li><a href="" cod-opcao="60">60 dias</a></li>
                                <li><a href="" cod-opcao="90">90 dias</a></li>
                                <li><a href="" cod-opcao="120">120 dias</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Valor de 1 ponto Yetz em R$</label>
                    <input type="text" id="valor-pontos" name="valor_pontos" data-thousands="." data-decimal="," data-prefix="R$ ">
                </div>
                <div class="form-group">
                    <label for="">Título do Regulamento Geral *</label>
                    <input type="text" id="titulo-regulamento" name="titulo_regulamento" required>
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Regulamento Geral *</label>
                    <div class="box">
                        <input type="file" name="campanha-regulamento[]" id="campanha-regulamento" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="campanha-regulamento"><span></span> <strong>Selecionar Arquivo</strong></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Logo da Campanha</label>
                    <div class="box">
                        <input type="file" name="campanha-logo[]" id="campanha-logo" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="campanha-logo"><span></span> <strong>Selecionar Arquivo</strong></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="check check-disponivel">
                        <div class="square"></div>
                        <span>Atendimento disponível.</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="check check-temporaria">
                        <div class="square"></div>
                        <span>Pontos temporários.</span>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para cadastrar sua campanha.
                    </p>
                    <button type="submit">finalizar cadastro <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>


    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO NO CADASTRO DA CAMPANHA', 'link' => '/sistema/campanhas/adicionar'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CADASTRO DE CAMPANHA REALIZADO COM SUCESSO!', 'link' => '/sistema/campanhas'])

@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if({{ \Auth::user()->perfil_id }} == 3)
            window.location.href = "/sistema";
        });
    </script>
    <script>
        $(function() {
            $('#valor-pontos').maskMoney();
        });
    </script>
@stop
