@extends('sistema.layout.admin-layout')

@section('titulo', $campanha->nome_campanha)

@section('conteudo')

    <div class="area-cadastro">
        <form id="editar-campanha">
            <input type="hidden" id="cod-campanha" value="{{ $campanha->cod_campanha }}">
            <div class="s-esq">
                <div class="title">
                    <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">
                        <div class="circle">
                            <i class="icone-arrow-left"></i>
                        </div>
                    </a>
                    <h2>editar campanha</h2>
                </div>
                <div class="form-group">
                    <label for="">Cliente *</label>
                    <div class="select-custom">
                        <input type="hidden" name="cod_cliente" id="campanha-cliente-id">
                        <a href="" class="item-selected">
                            <span>{{ $campanha->cliente->nome }}</span>
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">PDVs *</label>
                    <div id="campanha-pdvs" class="todos-pdvs">
                        @foreach($campanha->cliente->cliente_pdvs as $pdv)
                            <div class="check {{ $campanha->campanha_pdvs->contains($pdv) ? 'checked' : '' }}" data-cod="{{ $pdv->cod_pdv }}">
                                <div class="square"></div>
                                <span>{{ $pdv->nome_pdv }}</span>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Nome da Campanha *</label>
                    <input type="text" id="campanha-nome" name="campanha_nome" required value="{{ $campanha->nome_campanha }}">
                </div>
                <div class="form-group">
                    <label for="">Tipo de Campanha *</label>
                    <div class="select-custom">
                        <input type="hidden" name="campanha_tipo" id="campanha-tipo" value="{{ $campanha->super == 0 ? '0' : '' }}">
                        <a href="" class="item-selected">
                            <span>{{ $campanha->super ? 'Yetz Super' : 'Yetz Cards' }}</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="0">Yetz Cards</a></li>
                                <li><a href="" cod-opcao="1">Yetz Super</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Chave da Campanha *</label>
                    <input type="text" id="campanha-chave" name="campanha_chave" value="{{ $campanha->chave }}" required oninput="this.value = this.value.replace(/[^a-z0-9]/gi, '').toUpperCase();">
                </div>
                <div class="data">
                    <div class="form-group">
                        <label for="">Data de Início *</label>
                        <input type="text" mask="date" name="campanha_inicio" id="campanha-inicio" required value="{{ date('d/m/Y', strtotime($campanha->dt_inicio)) }}">
                    </div>
                    <div class="form-group">
                        <label for="">Data Final *</label>
                        <input type="text" mask="date" name="campanha_fim" id="campanha-fim" required value="{{ date('d/m/Y', strtotime($campanha->dt_fim)) }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Validade dos Pontos *</label>
                    <div class="select-custom">
                        <input type="hidden" name="validade_pontos" id="validade-pontos" value="{{ $campanha->validade_pontos }}">
                        <a href="" class="item-selected">
                            <span>{{ $campanha->validade_pontos }} dias</span>
                        </a>
                        <div class="dropdown">
                            <ul>
                                <li><a href="" cod-opcao="30">30 dias</a></li>
                                <li><a href="" cod-opcao="60">60 dias</a></li>
                                <li><a href="" cod-opcao="90">90 dias</a></li>
                                <li><a href="" cod-opcao="120">120 dias</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Valor de 1 ponto Yetz em R$</label>
                    <input type="text" id="valor-pontos" value="R$ {{ number_format($campanha->valor_pontos, '2', ',', '.') }}" name="valor_pontos" data-thousands="." data-decimal="," data-prefix="R$ ">
                </div>
                <div class="form-group">
                    <label for="">Título do Regulamento Geral *</label>
                    <input type="text" id="titulo-regulamento" name="titulo_regulamento" value="{{ $campanha->titulo_regulamento }}" required>
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Regulamento Geral *</label>
                    <div class="box">
                        <input type="file" name="campanha-regulamento[]" id="campanha-regulamento" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="campanha-regulamento"><span>{{ limpaString($campanha->regulamento, '.') }}</span> <strong>Selecionar Arquivo</strong></label>
                    </div>
                    @if(!empty($campanha->regulamento))
                    <div>
                            <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}/baixar-regulamento" target="_blank">Baixar regulamento: {{ $campanha->regulamento }}</a>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="" class="label-enviar-logo">Logo da Campanha</label>
                    <div class="box">
                        <input type="file" name="campanha-logo[]" id="campanha-logo" class="inputfile inputfile-6"
                               data-multiple-caption="{count} files selected" />
                        <label for="campanha-logo"><span>{{ limpaString($campanha->logo, '.') }}</span> <strong>Selecionar Arquivo</strong></label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="check check-disponivel {{ $campanha->fale_conosco ? 'checked' : '' }}">
                        <div class="square"></div>
                        <span>Atendimento disponível.</span>
                    </div>
                </div>
                <div class="form-group">
                    <div class="check check-temporaria {{ $campanha->pontos_temporarios ? 'checked' : '' }}">
                        <div class="square"></div>
                        <span>Pontos temporários.</span>
                    </div>
                </div>
            </div>
            <div class="s-dir">
                <div class="box-white">
                    <img src="{{ asset('img/icons/icone-check-adm.svg') }}" class="icon" alt="">
                    <p>
                        Preencha os campos ao lado e clique no botão abaixo para atualizar a campanha.
                    </p>
                    <button type="submit">atualizar <i class="icone-arrow-right"></i></button>
                </div>
                <span class="alert">Campos com * são obrigatórios.</span>
            </div>
        </form>
    </div>


    @include('sistema.includes.modal-erro', ['mensagem' => 'ERRO AO ATUALIZAR CAMPANHA', 'link' => '/sistema/campanhas/adicionar'])

    @include('sistema.includes.modal-sucesso', ['mensagem' => 'CAMPANHA ATUALIZADA COM SUCESSO!', 'link' => '/sistema/campanhas'])

@stop

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            if({{ \Auth::user()->perfil_id }} == 3)
            window.location.href = "/sistema";
        });
    </script>
    <script>
        $(function() {
            $('#valor-pontos').maskMoney();
        });
    </script>
@stop
