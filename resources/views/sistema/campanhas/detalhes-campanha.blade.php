@extends('sistema.layout.admin-layout')

@section('titulo', $campanha->nome_campanha)

@section('conteudo')
<div id="campanha">
      <input type="hidden" id="cod-campanha" value="{{ $campanha->cod_campanha }}">
      <input type="hidden" id="perfil-adm" value="{{ \Auth::user()->perfil_id }}">
      <main>
            @include('sistema.includes.menu-admin')
            <section class="area-geral-campanha">
                  <aside>
                        <div class="cont">
                              <a href="/sistema/campanhas" class="btn-voltar">
                                    <img src="{{ asset('img/icons-adm/circulo-laranja.svg') }}" alt="">
                                    <span>voltar</span>
                              </a>
                              <div class="title">
                                    <h1>{{ $campanha->nome_campanha }}</h1>
                                    @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                          <button type="button">
                                                <img src="{{ asset('img/icons-adm/bar-cinza.svg') }}" alt="">
                                          </button>
                                          <div class="dropdown">
                                                <ul>
                                                      <li>
                                                            <a
                                                                  href="/sistema/campanhas/editar/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">
                                                                  <i class="icone-lapis"></i>
                                                                  <span>editar</span>
                                                            </a>
                                                      </li>
                                                      @if(empty($campanha->trashed()))
                                                      <li>
                                                            <a onclick="campanha.excluir({{ $campanha->cod_campanha }})"
                                                                  class="btn-excluir" style="cursor:pointer;">
                                                                  <img src="{{ asset('img/icons-adm/excluir.svg') }}" alt="">
                                                                  <span>desativar</span>
                                                            </a>
                                                      </li>
                                                      @else
                                                      <li>
                                                            <a onclick="campanha.ativar({{ $campanha->cod_campanha }})"
                                                                  class="btn-excluir" style="cursor:pointer;">
                                                                  <i class="icone-lapis"></i>
                                                                  <span>ativar</span>
                                                            </a>
                                                      </li>
                                                      @endif
                                                </ul>
                                          </div>
                                    @endif
                              </div>
                              @if($campanha->trashed())
                              <div class="list">
                                    <h3 style="color: red;">campanha inativa</h3>
                              </div>
                              @endif
                              <ul class="info-list">
                                    <li>
                                          <h3>total de pts.</h3>
                                          <div class="pts">
                                                <i class="icone icone-circulo-estrela"></i>
                                                <i class="arrow icone-arrow-right"></i>
                                                <span>{{ number_format($campanha->campanha_protocolos()->whereNotNull('data_efetivacao')->get()->sum('total_pontos'), 0, '', '.') }}</span>
                                          </div>
                                    </li>
                                    <li>
                                          <h3>resgates</h3>
                                          <div class="pts">
                                                <i class="icone icone-resgate"></i>
                                                <i class="arrow icone-arrow-right"></i>
                                                <span>{{ number_format($campanha->resgates->sum('valor_resgate'), 0, '', '.') }}</span>
                                          </div>
                                    </li>
                                    <li>
                                          <h3>usuários</h3>
                                          <div class="pts">
                                                <i class="icone icone-user"></i>
                                                <i class="arrow icone-arrow-right"></i>
                                                <span>{{ $campanha->usuarios_campanha()->withTrashed()->where('status', 1)->count() }}/{{ $campanha->usuarios_campanha()->withTrashed()->count() }}</span>
                                          </div>
                                    </li>
                              </ul>
                              <div class="dados">
                                    <h2>dados da campanha</h2>
                                    <ul class="menu">
                                          <li>
                                                <span>Tipo de Campanha:
                                                      {{ $campanha->super ? 'Yetz Super' : 'Yetz Cards' }}</span>
                                          </li>
                                          <li>
                                                <span>Chave da Campanha: {{ $campanha->chave }}</span>
                                          </li>
                                          <li>
                                                <span>Data Inicial:
                                                      {{ date('d/m/Y', strtotime($campanha->dt_inicio)) }}</span>
                                          </li>
                                          <li>
                                                <span>Data Final:
                                                      {{ date('d/m/Y', strtotime($campanha->dt_fim)) }}</span>
                                          </li>
                                          <li>
                                                <span>Validade do Pontos: {{ $campanha->validade_pontos }} dias</span>
                                          </li>
                                          <li>
                                                <span>Atendimento :
                                                      {{ $campanha->fale_conosco ? 'Disponível' : 'Indisponível' }}</span>
                                          </li>
                                          <li>
                                                <span>Pontos Temporários :
                                                      {{ $campanha->pontos_temporarios ? 'Sim' : 'Não' }}</span>
                                          </li>
                                          <li>
                                                <span>PDVs:</span>
                                          </li>
                                    </ul>
                                    <div class="pdvs">
                                          @foreach($campanha->campanha_pdvs as $pdv)
                                          <span>{{ $pdv->nome_pdv }}</span>
                                          @endforeach
                                    </div>
                              </div>
                              {{--                    <a href="" class="btn-usuario">--}}
                              {{--                        <i class="icone-portal"></i>--}}
                              {{--                        <span>ACESSAR COMO USUÁRIO</span>--}}
                              {{--                    </a>--}}
                        </div>
                  </aside>
                  <div class="cont-geral">
                        <div class="topo-abas">
                              <a href="" class="btn-recolher-sidebar">
                                    <i class="icone-arrow-left"></i>
                              </a>
                              <ul class="nav-tabs">
                                    <li class="active">
                                          <a id="tab-pts">
                                                <i class="icone-circulo-estrela"></i>
                                          </a>
                                    </li>
                                    <li>
                                          <a id="tab-loja">
                                                <i class="icone-carrinho"></i>
                                          </a>
                                    </li>
                                    <li>
                                          <a id="tab-usuarios">
                                                <i class="icone-user"></i>
                                          </a>
                                    </li>
                                    <li>
                                          <a id="tab-resgates">
                                                <i class="icone-resgate"></i>
                                          </a>
                                    </li>
                                    <li>
                                          <a id="tab-regulamento">
                                                <i class="icone-arquivos"></i>
                                          </a>
                                    </li>
                                    @if($campanha->pontos_temporarios)
                                          <li>
                                                <a id="tab-temporarias" class="temporaria"></a>
                                          </li>
                                    @endif
                              </ul>
                        </div>

                        <div class="tab-pane" id="geral-pts">
                              <div class="backdrop" style="display: none;"></div>
                              <div>
                                    <div class="loader" id="loader-carga" style="display: none;">
                                          <i class="fa fa-spinner fa-spin"></i>
                                    </div>
                              </div>
                              <div class="add-file-pts">
                                    <div class="title">
                                          <i class="icone icone-circulo-estrela"></i>
                                          <i class="arrow icone-arrow-right"></i>
                                          <div class="text">
                                                <h2>ACIDIONAR PONTOS</h2>
                                                <p>Anexe aqui a planilha de pontos:</p>
                                          </div>
                                    </div>
                                    <form id="campanha-enviar-planilha">
                                          <input type="hidden" id="cod-campanha"
                                                value="{{ $campanha->cod_campanha }}" />
                                          <div class="box">
                                                <input type="file" name="file-7[]" id="carga-arquivo"
                                                      class="inputfile inputfile-6"
                                                      data-multiple-caption="{count} files selected" accept="xlsx" />
                                                <label for="carga-arquivo"><span></span> <strong><img
                                                                  src="{{ asset('img/icons-adm/anexo.svg') }}"
                                                                  alt=""></strong></label>
                                          </div>
                                          <button id="btn-enviar-planilha" type="submit" class="disabled">Enviar
                                                planilha</button>
                                    </form>
                              </div>

                              {{--PARA EFETIVAR--}}
                              <div class="todas-cargas">
                                    @foreach($campanha->campanha_protocolos()->where('data_efetivacao', '=', null)->orderBy('created_at', 'DESC')->get() as $protocolo)
                                    <div class="item efetivar">
                                          <span class="cod-carga">{{ $protocolo->protocolo }}</span>
                                          <div class="pts">
                                                <i class="icone icone-circulo-estrela"></i>
                                                <i class="seta icone-arrow-right"></i>
                                                <span>{{ number_format($protocolo->total_pontos, 0, '', '.') }}</span>
                                          </div>
                                          <span
                                                class="cinza">{{ date('d.m.Y', strtotime($protocolo->created_at)) }}</span>
                                          <span
                                                class="cinza">{{ \App\Models\Admin::find($protocolo->cod_adm_criador)->nome }}</span>
                                          @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                <a href="/sistema/campanhas/download-carga-de-pontos/{{$protocolo->hash_arquivo}}/{{ $protocolo->arquivo}}">{{ !empty($protocolo->titulo) ? mb_strimwidth($protocolo->titulo, 0, 30, "...") : 'Arquivo.XLSX' }}</a>
                                          @endif
                                          <div class="acoes">
                                                @if(!empty($protocolo->dados))
                                                <a
                                                      href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{limpaString($campanha->nome_campanha)}}/protocolo/{{ $protocolo->cod_protocolo }}">
                                                      <img src="{{ asset('img/icons-adm/olho.svg') }}" alt="">
                                                </a>
                                                @endif
                                                @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                      <a style="cursor: pointer;"
                                                            onclick="carga.excluir({{ $protocolo->cod_protocolo }})"
                                                            class="btn-excluir-carga">
                                                            <img src="{{ asset('img/icons-adm/xis.svg') }}" alt="">
                                                      </a>
                                                      <button type="button"
                                                            onclick="carga.efetivar({{ $protocolo->cod_protocolo }})"
                                                            class="btn-efetivar-carga">
                                                            efetivar
                                                      </button>
                                                @endif
                                          </div>
                                    </div>
                                    @endforeach
                                    @foreach($campanha->campanha_protocolos()->where('data_efetivacao', '!=', null)->orderBy('created_at', 'DESC')->get() as $protocolo)
                                    <div class="item efetivadas">
                                          <span class="cod-carga">{{ $protocolo->protocolo }}</span>
                                          <div class="pts">
                                                <i class="icone icone-circulo-estrela"></i>
                                                <i class="seta icone-arrow-right"></i>
                                                <span>{{ number_format($protocolo->total_pontos, 0, '', '.') }}</span>
                                          </div>
                                          <span class="cinza">{{ date('d.m.Y', strtotime($protocolo->created_at)) }}</span>
                                          <span class="cinza">{{ \App\Models\Admin::find($protocolo->cod_adm_criador)->nome }}</span>
                                          @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                <a href="/sistema/campanhas/download-carga-de-pontos/{{$protocolo->hash_arquivo}}/{{ $protocolo->arquivo}}">{{ !empty($protocolo->titulo) ? mb_strimwidth($protocolo->titulo, 0, 30, "...") : 'Arquivo.XLSX' }}</a>
                                          @endif
                                          <div class="acoes">
                                                @if(!empty($protocolo->dados))
                                                <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{limpaString($campanha->nome_campanha)}}/protocolo/{{ $protocolo->cod_protocolo }}"
                                                      class="btn-detalhes">detalhes</a>
                                                @endif
                                          </div>
                                    </div>
                                    @endforeach
                              </div>
                        </div>

                        <div class="tab-pane" id="regulamentos">
                              <div class="filtros">

                              </div>
                              <div class="painel-regulamentos">
                                    <div>
                                          <div class="loader" id="loader-regulamento" style="display: none;">
                                                <i class="fa fa-spinner fa-spin"></i>
                                          </div>
                                    </div>
                                    <div class="box-geral">
                                          <div class="title">
                                                <i class="icone icone-arquivos"></i>
                                                <i class="seta icone-arrow-right"></i>
                                                <h2>Regulamento Geral da Campanha</h2>
                                          </div>
                                          <a
                                                href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}/baixar-regulamento">{{ $campanha->regulamento }}</a>
                                          @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}/regulamento-geral">
                                                      <button type="button">atualizar</button>
                                                </a>
                                          @endif
                                    </div>
                                    <div class="todos">
                                          @foreach($campanha->usuarios_campanha()->select('ilha')->groupBy('ilha')->withTrashed()->get() as $ilha)
                                          @php
                                          $regulamento = $campanha->regulamentos()->where('ilha', $ilha->ilha)->first();
                                          @endphp
                                          <div class="box-regulamento">
                                                <h3>{{ preg_replace("/_/", " ", $ilha->ilha) }}</h3>
                                                <div class="nome">
                                                      @if(!empty($regulamento))
                                                      <i class="icone icone-pdv"></i>
                                                      <i class="seta icone-arrow-right"></i>
                                                      <span>{{ $regulamento->titulo }}</span>
                                                      @endif
                                                </div>
                                                @if(!empty($regulamento))
                                                <a
                                                      href="/sistema/campanhas/{{ $regulamento->id }}/{{ limpaString($regulamento->ilha) }}/baixar-regulamento-ilha" target="_blank">{{ preg_replace("/_/", " ", $regulamento->regulamento) }}</a>
                                                @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                      <a
                                                            href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}/regulamento/{{ limpaString($regulamento->ilha)  }}/{{ $regulamento->id }}"><button
                                                                  type="button" class="btn-atualizar">atualizar</button></a>
                                                      <a style="cursor: pointer;"
                                                            onclick="regulamento.excluir({{ $regulamento->id }})"
                                                            class="btn-excluir-carga"><img
                                                                  src="{{ asset('img/icons-adm/xis.svg') }}" alt=""></a>
                                                @endif
                                                @else
                                                      @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                            <span class="arquivo-pendente">Arquivo Pendente</span>
                                                            <a onclick="regulamento.novoRegulamento('{{ $ilha->ilha }}')"><button
                                                                        type="button" class="btn-enviar">enviar</button></a>
                                                      @endif
                                                @endif
                                          </div>
                                          @endforeach

                                    </div>
                              </div>
                        </div>

                        <div class="tab-pane" id="resgates">
                              <div class="filtros">
                                    <div class="form-group">
                                          <label for="">Status</label>
                                          <div class="select-custom">
                                                <input type="hidden" name="cod_cliente"
                                                      id="filtro-resgates-campanha-status">
                                                <a href="" class="item-selected">
                                                      <span>Todos</span>
                                                </a>
                                                <div class="dropdown">
                                                      <ul>
                                                            <li><a href="" cod-opcao="">Todos</a></li>
                                                            <li><a href="" cod-opcao="1">Aguardando</a></li>
                                                            <li><a href="" cod-opcao="2">Aprovado</a></li>
                                                            <li><a href="" cod-opcao="3">À Caminho</a></li>
                                                            <li><a href="" cod-opcao="4">Entregue</a></li>
                                                            <li><a href="" cod-opcao="5">Cancelado</a></li>
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="">Data Ínicio</label>
                                          <input type="text" mask="date" id="filtro-resgates-campanha-dt-inicio">
                                    </div>
                                    <div class="form-group">
                                          <label for="">Data Final</label>
                                          <input type="text" mask="date" id="filtro-resgates-campanha-dt-fim">
                                    </div>

                                    <div class="input-search">
                                          <input type="text">
                                          <button type="button" id="filtro-resgates-campanha-busca"
                                                onclick="campanha.filtrarResgates()">
                                                <img src="{{ asset('img/icons-adm/lupa.svg') }}" alt="">
                                          </button>
                                    </div>

                              </div>
                              <div class="dados-resgates-gerais">
                                    <div>
                                          <div class="loader" style="display: none;">
                                                <i class="fa fa-spinner fa-spin"></i>
                                          </div>
                                    </div>
                                    <div id="todos-resgates-filtrado">

                                    </div>
                              </div>
                        </div>

                        <div class="tab-pane" id="usuarios">
                              <div class="filtros">
                                    <div class="form-group">
                                          <label for="">PDV</label>
                                          <div class="select-custom">
                                                <input type="hidden" id="filtro-usuarios-campanha-pdv">
                                                <a href="" class="item-selected">
                                                      <span>Todos</span>
                                                </a>
                                                <div class="dropdown">
                                                      <ul>
                                                            <li><a href="" cod-opcao="">Todos</a></li>
                                                            @foreach($campanha->campanha_pdvs()->orderBy('nome_pdv',
                                                            'ASC')->get() as $pdv)
                                                            <li><a href=""
                                                                        cod-opcao="{{ $pdv->cod_pdv }}">{{ $pdv->nome_pdv }}</a>
                                                            </li>
                                                            @endforeach
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="">Ilha</label>
                                          <div class="select-custom">
                                                <input type="hidden" id="filtro-usuarios-campanha-ilha">
                                                <a href="" class="item-selected">
                                                      <span>Todos</span>
                                                </a>
                                                <div class="dropdown">
                                                      <ul>
                                                            <li><a href="" cod-opcao="">Todas</a></li>
                                                            @foreach($campanha->usuarios_campanha()->select('ilha')->groupBy('ilha')->withTrashed()->get()
                                                            as $ilha)
                                                            <li><a href=""
                                                                        cod-opcao="{{ $ilha->ilha }}">{{ $ilha->ilha }}</a>
                                                            </li>
                                                            @endforeach
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="">Status</label>
                                          <div class="select-custom">
                                                <input type="hidden" id="filtro-usuarios-campanha-status">
                                                <a href="" class="item-selected">
                                                      <span>Todos</span>
                                                </a>
                                                <div class="dropdown">
                                                      <ul>
                                                            <li><a href="" cod-opcao="">Todos</a></li>
                                                            <li><a href="" cod-opcao="1">Ativo</a></li>
                                                            <li><a href="" cod-opcao="0">Bloqueado</a></li>
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="form-group">
                                          <label for="">Cadastro</label>
                                          <div class="select-custom">
                                                <input type="hidden" id="filtro-usuarios-campanha-cadastro">
                                                <a href="" class="item-selected">
                                                      <span>Todos</span>
                                                </a>
                                                <div class="dropdown">
                                                      <ul>
                                                            <li><a href="" cod-opcao="">Todos</a></li>
                                                            <li><a href="" cod-opcao="1">Concluído</a></li>
                                                            <li><a href="" cod-opcao="0">Não Concluído</a></li>
                                                      </ul>
                                                </div>
                                          </div>
                                    </div>
                                    <div class="input-search">
                                          <input type="text" id="filtro-usuarios-campanha-busca">
                                          <button type="button">
                                                <img src="{{ asset('img/icons-adm/lupa.svg') }}" alt="">
                                          </button>
                                    </div>
                              </div>
                              <div class="todos-usuarios">
                                    <div>
                                          <div class="loader" style="display: none;">
                                                <i class="fa fa-spinner fa-spin"></i>
                                          </div>
                                    </div>
                                    <div id="todos-usuarios-filtrado">

                                    </div>
                              </div>
                        </div>

                        <div class="tab-pane" id="loja">
                              <div class="todas-lojas">
                                    <div>
                                          <div class="loader" style="display: none;">
                                                <i class="fa fa-spinner fa-spin"></i>
                                          </div>
                                    </div>
                                    <div class="todas-categorias-produtos" id="todas-categorias-produtos">
                                          @foreach($categorias as $categoria)
                                          <div class="area-box-loja">
                                                <div class="box-loja">
                                                      <div class="info">
                                                            <h3>{{ $categoria->categoria }}</h3>
                                                            <div class="qtd">
                                                                  <i
                                                                        class="icone icone-{{ $categoria->icone_categoria }}"></i>
                                                                  <i class="seta icone-arrow-right"></i>
                                                                  <h4><span
                                                                              class="qtd-marcados">{{ $campanha->produtos()->where('cod_categoria_produto', $categoria->cod_categoria_produto)->where('disponibilidade', 1)->count() }}</span>
                                                                        / <span
                                                                              class="total">{{ $categoria->categoria_unica_produtos()->where('disponibilidade', 1)->count() }}</span>
                                                                  </h4>
                                                            </div>
                                                      </div>
                                                      <div class="cards">
                                                            <div class="title">
                                                                  <div class="check check-all check-produtos {{ $campanha->produtos()->where('cod_categoria_produto', $categoria->cod_categoria_produto)->where('disponibilidade', 1)->count() == $categoria->categoria_unica_produtos()->where('disponibilidade', 1)->count() ? 'checked' : '' }}"
                                                                        id="{{ $categoria->cod_categoria_produto }}">
                                                                        <div class="square"></div>
                                                                        <span>Todos</span>
                                                                  </div>
                                                                  <button type="button" class="btn-expandir">
                                                                        <i class="icone-arrow-down"></i>
                                                                  </button>
                                                            </div>
                                                            <div class="todos-cards">
                                                                  <ul>
                                                                        @foreach($categoria->categoria_unica_produtos()->where('disponibilidade',1)->orderBy('nome_produto', 'ASC')->orderBy('valor_reais', 'ASC')->get() as  $produto)
                                                                        <li>
                                                                              <div id="{{ $produto->cod_produto }}"
                                                                                    class="check {{ !empty($campanha->produtos()->where('campanha_produto.cod_produto', $produto->cod_produto)->where('produto.disponibilidade', 1)->first()) ? 'checked' : '' }}">
                                                                                    <div class="square"></div>
                                                                                    <span> {{ $produto->nome_produto }}
                                                                                          - R$
                                                                                          {{ $produto->valor_reais }}</span>
                                                                              </div>
                                                                        </li>
                                                                        @endforeach
                                                                  </ul>
                                                            </div>
                                                      </div>
                                                </div>
                                          </div>
                                          @endforeach
                                    </div>
                              </div>
                        </div>

                        @if($campanha->pontos_temporarios)
                               <div class="tab-pane" id="cargas-temporarias" style="display: none;">
                                    <div class="backdrop-temporaria" style="display: none;"></div>
                                    <div>
                                          <div class="loader" id="loader-carga-temporaria" style="display: none;">
                                                <i class="fa fa-spinner fa-spin"></i>
                                          </div>
                                    </div>
                                    <div class="add-file-pts">
                                          <div class="title">
                                                <img src="{{ asset('img/icons-adm/timer.svg') }}" alt="">
                                                <i class="arrow icone-arrow-right"></i>
                                                <div class="text">
                                                      <h2>ACIDIONAR PONTOS</h2>
                                                      <p>Anexe aqui a planilha de pontos:</p>
                                                </div>
                                          </div>
                                          <form id="campanha-enviar-planilha-temporaria">
                                                <input type="hidden" id="cod-campanha"
                                                      value="{{ $campanha->cod_campanha }}" />
                                                <div class="box">
                                                      <input type="file" name="file-7[]" id="carga-arquivo-temporario" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" accept="xlsx" />
                                                      <label for="carga-arquivo-temporario"><span></span> <strong>
                                                            <img src="{{ asset('img/icons-adm/temporario.svg') }}" alt=""></strong>
                                                      </label>
                                                </div>
                                                <button id="btn-enviar-planilha-temporario" type="submit" class="disabled">Enviar Planilha</button>
                                          </form>
                                    </div>

                                    {{-- PARA EFETIVAR--}}
                                    <div class="todas-cargas">
                                          @foreach($campanha->campanha_protocolos_temporarios()->where('data_efetivacao', '=', null)->orderBy('created_at', 'DESC')->get() as $protocolo_temp)
                                          <div class="item efetivar">
                                                <span class="cod-carga">{{ $protocolo_temp->protocolo }}</span>
                                                <div class="pts">
                                                      <i class="icone icone-circulo-estrela"></i>
                                                      <i class="seta icone-arrow-right"></i>
                                                      <span>{{ number_format($protocolo_temp->total_pontos, 0, '', '.') }}</span>
                                                </div>
                                                <span
                                                      class="cinza">{{ date('d.m.Y', strtotime($protocolo_temp->created_at)) }}</span>
                                                <span
                                                      class="cinza">{{ \App\Models\Admin::find($protocolo_temp->cod_adm_criador)->nome }}</span>
                                                @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                      <a href="/sistema/campanhas/download-carga-de-pontos-temporaria/{{$protocolo_temp->hash_arquivo}}/{{ $protocolo_temp->arquivo}}">{{ !empty($protocolo_temp->titulo) ? mb_strimwidth($protocolo_temp->titulo, 0, 30, "...") : 'Arquivo.XLSX' }}</a>
                                                @endif
                                                <div class="acoes">
                                                      @if(!empty($protocolo_temp->dados))
                                                      <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{limpaString($campanha->nome_campanha)}}/protocolo-temporario/{{ $protocolo_temp->cod_protocolo }}">
                                                            <img src="{{ asset('img/icons-adm/olho.svg') }}" alt="">
                                                      </a>
                                                      @endif
                                                      @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                            <a style="cursor: pointer;"
                                                                  onclick="carga.excluir({{ $protocolo_temp->cod_protocolo }}, 1)"
                                                                  class="btn-excluir-carga">
                                                                  <img src="{{ asset('img/icons-adm/xis.svg') }}" alt="">
                                                            </a>
                                                            <button type="button"
                                                                  onclick="carga.efetivar({{ $protocolo_temp->cod_protocolo }}, 1)"
                                                                  class="btn-efetivar-carga">
                                                                  efetivar
                                                            </button>
                                                      @endif
                                                </div>
                                          </div>
                                          @endforeach
                                          @foreach($campanha->campanha_protocolos_temporarios()->where('data_efetivacao', '!=', null)->orderBy('created_at', 'DESC')->get() as $protocolo_temp)
                                          <div class="item efetivadas">
                                                <span class="cod-carga">{{ $protocolo_temp->protocolo }}</span>
                                                <div class="pts">
                                                      <i class="icone icone-circulo-estrela"></i>
                                                      <i class="seta icone-arrow-right"></i>
                                                      <span>{{ number_format($protocolo_temp->total_pontos, 0, '', '.') }}</span>
                                                </div>
                                                <span
                                                      class="cinza">{{ date('d.m.Y', strtotime($protocolo_temp->created_at)) }}</span>
                                                <span
                                                      class="cinza">{{ \App\Models\Admin::find($protocolo_temp->cod_adm_criador)->nome }}</span>
                                                @if(\Auth::user()->perfil_id == 1 || \Auth::user()->perfil_id == 2)
                                                      <a href="/sistema/campanhas/download-carga-de-pontos-temporaria/{{$protocolo_temp->hash_arquivo}}/{{ $protocolo_temp->arquivo}}">{{ !empty($protocolo_temp->titulo) ? mb_strimwidth($protocolo_temp->titulo, 0, 30, "...") : 'Arquivo.XLSX' }}</a>
                                                @endif
                                                <div class="acoes">
                                                      @if(!empty($protocolo_temp->dados))
                                                      <a href="/sistema/campanhas/{{ $campanha->cod_campanha }}/{{limpaString($campanha->nome_campanha)}}/protocolo-temporario/{{ $protocolo_temp->cod_protocolo }}"
                                                            class="btn-detalhes">detalhes</a>
                                                      @endif
                                                </div>
                                          </div>
                                          @endforeach
                                    </div>
                              </div>
                        @endif
                  </div>
            </section>
      </main>
</div>

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function() {
      if ({{ \Auth::user()->perfil_id }} == 3)
            window.location.href = "/sistema";
});
</script>
@stop
