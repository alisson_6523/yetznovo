<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Global site tag (gtag.usuario_antigo) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131006548-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('usuario_antigo', new Date());

        gtag('config', 'UA-131006548-1');
    </script>

    <title>Yetz Cards | Agradecimentos </title>

    <link rel="stylesheet" href="{{ asset('css/usuario_antigo/agradecimento-personalizado.css') }}">
    <link rel="stylesheet" href="/css/usuario_antigo/bootstrap.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/swiper.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/usuario_antigo/font-yetz.css">
    <link rel="stylesheet" href="/css/usuario_antigo/amaran.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/impressao.css" media="print">
    <link rel="stylesheet" href="/css/usuario_antigo/flaticon.css">
    <link rel="stylesheet" href="/css/usuario_antigo/main.css">
    <link rel="stylesheet" href="/css/usuario_antigo/responsivo.css">
    <link rel="shortcut icon" href="/css/usuario_antigo/favicon.png" type="image/png" />

</head>
</head>
<body>
<div class="conteudo-agradecimento">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img src="{{ asset('img/logo-login.png') }}" class="img-responsive">
                <p>Parabéns, você concluiu o resgate de prêmios na plataforma YETZ CARDS.</p>
                <p>A confirmação desse resgate será enviada para seu e-mail cadastrado. Atente-se para que não tenha ido para a caixa de spam.</p>
                <p>Para acompanhar o andamento do seu regate acesse a página <a href="/loja/resgates">Acompanhar Resgates.</a></p>
                <p>Para ver o registro dessa operação acesse a página <a href="/loja/movimentacao">Extrato de Pontos.</a></p>

                <a href="/loja" class="btn-voltar-home">Voltar para Home</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
