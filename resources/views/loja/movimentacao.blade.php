@extends('layout.layout-loja')

@section('title', 'Extrato de Pontos')

@section('conteudo-loja')

<div class="conteudo-mov">
      <div class="titulo-da-pagina">
            <div class="container">
                  <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                              <img src="{{ asset('img/icone-extrato-page.png') }}" class="icone-extrato-page">
                              <h1 class="titulo-pag">CONFIRA SUA MOVIMENTAÇÃO DE <strong>PONTOS</strong></h1>
                        </div>
                  </div>
            </div>
      </div>

      <div class="conteudo-movimentacao">
            <div class="container">
                  <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-3">
                              <p>Filtro por mês</p>
                              <div class="form-group">
                                    <select class="form-control input-lg" id="dropdown-data">
                                          <option>TODOS</option>
                                          @foreach ($datas_filtro as $data)
                                          <option>{{ date('m/Y', strtotime($data->first()->created_at)) }}</option>
                                          @endforeach
                                    </select>
                              </div>
                              <p>Filtro por operação</p>
                              <div class="form-group">
                                    <select class="form-control input-lg" id="dropdown-tipo">
                                          <option>TODOS</option>
                                          <option>CRÉDITO</option>
                                          <option>RESGATE</option>
                                          <option>ESTORNO</option>
                                          <option>EXPIRAÇÃO</option>
                                    </select>
                              </div>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-9">
                              @if(count($usuario->movimentacoes) > 0)
                              <div class="table-responsive" id="tabela-movimentacao">
                                    <table class="table table-bordered" id="tabela">
                                          <thead>
                                                <tr>
                                                      <th>DATA/HORA</th>
                                                      <th>TIPO</th>
                                                      <th>DESCRIÇÃO</th>
                                                      <th>PONTOS</th>
                                                      <th>DETALHES</th>
                                                </tr>
                                          </thead>
                                          <tbody>
                                                @foreach($usuario->movimentacoes()->where('cod_campanha', session('campanha')->cod_campanha)->orderBy('created_at', 'DESC')->get() as $movimentacao)
                                                <tr data-cod="{{ $movimentacao->cod_movimentacao }}"
                                                      data-tipo="{{ $movimentacao->tipo_movimentacao->descricao_tipo }}">
                                                      <td>{{ \Carbon\Carbon::parse($movimentacao->created_at)->format('d/m/Y H:i') }}
                                                      </td>
                                                      <td>{{ $movimentacao->tipo_movimentacao->descricao_tipo }}</td>
                                                      <td>{{ $movimentacao->descricao }}</td>
                                                      <td>{{ $movimentacao->valor }}</td>
                                                      <td class="btn-detalhes-extrato">
                                                            <span>ver detalhes</span>
                                                            <i class="fa fa-caret-down"></i>
                                                      </td>
                                                </tr>

                                                @if($movimentacao->cod_tipo_movimentacao == 1)
                                                <tr class="detalhes-resgate"
                                                      data-cod="{{ $movimentacao->cod_movimentacao }}"
                                                      data-tipo="{{ $movimentacao->tipo_movimentacao->descricao_tipo }}">
                                                      <td colspan="5">
                                                            <p>PROTOCOLO:<br>{{ $movimentacao->protocolo }}</p>
                                                            <ul>
                                                                  <li>PRÊMIOS:</li>
                                                                  {{-- foreach necessario pois não há relação com o historico, necessario fazer --}}
                                                                  @foreach(($movimentacao->premio->carrinho->produtos ??
                                                                  []) as $item)
                                                                  <li>{{ $item->nome_produto . ' - R$ ' . $item->valor_reais}}
                                                                  </li>
                                                                  @endforeach
                                                            </ul>
                                                      </td>
                                                </tr>
                                                @else
                                                <tr class="detalhes-resgate"
                                                      data-tipo="{{ $movimentacao->tipo_movimentacao->descricao_tipo }}">
                                                      <td colspan="5">
                                                            <p>PROTOCOLO: <br>{{ $movimentacao->protocolo }}<p>
                                                                        <p>REFERÊNCIA:
                                                                              <br>{{ $movimentacao->referencia }}<p>
                                                                                    <p>VALIDADE DOS PONTOS:
                                                                                          <br>{{ date('d/m/Y', strtotime(\Carbon\Carbon::parse($movimentacao->created_at)->addDays(180))) }}
                                                      </td>
                                                </tr>
                                                @endif
                                                @endforeach
                                          </tbody>
                                    </table>
                              </div>
                              @else
                              <h1 class="aviso-vazio">Sem registro de movimentação no momento.</h1>
                              @endif
                        </div>
                  </div>
            </div>
      </div>


      <div class="conteudo-acompanhe-seu-resgate-mobile">
            <div class="container-mobile">
                  <div class="filtro-mobile-resgates">
                        <div class="container-select tipo-data">
                              <div class="text-select-resgate">
                                    <p>Junho 2016</p>
                                    <img src="/img/mobile-resgate/resgate-01.png" alt="">
                              </div>

                              <div class="list-select-resgate">
                                    <ul>
                                          @foreach ($datas_filtro as $data)
                                          <li data-date="{{ date('m/Y', strtotime($data->first()->created_at)) }}">
                                                <p>{{ date('m/Y', strtotime($data->first()->created_at)) }}</p>
                                          </li>
                                          @endforeach
                                    </ul>
                              </div>
                        </div>

                        <div class="container-select tipo-movimentacao">
                              <div class="text-select-resgate">
                                    <p>Selecione</p>
                                    <img src="/img/mobile-resgate/resgate-01.png" alt="">
                              </div>

                              <div class="list-select-resgate">
                                    <ul>
                                          <li data-tipo="0">
                                                <p>TODOS</p>
                                          </li>
                                          <li data-tipo="1">
                                                <p>RESGATE</p>
                                          </li>
                                          <li data-tipo="2">
                                                <p>CRÉDITO</p>
                                          </li>
                                          <li data-tipo="3">
                                                <p>ESTORNO</p>
                                          </li>
                                          <li data-tipo="4">
                                                <p>EXPIRAÇÃO</p>
                                          </li>
                                    </ul>
                              </div>
                        </div>
                  </div>
            </div>

            <div class="container-cards">
                  @if(count($usuario->movimentacoes) > 0)
                      @foreach($usuario->movimentacoes()->where('cod_campanha', session('campanha')->cod_campanha)->orderBy('created_at', 'DESC')->get() as $movimentacao)
                      <div class="container-card-mobile" data-tipo="{{ $movimentacao->cod_tipo_movimentacao  }}"
                            data-date="{{ \Carbon\Carbon::parse($movimentacao->created_at)->format('m/Y') }}">
                            <div class="card-mobile">
                                  <div class="card-text">
                                        <h3>Nº: {{ $movimentacao->protocolo }}</h3>
                                        @if($movimentacao->cod_tipo_movimentacao == 1)
                                        <p>RESGATES: {{ count(($movimentacao->premio->carrinho->produtos ?? [])) }}</p>
                                        <p>Nº de pontos resgatados: {{ $movimentacao->valor }}</p>
                                        @else
                                        <p>{{ $movimentacao->tipo_movimentacao->descricao_tipo }}</p>
                                        <p>Nº de pontos: {{ $movimentacao->valor }}</p>
                                        <p>Referência: {{ $movimentacao->referencia }}</p>
                                        @endif
                                        <p>Data: {{ \Carbon\Carbon::parse($movimentacao->created_at)->format('d/m/Y H:i') }}
                                        </p>
                                  </div>

                                  @if($movimentacao->cod_tipo_movimentacao == 1)
                                  <div class="img-resgate-mobile">
                                        <img src="/img/mobile-resgate/resgate-01.png" alt="">
                                  </div>
                                  @endif
                            </div>

                            @if($movimentacao->cod_tipo_movimentacao == 1)
                            <div class="container-detalhe-card">
                                  @foreach(($movimentacao->premio->carrinho->produtos ?? []) as $item)
                                  <div class="datelhe-card">
                                        <div class="detalhe-card-img">
                                              <img src="{{ $item->link_foto }}" alt="">
                                        </div>
                                        <div class="detalhe-card-text">
                                              <p>Pedido: {{ $item->nome_produto }} - R${{ $item->valor_reais }}</p>
                                              <span>Pontos - {{ $item->valor_produto }}</span>
                                        </div>
                                  </div>
                                  @endforeach
                            </div>
                            @endif
                      </div>
                      @endforeach
                  @endif
            </div>
      </div>

</div>


@stop

@section('scripts')
<script src="{{ asset('js/usuario_antigo/usuario/movimentacoes.js') }}"></script>

<script>
let selects = Array.from(document.querySelectorAll('.text-select-resgate'))
let selectLi = Array.from(document.querySelectorAll('.tipo-movimentacao .list-select-resgate ul li'))
let selectLiData = Array.from(document.querySelectorAll('.tipo-data .list-select-resgate ul li'))
let selectsResgates = Array.from(document.querySelectorAll('.img-resgate-mobile'))
var statusFiltro;
var dateFiltro;

function removeItem(select) {
      let itens = Array.from(document.querySelectorAll(select))

      itens.forEach(function(item) {
            item.parentElement.querySelector('.list-select-resgate').classList.value.includes('active') ?
                  item.parentElement.querySelector('.list-select-resgate').classList.remove('active') : null
      })
}

function showItens() {
      Array.from(document.querySelectorAll('.container-cards .container-card-mobile')).forEach(function(item) {
            item.style.display = "block"
      })
}

function showItensDate() {

      if(dateFiltro && statusFiltro){
            Array.from(document.querySelectorAll('.container-cards .container-card-mobile')).forEach(function(item) {
                  let date = item.dataset.date
                  let tipo = item.dataset.tipo

                  if((tipo == statusFiltro) && (date == dateFiltro)){
                        item.style.display = "block"
                  }else{
                        item.style.display = "none"
                  }
            })
      }else{
            if(dateFiltro){
                  Array.from(document.querySelectorAll('.container-cards .container-card-mobile')).forEach(function(item) {
                        item.style.display = "block"
                  })
            }

            if(statusFiltro){
                  Array.from(document.querySelectorAll('.container-cards .container-card-mobile')).forEach(function(item) {
                        item.style.display = "block"
                  })
            }
      }
}

selects.forEach(function(select) {
      select.addEventListener('click', function() {
            if (select.parentElement.querySelector('.list-select-resgate').classList.value
                  .includes('active')) {
                  removeItem('.text-select-resgate')
            } else {
                  removeItem('.text-select-resgate')
                  select.parentElement.querySelector('.list-select-resgate').classList.toggle('active')
            }
      })
})

selectsResgates.forEach(function(selectResgate) {
      selectResgate.addEventListener('click', function() {
            let detalheCard = selectResgate.parentElement.parentElement.querySelector('.container-detalhe-card')
            let altura = 0;

            detalheCard.querySelectorAll('.datelhe-card').forEach(function(detalhe) {
                  altura += detalhe.offsetHeight + 2
            })

            if (detalheCard.classList.value.includes('active')) {
                  detalheCard.style.height = 0 + 'px'
                  detalheCard.classList.remove('active')
                  selectResgate.querySelector('img').classList.remove('active')
            } else {
                  detalheCard.style.height = altura + 'px'
                  detalheCard.classList.toggle('active')
                  selectResgate.querySelector('img').classList.toggle('active')
            }

      })
})

selectLi.forEach(function(li) {
      li.addEventListener('click', function() {
            li.parentElement.parentElement.classList.remove('active')
            let tipo = li.dataset.tipo

            if (tipo != 0) {
                  let setName = li.querySelector('p').innerHTML

                  statusFiltro = tipo
                  li.parentElement.parentElement.parentElement.querySelector('p').innerHTML = setName

                  let show = Array.from(document.querySelectorAll('.container-card-mobile:not([data-tipo="' + tipo + '"])'))

                  showItensDate()
                  show.forEach(function(item) {
                        item.style.display = "none"
                  })


            } else {
                  showItens()
            }
      })
})

selectLiData.forEach(function(li) {
      li.addEventListener('click', function() {
            li.parentElement.parentElement.classList.remove('active')
            let tipo = li.dataset.date

            if (tipo != 0) {
                  let setName = li.querySelector('p').innerHTML

                  dateFiltro = tipo

                  li.parentElement.parentElement.parentElement.querySelector('p').innerHTML = setName

                  let show = Array.from(document.querySelectorAll('.container-card-mobile:not([data-date="' + tipo + '"])'))

                  showItensDate()
                  show.forEach(function(item){
                        item.style.display = "none"
                  })


            } else {
                  showItens()
            }
      })
})
</script>

@stop
