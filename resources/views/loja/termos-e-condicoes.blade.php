@extends('layout.layout-loja')

@section('styles')
<link href="/css/usuario_antigo/personalizado-regulamento.css" rel="stylesheet">
@endsection

@section('title', 'Yetz Cards | Termos e Condições')

@section('conteudo-loja')
<div class="titulo-da-pagina">
      <div class="container">
            <div class="row">
                  <div class="col-xs-12 col-sm-12 col-md-12">
                        <h1>Termos e condições de uso do programa de recompensas yetz cards</h1>
                        <p>O Programa YETZ CARDS é um programa de recompensas através do qual as empresas participantes
                              poderão premiar seus funcionários em um portal de resgate de cartões, mediante pontuação
                              determinada através de regras da empresa contratante. </p>
                  </div>
            </div>
      </div>
</div>
<section id="topo-pagina">
      <div class="container">
            <div class="row">
                  <div class="conteudo-regulamento">
                        <div class="col-md-12">
                              <p style="text-align: center;"><strong>YETZ CARDS<br /></strong><strong>TERMOS E
                                          CONDI&Ccedil;&Otilde;ES DE USO</strong></p>
                              <p style="text-align: justify;">O presente <strong>TERMOS E CONDI&Ccedil;&Otilde;ES DE
                                          USO</strong> t&ecirc;m por finalidade estabelecer as <strong>REGRAS E
                                          CONDI&Ccedil;&Otilde;ES GERAIS</strong> para os usu&aacute;rios da plataforma
                                    de recompensas <strong>YETZ CARDS</strong>, de propriedade e mantida pela empresa
                                    <strong>YETZ INCENTIVO LTDA</strong>, CNPJ: 28.325.166/0001-05.</p>
                              <p style="text-align: justify;">A plataforma de recompensas destina-se &agrave;s empresas
                                    que desejam premiar colaboradores, parceiros, bem como toda e qualquer equipe em
                                    raz&atilde;o de desempenho superior ao ordinariamente esperado no exerc&iacute;cio
                                    de suas atividades. Isso se d&aacute; mediante premia&ccedil;&atilde;o por pontos,
                                    conforme regras espec&iacute;ficas da campanha ou a&ccedil;&atilde;o de incentivo de
                                    cada contratante. Esses pontos ser&atilde;o trocados por cart&otilde;es presentes
                                    e/ou vouchers digitais, dentro do per&iacute;odo de vig&ecirc;ncia da campanha e de
                                    acordo com os prazos estabelecidos, sendo prerrogativa do participante premiado a
                                    escolha cart&otilde;es presentes e/ou vouchers digitais a serem resgatados.</p>
                              <p style="text-align: justify;">O aceite ao <strong>TERMOS E CONDI&Ccedil;&Otilde;ES DE
                                          USO</strong> se d&aacute; no primeiro acesso, pela leitura e posterior clique
                                    no bot&atilde;o &ldquo;Li, compreendi e concordo com as disposi&ccedil;&otilde;es
                                    contidas nos Termos de Uso deste site&rdquo;; assim o
                                    <strong>USU&Aacute;RIO</strong> se responsabiliza e valida sua
                                    aceita&ccedil;&atilde;o e se torna apto a realizar seu cadastro para usufruir de
                                    todos os benef&iacute;cios e vantagens da plataforma.&nbsp; Para fins de
                                    demonstra&ccedil;&atilde;o da validade deste documento, a <strong>YETZ INCENTIVO
                                          LTDA</strong> poder&aacute; armazenar registros de logs de sua
                                    aceita&ccedil;&atilde;o. No caso de qualquer discord&acirc;ncia, total ou parcial,
                                    com este termo, o participante dever&aacute; abster-se de efetuar o cadastro e de
                                    utilizar qualquer canal da Plataforma.</p>
                              <p>&nbsp;</p>
                              <ol>
                                    <li><strong>CONCEITOS<br /><br /></strong><strong>1.1 YETZ INCENTIVO LTDA:
                                          </strong>empresa detentora da marca <strong>YETZ</strong> e suas marcas
                                          derivadas, respons&aacute;vel pela gest&atilde;o da <strong>PLATAFORMA DE
                                                RECOMPENSAS YETZ CARDS</strong>, apresentada no endere&ccedil;o <a
                                                href="http://www.yetzcards.com.br">www.yetzcards.com.br</a>.<br /><br /><strong>1.2
                                                YETZ CARDS</strong>: nome da plataforma de recompensas onde os pontos
                                          ser&atilde;o trocados por cart&otilde;es presente e/ou vouchers digitais,
                                          mediante regras da campanha de incentivo e desde que cumpridas as
                                          condi&ccedil;&otilde;es necess&aacute;rias citadas neste Termos e
                                          Condi&ccedil;&otilde;es de Uso, nos prazos
                                          determinados.<br /><br /><strong>1.3 CAMPANHA DE INCENTIVO</strong>: modo pelo
                                          qual as empresas contratantes premiam seus funcion&aacute;rios atrav&eacute;s
                                          de cart&otilde;es presente e/ou vouchers digitais. A empresa define o escopo
                                          da campanha, suas metas, regras, periodicidade e par&acirc;metros de
                                          convers&atilde;o. Al&eacute;m disso, o contratante tem como premissa definir
                                          quais dos cart&otilde;es presente e/ou vouchers digitais estar&atilde;o
                                          dispon&iacute;veis para resgate, podendo customizar sua
                                          campanha.<br /><br /><strong>1.4 USU&Aacute;RIOS</strong>: pessoa
                                          f&iacute;sica vinculada a uma Campanha devidamente cadastrada na Plataforma ou
                                          que receba de uma empresa participante um <strong>Cart&atilde;o</strong>
                                          <strong>YETZ CARD</strong>. Eventualmente chamado de participante, portador ou
                                          premiado.<br /><br /><strong>1.5 SENHA</strong>: secreta, pessoal e
                                          intransfer&iacute;vel, utilizada para acessar e realizar todas as
                                          opera&ccedil;&otilde;es na Plataforma YETZ CARDS.<br /><br /><strong>1.6
                                                PONTOS YETZ</strong>: unidade de medida determinada nas regras da
                                          Campanha, utilizada para resgates de cart&otilde;es presente e/ou vouchers
                                          digitais.<br /><br /><strong>1.7 CART&Atilde;O YETZ CARD</strong>:
                                          cart&atilde;o de pontos YETZ, entregue pelas empresas contratantes como
                                          recompensa e/ou incentivo e/ou premia&ccedil;&atilde;o, sem necessidade de
                                          cadastro em uma Campanha.<br /><br /><strong>1.8 CART&Otilde;ES
                                                PR&Eacute;-PAGOS DE SEGMENTO, LOJAS OU PRESENTE</strong>: meios de
                                          presentear, reconhecer e incentivar atrav&eacute;s de um cart&atilde;o
                                          presente, que pode ser utilizado na aquisi&ccedil;&atilde;o de bens e
                                          servi&ccedil;os em territ&oacute;rio nacional, nos estabelecimentos filiados
                                          &agrave; bandeira destacada no cart&atilde;o e/ou em lojas, ou marcas
                                          espec&iacute;ficas, respeitando o que &eacute; informado na
                                          descri&ccedil;&atilde;o e na regra de utiliza&ccedil;&atilde;o do
                                          cart&atilde;o.<br /><br /><strong>1.9 VOUCHERS DIGITAIS</strong>: meios de
                                          presentear, reconhecer e incentivar atrav&eacute;s de um voucher digital, que
                                          pode ser utilizado na aquisi&ccedil;&atilde;o de bens e servi&ccedil;os em
                                          territ&oacute;rio nacional, nos estabelecimentos informados na
                                          descri&ccedil;&atilde;o e conforme as regras de utiliza&ccedil;&atilde;o do
                                          voucher.<br /><br /><strong>1.10 CART&Atilde;O DE SEGMENTO</strong>:
                                          cart&atilde;o bandeirado para ser utilizado em lojas de determinado segmento
                                          como: Combust&iacute;vel, Entretenimento, Farm&aacute;cia, Papelaria,
                                          Qualidade de Vida e Supermercado. O detalhamento de cada segmento encontra-se
                                          dispon&iacute;vel na plataforma, na descri&ccedil;&atilde;o de cada um dos
                                          cart&otilde;es, bem como suas respectivas regras de
                                          utiliza&ccedil;&atilde;o.<br /><br /><strong>1.11 CART&Atilde;O DE
                                                LOJAS</strong>: cart&atilde;o de uma marca espec&iacute;fica, conforme
                                          detalhamento dispon&iacute;vel na plataforma, na descri&ccedil;&atilde;o de
                                          cada um dos cart&otilde;es, bem como suas respectivas regras de
                                          utiliza&ccedil;&atilde;o, espec&iacute;ficos de cada varejo emissor dos
                                          cart&otilde;es.<br /><br /><strong>1.12 CART&Atilde;O
                                                PR&Eacute;-PAGO</strong>: pode ser utilizado em qualquer estabelecimento
                                          que aceite cart&otilde;es bandeirados, conforme detalhamento dispon&iacute;vel
                                          na plataforma, na descri&ccedil;&atilde;o de cada um dos cart&otilde;es, bem
                                          como suas respectivas regras de
                                          utiliza&ccedil;&atilde;o.<br /><br /><strong>1.13 CATEGORIAS</strong>:
                                          segmenta&ccedil;&atilde;o dos cart&otilde;es presente e/ou vouchers digitais,
                                          indicadas por &iacute;cones espec&iacute;ficos.<br /><br /><strong>1.14
                                                CART&Atilde;O F&Iacute;SICO</strong>: cart&otilde;es f&iacute;sicos que
                                          s&atilde;o entregues no endere&ccedil;o cadastrado pela empresa contratante
                                          como aquele onde devam ser entregues os resgates dos
                                          usu&aacute;rios.<br /><br /><strong>1.15 VOUCHER DIGITAL</strong>: vouchers
                                          que s&atilde;o enviados para os e-mails cadastrados para cada usu&aacute;rio,
                                          podendo ser aquele de uso pessoal e cadastrado pelo participante ou outro
                                          determinado pela empresa contratante.<br /><br /><strong>1.16
                                                E-COMMERCE</strong>: s&atilde;o cart&otilde;es presente e/ou vouchers
                                          digitais que permitem compra atrav&eacute;s de
                                          e-commerce.<br /><br /><strong>1.17 LOJA F&Iacute;SICA</strong>: s&atilde;o
                                          cart&otilde;es presente e/ou vouchers digitais que permitem compras em lojas
                                          f&iacute;sicas.<br /><br /><strong>1.18 TERM&Ocirc;METRO</strong>: indicativo
                                          da possibilidade de resgatar um cart&atilde;o presente e/ou vouchers digitais,
                                          variando da cor vermelha at&eacute; a verde, conforme a proximidade com a
                                          obten&ccedil;&atilde;o dos pontos necess&aacute;rios.<br /><br /><strong>1.19
                                                CARRINHO</strong>: local onde s&atilde;o inseridos os cart&otilde;es
                                          presente e/ou vouchers digitais selecionados e finalizados os resgates.
                                          V&aacute;rios cart&otilde;es presente e/ou vouchers digitais podem ser
                                          resgatados no mesmo carrinho, desde que haja pontos YETZ
                                          suficientes.<br /><br /><strong>1.20 EXTRATO</strong>:
                                          consolida&ccedil;&atilde;o das movimenta&ccedil;&otilde;es de pontos recebidos
                                          (cr&eacute;ditos) e utiliza&ccedil;&atilde;o dos pontos (resgates),
                                          al&eacute;m de informar a data de expira&ccedil;&atilde;o dos pontos, conforme
                                          acordado entre YETZ e a contratante.<br /><br /><strong>1.21 SALDO</strong>:
                                          total de pontos YETZ recebidos menos o total
                                          utilizado.<br /><br /><strong>1.22 APP YETZ CARDS</strong>: aplicativo para
                                          dispositivos m&oacute;veis, nas plataformas iOS 9.0 e Android 4.0 ou
                                          superiores. Dispon&iacute;vel apenas para usu&aacute;rios cadastrados numa
                                          campanha e de acordo com determina&ccedil;&atilde;o/autoriza&ccedil;&atilde;o
                                          da contratante.<br /><br /><strong>1.23 COOKIES</strong>: s&atilde;o
                                          peda&ccedil;os de dados que os sites podem ativar em um navegador ou
                                          dispositivo para serem lidos em visitas futuras para saber mais sobre o
                                          usu&aacute;rio e personalizar uma experi&ecirc;ncia.</li>
                              </ol>
                              <p>&nbsp;</p>
                              <ol start="2">
                                    <li><strong>ELEGIBILIDADE E MODALIDADES DE
                                                PARTICIPA&Ccedil;&Atilde;O<br /></strong>O acesso de usu&aacute;rios a
                                          plataforma de recompensas <strong>YETZ CARDS</strong> pode ocorrer de 2 (duas)
                                          formas:<br /><br /><strong>2.1 CAMPANHA<br /><br /></strong>Os participantes
                                          ganham o direito de acesso ao integrarem uma campanha ou a&ccedil;&atilde;o de
                                          incentivos de empresa contratante e ter&atilde;o o pr&eacute;vio registro de
                                          seus dados e acesso a plataforma.<br /><br /><strong>2.2 CART&Atilde;O YETZ
                                                CARD<br /></strong>Os participantes ganham o direito de acesso ao
                                          receberem um cart&atilde;o j&aacute; com pontos carregados, n&atilde;o sendo
                                          necessariamente participante de uma campanha.</li>
                              </ol>
                              <p>&nbsp;</p>
                              <ol start="3">
                                    <li><strong>CADASTRO E SENHA<br /></strong><strong><br />3.1
                                                CAMPANHAS<br /><br /></strong>3.1.1 Ter&atilde;o acesso a plataforma
                                          usu&aacute;rios autorizados pelas empresas contratantes da plataforma de
                                          recompensas YETZ CARDS, que fornecer&atilde;o dados prim&aacute;rios como nome
                                          completo e CPF (ou outra forma de identifica&ccedil;&atilde;o num&eacute;rica
                                          &uacute;nica) de cada um dos participantes, que ser&atilde;o previamente
                                          registrados na plataforma.<br /><br />3.1.2 No primeiro acesso &agrave;
                                          Plataforma, o <strong>USU&Aacute;RIO</strong> dever&aacute; digitar a
                                          <strong>CHAVE DO CLIENTE</strong> que recebeu, clicar no bot&atilde;o
                                          <strong>BUSCAR</strong> e digitar sua <strong>IDENTIFICA&Ccedil;&Atilde;O
                                                PESSOAL</strong>. A seguir, clicar em <strong>VERIFICAR</strong> para
                                          iniciar o preenchimento do seu cadastro.<br /><br />3.1.3 No cadastro o nome
                                          do <strong>USU&Aacute;RIO</strong> e sua identifica&ccedil;&atilde;o pessoal
                                          estar&atilde;o preenchidos e n&atilde;o poder&atilde;o ser editados. Se houver
                                          diverg&ecirc;ncia, o participante n&atilde;o deve concluir o cadastro e deve
                                          procurar seu superior imediato. Caso esteja correto, dever&aacute; preencher
                                          as demais informa&ccedil;&otilde;es solicitadas.<br /><br />3.1.4 &Eacute;
                                          solicitado o cadastramento de uma conta de e-mail v&aacute;lida, que somente o
                                          participante poder&aacute; posteriormente alterar, acessando seu perfil na
                                          plataforma. Eventualmente, por solicita&ccedil;&atilde;o do cliente
                                          contratante, essa informa&ccedil;&atilde;o (e-mail) poder&aacute; estar
                                          previamente cadastrado (n&atilde;o podendo ser alterado).<br /><br />3.1.5
                                          &Eacute; solicitado o cadastramento de n&uacute;mero de telefone celular
                                          v&aacute;lido, que somente o participante poder&aacute; posteriormente
                                          alterar, acessando seu perfil na plataforma. Eventualmente, por
                                          solicita&ccedil;&atilde;o do cliente contratante, essa
                                          informa&ccedil;&atilde;o (celular) poder&aacute; estar previamente cadastrado
                                          ou ser nulo (n&atilde;o podendo ser alterado).<br /><br />3.1.6 &Eacute; de
                                          inteira responsabilidade do <strong>USU&Aacute;RIO</strong> a escolha e
                                          postagem de sua foto de perfil, que dever&aacute; ser de propriedade do
                                          participante, fidedigna e compat&iacute;vel com os bons costumes. Quaisquer
                                          consequ&ecirc;ncias oriundas de postagens de fotos inadequadas ser&atilde;o
                                          creditadas ao participante e poder&atilde;o implicar em sua exclus&atilde;o da
                                          plataforma.<br /><br />3.1.7 A senha de acesso dever&aacute; ser criada no
                                          momento do preenchimento do cadastro. Deve ser composta de, no m&iacute;nimo,
                                          seis caracteres alfanum&eacute;ricos ou especiais. Esta senha &eacute;
                                          secreta, pessoal e intransfer&iacute;vel, e deve ser utilizada para acessar a
                                          conta e realizar todas as transa&ccedil;&otilde;es na plataforma <strong>YETZ
                                                CARDS</strong>. &Eacute; de responsabilidade do participante manter sua
                                          senha protegida e n&atilde;o a fornecer para terceiros.<br /><br />3.1.8 Para
                                          concluir o cadastro, o participante deve ler as disposi&ccedil;&otilde;es
                                          contidas nos <strong>TERMOS E CONDIC&Otilde;ES DE USO</strong> e, caso
                                          concorde, marcar a caixa &ldquo;Li, compreendi e concordo com as
                                          disposi&ccedil;&otilde;es contidas nos Termos e Condi&ccedil;&otilde;es de Uso
                                          deste site&rdquo;. Se houver discord&acirc;ncia em rela&ccedil;&atilde;o ao
                                          presente termo, n&atilde;o dever&aacute; concluir seu
                                          cadastro.<br /><br />3.1.9 &Eacute; fortemente recomendado que no momento de
                                          finalizar o cadastro, o usu&aacute;rio marque a op&ccedil;&atilde;o
                                          &ldquo;Aceito receber mensagens SMS/E-mail da Plataforma YETZ CARDS&rdquo;
                                          para receber comunica&ccedil;&otilde;es sobre a campanha atrav&eacute;s da
                                          plataforma. A qualquer momento ele poder&aacute; mudar esse status em
                                          &ldquo;MEU PERFIL&rdquo;. Caso a empresa contratante n&atilde;o deseje que
                                          sejam encaminhadas mensagens, poder&aacute; solicitar o bloqueio dessa
                                          facilidade.<br /><br />3.1.10 Caso perca ou esque&ccedil;a a senha ap&oacute;s
                                          realizado o cadastro, o usu&aacute;rio poder&aacute; solicitar a
                                          reemiss&atilde;o a reemiss&atilde;o da mesma e ser&aacute; enviada uma senha
                                          provis&oacute;ria para o e-mail constante no cadastro. Para tal, escolher a
                                          op&ccedil;&atilde;o <strong>ESQUECI MINHA SENHA</strong> na janela de acesso,
                                          ap&oacute;s informar a <strong>CHAVE DO CLIENTE</strong> e
                                          identifica&ccedil;&atilde;o pessoal. Ao acessar a Plataforma com a senha
                                          provis&oacute;ria, ela pode ser alterada em seu perfil.<br /><br />3.1.11 O
                                          usu&aacute;rio pode alterar sua senha quando desejar. Para isso deve acessar a
                                          barra central, clicar em <strong>MEU PERFIL</strong> e em <strong>TROCAR
                                                SENHA</strong>.<br /><br />3.1.12 Manter o cadastro atualizado &eacute;
                                          de total responsabilidade do participante da Plataforma YETZ CARDS. O
                                          usu&aacute;rio arcar&aacute; com quaisquer preju&iacute;zos ou atrasos na
                                          entrega resgates de pr&ecirc;mios oriundos de cadastros incompletos,
                                          desatualizados ou com erros.<br /><br />3.1.13 Ap&oacute;s o cadastro
                                          conclu&iacute;do, o participante poder&aacute; acessar a plataforma
                                          regularmente, consultar pontos, extratos, solicitar resgate, dentre outras
                                          funcionalidades.<br /><br /><strong>3.2 YETZ CARD<br /><br /></strong>3.2.1
                                          Ap&oacute;s acessar o site www.yetz.com.br e ser direcionado &agrave; tela de
                                          acesso do cart&atilde;o YETZ CARD, o USU&Aacute;RIO deve digitar o
                                          n&uacute;mero do cart&atilde;o e PIN constantes na parte frontal do
                                          cart&atilde;o, que s&atilde;o revelados ao raspar a &aacute;rea demarcada. A
                                          seguir, clicar em VERIFICAR.<br /><br />3.2.2 YETZ CARD &eacute; um
                                          cart&atilde;o ao portador e poder&aacute; ser utilizado por quem tiver sua
                                          posse. O extrato e os pontos est&atilde;o vinculados ao cart&atilde;o e em
                                          caso de perda, roubo ou extravio n&atilde;o ser&aacute; poss&iacute;vel
                                          consultar o saldo ou recuperar os pontos creditados, sendo de responsabilidade
                                          do USU&Aacute;RIO a sua correta guarda e
                                          conserva&ccedil;&atilde;o.<br /><br />3.2.3 &Eacute; importante observar que
                                          n&atilde;o ser&aacute; poss&iacute;vel resgatar saldos remanescentes no
                                          cart&atilde;o YETZ CARDS e para zerar a pontua&ccedil;&atilde;o do
                                          cart&atilde;o &eacute; necess&aacute;rio que a escolha do(s) pr&ecirc;mios
                                          sejam no valor total do cart&atilde;o.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="4">
                                    <li><strong>PONTOS YETZ<br /><br /></strong><strong>4.1
                                                CAMPANHAS<br /><br /></strong>4.1.1 A carga de pontos acontece de acordo
                                          com as regras definidas pela empresa contratante, que envia periodicamente
                                          arquivo com a pontua&ccedil;&atilde;o relativa a cada USU&Aacute;RIO. As
                                          pontua&ccedil;&otilde;es podem ser creditadas quantas vezes forem
                                          necess&aacute;rias e pactuadas no decorrer da campanha em
                                          quest&atilde;o.<br /><br />4.1.2 Os cr&eacute;ditos de pontos YETZ s&atilde;o
                                          cumulativos, ou seja, ser&atilde;o somados aos j&aacute; existentes,
                                          permitindo que o participante utilize a soma dos saldos para resgate de seus
                                          cart&otilde;es presente e/ou vouchers digitais de
                                          premia&ccedil;&atilde;o.<br /><br />4.1.3 N&atilde;o &eacute; permitida a
                                          comercializa&ccedil;&atilde;o, troca, doa&ccedil;&atilde;o, permuta, venda,
                                          cess&atilde;o ou outra forma de transfer&ecirc;ncia dos pontos entre os
                                          participantes ou terceiros. No caso de falecimento do participante, o acesso
                                          &agrave; Plataforma ser&aacute; encerrado e os Pontos YETZ acumulados
                                          ser&atilde;o cancelados, n&atilde;o se constituindo direito de
                                          heran&ccedil;a.<br /><br />4.1.4 Os pontos YETZ creditados na Plataforma YETZ
                                          CARDS possuem prazo determinado conforme regras de cada campanha e indicados
                                          no extrato.<br /><br />4.1.5 &Eacute; responsabilidade do participante
                                          acompanhar seu extrato e vencimento dos pontos para que n&atilde;o tenha
                                          pontos vencidos e n&atilde;o resgatados. Caso isso suceda, o participante
                                          perder&aacute; seu direito aos pontos e n&atilde;o poder&aacute; solicitar
                                          reembolso de nenhuma esp&eacute;cie.<br /><br />4.1.6 N&atilde;o haver&aacute;
                                          incid&ecirc;ncia de qualquer tipo de juros, remunera&ccedil;&atilde;o ou
                                          b&ocirc;nus sobre a quantidade de pontos YETZ que cada participante possui e,
                                          em nenhuma hip&oacute;tese, estes poder&atilde;o ser substitu&iacute;dos por
                                          valores em Real.<br /><br />4.1.7 A contratada se reserva o direito de
                                          invalidar pontos YETZ creditados para o participante, se tiver conhecimento de
                                          que n&atilde;o foram cumpridas as regras constantes na Campanha ou por
                                          determina&ccedil;&atilde;o da contratante.<br /><br /><strong>4.2 YETZ
                                                CARD<br /><br /></strong>4.2.1 A contratante pode adquirir
                                          cart&otilde;es YETZ CARD com cr&eacute;dito em pontos pr&eacute;-carregado,
                                          para distribui&ccedil;&atilde;o aos seus colaboradores, conforme sua
                                          pol&iacute;tica de premia&ccedil;&atilde;o.<br /><br />4.2.2 Os pontos do YETZ
                                          CARD j&aacute; estar&atilde;o dispon&iacute;veis na Plataforma para resgate no
                                          acesso.<br /><br />4.2.3 Por se tratar de cart&atilde;o ao portador com saldo
                                          dispon&iacute;vel, em caso de roubo, perda ou extravio n&atilde;o ser&aacute;
                                          pass&iacute;vel de substitui&ccedil;&atilde;o, sendo a guarda de
                                          responsabilidade do portador.<br /><br />4.2.4 N&atilde;o &eacute; permitida a
                                          comercializa&ccedil;&atilde;o, troca, doa&ccedil;&atilde;o, permuta, venda,
                                          cess&atilde;o ou outra forma de transfer&ecirc;ncia dos pontos entre os
                                          participantes ou terceiros. No caso de falecimento do participante, o acesso
                                          &agrave; Plataforma ser&aacute; encerrado e os seus pontos YETZ acumulados
                                          ser&atilde;o cancelados, n&atilde;o se constituindo direito de
                                          heran&ccedil;a.<br /><br />4.2.5 O saldo vinculado ao cart&atilde;o pode ser
                                          consultado a cada acesso &agrave; plataforma.<br /><br />4.2.6 Os pontos YETZ
                                          creditados nos cart&otilde;es YETZ CARD possuem prazo determinado conforme
                                          regras estabelecidas por cada empresa contratante.<br /><br />4.2.7 Caso o
                                          total dos pontos do cart&atilde;o n&atilde;o seja utilizado at&eacute; a data
                                          de vencimento, o participante perder&aacute; seu direito aos pontos e
                                          n&atilde;o poder&aacute; solicitar reembolso de nenhuma
                                          esp&eacute;cie.<br /><br />4.2.8 N&atilde;o haver&aacute; incid&ecirc;ncia de
                                          qualquer tipo de juros, remunera&ccedil;&atilde;o ou b&ocirc;nus sobre a
                                          quantidade de pontos YETZ que cada participante possui e em nenhuma
                                          hip&oacute;tese, estes poder&atilde;o ser substitu&iacute;dos por valores em
                                          Real.<br /><br />4.2.9 A contratada se reserva o direito de invalidar pontos
                                          YETZ disponibilizados no cart&atilde;o por determina&ccedil;&atilde;o da
                                          contratante.<br /><br />4.2.10 Os pontos dos cart&otilde;es YETZ CARD
                                          n&atilde;o s&atilde;o cumulativos, n&atilde;o podendo ser somado pontos de
                                          dois diferentes cart&otilde;es para resgate &uacute;nico.<br /><br />4.2.11
                                          N&atilde;o existe saldo remanescente, ou seja, a possibilidade de resgatar
                                          saldo de pontos de cart&otilde;es que n&atilde;o tenham sido utilizados na sua
                                          totalidade.</li>
                              </ol>
                              <p>&nbsp;</p>
                              <ol start="5">
                                    <li><strong>RESGATE<br /><br /></strong>5.1 O participante dever&aacute; conferir se
                                          tem a quantidade suficiente de pontos YETZ para os cart&otilde;es presente
                                          e/ou vouchers digitais que deseja resgatar. Sempre que o term&ocirc;metro
                                          estiver verde, aquele cart&atilde;o e/ou voucher pode ser resgatado, porque
                                          significa que os pontos YETZ s&atilde;o suficientes.<br /><br />5.2 No ato do
                                          resgate o participante escolhe o tipo de pr&ecirc;mio: cart&otilde;es presente
                                          ou vouchers digitais. Cart&otilde;es Presente: atrav&eacute;s de cart&atilde;o
                                          f&iacute;sico, que ser&aacute; entregue no local determinado pela contratante
                                          e conforme cadastrado na plataforma, em at&eacute; 30 dias Vouchers Digitais:
                                          atrav&eacute;s de voucher digital, enviado para o endere&ccedil;o de e-mail
                                          cadastrado na plataforma, em at&eacute; 15 dias.<br /><br />5.3 A empresa
                                          contratante poder&aacute; optar por uma &uacute;nica forma de premiar suas
                                          equipes: ou somente atrav&eacute;s de cart&otilde;es presente ou vouchers
                                          digitais. Nestes casos as op&ccedil;&otilde;es n&atilde;o permitidas
                                          ser&atilde;o bloqueadas na plataforma.<br /><br />5.4 Na sequ&ecirc;ncia
                                          dever&aacute; ser escolhido como prefere resgatar o pr&ecirc;mio.
                                          Op&ccedil;&atilde;o 1: atrav&eacute;s de loja f&iacute;sica, com &iacute;cone
                                          de loja f&iacute;sica em vinho e o usu&aacute;rio dever&aacute; se dirigir
                                          para a loja escolhida para aquisi&ccedil;&atilde;o do pr&ecirc;mio.
                                          Op&ccedil;&atilde;o 2: atrav&eacute;s de e-commerce, compras
                                          eletr&ocirc;nicas, indicadas atrav&eacute;s de &iacute;cone laranja. A
                                          presen&ccedil;a dos dois &iacute;cones significa que ambas
                                          op&ccedil;&otilde;es poder&atilde;o ser utilizadas.<br /><br />5.5 Existem
                                          duas formas de pedir o resgate: a primeira &eacute; clicar sobre o
                                          cart&atilde;o e/ou voucher escolhido, ler as descri&ccedil;&otilde;es e clicar
                                          em adicionar ao carrinho. A outra &eacute; clicar no carrinho abaixo do
                                          cart&atilde;o/voucher que deseja adquirir.<br /><br />5.6 Depois de todos os
                                          produtos selecionados, basta clicar no &ldquo;carrinho&rdquo;, localizado na
                                          parte superior direita da home e em seguida &ldquo;visualizar carrinho&rdquo;
                                          para abrir a p&aacute;gina com informa&ccedil;&otilde;es dos pr&ecirc;mios a
                                          serem resgatados.<br /><br />5.7 Antes de finalizar o resgate confira todos os
                                          itens, cart&atilde;o/voucher, pontos e quantidade.<br /><br />5.8 O
                                          participante de uma CAMPANHA deve conferir o endere&ccedil;o de entrega
                                          informado pelo contratante e para onde ser&atilde;o enviados os cart&otilde;es
                                          resgatados, no caso de cart&atilde;o f&iacute;sico.<br /><br />5.9 Para
                                          vouchers digitais o endere&ccedil;o de e-mail deve ser conferido, e este
                                          ser&aacute; o cadastrado pelo usu&aacute;rio e/ou pela empresa
                                          contratante.<br /><br />5.10 O participante de YETZ CARD dever&aacute;, no
                                          momento do resgate, escolher a op&ccedil;&atilde;o de endere&ccedil;o para
                                          entrega dentre as que lhe ser&atilde;o apresentadas.<br /><br />5.11 A
                                          op&ccedil;&atilde;o &ldquo;RESGATAR&rdquo; ir&aacute; efetivar o
                                          resgate.<br /><br />5.12 Na janela aberta, o USU&Aacute;RIO poder&aacute;
                                          conferir os pontos a serem deduzidos de sua conta e, estando de acordo,
                                          clicar&aacute; em finalizar resgate, iniciando o processo de
                                          entrega.<br /><br />5.13 Ap&oacute;s realizado o resgate, caso o cart&atilde;o
                                          presente e/ou voucher digital solicitado venha a ser eliminado da plataforma,
                                          qualquer que seja o motivo, o resgate ser&aacute; cancelado e os respectivos
                                          pontos YETZ ser&atilde;o creditados no saldo do participante.<br /><br />5.14
                                          A contratada se reserva o direito de invalidar resgates realizados, se tiver
                                          conhecimento de que n&atilde;o foram cumpridas as regras constantes na
                                          Campanha ou por determina&ccedil;&atilde;o da contratante.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="6">
                                    <li><strong>PRAZOS E ENTREGA DOS CART&Otilde;ES PRESENTE E/OU VOUCHERS DIGITAIS
                                                RESGATADOS<br /><br /></strong>6.1 O prazo de entrega depender&aacute;
                                          da forma definida pelo usu&aacute;rio para receber os cart&otilde;es:
                                          Op&ccedil;&atilde;o 1: atrav&eacute;s de cart&atilde;o f&iacute;sico, que
                                          ser&aacute; entregue no local de trabalho, no endere&ccedil;o cadastrado na
                                          plataforma, em at&eacute; 30 dias. Op&ccedil;&atilde;o 2: atrav&eacute;s de
                                          voucher digital enviado para o endere&ccedil;o de e-mail cadastrado na
                                          plataforma, em at&eacute; 15 dias.<br /><br />6.2 A Contratada n&atilde;o se
                                          responsabiliza por atrasos oriundos de problemas alheios &agrave; sua
                                          gest&atilde;o, como greve dos CORREIOS, greves em geral, enchentes,
                                          inunda&ccedil;&otilde;es ou quaisquer outros impedimentos derivados de
                                          for&ccedil;as da natureza, pelo tempo que o problema persistir<br /><br />6.3
                                          Os cart&otilde;es ser&atilde;o desbloqueados ap&oacute;s 48 (quarenta e oito)
                                          horas da confirma&ccedil;&atilde;o da entrega dos mesmos &agrave; pessoa
                                          indicada pela empresa contratante.<br /><br />6.4 Importante observar se a
                                          embalagem tem sua integridade mantida e em caso de qualquer sinal de
                                          viola&ccedil;&atilde;o n&atilde;o dever&aacute; aceitar a
                                          entrega.<br /><br />6.5 Os participantes de CAMPANHAS receber&atilde;o seus
                                          cart&otilde;es resgatados no endere&ccedil;o determinado pela empresa
                                          contratante e confirmado por ele no momento do resgate. A entrega ao
                                          USU&Aacute;RIO ser&aacute; efetuada por um respons&aacute;vel determinado pela
                                          contratante, ap&oacute;s assinatura de protocolo de recebimento do
                                          participante. &Eacute; responsabilidade do respons&aacute;vel determinado pela
                                          contratante garantir a exist&ecirc;ncia de pessoa respons&aacute;vel, maior de
                                          18 anos e portadora de documento para receber a premia&ccedil;&atilde;o e
                                          assinar o documento de recebimento. Caso n&atilde;o seja poss&iacute;vel a
                                          entrega ap&oacute;s 3(tr&ecirc;s) tentativas, a premia&ccedil;&atilde;o
                                          retornar&aacute; para a base e haver&aacute; um novo c&aacute;lculo de prazo
                                          de entrega.</li>
                              </ol>
                              <p>&nbsp;</p>
                              <ol start="7">
                                    <li><strong>POL&Iacute;TICA DE CANCELAMENTO<br /><br /></strong>7.1 O cancelamento
                                          de resgate efetuado na Plataforma YETZ CARDS somente poder&aacute; ser
                                          solicitado e realizado at&eacute; 48 horas &uacute;teis ap&oacute;s o
                                          resgate.<br /><br />7.2 O usu&aacute;rio dever&aacute; enviar sua
                                          solicita&ccedil;&atilde;o de resgates efetuados atrav&eacute;s do Fale
                                          Conosco.<br /><br />7.3 Toda solicita&ccedil;&atilde;o de CANCELAMENTO
                                          passar&aacute; por an&aacute;lise da equipe YETZ e poder&aacute; ou n&atilde;o
                                          ter a aprova&ccedil;&atilde;o da contratante.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="8">
                                    <li><strong>CART&Otilde;ES PRESENTE E VOUCHERS DIGITAIS<br /><br /></strong>8.1 A
                                          Plataforma oferece cart&otilde;es presente e/ou vouchers digitais que podem
                                          ser utilizados nas respectivas lojas ou em qualquer estabelecimento mediante
                                          condi&ccedil;&otilde;es de uso descritas na plataforma.<br /><br />8.2 Os
                                          cart&otilde;es e/ou vouchers est&atilde;o distribu&iacute;dos em diversas
                                          categorias, que poder&atilde;o ser inclu&iacute;das ou eliminadas a qualquer
                                          momento, de acordo com negocia&ccedil;&otilde;es com nossos parceiros, sem que
                                          possa haver reinvindica&ccedil;&atilde;o do cart&atilde;o/voucher que foi
                                          eliminado, inclusive se este for encaminhado para a lista de desejos, que se
                                          constitui de um mero mecanismo de controle para o
                                          usu&aacute;rio.<br /><br />8.3 Caso um determinado cart&atilde;o/voucher seja
                                          solicitado e haja impossibilidade de atender o resgate por quaisquer motivos,
                                          incluindo indisponibilidade da loja, o resgate ser&aacute; cancelado e os
                                          pontos retornar&atilde;o para o usu&aacute;rio, sem que esse possa reivindicar
                                          algum tipo de ressarcimento &agrave; YETZ.<br /><br />8.4 Em cada
                                          cart&atilde;o/voucher acessado na Plataforma constam: descri&ccedil;&atilde;o,
                                          validade, informa&ccedil;&otilde;es importantes e regras de uso e &eacute; de
                                          responsabilidade do USU&Aacute;RIO ler e tomar conhecimento de todas elas, bem
                                          como ler as instru&ccedil;&otilde;es que s&atilde;o enviadas junto de cada
                                          cart&atilde;o/voucher.<br /><br />8.5 &Eacute; necess&aacute;rio o
                                          fornecimento do CPF no ato cadastro e desbloqueio de cart&otilde;es
                                          segmentados bandeirados.<br /><br />8.6 Na &aacute;rea de STREAMING
                                          est&atilde;o dispon&iacute;veis diversos conte&uacute;dos em v&iacute;deo e
                                          &aacute;udio. Podem ser mensagens, comunicados ou dicas de sua empresa,
                                          relacionados &agrave; campanha para melhorar ainda mais sua performance ou
                                          tutoriais que facilitam o uso da Plataforma.<br /><br />8.7
                                          NOTIFICA&Ccedil;&Otilde;ES &eacute; um espa&ccedil;o dedicado &agrave;
                                          comunica&ccedil;&atilde;o com os participantes. Utilizado para enviar alertas,
                                          lembretes de datas importantes, conte&uacute;dos e mensagens sobre a
                                          Campanha.<br /><br />8.8 Atrav&eacute;s de SMS, para o n&uacute;mero do
                                          celular informado e autorizado pelo usu&aacute;rio no momento do cadastro,
                                          s&atilde;o enviados recados importantes, sejam da contratante ou da YETZ. O
                                          usu&aacute;rio pode alterar o status de recebimento em MEU
                                          PERFIL.<br /><br />8.9 O CHAT, &eacute; um canal online para ajuda,
                                          esclarecimento de d&uacute;vidas ou sugest&otilde;es.<br /><br />8.10 A
                                          ativa&ccedil;&atilde;o das facilidades da plataforma como Streaming,
                                          Notifica&ccedil;&otilde;es, SMS, Popup, dentre outras, s&atilde;o definidas
                                          pela empresa contratante e aplicadas para seus colaboradores.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="9">
                                    <li><strong>UTILIZA&Ccedil;&Atilde;O DOS CART&Otilde;ES PRESENTE E/OU VOUCHERS
                                                DIGITAIS RESGATADOS<br /><br /></strong>9.1 Ap&oacute;s receber o
                                          resgate, qualquer eventualidade, incluindo desbloqueio, emiss&atilde;o de
                                          segunda via, saldo remanescente, vencidos, etc., devem ser tratados
                                          diretamente entre USU&Aacute;RIO e a empresa emissora do
                                          cart&atilde;o/voucher, n&atilde;o sendo a YETZ respons&aacute;vel por tais
                                          quest&otilde;es.<br /><br />9.2 Em cada cart&atilde;o/voucher acessado na
                                          Plataforma constam: descri&ccedil;&atilde;o, validade,
                                          informa&ccedil;&otilde;es importantes e regras de uso e &eacute; de
                                          responsabilidade do USU&Aacute;RIO ler e tomar conhecimento de todas elas, bem
                                          como ler as instru&ccedil;&otilde;es que s&atilde;o enviadas junto de cada
                                          cart&atilde;o/voucher.<br /><br />9.3 Por se tratar de cart&atilde;o/voucher
                                          ao portador, em caso de roubo, perda ou extravio n&atilde;o ser&aacute;
                                          pass&iacute;vel de substitui&ccedil;&atilde;o, sendo a guarda de
                                          responsabilidade do portador.<br /><br />9.4 Fica a crit&eacute;rio e
                                          responsabilidade do USU&Aacute;RIO no ato da compra, negociar valores e/ou
                                          complementar de outras formas o pagamento do produto/servi&ccedil;o adquirido,
                                          desde que a loja f&iacute;sica ou virtual aceite tal complemento. Da mesma
                                          forma &eacute; necess&aacute;rio verificar com cada loja a pol&iacute;tica de
                                          saldo remanescente no cart&atilde;o/voucher.<br /><br />9.5 &Eacute; vedado o
                                          uso do cart&atilde;o/voucher para qualquer atividade il&iacute;cita, ficando o
                                          participante, em caso de comprova&ccedil;&atilde;o, sujeito &agrave;
                                          sans&otilde;es da lei.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="10">
                                    <li><strong>COMUNICA&Ccedil;&Atilde;O COM A PLATAFORMA YETZ<br /><br /></strong>10.1
                                          &Eacute; crit&eacute;rio da empresa contratante a forma do USU&Aacute;RIO de
                                          dirimir d&uacute;vidas sobre o uso da Plataforma, os cart&otilde;es, vouchers,
                                          fazer reclama&ccedil;&otilde;es ou qualquer outra comunica&ccedil;&atilde;o
                                          necess&aacute;ria. Esta comunica&ccedil;&atilde;o pode ser, depois de
                                          consultadas D&uacute;vidas Frequentes, atrav&eacute;s do espa&ccedil;o Fale
                                          Conosco na Plataforma ou, caso n&atilde;o tenha essa op&ccedil;&atilde;o,
                                          diretamente com o superior imediato do USU&Aacute;RIO.<br /><br />10.2
                                          Assuntos relacionados &agrave; Campanha, pontos e crit&eacute;rios devem ser
                                          tratados sempre com o superior do USU&Aacute;RIO.<br /><br />10.3 As regras da
                                          Campanha, estipuladas pela contratante, s&atilde;o informadas em Minhas
                                          Campanhas, na barra central da Plataforma YETZ CARDS.<br /><br />10.4 O
                                          participante concordar&aacute;, ao efetuar o cadastro, que a Plataforma YETZ
                                          CARDS envie, sempre que necess&aacute;rio, comunica&ccedil;&otilde;es
                                          atrav&eacute;s de e-mail ou SMS informados no cadastramento,
                                          fun&ccedil;&atilde;o que pode ter o status de recebimento alterado conforme
                                          desejo do usu&aacute;rio. A empresa contratante pode determinar normas
                                          diferenciadas para suas Campanhas.<br /><br />10.5 No ato do cadastramento, o
                                          USU&Aacute;RIO define no campo apelido como gostaria de ser chamado na
                                          comunica&ccedil;&atilde;o entre a Plataforma e ele.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="11">
                                    <li><strong>POL&Iacute;TICA DE PRIVACIDADE<br /><br /></strong>11.1 A <strong>YETZ
                                                INCENTIVO LTDA</strong>, administradora da Plataforma <strong>YETZ
                                                CARDS</strong>, tem o compromisso de respeitar a privacidade e o sigilo
                                          das informa&ccedil;&otilde;es pessoais e demais dados acessados ou
                                          compartilhados pela contratante e participantes da Plataforma.<br /><br />11.2
                                          As empresas contratantes podem, a qualquer momento, solicitar
                                          informa&ccedil;&otilde;es de saldos e valores de resgates dos participantes
                                          inseridos em suas campanhas, mas n&atilde;o ser&aacute; permitido o acesso a
                                          informa&ccedil;&otilde;es sobre produtos ou servi&ccedil;os resgatados por
                                          cada participante.<br /><br />11.3 Ao se cadastrarem, os participantes
                                          concordam expressamente que todas suas informa&ccedil;&otilde;es possam ser
                                          armazenadas e utilizadas pelo YETZ, no contexto da campanha da qual participa,
                                          com vistas ao processamento de dados e estat&iacute;sticas, desenvolvimento de
                                          a&ccedil;&otilde;es promocionais, oferecimento de produtos ou servi&ccedil;os,
                                          identifica&ccedil;&atilde;o e ainda tratamento de fraudes ou outras atividades
                                          ilegais, inclusive como parte de investiga&ccedil;&atilde;o. Essa
                                          autoriza&ccedil;&atilde;o &eacute; extensiva &agrave; empresa contratante da
                                          plataforma, gestora da campanha, neste caso de forma coletiva, sem
                                          individualiza&ccedil;&atilde;o dos participantes<br /><br />11.4 Ao
                                          t&eacute;rmino da campanha todas as informa&ccedil;&otilde;es dos
                                          usu&aacute;rios participantes ser&atilde;o retiradas da base de dados ativa,
                                          ficando arquivadas em forma de back-up apenas para fins de auditoria, por um
                                          per&iacute;odo de 5 anos.<br /><br />11.5 O YETZ se reserva o direito de
                                          excluir da plataforma, com ou sem orienta&ccedil;&atilde;o da contratante e
                                          sem concord&acirc;ncia ou aviso pr&eacute;vio ao participante, bem como
                                          cancelar seus pontos YETZ e/ou eventuais resgates em curso quando houver
                                          ind&iacute;cio de qualquer irregularidade por parte do participante,
                                          independentemente de serem tomadas as medidas judiciais.<br /><br />11.6 As
                                          senhas dos participantes das campanhas s&atilde;o de uso pessoal e exclusivo,
                                          sendo de responsabilidade do usu&aacute;rio mant&ecirc;-la em
                                          seguran&ccedil;a, sigilo e confidencialidade, n&atilde;o revelando, divulgando
                                          ou fornecendo a terceiros.<br /><br />11.7 O n&uacute;mero do cart&atilde;o e
                                          respectivo PIN dos participantes da modalidade YETZ CARD s&atilde;o de uso
                                          pessoal e exclusivo, sendo responsabilidade do usu&aacute;rio mant&ecirc;-lo
                                          em seguran&ccedil;a, sigilo e confidencialidade, n&atilde;o revelando,
                                          divulgando ou fornecendo a terceiros.<br /><br />11.8 O YETZ recomenda que o
                                          participante apenas acesse sua conta por meio de computadores, celulares,
                                          tabletes e outros em ambiente seguro, evitando qualquer forma de risco e
                                          favorecimento a fraudes e atrav&eacute;s do uso de antiv&iacute;rus,
                                          anti-spywares, adwares e afins. Sempre e quando autorizado pelas empresas
                                          contratantes o acesso deve ser feito, preferencialmente, no ambiente de
                                          trabalho.<br /><br />11.9 Caso seja definido pela contratante poder&aacute;
                                          ser determinado como &uacute;nico acesso &agrave; plataforma os computadores
                                          do local de trabalho, o que ser&aacute; informado aos usu&aacute;rios
                                          atrav&eacute;s de comunica&ccedil;&atilde;o da empresa.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="12">
                                    <li><strong>CONFIGURA&Ccedil;&Otilde;ES DE USO<br /><br /></strong>12.1 A Plataforma
                                          YETZ CARDS em desktops e notebooks &eacute; melhor visualizada na
                                          resolu&ccedil;&atilde;o 1920x1080 pixels e &eacute; compat&iacute;vel com
                                          navegadores: Internet Explorer 11 ou superior, Mozilla Firefox 25 e Google
                                          Chrome 31 ou seus superiores.<br /><br />12.2 O aplicativo YETZ CARDS para
                                          smartphones ou tablets deve ser visualizado nas Plataformas iOS 9.0 e Android
                                          4.0 ou superiores.<br /><br />12.3 Caber&aacute; ao usu&aacute;rio instalar o
                                          navegador ou sistema operacional necess&aacute;rio para a correta
                                          visualiza&ccedil;&atilde;o da Plataforma YETZ CARDS.<br /><br />12.4YETZ CARDS
                                          faz uso de pop-ups, devendo o usu&aacute;rio desabilitar o bloqueador de
                                          pop-ups existente em seu browser.<br /><br />12.5 Para a adequada
                                          utiliza&ccedil;&atilde;o da Plataforma, o usu&aacute;rio dever&aacute; estar
                                          conectado &agrave; internet mediante Wifi, 3G ou 4G.<br /><br />12.6 A YETZ
                                          n&atilde;o se responsabiliza por nenhum tipo de custo, tarifas, taxas ou
                                          impostos que o participante pague para acessar a Plataforma YETZ CARDS
                                          atrav&eacute;s da WEB ou de APP.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="13">
                                    <li><strong>POL&Iacute;TICA DE COOKIES<br /><br /></strong>13.1 YETZ CARDS pode
                                          empregar cookies e tecnologias relacionadas para entender como a plataforma
                                          &eacute; acessada, preven&ccedil;&atilde;o de fraudes, as prefer&ecirc;ncias
                                          sobre p&aacute;ginas, pr&ecirc;mios e fun&ccedil;&otilde;es,
                                          intera&ccedil;&otilde;es dos usu&aacute;rios com banners,
                                          notifica&ccedil;&otilde;es e v&iacute;deos para customiza&ccedil;&atilde;o de
                                          pr&ecirc;mios e ofertas e perman&ecirc;ncia da sess&atilde;o do USU&Aacute;RIO
                                          v&aacute;lida.<br /><br />13.2 A plataforma n&atilde;o utiliza cookies para
                                          personalizar ou enviar an&uacute;ncios de terceiros.<br /><br />13.3 A
                                          plataforma YETZ CARDS utiliza cookies do navegador, onde o USU&Aacute;RIO pode
                                          alterar suas configura&ccedil;&otilde;es de privacidade.</li>
                              </ol>
                              <p><strong>&nbsp;</strong></p>
                              <ol start="14">
                                    <li><strong>OUTRAS DISPOSI&Ccedil;&Otilde;ES<br /><br /></strong>14.1 O participante
                                          reconhece o direito do YETZ de efetuar ajustes ou altera&ccedil;&otilde;es na
                                          plataforma, em qualquer que seja a se&ccedil;&atilde;o, sempre que julgar
                                          necess&aacute;rio.<br /><br />14.2 A plataforma se exime de qualquer
                                          responsabilidade relacionada a par&acirc;metros da campanha, acatando
                                          integralmente as informa&ccedil;&otilde;es recebidas pela empresa contratante
                                          na troca de arquivos com respectiva pontua&ccedil;&atilde;o.<br /><br />14.3 A
                                          YETZ se reserva o direito de ajustar, eliminar, incluir, a qualquer momento,
                                          as condi&ccedil;&otilde;es deste Termos e Condi&ccedil;&otilde;es de Uso,
                                          sempre que houver motivo que justifique, ajustando a data da vers&atilde;o em
                                          vigor, sendo responsabilidade do participante acompanhar regularmente este
                                          Termos e Condi&ccedil;&otilde;es de Uso e verificar eventuais
                                          modifica&ccedil;&otilde;es.<br /><br />14.4 YETZ n&atilde;o &eacute; uma
                                          institui&ccedil;&atilde;o financeira e deste modo os pontos YETZ n&atilde;o
                                          s&atilde;o moeda financeira e nem unidades de moeda em Real.<br /><br />14.5 O
                                          YETZ n&atilde;o ser&aacute; considerado em mora ou inadimplente de quaisquer
                                          de suas obriga&ccedil;&otilde;es previstas neste Termos e
                                          Condi&ccedil;&otilde;es de Uso da Plataforma YETZ CARDS, se o fato
                                          desencadeador de seu descumprimento decorrer de caso fortuito ou for&ccedil;a
                                          maior, na forma estabelecida pelo C&oacute;digo Civil Brasileiro (Lei n&ordm;
                                          10.406/2002).<br /><br />14.6 As marcas das bandeiras de parceiros s&atilde;o
                                          de propriedade de seus titulares e n&atilde;o s&atilde;o licenciadas, cedidas
                                          ou de outra forma transferidas a quem quer que seja.<br /><br />14.7 Este
                                          Termos e Condi&ccedil;&otilde;es de Uso se aplica ao acesso do participante
                                          &agrave; Plataforma YETZ CARDS atrav&eacute;s de quaisquer meios
                                          eletr&ocirc;nicos, como: computador, celulares, tablets e outros semelhantes,
                                          incluindo aplicativo YETZ CARDS destinado ao mesmo fim e com conte&uacute;do
                                          adaptado para a m&iacute;dia, mas conservando a mesma finalidade e
                                          informa&ccedil;&otilde;es.<br /><br />14.8 Toda e qualquer
                                          situa&ccedil;&atilde;o n&atilde;o prevista neste Termos e
                                          Condi&ccedil;&otilde;es de Uso, bem como eventuais casos omissos ser&atilde;o
                                          decididos, exclusivamente e soberanamente, pelo YETZ.<br /><br />14.9 Fica
                                          permanentemente vedado ao usu&aacute;rio copiar, reproduzir, distribuir ou
                                          divulgar o layout ou qualquer conte&uacute;do existente no website da
                                          Plataforma de Recompensas YETZ CARDS, sem pr&eacute;via
                                          autoriza&ccedil;&atilde;o, sob pena de responsabiliza&ccedil;&atilde;o civil
                                          e/ou criminal.</li>
                              </ol>
                              <p>O presente Termos e Condi&ccedil;&otilde;es de Uso ser&aacute; interpretado segundo a
                                    legisla&ccedil;&atilde;o brasileira e encontram-se devidamente registrados perante o
                                    Of&iacute;cio de Registro de T&iacute;tulos e Documentos da Cidade de S&atilde;o
                                    Paulo. Permanecer&aacute; v&aacute;lido indeterminadamente at&eacute; que seja
                                    substitu&iacute;do, a crit&eacute;rio da <strong>YETZ INCENTIVO LTDA</strong>, por
                                    novo regulamento devidamente registrado.</p>
                        </div>
                  </div>
            </div>
      </div>
</section>

<div class="xlg-margin3x"></div>
@stop