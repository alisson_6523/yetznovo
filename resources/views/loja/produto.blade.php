@extends('layout.layout-loja')


@section('title', 'Detalhes do Produto')

@section('conteudo-loja')

@section('styles')
    <link href="/css/usuario_antigo/personalizado-detalhes-produto.css" rel="stylesheet">
@stop



@php
    $desejado = $produto->styleDesejados($usuario->desejados);

    $termometro = $produto->termometro(round($usuario->saldo_usuario), $carrinho->valorCarrinho);

@endphp

<input type="hidden" value="{{ $produto->cod_produto }}" id="cod-produto"/>

<section id="topo-pagina">
    <div class="container">
        <div class="conteudo-detalhes-produto">

            <div class="row">

                <div class="col-xs-12 col-sm-12 col-md-6 container-produto">
                    <div class="produto-destaque-grande">
                        <a title="{{ $desejado['txt'] }}" class="btn-detalhes favorito-detalhe-produto lampada-grande {{ $desejado['like'] }}" onclick="favorito('{{ $produto->cod_produto }}', this);">
                            <i class="icone-icon_lamp"></i>
                        </a>
                        <div class="conteudo-imagem">
                            <img src="{{ $produto->link_foto }}" title="{{ $produto->nome_produto }} - R${{ $produto->valor_reais }}">
                            <div class="preco-produto">
                                <h3>
                                    <span>R$</span>{{ $produto->valor_reais }}
                                </h3>
                            </div>
                        </div>
                        <p class="obs-img"> *Imagens meramente ilustrativas</p>

                        <div class="area-selos">
                            @foreach ($produto->selos as $s)
                                <div class="selo {{ $s['class'] }}">
                                    <span><img src="{{ $s['icone'] }}" alt=""></span>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6">
                    <div class="descricao-produto {{ $termometro['classe_nova'] }}">
                        <div class="valor-produto">
                            <div class="progress barra-progresso">
                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:{{ $termometro['result'] }}%">
                                    <span class="sr-only">{{ $termometro['result'] }}% Complete</span>
                                </div>
                            </div>
                            <h1 class="saldo">{{ $produto->valor }} YETZ</h1>
                        </div>
                        <h2>Saldo: <strong>{{ number_format($usuario->saldo_usuario, 0, ',', '.') }} YETZ</strong></h2>

                        <div class="texto-alternativo">
                            <div class="msg" style="padding-bottom: 15px;">
                                <div class="icone"></div>
                                <div class="texto">
                                    <h3 class="text-upppercase">{{ $termometro['titulo_texto'] }}</h3>
                                    <p class="text-upppercase">{{ $termometro['texto'] }}</p>
                                </div>
                            </div>
                            <button type="button" class="obter-produto btn-adiciona-produto" cod-item="{{ $produto->cod_produto }}">Adicionar ao carrinho</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 box-informacoes-produto">
                    <div class="item-desc-produto">
                        <h3>DESCRIÇÃO DO PRODUTO</h3>
                        <p>{{ $produto->descricao_produto }}</p>
                    </div>
                    <div class="item-desc-produto" id="regras-utilizacao">
                        <h3>REGRAS DE UTILIZAÇÃO</h3>
                        <p>{{ $produto->detalhes_produto }}</p>
                    </div>
                    <div class="item-desc-produto">
                        <h3>INFORMAÇÕES IMPORTANTES</h3>
                        <p>{{ $produto->informacoes_importantes }}</p>
                    </div>
                    <div class="item-desc-produto">
                        <h3>VALIDADE</h3>
                        <p>{{ $produto->validade_produto }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="produtos-similares">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="titulo-similares">
                    <p>cartões <span>relacionados</span></p>
                </div>
            </div>
            <div class="prod-destaques">
                @foreach ($relacionados as $similar)
                @php
                    $termometro = $similar->termometro(round($usuario->saldo_usuario), $carrinho->valorCarrinho);
                    $desejados  = $similar->styleDesejados($usuario->desejados);
                @endphp
                    <div class="box-produto">
                        <div class="informacoes-produto">
                            <h1 class="sr-only">{{ $similar->nome_produto }}</h1>
                            <a href="/loja/produto/{{ $similar->cod_produto }}">
                                <img src="{{ $similar->link_foto }}" alt="{{ $similar->nome_completo }}" title="{{ $similar->nome_completo }}"  class="img-responsive">
                            </a>
                            <a title="{{ $desejados['txt'] }}" class="btn-detalhes btn-favoritar {{ $desejados['like'] }}" onclick="favorito('{{ $similar->cod_produto }}', this);">
                                <i class="icone-icon_lamp"></i>
                            </a>
                            <div class="preco-produto">
                                <h3>
                                    <span>R$</span>{{ $similar->valor_reais }}
                                </h3>
                            </div>
                        </div>
                        <div class="informacoes">
                            <div class="area-selos">

                                @foreach ($similar->selos as $s)
                                <div class="selo {{ $s['class'] }}">
                                    <span><img src="{{ $s['icone'] }}" alt=""></span>
                                </div>
                                @endforeach
                            </div>
                            <div class="area-valores {{ $termometro['classe_nova'] }}">
                                <div class="valores">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $termometro['result'] >= 0 ? $termometro['result'] : 0  }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}%">
                                            <span class="sr-only">{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}% Complete</span>
                                        </div>
                                    </div>
                                    <h1 class="saldo">{{ $similar->valor }}<span> YETZ</span></h1>
                                </div>
                                <a title="{{ $termometro['result'] >= 100 ? "Adicionar esse cartão ao carrinho" : "Você não possui pontos suficientes para resgatar esse item" }}" class="btn-adiciona-produto" cod-item="{{ $similar->cod_produto }}" >
                                    <span>adicionar</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

</section>
<!-- Modal -->
<div id="modalObterProduto" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Conteudo Modal-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><img src="/img/close.png"></button>
            </div>
            <div class="modal-body">
                <div class="foto-produto">
                    <img src="{{ $produto->link_foto }}">
                </div>
                <div class="conteudo-imagem-modal">
                    <h2>{{ $produto->nome_produto }}</h2>
                    <div class="listaProdutos">
                        <table class="table table-bordered">

                        </table>
                    </div>
                </div>
                <form action="/loja/resgate"  method="post">
                    <div class="fundo-endereco" style="display:none;">
                        <div class="centraliza-btn-endereco">
                            <button class="btn-collapsed" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true" aria-controls="collapseExample">
                                Desejo receber em outro endereço
                            </button>
                        </div>
                        <div class="collapse" id="collapseExample">
                            <div class="col-md-6">
                                <input type="text" id="cep" class="esq" name="cep" placeholder="Digite o CEP" onkeydown="mascara(this, validaCep)" style="width: 110px;">
                                <button type="button" class="btn btn-primary btnProcuraCep" style="margin-left: 10px; margin-top: 4px;">Procurar</button>
                                <div id="carregando" style="visibility: hidden"><img src="/img/loader.gif"></div>
                                <input type="text" id="bairro" name="bairro" placeholder="Digite o Bairro">
                                <input type="text" id="estado" class="esq" readonly="true" name="sigla_estado" placeholder="Digite o Estado">
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="logradouro" name="logradouro" placeholder="Digite o Logradouro" >
                                <input type="text" id="cidade" class="esq" readonly="true" name="cidade" placeholder="Digite a Cidade">
                                <input type="text" id="numero" name="numero" placeholder="Número" onkeydown="mascara(this, apenasNumeros)" style="width: 75px; margin-right: 15px;">
                                <input type="text" id="complemento" name="complemento" placeholder="Complemento" style="width: 130px;">

                                <input type="hidden" id="cod_endereco" name="cod_endereco">
                            </div>
                        </div>
                    </div>

                    <div class="footerDoModal modal-footer">
                        <div class="botoesDoModal">
                            <button type="button" data-dismiss="modal" class="btn btn-danger botaoCancelaTrocaModal classeDosBotoesDoModal">CANCELAR</button>

                            <input value="{{ $usuario->cod_usuario }}" type="hidden" name="cod_usuario">
                            <input value="{{ $produto->cod_produto }}" type="hidden" name="cod_produto">
                            <button class="botaoConfirmarTrocaModal classeDosBotoesDoModal btn btn-success" id="confirmaCompraProduto" data-toggle="modal" data-target="#md-normal" type="submit">TROCAR</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

<!--Modal Regras de Utilização-->
<div class="modal fade" id="modal-regras-utilizacao" tabindex="0" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Regras de Uso</h4>
            </div>
            <div class="modal-body sombra-topo">
                <div class="coluna-inteira no-padding">
                    <p>{{ $produto->detalhes_produto }}</p>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>

@stop

@section('scripts')
    <script src="{{ asset('js/usuario_antigo/loja/detalhes-produto.js') }}"></script>
@stop
