@extends('layout.layout-loja')

@section('title', 'Home')

@section('conteudo-loja')

    <!--Começo da SECTION Banner-->
    <section class="banner">
        <div class="container">
            <div class="setas">
                <span class="seta-esq1 icon-seta-esq"><img src="/img/seta-esq-slide-principal.png"></span>
                <span class="seta-dir1 icon-seta-dir"><img src="/img/seta-dir-slide-principal.png"></span>
            </div>
        </div>
        <!-- Swiper -->
        <div class="banner-area swiper-container">
            <div class="swiper-wrapper">
                @foreach ($banners as $banner)
                    <div class="swiper-slide">
                        <img src="{{ $banner->link }}" class="img-responsive"
                            alt="{{ $banner->titulo }}">
                    </div>
                @endforeach
            </div>
        </div>
    </section>


    <!--Fim da SECTION Banner-->
    <!--Começo da SECTION Acesso Banner-->
    <section class="acesso-site">
        <div class="container">
            <div class="row">
                <div class="col-xs-4 col-sm-6 col-md-4 col-lg-4">
                    <a href="{{ action('LojaController@resgates') }}">
                        <div class="extrato-pontos">
                            <img src="/img/icone-mov.png" class="img-responsive">
                            <p>Confira seus<br>
                                <strong>Resgates de Prêmios</strong>
                            </p>
                            <div class="conteudo-botao">
                                <a class="btn-proximo" href="{{ action('LojaController@resgates') }}">
                                    <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-6 col-md-4 col-lg-4">
                    <a href="{{ action('LojaController@movimentacao') }}">
                        <div class="extrato-pontos movimentacao">
                            <img src="/img/icon-extrato.png" class="img-responsive" alt="">
                            <p>Acompanhe seu<br />
                                <strong>Extrato de Pontos Yetz</strong>
                            </p>
                            <div class="conteudo-botao">
                                <a class="btn-proximo" href="{{ action('LojaController@movimentacao') }}"><i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-xs-4 col-sm-12 col-md-4 col-lg-4">
                    <a href="{{ action('LojaController@listaDesejos') }}">
                        <div class="lista-desejos">
                            <img src="/img/icon-desejos.png" class="img-responsive" alt="">
                            <p>Gerencie sua<br>
                                <strong>Lista de Desejos</strong></p>
                            <div class="conteudo-botao">
                                <a class="btn-proximo" href="{{ action('LojaController@listaDesejos') }}"><i class="fa fa-arrow-right"
                                        aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!--Fim da SECTION Acesso Banner-->

    <script>
        var popups = {
            lista_popups: {!! $usuario->popups !!},
            index_atual:0,
            proximo_popup: function(){
                return(this.lista_popups[this.index_atual + 1]);
            }
        }
    </script>

    <script src="{{ asset('js/usuario_antigo/vendor/jquery-1.11.2.min.js') }}"></script>
    <script src="{{ asset('js/usuario_antigo/loja/popup.js') }}"></script>
@stop
