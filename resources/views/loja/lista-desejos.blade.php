@extends('layout.layout-loja')

@section('title', 'Lista de Desejos')


@section('styles')
    <link rel="stylesheet" href="/css/usuario_antigo/personalizado-lista-desejo.css">
@endsection


@section('conteudo-loja')
        <section class="produtos-destaques fundo-cinza">
            <div class="titulo-da-pagina">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <i class="icone-icon_lamp"></i>
                            <h1>Confira sua <strong>Lista de desejos</strong></h1>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                        <div class="prod-destaques">
                            @if (!$usuario->desejados->count())
                                    <h1 class="aviso-vazio">Nenhum produto adicionado na lista! Clique <a href="" class="clique-lista-categorias" data-toggle="modal" data-target="#modalCategoriasGerais">aqui</a> para ver os nossos produtos.</h1>
                            @else
                                @foreach ($usuario->desejados as $p)
                                    @php
                                        $termometro = $p->termometro($usuario->saldo_usuario, $carrinho->valorCarrinho);
                                        $desejados = $p->styleDesejados($usuario->desejados);
                                    @endphp

                                    <div class="box-produto">
                                        <div class="informacoes-produto">
                                            <h1 class="sr-only">{{ $p->nome_produto }}</h1>
                                            <a href='/loja/produto/{{ $p->cod_produto }}' >
                                                <img src="{{ $p->link_foto }}" alt="{{ $p->nome_produto }} - R${{ $p->valor_reais }}" title="{{ $p->nome_completo }}"  class="img-responsive">
                                            </a>
                                            <a title="{{ $desejados['txt'] }}" class="btn-detalhes btn-favoritar {{ $desejados['like'] }}" onclick="favorito('{{ $p->cod_produto }}', this)">
                                                <i class="icone-icon_lamp"></i></a>
                                            <div class="preco-produto">
                                                <h3><span>R$</span>{{ $p->valor_reais }}</h3>
                                            </div>
                                        </div>
                                        <div class="informacoes">
                                            <div class="area-selos">
                                                @foreach ($p->selos as $s)
                                                    <div class="selo {{ $s['class'] }}">
                                                        <span><img src="{{ $s['icone'] }}" alt=""></span>
                                                    </div>
                                                @endforeach

                                            </div>
                                            <div class="area-valores">
                                                <div class="valores">
                                                    <div class="progress {{ $termometro['progresso'] }}">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="{{ $termometro['result'] >= 0 ? $termometro['result'] : 0  }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}%">
                                                            <span class="sr-only {{ $termometro['classe'] }}">{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}% Complete</span>
                                                        </div>
                                                    </div>
                                                    <h1 class="saldo {{ $termometro['classe'] }}">{{ $p->valor }}<span> YETZ</span></h1>
                                                </div>
                                                <a title="{{ $termometro['result'] >= 100 ? "Adicionar esse cartão ao carrinho" : "Você não possui pontos suficientes para resgatar esse item" }}" class="btn-adiciona-produto {{ $termometro['corCarrinho'] }}" cod-item="{{ $p->cod_produto }}" >
                                                    <span>adicionar</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach
                            @endif
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="produtos-visitados">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Produtos visualizados recentemente</h1>
                        </div>
                    </div>
                    <div class="row">
                        @if ($visitados->count() > 0)
                            <div class="categoria">

                                <div class="prod-destaques">
                                        @foreach ($visitados as $p)
                                        @php
                                            $termometro = $p->termometro($usuario->saldo_usuario, $carrinho->valorCarrinho);
                                            $desejados = $p->styleDesejados($usuario->desejados);
                                        @endphp

                                        <div class="box-produto">
                                            <div class="informacoes-produto">
                                                <h1 class="sr-only">{{ $p->nome_produto }}</h1>
                                                <a href='/loja/produto/{{ $p->cod_produto }}' >
                                                    <img src="{{ $p->link_foto }}" alt="{{ $p->nome_produto }} - R${{ $p->valor_reais }}" title="{{ $p->nome_completo }}"  class="img-responsive">
                                                </a>
                                                <a title="{{ $desejados['txt'] }}" class="btn-detalhes btn-favoritar {{ $desejados['like'] }}" onclick="favorito('{{ $p->cod_produto }}', this)">
                                                    <i class="icone-icon_lamp"></i></a>
                                                <div class="preco-produto">
                                                    <h3><span>R$</span>{{ $p->valor_reais }}</h3>
                                                </div>
                                            </div>
                                            <div class="informacoes">
                                                <div class="area-selos">
                                                    @foreach ($p->selos as $s)
                                                        <div class="selo {{ $s['class'] }}">
                                                            <span><img src="{{ $s['icone'] }}" alt=""></span>
                                                        </div>
                                                    @endforeach

                                                </div>
                                                <div class="area-valores">
                                                    <div class="valores">
                                                        <div class="progress {{ $termometro['progresso'] }}">
                                                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $termometro['result'] >= 0 ? $termometro['result'] : 0  }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}%">
                                                                <span class="sr-only {{ $termometro['classe'] }}">{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}% Complete</span>
                                                            </div>
                                                        </div>
                                                        <h1 class="saldo {{ $termometro['classe'] }}">{{ $p->valor }}<span> YETZ</span></h1>
                                                    </div>
                                                    <a title="{{ $termometro['result'] >= 100 ? "Adicionar esse cartão ao carrinho" : "Você não possui pontos suficientes para resgatar esse item" }}" class="btn-adiciona-produto {{ $termometro['corCarrinho'] }}" cod-item="{{ $p->cod_produto }}" >
                                                        <span>adicionar</span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        @endforeach
                                </div>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h1 class="msg-aviso-visitado">Você não visitou nenhum produto recentemente. {{ $visitados->count()}}</h1>
                            </div>
                        @endif
                    </div>
                </div>
        </section>
        @endsection

