@extends('layout.layout-loja')

@section('title', 'O Programa')

@section('conteudo-loja')

@php
    $detect = new \Mobile_Detect;
@endphp

<section class="conteudo-passos-yetz-cards">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <img src="/img/img-yetz-cards-passos.jpg" class="img-responsive">

                        <ul>
                              <li>
                                    <a href="#primeiro-passo" id="passo-01">
                                          <img src="/img/box-passo-01.png" class="img-responsive">
                                    </a>
                              </li>
                              <li>
                                    <a href="#segundo-passo" id="passo-02">
                                          <img src="/img/box-passo-02.png" class="img-responsive">
                                    </a>
                              </li>
                              <li>
                                    <a href="#terceiro-passo" id="passo-03">
                                          <img src="/img/box-passo-03.png" class="img-responsive">
                                    </a>
                              </li>
                              <li>
                                    <a href="#quarto-passo" id="passo-04">
                                          <img src="/img/box-passo-04.png" class="img-responsive">
                                    </a>
                              </li>
                              <li>
                                    <a href="#quinto-passo" id="passo-02">
                                          <img src="/img/box-passo-05.png" class="img-responsive">
                                    </a>
                              </li>
                        </ul>
                  </div>
            </div>
            <div class="row">
                  <div class="todos-passos">
                        <div class="passo primeiro-passo" id="primeiro-passo">
                              <div class="texto">
                                    <h1>1º Conclua seu Cadastro</h1>
                                    <p>Se você está inscrito em uma campanha na empresa onde trabalha, agora que
                                          concluiu seu cadastro, está pronto para receber pontos YETZ e trocar por
                                          prêmios.<br> É importante que tenha preenchido corretamente todos os campos,
                                          principalmente e-mail e telefone, que são os canais para o contato com a
                                          equipe YETZ. E lembre-se de mantê-los atualizados.
                                    </p>
                                    @if($detect->isMobile())
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/cadastro.mp4">
                                    @else
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/videos/Primeiro_Cadastro.mp4">
                                    @endif
                                          <img src="/img/icone-video-sobre.png" alt="">
                                          <span>Clique AQUI para ver o vídeo CADASTRO</span>
                                    </a>
                              </div>
                              <img src="/img/img-passo-01.png" class="img-responsive">
                        </div>


                        <div class="passo segundo-passo" id="segundo-passo">
                              <div class="texto">
                                    <h1>2º Navegue pela Plataforma </h1>
                                    <p>Conheça mais a Plataforma e os prêmios incríveis que você encontra aqui e que
                                          poderá resgatar com os pontos que receber. Lembre-se que as regras de
                                          pontuação dependem da Campanha que você está participando e dos resultados que
                                          você alcançar.
                                    </p>
                                        @if($detect->isMobile())
                                            <a class="btn-assistir" link="https://yetzcards.s3-sa-east-1.amazonaws.com/videos/mobile/navegacao.mp4">
                                        @else
                                            <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/videos/navegacao.mp4">
                                        @endif
                                          <img src="/img/icone-video-sobre.png" alt="">
                                          <span>Clique AQUI para ver o vídeo NAVEGAÇÃO</span>
                                    </a>
                              </div>
                              <img src="/img/img-passo-02.png" class="img-responsive">
                        </div>

                        <div class="passo terceiro-passo" id="terceiro-passo">
                              <div class="texto">
                                    <h1>3º Lista de Desejos</h1>
                                    <p>Você pode adicionar prêmios que gostou à lista de desejos. Basta clicar na
                                          lâmpada mágica. Assim, você monta uma lista personalizada com tudo que deseja
                                          resgatar quando tiver pontos suficientes.
                                    </p>
                                        @if($detect->isMobile())
                                            <a class="btn-assistir" link="https://yetzcards.s3-sa-east-1.amazonaws.com/videos/mobile/navegacao.mp4">
                                        @else
                                            <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/videos/lista-desejos.mp4">
                                        @endif
                                          <img src="/img/icone-video-sobre.png" alt="">
                                          <span>Clique AQUI para ver o vídeo LISTA DE DESEJOS</span>
                                    </a>
                              </div>
                              <img src="/img/img-passo-03.png" class="img-responsive">
                        </div>

                        <div class="passo quarto-passo" id="quarto-passo">
                              <div class="texto">
                                    <h1>4º Escolher Prêmios / Resgates</h1>
                                    <p>Agora que você ganhou seus pontos e escolheu seus prêmios é hora de resgatar os
                                          cartões. Você pode a qualquer momento conferir seu saldo de pontos na
                                          Plataforma. E não se esqueça de conferir se tudo está correto antes de
                                          finalizar um resgate!
                                    </p>
                                    @if($detect->isMobile())
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/premios.mp4">
                                              <img src="/img/icone-video-sobre.png" alt="">
                                              <span>Clique AQUI para ver o vídeo ESCOLHER PRÊMIOS</span>
                                        </a>
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/resgate.mp4">
                                            <img src="/img/icone-video-sobre.png" alt="">
                                            <span>Clique AQUI para ver o vídeo RESGATES</span>
                                        </a>
                                    @else
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/premios.mp4">
                                            <img src="/img/icone-video-sobre.png" alt="">
                                            <span>Clique AQUI para ver o vídeo ESCOLHER PRÊMIOS</span>
                                        </a>
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/resgate.mp4">
                                            <img src="/img/icone-video-sobre.png" alt="">
                                            <span>Clique AQUI para ver o vídeo RESGATES</span>
                                        </a>
                                    @endif

                              </div>
                              <img src="/img/img-passo-04.png" class="img-responsive">
                        </div>

                        <div class="passo quinto-passo" id="quinto-passo">
                              <div class="texto">
                                    <h1>5º Extrato de Pontos</h1>
                                    <p>Pronto! Agora você pode acompanhar o andamento de seu pedido na Plataforma, além
                                          de conferir seu Extrato de Pontos para já ir planejando seus próximos
                                          resgates!</p>
                                    @if($detect->isMobile())
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/mobile/resgate.mp4">
                                              <img src="/img/icone-video-sobre.png" alt="">
                                              <span>Clique AQUI para ver o vídeo EXTRATO DE PONTOS</span>
                                        </a>
                                    @else
                                        <a class="btn-assistir" link="https://larayetz.s3-sa-east-1.amazonaws.com/videos/extrato.mp4">
                                            <img src="/img/icone-video-sobre.png" alt="">
                                            <span>Clique AQUI para ver o vídeo EXTRATO DE PONTOS</span>
                                        </a>
                                    @endif
                              </div>
                              <img src="/img/img-passo-05.png" class="img-responsive">
                        </div>
                  </div>
            </div>
      </div>
</section>

<div class="modal fade" tabindex="-1" role="dialog" id="modal-media">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">
                                    <img src="/img/icon-close-video.png" class="btn-close" />
                              </span>
                        </button>
                  </div>
            </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



@stop

@section('scripts')
<script type="text/javascript">
let file;

$(".btn-assistir").click(function() {
      file = $(this).attr("link");
      $("#modal-media").modal("show");
});

$("#modal-media").on("show.bs.modal", function() {
      $(".modal-body").append('<div id="conteudo-midia">')
      $("#conteudo-midia").append('<video controls controlsList="nodownload" autoplay></video>');
      $("video").append('<source src="' + file + '" type="video/mp4" />');
});

$("#modal-media").on("hide.bs.modal", function() {
      $("#conteudo-midia").remove();
});
</script>
@endsection
