@extends('layout.layout-loja')

@section('title', 'O Programa')


@section('styles')

<link href="/css/usuario_antigo/cadastro.css" rel="stylesheet">
<link href="/css/usuario_antigo/personalizado-meu-perfil.css?{{ config('app.versao_cache') }}" rel="stylesheet">

<!--Fontes-->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>

@endsection




@section('conteudo-loja')


<section class="s-cadastro">
      <div class="container">
            <div class="row">
                  <div class="fundo-cadastro">
                        <h1>Perfil</h1>
                        <hr>
                        <form action="/loja/edita-perfil" id="formCadastro" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              <div class="row">
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                          <h2>Dados pessoais</h2>

                                          <div class="image-editor" id="image-cropper">
                                                <div class="cropit-preview cropit-image-preview"
                                                      style="background-image: url({{ $usuario->foto_usuario }}); border: 1px solid #ccc;border-radius: 75px;">
                                                </div>
                                                <input type="range" class="cropit-image-zoom-input">
                                                <input type="file" accept=“image/*;capture=camera”
                                                      class="cropit-image-input" id="trocaImagemPadrao">
                                                <input type="hidden" name="foto_perfil" class="hidden-image-data" />
                                                <button class="export">Export</button>
                                                <!-- <span id="nomeFotoCropit">Sem foto.jpg</span> -->
                                          </div>
                                          <div class="form-group">
                                                <img id="uploadPreview" style="display:none; width:70%; height: 70%;" />
                                          </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-6">
                                          <p class="nome-usuario-cadastro">Nome:
                                                <span>{{ $usuario->nome_usuario }}</span></p>
                                          <p class="cpf-usuario-cadastro">Login: <span
                                                      class="mask-cpf">{{ $usuario->login }}</span></p>

                                          <div class="container-btns">
                                                <button class="btn-verde pull-left active" type="button"
                                                      id="trocaFoto">Trocar foto</button>

                                                <button class="rotate-ccw-btn" type="button" id="rotate"><img
                                                            src="/img/girar-icon.png" alt=""></button>
                                          </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-12 col-md-12">
                                          <div class="form-group">
                                                <label for="">Apelido</label>
                                                <input type="text" class="form-control" name="apelido"
                                                      value="{{ $usuario->apelido }}">
                                          </div>
                                          <div class="form-group">
                                                <label for="">Data de Nascimento</label>
                                                <input type="text" placeholder="Data de Nascimento" id="dataNascimento"
                                                      onkeydown="mascara(this, dataDMA)" name="data_nascimento"
                                                      value="{{ $usuario->nascimento }}"
                                                      onkeydown="mascara(this, dataDMA)" class="form-control">
                                          </div>
                                          <div class="form-group">
                                                <label for="">Sexo</label>
                                                <select class="form-control" name="sexo">
                                                      <option value="-">selecione</option>
                                                      <option value="f" {{ $usuario->sexo == "f" ? 'selected' : '' }}>
                                                            Feminino</option>
                                                      <option value="m" {{ $usuario->sexo == "m" ? 'selected' : '' }}>
                                                            Masculino</option>
                                                </select>
                                          </div>
                                          @if(session('campanha')->cod_campanha != 64)
                                                <div class="form-group">
                                                      <label for="">Telefone</label>
                                                      <input class="mask-telefone form-control" type="text"
                                                            placeholder="Telefone" id="telefone" name="telefone"
                                                            value="{{ $usuario->telefone }}" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                      <label for="">Celular</label>
                                                      <input type="text" class="mask-whats form-control" placeholder="Celular"
                                                            id="telefone-whats" name="celular"
                                                            value="{{ $usuario->celular }}">
                                                </div>
                                                <div class="form-group">
                                                      <label for="">Email</label>
                                                      <input type="text" placeholder="E-mail" id="email"
                                                            onblur="mascara(this, verificaCampoEmail)" name="email"
                                                            value="{{ $usuario->email }}" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                      <div class="checkbox">
                                                            <label class="label-whatsapp"><input class="input-whatsapp"
                                                                        name="notificacao_celular" type="checkbox" value="1"
                                                                        {{ ($usuario->notificacao_celular == '1') ? 'checked' : '' }}>Aceito
                                                                  receber
                                                                  mensagens SMS/E-mail da plataforma YETZ CARDS</label>
                                                      </div>
                                                </div>
                                          @endif
                                          <button id="btnTrocarSenha" class="btn-verde pull-right" type="button">TROCAR
                                                SENHA</button>
                                    </div>
                              </div>
                              <div class="align-botoes-acao">
                                    <a href="/loja" class="btn-default btn-padrao btn-cancelar">CANCELAR</a>
                                    <input type="submit" value="ATUALIZAR" class="btn-default btn-padrao">
                              </div>
                        </form>
                  </div>
            </div>
      </div>
</section>

<!-- Modal Trocar Senha -->
<div id="modalTrocaSenha">
      <div class="container-modal">
            <div class="modal-header">
                  <img src="/img/logo-modal-login.png" class="logo-header-modal">
                  <button type="button" id="closeModalTrocaSenha" class="close" data-dismiss="modal">
                        <img src="/img/btn-close.png">
                  </button>
            </div>

            <div class="modal-body">
                  <form id="formModalTrocaSenha">
                        <div class="modal-body">
                              <h1>Troca de Senha</h1>
                              <p>Digite nos campos abaixo, a senha que deseja utilizar nos proximos acessos.</p>
                              <div class="form-group">
                                    <div class="input-group">
                                          <span class="input-group-addon" id="basic-addon1">
                                                <i class="glyphicon glyphicon-lock"></i>
                                          </span>
                                          <input id="senha" type="password" class="form-control input-lg" name="senha"
                                                onkeyup="verificaCapsCadastro(this, event)" placeholder="Nova senha">
                                    </div>
                                    <span class="alerta-caps">Caps Lock Ativado</span>
                              </div>
                              <div class="form-group">
                                    <div class="input-group">
                                          <span class="input-group-addon" id="basic-addon1">
                                                <i class="glyphicon glyphicon-lock"></i>
                                          </span>
                                          <input id="confirma-senha" type="password" class="form-control input-lg"
                                                name="confirma_senha" onkeyup="verificaCapsCadastro(this, event)"
                                                placeholder="Confirmar nova senha">
                                    </div>
                                    <span class="alerta-caps">Caps Lock Ativado</span>
                              </div>
                        </div>
                        <div class="footerDoModal modal-footer">
                              <div class="botoesDoModal">
                                    <input type="submit"
                                          class="botaoConfirmarTrocaModal classeDosBotoesDoModal btn btn-success"
                                          value="Salvar">
                              </div>
                        </div>
                  </form>
            </div>
      </div>
</div>

@php
      $detect = new \Mobile_Detect;
@endphp
<input type="hidden" id="is-iphone" value="{{ $detect->isiOS() }}">



@stop


@section('scripts')

<script src="../js/usuario_antigo/jquery-1.11.3.min.js"></script>
<script src="../lib/validate/jquery.validate.js"></script>
<script src="../js/usuario_antigo/bootstrap.js"></script>
<script src="../js/usuario_antigo/jquery.cropit.js"></script>
<script src="../js/usuario_antigo/validacoes.js"></script>
<script src="../js/usuario_antigo/jquery.mask.min.js"></script>
<script src="../js/usuario_antigo/jquery.amaran.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/exif-js"></script>
<script type="text/javascript">
$(function() {
      jQuery(".mask-telefone")
            .mask("(99) 9999-9999")
            .focusout(function(event) {
                  var target, phone, element;
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);
                  element.unmask();
                  if (phone.length > 10) {
                        element.mask("(99) 9 9999-999?9");
                  } else {
                        element.mask("(99) 9999-9999?9");
                  }
            });
      jQuery(".mask-whats")
            .mask("(99)9 9999-9999")
            .focusout(function(event) {
                  var target, phone, element;
                  target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                  phone = target.value.replace(/\D/g, '');
                  element = $(target);
                  element.unmask();
            });
      //    Cropit
      $('#trocaFoto').click(function() {
            $('#trocaImagemPadrao').trigger('click');
            $("#rotate").addClass('active')
            $(".cropit-image-zoom-input").addClass('active')
      });

      function getOrientation(file, callback) {
            var reader = new FileReader();
            console.log(file)
            reader.onload = function(e) {

                  var view = new DataView(e.target.result);
                  if (view.getUint16(0, false) != 0xFFD8) {
                        return callback(-2);
                  }
                  var length = view.byteLength,
                        offset = 2;
                  while (offset < length) {
                        if (view.getUint16(offset + 2, false) <= 8) return callback(-1);
                        var marker = view.getUint16(offset, false);
                        offset += 2;
                        if (marker == 0xFFE1) {
                              if (view.getUint32(offset += 2, false) != 0x45786966) {
                                    return callback(-1);
                              }

                              var little = view.getUint16(offset += 6, false) == 0x4949;
                              offset += view.getUint32(offset + 4, little);
                              var tags = view.getUint16(offset, little);
                              offset += 2;
                              for (var i = 0; i < tags; i++) {
                                    if (view.getUint16(offset + (i * 12), little) == 0x0112) {
                                          return callback(view.getUint16(offset + (i * 12) + 8,
                                                little));
                                    }
                              }
                        } else if ((marker & 0xFF00) != 0xFF00) {
                              break;
                        } else {
                              offset += view.getUint16(offset, false);
                        }
                  }
                  return callback(-1);
            };
            reader.readAsArrayBuffer(file);
      }


      $('#image-cropper').cropit({
            onImageLoaded() {

                  let img = document.querySelector('.hidden-image-data')

                  getOrientation(img,function(orientation){
                        alert('orientation: ' + orientation);
                  })
            }
      });

      // When user clicks select image button,
      // open select file dialog programmatically
      $('.select-image-btn').click(function() {
            $('.cropit-image-input').click();
      });

      // Handle rotation
      $('.rotate-cw-btn').click(function() {
            $('#image-cropper').cropit('rotateCW');
      });

      $('.rotate-ccw-btn').click(function() {
            $('#image-cropper').cropit('rotateCCW');
      });


      // var cropit = $('.image-editor').cropit({


      //       onFileChange: function(teste) {
      //             $('#nomeFotoCropit').text($('#trocaImagemPadrao').val());
      //       }
      // });

      // var _rotation = 0;
      // document.querySelector('#rotate').addEventListener('click', function(){
      //       _rotation += 90;
      //       $('.cropit-image-preview').css('transform', 'rotate(' + _rotation + 'deg)');
      //       // console.log(img)
      // })
      // // $('#rotate').click(function(){
      // //       cropit.cropit('rotateCW')
      // //       console.log(cropit)
      // //       // cropit.cropit('offset', { x: -18, y: -54 })
      // //       // $('.image-editor').cropit('rotateCCW')
      // // })


      $(".btn-cep").click(function(evento) {
            evento.preventDefault();
            carregarCepNovo();
      });

      function carregarCepNovo() {
            var carregandoEstilo = $("#carregando");
            var cepEstilo = $("#cep");
            var cep = cepEstilo.val().replace(/\D/g, '');
            var url = "https://viacep.com.br/ws/" + cep + "/json/";

            cepEstilo.removeClass('error-input');
            carregandoEstilo.toggle();

            $.ajax({
                  type: 'GET',
                  url: url,
                  async: true,
                  timeout: 10000,
                  contentType: "application/json",
                  dataType: 'json',
                  success: function(dados) {
                        if (dados.erro != true) {
                              limpaCamposEnd();
                              carregandoEstilo.toggle();
                              $('#estado').val(dados.uf);
                              $('#cidade').val(dados.localidade);

                              if (!(dados.bairro) == "") {
                                    $('#bairro').val(dados.bairro);
                              } else {
                                    $('#bairro').removeAttr("readOnly");
                              }

                              if (dados.logradouro == "") {
                                    $('#logradouro').removeAttr("readOnly");
                              } else {
                                    var logradouro = dados.logradouro;
                                    $('#logradouro').val(logradouro);
                              }

                              if (dados.bairro == "" || dados.logradouro == "")
                                    alert("Por favor complete o endereço");
                              else
                                    $('#numero').focus();
                        } else {
                              carregandoEstilo.toggle();
                              alert("CEP incorreto");
                              cepEstilo.addClass('error-input');
                        }
                  },
                  error: function(e) {
                        carregandoEstilo.toggle();
                        if (e.statusText == 'timeout') {
                              alert("Tempo limite atingido! Tente novamente...");
                        } else {
                              cepEstilo.addClass('error-input');
                              alert("Erro ao consultar CEP, contate o administrador!")
                        }
                  }
            });
      }

      function limpaCamposEnd() {
            $("#estado").val("");
            $("#estado").removeClass('error-input');
            $("#cidade").val("");
            $("#cidade").removeClass('error-input');
            $("#bairro").val("");
            $("#bairro").removeClass('error-input');
            $("#logradouro").val("");
            $("#logradouro").removeClass('error-input');
            $("#complemento").val("");
            $("#complemento").removeClass('error-input');
            $("#numero").val("");
            $("#numero").removeClass('error-input');
            $('#bairro').attr("readOnly");
            $('#logradouro').attr("readOnly");
      }
});

//Valida Formulário
$('#formCadastro').submit(function(e) {

      var erro = 0;

      if($('#is-iphone').val() == "1"){
            $('#image-cropper').cropit('rotateCCW');
            $('#image-cropper').cropit('rotateCCW');
            $('#image-cropper').cropit('rotateCCW');
      }

      $('.hidden-image-data').val($('.image-editor').cropit('export'));

      //Pega os estilos
      var dataNascimentoEstilo = $('#dataNascimento');
      var sexoEstilo = $('.select-sexo');
      var emailEstilo = $('#email');
      var telefoneEstilo = $('#telefone');


      //Pega os valores
      var dataNascimento = $('#dataNascimento').val();
      var sexo = $('#sexo').val();
      var email = $('#email').val();
      var telefone = $('#telefone').val();


      if (dataNascimento == "" || dataNascimento == null) {
            dataNascimentoEstilo.addClass('error-input');
            erro++;
      } else {
            dataNascimentoEstilo.removeClass('error-input');
      }

      if (sexo == "") {
            sexoEstilo.addClass('error-input');
            erro++;
      } else {
            sexoEstilo.removeClass('error-input');
      }

      if (email == "" || email == null || email.indexOf("@") == -1 || email.indexOf(".") == -1) {
            emailEstilo.addClass('error-input');
            erro++;
      } else {
            emailEstilo.removeClass('error-input');
      }
      if (erro == 0) {
            $("body").scrollTop(0);
            $("#tela-opacao").css("display", "block");
            $("body").css("overflow", "hidden");
            return true;
      } else {
            alerta('awesome error', 'Campos incorretos', 'Verifique os campos em vermelho', '',
                  'fa fa-times');
            return false;
      }
});


$('#formModalTrocaSenha').submit(function(e) {
      e.preventDefault();

      if ($("#senha").val() != $("#confirma-senha").val()) {
            alerta('awesome error', 'Senhas diferentes!', 'Você precisa confirmar a senha corretamente!',
                  '', 'fa fa-times');
            return;
      }

}).validate({
      rules: {
            senha: {
                  required: true,
                  minlength: 6
            },
            confirma_senha: {
                  required: true,
                  minlength: 6,
                  equalTo: '#senha'
            }
      },
      errorPlacement: function(error, element) {},
      submitHandler: function(form) {

            let usuario = new FormData();

            usuario.append('senha', $("#senha").val());
            usuario.append('confirma_senha', $("#confirma-senha").val());

            $.ajax({
                  url: "/loja/mudar-senha",
                  type: "POST",
                  data: usuario,
                  async: true,
                  contentType: false,
                  processData: false,
                  timeout: 10000,
                  headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  },
                  success: function(data) {
                        alerta('awesome ok', 'Senha alterada!',
                              'Sua senha foi alterada com sucesso!', '',
                              'fa fa-check');
                        document.querySelector('#modalTrocaSenha').classList.toggle(
                              'active');
                        $("#senha").val('');
                        $("#confirma-senha").val('');
                  },
                  error: function(x, t, m) {
                        alerta('awesome error', data.mensagem, '', '',
                              'fa fa-times');
                        console.log("erro=> " + m);
                        document.querySelector('#modalTrocaSenha').classList.toggle(
                              'active')
                  }
            });

      }
});

document.querySelectorAll('#btnTrocarSenha').forEach(function(btn) {
      btn.addEventListener('click', function() {
            document.querySelector('#modalTrocaSenha').classList.toggle('active')
      })
})

document.querySelector('#modalTrocaSenha').addEventListener('click', function(e) {
      if (e.target.classList.value.includes('active')) {
            e.target.classList.toggle('active')
      }
})

document.querySelector('#closeModalTrocaSenha').addEventListener('click', function() {
      document.querySelector('#modalTrocaSenha').classList.toggle('active')
})


function alerta(tema, titulo, menssagem, info, icon) {
      $.amaran({
            'theme': tema,
            'content': {
                  title: titulo,
                  message: menssagem,
                  info: info,
                  icon: icon
            },
            'position': 'bottom right',
            'cssanimationIn': 'tada',
            'cssanimationOut': 'bounceOut',
      });
}
</script>

@endsection
