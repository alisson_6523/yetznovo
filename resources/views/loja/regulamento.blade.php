@extends('layout.layout-loja')

@php
      $ios = '';
      $detect = new \Mobile_Detect;
@endphp

@section('styles')
<link href="/css/usuario_antigo/personalizado-regulamento.css" rel="stylesheet">
@endsection

@section('title', 'Minha Campanha')


@section('conteudo-loja')
<div class="titulo-da-pagina">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <h1 class="titulo-pag">Minha Campanha</h1>
                  </div>
            </div>
      </div>
</div>
<div id="area-campanhas">
      <div class="container">
            <div class="row">
                  <div class="col-md-12">
                        <div class="table-responsive">
                              <table class="table table-responsive" id="tabela">
                                    <thead>
                                          <tr>
                                                <th>REGULAMENTO</th>
                                          </tr>
                                    </thead>
                                    <tbody>
                                          @php
                                          $regulamento = \App\Models\Regulamento::where('cod_campanha', $campanha->cod_campanha)->where('ilha', $usuario->ilha)->first();
                                          @endphp
                                          <tr>
                                                <td>
                                                      @if(!empty($regulamento))
{{--                                                      <a href="/loja/baixar-regulamento/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">--}}
{{--                                                         {{ $regulamento->titulo == "" ? $regulamento->regulamento : $regulamento->titulo }}</a>--}}
                                                            <a href="{{ \Storage::cloud()->temporaryUrl('regulamento-campanha/' . $regulamento->regulamento_hash, \Carbon\Carbon::now()->addMinutes(5)) }}" target="_blank">{{ $regulamento->titulo == "" ? $regulamento->regulamento : $regulamento->titulo }}</a>
                                                      @elseif(empty($regulamento) && !empty($campanha->regulamento))
{{--                                                      <a href="/loja/baixar-regulamento/{{ $campanha->cod_campanha }}/{{ limpaString($campanha->nome_campanha) }}">--}}
{{--                                                         {{ $campanha->titulo_regulamento == "" ? $campanha->nome_campanha : $campanha->titulo_regulamento }}</a>--}}
                                                            <a href="{{ \Storage::cloud()->temporaryUrl($campanha->regulamento_hash, \Carbon\Carbon::now()->addMinutes(5)) }}" target="_blank">{{ $campanha->titulo_regulamento == "" ? $campanha->nome_campanha : $campanha->titulo_regulamento }}</a>
                                                      @else
                                                      Sem Regulamento
                                                      @endif
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>
                        </div>
                  </div>
            </div>
      </div>
</div>

<div id="area-campanha-mobile">
      <div class="container-cards">
            <div class="container-card-mobile" data-tipo="1" data-date="09/2019">
                  <div class="card-mobile">
                      @php
                          $regulamento = \App\Models\Regulamento::where('cod_campanha', $campanha->cod_campanha)->where('ilha', $usuario->ilha)->first();
                          $mobileSo = $detect->isiOS() ? 'ios' : 'android';

                            if(!empty($regulamento))
                                $linkMobile =  ($mobileSo == 'android' ? "/loja/baixar-regulamento/".$campanha->cod_campanha."/".limpaString($campanha->nome_campanha) : \Storage::disk('s3')->url('regulamento-campanha/'.$regulamento->regulamento_hash));

                            else
                                $linkMobile =  ($mobileSo == 'android' ? "/loja/baixar-regulamento/".$campanha->cod_campanha."/".limpaString($campanha->nome_campanha) : \Storage::disk('s3')->url($campanha->regulamento_hash));
                      @endphp
                      <div class="card-text {{ $mobileSo }}">
                          <h3>Regulamento: </h3>
                          @if(!empty($regulamento))
					@if($mobileSo == "ios")
						<a href="{{ \Storage::disk('s3')->url('regulamento-campanha/'.$regulamento->regulamento_hash) }}"  rel="noopener noreferrer"> {{ $regulamento->titulo == "" ? $regulamento->regulamento : $regulamento->titulo }} </a>
					@else
						<span data-link="{{ \Storage::disk('s3')->url('regulamento-campanha/'.$regulamento->regulamento_hash) }}">
							{{ $regulamento->titulo == "" ? $regulamento->regulamento : $regulamento->titulo }}</span>
					@endif
                          @elseif(empty($regulamento) && !empty($campanha->regulamento))
                              <span  data-link="{{ $linkMobile }}">
                                  {{ $campanha->titulo_regulamento == "" ? $campanha->nome_campanha : $campanha->titulo_regulamento }}</span>
                          @else
                              Sem Regulamento
                          @endif
                      </div>
			    <!-- {{ $detect->isiOS() ? 'ios' : '' }} -->
                      <div class="modal-iframe {{ $detect->isiOS() ? 'ios' : 'android' }} ">

                              <div class="container-modal">
                                    <div class="container-close-modal">
                                          <img src="/img/icon-close-video.png" alt="">
                                    </div>
                                    <iframe src="" frameborder="0" style="width: 100%;height: 300px;height: 100%;     overflow: scroll;"></iframe>
                              </div>
                      </div>

                  </div>
            </div>
      </div>
</div>

@stop
