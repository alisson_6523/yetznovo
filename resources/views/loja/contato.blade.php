@extends('layout.layout-loja')

@section('title', 'O Programa')


@section('styles')
    <link href="/css/usuario_antigo/personalizado-contato.css" rel="stylesheet">
@endsection


@section('conteudo-loja')
        <section class="area-contato">
                <div class="topo-contato">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <img src="/img/icone-balao-msg.png">
                                <h1 class="titulo-pag">fale conosco</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="box-conteudo envio-mensagem">
                                    <div class="topo">
                                        <div class="icone">
                                            <img src="/img/envie-mensagem.svg" alt="">
                                        </div>
                                        <div class="texto">
                                            <p class="titulo">envie sua mensagem</p>
                                            <p class="mensagem">Aqui você pode nos enviar sugestões, dúvidas ou reclamações sobre o funcionamento do site.</p>
                                        </div>
                                    </div>
                                    <div class="conteudo">
                                        <form id="form-fale-conosco">
                                            <input name="nome" id="fale-conosco-nome" type="text" value="{{ \Auth::user()->nome_usuario }}" disabled>
                                            <input name="telefone" id="fale-conosco-telefone" type="text" value="{{ \Auth::user()->telefone }}" disabled>
                                            <input name="email" id="fale-conosco-email" type="text" value="{{ \Auth::user()->email }}" disabled>
                                            <input name="assunto" id="fale-conosco-assunto" type="text" placeholder="ASSUNTO">
                                            <textarea name="mensagem" id="fale-conosco-mensagem"></textarea>
                                            <button type="submit" id="btn-enviar-contato">enviar</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="box-conteudo duvidas-frequentes">
                                    <div class="topo">
                                        <div class="icone">
                                            <img src="/img/duvidas-frequentes.svg" alt="">
                                        </div>
                                        <div class="texto">
                                            <p class="titulo">dúvidas frequentes</p>
                                            <p class="texto">Encontre aqui as respostas para as principais dúvidas sobre a plataforma.</p>
                                        </div>
                                    </div>
                                    <div class="conteudo">
                                        <div class="bloco-busca">
                                            <input type="text" placeholder="Busca">
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que é o YETZ CARDS?</span>
                                            <span class="resposta">YETZ CARDS é uma Plataforma de Recompensas onde, em uma Campanha, você utiliza pontos que recebe da empresa onde trabalha para adquirir o cartão de sua loja preferida. Daí é só ir na loja e trocar pelo seu prêmio.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que é uma Campanha?</span>
                                            <span class="resposta">
                                                Campanha é uma forma das empresas premiarem os funcionários. A empresa define o escopo da Campanha, suas metas, regras, periodicidade e parâmetros de conversão. A partir daí informa à equipe YETZ a pontuação obtida por cada colaborador, que tem pontos YETZ creditados em sua conta na Plataforma, para resgate de cartões.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como sei quais as Campanhas que eu participo?</span>
                                            <span class="resposta">
                                                Na barra central, onde está seu nome, você pode acessar o Menu “Minhas Campanhas” e lá estarão as Campanhas que participa na Plataforma YETZ CARDS e suas respectivas regras.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Quais são os prêmios da Plataforma de Recompensas YETZ CARDS?</span>
                                            <span class="resposta">
                                                A YETZ CARDS oferece centenas de cartões pré-pagos em diversas categorias, que podem ser segmentados, de lojas e cartão presente.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que são cartões segmentados?</span>
                                            <span class="resposta">São cartões que você pode utilizar em lojas de determinado segmento, como: Combustível, Entretenimento, Farmácia, Papelaria, Qualidade de Vida e Supermercado. Se quiser ver o detalhamento de cada segmento, vá na descrição dos cartões, onde tem também suas respectivas regras de utilização.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que são cartões de uma loja?</span>
                                            <span class="resposta">São cartões de uma marca específica e você tem várias opções de lojas para escolher. Veja no detalhamento dos cartões suas regras de uso e confira se na sua cidade tem a loja física ou se pode usar o cartão em loja virtual.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que é cartão presente?</span>
                                            <span class="resposta">É um cartão que poderá ser utilizado no estabelecimento que desejar, contanto que o mesmo aceite cartões bandeirados.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que são Pontos YETZ? Como conquisto?</span>
                                            <span class="resposta">São pontos atribuídos a você dentro da Plataforma YETZ CARDS, a partir de critérios e periodicidade estabelecidos pela sua empresa, quando você participa de uma Campanha. Esses pontos têm relação ao desempenho que você obtiver no seu trabalho, de modo que quanto maior sua produção, mais pontos obterá.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como consulto meu saldo?</span>
                                            <span class="resposta">Na barra central da Plataforma você tem a informação do seu SALDO ATUAL. Caso deseje detalhamento, acesse Extrato de Pontos.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Não concordo com a quantidade de pontos que recebi, o que devo fazer?</span>
                                            <span class="resposta">Se após conferir o regulamento em Minhas Campanhas persistir dúvida, contate seu supervisor ou superior imediato para tirar suas dúvidas sobre esse assunto.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Meus pontos possuem data de validade?</span>
                                            <span class="resposta">Sim. Você pode acompanhar a validade dos seus pontos em Extrato de Pontos.  É de sua responsabilidade acompanhar seu extrato e vencimento dos pontos para que não tenha pontos vencidos e não resgatados. Caso isso suceda, perderá seu direito aos pontos e não poderá solicitar reembolso de nenhuma espécie.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Onde vejo minhas movimentações de pontos?</span>
                                            <span class="resposta">Basta clicar em Extrato de Pontos e ver todas as operações de créditos e resgates.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">É possível transferir Pontos YETZ para outra pessoa?</span>
                                            <span class="resposta">Não é permitida a comercialização, troca, doação, permuta, venda, cessão ou outra forma de transferência dos pontos entre os participantes ou terceiros. Confira em Termos e Condições de Uso.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Meus pontos diminuíram sem eu efetuar um resgate. O que devo fazer?</span>
                                            <span class="resposta">Caso isso aconteça, procure seu supervisor ou superior imediato e obtenha informações.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Fui desligado da empresa e tenho pontos na plataforma que não utilizei. Posso usar? Até quando?</span>
                                            <span class="resposta">Essa situação varia de caso a caso e depende das Regras da Campanha que você participa. Consulte Minhas Campanhas na plataforma.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que é Lista de Desejos? Como funciona?</span>
                                            <span class="resposta">É o local onde você poderá ver todos os cartões que marcou clicando na lâmpada mágica, cartões que tem interesse ou ainda não tem pontos suficientes para resgate. Assista ao vídeo para entender melhor.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que significa uma barra de cores diferente que aparece abaixo dos cartões?</span>
                                            <span class="resposta">Trata-se de um termômetro colorido que indica a relação entre sua pontuação e aquela necessária para adquirir o prêmio. Se estiver vermelho você ainda precisa juntar muitos pontos, amarelo significa que está próximo e se estiver verde é porque já tem saldo suficiente para o resgate.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que é Streaming?</span>
                                            <span class="resposta">É onde estão disponíveis diversos conteúdos em vídeo e áudio. Podem ser mensagens, comunicados ou dicas de sua empresa, relacionados à Campanha para melhorar ainda mais sua performance ou tutoriais que facilitam o uso da Plataforma.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">O que são e qual a função das notificações?</span>
                                            <span class="resposta">É um espaço dedicado à comunicação com os participantes. Utilizado para enviar alertas, lembretes de datas importantes, conteúdos e mensagens sobre a Campanha.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como faço para alterar o status de “receber” ou “não receber” e-mail marketing e SMS?</span>
                                            <span class="resposta">Na barra central, onde tem seu nome, acesse MEU PERFIL. Marque ou desmarque a caixa na parte inferior da janela “Aceito receber mensagens SMS/E-mail da Plataforma YETZ CARDS” e clique em atualizar. Lembre-se que autorizar receber mensagens garante você ficar por dentro do que acontece na sua Campanha.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Abaixo dos cartões vejo um ou dois ícones e-commerce (laranja) e loja física (vinho), o que isso significa?</span>
                                            <span class="resposta">Significa de que forma você poderá utilizar o cartão. Se constar apenas o ícone de E-COMMERCE o cartão apenas permite compras virtuais. Se constar apenas o ícone vinho, o cartão deverá, necessariamente, ser utilizado em LOJA FÍSICA. Caso constem ambos os ícones você pode utilizar o cartão das duas formas, física ou virtualmente.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Por que alguns cartões aparecem dentro da imagem de um computador?</span>
                                            <span class="resposta">Esses cartões são VOUCHERS VIRTUAIS e serão enviados para o e-mail que você cadastrou, eletronicamente. Caso o cartão não esteja dentro do computador, CARTÃO FÍSICO, será entregue no endereço do usuário, fisicamente.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como resgatar um cartão?</span>
                                            <span class="resposta">Para resgatar um cartão basta adicioná-lo ao carrinho, clicando no ícone abaixo do cartão ou no botão “adicionar ao carrinho”, que está em “descrição do cartão”. É possível ver tudo que está pretendendo resgatar clicando no carrinho. Se desejar, pode retirar itens ou finalizar o resgate. Lembre-se de conferir os dados antes de concluir.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Finalizei o resgate e quero cancelar. O que devo fazer?</span>
                                            <span class="resposta">O cancelamento do cartão poderá ser solicitado até 48 horas úteis após o resgate. Será realizada análise pela equipe YETZ que poderá ou não aprovar a solicitação, desde que o cartão não tenha sido enviado para o usuário. Para pedir cancelamento enviar solicitação através do Fale Conosco.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Fiz um resgate, quando vou receber meu cartão?</span>
                                            <span class="resposta">Depende da forma que definiu receber seu cartão: Opção 1: através de cartão físico, que será entregue no local de trabalho, no endereço cadastrado na plataforma, em até 30 dias. Opção 2: através de voucher digital, enviado para o endereço de e-mail cadastrado na plataforma, em até 10 dias.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Em qual endereço irei receber os cartões que resgatar?</span>
                                            <span class="resposta">Os cartões serão entregues no seu local de trabalho para seu superior. Retire com ele, confira e assine o protocolo de recebimento.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como acompanho o meu resgate?</span>
                                            <span class="resposta">Escolha a opção Acompanhar Resgates na Plataforma e no item “status” escolha “ver detalhes”.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Meu resgate não chegou na data prevista. Como devo proceder?</span>
                                            <span class="resposta">Caso isso aconteça, procure seu supervisor ou superior imediato e obtenha informações.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Meu resgate foi cancelado, o que devo fazer?</span>
                                            <span class="resposta">Caso isso aconteça, procure seu supervisor ou superior imediato e obtenha informações.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Fui desligado da empresa, mas tinha solicitado um resgate. Onde meu cartão será entregue?</span>
                                            <span class="resposta">Seu cartão será entregue no endereço informado pela Plataforma na conclusão do resgate, para o supervisor responsável pela área onde você estava registrado, caso as Regras da Campanha que você participa prevejam essa situação. Consulte as regras em Minhas Campanhas na plataforma.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como descubro onde o cartão que resgatei pode ser utilizado?</span>
                                            <span class="resposta">Consulte a “Descrição, Regras de Uso e Validade” clicando no referido cartão, na Plataforma.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Por quanto tempo o cartão resgatado é válido?</span>
                                            <span class="resposta">Consulte a “Descrição, Regras de Uso e Validade” clicando no referido cartão, na Plataforma.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Posso complementar o valor da compra com outra forma de pagamento?</span>
                                            <span class="resposta">No caso de compras efetuadas através de e-commerce não é possível. Para compras em lojas físicas, a maioria das lojas aceita, mas é necessário confirmar as opções na loja escolhida.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Posso fazer várias compras com um cartão, até acabar o saldo e dentro do vencimento dele?</span>
                                            <span class="resposta">Compras efetuadas através de e-commerce não permitem saldo remanescente no cartão, você precisará utilizar o valor na totalidade ou perderá o saldo que ficar no cartão. Para compras em lojas físicas, a maioria das lojas permite que se efetue várias compras, mas é necessário confirmar na loja escolhida.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Não estou conseguindo utilizar o cartão que resgatei, o que devo fazer?</span>
                                            <span class="resposta">Primeiro entre em contato com a loja ou empresa descrita no cartão para saber o que pode ter ocorrido. Se o assunto não for solucionado, contate a YETZ através do Fale Conosco.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Em caso de perda ou roubo do meu cartão, como devo proceder?</span>
                                            <span class="resposta">Alguns cartões precisam que seja efetuado um cadastro no site da empresa/loja para desbloqueio dos créditos. Caso você tenha realizado este cadastro, entre em contato com a empresa do cartão para solicitação da segunda via, autorizando a cobrança da taxa para essa emissão.</span>
                                            <span class="resposta">Caso trate-se de cartões que não precisam ou não tenha feito o cadastro, não há como recuperar o saldo, pois cartões ao portador com saldo disponível não podem ser substituídos em caso de roubo, perda ou extravio, sendo a guarda de responsabilidade do portador.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como atualizo os meus dados cadastrais?</span>
                                            <span class="resposta">O nome do USUÁRIO e identificação pessoal estarão preenchidos e não poderão ser editados.</span>
                                            <span class="resposta">Outros dados, como foto, e-mail, dados pessoais, senha, telefones podem ser alterados acessando MEU PERFIL na barra central. É importante que você mantenha seus dados sempre atualizados.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Esqueci minha senha. O que eu faço?</span>
                                            <span class="resposta">Escolher a opção ESQUECI MINHA SENHA na janela de acesso, após informar CHAVE DO CLIENTE e identificação pessoal. Você receberá uma senha em seu e-mail cadastrado. Não deixe de verificar sua caixa de Spam. Caso deseje, pode alterar clicando em MEU PERFIL e em TROCAR SENHA.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Meu colega que participa da Campanha esqueceu a senha e não tem mais acesso ao e-mail cadastrado na Plataforma. O que ele deve fazer?</span>
                                            <span class="resposta">Deve procurar o superior de sua área, que tomará as providências junto ao YETZ.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Estou com problemas, como faço para entrar em contato com a Plataforma?</span>
                                            <span class="resposta">Em Fale Conosco, clique na opção Dúvidas Frequentes e veja as principais questões relacionadas à Plataforma. Se não encontrou a resposta que precisa, contate a YETZ CARDS através do Fale Conosco.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">Como faço para falar com alguém do YETZ?</span>
                                            <span class="resposta">Em Fale Conosco, você pode enviar um e-mail para a Equipe YETZ acerca de sugestões, reclamações sobre o funcionamento da Plataforma ou dúvidas. Antes de enviar seu e-mail confira ao lado, em Dúvidas Frequentes, se encontra resposta para seu questionamento.</span>
                                        </div>
                                        <div class="bloco-duvida">
                                            <span class="duvida">A YETZ CARDS possui aplicativo para celulares, tablets e smartphones?</span>
                                            <span class="resposta">Sim, o APP YETZ CARDS para dispositivos móveis é compatível com iOS 9.0 e Android 4.0 ou superiores, disponível para download na App Store e na Google Play. Lá você pode efetuar todas suas operações.</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4">
                                <div class="box-conteudo chat-online">
                                    <div class="topo">
                                        <div class="icone">
                                            <img src="/img/chat-online.svg" alt="">
                                        </div>
                                        <div class="texto">
                                            <p class="titulo">chat online</p>
                                            <p class="texto">Em desenvolvimento.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </section>



@stop

@section('scripts')


<script src="/js/usuario_antigo/vendor/jquery.mask.min.js"></script>
<script src="/js/usuario_antigo/vendor/jquery.validate.min.js"></script>
<script src="/lib/sweetalert2/sweetalert2.all.js"></script>

<script>
@if(old('email'))
        alerta('awesome ok', 'Contato realizado!', 'Agradecemos o seu contato. Em breve você receberá a resposta da área selecionada', '', 'fa fa-check');
@elseif(old('email'))
        alerta('awesome error', 'Erro!', 'Erro ao enviar email', '', 'fa fa-times');
@endif

    $(document).ready(function(){
        if(location.href.includes("duvidas")) {
           document.querySelector(".box-conteudo.duvidas-frequentes").classList.add("active");
        } else if(location.href.includes("mensagem")) {
           document.querySelector(".box-conteudo.envio-mensagem").classList.add("active");
        }

        $('#form-fale-conosco input[name="telefone"]').mask('(00) 0000-00009');

        $('#form-fale-conosco input[name="telefone"]').keydown(function(event) {
           if(event.keycode != 8){
              if($(this).val().length >= 14){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
                $('#form-fale-conosco input[name="telefone"]').mask('(00) 00000-0000');
              } else {
                $('#form-fale-conosco input[name="telefone"]').mask('(00) 0000-00000');
              }
           }
        });
    });

    $("#form-fale-conosco input, #form-fale-conosco textarea").each(function (key, val) {
        val.onpaste = function (e) {
            e.preventDefault();
        };
    });




    $("#form-fale-conosco").submit(function(e) {
        e.preventDefault();
    }).validate({
        rules: {
            nome: {
                required: true
            },
            telefone: {
                required: true
            },
            email: {
                required: true
            },
            assunto: {
                required: true
            },
            mensagem: {
                required: true
            }
        },
        errorPlacement: function (error, element) {
        },
        submitHandler: function (form) {

            $("#btn-enviar-contato").attr('disabled', true);
            let contato = new FormData();

            contato.append('assunto', $("#fale-conosco-assunto").val());
            contato.append('mensagem', $("#fale-conosco-mensagem").val());

            $.ajax({
                url: "/loja/contato/enviar",
                type: "POST",
                data: contato,
                async: true,
                contentType: false,
                processData: false,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if(!data.erro){
                        swal({
                            type: 'success',
                            title: 'Sucesso!',
                            text: data.mensagem,
                        }).then(function () {
                            location.reload();
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                },
                error: function (x, t, m) {
                    swal({
                        type: 'error',
                        title: 'Ops! Algo aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });

        }
    });



</script>




@endsection


