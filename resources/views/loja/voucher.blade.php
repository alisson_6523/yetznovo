@extends('layout.layout-loja')

@section('title', 'Resgates')


@section('styles')
    <link href="/css/usuario_antigo/personalizado-pedidos.css" rel="stylesheet">
@endsection


@section('conteudo-loja')

    <section class="conteudo-acompanhar">
        <div class="titulo-da-pagina">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <img src="/img/icone-movimentacao.png">
                        <h1 class="titulo-pag">Acompanhe seus <strong>vouchers digitais</strong></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="conteudo-acompanhe-seu-resgate">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-3" style="display:none;">
                        <p>Selecione o mês</p>
                        <div class="form-group">
                            <select class="form-control input-lg" id="dropdown-data">
                                <option>TODOS</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        @if (count($usuario->resgates()->whereHas('carrinho', function($q){
                                $q->whereHas('itens', function($q2){
                                        $q2->whereNotNull('link_voucher')->whereNotNull('email_enviado');
                                });
                            })->where('cod_campanha', session('campanha')->cod_campanha)->orderBy('cod_resgate', 'DESC')->get()) > 0 )
                            <div class="table-responsive">
                                <table class="table table-responsive" id="tabela">
                                    <thead>
                                    <tr>
                                        <th>Nº</th>
                                        <th>RESGATE</th>
                                        <th>DATA</th>
                                        <th>STATUS</th>
                                        <th>LINK</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php
                                        $count = 0;
                                    @endphp
                                    @foreach ($usuario->resgates()->where('cod_campanha', session('campanha')->cod_campanha)->orderBy('cod_resgate', 'DESC')->get() as $resgate)
                                        @foreach ($resgate->carrinho->itens()->whereNotNull('link_voucher')->whereNotNull('email_enviado')->get() as $item)
                                            <tr>
                                                <td>{{ $resgate->registro }}</td>
                                                <td>{{ $item->produto->nome_produto }} -
                                                    R$ {{ $item->produto->valor_reais }}</td>
                                                <td>{{ $resgate->data_resgate }}</td>
                                                <td class="detalhe-pedido td{{ $count }}"
                                                    onclick="detalhes('pedido{{ $count }}', 'seta{{ $count }}', 'td{{ $count }}')">
                                                    <span>ver detalhes</span>
                                                    <i class="fa fa-caret-down caret-table seta{{ $count }}"></i>
                                                </td>
                                                <td>
                                                    <a href="{{ $item->link_voucher }}" target="_blank">Link Voucher Digital</a>
                                                </td>
                                            </tr>
                                            <tr id="pedido{{ $count }}" hidden="true">
                                                <td colspan="5">
                                                    <div class="status-pedido"
                                                         cod_status="{{ $item->cod_status }}">
                                                        @if ($item->cod_status != 5)
                                                            <ul id="status-list">
                                                                <li>
                                                                    <div class="circulo-status">Solicitado
                                                                    </div>
                                                                    <span class="num-etapa">1</span>
                                                                </li>
                                                                <li>
                                                                    <div class="circulo-status">Aprovado</div>
                                                                    <span class="num-etapa">2</span>
                                                                </li>
                                                                <li>
                                                                    <div class="circulo-status">A caminho
                                                                    </div>
                                                                    <span class="num-etapa">3</span>
                                                                </li>
                                                                <li>
                                                                    <div class="circulo-status">Entregue</div>
                                                                    <span class="num-etapa">4</span>
                                                                    <span class="previsao-entrega">
                                                                        @if(!empty($item->data_entregue))
                                                                            Previsão de entrega:
                                                                            {{ date('d/m/Y', strtotime($item->data_entregue)) }}
                                                                        @else
                                                                            previsão de entrega:
                                                                            {{ date('d/m/Y', strtotime($item->produto->tipo == 1 ? \Carbon\Carbon::now()->addDays(30) : \Carbon\Carbon::now()->addDays(15))) }}
                                                                        @endif
                                                                                      </span>
                                                                </li>
                                                            </ul>
                                                        @else
                                                            <p id="status-cancelado">Pedido Cancelado</p>
                                                        @endif
                                                    </div>
                                                </td>
                                            </tr>
                                            @php
                                                $count++;
                                            @endphp
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                        @else
                            <h1 class="aviso-vazio">Não há registro de vouchers disponíveis!</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>


        <div class="conteudo-acompanhe-seu-resgate-mobile">
            @php
                $ios = '';
                $detect = new \Mobile_Detect;
                $mobileSo = $detect->isiOS() ? 'ios' : 'android';
            @endphp

            <div class="container-cards">
                @foreach ($usuario->resgates()->where('cod_campanha', session('campanha')->cod_campanha)->orderBy('created_at', 'DESC')->get() as $resgate)
                    @foreach ($resgate->carrinho->itens()->whereNotNull('link_voucher')->whereNotNull('email_enviado')->get() as $item)
                        <div class="container-card-mobile">
                            <div class="card-mobile">
                                <div class="card-text">
                                    <h3>Nº: {{ $resgate->registro }}</h3>
                                    <p>Resgates: {{ $resgate->carrinho->itens->count() }}</p>
                                    <p>Nº de pontos Resgatados
                                        : {{ number_format($resgate->valor_resgate, '0', ',', '.') }}</p>
                                    <p>Data: {{ date('d/m/Y', strtotime($resgate->created_at)) }}</p>
                                    <p>Previsão de entrega:
                                        {{ !empty($item->data_entregue) ? date('d/m/Y', strtotime($item->data_entregue)) : date('d/m/Y', strtotime($item->produto->tipo == 1 ? \Carbon\Carbon::now()->addDays(30) : \Carbon\Carbon::now()->addDays(15))) }}
                                    </p>

                                    @if($mobileSo == "ios")
{{--                                        <br/>--}}
{{--                                        <input type="text" class="form-control" oninput="this.value = '{{ $item->link_voucher }}'" id="link-voucher-ios" value="{{ $item->link_voucher }}">--}}
                                        <div class="btn-link-voucher">
                                            <a onclick="copyToClipboard('{{ $item->link_voucher }}')" id="btn-link">Copiar Link</a>
                                        </div>
                                    @else
                                        <div class="btn-link-voucher">
                                            <a href="{{ $item->link_voucher }}" target="_blank">Link</a>
                                        </div>
                                    @endif
                                </div>

                                <div class="img-resgate-mobile">
                                    <img src="/img/mobile-resgate/resgate-01.png" alt="">
                                </div>
                            </div>

                            <div class="container-detalhe-card">
                                <div class="datelhe-card">
                                    <div class="detalhe-card-img">
                                        <img src="{{ $item->produto->link_foto }}" alt="">
                                    </div>

                                    <div class="detalhe-card-text">
                                        <p>Pedido: {{ $item->produto->nome_produto }} -
                                            R${{ $item->produto->valor_reais }}</p>
                                        <span>Pontos -
                                                {{ number_format($item->produto->valor_produto, '0', ',', '.') }}</span>
                                    </div>
                                </div>
                            </div>

                            <div class="container-status">
                                <div class="detalhe-status active">
                                    <span></span>
                                    <p>Pedido Solicitado</p>
                                </div>
                                <div class="detalhe-status active">
                                    <span></span>
                                    <p>Aguardando CONFIRMAÇÃO</p>
                                </div>
                                <div class="detalhe-status {{ $item->cod_status >= 2 ? 'active' : '' }}">
                                    <span></span>
                                    <p>Aprovado</p>
                                </div>
                                <div class="detalhe-status {{ $item->cod_status >= 3 ? 'active' : '' }}">
                                    <span></span>
                                    <p>A caminho</p>
                                </div>
                                <div class="detalhe-status {{ $item->cod_status >= 4 ? 'active' : '' }}">
                                    <span></span>
                                    <p>entregue</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endforeach
            </div>
        </div>

    </section>

@endsection

@section('scripts')

    <script type="text/javascript">
        let selects = Array.from(document.querySelectorAll('.text-select-resgate'))
        let selectsResgates = Array.from(document.querySelectorAll('.img-resgate-mobile'))

        function removeItem(select) {
            let itens = Array.from(document.querySelectorAll(select))

            itens.forEach(function (item) {
                item.parentElement.querySelector('.list-select-resgate').classList.value.includes('active') ?
                    item.parentElement.querySelector('.list-select-resgate').classList.remove('active') : null
            })
        }

        function bulletActive(bullets) {
            let index = 1;
            Array.from(bullets).forEach(function (bullet) {
                if (bullets[index]) {
                    if (bullets[index].classList.value.includes('active')) {
                        index++;
                    } else {
                        bullets[index - 1].querySelector('span').classList.add('active')
                        index++;
                    }
                }
            })
        }

        selects.forEach(function (select) {
            select.addEventListener('click', function () {
                if (select.parentElement.querySelector('.list-select-resgate').classList.value
                    .includes('active')) {
                    removeItem('.text-select-resgate')
                } else {
                    removeItem('.text-select-resgate')
                    select.parentElement.querySelector('.list-select-resgate').classList.toggle(
                        'active')
                }
            })
        })

        selectsResgates.forEach(function (selectResgate) {
            selectResgate.addEventListener('click', function () {
                let detalheCard = selectResgate.parentElement.parentElement.querySelector(
                    '.container-detalhe-card')
                let status = selectResgate.parentElement.parentElement.querySelector(
                    '.container-status')
                let altura = 0;

                //parentElement.parentElement.querySelector('.container-status .detalhe-status').classList.value.includes('active')

                let bullets = selectResgate.parentElement.parentElement.querySelectorAll(
                    '.container-status .detalhe-status')
                bulletActive(bullets);

                detalheCard.querySelectorAll('.datelhe-card').forEach(function (detalhe) {
                    altura += detalhe.offsetHeight + 2
                })


                if (detalheCard.classList.value.includes('active')) {
                    detalheCard.style.height = 0 + 'px'
                    detalheCard.classList.remove('active')
                    selectResgate.querySelector('img').classList.remove('active')
                    status.classList.remove('active')
                } else {
                    detalheCard.style.height = altura + 'px'
                    detalheCard.classList.toggle('active')
                    status.classList.toggle('active')
                    selectResgate.querySelector('img').classList.toggle('active')
                }

            })
        })


        function detalhes(id, elem, td) {
            $("#" + id + "").toggle("fast");
            if ($("." + elem + "").hasClass("fa-caret-down")) {
                $("." + elem + "").removeClass("fa-caret-down");
                $("." + elem + "").addClass("fa-caret-up");
                $("." + td + " span").text("ocultar detalhes");
            } else {
                $("." + elem + "").removeClass("fa-caret-up");
                $("." + elem + "").addClass("fa-caret-down");
                $("." + td + " span").text("ver detalhes");
            }
            pedido = $("#" + id);
            cod_status = pedido.find(".status-pedido").attr("cod_status");

            if (cod_status != 5) {
                arr_estilos = ["aguardando", "aprovado", "caminho", "entregue"];
                circulos = pedido.find(".circulo-status");
                numeros = pedido.find(".num-etapa");

                for (i = 0; i < cod_status; i++) {
                    circulos[i].classList.add("status-" + arr_estilos[i]);
                    numeros[i].classList.add("num-" + arr_estilos[i]);
                }
            }
            //                showStatus(id);
        }

        function showStatus(pedido) {
            pedido = $("#" + pedido);
            cod_status = pedido.find(".status-pedido").attr("cod_status");

            if (cod_status != 5) {
                arr_estilos = ["aguardando", "aprovado", "caminho", "entregue"];
                circulos = pedido.find(".circulo-status");
                numeros = pedido.find(".num-etapa");

                for (i = 0; i <= cod_status; i++) {
                    circulos[i].classList.add("status-" + arr_estilos[i]);
                    numeros[i].classList.add("num-" + arr_estilos[i]);
                }
            }
        };

        $("#link-voucher-ios").click(function(){
            $('#link-voucher-ios').select();
        });


        function copyToClipboard(string) {
            let textarea;
            let result;

            try {
                textarea = document.createElement('textarea');
                textarea.setAttribute('readonly', true);
                textarea.setAttribute('contenteditable', true);
                textarea.style.position = 'fixed'; // prevent scroll from jumping to the bottom when focus is set.
                textarea.value = string;

                document.body.appendChild(textarea);

                textarea.select();

                const range = document.createRange();
                range.selectNodeContents(textarea);

                const sel = window.getSelection();
                sel.removeAllRanges();
                sel.addRange(range);

                textarea.setSelectionRange(0, textarea.value.length);
                result = document.execCommand('copy');
            } catch (err) {
                console.error(err);
                result = null;
            } finally {
                document.body.removeChild(textarea);
            }

            // manual copy fallback using prompt
            if (!result) {
                const isMac = navigator.platform.toUpperCase().indexOf('MAC') >= 0;
                const copyHotkey = isMac ? '⌘C' : 'CTRL+C';
                result = prompt(`Press ${copyHotkey}`, string); // eslint-disable-line no-alert
                if (!result) {
                    return false;
                }
            }

            $("#btn-link").html('Link Copiado!');
            $("#btn-link").css({'color':'white'});
            $(".btn-link-voucher").css({'background-color':'#5cb85c'});
            setTimeout(()=>{
                $("#btn-link").html('Copiar Link');
                $("#btn-link").css({'color':'#6d7f89'});
                $(".btn-link-voucher").css('background-color', '#f0f3f5');
            }, 5000);
        }

    </script>
@endsection
