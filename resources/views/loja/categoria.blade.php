@extends('layout.layout-loja')

@section('title', 'Resgate Seus Pontos')

@section('styles')

<!-- CSS Personalizado -->
<link href="/css/usuario_antigo/personalizado-produto.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.1.1/rangeslider.min.css">

@stop

@section('conteudo-loja')

<section class="filtro">
      <div class="container">
            <div id="bloco-categoria" class="col-xs-12 col-sm-12 col-md-3">
                  <div class="nome-categoria-novo">
                        <a href="/loja" class="voltar-mobile">
                              <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </a>

                        <img src="/img/menu_categoria.svg#{{ $categoria->icone_categoria ?  $categoria->icone_categoria :  'ver-todos' }}" alt="">
                        <span class="nome-categoria">{{ $categoria->categoria ? $categoria->categoria : 'Todas Categorias' }}</span>
                  </div>
            </div>
            <div class="hidden-sm hidden-xs col-md-9 bloco-busca">
                  <div class="input-group input-group-pesquisa">
                        <input type="text" class="form-control input-pesquisa" aria-describedby="basic-addon2"
                              placeholder="BUSCA">
                        <span class="input-group-addon" id="basic-addon2"><i
                                    class="glyphicon glyphicon-search"></i></span>
                  </div>
                  <select name="cartao-tipo" class="form-control" id="cartao-tipo">
                        <option value="todos">TODOS</option>
                        <option value="fisico">CARTÃO FÍSICO</option>
                        <option value="voucher">VOUCHER DIGITAL</option>
                  </select>
                  <select name="ordenacao" class="form-control input-ordenacao" id="ordernar">
                        <option value="todospreco">TODOS</option>
                        <option value="maiorpreco">MAIOR PREÇO</option>
                        <option value="menorpreco">MENOR PREÇO</option>
                  </select>
                  <div class="btn-group" id="dropdown-precos">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false">
                              Definir valores
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right">
                              <li id="input-menor-preco">
                                    <div class="input-group">
                                          <span class="input-group-addon">YETZ</span>
                                          <input class="form-control" type="text" />
                                    </div>
                              </li>
                              <li id="separador-lista-preco"><span>-</span></li>
                              <li id="input-maior-preco">
                                    <div class="input-group">
                                          <span class="input-group-addon">YETZ</span>
                                          <input class="form-control" type="text" />
                                    </div>
                              </li>
                              <li id="lista-menores-valores">
                                    <a>500</a>
                                    <a>1.000</a>
                                    <a>1.250</a>
                                    <a>1.500</a>
                                    <a>2.000</a>
                                    <a>2.500</a>
                                    <a>5.000</a>
                                    <a>10.000</a>
                                    <a>20.000</a>
                                    <a>25.000</a>
                                    <a>50.000</a>
                              </li>
                              <li id="lista-maiores-valores">
                                    <a>Todos os valores</a>
                                    <a>500</a>
                                    <a>1.000</a>
                                    <a>1.250</a>
                                    <a>1.500</a>
                                    <a>2.000</a>
                                    <a>2.500</a>
                                    <a>5.000</a>
                                    <a>10.000</a>
                                    <a>20.000</a>
                                    <a>25.000</a>
                                    <a>50.000</a>
                              </li>
                        </ul>
                  </div>
            </div>
      </div>
</section>

<section class="todos-prods-categorias">
      <div class="container">
            <div class="row">
                  @if ($produtos->count() > 0)
                  <div class="categoria">
                        <!-- Swiper -->
                        <div class="prod-destaques">
                              @foreach ($produtos as $p)
                              @php
                              $termometro = $p->termometro($usuario->saldo_usuario, $carrinho->valorCarrinho);
                              $desejados  = $p->styleDesejados($usuario->desejados);
                              @endphp
                              <div class="box-produto" data-cod-produto="{{ $p->cod_produto }}"
                                    data-tipo="{{ $p->tipo }}" data-preco="{{ $p->valor }}">

                                    <div class="informacoes-produto">
                                          <h1 class="sr-only">{{ $p->nome_produto }}</h1>
                                          <a href='/loja/produto/{{ $p->cod_produto }}'>
                                                <img src="{{ $p->link_foto }}"
                                                      alt="{{ $p->nome_produto }} - R${{ $p->valor_reais }}"
                                                      title="{{ $p->nome_completo }}" class="img-responsive">
                                          </a>
                                          <a title="{{ $desejados['txt'] }}"
                                                class="btn-detalhes btn-favoritar {{ $desejados['like'] }}"
                                                onclick="favorito('{{ $p->cod_produto }}', this)">
                                                <i class="icone-icon_lamp"></i></a>
                                          <div class="preco-produto">
                                                <h3><span>R$</span>{{ $p->valor_reais }}</h3>
                                          </div>
                                    </div>
                                   <div class="valor-produto-mobile">
                                        <h3>{{ $p->nome_produto }}  <span>R$</span>{{ $p->valor_reais }}</h3>
                                   </div>
                                    <div class="informacoes">
                                          <div class="area-selos">
                                                @foreach ($p->selos as $s)
                                                <div class="selo {{ $s['class'] }}">
                                                      <span><img src="{{ $s['icone'] }}" alt=""></span>
                                                </div>
                                                @endforeach
                                          </div>
                                          <div class="area-valores {{ $termometro['classe_nova'] }}">
                                                <div class="valores">
                                                      <div class="progress">
                                                            <div class="progress-bar" role="progressbar"
                                                                  aria-valuenow="{{ $termometro['result'] >= 0 ? $termometro['result'] : 0  }}"
                                                                  aria-valuemin="0" aria-valuemax="100"
                                                                  style="width:{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}%">
                                                                  <span class="sr-only {{ $termometro['classe'] }}">{{ $termometro['result'] >= 0 ? $termometro['result'] : 0 }}%
                                                                        Complete</span>
                                                            </div>
                                                      </div>
                                                      <h1 class="saldo {{ $termometro['classe'] }}">
                                                            {{ $p->valor }}<span> YETZ</span></h1>
                                                </div>
                                                <a title="{{ $termometro['result'] >= 100 ? "Adicionar esse cartão ao carrinho" : "Você não possui pontos suficientes para resgatar esse item" }}"
                                                      class="btn-adiciona-produto {{ $termometro['corCarrinho'] }} "
                                                      cod-item="{{ $p->cod_produto }}">
                                                      <span>adicionar</span>
                                                </a>
                                          </div>
                                    </div>
                              </div>
                              @endforeach
                        </div>
                  </div>
                  @else
                  <h1 class="texto-nao-possui-produto">Esta categoria não possui produtos!</h1>
                  @endif
            </div>
      </div>
</section>


@stop

@section('scripts')
<script src="{{ asset('js/usuario_antigo/loja/categoria.js') }}"></script>
@stop
