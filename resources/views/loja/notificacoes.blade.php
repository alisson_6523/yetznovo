@extends('layout.layout-loja')

@section('title', 'Central de Notificações | Home')

@section('conteudo-loja')


        <section class="titulo-da-pagina titulo-notificacoes">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <img src="/img/icone-sino.png" class="img-responsive">
                        <h1>TODAS AS <strong>NOTIFICAÇÕES</strong></h1>
                    </div>
                </div>
            </div>
        </section>

        <section class="todas-notificacoes-all">
            <div class="container">
                <div class="row">

                    @foreach ($usuario->notifications()->orderBy('created_at', 'DESC')->get() as $notificacao)
                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <a href="#"  data-toggle="modal">
                                <div class="box-notificacao {{ $notificacao->data['class'] }}" notificacao_id="{{ $notificacao->id }}">
                                    <div class="lado-esq">
                                        <div class="box-imagem"></div>
                                        <span>{{ $notificacao->creted_at }}</span>
                                    </div>
                                    <div class="lado-dir">
                                        <div class="texto">
{{--                                            <p class="tipo-notificacao"><strong>{{ $notificacao->data['topo'] }}</strong></p>--}}
                                            <p class="titulo-notificacao"><strong>{{ $notificacao->data['titulo'] }}</strong></p>
                                            <p class="mensagem">{{ $notificacao->data['mensagem'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                    @foreach ($usuario->notificacoes_gerais()->orderBy('created_at', 'DESC')->get() as $notificacao)

                        @php
                            $n = $notificacao->notificacao();
                        @endphp

                        <div class="col-xs-12 col-sm-6 col-md-4">
                            <a href="#"  data-toggle="modal">
                                <div class="box-notificacao {{ $n['class'] }}" notificacao_id="{{ $notificacao->id }}">
                                    <div class="lado-esq">
                                        <div class="box-imagem"></div>
                                        <span>{{ $notificacao->creted_at }}</span>
                                    </div>
                                    <div class="lado-dir">
                                        <div class="texto">
                                            <p class="titulo-notificacao"><strong>{{ $n['titulo'] }}</strong></p>
                                            <p class="mensagem">{{ $n['mensagem'] }}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach

                </div>
            </div>
        </section>


@stop
