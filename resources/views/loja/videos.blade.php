@extends('layout.layout-loja')

@section('title', 'Streaming')

@section('styles')
<!-- CSS Personalizado -->
<link href="/css/usuario_antigo/personalizado-videos.css" rel="stylesheet">

@endsection

@section('conteudo-loja')

<section class="filtro s-videos">
      <div class="container">
            <div class="col-xs-4 col-md-4">
                  <img src="/img/icon-notificacao.png" class="icon-videos" /><span>STREAMING</span>
            </div>
            {{--                <div class="col-xs-8 col-md-8 bloco-filtros">--}}
            {{--                    <div class="input-group input-group-pesquisa">--}}
            {{--                        <input type="text" class="form-control input-pesquisa" aria-describedby="basic-addon2">--}}
            {{--                        <span class="input-group-addon" id="basic-addon2"><i class="glyphicon glyphicon-search"></i></span>--}}
            {{--                    </div>--}}
            {{--                    <select name="ordenacao" class="input-group form-control input-ordenacao" id="ordernar">--}}
            {{--                        <option value="todospreco">TODOS</option>--}}
            {{--                        <option value="0">teste</option>--}}
            {{--                        <option value="0">teste</option>--}}
            {{--                    </select>--}}
            {{--                </div>--}}
      </div>
</section>

<section class="videos">
      <div class="container container-videos">
            <div class="row">
                  @if(count($usuario->videos) > 0)
                  @foreach ($usuario->videos as $video)
                  <div class="col-sm-6 col-md-4 col-lg-3">
                        <div class="card-video" data-tipo="{{ $video->tipo }}">
                              <div class="content-card">
                                    <div class="img-video">
                                          <img src="{{ $video->foto_capa }}" />
                                    </div>
                                    <div class="title-video">
                                          <p>{{ $video->titulo }}</p>
                                    </div>
                                    <div class="description-video">
                                          <p>{{ $video->descricao }}</p>
                                    </div>
                                    <div>
                                          @if ($video->extensao == "mp4")
                                          <a class="btn-assistir" link="{{ $video->link }}"
                                                onclick="marcarLido({{ $video->cod_video }})">
                                                <div class="content-btn-assistir">
                                                      <img src="{{ asset('img/icon-assistir.png') }}"
                                                            class="icon-videos" />
                                                      <span>ASSISTIR</span>
                                                </div>
                                          </a>
                                          @elseif ($video->extensao == "mp3")
                                          <div id="audioplayer">
                                                <audio class="media">
                                                      <source src="{{ $video->link }}">
                                                </audio>
                                                <button class="pButton play"
                                                      onclick="marcarLido({{ $video->cod_video }})"></button>
                                                <div class="timeline">
                                                      <div class="playhead">
                                                            <span class="tempo-corrente">00:00</span>
                                                      </div>
                                                </div>
                                          </div>
                                          @endif
                                    </div>
                              </div>
                        </div>
                  </div>
                  @endforeach
                  @else

                  <h3>NÃO HÁ MÍDIAS DISPONÍVEIS NO MOMENTO.</h3>

                  @endif
            </div>
      </div>



</section>

<div class="modal-quantidade">
      <div class="container-modal">
            <div class="container-descricao">

            </div>

            <div class="close-modal-quantidade">
                  <img src="/Assets/Images/Institucional/modal-02.png" alt="">
            </div>
      </div>
</div>

<!-- <div class="modal fade" tabindex="-1" role="dialog" id="modal-media">
      <div class="modal-dialog" role="document">
            <div class="modal-content">
                  <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">
                                    <img src="{{ asset('img/icon-close-video.png') }}" class="btn-close" />
                              </span>
                        </button>
                  </div>
            </div>
      </div>
</div> -->


@endsection

@section('scripts')

<script type="text/javascript">
$(".btn-assistir").click(function() {
      $(".videos").find(".pause").click();
      file = $(this).attr("link");
      // $("#modal-media").modal("show");
	$('.modal-quantidade').addClass('active')
	$('.container-descricao').empty();
	$(".container-descricao").append('<video controls controlsList="nodownload" autoplay></video>');
      $("video").append('<source src="' + file + '" type="video/mp4" />');
});

let modalVideo = document.querySelector('.modal-quantidade')

modalVideo.addEventListener('click', () => {
		modalVideo.classList.toggle('active')
		if(!modalVideo.classList.value.includes('active')){
			$('.container-descricao').empty();
		}
      })


$("#modal-media").on("show.bs.modal", function() {

});

$("#modal-media").on("hide.bs.modal", function() {
      $("#conteudo-midia").remove();
});

function marcarLido(id) {
      let video = new FormData();
      video.append('video_id', id);

      $.ajax({
            url: window.location.pathname + "/marcar-visto",
            type: "POST",
            data: video,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {
                  console.log(data);
            },
            error: function(x, t, m) {
                  console.log("erro=> " + m);
            }
      });
}
</script>

@endsection
