<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Minhas notificações</title>
</head>

<body>

    <h1>Minhas notificações</h1>
    @if($usuario->notifications)
    <p>{{ $usuario->notifications->count() }} notificações</p>
    @foreach ($usuario->notifications or [] as $n)
    {{-- @dd($n->data) --}}
    <pre>[{{ $n->type }}] - {{ $n->data['title'] }}</pre>
    <hr>
    @endforeach
    @else
    <p>Não há notificações</p>
    @endif

    <ul>
        <li><a href="/notification/send">Enviar notificação teste</a></li>
        <li>
            <form action="/notification/personalizada" method="post">
                {{ csrf_field() }}
                <input type="text" name="title" placeholder="título" id="">
                <input type="text" name="mensagem" placeholder="mensagem" id="">
                <input type="submit" value="Enviar notificação personalizada">
            </form>

        </li>
    </ul>


</body>

</html>