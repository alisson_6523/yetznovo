<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>
    <!-- Global site tag (gtag.usuario_antigo) - Google Analytics -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0"/>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131006548-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('usuario_antigo', new Date());

        gtag('config', 'UA-131006548-1');
    </script>

@isset($campanha->analytics)
    <!-- Campanha site tag (gtag.usuario_antigo) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id={{ $campanha->analytics }}"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('usuario_antigo', new Date());

            gtag('config', '{{ $campanha->analytics }}');
        </script>

    @endisset


    <title>Yetz Cards | @yield('title')</title>

    <link rel="stylesheet" href="/css/usuario_antigo/bootstrap.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/swiper.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">

    <link rel="stylesheet" href="/css/usuario_antigo/font-yetz.css">
    <link rel="stylesheet" href="/css/usuario_antigo/amaran.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/impressao.css" media="print">
    <link rel="stylesheet" href="/css/usuario_antigo/flaticon.css">
    <link rel="stylesheet" href="/css/usuario_antigo/main.css?{{ config('app.versao_cache') }}">
    <link rel="stylesheet" href="/css/usuario_antigo/responsivo.css?{{ config('app.versao_cache') }}">


    <script src="/js/usuario_antigo/vendor/modernizr-2.8.3.min.js?{{ config('app.versao_cache') }}"></script>

    <link rel="shortcut icon" href='{{ asset("img/favicon.png") }}' type="image/png"/>
    <link rel="stylesheet" href="/css/usuario_antigo/personalizado-carrinho.css?{{ config('app.versao_cache') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('styles')
    <link rel="stylesheet" href="/css/mobile/app.css?{{ config('app.versao_cache') }}">

    @php
        $detect = new \Mobile_Detect;
    @endphp
</head>

<body id="content-home">

@if( \Request::is('loja'))
    <div class="loader">
        <i class="fa fa-spinner fa-spin"></i>
    </div>
@endif

<header class="header-novo">
    <div class="container topo-menu">
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-3 area-logo personalizado">
                <div class="logo-menu-novo">
                    <a href="/loja"><img src="/img/logo-menu-novo.png"></a>
                    @if(!empty($campanha->logo))
                        <img src="{{ \Storage::cloud()->temporaryUrl($campanha->logo_hash, \Carbon\Carbon::now()->addMinutes(1)) }}">
                    @endif
                </div>
            </div>
            <div class="col-xs-7 col-sm-7 col-md-9 area-informacoes personalizado">
                <div class="todas-informacoes">
                    <a class="icone-menu">
                        <img src="/img/menu.svg" onclick="toggleMenu()">
                    </a>
                    <div class="usuario-novo">
                        <img src="{{ $usuario->foto_usuario }}" class="foto-usuario" alt="">
                        <span class="nome-usuario"> {{ $usuario->apelido }} </span>
                        <div class="bloco-icones-menu">
                            <a class="btn-alerta">
                                <img src="/img/icone-sino.svg">
                                <div class="num"><span class="num-notificacoes"></span></div>
                            </a>
                            <a href="/loja/videos">
                                <img src="/img/icone-youtube.svg">
                                @if($usuario->videos->count() && ($usuario->videos->count() - $usuario->countVideosVistos) != 0)
                                    <div class="num">
                                        <span class="num-videos">{{ ($usuario->videos->count() - $usuario->countVideosVistos) }}</span>
                                    </div>
                                @endif
                            </a>
                        </div>
                    </div>
                    <div class="saldo-carrinho">
                        <button class="btn-voucher"><img src="/img/icone-voucher.svg"></button>

                        <div class="saldo-novo sem-borda-verde">
                            <h1>
                                <span class="texto">saldo</span>
                                <span class="valor">
                                    <strong id="saldoCabecalho">{{ number_format((\App\Models\Campanha::find(session('campanha')->cod_campanha)->pontos_temporarios ? $usuario->saldo_temporario_usuario + $usuario->saldo_usuario : $usuario->saldo_usuario), 0, ',', '.') }} </strong>
                                </span>
                                <span class="unidade">YETZ</span>
                            </h1>
                        </div>

                        <div class="carrinho">
                            <div class="btn-carrinho-compras">
                                <button type="button" class="btn-carrinho" id="btn-carrinho">
                                    <img src="/img/icone-carrinho.png">
                                    <span class="texto-carrinho">Carrinho</span>
                                    <span class="num-itens-carrinho">{{  $carrinho->itens->count() != 0 ? '- '. str_pad($carrinho->itens->count(), 2, "0", STR_PAD_LEFT) : '' }}</span>
                                    <img src="/img/arrow-down-branco.svg"
                                         class="seta-dir-menu-novo">
                                </button>
                                <div class="resumo-carrinho sumir">
                                    <div id="produtosCarrinho">
                                        @php
                                            $valorTotal = 0;
                                        @endphp
                                        @if (!$carrinho->itens->count())
                                            <h1 class="aviso-vazio">Carrinho vazio!</h1>
                                        @else
                                            @foreach ($carrinho->produtos->groupBy('cod_produto') as $p)
                                                @php
                                                    $valor = round(($campanha->valor_pontos != 0 ?
                                                    $p->first()->valor_reais / $campanha->valor_pontos :
                                                    $p->first()->valor_produto));
                                                    $valorTotal += $valor * count($p);
                                                @endphp
                                                <div class="produto-carrinho"
                                                     cod-item="{{ $p->first()->cod_item }}"
                                                     cod-produto="{{ $p->first()->cod_produto }}">
                                                    <div class="valor-produto">
                                                        <img class="foto-produto-carrinho"
                                                             src="{{ $p->first()->link_foto }}">
                                                        <div class="texto-carrinho">
                                                            <p>{{ $p->first()->categoria->categoria }}
                                                            </p>
                                                            <p>{{ $p->first()->nome_produto }} -
                                                                R$
                                                                {{ $p->first()->valor_reais }}
                                                            </p>
                                                        </div>
                                                        <div class="btns">
                                                                                    <span>
                                                                                          <i class="fa fa-shopping-basket"
                                                                                             aria-hidden="true"></i>
                                                                                          <span
                                                                                                  class="qtd-produto">{{ count($p) }}</span>
                                                                                    </span>
                                                            <span><i class="fa fa-trash-o btn-excluir"
                                                                     aria-hidden="true"></i></span>
                                                        </div>
                                                        <div class="valor pull-right">
                                                            <span>{{ $valor }} yetz</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                    <div class="area-total">
                                        <a href="/loja/carrinho"
                                           class="btn-resgatar-carrinho">Visualizar Carrinho</a>
                                        <div class="valor-total pull-right">
                                            <span>Total: {{ $valorTotal }} yetz</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if(\App\Models\Campanha::find(session('campanha')->cod_campanha)->pontos_temporarios)
                            <div class="container-hover">
                                <div class="pts-bloqueado">
                                    <img src="/img/hover-01.png" alt="">

                                    <div class="container-text">
                                        <h3>ESTES PONTOS PODEM SER SEUS!</h3>
                                        <p>Alcance a meta de pontos trimestral da campanha, depois é só escolher quais
                                            prêmios vai resgatar!</p>

                                        <span>DIGA YETZ PARA SEUS DESEJOS…</span>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="todas-categorias">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <button id="btn-categoria" class="btn-categorias">todas<br>categorias <img
                                src="/img/seta-baixo-categoria.svg" class="normal"
                                id="seta-categoria"></button>
                    <div class="seta-esq-slide-categoria">
                        <img src="/img/seta-esq-slide-categoria.svg">
                    </div>
                    <div class="menu-categorias">
                        <div class="swiper-wrapper">
                            @foreach ($categorias as $i => $c)
                                <div class="swiper-slide {{ ($categoria != null && $c->link_categoria == $categoria->link_categoria) ? 'active-categoria' : '' }}"
                                     title="{{ $c->categoria }}">
                                    <a href="/loja/{{ $c->link_categoria }}">
                                                            <span class="{{ $c->icone_categoria }}"
                                                                  style="background-image: url({{ asset('img/menu_categoria.svg') }}#{{ $c->icone_categoria }});"></span>
                                        <span class="{{ $c->icone_categoria . '-amarelo' }}"
                                              style="background-image: url({{ asset('img/menu_categoria.svg') }}#{{ $c->icone_categoria . '-amarelo' }});"></span>
                                    </a>
                                </div>
                            @endforeach
                            <div class="swiper-slide" title="TODOS OS PRODUTOS">
                                <a href="/loja/todos">
                                                            <span class="ver-todos"
                                                                  style="background-image: url({{ asset('img/menu_categoria.svg') }}#ver-todos);"></span>
                                    <span class="ver-todos-amarelo"
                                          style="background-image: url({{ asset('img/menu_categoria.svg') }}#ver-todos-amarelo);"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="seta-dir-slide-categoria" title="VER MAIS">
                        <img src="/img/seta-dir-slide-categoria.svg">
                    </div>
                </div>
            </div>
        </div>
        <div class="dropdown-todas-categorias sumir">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        @foreach ($categorias as $index => $c)
                            @if (($index + 1) % 5 == 1)
                                <div class="categorias-divido">
                                    <ul>
                                        @endif
                                        <li>
                                            <a href="/loja/{{ $c->link_categoria }}">
                                                <img src="/img/menu_categoria.svg#{{ $c->icone_categoria . '-amarelo' }}"
                                                     alt="">
                                                <span
                                                        class="{{ $c->icone_categoria }}"></span>{{ $c->categoria }}
                                            </a>
                                        </li>
                                        @if ((($index + 1) % 5 == 0) && ($index + 1) < count($categorias))
                                    </ul>
                                </div> @endif @endforeach
                        <li>
                            <a href="/loja/todos">
                                <img src="/img/menu_categoria.svg#ver-todos-amarelo"
                                     alt="">
                                <span class="ver-todos"></span>Ver Todos
                            </a>
                        </li>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div id="home{{ \Request::is('loja') ? '-mobile' : '' }}">
    <div class="informacoes-usuario-responsivo">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
                    <div class="foto-usuario">
                        <img src="{{ $usuario->foto_usuario }}" class="img-responsive">
                    </div>
                    <div class="nome-usuario">
                        <h1>{{ $usuario->abreviacao }}</h1>
                        <p>você tem pontos pra trocar</p>

                        <h2>{{ number_format((\App\Models\Campanha::find(session('campanha')->cod_campanha)->pontos_temporarios ? $usuario->saldo_temporario_usuario + $usuario->saldo_usuario : $usuario->saldo_usuario), 0, ',', '.') }} YETZ</h2>

                        <img src="/img/seta-baixo-responsivo.png" class="seta-baixo-responsivo">

                        <button type="button" class="btn btn-resgatar-pts" data-toggle="modal"
                                data-target="#modalCategoriasGerais">resgatar pontos
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="cards-mobile">
        <h2>Categorias</h2>

        <div class="container-cards">

            @foreach($categorias as $categoria)
                <a href="/loja/{{ $categoria->link_categoria }}">
                    <div class="card-mobile">
                        <img src="/img/mobile/{{ $categoria->icone_categoria }}.png" alt="">
                        <span>{{ $categoria->categoria }}</span>
                    </div>
                </a>
            @endforeach

            <a href="/loja/todos">
                <div class="card-mobile">
                    <img src="/img/mobile/todos.svg" alt="">
                    <span>TODAS CATEGORIAS</span>
                </div>
            </a>

        </div>
    </div>


@yield('conteudo-loja')

<!--Começo da FOOTER-->
    <section class="saldo-carrinho-responsivo">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-xs-7 pontos">
                    <span class="titulo">saldo</span>
{{--                    <span class="saldo">{{ number_format($usuario->saldo_usuario, 0, ',', '.') }}</span>--}}
                    <span class="saldo">{{ number_format((\App\Models\Campanha::find(session('campanha')->cod_campanha)->pontos_temporarios ? $usuario->saldo_temporario_usuario + $usuario->saldo_usuario : $usuario->saldo_usuario), 0, ',', '.') }}</span>
                    <span class="texto-yetz">yetz</span>
                </div>
                <div class="bloco-carrinho">
                    <a class="btn-carrinho">
                        <span class="qtd-itens">{{ $carrinho->itens->count() }}</span>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <div class="resumo-carrinho-mobile">
        <div id="produtosCarrinho">
            @php
                $valorTotal = 0;
            @endphp
            @if (!$carrinho->itens->count())
                <h1 class="aviso-vazio">Carrinho vazio!</h1>
            @else
                @foreach ($carrinho->produtos->groupBy('cod_produto') as $p)
                    @php
                        $valor = round(($campanha->valor_pontos != 0 ? $p->first()->valor_reais / $campanha->valor_pontos : $p->first()->valor_produto));
                        $valorTotal += $valor * count($p);
                    @endphp
                    <div class="produto-carrinho" cod-item="{{ $p->first()->cod_item }}"
                         cod-produto="{{ $p->first()->cod_produto }}">
                        <div class="valor-produto">
                            <img class="foto-produto-carrinho" src="{{ $p->first()->link_foto }}">
                            <div class="bloco-informacoes">
                                <div class="bloco-texto">
                                    <div class="texto-carrinho">
                                        <p>{{ $p->first()->categoria->categoria }}</p>
                                        <p>{{ $p->first()->nome_produto }} -
                                            R${{ $p->first()->valor_reais }}</p>
                                    </div>
                                    <div class="btns">
                                                      <span>
                                                            <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                                                            <span class="qtd-produto">{{ count($p) }}</span>
                                                      </span>
                                        <span>
                                                            <i class="fa fa-trash-o btn-excluir" aria-hidden="true"></i>
                                                      </span>
                                    </div>
                                    <div class="valor">
                                        <span>{{ $valor }} YETZ</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
        <div class="area-total">
        @if($carrinho->itens->count() > 0)
            <!-- <a href="/loja/carrinho" class="btn-resgatar-carrinho">Visualizar Carrinho</a> -->

                <div class="valor-total pull-right">
                    <span>Total: {{ $valorTotal }} yetz</span>
                </div>
            @endif
        </div>
    </div>
    <section class="topo-produto">
        <h1>resgate <strong>prêmios</strong></h1>
        <div class="etapas">
            <div class="etap">
                <img src="/img/icone-conhecimento.png" class="icone-conhecimento">
                <p>GANHE PONTOS YETZ <br>POR RECONHECIMENTO</p>
                <img class="seta-etapas" src="/img/seta.png">
            </div>
            <div class="etap">
                <img src="/img/etapa02.png">
                <p class="texto-simples">acumule</p>
                <img class="seta-etapas" src="/img/seta.png">
            </div>
            <div class="etap">
                <img src="/img/etapa03.png">
                <p>troque por prêmios</p>
            </div>
        </div>
    </section>


    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <div class="conteudo">
                            <ul class="item">
                                <li>
                                    <a href="/loja">Página Inicial</a>
                                </li>
                                <li>
                                    <a href="{{ action('LojaController@conheca') }}">Conheça
                                        YETZ CARDS</a>
                                </li>
                                <li>
                                    <a href="{{ action('LojaController@listaDesejos') }}">Lista
                                        de Desejos</a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li>
                                    <a href="{{ action('LojaController@movimentacao') }}">Extrato
                                        de Pontos YETZ</a>
                                </li>
                                <li>
                                    <a href="{{ action('LojaController@resgates') }}">Acompanhar
                                        Resgate</a>
                                </li>
                                <li>
                                    <a href="{{ action('LojaController@perfil') }}">Meu
                                        Perfil</a>
                                </li>
                            </ul>
                            <ul class="item">
                                <li>
                                    <a href="{{ action('LojaController@contato') }}">Dúvidas
                                        Frequentes</a>
                                </li>
                                <li>
                                    <a href="/loja/termos-condicoes">Termos e Condições de
                                        Uso</a>
                                </li>
                                @if($campanha->fale_conosco)
                                    <li>
                                        <a href="{{ action('LojaController@contato') }}">Fale
                                            Conosco</a>
                                    </li>
                                @endif
                            </ul>
                            <div class="apps">
                                <span>Baixe o aplicativo YETZ CARDS:</span>
                                <ul>
                                    <li><a href="https://itunes.apple.com/us/app/yetz/id1229861934?l=pt&ls=1&mt=8"
                                           target="_blank"><img
                                                    src="/img/app-store.png"></a></li>
                                    <li><a href="https://play.google.com/store/apps/details?id=com.QuatroMais.Yetz"
                                           target="_blank"><img
                                                    src="/img/google-play.png"></a></li>
                                </ul>
                            </div>
                            <div class="site-seguro">
                                <a href="#"
                                   onclick="window.open('https://www.sitelock.com/verify.php?site=yetzcards.com.br','SiteLock','width=600,height=600,left=160,top=170');"><img
                                            class="img-responsive" alt="SiteLock" title="SiteLock"
                                            src="//shield.sitelock.com/shield/yetzcards.com.br"/></a>
                            </div>
                        </div>
                    </nav>

                    <img src="/img/logo-rodape.png" class="img-responsive logo-rodape">

                    <p>Este site é melhor visualizado em: Microsoft Edge 41.16299.371.0, Firefox
                        2.0.0.2, Safari 3.1.2,
                        Google Chrome 1.0 ou suas versões mais recentes.<br>
                        ® YETZ MARKETING, INCENTIVO E RELACIONAMENTO LTDA. | 2017 | Todos os direitos
                        reservados - CNPJ:
                        28.325.166/0001-05.</p>
                </div>
            </div>
        </div>
    </footer>

@include('includes.modal-notificacao')
@include('includes.modal-voucher')

@include('includes.menu-notificacao')

<!--Fim da FOOTER-->
    <div id="menu">
        <div class="content">
            <a class="btn-close" onclick="toggleMenu()"></a>
            <div class="box-itens">
                <div class="bloco-imagem">
                    <a href="{{ action('LojaController@perfil') }}">
                        <img src="{{ $usuario->foto_usuario }}" class="foto-usuario" alt="">
                    </a>
                </div>
                <div class="bloco-nome">
                    <span class="nome-usuario">{{ $usuario->abreviacao }}</span>
                    <img src="/img/seta-baixo-categorias.png" alt="">
                </div>
                <div class="bloco-icones">
                    <a class="btn-alerta">
                        <img src="/img/icone-sino.svg">
                        <div class="num"><span class="num-notificacoes"></span></div>
                    </a>

                    <a href="/loja/videos">
                        <img src="/img/icone-youtube.svg">
                        @if($usuario->videos->count() && ($usuario->videos->count() - $usuario->countVideosVistos) != 0)
                            <div class="num">
                                <span class="num-videos">{{ ($usuario->videos->count() - $usuario->countVideosVistos) }}</span>
                            </div>
                        @endif
                    </a>

                    <div class="btn-mobile-voucher">
                        <button class="btn-voucher"><img src="/img/icone-voucher.svg"></button>
                    </div>
                    {{-- <a title="EM BREVE">
<img src="/img/icone-controle.svg">
<!--<div class="num"><span class="num-games">200</span></div>-->
</a> --}}
                </div>
                <ul class="bloco-links">
                    <li class="link-yetz">
                        <a href="/loja/conheca">
                            <div class="icone-link"></div>
                            <span>conheça yetz cards</span>
                        </a>
                    </li>
                    <li class="link-perfil">
                        <a href="/loja/perfil">
                            <div class="icone-link"></div>
                            <span>meu perfil</span>
                        </a>
                    </li>
                    <li class="link-desejos">
                        <a href="/loja/lista-desejos">
                            <div class="icone-link"></div>
                            <span>lista de desejos</span>
                        </a>
                    </li>
                    <li class="link-campanhas">
                        <a href="/loja/regulamento">
                            <div class="icone-link"></div>
                            <span>minha campanha</span>
                        </a>
                    </li>
                    <li class="link-pontos">
                        <a href="/loja/movimentacao">
                            <div class="icone-link"></div>
                            <span>extrato de pontos</span>
                        </a>
                    </li>
                    <li class="link-resgates">
                        <a href="/loja/resgates">
                            <div class="icone-link"></div>
                            <span>acompanhar resgates</span>
                        </a>
			  </li>

                    <li class="link-voucher">
                        <a href="/loja/vouchers-digitais">
                            <div class="icone-link"></div>
                            <span>meus  vouchers digitais</span>
                        </a>
                    </li>
                    @if($campanha->fale_conosco)
                        <li class="link-fale-conosco">
                            <a href="/loja/contato">
                                <div class="icone-contato"></div>
                                <span>fale conosco</span>
                            </a>
                        </li>
                    @endif
                    <li class="link-sair">
                        <a href="{{ action('LoginController@logout') }}">
                            <div class="icone-link"></div>
                            <span>sair</span>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>

    @include('includes.modal-loja')

    @if(!empty($usuario->popups))
        @foreach($usuario->popups as $key => $popup)
            <div class="modal fade" tabindex="-1" style="z-index: {{ 2000 - $key }};"  role="dialog" class="modal-notificacao-popup" id="modal-popup-ilha">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal">
                            <img src="{{ asset('img/popup/icone-fechar.png') }}">
                        </button>
                        <div class="modal-body">
                            <img src="{{ $popup->link_foto_popup }}" class="img-responsive">
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>
<script src="{{ asset('js/usuario_antigo/vendor/jquery-1.11.2.min.js') }}"></script>
<script src="{{ asset('js/usuario_antigo/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/usuario_antigo/vendor/swiper.min.js') }}"></script>
<script src="{{ asset('js/usuario_antigo/jquery.amaran.min.js') }}"></script>
<script src="{{ asset('js/usuario_antigo/main.js')}}?{{config('app.versao_cache') }}"></script>
<script src="{{ asset('js/LightBox/js/html5lightbox.js')}}?{{config('app.versao_cache') }}"></script>
<script src="{{ asset('js/usuario_antigo/loja/geral.js') }}?{{ config('app.versao_cache') }}"></script>

@yield('scripts')

</body>

</html>
