@component('mail::layout')
    {{-- Header --}}
    {{-- @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            
        @endcomponent
    @endslot --}}

    {{-- Body --}}
    <img src="{{ url("https://www.yetzcards.com.br/img/img-mmkt/yetz-tipos-premios_01.jpg") }}" class="site-logo" />
    {!! $slot !!}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {!!  $subcopy !!}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. @lang('Todos os direitos reservados.')
        @endcomponent
    @endslot
@endcomponent
