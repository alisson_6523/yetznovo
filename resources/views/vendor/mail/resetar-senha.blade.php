<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Yetz Cards - Login</title>
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131006548-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
         function gtag(){dataLayer.push(arguments);}
         gtag('js', new Date());

         gtag('config', 'UA-131006548-1');
    </script>

    <link rel="shortcut icon" href="img/favicon.png" type="image/png" />

    <link rel="stylesheet" href="/css/usuario_antigo/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="/css/usuario_antigo/adm/vendor/font-awesome/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/login-personalizado.css">
    <link rel="stylesheet" href="/css/usuario_antigo/amaran.min.css">
    <link rel="stylesheet" href="/css/usuario_antigo/responsivo.css" />
    <link rel="stylesheet" href="/css/usuario_antigo/personalizado-videos.css" />

    <!--Edney: mudei daqui-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <!--Edney: até aqui-->
    <script src="/js/usuario_antigo/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <section class="conteudo-login conteudo-nova-senha">
        <div class="conteudo-form nova-senha">
            <div class="logo">
                <img src="/img/logo-yetz-cards.png">
            </div>
            <div class="titulo-login">
                <div class="traco"></div>
                <h1>minha área</h1>
            </div>
            <form action="/confirma-nova-senha" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="token" value='{{ $token }}'>
                @if($errors->any())
                    <h4>{{ $errors->first() }}</h4>
                @endif
                <div class="form-group">
                    <div class="input-group">
                        <input type="password" name="password" class="form-control input-lg" id="input-senha"
                            placeholder="Digite a nova senha">
                    </div>
                </div>
                <div class="form-group">

                    <div class="input-group">
                        <input type="password" name="password_confirm" class="form-control input-lg"
                            id="input-senha-confirma" placeholder="Confirme a nova senha">
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" id="btn-buscar-chave" value="trocar senha">
                </div>
            </form>
            <div class="progress" id="progress-login">
                <div class="progress-bar progress-bar-striped progress-bar-warning active" role="progressbar"
                    aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
            </div>
            <div class="sobre-yetz">
                <!--                    <img src="/img/logo-yetz-login.svg" class="logo-sobre">-->

                <div class="texto">
                    <span>® YETZ MARKETING, INCENTIVO E RELACIONAMENTO LTDA.
                        | 2017 | Todos os direitos reservados / CNPJ: 28.325.166/0001-05.</span>

                    <span>Este site é melhor visualizado em: Internet Explorer 9,
                        Firefox 2.0.0.2, Safari 3.1.2, Google Chrome 1.0 ou suas versões
                        mais recentes.</span>

                </div>
            </div>
            <div class="site-seguro">
                <a href="#"
                    onclick="window.open('https://www.sitelock.com/verify.php?site=yetzcards.com.br', 'SiteLock', 'width=600,height=600,left=160,top=170');"><img
                        class="img-responsive" alt="SiteLock" title="SiteLock"
                        src="//shield.sitelock.com/shield/yetzcards.com.br" /></a>
                <img src="/img/logo-yetz-login.svg" class="logo-sobre">
            </div>
        </div>
    </section>


    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <script src="/js/usuario_antigo/vendor/bootstrap.min.js"></script>
    <script src="/js/usuario_antigo/vendor/input-mask-jquery.js"></script>
    <script src="/js/usuario_antigo/jquery.amaran.min.js"></script>

    <script src="/js/usuario_antigo/loja/login.js"></script>

</body>

</html>
