<div class="modal-voucher">
    <div class="overlay">
    </div>
    <div class="box-white">
        <button class="btn-close-modal">
            <img src="/img/xis.svg" alt="">
        </button>
        <div class="box-voucher">
            <img class="icon" src="/img/icone-circulo-estrela.svg" alt="">
            <div class="text">
                <p>CUPOM YETZ CARDS</p>
                <span class="icon-voucher">
                    <img src="/img/icone-voucher.svg" alt="">
                </span>
            </div>
        </div>
        <div class="texto">
            <div class="traco"></div>
            <h2>CUPOM YETZ CARDS</h2>
            <p>
                Preencha o campo abaixo com o CÓDIGO informado no CUPOM YETZ CARDS
                que você ganhou e clique na seta verde para ADICIONAR os PONTOS.
            </p>
            <div class="formulario">
                <input type="text" name="cupom" class="input-cupom">
                <button type="button" class="btn-validar-cupom">
                    <img src="/img/arrow-right.svg" alt="">
                </button>
            </div>
        </div>
    </div>
    <div class="box-sucesso">
        <img src="/img/circulo-estrela-verde.svg" alt="">
        <h2>CUPOM YETZ CARDS</h2>
        <p>
            Parabéns, você carregou XXXX pontos na sua conta YETZ CARDS.
        </p>
        <button type="button" class="btn-finalizar">finalizar</button>
    </div>
    <div class="box-erro">
        <img src="/img/icone-error.svg" alt="">
        <h2>CUPOM YETZ CARDS</h2>
        <p>
            CUPOM YETZ CARDS não encontrado, verifique os dados digitados e tente novamente.
        </p>
        <button type="button" class="btn-voltar">voltar</button>
    </div>
</div>