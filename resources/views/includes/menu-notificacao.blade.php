<div class="menu-lateral-notificacoes">
      <div class="background"></div>
      <div class="content">
            <div class="area-notificacoes">
                  <div class="topo">
                        <h1>notificações</h1>
                        <a class="btn-close" onclick="closeMenuNotificacoes(event)">
                              <img src="/img/icone-fechar-amarelo.svg">
                        </a>
                  </div>
                  @if(empty($usuario->notifications))
                  <div class="notificacao-vazio">
                        <h2>Você não possui nova notificação!</h2>
                  </div>
                  @else
                  <div class="todas-notificacoes" cod_usuario="{{ $usuario->cod_usuario }}">
                        @foreach ($usuario->notifications()->orderBy('created_at', 'DESC')->get() as $notificacao)
                        @if (empty($notificacao->read_at) && isset($notificacao->data['cod_campanha']) &&
                        $notificacao->data['cod_campanha'] == session('campanha')->cod_campanha)
                        <div class="item_notificacao {{ $notificacao->data['class'] }}"
                              notificacao_id="{{ $notificacao->id }}">
                              <div class="img-notificacao">
                                    <div class="imagem"></div>
                              </div>
                              <div class="texto">
                                    <p class="titulo">{{ $notificacao->data['titulo'] }}</p>
                                    <p class="mensagem">{{ $notificacao->data['mensagem'] }}</p>
                              </div>
                              <a class="btn-visualizar-mensagem">
                                    <img src="/img/icone-fechar-preto.svg" alt="">
                              </a>
                        </div>
                        @endif
                        @endforeach

                        @foreach ($usuario->notificacoes_gerais()->orderBy('created_at', 'DESC')->get() as $notificacao)
                            @if (!$notificacao->data_leitura)

                                @php
                                $n = $notificacao->notificacao();
                                @endphp

                                <div class="item_notificacao {{$n['class']}}" notificacao_id="{{ $notificacao->id }}">
                                      <div class="img-notificacao">
                                            <div class="imagem"></div>
                                      </div>
                                      <div class="texto">
                                            <p class="titulo">{{ $n['titulo'] }}</p>
                                            <p class="mensagem">{{ $n['mensagem'] }}</p>
                                      </div>
                                      <a class="btn-visualizar-mensagem">
                                            <img src="/img/icone-fechar-preto.svg" alt="">
                                      </a>
                                </div>
                            @endif
                        @endforeach
                  </div>
                  @endif
                  <a href="{{ action('LojaController@notificacoes') }}" class="btn-ver-alertas">
                        <span>notificações já lidas</span>
                  </a>
            </div>
      </div>
</div>
