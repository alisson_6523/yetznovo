<!-- Modal -->
<div class="modal fade" id="modalCategoriasGerais" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><img
                        src="/img/close.png"></button>
            <div class="modal-body">
                <h1>Categorias</h1>
                <ul>
                    @foreach ($categorias as $categoria)
                        <li>
                            <a href="/loja/{{ $categoria->link_categoria }}">
                                <img src="/img/menu_categoria.svg#{{ $categoria->icone_categoria.'-amarelo' }}"
                                     title="{{ $categoria->categoria }}" alt="">
                            </a>
                        </li>
                    @endforeach
                    <li>
                        <a href="/loja">
                            <img src="/img/menu_categoria.svg#ver-todos-amarelo" title="VER TODOS" alt="">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>