<!-- Modal -->
<div class="modal fade" id="modalNotificacao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     notificacao_nova="">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <img src="/img/logo-modal-login.png" class="logo-header-modal">
                <button type="button" class="close" data-dismiss="modal"><img
                            src="/img/btn-close.png"></button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="cod-usuario">
                <input type="hidden" class="cod-notificacao">
                <h1 id="tipo-notificacao"></h1>
                <span id="titulo-notificacao"></span>
                <p id="conteudo-notificacao"></p>
            </div>
        </div>
    </div>
</div>
