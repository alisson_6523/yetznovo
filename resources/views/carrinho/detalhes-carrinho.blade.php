@extends('layout.layout-loja')

@section('title', 'Detalhes do Carrinho')

@section('conteudo-loja')

<input type="hidden" value="{{ $usuario->saldo_usuario }}" id="saldo-real" />
<section class="produtos-destaques fundo-cinza">
      <div class="container">
            <div class="row">
                  <div class="lista-produtos-mobile">

			@foreach($carrinho->produtos->groupBy('cod_produto') as $item)
				<div class="card-produto" data-code="{{ $item->first()->cod_produto }}">
					<div class="container-card-produto">
						<div class="container-img">
							<img src="{{ $item->first()->link_foto }}" alt="">
						</div>

						<div class="text-lista-produto">
							<p>{{ $item->first()->nome_produto }} - R$ {{ $item->first()->valor_reais }}</p>
							<h3>pontos - <span>{{ number_format($item->first()->valor, 2, ',', '.') }}</span></h3>
						</div>

						<div class="btn-close-mobile">
							<img src="/img/mobile-lista-produto/lista-produto-02.png" alt="">
						</div>
					</div>

					<div class="container-card-qtd">
						<div class="produto-qtd">
							<span class="quantity-input-sub"></span>
							<span>{{ $item->count() }}</span>
							<span class="quantity-input-add"></span>
						</div>

						<div class="produto-sub-total">
							<p>Subtotal  <span>{{ number_format($item->first()->valor * count($item), 2, ',', '.') }}</span></p>
						</div>
					</div>
				</div>
			@endforeach

                  </div>
                  <div class="lista-produtos">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                              <div class="table-responsive">
                                    <table class="table cart-table">
                                          <thead>
                                                <tr>
                                                      <th class="maior">NOME DO CARTÃO</th>
                                                      <th class="menor">PONTOS</th>
                                                      <th class="menor">QUANTIDADE</th>
                                                      <th class="menor">SUBTOTAL</th>
                                                </tr>
                                          </thead>
                                          <tbody>
                                                @foreach($carrinho->produtos->groupBy('cod_produto') as $item)
                                                <tr cod-item="{{ $item->first()->cod_produto }}">
                                                      <td class="item-name-col">
                                                            <figure>
                                                                  <a target="blank"><img
                                                                              src="{{ $item->first()->link_foto }}"
                                                                              alt="Foto do item {{ $item->first()->nome_produto }}" /></a>
                                                            </figure>
                                                            <header class="item-name">
                                                                  {{ $item->first()->nome_produto }} - R$
                                                                  {{ $item->first()->valor_reais }}
                                                            </header>
                                                      </td>
                                                      <td class="item-price-col">
                                                            <span class="item-price-special">
                                                                  <span
                                                                        class="valor-unitario">{{ number_format($item->first()->valor, 2, ',', '.') }}</span>
                                                            </span>
                                                      </td>
                                                      <td>
                                                            <div class="custom-quantity-input">
                                                                  <input type="text" name="quantity"
                                                                        value="{{ count($item) }}" disabled="disabled">
                                                                  <a href="#" onclick="return false;"
                                                                        class="quantity-btn quantity-input-up"><i
                                                                              class="fa fa-angle-up"></i></a>
                                                                  <a href="#" onclick="return false;"
                                                                        class="quantity-btn quantity-input-down"><i
                                                                              class="fa fa-angle-down"></i></a>
                                                            </div>
                                                      </td>
                                                      <td class="item-total-col">
                                                            <span class="item-price-special">
                                                                  <span
                                                                        class="valor-multiplicado">{{ number_format($item->first()->valor * count($item), 2, ',', '.') }}</span>
                                                                  yetz
                                                            </span>
                                                            <a class="close-button">
                                                                  <i class="fa fa-times" aria-hidden="true"></i>
                                                            </a>
                                                      </td>
                                                </tr>
                                                @endforeach
                                          </tbody>
                                    </table>
                              </div>
                        </div><!-- End .col-md-12 -->
                  </div>
            </div>
            <div class="xs-margin"></div>
            <div class="row">
                  <div class="col-md-8 col-sm-12 col-xs-12 lg-margin endereco-entrega">
                        <div class="titulo">ENTREGA</div>
                        <div class="body">
                              <div class="campos-endereco">
                                    <div class="textos-info-pedidos">
                                          @if(count($carrinho->produtos->where('tipo', 2)) > 0)
                                              <div class="form-group">
                                                  @if(count($carrinho->produtos->where('tipo', 2)) == 1)
                                                    <p>O VOUCHER DIGITAL SOLICITADO SERÁ ENVIADO PARA O E-MAIL CADASTRADO.</p>
                                                  @else
                                                      <p>OS VOUCHERS DIGITAIS SOLICITADOS SERÃO ENVIADOS PARA O E-MAIL CADASTRADO.</p>
                                                  @endif
                                              </div>
                                          @endif
                                          @if(count($carrinho->produtos->where('tipo', 1)) > 0)
                                              <div class="form-group">
                                                  @if(count($carrinho->produtos->where('tipo', 1)) == 1)
                                                      <p>O CARTÃO FÍSICO SOLICITADO SERÁ ENTREGUE NO ENDEREÇO:</p>
                                                  @else
                                                      <p>OS CARTÕES FÍSICOS SOLICITADOS SERÃO ENTREGUES NO ENDEREÇO:</p>
                                                  @endif
                                              </div>
                                              <div class="form-group" id="texto-endereco">
                                                    <label>{{ $usuario->pdv->nome_pdv }}</label><br>
                                                    <label>Logradouro: {{ $usuario->pdv->logradouro }}</label><br>
                                                    <label>Numero: {{ $usuario->pdv->numero }}</label><br>
                                                    <label>Bairro: {{ $usuario->pdv->bairro }}</label><br>
                                                    <label>Cidade:
                                                          {{ $usuario->pdv->cidade . ' - ' . $usuario->pdv->estado }}</label><br>
                                              </div>
                                          @endif
                                          <div class="form-group">
                                                <p>EM FUNÇÃO DAS FESTAS DE FINAL DE ANO, RESGATES DE CARTÕES FÍSICOS OU VOUCHERS DIGITAIS SOLICITADOS A PARTIR DE 28/11 SERÃO ENTREGUES EM DIFERENTES PRAZOS, CONFIRA:</p>
                                                <p>DE 28/11 a 19/12/2019 – ENTREGUE  13/01/2020</p>
                                                <p>DE 20/12 a 06/01/2020 – ENTREGUE 03/02/2020</p>
                                                <p>A PARTIR DE 07/01/2020 – FLUXO NORMAL</p>
                                          </div>
                                    </div>
                                    {{--                    <input type="text" placeholder="CIDADE" name="cidade" value="" class="cidade" id="localidade" >--}}
                                    {{--                    <input type="text" placeholder="LOGRADOURO" name="logradouro" value="" class="logradouro" id="logradouro" >--}}
                                    {{--                    <input type="text" placeholder="BAIRRO" name="bairro" value="" class="bairro" id="bairro" >--}}
                                    {{--                    <input type="text" placeholder="NÚMERO" name="numero" value="" class="numero" id="numero" >--}}
                                    {{--                    <input type="text" placeholder="COMPLEMENTO" name="complemento" value="" class="logradouro" id="complmento" >--}}

                              </div>
                        </div>
                  </div><!-- End .col-md-8 -->

                  <div class="col-md-4 col-sm-12 col-xs-12">
                        <table class="total-table">
                              <tbody>
                                    <tr>
                                          <td class="saldo-table-title">TOTAL:</td>
                                          <td class="texto-saldo"><span
                                                      id="totalComFrete">{{ round($carrinho->produtos->sum('valor')) }}</span> yetz</td>
                                    </tr>
                                    <tr>
                                          <td class="saldo-table-title">MEUS PONTOS:</td>
                                          <td
                                                class="texto-saldo {{ round($carrinho->produtos->sum('valor')) < number_format($usuario->saldo_usuario, 0, ',', '.') ? 'vermelho-saldo' : 'verde-saldo' }}">
                                                <span data-saldo="{{ number_format($usuario->saldo_usuario, 2, ',', '.') }}" id="textoSaldo">{{ number_format($usuario->saldo_usuario, 2, ',', '.') }} yetz</span> </td>
                                    </tr>
                              </tbody>
                        </table>
                        <div class="xss-margin"></div><!-- End .space -->
                        @if(count($carrinho->produtos)>0)
                        <input type="button" class="btn btn-custom btn-resgatar" {{ \App\Models\Campanha::find(session('campanha')->cod_campanha)->dt_fim }} value="{{ \App\Models\Campanha::find(session('campanha')->cod_campanha)->dt_fim <= \Carbon\Carbon::now() ? 'CAMPANHA ENCERRADA' : 'RESGATAR' }}" {{ \App\Models\Campanha::find(session('campanha')->cod_campanha)->dt_fim <= \Carbon\Carbon::now() ? 'disabled' : 'id=geraForm' }}>
                        @endif
                  </div><!-- End .col-md-4 -->
            </div>
      </div>
</section>

<!-- Modal -->
<div id="modalConfirmaResgate" class="modal fade" role="dialog">
      <div class="modal-dialog">
            <!-- Conteudo Modal-->
            <div class="modal-content">
                  <div class="modal-header">
                        <img src="{{ asset('img/logo-menu-novo.png') }}" title="Logo Yetz Cards" alt="Logo Yetz Cards">
                  </div>
                  <div class="modal-body">
                        <p class="texto-superior">
                              Falta apenas um passo para concluir seu resgate. Confira com atenção a quantidade de
                              pontos que vai ser debitada e clique em "finalizar resgate".
                        </p>
                        <div class="saldo-modal">
                              <p class="txt">saldo anterior</p>
                              <p class="valores" id="resultSaldo"></p>
                              <hr>
                              <p class="txt">resgate atual</p>
                              <p class="valores vermelho-saldo" id="resultValorTotal"></p>
                              <hr>
                              <p class="txt">saldo atualizado</p>
                              <p class="valores verde-saldo" id="resultSaldoFinal"></p>
                        </div>
                        <div class="confirmacao">
                              <div class="opcao">
                                    <div class="circle"></div>
                                    <span>Confirmo que li e estou de acordo o regulamento do(s) cartão/cartões
                                          resgatado(s)*, referentes a:</span>
                              </div>
                              <ul>
                                    <li>
                                          <span class="circle"></span>
                                          <p>ACEITO EM E-COMMERCE E/OU LOJA FÍSICA</p>
                                    </li>
                                    <li>
                                          <span class="circle"></span>
                                          <p>CIDADES ONDE É ACEITO</p>
                                    </li>
                                    <li>
                                          <span class="circle"></span>
                                          <p>DESCRIÇÃO DO PRODUTO</p>
                                    </li>
                                    <li>
                                          <span class="circle"></span>
                                          <p>INFORMAÇÕES IMPORTANTES </p>
                                    </li>
                                    <li>
                                          <span class="circle"></span>
                                          <p>REGRAS DE USO</p>
                                    </li>
                                    <li>
                                          <span class="circle"></span>
                                          <p>VALIDADE</p>
                                    </li>
                              </ul>
                              <span class="text-danger">*NÃO É POSSÍVEL trocar cartões depois do resgate concluído.
                              </span>
                        </div>
                        <form method="POST" id="formResgate">
                              <input type="hidden" name="cod_endereco" id="codEndereco" value="">
                              <input type="hidden" name="cep" id="cepHidden">
                              <input type="hidden" name="sigla_estado" id="estadoHidden">
                              <input type="hidden" name="cidade" id="cidadeHidden">
                              <input type="hidden" name="bairro" id="bairroHidden">
                              <input type="hidden" name="logradouro" id="ruaHidden">
                              <input type="hidden" name="numero" id="numHidden">
                              <input type="hidden" name="complemento" id="complementoHidden">
                              <input type="hidden" name="nome_destinatario" id="destinatarioHidden" value="">
                              <input type="hidden" name="celular_destinatario" id="telefoneDestinatarioHidden" value="">
                              <input type="hidden" name="cpf_destinatario" id="cpfDestinatarioHidden" value="">
                              <input type="hidden" name="email_destinatario" id="emailDestinatarioHidden" value="">
                              <input type="submit" class="btn btn-custom btn-resgatar" disabled
                                    value="FINALIZAR RESGATE">
                        </form>
                  </div>
            </div>

      </div>
</div>

@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function () {
	if({{ count($carrinho->produtos) }} == 0)
		window.location.href = "/loja";
});
</script>
<script src="{{ asset('js/usuario_antigo/loja/detalhes-carrinho.js') }}"></script>
@stop
