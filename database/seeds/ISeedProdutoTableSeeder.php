<?php

use Illuminate\Database\Seeder;

class ISeedProdutoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        
        \DB::table('produto')->insert(array (
            0 => 
            array (
                'cod_produto' => 3286,
                'nome_produto' => 'ANGELONI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ANGELONI. 
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.angeloni.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-angeloni.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'cod_produto' => 3287,
                'nome_produto' => 'ANGELONI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ANGELONI. 
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.angeloni.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-angeloni.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'cod_produto' => 3288,
                'nome_produto' => 'ANGELONI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ANGELONI. 
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.angeloni.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-angeloni.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'cod_produto' => 3289,
                'nome_produto' => 'ANGELONI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ANGELONI. 
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo. ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.angeloni.com.br',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-angeloni.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'cod_produto' => 3290,
                'nome_produto' => 'ASSAÍ',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ASSAÍ, integrante do Grupo Pão de Açúcar.
Atacado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.assai.com.br ',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-assai.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'cod_produto' => 3291,
                'nome_produto' => 'ASSAÍ',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ASSAÍ, integrante do Grupo Pão de Açúcar.
Atacado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.assai.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-assai.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'cod_produto' => 3292,
                'nome_produto' => 'ASSAÍ',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados ASSAÍ, integrante do Grupo Pão de Açúcar.
Atacado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.assai.com.br ',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-assai.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'cod_produto' => 3293,
                'nome_produto' => 'BIG',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BIG, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas BIG poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                                    
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                     
Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/big ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-big.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:50',
                'updated_at' => '2019-06-05 20:04:50',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'cod_produto' => 3294,
                'nome_produto' => 'BIG',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BIG, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas BIG poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                                    
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                     
Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/big ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-big.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'cod_produto' => 3295,
                'nome_produto' => 'BIG',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BIG, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas BIG poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                                    
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                     
Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/big ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-big.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'cod_produto' => 3296,
                'nome_produto' => 'BOM PREÇO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BOM PREÇO, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas  Bompreço  poderão ser utilizados até, no máximo, dois (02) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/bompreco ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-bompreco.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'cod_produto' => 3297,
                'nome_produto' => 'BOM PREÇO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BOM PREÇO, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas  Bompreço  poderão ser utilizados até, no máximo, dois (02) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/bompreco ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-bompreco.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'cod_produto' => 3298,
                'nome_produto' => 'BOM PREÇO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados BOM PREÇO, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas  Bompreço  poderão ser utilizados até, no máximo, dois (02) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/bompreco ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-bompreco.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'cod_produto' => 3299,
                'nome_produto' => 'CARREFOUR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) cartões por compra.',
                'informacoes_importantes' => 'O cartão que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-carrefour.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'cod_produto' => 3300,
                'nome_produto' => 'CARREFOUR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) cartões por compra.',
                'informacoes_importantes' => 'O cartão que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-carrefour.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'cod_produto' => 3301,
                'nome_produto' => 'CARREFOUR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) cartões por compra.',
                'informacoes_importantes' => 'O cartão que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-carrefour.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'cod_produto' => 3302,
                'nome_produto' => 'CARREFOUR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) cartões por compra.',
                'informacoes_importantes' => 'O cartão que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-carrefour.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'cod_produto' => 3303,
                'nome_produto' => 'CHEZ FRANCE',
                'descricao_produto' => 'Cartão para aquisição dos vinhos da CHEZ FRANCE, loja virtual de vinhos franceses.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.chezfrance.com.br                                                                                                                                                                                                                                                                                                                                                                                                                  ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-vinhos-chez-france.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'cod_produto' => 3304,
                'nome_produto' => 'CHEZ FRANCE',
                'descricao_produto' => 'Cartão para aquisição dos vinhos da CHEZ FRANCE, loja virtual de vinhos franceses.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.chezfrance.com.br                                                                                                                                                                                                                                                                                                                                                                                                                  ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-vinhos-chez-france.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'cod_produto' => 3305,
                'nome_produto' => 'CHEZ FRANCE',
                'descricao_produto' => 'Voucher digital para aquisição dos vinhos da CHEZ FRANCE, loja virtual de vinhos franceses.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.chezfrance.com.br                                                                                                                                                                                                                                                                                                                                                                                                                  ',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-alimentacao-vinhoschezfrance.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'cod_produto' => 3306,
                'nome_produto' => 'CHEZ FRANCE ',
                'descricao_produto' => 'Cartão para aquisição dos vinhos da CHEZ FRANCE, loja virtual de vinhos franceses.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.chezfrance.com.br                                                                                                                                                                                                                                                                                                                                                                                                                  ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-vinhos-chez-france.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'cod_produto' => 3307,
                'nome_produto' => 'EXTRA DELIVERY',
                'descricao_produto' => 'Voucher digital Multicash para aquisição de produtos através de delivery Pão de Açúcar ou Delivery Extra e receber no endereço indicado. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
É necessário informar o código de segurança de 3 (três) dígitos. 
Permitida a utilização de 1 (um) Cartão Presente por compra realizada, sendo que o saldo do cartão deverá ser igual ou inferior ao valor total, uma vez que não é permitida outras formas de pagamento no site.',
                'informacoes_importantes' => 'Consulte as localidades de entrega.
Para compras e mais informações acesse: www.paodeacucar.com e deliveryextra.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-alimentacao-deliveryextra.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'cod_produto' => 3308,
                'nome_produto' => 'EXTRA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados EXTRA, EXTRA Hiper e Mini Mercado EXTRA, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.extra.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-extra.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'cod_produto' => 3309,
                'nome_produto' => 'EXTRA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados EXTRA, EXTRA Hiper e Mini Mercado EXTRA, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.extra.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-extra.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'cod_produto' => 3310,
                'nome_produto' => 'EXTRA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados EXTRA, EXTRA Hiper e Mini Mercado EXTRA, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.extra.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-extra.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'cod_produto' => 3311,
                'nome_produto' => 'EXTRA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados EXTRA, EXTRA Hiper e Mini Mercado EXTRA, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.extra.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-extra.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'cod_produto' => 3312,
                'nome_produto' => 'MAXXI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MAXXI, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas. 
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Maxxi Atacado poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.maxxiatacado.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-maxxi.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'cod_produto' => 3313,
                'nome_produto' => 'MAXXI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MAXXI, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas. 
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Maxxi Atacado poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.maxxiatacado.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-maxxi.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:51',
                'updated_at' => '2019-06-05 20:04:51',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'cod_produto' => 3314,
                'nome_produto' => 'MAXXI',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MAXXI, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas. 
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Maxxi Atacado poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                      Para endereços e mais informações acesse: www.maxxiatacado.com.br ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-maxxi.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'cod_produto' => 3315,
                'nome_produto' => 'MERCADORAMA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MERCADORAMA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Mercadorama poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                  Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/mercadorama',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-mercadorama.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'cod_produto' => 3316,
                'nome_produto' => 'MERCADORAMA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MERCADORAMA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Mercadorama poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                  Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/mercadorama',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-mercadorama.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'cod_produto' => 3317,
                'nome_produto' => 'MERCADORAMA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados MERCADORAMA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.     
Para as lojas Mercadorama poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                  Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/mercadorama',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-mercadorama.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'cod_produto' => 3318,
                'nome_produto' => 'MULTICASH',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto das redes do Grupo Pão de Açúcar.
Hipermercados e lojas de departamentos que oferecem produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.multibeneficiosgpa.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'cod_produto' => 3319,
                'nome_produto' => 'MULTICASH',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto das redes do Grupo Pão de Açúcar.
Hipermercados e lojas de departamentos que oferecem produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.multibeneficiosgpa.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'cod_produto' => 3320,
                'nome_produto' => 'MULTICASH',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto das redes do Grupo Pão de Açúcar.
Hipermercados e lojas de departamentos que oferecem produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.multibeneficiosgpa.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'cod_produto' => 3321,
                'nome_produto' => 'MULTICASH',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto das redes do Grupo Pão de Açúcar.
Hipermercados e lojas de departamentos que oferecem produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.multibeneficiosgpa.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'cod_produto' => 3322,
                'nome_produto' => 'NACIONAL',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados NACIONAL, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas Nacional poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/nacional ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-nacional.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'cod_produto' => 3323,
                'nome_produto' => 'NACIONAL',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados NACIONAL, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas Nacional poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/nacional ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-nacional.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'cod_produto' => 3324,
                'nome_produto' => 'NACIONAL',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados NACIONAL, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Para as lojas Nacional poderão ser utilizados até, no máximo, sete (07) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).      
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.walmartbrasil.com.br/bandeiras/nacional ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-nacional.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'cod_produto' => 3325,
                'nome_produto' => 'PÃO DE AÇÚCAR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados PÃO DE AÇÚCAR e MINUTO PÃO DE AÇÚCAR, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse:  www.paodeacucar.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-pao-de-acucar.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'cod_produto' => 3326,
                'nome_produto' => 'PÃO DE AÇÚCAR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados PÃO DE AÇÚCAR e MINUTO PÃO DE AÇÚCAR, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse:  www.paodeacucar.com',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-pao-de-acucar.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'cod_produto' => 3327,
                'nome_produto' => 'PÃO DE AÇÚCAR',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados PÃO DE AÇÚCAR e MINUTO PÃO DE AÇÚCAR, integrantes do Grupo Pão de Açúcar.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse:  www.paodeacucar.com',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-multicash-pao-de-acucar.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'cod_produto' => 3328,
                'nome_produto' => 'PÃO DE AÇÚCAR',
                'descricao_produto' => 'Voucher digital Multicash para aquisição de produtos através de delivery Pão de Açúcar ou Delivery Extra e receber no endereço indicado. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
É necessário informar o código de segurança de 3 (três) dígitos. 
Permitido apenas um voucher por compra, sendo que o saldo do cartão deverá ser igual ou inferior ao valor total, uma vez que não é permitida outras formas de pagamento no site.',
                'informacoes_importantes' => 'Consulte as localidades de entrega.
Para compras e mais informações acesse: www.paodeacucar.com e deliveryextra.com.br.',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-alimentacao-multicash-pao-de-acucar.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'cod_produto' => 3329,
                'nome_produto' => 'PORTUS',
                'descricao_produto' => 'Voucher digital para aquisição de bebidas de alta qualidade PORTUS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.    ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.portusimportadora.com.br                                                                                                                                                                                                                                                                                                                                                                                                          ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-alimentacao-portus.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'cod_produto' => 3330,
                'nome_produto' => 'PORTUS',
                'descricao_produto' => 'Voucher digital para aquisição de bebidas de alta qualidade PORTUS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.portusimportadora.com.br                                                                                                                                                                                                                                                                                                                                                                                                          ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-alimentacao-portus.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'cod_produto' => 3331,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAMS CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    
Para as lojas Sam’s Club poderão ser utilizados até, no máximo, nove (09) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                         
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'cod_produto' => 3332,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAM\'S CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                          
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:52',
                'updated_at' => '2019-06-05 20:04:52',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'cod_produto' => 3333,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAM\'S CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    
Para as lojas Sam’s Club poderão ser utilizados até, no máximo, nove (09) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                          
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'cod_produto' => 3334,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-mastercard.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'cod_produto' => 3335,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-mastercard.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'cod_produto' => 3336,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas do segmento ALIMENTAÇÃO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-mastercard.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'cod_produto' => 3337,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-visa.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'cod_produto' => 3338,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-visa.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'cod_produto' => 3339,
                'nome_produto' => 'SEGMENTADO ALIMENTAÇÃO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce do segmento ALIMENTAÇÃO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-segmentado-supermercado-visa.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'cod_produto' => 3340,
                'nome_produto' => 'TODO DIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados TODO DIA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.         
Para as lojas Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                         Para as lojas Todo Dia Sudeste poderão ser utilizados até, no máximo, sete (09) cartões por compra.                                                                                                                                                                                                                                                                                                                                                    Para as lojas Todo Dia Nordeste poderão ser utilizados até, no máximo, sete (02) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                             ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.mercadotododia.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-todo-dia.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'cod_produto' => 3341,
                'nome_produto' => 'TODO DIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados TODO DIA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.         
Para as lojas Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                         Para as lojas Todo Dia Sudeste poderão ser utilizados até, no máximo, sete (09) cartões por compra.                                                                                                                                                                                                                                                                                                                                                    Para as lojas Todo Dia Nordeste poderão ser utilizados até, no máximo, sete (02) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                             ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.mercadotododia.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-todo-dia.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'cod_produto' => 3342,
                'nome_produto' => 'TODO DIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados TODO DIA, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.         
Para as lojas Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                         Para as lojas Todo Dia Sudeste poderão ser utilizados até, no máximo, sete (09) cartões por compra.                                                                                                                                                                                                                                                                                                                                                    Para as lojas Todo Dia Nordeste poderão ser utilizados até, no máximo, sete (02) cartões por compra.                                                                                                                                                                                                                                                                                                                                                                             ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.mercadotododia.com.br ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-todo-dia.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'cod_produto' => 3343,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'cod_produto' => 3344,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'cod_produto' => 3345,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-alimentacao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'cod_produto' => 3346,
                'nome_produto' => 'HABBO',
                'descricao_produto' => 'Cartão HABBO é uma opção de pagamento dos usuários do site de entretenimento social online Habbo.                                                                                                                                                                                                                                                                                                                        ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.',
                'informacoes_importantes' => ' Pode ser resgatado como Habbo Moedas, principal forma de câmbio utilizada no Habbo Hotel, para os usuários comprarem itens virtuais e pagarem pela associação ao Habbo Clube. 
Para mais informações acesse: www.habbo.com.br                                                                                                                                                                    ',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-habbo.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'cod_produto' => 3347,
                'nome_produto' => 'LEAGUE OF LEGENDS',
                'descricao_produto' => 'Cartão para compras de conteúdos exclusivos no jogo LEAGUE OF LEGENDS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a 5 cartões por dia.',
            'informacoes_importantes' => 'Este cartão é válido para compra de RP\'s (Riot Points) para jogadores de League of Legends.
O valor será creditado a uma conta, por isso precisa ter cadastro.
Para mais informações acesse: play.br.leagueoflegends.com/pt_BR',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-leagueoflegends.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:53',
                'updated_at' => '2019-06-05 20:04:53',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'cod_produto' => 3348,
                'nome_produto' => 'LEAGUE OF LEGENDS',
                'descricao_produto' => 'Cartão para compras de conteúdos exclusivos no jogo LEAGUE OF LEGENDS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.                                                                                                                                        ',
            'informacoes_importantes' => 'Este cartão é válido para compra de RP\'s (Riot Points) para jogadores de League of Legends.
O valor será creditado a uma conta, por isso precisa ter cadastro.
Para mais informações acesse: play.br.leagueoflegends.com/pt_BR',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-leagueoflegends.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'cod_produto' => 3349,
                'nome_produto' => 'LEAGUE OF LEGENDS',
                'descricao_produto' => 'Voucher digital para compras de conteúdos exclusivos no jogo LEAGUE OF LEGENDS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a 5 cartões por dia.',
            'informacoes_importantes' => 'Este cartão é válido para compra de RP\'s (Riot Points) para jogadores de League of Legends.
O valor será creditado a uma conta, por isso precisa ter cadastro.
Para mais informações acesse: play.br.leagueoflegends.com/pt_BR',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-brinquedos-e-games-leagueoflegends.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'cod_produto' => 3350,
                'nome_produto' => 'LEVEL UP',
                'descricao_produto' => 'Cartão para compras de CASH para creditar sua conta LEVEL UP, distribuidora de jogos on-line.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O PIN desse cartão só pode ser utilizado uma vez.',
                'informacoes_importantes' => 'Este cartão é válido para acesso aos servidores brasileiros da Level Up.
Para mais informações acesse: www.levelupgames.uol.com.br/levelup',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-level-up.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'cod_produto' => 3351,
                'nome_produto' => 'LEVEL UP',
                'descricao_produto' => 'Cartão para compras de CASH para creditar sua conta LEVEL UP, distribuidora de jogos on-line.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O PIN desse cartão só pode ser utilizado uma vez.',
                'informacoes_importantes' => 'Este cartão é válido para acesso aos servidores brasileiros da Level Up.
Para mais informações acesse: www.levelupgames.uol.com.br/levelup',
                'valor_produto' => 800,
                'valor_reais' => '40.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-level-up.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'cod_produto' => 3356,
                'nome_produto' => 'PLAYSTATION STORE',
                'descricao_produto' => 'Cartão para compra de conteúdo na PLAYSTATION STORE, como filmes, músicas, packs de expansão e diversos itens para personalizar personagens.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a R$ 300,00 de crédito por dia.
',
                'informacoes_importantes' => 'Aceito para quem tem conta na PlayStation Store. 
O Cartão PlayStation resgata o crédito na conta automaticamente ao ser ativado.
Válido somente pelo serviço PlayStation Store no Brasil.
Para mais informações acesse: store.playstation.com',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-playstation-store.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'cod_produto' => 3357,
                'nome_produto' => 'PLAYSTATION STORE',
                'descricao_produto' => 'Cartão para compra de conteúdo na PLAYSTATION STORE, como filmes, músicas, packs de expansão e diversos itens para personalizar personagens.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a R$ 300,00 de crédito por dia.
',
                'informacoes_importantes' => 'Aceito para quem tem conta na PlayStation Store. 
O Cartão PlayStation resgata o crédito na conta automaticamente ao ser ativado.
Válido somente pelo serviço PlayStation Store no Brasil.
Para mais informações acesse: store.playstation.com',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-playstation-store.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'cod_produto' => 3358,
                'nome_produto' => 'PLAYSTATION STORE',
                'descricao_produto' => 'Voucher digital para compra de conteúdo na PLAYSTATION STORE, como filmes, músicas, packs de expansão e diversos itens para personalizar personagens.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a R$ 300,00 de crédito por dia.
',
                'informacoes_importantes' => 'Aceito para quem tem conta na PlayStation Store. 
O Cartão PlayStation resgata o crédito na conta automaticamente ao ser ativado.
Válido somente pelo serviço PlayStation Store no Brasil.
Para mais informações acesse: store.playstation.com',
                'valor_produto' => 1200,
                'valor_reais' => '60.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-brinquedos-e-games-playstation-store.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'cod_produto' => 3359,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Cartão para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-ri-happy.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'cod_produto' => 3360,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Cartão para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-ri-happy.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'cod_produto' => 3361,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Cartão para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-ri-happy.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'cod_produto' => 3362,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Voucher digital para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 1500,
                'valor_reais' => '75.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-brinquedos-e-games-ri-happy.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'cod_produto' => 3363,
                'nome_produto' => 'XBOX LIVE',
                'descricao_produto' => 'Cartão que dá direito a créditos no XBOX LIVE, que permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Aceito no Xbox Marketplace.',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-xbox-live.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'cod_produto' => 3364,
                'nome_produto' => 'XBOX LIVE',
                'descricao_produto' => 'Cartão que dá direito a créditos no XBOX LIVE, que permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Aceito no Xbox Marketplace.',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-xbox-live.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'cod_produto' => 3365,
                'nome_produto' => 'XBOX LIVE',
                'descricao_produto' => 'Cartão que dá direito a créditos no XBOX LIVE, que permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Aceito no Xbox Marketplace. ',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games-xbox-live.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'cod_produto' => 3366,
                'nome_produto' => 'XBOX LIVE',
                'descricao_produto' => 'Voucher digital que dá direito a créditos no XBOX LIVE, que permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Aceito no Xbox Marketplace.',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-brinquedos-e-games-xbox-live.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'cod_produto' => 3367,
                'nome_produto' => 'XBOX LIVE ANUAL',
                'descricao_produto' => 'Cartão que dá direito a uma assinatura de 12 meses no XBOX LIVE, permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.
Aceito para mensalidades de Xbox Live.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 2980,
                'valor_reais' => '149.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games--xboxgold12meses.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'cod_produto' => 3368,
                'nome_produto' => 'XBOX LIVE TRIMESTRAL',
                'descricao_produto' => 'Cartão que dá direito a uma assinatura de 3 meses no XBOX LIVE, permite jogar online de modo ilimitado, fazer download de jogos e de demos, ter acesso a ofertas e descontos exclusivos, ver filmes e shows em HD, ouvir músicas através do Xbox Music e navegar na web através da TV com o Microsoft Edge para Xbox.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Válido somente pelo serviço nas lojas do Brasil. 
Assinaturas adicionais podem ser exigidas para algumas funções.
Aceito para mensalidades de Xbox Live.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        Para mais informações acesse: Xbox.com/live',
                'valor_produto' => 1380,
                'valor_reais' => '69.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-brinquedos-e-games--xboxgold3meses.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:54',
                'updated_at' => '2019-06-05 20:04:54',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'cod_produto' => 3369,
                'nome_produto' => 'PRESENTE VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-visa.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'cod_produto' => 3370,
                'nome_produto' => 'PRESENTE VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-visa.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '7 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'cod_produto' => 3371,
                'nome_produto' => 'PRESENTE VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-visa.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '8 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'cod_produto' => 3372,
                'nome_produto' => 'PRESENTE VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 20000,
                'valor_reais' => '1000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-visa.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '9 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'cod_produto' => 3373,
                'nome_produto' => 'PRESENTE VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 30000,
                'valor_reais' => '1500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-visa.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '10 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'cod_produto' => 3374,
                'nome_produto' => 'PRESENTE MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas que aceitem a bandeira Mastercard.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-mastercard.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'cod_produto' => 3375,
                'nome_produto' => 'PRESENTE MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas que aceitem a bandeira Mastercard.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-mastercard.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'cod_produto' => 3376,
                'nome_produto' => 'PRESENTE MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas que aceitem a bandeira Mastercard.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 15000,
                'valor_reais' => '750.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-mastercard.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'cod_produto' => 3377,
                'nome_produto' => 'PRESENTE MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas que aceitem a bandeira Mastercard.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 25000,
                'valor_reais' => '1250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-mastercard.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'cod_produto' => 3378,
                'nome_produto' => 'PRESENTE MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas que aceitem a bandeira Mastercard.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 35000,
                'valor_reais' => '1750.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-presente-mastercard.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'cod_produto' => 3379,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'cod_produto' => 3380,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'cod_produto' => 3381,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'cod_produto' => 3382,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'cod_produto' => 3383,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Não permite saldo remanescente.                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-c&c.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'cod_produto' => 3384,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Não permite saldo remanescente.                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-c&c.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'cod_produto' => 3385,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Não permite saldo remanescente.                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-c&c.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'cod_produto' => 3386,
                'nome_produto' => 'CASAS BAHIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas CASAS BAHIA, integrante do Grupo Pão de Açúcar.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias). 
Não é válido para recarga de crédito de celular.                                                                          
Para endereços e mais informações acesse: www.casasbahia.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-casas-bahia.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'cod_produto' => 3387,
                'nome_produto' => 'CASAS BAHIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas CASAS BAHIA, integrante do Grupo Pão de Açúcar.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias). 
Não é válido para recarga de crédito de celular.                                                                          
Para endereços e mais informações acesse: www.casasbahia.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-casas-bahia.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'cod_produto' => 3388,
                'nome_produto' => 'CASAS BAHIA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas CASAS BAHIA, integrante do Grupo Pão de Açúcar.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias). 
Não é válido para recarga de crédito de celular.                                                                          
Para endereços e mais informações acesse: www.casasbahia.com.br ',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-casas-bahia.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:55',
                'updated_at' => '2019-06-05 20:04:55',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'cod_produto' => 3389,
                'nome_produto' => 'HAVAN',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na HAVAN, loja de departamentos de Eletrodomésticos, Eletrônicos e Decoração. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havan.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-havan.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'cod_produto' => 3390,
                'nome_produto' => 'HAVAN',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na HAVAN, loja de departamentos de Eletrodomésticos, Eletrônicos e Decoração. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havan.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-havan.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'cod_produto' => 3391,
                'nome_produto' => 'HAVAN',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na HAVAN, loja de departamentos de Eletrodomésticos, Eletrônicos e Decoração. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havan.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-havan.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'cod_produto' => 3392,
                'nome_produto' => 'LILI WOOD',
                'descricao_produto' => 'Voucher digital para aquisição de produtos de decoração e iluminação na LILI WOOD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Não pode incluir o valor do frete no voucher. 
Para compras e mais informações acesse: liliwood.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-liliwood.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'cod_produto' => 3393,
                'nome_produto' => 'LILI WOOD',
                'descricao_produto' => 'Voucher digital para aquisição de produtos de decoração e iluminação na LILI WOOD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Não pode incluir o valor do frete no voucher. 
Para compras e mais informações acesse: liliwood.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-liliwood.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'cod_produto' => 3394,
                'nome_produto' => 'LILI WOOD',
                'descricao_produto' => 'Voucher digital para aquisição de produtos de decoração e iluminação na LILI WOOD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Não pode incluir o valor do frete no voucher. 
Para compras e mais informações acesse: liliwood.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-liliwood.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'cod_produto' => 3395,
                'nome_produto' => 'OPPA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na OPPA, loja de móveis e produtos de decoração.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.                                                 ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.oppa.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-oppa.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'cod_produto' => 3396,
                'nome_produto' => 'OPPA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na OPPA, loja de móveis e produtos de decoração.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo. ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.oppa.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-oppa.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'cod_produto' => 3397,
                'nome_produto' => 'OPPA',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na OPPA, loja de móveis e produtos de decoração.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.oppa.com.br',
                'valor_produto' => 20000,
                'valor_reais' => '1.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-oppa.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'cod_produto' => 3398,
                'nome_produto' => 'OPPA',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na OPPA, loja de móveis e produtos de decoração.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.                                                 ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.oppa.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-casa-e-decoracao-oppa.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'cod_produto' => 3399,
                'nome_produto' => 'PONTO FRIO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no PONTO FRIO.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.pontofrio.com.br  ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-ponto-frio.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'cod_produto' => 3400,
                'nome_produto' => 'PONTO FRIO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no PONTO FRIO.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.pontofrio.com.br  ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-ponto-frio.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'cod_produto' => 3401,
                'nome_produto' => 'PONTO FRIO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no PONTO FRIO.
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
            'informacoes_importantes' => 'O cartão que receberá é o "Multicash", aceito em toda rede do Grupo Pão de Açúcar (Assaí, Casas Bahia, Extra, Pão de Açúcar, Ponto Frio, Postos de Combustíveis e Drogarias).
Não é válido para recarga de crédito de celular.   
Para endereços e mais informações acesse: www.pontofrio.com.br  ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-ponto-frio.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'cod_produto' => 3402,
                'nome_produto' => 'RAKUTEN',
                'descricao_produto' => 'Voucher digital  para aquisição de produtos na RAKUTEN, Marketplace com mais de 30 diferentes categorias de produtos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.rakuten.com.br                                                                                                                                                                                                                                                                                                                                                                                 ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-casa-e-decoracao-rakuten.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            113 => 
            array (
                'cod_produto' => 3403,
                'nome_produto' => 'RAKUTEN',
                'descricao_produto' => 'Voucher digital  para aquisição de produtos na RAKUTEN, Marketplace com mais de 30 diferentes categorias de produtos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.rakuten.com.br                                                                                                                                                                                                                                                                                                                                                                                 ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-casa-e-decoracao-rakuten.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'cod_produto' => 3404,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAMS CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    
Para as lojas Sam’s Club poderão ser utilizados até, no máximo, nove (09) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                          
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'cod_produto' => 3405,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAM\'S CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    
Para as lojas Sam’s Club poderão ser utilizados até, no máximo, nove (09) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                          
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'cod_produto' => 3406,
                'nome_produto' => 'SAMS CLUB',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados SAM\'S CLUB, integrante do grupo Walmart.   
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    
Para as lojas Sam’s Club poderão ser utilizados até, no máximo, nove (09) cartões por compra.',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).                
Recarga de Celular e Vale Gás (consulte disponibilidade na loja escolhida).   
Para utilizar o cartão no Sam’s Club é preciso ser sócio.                                                                                                                                                                                                                                                                                                                                                                                          
Para endereços e mais informações acesse: www.samsclub.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-sam\'s-club.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'cod_produto' => 3407,
                'nome_produto' => 'TELHANORTE',
                'descricao_produto' => 'Cartão para aquisição de produtos na TELHANORTE, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.      ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.telhanorte.com.br                                                                                                                                                                                                                                                                        ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-telha-norte.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'cod_produto' => 3408,
                'nome_produto' => 'TELHANORTE',
                'descricao_produto' => 'Cartão para aquisição de produtos na TELHANORTE, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.      ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.telhanorte.com.br                                                                                                                                                                                                                                                                        ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-telha-norte.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'cod_produto' => 3409,
                'nome_produto' => 'TELHANORTE',
                'descricao_produto' => 'Cartão para aquisição de produtos na TELHANORTE, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.      ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.telhanorte.com.br                                                                                                                                                                                                                                                                        ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-telha-norte.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:56',
                'updated_at' => '2019-06-05 20:04:56',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'cod_produto' => 3410,
                'nome_produto' => 'TOK&STOK',
                'descricao_produto' => 'Cartão para aquisição de móveis e decorações na TOK&STOCK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tokstok.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-tok-stok.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'cod_produto' => 3411,
                'nome_produto' => 'TOK&STOK',
                'descricao_produto' => 'Cartão para aquisição de móveis e decorações na TOK&STOCK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tokstok.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-tok-stok.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'cod_produto' => 3412,
                'nome_produto' => 'TOK&STOK',
                'descricao_produto' => 'Cartão para aquisição de móveis e decorações na TOK&STOCK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tokstok.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-tok-stok.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'cod_produto' => 3413,
                'nome_produto' => 'TOK&STOK',
                'descricao_produto' => 'Cartão para aquisição de móveis e decorações na TOK&STOCK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tokstok.com.br',
                'valor_produto' => 15000,
                'valor_reais' => '750.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-tok-stok.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'cod_produto' => 3414,
                'nome_produto' => 'TOK&STOK',
                'descricao_produto' => 'Voucher digital para aquisição de móveis e decorações na TOK&STOCK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tokstok.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-casa-e-decoracao-tok-stok.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'cod_produto' => 3415,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'cod_produto' => 3416,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'cod_produto' => 3417,
                'nome_produto' => 'WALMART',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nos supermercados WALMART.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.  
Para as lojas BIG, Nacional, Mercadorama, Maxxi Atacado e Todo Dia Sul poderão ser utilizados até, no máximo, sete (07) cartões por compra. 
Para as lojas Walmart, Sam’s Club e Todo Dia Sudeste poderão ser utilizados até, no máximo, nove (09) cartões por compra. 
Para as lojas Hiper Bompreço, Bompreço e Todo Dia Nordeste poderão ser utilizados até, no máximo, dois (02) cartões por compra. ',
            'informacoes_importantes' => 'O cartão que receberá é o "Super Premiação", aceito em toda rede do Grupo Walmart (Big, Bom Preço, Maxxi, Mercadorama, Nacional, Sams Club, Todo Dia e Walmart).  
Para Recarga de Celular e Vale Gás consulte disponibilidade na loja escolhida.                                                                                                                                                                                                                                                                                                                                                                                                     Para endereços e mais informações acesse: www.walmartbrasil.com.br ',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-superpremiacao-walmart.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'cod_produto' => 3418,
                'nome_produto' => 'ENGLISH LIVE MENSAL',
                'descricao_produto' => 'Voucher digital para uso na escola online de idiomas English Live.
Aulas de inglês com focos em viagens, cursos para business e provas técnicas no exterior. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em APENAS em e-commerce.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Exclusivo para o curso English Live Viagens.
Durabilidade de 1 mês de aulas. 
Para compras, utilização e mais informações acesse: www.englishlive.com.br',
                'valor_produto' => 1180,
                'valor_reais' => '59.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-curso-englishliveviagens-mensal.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'cod_produto' => 3419,
                'nome_produto' => 'ENGLISH LIVE TRIMESTRAL',
                'descricao_produto' => 'Voucher digital para uso na escola online de idiomas English Live.
Aulas de inglês com focos em viagens, cursos para business e provas técnicas no exterior. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em APENAS em e-commerce.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Exclusivo para o curso English Live Viagens.
Durabilidade de 3 meses de aulas. 
Para compras, utilização e mais informações acesse: www.englishlive.com.br',
                'valor_produto' => 3780,
                'valor_reais' => '189.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-curso-englishliveviagens-trimestral.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'cod_produto' => 3420,
                'nome_produto' => 'BARATEIRO.COM',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no BARATEIRO.COM.
Loja especializada em eletrodomésticos e eletroeletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Pode ser utilizado várias vezes até zerar o saldo.            ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.barateiro.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-barateirocom.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'cod_produto' => 3421,
                'nome_produto' => 'BARATEIRO.COM',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no BARATEIRO.COM.
Loja especializada em eletrodomésticos e eletroeletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Pode ser utilizado várias vezes até zerar o saldo.            ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.barateiro.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-barateirocom.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'cod_produto' => 3422,
                'nome_produto' => 'BARATEIRO.COM',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto no BARATEIRO.COM.
Loja especializada em eletrodomésticos e eletroeletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Pode ser utilizado várias vezes até zerar o saldo.            ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.barateiro.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-multicash-barateirocom.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'cod_produto' => 3423,
                'nome_produto' => 'CARREFOUR.COM',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'O voucher digital que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-carrefour.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'cod_produto' => 3424,
                'nome_produto' => 'CARREFOUR.COM',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nos supermercados CARREFOUR.
Hipermercado e loja de departamentos que oferece produtos variados.   ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'O voucher digital que receberá é aceito nas lojas Carrefour, Carrefour Bairro, Postos de Combustíveis Carrefour, Drogarias Carrefour e Carrefour Express.
Válido para recarga de crédito de celular.                                                                                                                                                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.carrefour.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-carrefour.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'cod_produto' => 3425,
                'nome_produto' => 'CASAS BAHIA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nas CASAS BAHIA.COM.BR
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.casasbahia.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-casasbahia.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'cod_produto' => 3426,
                'nome_produto' => 'EXTRA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto no EXTRA.COM.
Comercializa produtos variados como móveis, eletrônicos, celulares, notebooks.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'O cartão presente não é aceito para compra de alimentos ou outros itens do site deliveryextra.com.br. 
Para compras e mais informações acesse:  www.extra.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-extra.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:57',
                'updated_at' => '2019-06-05 20:04:57',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'cod_produto' => 3427,
                'nome_produto' => 'FASTSHOP',
                'descricao_produto' => 'Cartão para aquisição de produtos na FASTSHOP.
Loja especializada em eletrodomésticos e eletroeletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Compras EXCLUSIVAMENTE através do site:  www.fastincentivos.com.br/Incentivale
Não permite saldo remanescente. ',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito no site www.fastshop.com.br',
                'valor_produto' => 40000,
                'valor_reais' => '2000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-eletronicos-fastshop.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'cod_produto' => 3428,
                'nome_produto' => 'POLISHOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na POLISHOP.
Loja de produtos de beleza, cadeiras de massagem, elétricos e eletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O valor da compra com o Cartão Presente deverá ser pelo menos R$ 1 acima do valor do cartão utilizado.
Não será considerado o valor do frete neste total, apenas o valor do produto.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.polishop.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-polishop.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'cod_produto' => 3429,
                'nome_produto' => 'POLISHOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na POLISHOP.
Loja de produtos de beleza, cadeiras de massagem, elétricos e eletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O valor da compra com o Cartão Presente deverá ser pelo menos R$ 1 acima do valor do cartão utilizado.
Não será considerado o valor do frete neste total, apenas o valor do produto.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.polishop.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-polishop.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'cod_produto' => 3430,
                'nome_produto' => 'POLISHOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na POLISHOP.
Loja de produtos de beleza, cadeiras de massagem, elétricos e eletrônicos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O valor da compra com o Cartão Presente deverá ser pelo menos R$ 1 acima do valor do cartão utilizado.
Não será considerado o valor do frete neste total, apenas o valor do produto.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.polishop.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-polishop.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'cod_produto' => 3431,
                'nome_produto' => 'PONTO FRIO.COM',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na PONTO FRIO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.   
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.                             ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.pontofrio.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-pontofrio.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'cod_produto' => 3432,
                'nome_produto' => 'SHOPTIME',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na SHOPTIME.
Varejo de eletrodomésticos, eletroportáteis e utensílios de cozinha, cama, mesa, banho, dentre outros.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.www.shoptime.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-shoptime.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'cod_produto' => 3433,
                'nome_produto' => 'SHOPTIME',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na SHOPTIME.
Varejo de eletrodomésticos, eletroportáteis e utensílios de cozinha, cama, mesa, banho, dentre outros.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.www.shoptime.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-shoptime.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'cod_produto' => 3434,
                'nome_produto' => 'SUBMARINO',
                'descricao_produto' => 'Voucher digital para aquisição produtos no SUBMARINO, como celulares, eletrodomésticos, livros e jogos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Não permite saldo remanescente.
Permitida a utilização de até 2 (dois) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                       ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.submarino.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-eletronicos-submarino.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'cod_produto' => 3435,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.    ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-centauro.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'cod_produto' => 3436,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-centauro.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'cod_produto' => 3437,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-centauro.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'cod_produto' => 3438,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-centauro.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'cod_produto' => 3439,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras, endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'e00266a144d52b4ccf5d628abdb6e163.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'cod_produto' => 3440,
                'nome_produto' => 'DECATHLON',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas  DECATHLON, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.                                                                                                                                                                                                                                                                                                                                                                      
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.decathlon.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-decathlon.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'cod_produto' => 3441,
                'nome_produto' => 'DECATHLON',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas  DECATHLON, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.                                                                                                                                                                                                                                                                                                                                                                   
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.decathlon.com.br ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-decathlon.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'cod_produto' => 3442,
                'nome_produto' => 'DECATHLON',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas  DECATHLON, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.                                                                                                                                                                                                                                                                                                                                                      
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.decathlon.com.br ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-decathlon.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'cod_produto' => 3443,
                'nome_produto' => 'DECATHLON',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nas lojas  DECATHLON, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.                                                                                                                                                                                                                                                                                                                                                            
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.decathlon.com.br ',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-decathlon.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:58',
                'updated_at' => '2019-06-05 20:04:58',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'cod_produto' => 3444,
                'nome_produto' => 'DECATHLON',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nas lojas  DECATHLON, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.decathlon.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-decathlon.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'cod_produto' => 3445,
                'nome_produto' => 'NBA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na  NBA.COM, que comercializa produtos da liga profissional de basquete.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                           ',
                'informacoes_importantes' => 'Para compras e mais informações acesse:www.lojaNBA.com',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-nba.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'cod_produto' => 3446,
                'nome_produto' => 'NBA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na  NBA.COM, que comercializa produtos da liga profissional de basquete.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                           ',
                'informacoes_importantes' => 'Para compras e mais informações acesse:www.lojaNBA.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-nba.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'cod_produto' => 3447,
                'nome_produto' => 'NBA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na  NBA.COM, que comercializa produtos da liga profissional de basquete.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                           ',
                'informacoes_importantes' => 'Para compras e mais informações acesse:www.lojaNBA.com',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-nba.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'cod_produto' => 3448,
                'nome_produto' => 'NETSHOES',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na NETSHOES, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.  ',
                'informacoes_importantes' => 'Para mais informações acesse: www.netshoes.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-netshoes.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'cod_produto' => 3449,
                'nome_produto' => 'NETSHOES',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na NETSHOES, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.  ',
                'informacoes_importantes' => 'Para mais informações acesse: www.netshoes.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-netshoes.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'cod_produto' => 3450,
                'nome_produto' => 'NETSHOES',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto na NETSHOES, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.  ',
                'informacoes_importantes' => 'Para mais informações acesse: www.netshoes.com.br',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-esportes-netshoes.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'cod_produto' => 3451,
                'nome_produto' => 'NETSHOES',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na NETSHOES, especializada em artigos esportivos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5(cinco) Cartões Presente por compra realizada.  ',
                'informacoes_importantes' => 'Para mais informações acesse: www.netshoes.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-esportes-netshoes.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'cod_produto' => 3452,
                'nome_produto' => 'CESTAS MICHELLI',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na CESTAS MICHELLI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Você deverá inserir o cógido do voucher no local do site indicado para digitação.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.cestasmichelli.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-cestasmichelli.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'cod_produto' => 3453,
                'nome_produto' => 'CESTAS MICHELLI',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na CESTAS MICHELLI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Você deverá inserir o cógido do voucher no local do site indicado para digitação.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.cestasmichelli.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-cestasmichelli.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'cod_produto' => 3454,
                'nome_produto' => 'CESTAS MICHELLI',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na CESTAS MICHELLI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Você deverá inserir o cógido do voucher no local do site indicado para digitação.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.cestasmichelli.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-cestasmichelli.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'cod_produto' => 3455,
                'nome_produto' => 'GIULIANA FLORES',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na GIULIANA FLORES.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.giulianaflores.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-giulianaflores.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'cod_produto' => 3456,
                'nome_produto' => 'GIULIANA FLORES',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na GIULIANA FLORES.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.giulianaflores.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-giulianaflores.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'cod_produto' => 3457,
                'nome_produto' => 'GIULIANA FLORES',
                'descricao_produto' => 'Voucher digital para aquisição de flores, cestas de café da manhã e presentes na GIULIANA FLORES.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.giulianaflores.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-flores-presentess-giulianaflores.jpg',
                'cod_categoria_produto' => 1,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'cod_produto' => 3458,
                'nome_produto' => 'INGRESSO.COM',
                'descricao_produto' => 'Voucher digital para aquisição de ingressos em cinemas como Cinemark, Cinépolis, Kiniplex, UCI, dentre outros. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de um vale por compra. 
Válido para qualquer dia da semana e horário, e também para salas 3D, Vip e especiais. ',
                'informacoes_importantes' => 'O vale não poderá ser cancelado, trocado ou devolvido. 
Para compras e mais informações acesse: www.ingresso.com',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-laser-cultura-ingresso.com.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:04:59',
                'updated_at' => '2019-06-05 20:04:59',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'cod_produto' => 3459,
                'nome_produto' => 'O MELHOR DA VIDA',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'A aquisição dos pacotes deve ser realizada no site www.omelhordavida.com.br.
O Presente Experiência é válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.                                                                                                                                                                                                                          Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'O parceiro reserva-se o direito de não aceitar a reserva de mais de um voucher para uma única reserva.                                                                                                                                                                                                                                                                                                                                                O voucher é válido para uma única atividade dentre as diversas apresentadas no site www.omelhordavida.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-laser-cultura-omelhordavida.com.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'cod_produto' => 3460,
                'nome_produto' => 'CINEMARK  1 PAR 3D',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos para sessões 3D na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto XD, D-BOX e salas Prime.                   ',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 920,
                'valor_reais' => '46.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinemark-3d-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'cod_produto' => 3461,
                'nome_produto' => 'CINEMARK 1 PAR',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier e XD. ',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 800,
                'valor_reais' => '40.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinemark-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'cod_produto' => 3462,
                'nome_produto' => 'CINEMARK 1 PAR COMBO',
            'descricao_produto' => 'Cartão para aquisição de um par de ingressos e combo (pipoca com refrigerante) na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier, 3D e XD.',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 1240,
                'valor_reais' => '62.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinemark-combo-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'cod_produto' => 3463,
                'nome_produto' => 'CINEMARK 2 PARES',
                'descricao_produto' => 'Cartão para aquisição de dois pares de ingressos na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier e XD. ',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinemark-2pares.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'cod_produto' => 3464,
                'nome_produto' => 'CINEMARK 3 PARES',
                'descricao_produto' => 'Cartão para aquisição de três pares de ingressos na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier e XD. ',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 2400,
                'valor_reais' => '120.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinemark-3pares.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'cod_produto' => 3465,
                'nome_produto' => 'CINEMARK INDIVIDUAL',
                'descricao_produto' => 'Voucher para aquisição de um par de ingressos na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier, 3D e XD.',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-lazer-cinemark-individual.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'cod_produto' => 3466,
                'nome_produto' => 'CINÉPOLIS  1 PAR',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos na rede de cinemas CINÉPOLIS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto 3D, salas Cinépolis VIP®, Macro XE®, IMAX® e 4DX®.                                                                                                                                                                                                                                                                                                             Não é válido para sessões especiais, como eventos esportivos, transmissões ao vivo, óperas e outros.                    ',
                'informacoes_importantes' => 'Acesse o site www.omelhordavida.com.br e encontre a sua nova experiência!',
                'valor_produto' => 680,
                'valor_reais' => '34.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinepolis-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'cod_produto' => 3467,
                'nome_produto' => 'CINÉPOLIS 1 PAR COMBO',
            'descricao_produto' => 'Cartão para aquisição de um par de ingressos  e combo (pipoca com refrigerante) na rede de cinemas CINÉPOLIS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto 3D, salas Cinépolis VIP®, Macro XE®, IMAX® e 4DX®.                                                                                                                                                                                                                                                                                                             Não é válido para sessões especiais, como eventos esportivos, transmissões ao vivo, óperas e outros.                    ',
                'informacoes_importantes' => 'Você receberá o ingresso para acesso à sessão e pipoca média salgada com refrigerante 750ml.
Para informações de salas e programação acesse:  www.cinepolis.com.br',
                'valor_produto' => 1240,
                'valor_reais' => '62.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-cinepolis-1par-combo.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'cod_produto' => 3468,
                'nome_produto' => 'KINOPLEX 1 PAR',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos na rede de cinemas KINOPLEX.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente. 
Válido para qualquer dia da semana e horário da rede Severiano Ribeiro/Kinoplex, exceto salas 3D, Platinum, VIP e em parcerias com a UCI.',
                'informacoes_importantes' => 'Para informações de salas e programação acesse:  www.kinoplex.com.br',
                'valor_produto' => 760,
                'valor_reais' => '38.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-kinoplex-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'cod_produto' => 3469,
                'nome_produto' => 'O MELHOR DA VIDA',
                'descricao_produto' => 'Cartão da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'A aquisição dos pacotes deve ser realizada no site www.omelhordavida.com.br.
O Presente Experiência é válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.                                                                                                                                                                                                                          Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'O parceiro reserva-se o direito de não aceitar a reserva de mais de um voucher para uma única reserva.                                                                                                                                                                                                                                                                                                                                                O voucher é válido para uma única atividade dentre as diversas apresentadas no site www.omelhordavida.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-omelhordavida.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'cod_produto' => 3470,
                'nome_produto' => 'O MELHOR DA VIDA',
                'descricao_produto' => 'Cartão da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'A aquisição dos pacotes deve ser realizada no site www.omelhordavida.com.br.
O Presente Experiência é válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.                                                                                                                                                                                                                          Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'O parceiro reserva-se o direito de não aceitar a reserva de mais de um voucher para uma única reserva.                                                                                                                                                                                                                                                                                                                                                O voucher é válido para uma única atividade dentre as diversas apresentadas no site www.omelhordavida.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-omelhordavida.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'cod_produto' => 3471,
                'nome_produto' => 'O MELHOR DA VIDA',
                'descricao_produto' => 'Cartão da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'A aquisição dos pacotes deve ser realizada no site www.omelhordavida.com.br.
O Presente Experiência é válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.                                                                                                                                                                                                                          Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'O parceiro reserva-se o direito de não aceitar a reserva de mais de um voucher para uma única reserva.                                                                                                                                                                                                                                                                                                                                                O voucher é válido para uma única atividade dentre as diversas apresentadas no site www.omelhordavida.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-omelhordavida.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'cod_produto' => 3472,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-mastercard.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'cod_produto' => 3473,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-mastercard.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'cod_produto' => 3474,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento ENTRETENIMENTO que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-mastercard.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'cod_produto' => 3475,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento ENTRETENIMENTO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-visa.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'cod_produto' => 3476,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento ENTRETENIMENTO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-visa.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'cod_produto' => 3477,
                'nome_produto' => 'SEGMENTADO ENTRETENIMENTO VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento ENTRETENIMENTO que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento ENTRETENIMENTO que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-segmentado-entretenimento-visa.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => ' 6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:00',
                'updated_at' => '2019-06-05 20:05:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'cod_produto' => 3478,
                'nome_produto' => 'UCI 1 PAR',
            'descricao_produto' => 'Cartão para aquisição de um par de ingressos e combo (pipoca com refrigerante) na rede de cinemas UCI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário,  para todos os filmes e sessões, exceto nos cinemas em Salvador/BA e salas 3D, IMAX, de Lux e 4D.     ',
                'informacoes_importantes' => 'Você receberá o ingresso para acesso ao cinema e pipoca pequena com refrigerante 500ml.
Para informações de salas e programação acesse: www.ucicinemas.com.br',
                'valor_produto' => 800,
                'valor_reais' => '40.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-uci-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'cod_produto' => 3479,
                'nome_produto' => 'UCI 1 PAR COMBO',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos na rede de cinemas UCI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto nos cinemas em Salvador/BA e salas 3D, IMAX, de Lux e 4D.     ',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.ucicinemas.com.br',
                'valor_produto' => 1300,
                'valor_reais' => '65.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-lazer-uci-combo-1par.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'cod_produto' => 3480,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'cod_produto' => 3481,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'cod_produto' => 3482,
                'nome_produto' => 'AMERICANAS.COM',
                'descricao_produto' => 'Cartão para aquisição de produtos na AMERICANAS.COM, empresa do varejo de serviços e produtos diversos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.americanas.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-casa-e-decoracao-americanas.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'cod_produto' => 3483,
                'nome_produto' => 'LIVRARIA CULTURA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA CULTURA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.livrariacultura.com.br',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-cultura.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'cod_produto' => 3484,
                'nome_produto' => 'LIVRARIA CULTURA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA CULTURA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.livrariacultura.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-cultura.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'cod_produto' => 3485,
                'nome_produto' => 'LIVRARIA CULTURA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA CULTURA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Pode ser utilizado várias vezes até zerar o saldo.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.livrariacultura.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-cultura.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'cod_produto' => 3486,
                'nome_produto' => 'LIVRARIA DA VILA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA DA VILA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para mais informações acesse: www.livrariadavila.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-livraria-da-vila.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'cod_produto' => 3487,
                'nome_produto' => 'LIVRARIA DA VILA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA DA VILA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para mais informações acesse: www.livrariadavila.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-livraria-da-vila.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'cod_produto' => 3488,
                'nome_produto' => 'LIVRARIA DA VILA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA DA VILA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.          ',
                'informacoes_importantes' => 'Para mais informações acesse: www.livrariadavila.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-livraria-da-vila.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'cod_produto' => 3489,
                'nome_produto' => 'LIVRARIA SARAIVA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA SARAIVA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras, endereços e mais informações acesse: www.saraiva.com.br',
                'valor_produto' => 1400,
                'valor_reais' => '70.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-saraiva.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'cod_produto' => 3490,
                'nome_produto' => 'LIVRARIA SARAIVA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA SARAIVA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras, endereços e mais informações acesse: www.saraiva.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-saraiva.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'cod_produto' => 3491,
                'nome_produto' => 'LIVRARIA SARAIVA',
                'descricao_produto' => 'Cartão para aquisição de livros e demais produtos na LIVRARIA SARAIVA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras, endereços e mais informações acesse: www.saraiva.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-saraiva.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'cod_produto' => 3492,
                'nome_produto' => 'LIVRARIA SARAIVA',
                'descricao_produto' => 'Voucher digital para aquisição de livros e demais produtos na LIVRARIA SARAIVA, como DVDs, Blu-rays, CDs, vinis, games, e-books, audiolivros e mais.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.      
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras, endereços e mais informações acesse: www.saraiva.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-papelaria-e-livraria-saraiva.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'cod_produto' => 3493,
                'nome_produto' => 'OFFICE 365',
                'descricao_produto' => 'Cartão para aquisição do OFFICE 365 HOME, assinatura de 1 ano, disponível para até 5 usuários.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Requisitos do sistema: Ter uma conta Microsoft do Skype. Acesso à internet (tarifas aplicáveis) alguns recursos podem exigir hardware adicional.
PC: Somente Sistema Operacional Windows 7, 8 ou 8.1 de 32 ou 64 bits. MAC: Processador Intel, Mac OS X versão 10.6 ou 10.7 1GB de RAM 2.5GB de espaço disponível em disco.
Formato de disco rígido HFS + resolução de tela de 1280×800.                                                                                                                                                                                                                                                                                                                                                                                                                                     Chave do produto (Product Key) interna. Uso não comercial. É preciso aceitar o Contrato de Licença, www.microsoft.com/useterms. Ativação necessária. ',
                'informacoes_importantes' => 'Funciona em até 5 PCs, Macs ou tablets, espaço no OneDrive e 60 minutos grátis no Skype.                                                                                            
A disponibilidade varia conforme idioma.                                                                                                                                                                                                                                                                                                                     
Os minutos Skype para o mundo estão disponíveis somente para os países selecionados.                                                                                                                                                                                                                                                                                                                                                                                        Para mais informações (incluindo requisitos do sistema a para iPad, iPhone e Android) acesse: www.office.com/information',
                'valor_produto' => 5980,
                'valor_reais' => '299.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-office.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'cod_produto' => 3494,
                'nome_produto' => 'OFFICE 365',
                'descricao_produto' => 'Voucher digital para aquisição do OFFICE 365 HOME, assinatura de 1 ano, disponível para até 5 usuários.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Requisitos do sistema: Ter uma conta Microsoft do Skype. Acesso à internet (tarifas aplicáveis) alguns recursos podem exigir hardware adicional.
PC: Somente Sistema Operacional Windows 7, 8 ou 8.1 de 32 ou 64 bits. MAC: Processador Intel, Mac OS X versão 10.6 ou 10.7 1GB de RAM 2.5GB de espaço disponível em disco.
Formato de disco rígido HFS + resolução de tela de 1280×800.                                                                                                                                                                                                                                                                                                                                                                                                                                     Chave do produto (Product Key) interna. Uso não comercial. É preciso aceitar o Contrato de Licença, www.microsoft.com/useterms. Ativação necessária. ',
                'informacoes_importantes' => 'Funciona em até 5 PCs, Macs ou tablets, espaço no OneDrive e 60 minutos grátis no Skype.                                                                                            
A disponibilidade varia conforme idioma.                                                                                                                                                                                                                                                                                                                     
Os minutos Skype para o mundo estão disponíveis somente para os países selecionados.                                                                                                                                                                                                                                                                                                                                                                                        Para mais informações (incluindo requisitos do sistema a para iPad, iPhone e Android) acesse: www.office.com/information',
                'valor_produto' => 5980,
                'valor_reais' => '299.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-papelaria-e-livraria-office.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'cod_produto' => 3495,
                'nome_produto' => 'SEGMENTADO PAPELARIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento PAPELARIA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento PAPELARIA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 1400,
                'valor_reais' => '70.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-segmentado-papelaria-visa.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'cod_produto' => 3496,
                'nome_produto' => 'SEGMENTADO PAPELARIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento PAPELARIA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento PAPELARIA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-segmentado-papelaria-visa.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:01',
                'updated_at' => '2019-06-05 20:05:01',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'cod_produto' => 3497,
                'nome_produto' => 'SEGMENTADO PAPELARIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento PAPELARIA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento PAPELARIA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-segmentado-papelaria-visa.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'cod_produto' => 3498,
                'nome_produto' => 'SUBMARINO',
                'descricao_produto' => 'Cartão para aquisição produtos no SUBMARINO, como celulares, eletrodomésticos, livros e jogos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Não permite saldo remanescente.
Permitida a utilização de até 2 (dois) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                       ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.submarino.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-submarino.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'cod_produto' => 3499,
                'nome_produto' => 'SUBMARINO',
                'descricao_produto' => 'Cartão para aquisição produtos no SUBMARINO, como celulares, eletrodomésticos, livros e jogos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Não permite saldo remanescente.
Permitida a utilização de até 2 (dois) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                       ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.submarino.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-submarino.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'cod_produto' => 3500,
                'nome_produto' => 'SUBMARINO',
                'descricao_produto' => 'Cartão para aquisição produtos no SUBMARINO, como celulares, eletrodomésticos, livros e jogos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Não permite saldo remanescente.
Permitida a utilização de até 2 (dois) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                       ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.submarino.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-submarino.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'cod_produto' => 3501,
                'nome_produto' => 'UBOOK ANUAL',
                'descricao_produto' => 'Cartão do UBOOK, serviço por assinatura ANUAL para acesso a acervo de audiolivros, através de  celular, computador ou tablet.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.ubook.com.br',
                'valor_produto' => 3098,
                'valor_reais' => '155.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-ubook-anual.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'cod_produto' => 3502,
                'nome_produto' => 'UBOOK MENSAL',
                'descricao_produto' => 'Cartão do UBOOK, serviço por assinatura MENSAL para acesso a acervo de audiolivros, através de  celular, computador ou tablet.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.ubook.com.br',
                'valor_produto' => 378,
                'valor_reais' => '19.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-ubook-mensal.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'cod_produto' => 3503,
                'nome_produto' => 'UBOOK SEMESTRAL',
                'descricao_produto' => 'Cartão do UBOOK, serviço por assinatura SEMESTRAL para acesso a acervo de audiolivros, através de  celular, computador ou tablet.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.ubook.com.br',
                'valor_produto' => 1788,
                'valor_reais' => '89.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-ubook-semestral.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'cod_produto' => 3504,
                'nome_produto' => 'UBOOK TRIMESTRAL',
                'descricao_produto' => 'Cartão do UBOOK, serviço por assinatura TRIMESTRAL para acesso a acervo de audiolivros, através de  celular, computador ou tablet.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.ubook.com.br',
                'valor_produto' => 1014,
                'valor_reais' => '51.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-papelaria-e-livraria-ubook-trimestral.jpg',
                'cod_categoria_produto' => 9,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'cod_produto' => 3505,
                'nome_produto' => 'SHOESTOCK',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na SHOESTOCK, loja de sapatos femininos, masculinos e infantis, além de bolsas e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereço e mais informações acesse: shoestock.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-ubook-shuestock.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'cod_produto' => 3506,
                'nome_produto' => 'SHOESTOCK',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na SHOESTOCK, loja de sapatos femininos, masculinos e infantis, além de bolsas e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereço e mais informações acesse: shoestock.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-ubook-shuestock.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'cod_produto' => 3507,
                'nome_produto' => 'SHOESTOCK',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na SHOESTOCK, loja de sapatos femininos, masculinos e infantis, além de bolsas e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereço e mais informações acesse: shoestock.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-ubook-shuestock.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'cod_produto' => 3508,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-tiptop.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'cod_produto' => 3509,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-tiptop.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'cod_produto' => 3510,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-tiptop.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'cod_produto' => 3511,
                'nome_produto' => 'VIVARA',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na VIVARA, joias em ouro, prata, pedras e relógios. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Não há limites de vales com compra. 
Resgate somente através do CPF e apresentação do documento.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.vivara.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-vivara.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'cod_produto' => 3512,
                'nome_produto' => 'VIVARA',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na VIVARA, joias em ouro, prata, pedras e relógios. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Não há limites de vales com compra. 
Resgate somente através do CPF e apresentação do documento.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.vivara.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-vivara.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'cod_produto' => 3513,
                'nome_produto' => 'ZATTINI',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na  ZATTINI, especializada em moda. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5 (cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.zattini.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-zattini.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:02',
                'updated_at' => '2019-06-05 20:05:02',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'cod_produto' => 3514,
                'nome_produto' => 'ARTLLURE',
                'descricao_produto' => 'Cartão para aquisição de produtos na ARTLLURE, joias com design exclusivos, em ouro, prata e pedras. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de somente 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.artllure.com.br',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-artllure.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'cod_produto' => 3515,
                'nome_produto' => 'ARTLLURE',
                'descricao_produto' => 'Cartão para aquisição de produtos na ARTLLURE, joias com design exclusivos, em ouro, prata e pedras. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de somente 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.artllure.com.br',
                'valor_produto' => 1800,
                'valor_reais' => '90.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-artllure.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'cod_produto' => 3516,
                'nome_produto' => 'ARTLLURE',
                'descricao_produto' => 'Cartão para aquisição de produtos na ARTLLURE, joias com design exclusivos, em ouro, prata e pedras. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de somente 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.artllure.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-artllure.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'cod_produto' => 3517,
                'nome_produto' => 'ARTLLURE',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na ARTLLURE, joias com design exclusivos, em ouro, prata e pedras. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de somente 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.artllure.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-artllure.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'cod_produto' => 3518,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-c&a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'cod_produto' => 3519,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-c&a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'cod_produto' => 3520,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-c&a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'cod_produto' => 3521,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-c&a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'cod_produto' => 3522,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-moda-c&a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'cod_produto' => 3523,
                'nome_produto' => 'HAVAIANAS',
                'descricao_produto' => 'Cartão para aquisição de produtos na HAVAIANAS, loja de calçados e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Poderá ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havaianas.com.br',
                'valor_produto' => 798,
                'valor_reais' => '39.90',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-havaianas.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'cod_produto' => 3524,
                'nome_produto' => 'HAVAIANAS',
                'descricao_produto' => 'Cartão para aquisição de produtos na HAVAIANAS, loja de calçados e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Poderá ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havaianas.com.br',
                'valor_produto' => 1998,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-havaianas.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'cod_produto' => 3525,
                'nome_produto' => 'HAVAIANAS',
                'descricao_produto' => 'Cartão para aquisição de produtos na HAVAIANAS, loja de calçados e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Poderá ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havaianas.com.br',
                'valor_produto' => 2998,
                'valor_reais' => '149.90',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-havaianas.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'cod_produto' => 3526,
                'nome_produto' => 'MARISA',
                'descricao_produto' => 'Cartão para aquisição de produtos na MARISA, loja especializada em moda feminina, masculina, infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro. 
Para endereços e mais informações acesse: www.marisa.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-marisa.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'cod_produto' => 3527,
                'nome_produto' => 'MARISA',
                'descricao_produto' => 'Cartão para aquisição de produtos na MARISA, loja especializada em moda feminina, masculina, infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro. 
Para endereços e mais informações acesse: www.marisa.com.br',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-marisa.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'cod_produto' => 3528,
                'nome_produto' => 'MARISA',
                'descricao_produto' => 'Cartão para aquisição de produtos na MARISA, loja especializada em moda feminina, masculina, infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro. 
Para endereços e mais informações acesse: www.marisa.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-marisa.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'cod_produto' => 3529,
                'nome_produto' => 'RENNER',
                'descricao_produto' => 'Cartão para aquisição de produtos na RENNER, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.         
Para endereços e mais informações acesse: www.lojasrenner.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-renner.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'cod_produto' => 3530,
                'nome_produto' => 'RENNER',
                'descricao_produto' => 'Cartão para aquisição de produtos na RENNER, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.         
Para endereços e mais informações acesse: www.lojasrenner.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-renner.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'cod_produto' => 3531,
                'nome_produto' => 'RENNER',
                'descricao_produto' => 'Cartão para aquisição de produtos na RENNER, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.         
Para endereços e mais informações acesse: www.lojasrenner.com.br ',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-renner.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'cod_produto' => 3532,
                'nome_produto' => 'RIACHUELO',
                'descricao_produto' => 'Cartão para aquisição de produtos na RIACHUELO, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                    
Para endereços e mais informações acesse: www.riachuelo.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-riachuelo.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'cod_produto' => 3533,
                'nome_produto' => 'RIACHUELO',
                'descricao_produto' => 'Cartão para aquisição de produtos na RIACHUELO, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                    
Para endereços e mais informações acesse: www.riachuelo.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-riachuelo.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'cod_produto' => 3534,
                'nome_produto' => 'RIACHUELO',
                'descricao_produto' => 'Cartão para aquisição de produtos na RIACHUELO, loja especializada em moda feminina, masculina, infantil e casa. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                    
Para endereços e mais informações acesse: www.riachuelo.com.br ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-riachuelo.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:03',
                'updated_at' => '2019-06-05 20:05:03',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'cod_produto' => 3535,
                'nome_produto' => 'SHOULDER',
                'descricao_produto' => 'Cartão para utilização na SHOULDER, loja de roupas e acessórios femininos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em algumas lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Não permite saldo remanescente.        
É necessário o cadastro em loja.  ',
                'informacoes_importantes' => 'No site consta lista das lojas que aceitam o cartão.                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.shoulder.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-shoulder.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'cod_produto' => 3536,
                'nome_produto' => 'SHOULDER',
                'descricao_produto' => 'Cartão para utilização na SHOULDER, loja de roupas e acessórios femininos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em algumas lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Não permite saldo remanescente.        
É necessário o cadastro em loja.  ',
                'informacoes_importantes' => 'No site consta lista das lojas que aceitam o cartão.                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.shoulder.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-shoulder.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'cod_produto' => 3537,
                'nome_produto' => 'SHOULDER',
                'descricao_produto' => 'Cartão para utilização na SHOULDER, loja de roupas e acessórios femininos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em ALGUMAS lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Não permite saldo remanescente.        
É necessário o cadastro em loja.  ',
                'informacoes_importantes' => 'No site consta lista das lojas que aceitam o cartão.                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.shoulder.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-shoulder.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'cod_produto' => 3538,
                'nome_produto' => 'ZARA',
                'descricao_produto' => 'Cartão para aquisição de produtos na ZARA, loja especializada em moda feminina, masculina, infantil e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 10 (dez) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.zara.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zara.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'cod_produto' => 3539,
                'nome_produto' => 'ZARA',
                'descricao_produto' => 'Cartão para aquisição de produtos na ZARA, loja especializada em moda feminina, masculina, infantil e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 10 (dez) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.zara.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zara.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'cod_produto' => 3540,
                'nome_produto' => 'ZARA',
                'descricao_produto' => 'Cartão para aquisição de produtos na ZARA, loja especializada em moda feminina, masculina, infantil e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 10 (dez) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse:  www.zara.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zara.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'cod_produto' => 3541,
                'nome_produto' => 'ZATTINI',
                'descricao_produto' => 'Cartão para aquisição de produtos na  ZATTINI, especializada em moda. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5 (cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.zattini.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zattini.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'cod_produto' => 3542,
                'nome_produto' => 'ZATTINI',
                'descricao_produto' => 'Cartão para aquisição de produtos na  ZATTINI, especializada em moda. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5 (cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.zattini.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zattini.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'cod_produto' => 3543,
                'nome_produto' => 'ZATTINI',
                'descricao_produto' => 'Cartão para aquisição de produtos na  ZATTINI, especializada em moda. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 5 (cinco) Cartões Presente por compra realizada.                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => '
Para compras e mais informações acesse: www.zattini.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-moda-zattini.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'cod_produto' => 3544,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Cartão para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-pet-petz.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            255 => 
            array (
                'cod_produto' => 3545,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Cartão para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito em loja física.
Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-pet-petz.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            256 => 
            array (
                'cod_produto' => 3546,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Cartão para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito em loja física.
Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-pet-petz.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            257 => 
            array (
                'cod_produto' => 3547,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Voucher digital para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.      
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-pet-petz.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            258 => 
            array (
                'cod_produto' => 3548,
                'nome_produto' => 'COCO BAMBU',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes COCO BAMBU.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA nos restaurantes Coco Bambu da cidade de SP.
Para endereços e mais informações acesse: www.cocobambu.com ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-cocobambu.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            259 => 
            array (
                'cod_produto' => 3549,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            260 => 
            array (
                'cod_produto' => 3550,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 1500,
                'valor_reais' => '75.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            261 => 
            array (
                'cod_produto' => 3551,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            262 => 
            array (
                'cod_produto' => 3552,
                'nome_produto' => 'PEIXE URBANO RESTAURANTES',
                'descricao_produto' => 'Voucher digital PEIXE URBANO RESTAURANTES é destinado à aquisição de créditos para consumo em restaurantes.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.peixeurbano.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-peixeurbano.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            263 => 
            array (
                'cod_produto' => 3553,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 2400,
                'valor_reais' => '120.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-pizzahut.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            264 => 
            array (
                'cod_produto' => 3554,
                'nome_produto' => 'SPOLETO',
                'descricao_produto' => 'Voucher digital para utilização nas unidades dos restaurantes SPOLETO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.    ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.spoleto.com.br',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-spoleto.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            265 => 
            array (
                'cod_produto' => 3555,
                'nome_produto' => 'TERRAÇO ITÁLIA',
                'descricao_produto' => 'Voucher digital para uso no restaurante TERRAÇO ITÁLIA em almoço, jantar ou happy hour.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.     
Não é válido em datas comemorativas.
Necessário reservar com 2 semanas de antecedência e a apresentação do convite.',
                'informacoes_importantes' => 'Taxa de serviço de 12% cobrada a parte
Para endereço e mais informações acesse: www.terracoitalia.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-terracoitalia.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:04',
                'updated_at' => '2019-06-05 20:05:04',
                'deleted_at' => NULL,
            ),
            266 => 
            array (
                'cod_produto' => 3556,
                'nome_produto' => 'TGI FRIDAYS',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes TGI FRIDAYS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para uso exclusivo em São Paulo, unidade localizada na Av. Cidade Jardim, 56 - Itaim Bibi. 
Para endereços e mais informações acesse: www.tgifridaysbrasil.com.br',
                'valor_produto' => 1500,
                'valor_reais' => '75.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-tgifridays.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => ' 6meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            267 => 
            array (
                'cod_produto' => 3557,
                'nome_produto' => ' ABBRACCIO',
                'descricao_produto' => 'Cartão para uso nos restaurantes  ABBRACCIO CUCINA ITALIANA. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.abbracciorestaurante.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-abbraccio.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            268 => 
            array (
                'cod_produto' => 3558,
                'nome_produto' => ' ABBRACCIO',
                'descricao_produto' => 'Cartão para uso nos restaurantes  ABBRACCIO CUCINA ITALIANA. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.abbracciorestaurante.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-abbraccio.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            269 => 
            array (
                'cod_produto' => 3559,
                'nome_produto' => ' ABBRACCIO',
                'descricao_produto' => 'Cartão para uso nos restaurantes  ABBRACCIO CUCINA ITALIANA. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.abbracciorestaurante.com.br ',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-abbraccio.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            270 => 
            array (
                'cod_produto' => 3560,
                'nome_produto' => 'AMERICA',
                'descricao_produto' => 'Cartão para uso nos restaurantes AMERICA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.restauranteamerica.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-america.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            271 => 
            array (
                'cod_produto' => 3561,
                'nome_produto' => 'AMERICA',
                'descricao_produto' => 'Cartão para uso nos restaurantes AMERICA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.restauranteamerica.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-america.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            272 => 
            array (
                'cod_produto' => 3562,
                'nome_produto' => 'AMERICA',
                'descricao_produto' => 'Cartão para uso nos restaurantes AMERICA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.restauranteamerica.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-america.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            273 => 
            array (
                'cod_produto' => 3563,
                'nome_produto' => 'AMERICA',
                'descricao_produto' => 'Cartão para uso nos restaurantes AMERICA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.                                                                                                                                                                                                                      ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.restauranteamerica.com.br ',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-america.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            274 => 
            array (
                'cod_produto' => 3564,
                'nome_produto' => 'APPLEBEES',
                'descricao_produto' => 'Cartão para uso nos restaurante APPLEBEE\'S.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA exclusiva  em ALGUNS restaurantes da cidade de SP.
Para endereços e mais informações acesse: www.applebees.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-applebees.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            275 => 
            array (
                'cod_produto' => 3565,
                'nome_produto' => 'APPLEBEES',
                'descricao_produto' => 'Cartão para uso nos restaurante APPLEBEE\'S.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA exclusiva  em ALGUNS restaurantes da cidade de SP.
Para endereços e mais informações acesse: www.applebees.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-applebees.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            276 => 
            array (
                'cod_produto' => 3566,
                'nome_produto' => 'APPLEBEES',
                'descricao_produto' => 'Cartão para uso nos restaurante APPLEBEE\'S.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA exclusiva  em ALGUNS restaurantes da cidade de SP.
Para endereços e mais informações acesse: www.applebees.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-applebees.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            277 => 
            array (
                'cod_produto' => 3567,
                'nome_produto' => 'APPLEBEES BÔNUS',
                'descricao_produto' => 'Voucher digital para uso nos restaurante APPLEBEE\'S.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Não permite saldo remanescente.',
            'informacoes_importantes' => 'O resgate de vale de R$49,90 dá direito a R$70,00 de saldo para consumir (bônus de R$20,10)
Utilização exclusiva  em alguns restaurantes AppleBee\'s  da cidade de SP.
Para endereços e mais informações acesse: www.applebees.com.br',
                'valor_produto' => 998,
                'valor_reais' => '49.90',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-restaurante-applebees.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '1 mês',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            278 => 
            array (
                'cod_produto' => 3568,
                'nome_produto' => 'BACIO DI LATTE',
                'descricao_produto' => 'Cartão para uso nas sorveterias BACIO DI LATTE.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Não permite saldo remanescente.     
O crédito não é válido para uso nos quiosques da marca.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.baciodilatte.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-bacio-di-latte.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses ',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            279 => 
            array (
                'cod_produto' => 3569,
                'nome_produto' => 'COCO BAMBU',
                'descricao_produto' => 'Cartão para uso nos restaurantes COCO BAMBU.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA nos restaurantes Coco Bambu da cidade de SP.
Para endereços e mais informações acesse: www.cocobambu.com ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-coco-bambu.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            280 => 
            array (
                'cod_produto' => 3570,
                'nome_produto' => 'COCO BAMBU',
                'descricao_produto' => 'Cartão para uso nos restaurantes COCO BAMBU.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA nos restaurantes Coco Bambu da cidade de SP.
Para endereços e mais informações acesse: www.cocobambu.com ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-coco-bambu.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            281 => 
            array (
                'cod_produto' => 3571,
                'nome_produto' => 'COCO BAMBU',
                'descricao_produto' => 'Cartão para uso nos restaurantes COCO BAMBU.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA nos restaurantes Coco Bambu da cidade de SP.
Para endereços e mais informações acesse: www.cocobambu.com ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-coco-bambu.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            282 => 
            array (
                'cod_produto' => 3572,
                'nome_produto' => 'JOHNNY ROCKETS',
                'descricao_produto' => 'Cartão para uso nos restaurantes JOHNNY ROCKETS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                                                                                                                                ',
                'informacoes_importantes' => '
Utilização EXCLUSIVA nos restaurantes da cidade de SP.
Para endereços e mais informações acesse:  www.johnnyrockets.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-johnnyrockets.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            283 => 
            array (
                'cod_produto' => 3573,
                'nome_produto' => 'JOHNNY ROCKETS',
                'descricao_produto' => 'Cartão para uso nos restaurantes JOHNNY ROCKETS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                                                                                                                                ',
                'informacoes_importantes' => '
Utilização EXCLUSIVA nos restaurantes da cidade de SP.
Para endereços e mais informações acesse:  www.johnnyrockets.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-johnnyrockets.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            284 => 
            array (
                'cod_produto' => 3574,
                'nome_produto' => 'JOHNNY ROCKETS',
                'descricao_produto' => 'Cartão para uso nos restaurantes JOHNNY ROCKETS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                                                                                                                                ',
                'informacoes_importantes' => '
Utilização EXCLUSIVA nos restaurantes da cidade de SP.
Para endereços e mais informações acesse:  www.johnnyrockets.com.br ',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-johnnyrockets.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:05',
                'updated_at' => '2019-06-05 20:05:05',
                'deleted_at' => NULL,
            ),
            285 => 
            array (
                'cod_produto' => 3575,
                'nome_produto' => 'MADERO',
                'descricao_produto' => 'Cartão para uso nos restaurantes MADERO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Aceito no Madero e Madero Prime.
Para endereços e mais informações acesse: www.restaurantemadero.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-madero.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            286 => 
            array (
                'cod_produto' => 3576,
                'nome_produto' => 'MADERO',
                'descricao_produto' => 'Cartão para uso nos restaurantes MADERO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Aceito no Madero e Madero Prime.
Para endereços e mais informações acesse: www.restaurantemadero.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-madero.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            287 => 
            array (
                'cod_produto' => 3577,
                'nome_produto' => 'MADERO',
                'descricao_produto' => 'Cartão para uso nos restaurantes MADERO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Aceito no Madero e Madero Prime.
Para endereços e mais informações acesse: www.restaurantemadero.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-madero.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            288 => 
            array (
                'cod_produto' => 3578,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Cartão para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            289 => 
            array (
                'cod_produto' => 3579,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Cartão para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            290 => 
            array (
                'cod_produto' => 3580,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Cartão para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-outback.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            291 => 
            array (
                'cod_produto' => 3581,
                'nome_produto' => 'PARIS 6',
                'descricao_produto' => 'Cartão para uso nos restaurantes PARIS 6.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.paris6bistro.com  ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-paris-6.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            292 => 
            array (
                'cod_produto' => 3582,
                'nome_produto' => 'PARIS 6',
                'descricao_produto' => 'Cartão para uso nos restaurantes PARIS 6.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.paris6bistro.com  ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-paris-6.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            293 => 
            array (
                'cod_produto' => 3583,
                'nome_produto' => 'PEIXE URBANO RESTAURANTES',
                'descricao_produto' => 'Cartão PEIXE URBANO RESTAURANTES é destinado à aquisição de créditos para consumo em restaurantes.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.peixeurbano.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-peixeurbano-restaurante.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            294 => 
            array (
                'cod_produto' => 3584,
                'nome_produto' => 'PEIXE URBANO RESTAURANTES',
                'descricao_produto' => 'Cartão PEIXE URBANO RESTAURANTES é destinado à aquisição de créditos para consumo em restaurantes.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.peixeurbano.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-peixeurbano-restaurante.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            295 => 
            array (
                'cod_produto' => 3585,
                'nome_produto' => 'PEIXE URBANO RESTAURANTES',
                'descricao_produto' => 'Cartão PEIXE URBANO RESTAURANTES é destinado à aquisição de créditos para consumo em restaurantes.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para mais informações acesse: www.peixeurbano.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-peixeurbano-restaurante.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            296 => 
            array (
                'cod_produto' => 3586,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para uso exclusivo na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-pizza-hut.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            297 => 
            array (
                'cod_produto' => 3587,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para uso exclusivo na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 1800,
                'valor_reais' => '90.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-pizza-hut.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            298 => 
            array (
                'cod_produto' => 3588,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para uso exclusivo na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-pizza-hut.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            299 => 
            array (
                'cod_produto' => 3589,
                'nome_produto' => 'SPOLETO',
                'descricao_produto' => 'Cartão para utilização nas unidades dos restaurantes SPOLETO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.    ',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.spoleto.com.br',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-spoleto.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            300 => 
            array (
                'cod_produto' => 3590,
                'nome_produto' => 'STARBUCKS',
                'descricao_produto' => 'Cartão para uso nos cafés STARBUCKS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.starbucks.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-starbucks.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            301 => 
            array (
                'cod_produto' => 3591,
                'nome_produto' => 'STARBUCKS',
                'descricao_produto' => 'Cartão para uso nos cafés STARBUCKS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.starbucks.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-starbucks.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            302 => 
            array (
                'cod_produto' => 3592,
                'nome_produto' => 'TGI FRIDAYS',
                'descricao_produto' => 'Cartão para uso nos restaurantes TGI FRIDAYS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para uso exclusivo em São Paulo, unidade localizada na Av. Cidade Jardim, 56 - Itaim Bibi. 
Para endereços e mais informações acesse: www.tgifridaysbrasil.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-tgifridays.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => ' 6meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            303 => 
            array (
                'cod_produto' => 3593,
                'nome_produto' => 'TGI FRIDAYS',
                'descricao_produto' => 'Cartão para uso nos restaurantes TGI FRIDAYS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para uso exclusivo em São Paulo, unidade localizada na Av. Cidade Jardim, 56 - Itaim Bibi. 
Para endereços e mais informações acesse: www.tgifridaysbrasil.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-tgifridays.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => ' 6meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            304 => 
            array (
                'cod_produto' => 3594,
                'nome_produto' => 'TGI FRIDAYS',
                'descricao_produto' => 'Cartão para uso nos restaurantes TGI FRIDAYS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para uso exclusivo em São Paulo, unidade localizada na Av. Cidade Jardim, 56 - Itaim Bibi. 
Para endereços e mais informações acesse: www.tgifridaysbrasil.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-restaurante-tgifridays.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => ' 6meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:06',
                'updated_at' => '2019-06-05 20:05:06',
                'deleted_at' => NULL,
            ),
            305 => 
            array (
                'cod_produto' => 3595,
                'nome_produto' => 'ANNA PEGOVA',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos e tratamentos estéticos faciais e corporais ANNA PEGOVA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.annapegova.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-anna-pegova.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            306 => 
            array (
                'cod_produto' => 3596,
                'nome_produto' => 'ANNA PEGOVA',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos e tratamentos estéticos faciais e corporais ANNA PEGOVA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.annapegova.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-anna-pegova.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            307 => 
            array (
                'cod_produto' => 3597,
                'nome_produto' => 'ANNA PEGOVA',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos e tratamentos estéticos faciais e corporais ANNA PEGOVA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.annapegova.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-anna-pegova.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            308 => 
            array (
                'cod_produto' => 3598,
                'nome_produto' => 'ANNA PEGOVA',
                'descricao_produto' => 'Voucher digital destinado à aquisição de produtos e tratamentos estéticos faciais e corporais ANNA PEGOVA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Loja Física: Permite utilização de diversos Cartões Presente
E-commerce: Permitida a utilização de 1 (um) Cartão Presente por compra realizada.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.annapegova.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-saude-e-beleza-anna-pegova.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            309 => 
            array (
                'cod_produto' => 3599,
                'nome_produto' => 'FIVE DIAMONDS',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos FIVE DIAMONDS, empresa de vitaminas e suplementos, pioneira em Nutrição Celular no Brasil.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Compras e mais informações através do site www.fivediamonds.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-five-diamonds.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            310 => 
            array (
                'cod_produto' => 3600,
                'nome_produto' => 'FIVE DIAMONDS',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos FIVE DIAMONDS, empresa de vitaminas e suplementos, pioneira em Nutrição Celular no Brasil.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Compras e mais informações através do site www.fivediamonds.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-five-diamonds.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            311 => 
            array (
                'cod_produto' => 3601,
                'nome_produto' => 'FIVE DIAMONDS',
                'descricao_produto' => 'Cartão destinado à aquisição de produtos FIVE DIAMONDS, empresa de vitaminas e suplementos, pioneira em Nutrição Celular no Brasil.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Compras e mais informações através do site www.fivediamonds.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-five-diamonds.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            312 => 
            array (
                'cod_produto' => 3602,
                'nome_produto' => 'FIVE DIAMONDS',
                'descricao_produto' => 'Voucher digital destinado à aquisição de produtos FIVE DIAMONDS, empresa de vitaminas e suplementos, pioneira em Nutrição Celular no Brasil.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Compras e mais informações através do site www.fivediamonds.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-saude-e-beleza-five-diamonds.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            313 => 
            array (
                'cod_produto' => 3603,
                'nome_produto' => 'O BOTICÁRIO',
                'descricao_produto' => 'Cartão para aquisição de produtos no O BOTICÁRIO, marca de beleza com itens de maquiagem, perfumaria e cuidados pessoais.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo. 
Permitida a utilização de 1 Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Os pagamentos de produtos efetuados com o Cartão não oferecerão pontuação ao Programa Fidelidade. 
Para endereços e mais informações acesse: www.boticario.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-oboticario.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            314 => 
            array (
                'cod_produto' => 3604,
                'nome_produto' => 'O BOTICÁRIO',
                'descricao_produto' => 'Cartão para aquisição de produtos no O BOTICÁRIO, marca de beleza com itens de maquiagem, perfumaria e cuidados pessoais.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo. 
Permitida a utilização de 1 Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Os pagamentos de produtos efetuados com o Cartão não oferecerão pontuação ao Programa Fidelidade. 
Para endereços e mais informações acesse: www.boticario.com.br ',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-oboticario.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            315 => 
            array (
                'cod_produto' => 3605,
                'nome_produto' => 'O BOTICÁRIO',
                'descricao_produto' => 'Cartão para aquisição de produtos no O BOTICÁRIO, marca de beleza com itens de maquiagem, perfumaria e cuidados pessoais.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo. 
Permitida a utilização de 1 Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Os pagamentos de produtos efetuados com o Cartão não oferecerão pontuação ao Programa Fidelidade. 
Para endereços e mais informações acesse: www.boticario.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-oboticario.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            316 => 
            array (
                'cod_produto' => 3606,
                'nome_produto' => 'O BOTICÁRIO',
                'descricao_produto' => 'Cartão para aquisição de produtos no O BOTICÁRIO, marca de beleza com itens de maquiagem, perfumaria e cuidados pessoais.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo. 
Permitida a utilização de 1 Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Os pagamentos de produtos efetuados com o Cartão não oferecerão pontuação ao Programa Fidelidade. 
Para endereços e mais informações acesse: www.boticario.com.br ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-oboticario.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            317 => 
            array (
                'cod_produto' => 3607,
                'nome_produto' => 'PEIXE URBANO BELEZA E FITNESS',
                'descricao_produto' => 'Cartão destinado à aquisição de serviços de beleza e fitness no PEIXE URBANO BELEZA E FITNESS.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Benefício não transferível e não cumulativo com outras ações.
Para mais informações acesse: www.peixeurbano.com.br.',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-peixe-urbano-beleza.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            318 => 
            array (
                'cod_produto' => 3608,
                'nome_produto' => 'PEIXE URBANO BELEZA E FITNESS',
                'descricao_produto' => 'Cartão destinado à aquisição de serviços de beleza e fitness no PEIXE URBANO BELEZA E FITNESS.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Benefício não transferível e não cumulativo com outras ações.
Para mais informações acesse: www.peixeurbano.com.br.',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-peixe-urbano-beleza.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            319 => 
            array (
                'cod_produto' => 3609,
                'nome_produto' => 'PEIXE URBANO BELEZA E FITNESS',
                'descricao_produto' => 'Cartão destinado à aquisição de serviços de beleza e fitness no PEIXE URBANO BELEZA E FITNESS.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Benefício não transferível e não cumulativo com outras ações.
Para mais informações acesse: www.peixeurbano.com.br.',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-peixe-urbano-beleza.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            320 => 
            array (
                'cod_produto' => 3610,
                'nome_produto' => 'PEIXE URBANO BELEZA E FITNESS',
                'descricao_produto' => 'Voucher digital destinado à aquisição de serviços de beleza e fitness no PEIXE URBANO BELEZA E FITNESS.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Benefício não transferível e não cumulativo com outras ações.
Para mais informações acesse: www.peixeurbano.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-saude-e-beleza-peixe-urbano-beleza.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:07',
                'updated_at' => '2019-06-05 20:05:07',
                'deleted_at' => NULL,
            ),
            321 => 
            array (
                'cod_produto' => 3611,
                'nome_produto' => 'SEGMENTADO FARMACIA  MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            322 => 
            array (
                'cod_produto' => 3612,
                'nome_produto' => 'SEGMENTADO FARMACIA  MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            323 => 
            array (
                'cod_produto' => 3613,
                'nome_produto' => 'SEGMENTADO FARMACIA  MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            324 => 
            array (
                'cod_produto' => 3614,
                'nome_produto' => 'SEGMENTADO FARMACIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 1400,
                'valor_reais' => '70.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            325 => 
            array (
                'cod_produto' => 3615,
                'nome_produto' => 'SEGMENTADO FARMACIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            326 => 
            array (
                'cod_produto' => 3616,
                'nome_produto' => 'SEGMENTADO FARMACIA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento FARMÁCIAS E DROGARIAS que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento FARMÁCIAS E DROGRARIAS que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-farmacia-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            327 => 
            array (
                'cod_produto' => 3617,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            328 => 
            array (
                'cod_produto' => 3618,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            329 => 
            array (
                'cod_produto' => 3619,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas do segmento QUALIDADE DE VIDA que aceitem a bandeira MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-mastercard.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            330 => 
            array (
                'cod_produto' => 3620,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            331 => 
            array (
                'cod_produto' => 3621,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            332 => 
            array (
                'cod_produto' => 3622,
                'nome_produto' => 'SEGMENTADO QUALIDADE DE VIDA VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em lojas do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em lojas físicas e e-commerce do segmento QUALIDADE DE VIDA que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para uso no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-segmentado-qualidade-de-vida-visa.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            333 => 
            array (
                'cod_produto' => 3623,
                'nome_produto' => 'SEPHORA',
                'descricao_produto' => 'Cartão para aquisição de produtos de beleza e perfumaria na SEPHORA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito no site.
Para endereços e mais informações acesse: www.sephora.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-sephora.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            334 => 
            array (
                'cod_produto' => 3624,
                'nome_produto' => 'SEPHORA',
                'descricao_produto' => 'Cartão para aquisição de produtos de beleza e perfumaria na SEPHORA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito no site.
Para endereços e mais informações acesse: www.sephora.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-sephora.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            335 => 
            array (
                'cod_produto' => 3625,
                'nome_produto' => 'SEPHORA',
                'descricao_produto' => 'Cartão para aquisição de produtos de beleza e perfumaria na SEPHORA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.                                                                                                                                                                                                                   ',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito no site.
Para endereços e mais informações acesse: www.sephora.com.br',
                'valor_produto' => 7000,
                'valor_reais' => '350.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-saude-e-beleza-sephora.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            336 => 
            array (
                'cod_produto' => 3626,
                'nome_produto' => 'CARREFOUR COMBUSTIVEL',
                'descricao_produto' => 'Cartão para ser utilizado nos postos de combustível da rede CARREFOUR. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Válido somente com o código apresentado no cartão.',
                'informacoes_importantes' => 'O Cartão Presente NÃO pode ser utilizado nas lojas físicas Carrefour e Carrefour Express, nem no site ou farmácias da rede.                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.carrefour.com.br/institucional/lojas/carrefour-posto  ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-carrefour-posto.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            337 => 
            array (
                'cod_produto' => 3627,
                'nome_produto' => 'CARREFOUR COMBUSTIVEL',
                'descricao_produto' => 'Cartão para ser utilizado nos postos de combustível da rede CARREFOUR. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Válido somente com o código apresentado no cartão.',
                'informacoes_importantes' => 'O Cartão Presente NÃO pode ser utilizado nas lojas físicas Carrefour e Carrefour Express, nem no site ou farmácias da rede.                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.carrefour.com.br/institucional/lojas/carrefour-posto  ',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-carrefour-posto.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            338 => 
            array (
                'cod_produto' => 3628,
                'nome_produto' => 'CARREFOUR COMBUSTIVEL',
                'descricao_produto' => 'Cartão para ser utilizado nos postos de combustível da rede CARREFOUR. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Válido somente com o código apresentado no cartão.',
                'informacoes_importantes' => 'O Cartão Presente NÃO pode ser utilizado nas lojas físicas Carrefour e Carrefour Express, nem no site ou farmácias da rede.                                                                                                                                                                                                                                                                                                       Para endereços e mais informações acesse: www.carrefour.com.br/institucional/lojas/carrefour-posto  ',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-carrefour-posto.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            339 => 
            array (
                'cod_produto' => 3629,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira  MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-mastercard.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            340 => 
            array (
                'cod_produto' => 3630,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira  MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-mastercard.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:08',
                'updated_at' => '2019-06-05 20:05:08',
                'deleted_at' => NULL,
            ),
            341 => 
            array (
                'cod_produto' => 3631,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL MASTERCARD',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira MASTERCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira  MASTERCARD.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: www.valepresente.com.br',
                'valor_produto' => 8000,
                'valor_reais' => '400.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-mastercard.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            342 => 
            array (
                'cod_produto' => 3632,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-visa.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            343 => 
            array (
                'cod_produto' => 3633,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-visa.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            344 => 
            array (
                'cod_produto' => 3634,
                'nome_produto' => 'SEGMENTADO COMBUSTÍVEL VISA',
                'descricao_produto' => 'Cartão para ser utilizado exclusivamente em postos de COMBUSTÍVEL que aceitem a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Para uso em postos de COMBUSTIVEL que aceitem a bandeira VISA.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão e senha que virá acompanhando, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-segmentado-combustivel-visa.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            345 => 
            array (
                'cod_produto' => 3635,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Cartão para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 500,
                'valor_reais' => '25.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-uber.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            346 => 
            array (
                'cod_produto' => 3636,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Cartão para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-uber.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            347 => 
            array (
                'cod_produto' => 3637,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Cartão para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-transporte-uber.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            348 => 
            array (
                'cod_produto' => 3638,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Voucher digital para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce. 
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-transporte-uber.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            349 => 
            array (
                'cod_produto' => 3639,
                'nome_produto' => 'CVC',
                'descricao_produto' => 'Cartão para aquisição de produtos CVC, englobando viagens e roteiros nacionais e internacionais, hospedagem, locação de automóveis e outros.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e televendas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços, agentes credenciados e mais informações acesse: www.cvc.com.br ',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-cvc.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            350 => 
            array (
                'cod_produto' => 3640,
                'nome_produto' => 'CVC',
                'descricao_produto' => 'Cartão para aquisição de produtos CVC, englobando viagens e roteiros nacionais e internacionais, hospedagem, locação de automóveis e outros.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e televendas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços, agentes credenciados e mais informações acesse: www.cvc.com.br ',
                'valor_produto' => 30000,
                'valor_reais' => '1500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-cvc.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            351 => 
            array (
                'cod_produto' => 3641,
                'nome_produto' => 'CVC',
                'descricao_produto' => 'Cartão para aquisição de produtos CVC, englobando viagens e roteiros nacionais e internacionais, hospedagem, locação de automóveis e outros.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e televendas.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de até 3 Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços, agentes credenciados e mais informações acesse: www.cvc.com.br ',
                'valor_produto' => 60000,
                'valor_reais' => '3000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-cvc.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            352 => 
            array (
                'cod_produto' => 3642,
                'nome_produto' => 'FLOT  TRAVEL CARD',
                'descricao_produto' => 'Cartão para aquisição de pacotes turísticos nacionais e internacionais da FLOT TRAVEL.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerc e televendas.
Poderá ser utilizado várias vezes até zerar o saldo.
Exclusivo para pacotes completos. Não é permitida a compra de passagens aéreas, pagamento de hospedagem, seguro de saúde, taxas e outros. ',
            'informacoes_importantes' => 'Central Flot Travel Card: (11) 4504-4544
Compras e mais informações através do site: www.flot.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-flot-travel.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            353 => 
            array (
                'cod_produto' => 3643,
                'nome_produto' => 'FLOT  TRAVEL CARD',
                'descricao_produto' => 'Cartão para aquisição de pacotes turísticos nacionais e internacionais da FLOT TRAVEL.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerc e televendas.
Poderá ser utilizado várias vezes até zerar o saldo.
Exclusivo para pacotes completos. Não é permitida a compra de passagens aéreas, pagamento de hospedagem, seguro de saúde, taxas e outros. ',
            'informacoes_importantes' => 'Central Flot Travel Card: (11) 4504-4544
Compras e mais informações através do site: www.flot.com.br',
                'valor_produto' => 20000,
                'valor_reais' => '1000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-flot-travel.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            354 => 
            array (
                'cod_produto' => 3644,
                'nome_produto' => 'FLOT  TRAVEL CARD',
                'descricao_produto' => 'Cartão para aquisição de pacotes turísticos nacionais e internacionais da FLOT TRAVEL.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerc e televendas.
Poderá ser utilizado várias vezes até zerar o saldo.
Exclusivo para pacotes completos. Não é permitida a compra de passagens aéreas, pagamento de hospedagem, seguro de saúde, taxas e outros. ',
            'informacoes_importantes' => 'Central Flot Travel Card: (11) 4504-4544
Compras e mais informações através do site: www.flot.com.br',
                'valor_produto' => 40000,
                'valor_reais' => '2000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-flot-travel.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            355 => 
            array (
                'cod_produto' => 3645,
                'nome_produto' => 'FLOT  TRAVEL CARD',
                'descricao_produto' => 'Cartão para aquisição de pacotes turísticos nacionais e internacionais da FLOT TRAVEL.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerc e televendas.
Poderá ser utilizado várias vezes até zerar o saldo.
Exclusivo para pacotes completos. Não é permitida a compra de passagens aéreas, pagamento de hospedagem, seguro de saúde, taxas e outros. ',
            'informacoes_importantes' => 'Central Flot Travel Card: (11) 4504-4544
Compras e mais informações através do site: www.flot.com.br',
                'valor_produto' => 60000,
                'valor_reais' => '3000.00',
                'fotos' => NULL,
                'foto_exemplo' => 'yetzcards-cartao-viagem-flottravelcard.jpg',
                'cod_categoria_produto' => 14,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 3,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            356 => 
            array (
                'cod_produto' => 3646,
                'nome_produto' => 'EXTRA DELIVERY',
                'descricao_produto' => 'Voucher digital Multicash para aquisição de produtos através de delivery Pão de Açúcar ou Delivery Extra e receber no endereço indicado. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Consulte as localidades de entrega.
Para compras e mais informações acesse: www.paodeacucar.com e deliveryextra.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '0d37f09c99ae4f7dc006a1e3793f09dc.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            357 => 
            array (
                'cod_produto' => 3647,
                'nome_produto' => 'PÃO DE AÇÚCAR DELIVERY',
                'descricao_produto' => 'Voucher digital Multicash para aquisição de produtos através de delivery Pão de Açúcar ou Delivery Extra e receber no endereço indicado. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
É necessário informar o código de segurança de 3 (três) dígitos. 
Permitido apenas um voucher por compra, sendo que o saldo do cartão deverá ser igual ou inferior ao valor total, uma vez que não é permitida outras formas de pagamento no site.',
                'informacoes_importantes' => 'Consulte as localidades de entrega.
Para compras e mais informações acesse: www.paodeacucar.com e deliveryextra.com.br.',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '5412efff0984fa780d1d443cfa2483e1.jpg',
                'cod_categoria_produto' => 2,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:09',
                'updated_at' => '2019-06-05 20:05:09',
                'deleted_at' => NULL,
            ),
            358 => 
            array (
                'cod_produto' => 3648,
                'nome_produto' => 'LEAGUE OF LEGENDS',
                'descricao_produto' => 'Cartão para compras de conteúdos exclusivos no jogo LEAGUE OF LEGENDS.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.      
O PIN desse cartão só pode ser utilizado uma vez.
Permitido cadastrar diversos cartões por conta, limitado a 5 cartões por dia.',
            'informacoes_importantes' => 'Este cartão é válido para compra de RP\'s (Riot Points) para jogadores de League of Legends.
O valor será creditado a uma conta, por isso precisa ter cadastro.
Para mais informações acesse: play.br.leagueoflegends.com/pt_BR',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '1c9611a8c9ec0ad1ce4015c0740e6402.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            359 => 
            array (
                'cod_produto' => 3649,
                'nome_produto' => 'PBKIDS',
                'descricao_produto' => 'Cartão para aquisição de produtos da PBKIDS, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias PBKIDS.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.pbkids.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '1e7983f3f08f7951cf4a2072ba14ebaf.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            360 => 
            array (
                'cod_produto' => 3650,
                'nome_produto' => 'PBKIDS',
                'descricao_produto' => 'Cartão para aquisição de produtos da PBKIDS, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias PBKIDS.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.pbkids.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'df3200cae2dffecb7013232a887b5546.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '5 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            361 => 
            array (
                'cod_produto' => 3651,
                'nome_produto' => 'PBKIDS',
                'descricao_produto' => 'Cartão para aquisição de produtos da PBKIDS, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias PBKIDS.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.pbkids.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => '227b21b9c1a4aff4234065616c9d3512.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            362 => 
            array (
                'cod_produto' => 3652,
                'nome_produto' => 'ALÔ BEBÊ',
                'descricao_produto' => 'Cartão para aquisição de produtos da ALÔ BEBÊ, rede varejista de brinquedos, roupas e acessórios para bebês.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
O cartão deve ser apresentado no ato do pagamento.
Pode ser usado parcialmente até zerar o saldo.
Permite complementar com outras formas de pagamento. 
É possível utilizar quantos cartões quiser como forma de pagamento.',
                'informacoes_importantes' => 'site www.alobebe.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '41abd1f476973f09f4e39ca3a08fe032.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            363 => 
            array (
                'cod_produto' => 3653,
                'nome_produto' => 'ALÔ BEBÊ',
                'descricao_produto' => 'Cartão para aquisição de produtos da ALÔ BEBÊ, rede varejista de brinquedos, roupas e acessórios para bebês.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
O cartão deve ser apresentado no ato do pagamento.
Pode ser usado parcialmente até zerar o saldo.
Permite complementar com outras formas de pagamento. 
É possível utilizar quantos cartões quiser como forma de pagamento.',
                'informacoes_importantes' => 'site www.alobebe.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => '1f38765c3adddc609670f8132c45aa5c.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            364 => 
            array (
                'cod_produto' => 3654,
                'nome_produto' => 'ALÔ BEBÊ',
                'descricao_produto' => 'Cartão para aquisição de produtos da ALÔ BEBÊ, rede varejista de brinquedos, roupas e acessórios para bebês.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
O cartão deve ser apresentado no ato do pagamento.
Pode ser usado parcialmente até zerar o saldo.
Permite complementar com outras formas de pagamento. 
É possível utilizar quantos cartões quiser como forma de pagamento.',
                'informacoes_importantes' => 'site www.alobebe.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'c1a91db6bec8d5fbe3db50db4f5c5dfc.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            365 => 
            array (
                'cod_produto' => 3655,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Cartão para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'fde929da82189fb60e86de6582347605.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            366 => 
            array (
                'cod_produto' => 3656,
                'nome_produto' => 'RI HAPPY',
                'descricao_produto' => 'Voucher digital para aquisição de produtos da RI HAPPY, rede varejista de brinquedos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Lojas físicas: pode ser utilizado 2 vezes até zerar o saldo.
E-commerce: não permite saldo remanescente.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Não é aceito nas franquias Ri Happy.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              Para endereços e mais informações acesse: www.rihappy.com.br',
                'valor_produto' => 1500,
                'valor_reais' => '75.00',
                'fotos' => NULL,
                'foto_exemplo' => '02f5a5eddd9f8ea91eca0b34d13328aa.jpg',
                'cod_categoria_produto' => 13,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            367 => 
            array (
                'cod_produto' => 3657,
                'nome_produto' => 'PRESENTE  VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '25158010a513e97ad50caa9cb5291fca.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            368 => 
            array (
                'cod_produto' => 3658,
                'nome_produto' => 'PRESENTE  VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => '32abf50f6a29ecb08ff87ddd2923f33b.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '7 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            369 => 
            array (
                'cod_produto' => 3659,
                'nome_produto' => 'PRESENTE  VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce que aceitem a bandeira Visa.
Pode ser utilizado várias vezes até zerar o saldo.
Siga impreterivelmente todas as instruções que receber juntamente com o cartão, elas são fundamentais para sua compra ser bem sucedida.
É necessário o fornecimento do CPF no ato cadastro e desbloqueio do cartão.
Para usar no e-commerce o site deve ter o selo da Visa Electron.
ATENÇÃO: Este cartão deverá ser sempre utilizado na função crédito.',
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'cb34592b655814eb81e96ef48b343874.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '8 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            370 => 
            array (
                'cod_produto' => 3662,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '83bbbe32c2564bd7b68edd329f6bf1f4.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            371 => 
            array (
                'cod_produto' => 3663,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '94b6494cb2231394ed46df8b0cb6be80.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            372 => 
            array (
                'cod_produto' => 3664,
                'nome_produto' => 'C&C',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&C, especializada em materiais de construção, reforma e decoração.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para mais informações acesse: www.cec.com.br',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => '8f90b89f6bc9354524e5bcb0b12714b0.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            373 => 
            array (
                'cod_produto' => 3665,
                'nome_produto' => 'RAKUTEN',
                'descricao_produto' => 'Voucher digital  para aquisição de produtos na RAKUTEN, Marketplace com mais de 30 diferentes categorias de produtos. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.rakuten.com.br                                                                                                                                                                                                                                                                                                                                                                                 ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '629796e0ed3651a4e7f211c9d33c4903.jpg',
                'cod_categoria_produto' => 3,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            374 => 
            array (
                'cod_produto' => 3666,
                'nome_produto' => 'CASAS BAHIA.COM',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto nas CASAS BAHIA.COM.BR
Loja de departamentos que oferece produtos variados. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Permitida a utilização de apenas 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.casasbahia.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '0ed3074cea47d45fa02055ef4ca2625d.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            375 => 
            array (
                'cod_produto' => 3667,
                'nome_produto' => 'IPLACE',
                'descricao_produto' => 'Cartão para aquisição de produtos nas lojas IPLACE, especializada em produtos da Apple.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo.
Pode complementar com outras formas de pagamento disponíveis na loja.
Poderá ser utilizado um (1) ou mais cartões presente por compra. ',
                'informacoes_importantes' => 'Não permite consulta de saldo.
Para mais informações acesse: www.lojaiplace.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '13cecda9a02341adb7e13786daff8bd0.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '15 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            376 => 
            array (
                'cod_produto' => 3668,
                'nome_produto' => 'IPLACE',
                'descricao_produto' => 'Cartão para aquisição de produtos nas lojas IPLACE, especializada em produtos da Apple.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo.
Pode complementar com outras formas de pagamento disponíveis na loja.
Poderá ser utilizado um (1) ou mais cartões presente por compra. ',
                'informacoes_importantes' => 'Não permite consulta de saldo.
Para mais informações acesse: www.lojaiplace.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'b7cf7f9e11ccd52340e63d2a96dc6c1f.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '15 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            377 => 
            array (
                'cod_produto' => 3669,
                'nome_produto' => 'IPLACE',
                'descricao_produto' => 'Cartão para aquisição de produtos nas lojas IPLACE, especializada em produtos da Apple.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas
Pode ser utilizado várias vezes até zerar o saldo.
Pode complementar com outras formas de pagamento disponíveis na loja.
Poderá ser utilizado um (1) ou mais cartões presente por compra. ',
                'informacoes_importantes' => 'Não permite consulta de saldo.
Para mais informações acesse: www.lojaiplace.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => 'd6231bbae89d7c17630b53c3896d8717.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '15 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:10',
                'updated_at' => '2019-06-05 20:05:10',
                'deleted_at' => NULL,
            ),
            378 => 
            array (
                'cod_produto' => 3670,
                'nome_produto' => 'SHOPTIME',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na SHOPTIME.
Varejo de eletrodomésticos, eletroportáteis e utensílios de cozinha, cama, mesa, banho, dentre outros.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de até 3 (três) Cartões Presente por compra realizada.',
                'informacoes_importantes' => 'Para compras e mais informações acesse: www.www.shoptime.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '98fe103df116966d0546f597819f1c51.jpg',
                'cod_categoria_produto' => 5,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            379 => 
            array (
                'cod_produto' => 3671,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '77c61dfd8424edd14aa309d87c0cffe2.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            380 => 
            array (
                'cod_produto' => 3672,
                'nome_produto' => 'CENTAURO',
                'descricao_produto' => 'Cartão para aquisição de qualquer produto nas lojas CENTAURO, rede de produtos esportivos.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.    
No e-commerce é permitido apenas 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.centauro.com.br ',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '7f180f26e4e9f6de92614e6318e441c3.jpg',
                'cod_categoria_produto' => 6,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            381 => 
            array (
                'cod_produto' => 3673,
                'nome_produto' => 'NETFLIX',
                'descricao_produto' => 'Cartão para uso na Netflix, provedora de filmes e séries de televisão via streaming.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'O crédito do cartão será adicionado integralmente na conta do usuário.
Serão descontados os valores referentes até o término do crédito, conforme o plano de assinatura escolhido.
Sem limite de quantidade de cartões utilizados. ',
                'informacoes_importantes' => 'Aceito somente para a conta Netflix Brasil. 
Para compras e mais informações acesse: www.netflix.com',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => '2cdae2554c23a56aad1df3421b200343.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            382 => 
            array (
                'cod_produto' => 3674,
                'nome_produto' => 'NETFLIX',
                'descricao_produto' => 'Cartão para uso na Netflix, provedora de filmes e séries de televisão via streaming.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'O crédito do cartão será adicionado integralmente na conta do usuário.
Serão descontados os valores referentes até o término do crédito, conforme o plano de assinatura escolhido.
Sem limite de quantidade de cartões utilizados. ',
                'informacoes_importantes' => 'Aceito somente para a conta Netflix Brasil. 
Para compras e mais informações acesse: www.netflix.com',
                'valor_produto' => 1400,
                'valor_reais' => '70.00',
                'fotos' => NULL,
                'foto_exemplo' => '4f481127ef45e53c9c67a8c9dc14d085.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            383 => 
            array (
                'cod_produto' => 3675,
                'nome_produto' => 'NETFLIX',
                'descricao_produto' => 'Cartão para uso na Netflix, provedora de filmes e séries de televisão via streaming.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'O crédito do cartão será adicionado integralmente na conta do usuário.
Serão descontados os valores referentes até o término do crédito, conforme o plano de assinatura escolhido.
Sem limite de quantidade de cartões utilizados. ',
                'informacoes_importantes' => 'Aceito somente para a conta Netflix Brasil. 
Para compras e mais informações acesse: www.netflix.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => '63bdfd6dfe38e9fca2ab5aff32724a74.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            384 => 
            array (
                'cod_produto' => 3676,
                'nome_produto' => 'NETFLIX',
                'descricao_produto' => 'Cartão para uso na Netflix, provedora de filmes e séries de televisão via streaming.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'O crédito do cartão será adicionado integralmente na conta do usuário.
Serão descontados os valores referentes até o término do crédito, conforme o plano de assinatura escolhido.
Sem limite de quantidade de cartões utilizados. ',
                'informacoes_importantes' => 'Aceito somente para a conta Netflix Brasil. 
Para compras e mais informações acesse: www.netflix.com',
                'valor_produto' => 800,
                'valor_reais' => '40.00',
                'fotos' => NULL,
                'foto_exemplo' => 'eb810f28529ee76544e17bb1c92ca862.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            385 => 
            array (
                'cod_produto' => 3677,
                'nome_produto' => 'NETFLIX',
                'descricao_produto' => 'Cartão para uso na Netflix, provedora de filmes e séries de televisão via streaming.
',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'O crédito do cartão será adicionado integralmente na conta do usuário.
Serão descontados os valores referentes até o término do crédito, conforme o plano de assinatura escolhido.
Sem limite de quantidade de cartões utilizados. ',
                'informacoes_importantes' => 'Aceito somente para a conta Netflix Brasil. 
Para compras e mais informações acesse: www.netflix.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'a8298551b4082f3b9cd9ea9207e78f7a.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            386 => 
            array (
                'cod_produto' => 3678,
                'nome_produto' => 'INGRESSO.COM',
                'descricao_produto' => 'Voucher digital para aquisição de ingressos em cinemas como Cinemark, Cinépolis, Kiniplex, UCI, dentre outros. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de um vale por compra. 
Válido para qualquer dia da semana e horário, e também para salas 3D, Vip e especiais. ',
                'informacoes_importantes' => 'O vale não poderá ser cancelado, trocado ou devolvido. 
Para compras e mais informações acesse: www.ingresso.com',
                'valor_produto' => 400,
                'valor_reais' => '20.00',
                'fotos' => NULL,
                'foto_exemplo' => '90661384c88a7987a9954e3f175ede61.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            387 => 
            array (
                'cod_produto' => 3679,
                'nome_produto' => 'INGRESSO.COM',
                'descricao_produto' => 'Voucher digital para aquisição de ingressos em cinemas como Cinemark, Cinépolis, Kiniplex, UCI, dentre outros. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.                                                                                                                                                                                                                                                                                                                                                                 
Não permite saldo remanescente.
Permitida a utilização de um vale por compra. 
Válido para qualquer dia da semana e horário, e também para salas 3D, Vip e especiais. ',
                'informacoes_importantes' => 'O vale não poderá ser cancelado, trocado ou devolvido. 
Para compras e mais informações acesse: www.ingresso.com',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '320c074fbf82c0f3be6e0924d52bdeb9.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            388 => 
            array (
                'cod_produto' => 3680,
                'nome_produto' => 'O MELHOR DA VIDA DVERSÃO',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ',
                'informacoes_importantes' => 'Experiências disponíveis: Passeios, Parques, Ingressos, Cursos e Terceira idade.
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => '60478d30e728a4a1465402184b8ff89d.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            389 => 
            array (
                'cod_produto' => 3681,
                'nome_produto' => 'KINOPLEX 1 PAR COMBO',
                'descricao_produto' => 'Cartão para aquisição de um par de ingressos na rede de cinemas KINOPLEX.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente. 
Válido para qualquer dia da semana e horário da rede Severiano Ribeiro/Kinoplex, exceto salas 3D, Platinum, VIP e em parcerias com a UCI.',
                'informacoes_importantes' => 'Para informações de salas e programação acesse:  www.kinoplex.com.br',
                'valor_produto' => 1240,
                'valor_reais' => '62.00',
                'fotos' => NULL,
                'foto_exemplo' => '1fa845703ee9ff4428f4e267257443e9.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            390 => 
            array (
                'cod_produto' => 3682,
                'nome_produto' => 'O MELHOR DA VIDA ESPORTE',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Experiências disponíveis: Academias, No Ar, Na Terra, Hotéis com Esporte
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '16d268f04a3af111871f4fa32746558c.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            391 => 
            array (
                'cod_produto' => 3683,
                'nome_produto' => 'O MELHOR DA VIDA GOURMET',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Experiências disponíveis: Degustação, Jantar Romântico, em Família ou em Grupo, Jantar Temático, Bares.
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => 'f1cc25979d8e91cd42bc4aa0649fb546.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            392 => 
            array (
                'cod_produto' => 3684,
                'nome_produto' => 'O MELHOR DA VIDA VIP',
                'descricao_produto' => 'Voucher digital da empresa Cartão da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Experiências disponíveis: Hotéis, Tratamentos, Jantares Especiais, Passeios de Luxo
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => '5e45cad50100d0e38a09aae139ca819f.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            393 => 
            array (
                'cod_produto' => 3685,
                'nome_produto' => 'O MELHOR DA VIDA ZEN',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Experiências disponíveis: Spa, Hotel Spa, Terapias, Medicina Alternativa, Estética.
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => '4ee507456d373bdf6f55f23ab16e0c3f.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            394 => 
            array (
                'cod_produto' => 3686,
                'nome_produto' => 'O MELHOR DA VIDA TRIP',
                'descricao_produto' => 'Voucher digital da empresa O MELHOR DA VIDA, especializada em oferecer experiências esportivas, culturais e gastronômicas.  ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido somente com o código apresentado no cartão. 
Sem a apresentação do Voucher original, o parceiro não poderá aceitar o cliente.      
Os vouchers não são cumulativos.                                                                                                                                                                                                                                                                                                                                                                                                         ',
                'informacoes_importantes' => 'Experiências disponíveis: Hotéis, Pousadas, Aventuras, Nacional, Internacional.
Para compras e mais informações acesse www.omelhordavida.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => '0a1601b77a5982434b50c300c8bdd15e.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            395 => 
            array (
                'cod_produto' => 3687,
                'nome_produto' => 'SPOTIFY MENSAL',
                'descricao_produto' => 'Cartão para uso no SPOTIFY, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'É possível somente um Cartão Presente  por resgate. 
O valor de R$ 17,00 é referente a um mês de mensalidade Spotify Premium e será vinculado integralmente após o resgate.  ',
                'informacoes_importantes' => 'Cartão válido somente para a conta Premium, não sendo possível para planos familiares ou promoções. 
USO EXCLUSIVO no site. Não é possível utilizar o Cartão Presente no aplicativo.
Para compras e mais informações acesse www.spotify.com/redeem',
                'valor_produto' => 340,
                'valor_reais' => '17.00',
                'fotos' => NULL,
                'foto_exemplo' => 'fcde6c84c27245b3f65bd1fc73e27a55.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            396 => 
            array (
                'cod_produto' => 3688,
                'nome_produto' => 'SPOTIFY TRIMESTRAL',
                'descricao_produto' => 'Cartão para uso no SPOTIFY, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'É possível somente um Cartão Presente  por resgate. 
O valor de R$ 50,00 é referente a três meses de mensalidade Spotify Premium e será vinculado integralmente após o resgate.  ',
                'informacoes_importantes' => 'Cartão válido somente para a conta Premium, não sendo possível para planos familiares ou promoções. 
USO EXCLUSIVO no site. Não é possível utilizar o Cartão Presente no aplicativo.
Para compras e mais informações acesse www.spotify.com/redeem',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '810667b7a9e8f2c5239b28453aba7dc5.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            397 => 
            array (
                'cod_produto' => 3689,
                'nome_produto' => 'SPOTIFY SEMESTRAL',
                'descricao_produto' => 'Cartão para uso no SPOTIFY, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'É possível somente um Cartão Presente  por resgate. 
O valor de R$ 100,00 é referente a seis meses de mensalidade Spotify Premium e será vinculado integralmente após o resgate.  ',
                'informacoes_importantes' => 'Cartão válido somente para a conta Premium, não sendo possível para planos familiares ou promoções. 
USO EXCLUSIVO no site. Não é possível utilizar o Cartão Presente no aplicativo.
Para compras e mais informações acesse www.spotify.com/redeem',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'cb219c4162349b85278059796dfe167b.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            398 => 
            array (
                'cod_produto' => 3690,
                'nome_produto' => 'DEEZER',
                'descricao_produto' => 'Cartão para uso no DEEZER, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Não é possível utilizar o Cartão Presente Deezer Virtual no aplicativo. 
O Cartão Presente Deezer Virtual é válido somente para a conta Premium, não sendo possível para planos HiFi, Anual, Familiar, Estudantil ou promoções.
Cartão ao portador, não recarregável, intransferível e não cumulativo.
Para compras e mais informações acesse www.deezer.com',
                'valor_produto' => 338,
                'valor_reais' => '17.00',
                'fotos' => NULL,
                'foto_exemplo' => 'fb0a55c3c65163ac76d38066ea1d5019.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:11',
                'updated_at' => '2019-06-05 20:05:11',
                'deleted_at' => NULL,
            ),
            399 => 
            array (
                'cod_produto' => 3691,
                'nome_produto' => 'DEEZER',
                'descricao_produto' => 'Cartão para uso no DEEZER, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito EXCLUSIVAMENTE em e-commerce.   
O valor de R$ 45,00 é referente a 3 meses de mensalidade Deezer e será vinculado integralmente após o resgate.
É possível somente um Cartão Presente Deezer Virtual por resgate.   
O Cartão Presente Deezer Virtual será encaminhado para o e-mail cadastrado em até 7 dias úteis após a confirmação do resgate.         ',
                'informacoes_importantes' => 'Não é possível utilizar o Cartão Presente Deezer Virtual no aplicativo. 
O Cartão Presente Deezer Virtual é válido somente para a conta Premium, não sendo possível para planos HiFi, Anual, Familiar, Estudantil ou promoções.
Cartão ao portador, não recarregável, intransferível e não cumulativo.
Para compras e mais informações acesse www.deezer.com',
                'valor_produto' => 900,
                'valor_reais' => '45.00',
                'fotos' => NULL,
                'foto_exemplo' => '88a6d0b6582e84baf1f9252c254be20a.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            400 => 
            array (
                'cod_produto' => 3692,
                'nome_produto' => 'DEEZER',
                'descricao_produto' => 'Cartão para uso no DEEZER, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito EXCLUSIVAMENTE em e-commerce.   
O valor de R$ 90,00 é referente a 6 meses de mensalidade Deezer e será vinculado integralmente após o resgate.
É possível somente um Cartão Presente Deezer Virtual por resgate.   
O Cartão Presente Deezer Virtual será encaminhado para o e-mail cadastrado em até 7 dias úteis após a confirmação do resgate.         ',
                'informacoes_importantes' => 'Não é possível utilizar o Cartão Presente Deezer Virtual no aplicativo. 
O Cartão Presente Deezer Virtual é válido somente para a conta Premium, não sendo possível para planos HiFi, Anual, Familiar, Estudantil ou promoções.
Cartão ao portador, não recarregável, intransferível e não cumulativo.
Para compras e mais informações acesse www.deezer.com',
                'valor_produto' => 1800,
                'valor_reais' => '90.00',
                'fotos' => NULL,
                'foto_exemplo' => '3ee8b2a629e7443a12bbcaafee1a173a.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            401 => 
            array (
                'cod_produto' => 3693,
                'nome_produto' => 'GOOGLE PLAY',
            'descricao_produto' => 'Cartão para uso nas lojas Android (GOOGLE PLAY). ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido EXCLUSIVAMENTE pelo serviço nas lojas do Brasil. 
Cartão não-recarregável ou reembolsável. 
O valor do cartão será creditado no ato do resgate do cartão.',
                'informacoes_importantes' => NULL,
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => '5a2f053d33372db8b7b9c84a44682f39.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            402 => 
            array (
                'cod_produto' => 3694,
                'nome_produto' => 'GOOGLE PLAY',
            'descricao_produto' => 'Cartão para uso nas lojas Android (GOOGLE PLAY). ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido EXCLUSIVAMENTE pelo serviço nas lojas do Brasil. 
Cartão não-recarregável ou reembolsável. 
O valor do cartão será creditado no ato do resgate do cartão.',
                'informacoes_importantes' => NULL,
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'e05aebe18324b6dd82d726ef958147d5.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            403 => 
            array (
                'cod_produto' => 3695,
                'nome_produto' => 'GOOGLE PLAY',
            'descricao_produto' => 'Cartão para uso nas lojas Android (GOOGLE PLAY). ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Válido EXCLUSIVAMENTE pelo serviço nas lojas do Brasil. 
Cartão não-recarregável ou reembolsável. 
O valor do cartão será creditado no ato do resgate do cartão.',
                'informacoes_importantes' => NULL,
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'd7e0bdef959846b3eb450e7818a1262c.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            404 => 
            array (
                'cod_produto' => 3696,
                'nome_produto' => 'UCI 1 PAR',
            'descricao_produto' => 'Cartão para aquisição de um par de ingressos e combo (pipoca com refrigerante) na rede de cinemas UCI.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário,  para todos os filmes e sessões, exceto nos cinemas em Salvador/BA e salas 3D, IMAX, de Lux e 4D.     ',
                'informacoes_importantes' => 'Você receberá o ingresso para acesso ao cinema e pipoca pequena com refrigerante 500ml.
Para informações de salas e programação acesse: www.ucicinemas.com.br',
                'valor_produto' => 740,
                'valor_reais' => '37.00',
                'fotos' => NULL,
                'foto_exemplo' => 'ca6630f4811618208ba98cd3eb344686.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '45 dias',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            405 => 
            array (
                'cod_produto' => 3697,
                'nome_produto' => 'SHOESTOCK',
                'descricao_produto' => 'Voucher digital para aquisição de qualquer produto na SHOESTOCK, loja de sapatos femininos, masculinos e infantis, além de bolsas e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Cartão Presente por compra realizada.',
                'informacoes_importantes' => 'Para endereço e mais informações acesse: shoestock.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => 'baa09f440e939d48b31342147e6925ad.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            406 => 
            array (
                'cod_produto' => 3698,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => '76f6d902f15ba4ad657f9ba64ffcc129.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            407 => 
            array (
                'cod_produto' => 3699,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => 'ace6a070bc6efc162646a51e175f3580.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            408 => 
            array (
                'cod_produto' => 3700,
                'nome_produto' => 'TIP TOP',
                'descricao_produto' => 'Voucher digital para aquisição de produtos na TIP TOP, loja especializada em moda infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não há limite de vales por compra.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.tiptop.com.br',
                'valor_produto' => 10000,
                'valor_reais' => '500.00',
                'fotos' => NULL,
                'foto_exemplo' => '9cfc7af79af7b753980746e00fda238d.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            409 => 
            array (
                'cod_produto' => 3701,
                'nome_produto' => 'C&A',
                'descricao_produto' => 'Cartão para aquisição de produtos na C&A, loja especializada em moda feminina, masculina e infantil. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em lojas físicas e e-commerce.
Pode ser utilizado várias vezes até zerar o saldo.
Quando utilizado em conjunto com outras formas de pagamento, o cartão deverá ter todo o seu saldo consumido.',
                'informacoes_importantes' => 'Não é válido para pagamento de fatura, carnê, compras parceladas, produtos financeiros ou troca por dinheiro.                                                                                                                                                                                                                                                                                                                                                                                               O cartão não pode ser utilizado na compra de outro cartão presente.                                                                                                                                                                                                                                                                                                                                                                               
Para endereços e mais informações acesse:  www.cea.com.br',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'a474827194a70bc1f5adde84e7b3698e.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            410 => 
            array (
                'cod_produto' => 3702,
                'nome_produto' => 'HAVAIANAS',
                'descricao_produto' => 'Cartão para aquisição de produtos na HAVAIANAS, loja de calçados e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havaianas.com.br',
                'valor_produto' => 1998,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '1b4bd87349d1dcc021c4aad0fea16d7a.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            411 => 
            array (
                'cod_produto' => 3703,
                'nome_produto' => 'SHOULDER',
                'descricao_produto' => 'Cartão para utilização na SHOULDER, loja de roupas e acessórios femininos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em ALGUMAS lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Não permite saldo remanescente.        
É necessário o cadastro em loja.  ',
                'informacoes_importantes' => 'No site consta lista das lojas que aceitam o cartão.                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.shoulder.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '0d6d6b75dc362ca1d56600e673626407.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            412 => 
            array (
                'cod_produto' => 3704,
                'nome_produto' => 'SHOULDER',
                'descricao_produto' => 'Cartão para utilização na SHOULDER, loja de roupas e acessórios femininos.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito em ALGUMAS lojas físicas.                                                                                                                                                                                                                                                                                                                                                                             
Não permite saldo remanescente.        
É necessário o cadastro em loja.  ',
                'informacoes_importantes' => 'No site consta lista das lojas que aceitam o cartão.                                                                                                                                                                                                                                                                                                    
Para endereços e mais informações acesse: www.shoulder.com.br',
                'valor_produto' => 5000,
                'valor_reais' => '250.00',
                'fotos' => NULL,
                'foto_exemplo' => '9ba4aca636c224f6f33facd9e522a230.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            413 => 
            array (
                'cod_produto' => 3705,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Cartão para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito em loja física.
Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '9341e03f1cb0c2814f17e619f27e5851.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            414 => 
            array (
                'cod_produto' => 3706,
                'nome_produto' => 'PETZ',
                'descricao_produto' => 'Voucher digital para uso nas lojas PETZ, especializada em serviços de estética, veterinária e produtos diversos para: cães, gatos, peixes, aves, roedores, répteis, além de linha de piscina e jardinagem.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
Permitida a utilização de 1 (um) Voucher Digital por compra realizada.',
                'informacoes_importantes' => 'ATENÇÃO: O Cartão NÃO É aceito em loja física.
Para mais informações acesse: www.petz.com.br',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '28abcb8c6417eedc9f1e8bd9c32a4981.jpg',
                'cod_categoria_produto' => 15,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            415 => 
            array (
                'cod_produto' => 3707,
                'nome_produto' => 'MADERO',
                'descricao_produto' => 'Cartão para uso nos restaurantes MADERO.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não é aceito para delivery.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Aceito no Madero e Madero Prime.
Para endereços e mais informações acesse: www.restaurantemadero.com.br',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '0f02f4da583097167fed1efdf784f8be.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            416 => 
            array (
                'cod_produto' => 3708,
                'nome_produto' => 'OUTBACK',
                'descricao_produto' => 'Voucher digital para uso nos restaurantes OUTBACK STEAKHOUSE. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Não permite saldo remanescente.',
                'informacoes_importantes' => 'Não pode ser utilizado para compra de bebidas alcoólicas.
Para endereços e mais informações acesse: www.outback.com.br',
                'valor_produto' => 2400,
                'valor_reais' => '120.00',
                'fotos' => NULL,
                'foto_exemplo' => '76bef1ffb53fb04c43bac953f99f591b.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '4 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:12',
                'updated_at' => '2019-06-05 20:05:12',
                'deleted_at' => NULL,
            ),
            417 => 
            array (
                'cod_produto' => 3709,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Voucher digital para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'db66b20e1dbd9872d5310a6bc8039a0f.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            418 => 
            array (
                'cod_produto' => 3710,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 1800,
                'valor_reais' => '90.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dda071071975e34c86248658d97f77f8.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            419 => 
            array (
                'cod_produto' => 3711,
                'nome_produto' => 'PIZZA HUT',
                'descricao_produto' => 'Cartão para utilização nas unidades PIZZA HUT.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Pode ser utilizado várias vezes até zerar o saldo.',
                'informacoes_importantes' => 'Utilização EXCLUSIVA na cidade de São Paulo.
Para endereços e mais informações acesse: ecommerce-pizza-hut.herokuapp.com',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => '27b0a3b50acf69e33f945005979ed7e7.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            420 => 
            array (
                'cod_produto' => 3712,
                'nome_produto' => 'L\'OCCITANE AU BRÉSIL',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => 'fc9cda9fb3b132b977c536510aa5b6b9.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            421 => 
            array (
                'cod_produto' => 3713,
                'nome_produto' => 'L\'OCCITANE AU BRÉSIL',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '0a34f427e70b7df92a522ed723c23983.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            422 => 
            array (
                'cod_produto' => 3714,
                'nome_produto' => 'L\'OCCITANE AU BRÉSIL',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => 'a11639db9fa7a7bd430debd4df8c1a22.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            423 => 
            array (
                'cod_produto' => 3715,
                'nome_produto' => 'L\'OCCITANE EN PROVENCE',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '0110bb0e3e23d0e1e93f0dc484338898.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            424 => 
            array (
                'cod_produto' => 3716,
                'nome_produto' => 'L\'OCCITANE EN PROVENCE',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 4000,
                'valor_reais' => '200.00',
                'fotos' => NULL,
                'foto_exemplo' => 'f8d71b5bf4ac37470067c039ddaf0ffb.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            425 => 
            array (
                'cod_produto' => 3717,
                'nome_produto' => 'L\'OCCITANE EN PROVENCE',
                'descricao_produto' => 'Voucher Digital da L\'OCCITANE AU BRÉSIL, empresa de cosméticos e produtos Corporais, Faciais, Banho e Cabelo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Não permite saldo remanescente.
O cartão deverá ser utilizado para compras com valor superior ao crédito disponível , a partir de R$ 1,00.
Não há limite de vales por compra. 
Aceita pagamentos complementares com as formas disponíveis no site.',
                'informacoes_importantes' => 'Compras e mais informações através do site https://br.loccitane.com',
                'valor_produto' => 6000,
                'valor_reais' => '300.00',
                'fotos' => NULL,
                'foto_exemplo' => '5cb547f792fb2f93b93dc79d9bbf0590.jpg',
                'cod_categoria_produto' => 11,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            426 => 
            array (
                'cod_produto' => 3718,
                'nome_produto' => 'GOODCARD COMBUSTÍVEL',
                'descricao_produto' => 'Cartão para ser utilizado nos postos de combustível que atendam a rede GOODCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em lojas físicas.
Pode ser utilizado várias vezes até zerar o saldo.
Permite complementar o pagamento com as formas disponíveis nos estabelecimentos.
Para ativar o Cartão Combustível Good Card - Presente Perfeito, entre em contato através do telefone 4003-9099 e vincule o seu cartão ao CPF. 
Não é possível vincular cartões a CNPJ.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.cartaopresenteperfeito.com.br.',
                'valor_produto' => 2000,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '51b926d385fc604c76977e1ddf5dbdf6.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            427 => 
            array (
                'cod_produto' => 3719,
                'nome_produto' => 'GOODCARD COMBUSTÍVEL',
                'descricao_produto' => 'Cartão para ser utilizado nos postos de combustível que atendam a rede GOODCARD.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Pode ser utilizado várias vezes até zerar o saldo.
Permite complementar o pagamento com as formas disponíveis nos estabelecimentos.
Para ativar o Cartão Combustível Good Card - Presente Perfeito, entre em contato através do telefone 4003-9099 e vincule o seu cartão ao CPF. 
Não é possível vincular cartões a CNPJ.',
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.cartaopresenteperfeito.com.br.',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => 'ffd4594965b77ab9ca14740bd932fa7f.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 0,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            428 => 
            array (
                'cod_produto' => 3720,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Cartão para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 500,
                'valor_reais' => '25.00',
                'fotos' => NULL,
                'foto_exemplo' => '6e593ffa71d9a159cd41362ed3bf690f.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            429 => 
            array (
                'cod_produto' => 3721,
                'nome_produto' => 'UBER EATS',
                'descricao_produto' => 'Voucher digital para aquisição e entrega de comidas de restaurantes através da UBER EATS. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Acesse com sua conta Uber Eats para fazer seu pedido e pague com sua conta Uber. 
Para utilizar o seu Uber Eats Pré - Pago Virtual, acesse seu aplicativo Uber e insira os créditos na área de Uber Pré-Pago.
O Uber Eats poderá ser utilizado no valor total ou ainda complementar o pagamento com as formas disponíveis no aplicativo.
É possível adicionar quantos vales quiser na carteira Uber com o limite de R$ 1.000,00.',
                'informacoes_importantes' => 'Uber Eats Pré - Pago Virtual é ao portador e não é recarregável.
Consulta de saldo e acesso através do Aplicativo Uber. ',
                'valor_produto' => 600,
                'valor_reais' => '30.00',
                'fotos' => NULL,
                'foto_exemplo' => '57627073b573116ca9c01ec6a248f7a4.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            430 => 
            array (
                'cod_produto' => 3722,
                'nome_produto' => 'UBER EATS',
                'descricao_produto' => 'Voucher digital para aquisição e entrega de comidas de restaurantes através da UBER EATS. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Acesse com sua conta Uber Eats para fazer seu pedido e pague com sua conta Uber. 
Para utilizar o seu Uber Eats Pré - Pago Virtual, acesse seu aplicativo Uber e insira os créditos na área de Uber Pré-Pago.
O Uber Eats poderá ser utilizado no valor total ou ainda complementar o pagamento com as formas disponíveis no aplicativo.
É possível adicionar quantos vales quiser na carteira Uber com o limite de R$ 1.000,00.',
                'informacoes_importantes' => 'Uber Eats Pré - Pago Virtual é ao portador e não é recarregável.
Consulta de saldo e acesso através do Aplicativo Uber. ',
                'valor_produto' => 1000,
                'valor_reais' => '50.00',
                'fotos' => NULL,
                'foto_exemplo' => '9b65d9d0c15f49bcf93e9e5cf6fee0df.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            431 => 
            array (
                'cod_produto' => 3723,
                'nome_produto' => 'UBER EATS',
                'descricao_produto' => 'Voucher digital para aquisição e entrega de comidas de restaurantes através da UBER EATS. ',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Acesse com sua conta Uber Eats para fazer seu pedido e pague com sua conta Uber. 
Para utilizar o seu Uber Eats Pré - Pago Virtual, acesse seu aplicativo Uber e insira os créditos na área de Uber Pré-Pago.
O Uber Eats poderá ser utilizado no valor total ou ainda complementar o pagamento com as formas disponíveis no aplicativo.
É possível adicionar quantos vales quiser na carteira Uber com o limite de R$ 1.000,00.',
                'informacoes_importantes' => 'Uber Eats Pré - Pago Virtual é ao portador e não é recarregável.
Consulta de saldo e acesso através do Aplicativo Uber. ',
                'valor_produto' => 3000,
                'valor_reais' => '150.00',
                'fotos' => NULL,
                'foto_exemplo' => '734584b88fd56898f1a01143e70c2619.jpg',
                'cod_categoria_produto' => 10,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            432 => 
            array (
                'cod_produto' => 3724,
                'nome_produto' => 'UBER',
                'descricao_produto' => 'Voucher digital para adicionar créditos na conta UBER e efetuar pagamento de corridas pelo aplicativo.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito APENAS em e-commerce.
Conforme sua utilização, serão descontados da conta os valores referentes até o término do crédito. 
Permitido apenas um cartão por assinatura.',
                'informacoes_importantes' => 'Válido para cadastrados e novos cadastrados.
Acesse o aplicativo Uber para mais informações.',
                'valor_produto' => 1600,
                'valor_reais' => '80.00',
                'fotos' => NULL,
                'foto_exemplo' => '5eb59dea71cee4ccd133a0ca7da97f6e.jpg',
                'cod_categoria_produto' => 4,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            433 => 
            array (
                'cod_produto' => 3725,
                'nome_produto' => 'PRESENTE  VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 20000,
                'valor_reais' => '1.00',
                'fotos' => NULL,
                'foto_exemplo' => '70b264c196c296e845ab3b85a66a0d82.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '9 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            434 => 
            array (
                'cod_produto' => 3726,
                'nome_produto' => 'PRESENTE  VISA',
                'descricao_produto' => 'Cartão para ser utilizado em todos os estabelecimentos que aceitam a bandeira VISA.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Para mais informações acesse: https://meu.brasilprepagos.com.br/bpp',
                'valor_produto' => 30000,
                'valor_reais' => '2.00',
                'fotos' => NULL,
                'foto_exemplo' => '69684a0e36bd8f7a7f6306a1526b12d8.jpg',
                'cod_categoria_produto' => 18,
                'video' => '-',
                'validade_produto' => '10 meses',
                'selo' => 2,
                'tipo' => 0,
                'disponibilidade' => 0,
                'created_at' => '2019-06-05 20:05:13',
                'updated_at' => '2019-06-05 20:05:13',
                'deleted_at' => NULL,
            ),
            435 => 
            array (
                'cod_produto' => 3727,
                'nome_produto' => 'DEEZER',
                'descricao_produto' => 'Cartão para uso no DEEZER, serviço de streaming digital.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Não é possível utilizar o Cartão Presente Deezer Virtual no aplicativo. 
O Cartão Presente Deezer Virtual é válido somente para a conta Premium, não sendo possível para planos HiFi, Anual, Familiar, Estudantil ou promoções.
Cartão ao portador, não recarregável, intransferível e não cumulativo.
Para compras e mais informações acesse www.deezer.com',
                'valor_produto' => 338,
                'valor_reais' => '17.00',
                'fotos' => NULL,
                'foto_exemplo' => 'd0c66d863e6774069951530f472eb349.jpg',
                'cod_categoria_produto' => 7,
                'video' => '-',
                'validade_produto' => '6 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            436 => 
            array (
                'cod_produto' => 3728,
                'nome_produto' => 'HAVAIANAS',
                'descricao_produto' => 'Cartão para aquisição de produtos na HAVAIANAS, loja de calçados e acessórios.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => NULL,
                'informacoes_importantes' => 'Para endereços e mais informações acesse: www.havaianas.com.br',
                'valor_produto' => 1998,
                'valor_reais' => '100.00',
                'fotos' => NULL,
                'foto_exemplo' => '7939084dd05b30753e3c554454bc4dcb.jpg',
                'cod_categoria_produto' => 8,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            437 => 
            array (
                'cod_produto' => 3729,
                'nome_produto' => 'Aprova Bancários - CPA 10',
                'descricao_produto' => 'Voucher digital para uso Escola Aprova que é uma Escola voltada exclusivamente para bancários e correspondentes bancários',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito para curso CPA 10 ONLINE.
A matrícula será através da Equipe YetzCards.
Deve matricular-se em até 3 meses após o resgate do voucher.',
                'informacoes_importantes' => 'Exclusivo para o curso CPA 10 ONLINE
Durabilidade de 20h de aula.
Para método, conteúdo, carga horária e mais informações acesse: https://aprovabancarios.com/anbima/cpa-10/curso-online/',
                'valor_produto' => 5940,
                'valor_reais' => '297.00',
                'fotos' => NULL,
                'foto_exemplo' => '3b53c47887c51aea5d9f1b26afb4caed.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            438 => 
            array (
                'cod_produto' => 3730,
                'nome_produto' => 'Aprova Bancários - CPA 20',
                'descricao_produto' => 'Voucher digital para uso Escola Aprova que é uma Escola voltada exclusivamente para bancários e correspondentes bancários',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito para curso CPA 10 ONLINE.
A matrícula será através da Equipe YetzCards.
Deve matricular-se em até 3 meses após o resgate do voucher.',
                'informacoes_importantes' => 'Exclusivo para o curso CPA 20 ONLINE
Durabilidade de 25h de aula.
Para unidades, método, conteúdo, carga horária e mais informações acesse: https://aprovabancarios.com/anbima/cpa-20/curso-online/',
                'valor_produto' => 6940,
                'valor_reais' => '347.00',
                'fotos' => NULL,
                'foto_exemplo' => '8ec5f5a8defd5873a484757b4d9a299f.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 1,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            439 => 
            array (
                'cod_produto' => 3731,
                'nome_produto' => 'IFB - CPA 10',
                'descricao_produto' => 'Voucher digital para uso no Instituto de Formação Bancária que é a maior empresa em cursos preparatórios para certificações financeiras. Oferecemos anualmente mais de 100 cursos preparatórios de CPA 10, 20, CEA, Agente Autônomo, CA 600 e Matemática Financeira em mais de 30 cidades, com mais de 3.800 alunos por ano e aprovação média de 85%.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito para curso CPA 10 Presencial em qualquer unidade do Estado de São Paulo.
A matrícula, escolha da data e unidade será através da Equipe YetzCards.
Deve matricular-se em até 3 meses após o resgate do voucher.',
                'informacoes_importantes' => 'Exclusivo para o curso CPA 10 PRESENCIAL
Durabilidade de 2 dias de aula.
Para unidades, método, conteúdo, carga horária e mais informações acesse: https://institutoformacaobancaria.com.br/cpa-10/',
                'valor_produto' => 9800,
                'valor_reais' => '490.00',
                'fotos' => NULL,
                'foto_exemplo' => '73a43ed38e0c7a5a74d1f8d9f60cd473.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            440 => 
            array (
                'cod_produto' => 3732,
                'nome_produto' => 'IFB - CPA 20',
                'descricao_produto' => 'Voucher digital para uso no Instituto de Formação Bancária que é a maior empresa em cursos preparatórios para certificações financeiras. Oferecemos anualmente mais de 100 cursos preparatórios de CPA 10, 20, CEA, Agente Autônomo, CA 600 e Matemática Financeira em mais de 30 cidades, com mais de 3.800 alunos por ano e aprovação média de 85%.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito para curso CPA 20 Presencial em qualquer unidade do Estado de São Paulo.
A matrícula, escolha da data e unidade será através da Equipe YetzCards.
Deve matricular-se em até 3 meses após o resgate do voucher.',
                'informacoes_importantes' => 'Exclusivo para o curso CPA 20 PRESENCIAL
Durabilidade de 24h de aula (3 sábados ou 2 semanas).
Para unidades, método, conteúdo, carga horária e mais informações acesse: https://institutoformacaobancaria.com.br/cpa-20/',
                'valor_produto' => 15800,
                'valor_reais' => '790.00',
                'fotos' => NULL,
                'foto_exemplo' => '8fa01ae4e02d1eed366c390c638959a3.jpg',
                'cod_categoria_produto' => 17,
                'video' => '-',
                'validade_produto' => '3 meses',
                'selo' => 0,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
            441 => 
            array (
                'cod_produto' => 3733,
                'nome_produto' => 'CINEMARK INDIVIDUAL',
                'descricao_produto' => 'Voucher para aquisição de um par de ingressos na rede de cinemas CINEMARK.',
                'cod_bandeira' => NULL,
                'detalhes_produto' => 'Aceito apenas em loja física.
Não permite saldo remanescente.
Válido para qualquer dia da semana e horário, exceto salas Prime, Premier, 3D e XD.',
                'informacoes_importantes' => 'Para informações de salas e programação acesse: www.cinemark.com.br',
                'valor_produto' => 440,
                'valor_reais' => '22.00',
                'fotos' => NULL,
                'foto_exemplo' => 'dv-yetzcards-cartao-lazer-cinemark-individual.jpg',
                'cod_categoria_produto' => 7,
                'video' => NULL,
                'validade_produto' => '45 dias',
                'selo' => 2,
                'tipo' => 1,
                'disponibilidade' => 1,
                'created_at' => '2019-06-05 20:05:14',
                'updated_at' => '2019-06-05 20:05:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}