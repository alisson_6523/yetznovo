<?php

use Illuminate\Database\Seeder;
use App\Models\HistoricoMovimentacao;
use Carbon\Carbon;

class HistoricoMovimentacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //


        HistoricoMovimentacao::insert([
            [
                'cod_movimentacao' => '1',
                'cod_usuario' => '1',
                'cod_tipo_movimentacao' => '2',
                'valor' => '39000',
                'descricao' => 'Pontos iniciais gerados pelas Seed "HistoricoMovimentacaoSeeder"',
                'protocolo' => '55555555555555',
                'expiracao' => NULL,
                'referencia' => NULL,
                'created_at' => Carbon::now()
            ],
            [
                'cod_movimentacao' => '2',
                'cod_usuario' => '1',
                'cod_tipo_movimentacao' => '1',
                'valor' => '5000',
                'descricao' => 'Resgate gerado pela seeder"',
                'protocolo' => '1c5c5409e1',
                'expiracao' => NULL,
                'referencia' => NULL,
                'created_at' => Carbon::now()
            ],
            [
                'cod_movimentacao' => '3',
                'cod_usuario' => '1',
                'cod_tipo_movimentacao' => '1',
                'valor' => '7000',
                'descricao' => 'Resgate gerado pela seeder"',
                'protocolo' => '2c8f56dfdd',
                'expiracao' => NULL,
                'referencia' => NULL,
                'created_at' => Carbon::now()
            ],
            [
                'cod_movimentacao' => '4',
                'cod_usuario' => '1',
                'cod_tipo_movimentacao' => '1',
                'valor' => '7000',
                'descricao' => 'Resgate gerado pela seeder"',
                'protocolo' => '3c9c4fd5cd',
                'expiracao' => NULL,
                'referencia' => NULL,
                'created_at' => Carbon::now()
            ],
        ]);
    }
}
