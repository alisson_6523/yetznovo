<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::insert([
            [
                'id' => 1,
                'password' => \Hash::make('qwe'),
                'cod_pdv' => 1,
                'nivel_usuario' => 1,
                'status' => 1,
                'login' => 'EDNEY',
                'nome_usuario' => 'Edney P Gonçalves',
                'data_nascimento' => '1991-02-10',
                'apelido' => 'Edney',
                'protocolo_insercao' => 1,
                'pontos_reais' => '20000',
                'foto_usuario' => 'semfoto.jpg',
                'email' => 'desenvolvimento@cryptos.eti.br',
                'telefone' => '8888-8888',
                'celular' => '8888-8888',
                'sexo' => 'm',
                'ilha' => 'CRY',
                'notificacao_celular' => 0,
                'cargo' => 'dev',

            ],

            [
                'id' => 2,
                'password' => \Hash::make('123123'),
                'cod_pdv' => 1,
                'nivel_usuario' => 1,
                'status' => 1,
                'login' => 'USUARIO',
                'nome_usuario' => 'Usuário',
                'data_nascimento' => '1991-02-10',
                'apelido' => 'Usuario',
                'protocolo_insercao' => 1,
                'pontos_reais' => '20000',
                'email' => 'desenvolvimento@cryptos.eti.br',
                'telefone' => '8888-8888',
                'celular' => '8888-8888',
                'foto_usuario' => 'semfoto.jpg',
                'sexo' => 'm',
                'ilha' => 'YETZ',
                'notificacao_celular' => 0,
                'cargo' => 'dev',

            ],
            [
                'id' => 3,
                'password' => \Hash::make('123123'),
                'cod_pdv' => 2,
                'nivel_usuario' => 1,
                'status' => 1,
                'login' => 'USR2',
                'nome_usuario' => 'Usuário 2',
                'data_nascimento' => '1991-02-10',
                'apelido' => 'Usuario',
                'protocolo_insercao' => 1,
                'pontos_reais' => '20000',
                'email' => 'desenvolvimento@cryptos.eti.br',
                'telefone' => '8888-8888',
                'celular' => '8888-8888',
                'foto_usuario' => 'semfoto.jpg',
                'sexo' => 'm',
                'ilha' => 'YETZ',
                'notificacao_celular' => 0,
                'cargo' => 'dev',

            ],
            [
                'id' => 4,
                'password' => \Hash::make('123123'),
                'cod_pdv' => 2,
                'nivel_usuario' => 1,
                'status' => 1,
                'login' => 'USR3',
                'nome_usuario' => 'Usuário 3',
                'data_nascimento' => '1991-02-10',
                'apelido' => 'Usuario',
                'protocolo_insercao' => 1,
                'pontos_reais' => '20000',
                'email' => 'desenvolvimento@cryptos.eti.br',
                'telefone' => '8888-8888',
                'celular' => '8888-8888',
                'foto_usuario' => 'semfoto.jpg',
                'sexo' => 'm',
                'ilha' => 'CARAVELA',
                'notificacao_celular' => 0,
                'cargo' => 'dev',

            ],
        ]);



    }
}
