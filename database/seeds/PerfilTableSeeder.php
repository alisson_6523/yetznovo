<?php

use Illuminate\Database\Seeder;
use App\Models\AdminPerfil;

class PerfilTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminPerfil::insert([
            ['nome' => 'Administrador Master'],
            ['nome' => 'Administrador Cliente']
        ]);
    }
}
