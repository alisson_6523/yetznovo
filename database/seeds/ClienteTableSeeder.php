<?php

use Illuminate\Database\Seeder;
use App\Models\Cliente;
use Illuminate\Support\Carbon;

class ClienteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Cliente::insert([
                [
                'razao_social'     => 'Caravela',
                'nome_fantasia'     => 'Caravela',
                'inscricao_estadual'     => '222222222',
                'inscricao_municipal'     => '222222',
                'cnpj'     => '222222',
                'cep'     => '37.701-001',
                'logradouro'     => 'R. Rio G do Sul',
                'bairro'     => 'Centro',
                'cidade'     => 'Poços de Caldas',
                'estado'     => 'MG',
                'numero'    => 1200,
                'telefone'     => '(35) 3722-2350',
                'email'     => 'contato@agenciacaravela.com.br',
                'site'     => 'www.agenciacaravela.com.br'
                ],
                [
                'razao_social'     => 'Cryptos',
                'nome_fantasia'     => 'Cryptos',
                'inscricao_estadual'     => '33333333333',
                'inscricao_municipal'     => '33333333',
                'cnpj'     => '3333333',
                'cep'     => '37.701-001',
                'logradouro'     => 'R. Rio G do Sul',
                'bairro'     => 'Centro',
                'cidade'     => 'Poços de Caldas',
                'estado'     => 'MG',
                'numero'    => 1200,
                'telefone'     => '(35) 3722-2350',
                'email'     => 'contato@agenciacaravela.com.br',
                'site'     => 'www.agenciacaravela.com.br'
                ]
            ]
    
    );

    }
}
