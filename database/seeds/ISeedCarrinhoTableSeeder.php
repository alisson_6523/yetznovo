<?php

use Illuminate\Database\Seeder;

class ISeedCarrinhoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('carrinho')->delete();
        
        \DB::table('carrinho')->insert(array (
            0 => 
            array (
                'cod_carrinho' => 1,
                'cod_usuario' => 1,
                'status_carrinho' => 2,
                'created_at' => '2019-07-15 17:53:50',
                'updated_at' => '2019-07-15 17:54:15',
            ),
            1 => 
            array (
                'cod_carrinho' => 2,
                'cod_usuario' => 1,
                'status_carrinho' => 2,
                'created_at' => '2019-07-15 17:54:21',
                'updated_at' => '2019-07-15 17:56:15',
            ),
            2 => 
            array (
                'cod_carrinho' => 3,
                'cod_usuario' => 1,
                'status_carrinho' => 2,
                'created_at' => '2019-07-15 17:56:24',
                'updated_at' => '2019-07-15 17:56:24',
            ),
        ));
        
        
    }
}