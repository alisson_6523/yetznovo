<?php

use Illuminate\Database\Seeder;
use App\Models\NotificacaoStreaming;

class NotificacaoStreamingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $streaming = [
            ['cod_usuario' => '1', 'cod_streaming' => '12', 'visualizado' => '0'],
            ['cod_usuario' => '1', 'cod_streaming' => '13', 'visualizado' => '0'],
            ['cod_usuario' => '1', 'cod_streaming' => '14', 'visualizado' => '0'],
            ['cod_usuario' => '1', 'cod_streaming' => '15', 'visualizado' => '0'],
        ];

        NotificacaoStreaming::insert($streaming);


    }
}
