<?php

use Illuminate\Database\Seeder;

class ISeedItemCarrinhoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('item_carrinho')->delete();
        
        \DB::table('item_carrinho')->insert(array (
            0 => 
            array (
                'cod_item' => 1,
                'cod_produto' => 3379,
                'cod_carrinho' => 1,
                'cod_status' => 1,
                'created_at' => '2019-07-15 17:54:08',
                'updated_at' => '2019-07-15 17:54:08',
            ),
            1 => 
            array (
                'cod_item' => 2,
                'cod_produto' => 3416,
                'cod_carrinho' => 2,
                'cod_status' => 1,
                'created_at' => '2019-07-15 17:55:54',
                'updated_at' => '2019-07-15 17:55:54',
            ),
            2 => 
            array (
                'cod_item' => 3,
                'cod_produto' => 3428,
                'cod_carrinho' => 2,
                'cod_status' => 1,
                'created_at' => '2019-07-15 17:55:59',
                'updated_at' => '2019-07-15 17:55:59',
            ),
            3 => 
            array (
                'cod_item' => 4,
                'cod_produto' => 3428,
                'cod_carrinho' => 3,
                'cod_status' => 1,
                'created_at' => '2019-07-15 17:55:59',
                'updated_at' => '2019-07-15 17:55:59',
            ),
            4 => 
            array (
                'cod_item' => 5,
                'cod_produto' => 3416,
                'cod_carrinho' => 3,
                'cod_status' => 1,
                'created_at' => '2019-07-15 17:55:59',
                'updated_at' => '2019-07-15 17:55:59',
            ),
        ));
        
        
    }
}