<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class AdministradorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::insert([
            [
                'perfil_id'  => 1,
                'nome'       => 'Edney Paulo Gonçalves',
                'email'      => 'desenvolvimento@cryptos.eti.br',
                'login'      => 'edney',
                'password'   => Hash::make('qwe'),
                'created_at' => Carbon::now()
            ],
            [
                'perfil_id'  => 1,
                'nome'       => 'Administrador',
                'email'      => 'administrador@yetzcards.com.br',
                'login'      => 'admin',
                'password'   => Hash::make('123123'),
                'created_at' => Carbon::now()
            ]
        ]);

    }
}
