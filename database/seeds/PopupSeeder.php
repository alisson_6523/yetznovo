<?php

use Illuminate\Database\Seeder;
use App\Models\NotificacaoPopup;

class PopupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $notificacao_popups = array(
            array('cod_popup' => '22', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 14/02/2019','foto_popup' => '249f6bf556263fab2c15bb2fefaee9a2.jpg','entrada_popup' => '2019-02-13 22:00:00','saida_popup' => '2019-06-23 21:00:00'),
            array('cod_popup' => '24', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 15/02/2019','foto_popup' => '8e4b121da0615c83966e87ffa2c69dd4.jpg','entrada_popup' => '2019-02-14 22:00:00','saida_popup' => '2019-02-21 21:00:00'),
            array('cod_popup' => '25', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 08/03/2019','foto_popup' => '7dffcde0cb178618ed939bbd2c337eb3.jpg','entrada_popup' => '2019-03-07 21:00:00','saida_popup' => '2019-03-08 21:00:00'),
            array('cod_popup' => '26', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 08/03/2019','foto_popup' => '66233cd2603f248db35c93df268c4c36.jpg','entrada_popup' => '2019-03-08 21:00:00','saida_popup' => '2019-03-30 21:00:00'),
            array('cod_popup' => '27', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 04/04/2019','foto_popup' => '1d23f23503ed88f1fcb90a8ed2bd0fe4.jpg','entrada_popup' => '2019-04-03 21:00:00','saida_popup' => '2019-04-10 21:00:00'),
            array('cod_popup' => '28', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 18/04/2019','foto_popup' => '379737a43ae05f02ed0524189fb6c275.jpg','entrada_popup' => '2019-04-17 21:00:00','saida_popup' => '2019-04-21 21:00:00'),
            array('cod_popup' => '29', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 06/06/2019','foto_popup' => '1bdd37bda80fa1c619921a5e81d384dd.jpg','entrada_popup' => '2019-06-05 21:00:00','saida_popup' => '2019-06-23 21:00:00'),
            array('cod_popup' => '30', 'cod_cliente' => '1', 'titulo_popup' => 'Popup criado no dia 06/06/2019','foto_popup' => '36cbe724daf892371baee4278ecfb143.jpg','entrada_popup' => '2019-06-05 21:00:00','saida_popup' => '2019-06-13 21:00:00')
          );


          NotificacaoPopup::insert($notificacao_popups);




    }
}
