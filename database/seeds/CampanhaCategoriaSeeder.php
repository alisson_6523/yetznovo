<?php

use Illuminate\Database\Seeder;
use App\Models\Campanha;
use App\Models\CategoriaProduto;
use App\Models\CategoriaExibir;
use App\Models\CampanhaCategorium;
use App\Models\CampanhaProduto;

class CampanhaCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = CategoriaProduto::all();

        foreach ($categorias as $c) :
            $ce = CampanhaCategorium::where('cod_campanha', '=', '1')->where('cod_categoria', '=', $c->cod_categoria_produto)->first();
            if (!$ce) :
                CampanhaCategorium::create(['cod_campanha' => 1, 'cod_categoria' => $c->cod_categoria_produto]);

                $produtos = $c->produtos;

                foreach ($produtos as $p) :

                    $cp = CampanhaProduto::where('cod_campanha', '=', '1')->where('cod_produto', '=', $p->cod_produto)->first();

                    if (!$cp)
                        CampanhaProduto::create(['cod_campanha' => 1, 'cod_produto' => $p->cod_produto]);

                endforeach;
            endif;
        endforeach;
    }
}
