<?php

use Illuminate\Database\Seeder;
use App\Models\StatusUsuario;

class StatusUsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $statement = "SET sql_mode='NO_AUTO_VALUE_ON_ZERO'";
        DB::unprepared($statement);
        StatusUsuario::insert([
            ['cod_status'=> '0', 'nome_status'     => 'Não Cadastrado'],
            ['cod_status'=> '1', 'nome_status'     => 'Cadastrado'],
            ['cod_status'=> '2', 'nome_status'     => 'Solicitou Senha'],
            ['cod_status'=> '3', 'nome_status'     => 'Bloqueado'],
        ]);

    }
}
