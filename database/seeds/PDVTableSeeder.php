<?php

use Illuminate\Database\Seeder;
use App\Models\ClientePdv;

class PDVTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $pdvs = [
            [
                'cod_pdv' => '1',
                'cliente_id' => '1',
                'registro' => 'CRYPTOS',
                'nome_pdv' => 'Cryptos',
                'razao_social' => 'Cryptos Segurança e Tecnologia da Informação',
                'cnpj' => '2222222222',
                'cep' => '37701-001',
                'logradouro' => 'Rua Rio Grande do Sul',
                'numero' => '1200',
                'complemento' => '1 andar',
                'bairro' => 'Centro',
                'cidade' => 'Poços de Caldas',
                'estado' => 'MG',
                'telefone' => '(35)3714-3714',
                'email' => 'contato@agenciacaravela.com.br',
                'site' => 'https://www.agenciacaravela.com.br',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
            [
                'cod_pdv' => '2',
                'cliente_id' => '1',
                'registro' => 'CARAVELA',
                'nome_pdv' => 'Caravela',
                'razao_social' => 'Caravela Comunicações',
                'cnpj' => '2222222222',
                'cep' => '37701-001',
                'logradouro' => 'Rua Rio Grande do Sul',
                'numero' => '1200',
                'complemento' => '1 andar',
                'bairro' => 'Centro',
                'cidade' => 'Poços de Caldas',
                'estado' => 'MG',
                'telefone' => '(35)3714-3714',
                'email' => 'contato@agenciacaravela.com.br',
                'site' => 'https://www.agenciacaravela.com.br',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now()
            ],
        ];

        ClientePdv::insert($pdvs);
    }
}
