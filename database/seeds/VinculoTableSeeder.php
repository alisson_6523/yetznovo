<?php

use Illuminate\Database\Seeder;
use App\Models\Vinculo;

class VinculoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Vinculo::insert([
            ['cod_video' => '12', 'cod_pdv' => '1', 'ilha' => 'CRY'],
            ['cod_video' => '12', 'cod_pdv' => '1', 'ilha' => 'YETZ'],
            ['cod_video' => '13', 'cod_pdv' => '1', 'ilha' => 'CRY']
        ]);
    }
}
