<?php

use Illuminate\Database\Seeder;

class ISeedCupomPedidoTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cupom_pedido')->delete();
        
        \DB::table('cupom_pedido')->insert(array (
            0 => 
            array (
                'id' => 1,
                'admin_id' => 1,
                'cliente_id' => 1,
                'nome' => 'Primeiro lote gerado por seeds',
                'quantidade' => 3,
                'prefixo' => '#GANHEI',
                'created_at' => '2019-07-24 00:00:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}