<?php

use Illuminate\Database\Seeder;

class ISeedResgateTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('resgate')->delete();
        
        \DB::table('resgate')->insert(array (
            0 => 
            array (
                'cod_resgate' => 1,
                'cod_usuario' => 1,
                'cod_carrinho' => 1,
                'cod_entrega' => NULL,
                'cod_status' => 1,
                'cod_campanha' => 1,
                'registro' => '1c5c5409e1',
                'valor_resgate' => 5000,
                'data_entregue' => '2019-08-19 17:54:15',
                'created_at' => '2019-06-15 17:54:15',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'cod_resgate' => 2,
                'cod_usuario' => 1,
                'cod_carrinho' => 2,
                'cod_entrega' => NULL,
                'cod_status' => 1,
                'cod_campanha' => 1,
                'registro' => '2c8f56dfdd',
                'valor_resgate' => 7000,
                'data_entregue' => '2019-08-19 17:56:14',
                'created_at' => '2019-07-15 17:56:14',
                'updated_at' => '2019-07-15 17:56:14',
            ),
            2 => 
            array (
                'cod_resgate' => 3,
                'cod_usuario' => 2,
                'cod_carrinho' => 3,
                'cod_entrega' => NULL,
                'cod_status' => 1,
                'cod_campanha' => 1,
                'registro' => '3c9c4fd5cd',
                'valor_resgate' => 7000,
                'data_entregue' => '2019-08-22 15:03:32',
                'created_at' => '2019-07-18 15:03:32',
                'updated_at' => '2019-07-18 15:03:32',
            ),
        ));
        
        
    }
}