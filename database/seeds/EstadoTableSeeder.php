<?php

use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = [
            ['sigla' => 'AC','nome_estado' => 'Acre','cod_ibge' => '12'],
            ['sigla' => 'AL','nome_estado' => 'Alagoas','cod_ibge' => '27'],
            ['sigla' => 'AM','nome_estado' => 'Amazonas','cod_ibge' => '13'],
            ['sigla' => 'AP','nome_estado' => 'Amapá','cod_ibge' => '16'],
            ['sigla' => 'BA','nome_estado' => 'Bahia','cod_ibge' => '29'],
            ['sigla' => 'CE','nome_estado' => 'Ceará','cod_ibge' => '23'],
            ['sigla' => 'DF','nome_estado' => 'Distrito Federal','cod_ibge' => '53'],
            ['sigla' => 'ES','nome_estado' => 'Espírito Santo','cod_ibge' => '32'],
            ['sigla' => 'GO','nome_estado' => 'Goiás','cod_ibge' => '52'],
            ['sigla' => 'MA','nome_estado' => 'Maranhão','cod_ibge' => '21'],
            ['sigla' => 'MG','nome_estado' => 'Minas Gerais','cod_ibge' => '31'],
            ['sigla' => 'MS','nome_estado' => 'Mato Grosso do Sul','cod_ibge' => '50'],
            ['sigla' => 'MT','nome_estado' => 'Mato Grosso','cod_ibge' => '51'],
            ['sigla' => 'PA','nome_estado' => 'Pará','cod_ibge' => '15'],
            ['sigla' => 'PB','nome_estado' => 'Paraíba','cod_ibge' => '25'],
            ['sigla' => 'PE','nome_estado' => 'Pernambuco','cod_ibge' => '26'],
            ['sigla' => 'PI','nome_estado' => 'Piauí','cod_ibge' => '22'],
            ['sigla' => 'PR','nome_estado' => 'Paraná','cod_ibge' => '41'],
            ['sigla' => 'RJ','nome_estado' => 'Rio de Janeiro','cod_ibge' => '33'],
            ['sigla' => 'RN','nome_estado' => 'Rio Grande do Norte','cod_ibge' => '24'],
            ['sigla' => 'RO','nome_estado' => 'Rondônia','cod_ibge' => '11'],
            ['sigla' => 'RR','nome_estado' => 'Roraima','cod_ibge' => '14'],
            ['sigla' => 'RS','nome_estado' => 'Rio Grande do Sul','cod_ibge' => '43'],
            ['sigla' => 'SC','nome_estado' => 'Santa Catarina','cod_ibge' => '42'],
            ['sigla' => 'SE','nome_estado' => 'Sergipe','cod_ibge' => '28'],
            ['sigla' => 'SP','nome_estado' => 'São Paulo','cod_ibge' => '35'],
            ['sigla' => 'TO','nome_estado' => 'Tocantis','cod_ibge' => '17']
        ];
          
        Estado::insert($estados);


    }
}
