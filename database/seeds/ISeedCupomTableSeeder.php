<?php

use Illuminate\Database\Seeder;

class ISeedCupomTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cupom')->delete();
        
        \DB::table('cupom')->insert(array (
            0 => 
            array (
                'id' => 1,
                'pedidoId' => 1,
                'userId' => NULL,
                'codigo' => '#GANHEI800-zxcv',
                'pontos' => 800,
                'usado_em' => NULL,
                'created_at' => '2019-07-24 00:00:00',
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'pedidoId' => 1,
                'userId' => NULL,
                'codigo' => '#GANHEI800-asdf',
                'pontos' => 800,
                'usado_em' => NULL,
                'created_at' => '2019-07-24 00:00:00',
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'pedidoId' => 1,
                'userId' => NULL,
                'codigo' => '#GANHEI800-ghjk',
                'pontos' => 800,
                'usado_em' => NULL,
                'created_at' => '2019-07-24 00:00:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}