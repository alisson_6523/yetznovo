<?php

use Illuminate\Database\Seeder;
use App\Models\NotificacaoCategorium;

class NotificacaoCategoriaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        NotificacaoCategorium::insert(
            array(
                array('cod_categoria' => '1','nome_categoria' => 'Crédito'),
                array('cod_categoria' => '2','nome_categoria' => 'Alerta'),
                array('cod_categoria' => '3','nome_categoria' => 'Novo Cartão'),
                array('cod_categoria' => '4','nome_categoria' => 'Dicas')
              )
        );
    }
}
