<?php

use Illuminate\Database\Seeder;
use App\Models\CampanhaProtocolo;

class CampanhaProtocoloSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CampanhaProtocolo::insert([
            'cod_campanha'      => 1,
            'protocolo'     => 'eeeeeeeeeeee',
            'checksum'     => md5('hoje é quinta-feira!!!'),
            'efetivado'     => 1,
            'registros_aceitos'     => 1,
            'registros_nao_aceitos'     => 0,
            'total_pontos'     => 0,
            'link_xls'     => '-',
            'link_cvl'     => '-',
            'arquivo_carregado'     => '-',
            'tipo_protocolo'     => 1,
        ]);
    }
}
