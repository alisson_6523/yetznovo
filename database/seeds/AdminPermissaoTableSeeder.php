<?php

use Illuminate\Database\Seeder;

class AdminPermissaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\AdminPermissao::insert([
            ['perfilId' => 1, 'paginaId' => 1, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 2, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 3, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 4, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 5, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 6, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 7, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 8, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 9, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 10, 'created_at' => \Carbon\Carbon::now()],
            ['perfilId' => 1, 'paginaId' => 11, 'created_at' => \Carbon\Carbon::now()],
        ]);
    }
}
