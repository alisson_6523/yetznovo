<?php

use Illuminate\Database\Seeder;
use App\Models\Campanha;
use Illuminate\Support\Carbon;

class CampanhaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Campanha::create([
            'cliente_id' => 1,
            'nome_campanha' => 'Desenvolvedores Yetz',
            'chave' => 'CRYPTOS',
            'super' => FALSE,
            'fale_conosco' => FALSE,
            'dt_inicio' => '2019-07-01',
            'dt_fim'    => '2019-08-01',
            'autenticacao' => 1
        ]);

    }
}
