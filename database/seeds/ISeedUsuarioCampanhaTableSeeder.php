<?php

use Illuminate\Database\Seeder;

class ISeedUsuarioCampanhaTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('usuario_campanha')->delete();
        
        \DB::table('usuario_campanha')->insert(array (
            0 => 
            array (
                'cod_usuario' => 1,
                'cod_campanha' => 1,
            ),
            1 => 
            array (
                'cod_usuario' => 2,
                'cod_campanha' => 1,
            ),
            2 => 
            array (
                'cod_usuario' => 3,
                'cod_campanha' => 1,
            ),
            3 => 
            array (
                'cod_usuario' => 4,
                'cod_campanha' => 1,
            ),
        ));
        
        
    }
}