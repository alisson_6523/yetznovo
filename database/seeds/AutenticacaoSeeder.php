<?php

use Illuminate\Database\Seeder;
use App\Models\Autenticacao;

class AutenticacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Autenticacao::create([
            'cod_autenticacao'      => 1,
            'tipo_autenticacao'     => 'matricula',
        ]);

        Autenticacao::create([
            'cod_autenticacao'      => 2,
            'tipo_autenticacao'     => 'cpf',
        ]);

        Autenticacao::create([
            'cod_autenticacao'      => 3,
            'tipo_autenticacao'     => 'super',
        ]);

    }
}
