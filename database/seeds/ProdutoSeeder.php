<?php

use Illuminate\Database\Seeder;
use App\Models\Produto;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // nada a fazer por aqui... substituída pela classe ISeedProdutoTableSeeder
        
        // $produtosOLD = DB::connection('old')->table('produto')->get();
        $produtosOLD = [];

        foreach($produtosOLD as $p){

            
            $produto = new Produto();

            $produto->cod_produto = $p->cod_produto;
            $produto->nome_produto = $p->nome_produto;
		    $produto->descricao_produto = $p->descricao_produto;
		    $produto->cod_bandeira = $p->cod_bandeira;
		    $produto->detalhes_produto = $p->detalhes_produto;
		    $produto->informacoes_importantes = $p->informacoes_importantes;
		    $produto->valor_produto = $p->valor_produto;
		    $produto->valor_reais = $p->valor_reais;
		    $produto->fotos = $p->fotos;
		    $produto->foto_exemplo = $p->foto_exemplo;
		    $produto->cod_categoria_produto = $p->cod_categoria_produto;
		    $produto->video = $p->video;
		    $produto->validade_produto = $p->validade_produto;
		    $produto->selo = $p->selo;
		    $produto->tipo = $p->tipo;
		    $produto->disponibilidade = $p->disponibilidade;

            $produto->save();

        }


        $produtos = Produto::all();

        foreach($produtos as $p){

            $this->command->info("novo produto: $p->nome_produto");

        }



    }
}
