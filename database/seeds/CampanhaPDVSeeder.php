<?php

use Illuminate\Database\Seeder;
use App\Models\CampanhaPdv;

class CampanhaPDVSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        CampanhaPdv::insert(
            [
                [
                    'cod_campanha'      => 1,
                    'cod_pdv'     => 1,
                ],
                [
                    'cod_campanha'      => 1,
                    'cod_pdv'     => 2,
                ]
            ]
        );
    }
}
