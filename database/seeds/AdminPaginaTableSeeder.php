<?php

use Illuminate\Database\Seeder;

class AdminPaginaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\AdminPagina::insert([
            [
                'id' => 1,
                'nome' => 'campanhas',
                'link' => 'sistema/campanhas',
                'icone' => 'campanha',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 2,
                'nome' => 'clientes',
                'link' => 'sistema/clientes',
                'icone' => 'cliente',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3,
                'nome' => 'portal',
                'link' => 'sistema/portal',
                'icone' => 'portal',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 4,
                'nome' => 'notificações',
                'link' => 'sistema/notificacoes',
                'icone' => 'notificacao',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 5,
                'nome' => 'popups',
                'link' => 'sistema/popups',
                'icone' => 'notificacao',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 6,
                'nome' => 'streaming',
                'link' => 'sistema/streaming',
                'icone' => 'streaming',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 7,
                'nome' => 'cupons',
                'link' => 'sistema/cupons',
                'icone' => 'cartao',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 8,
                'nome' => 'resgates',
                'link' => 'sistema/resgates',
                'icone' => 'resgate',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 9,
                'nome' => 'usuários',
                'link' => 'sistema/usuarios',
                'icone' => 'user',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 10,
                'nome' => 'relatórios',
                'link' => 'sistema/relatorios',
                'icone' => 'grafico',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 11,
                'nome' => 'uploads',
                'link' => 'sistema/uploads',
                'icone' => 'arquivo',
                'grupo' => NULL,
                'created_at' => \Carbon\Carbon::now()
            ]
        ]);

    }
}
