<?php

use Illuminate\Database\Seeder;
use App\Models\ProdutoTipo;

class ProdutoTipoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $statement = "SET sql_mode='NO_AUTO_VALUE_ON_ZERO'";
        DB::unprepared($statement);

        ProdutoTipo::insert([
            ['cod_tipo'=> '0', 'nome_tipo'     => 'Cartão Físico'],
            ['cod_tipo'=> '1', 'nome_tipo'     => 'Voucher Digital'],
        ]);
    }
}
