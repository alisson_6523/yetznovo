<?php

use Illuminate\Database\Seeder;
use App\Models\TipoMovimentacao;
class TipoMovimentacaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoMovimentacao::insert([
            ['cod_tipo' => '1', 'descricao_tipo' => 'RESGATE'], 
            ['cod_tipo' => '2', 'descricao_tipo' => 'CRÉDITO'], 
            ['cod_tipo' => '3', 'descricao_tipo' => 'ESTORNO'],
            ['cod_tipo' => '4', 'descricao_tipo' => 'EXPIRAÇÃO']]);
    }
}
