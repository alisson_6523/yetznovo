<?php

use Illuminate\Database\Seeder;
use App\Models\CategoriaProduto;

class CategoriaProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categorias = [
            ['cod_categoria_produto' => '1','categoria' => 'FLORES E PRESENTES','descricao' => NULL,'link_categoria' => 'presente','icone_categoria' => 'presente','ordem' => '0'],
            ['cod_categoria_produto' => '2','categoria' => 'ALIMENTAÇÃO','descricao' => '','link_categoria' => 'alimentacao','icone_categoria' => 'alimentacao','ordem' => '0'],
            ['cod_categoria_produto' => '3','categoria' => 'CASA E DECORAÇÃO','descricao' => '','link_categoria' => 'casa-decoracao','icone_categoria' => 'casa-decoracao','ordem' => '0'],
            ['cod_categoria_produto' => '4','categoria' => 'TRANSPORTES','descricao' => '','link_categoria' => 'combustivel-viagem','icone_categoria' => 'combustivel','ordem' => '0'],
            ['cod_categoria_produto' => '5','categoria' => 'ELETROELETRÔNICOS','descricao' => '','link_categoria' => 'eletronicos','icone_categoria' => 'eletronicos','ordem' => '0'],
            ['cod_categoria_produto' => '6','categoria' => 'ESPORTES','descricao' => NULL,'link_categoria' => 'esportes','icone_categoria' => 'esportes','ordem' => '0'],
            ['cod_categoria_produto' => '7','categoria' => 'LAZER E CULTURA','descricao' => NULL,'link_categoria' => 'lazer-cultura','icone_categoria' => 'lazer-cultura','ordem' => '0'],
            ['cod_categoria_produto' => '8','categoria' => 'MODA','descricao' => NULL,'link_categoria' => 'moda','icone_categoria' => 'moda','ordem' => '0'],
            ['cod_categoria_produto' => '9','categoria' => 'LIVRARIA E PAPELARIA','descricao' => '','link_categoria' => 'papelaria-livrarias','icone_categoria' => 'papelaria-livrarias','ordem' => '0'],
            ['cod_categoria_produto' => '10','categoria' => 'RESTAURANTES','descricao' => NULL,'link_categoria' => 'restaurantes','icone_categoria' => 'restaurantes','ordem' => '0'],
            ['cod_categoria_produto' => '11','categoria' => 'SAÚDE E BELEZA','descricao' => NULL,'link_categoria' => 'saude-beleza','icone_categoria' => 'saude-beleza','ordem' => '0'],
            ['cod_categoria_produto' => '13','categoria' => 'BRINQUEDOS E GAMES','descricao' => '','link_categoria' => 'brinquedo','icone_categoria' => 'brinquedo','ordem' => '0'],
            ['cod_categoria_produto' => '14','categoria' => 'VIAGENS','descricao' => NULL,'link_categoria' => 'viagem','icone_categoria' => 'viagem','ordem' => '0'],
            ['cod_categoria_produto' => '15','categoria' => 'PET','descricao' => NULL,'link_categoria' => 'pet','icone_categoria' => 'pet','ordem' => '0'],
            ['cod_categoria_produto' => '17','categoria' => 'CURSOS','descricao' => NULL,'link_categoria' => 'cursos','icone_categoria' => 'cursos','ordem' => '0'],
            ['cod_categoria_produto' => '18','categoria' => 'CARTÃO PRÉ-PAGO','descricao' => NULL,'link_categoria' => 'cartao-pre-pago','icone_categoria' => 'cartao-pre-pago','ordem' => '0']
        ];

        CategoriaProduto::insert($categorias);



    }
}
