<?php

use Illuminate\Database\Seeder;
use App\Models\CategoriaProduto;
use App\Models\CategoriaExibir;
use App\Models\Video;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call(StatusResgateSeeder::class);
        $this->call(EstadoTableSeeder::class);
        $this->call(AutenticacaoSeeder::class);
        $this->call(ClienteTableSeeder::class);
        $this->call(CampanhaSeeder::class);
        $this->call(TipoMovimentacaoSeeder::class);
        $this->call(PDVTableSeeder::class);
        $this->call(StatusUsuarioTableSeeder::class);
//        $this->call(CampanhaProtocoloSeeder::class);
        $this->call(CampanhaPDVSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriaProdutoSeeder::class);
        $this->call(CampanhaCategoriaSeeder::class);
        $this->call(ProdutoTipoSeeder::class);
        $this->call(ISeedProdutoTableSeeder::class);
        $this->call(ISeedCategoriaExibirTableSeeder::class);
        $this->call(PerfilTableSeeder::class);
        $this->call(AdministradorTableSeeder::class);
        $this->call(VideoTableSeeder::class);
        $this->call(NotificacaoStreamingTableSeeder::class);
        $this->call(PopupSeeder::class);
        $this->call(PopupUsuarioSeeder::class);
        $this->call(HistoricoMovimentacaoSeeder::class);
        $this->call(ISeedCategoriaExibirTableSeeder::class);
        $this->call(NotificacaoCategoriaTableSeeder::class);
        $this->call(VinculoTableSeeder::class);
        $this->call(ISeedBannerTableSeeder::class);
        $this->call(ISeedCarrinhoTableSeeder::class);
        $this->call(ISeedItemCarrinhoTableSeeder::class);
        $this->call(ISeedResgateTableSeeder::class);
        $this->call(ISeedCupomPedidoTableSeeder::class);
        $this->call(ISeedCupomTableSeeder::class);
        $this->call(ISeedUsuarioCampanhaTableSeeder::class);
    }
}
