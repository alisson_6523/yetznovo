<?php

use Illuminate\Database\Seeder;

class ISeedCategoriaExibirTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categoria_exibir')->delete();
        
        \DB::table('categoria_exibir')->insert(array (
            0 => 
            array (
                'id' => 1,
                'cod_categoria' => 1,
                'cod_produto' => 3452,
            ),
            1 => 
            array (
                'id' => 2,
                'cod_categoria' => 1,
                'cod_produto' => 3453,
            ),
            2 => 
            array (
                'id' => 3,
                'cod_categoria' => 1,
                'cod_produto' => 3454,
            ),
            3 => 
            array (
                'id' => 4,
                'cod_categoria' => 1,
                'cod_produto' => 3455,
            ),
            4 => 
            array (
                'id' => 5,
                'cod_categoria' => 1,
                'cod_produto' => 3456,
            ),
            5 => 
            array (
                'id' => 6,
                'cod_categoria' => 1,
                'cod_produto' => 3457,
            ),
            6 => 
            array (
                'id' => 7,
                'cod_categoria' => 2,
                'cod_produto' => 3286,
            ),
            7 => 
            array (
                'id' => 8,
                'cod_categoria' => 2,
                'cod_produto' => 3287,
            ),
            8 => 
            array (
                'id' => 9,
                'cod_categoria' => 2,
                'cod_produto' => 3288,
            ),
            9 => 
            array (
                'id' => 10,
                'cod_categoria' => 2,
                'cod_produto' => 3289,
            ),
            10 => 
            array (
                'id' => 11,
                'cod_categoria' => 2,
                'cod_produto' => 3290,
            ),
            11 => 
            array (
                'id' => 12,
                'cod_categoria' => 2,
                'cod_produto' => 3291,
            ),
            12 => 
            array (
                'id' => 13,
                'cod_categoria' => 2,
                'cod_produto' => 3292,
            ),
            13 => 
            array (
                'id' => 14,
                'cod_categoria' => 2,
                'cod_produto' => 3293,
            ),
            14 => 
            array (
                'id' => 15,
                'cod_categoria' => 2,
                'cod_produto' => 3294,
            ),
            15 => 
            array (
                'id' => 16,
                'cod_categoria' => 2,
                'cod_produto' => 3295,
            ),
            16 => 
            array (
                'id' => 17,
                'cod_categoria' => 2,
                'cod_produto' => 3296,
            ),
            17 => 
            array (
                'id' => 18,
                'cod_categoria' => 2,
                'cod_produto' => 3297,
            ),
            18 => 
            array (
                'id' => 19,
                'cod_categoria' => 2,
                'cod_produto' => 3298,
            ),
            19 => 
            array (
                'id' => 20,
                'cod_categoria' => 2,
                'cod_produto' => 3299,
            ),
            20 => 
            array (
                'id' => 21,
                'cod_categoria' => 2,
                'cod_produto' => 3300,
            ),
            21 => 
            array (
                'id' => 22,
                'cod_categoria' => 2,
                'cod_produto' => 3301,
            ),
            22 => 
            array (
                'id' => 23,
                'cod_categoria' => 2,
                'cod_produto' => 3302,
            ),
            23 => 
            array (
                'id' => 24,
                'cod_categoria' => 2,
                'cod_produto' => 3303,
            ),
            24 => 
            array (
                'id' => 25,
                'cod_categoria' => 2,
                'cod_produto' => 3304,
            ),
            25 => 
            array (
                'id' => 26,
                'cod_categoria' => 2,
                'cod_produto' => 3305,
            ),
            26 => 
            array (
                'id' => 27,
                'cod_categoria' => 2,
                'cod_produto' => 3306,
            ),
            27 => 
            array (
                'id' => 28,
                'cod_categoria' => 2,
                'cod_produto' => 3307,
            ),
            28 => 
            array (
                'id' => 29,
                'cod_categoria' => 2,
                'cod_produto' => 3308,
            ),
            29 => 
            array (
                'id' => 30,
                'cod_categoria' => 2,
                'cod_produto' => 3309,
            ),
            30 => 
            array (
                'id' => 31,
                'cod_categoria' => 2,
                'cod_produto' => 3310,
            ),
            31 => 
            array (
                'id' => 32,
                'cod_categoria' => 2,
                'cod_produto' => 3311,
            ),
            32 => 
            array (
                'id' => 33,
                'cod_categoria' => 2,
                'cod_produto' => 3312,
            ),
            33 => 
            array (
                'id' => 34,
                'cod_categoria' => 2,
                'cod_produto' => 3313,
            ),
            34 => 
            array (
                'id' => 35,
                'cod_categoria' => 2,
                'cod_produto' => 3314,
            ),
            35 => 
            array (
                'id' => 36,
                'cod_categoria' => 2,
                'cod_produto' => 3315,
            ),
            36 => 
            array (
                'id' => 37,
                'cod_categoria' => 2,
                'cod_produto' => 3316,
            ),
            37 => 
            array (
                'id' => 38,
                'cod_categoria' => 2,
                'cod_produto' => 3317,
            ),
            38 => 
            array (
                'id' => 39,
                'cod_categoria' => 2,
                'cod_produto' => 3318,
            ),
            39 => 
            array (
                'id' => 40,
                'cod_categoria' => 2,
                'cod_produto' => 3319,
            ),
            40 => 
            array (
                'id' => 41,
                'cod_categoria' => 2,
                'cod_produto' => 3320,
            ),
            41 => 
            array (
                'id' => 42,
                'cod_categoria' => 2,
                'cod_produto' => 3321,
            ),
            42 => 
            array (
                'id' => 43,
                'cod_categoria' => 2,
                'cod_produto' => 3322,
            ),
            43 => 
            array (
                'id' => 44,
                'cod_categoria' => 2,
                'cod_produto' => 3323,
            ),
            44 => 
            array (
                'id' => 45,
                'cod_categoria' => 2,
                'cod_produto' => 3324,
            ),
            45 => 
            array (
                'id' => 46,
                'cod_categoria' => 2,
                'cod_produto' => 3325,
            ),
            46 => 
            array (
                'id' => 47,
                'cod_categoria' => 2,
                'cod_produto' => 3326,
            ),
            47 => 
            array (
                'id' => 48,
                'cod_categoria' => 2,
                'cod_produto' => 3327,
            ),
            48 => 
            array (
                'id' => 49,
                'cod_categoria' => 2,
                'cod_produto' => 3328,
            ),
            49 => 
            array (
                'id' => 50,
                'cod_categoria' => 2,
                'cod_produto' => 3329,
            ),
            50 => 
            array (
                'id' => 51,
                'cod_categoria' => 2,
                'cod_produto' => 3330,
            ),
            51 => 
            array (
                'id' => 52,
                'cod_categoria' => 2,
                'cod_produto' => 3331,
            ),
            52 => 
            array (
                'id' => 53,
                'cod_categoria' => 2,
                'cod_produto' => 3332,
            ),
            53 => 
            array (
                'id' => 54,
                'cod_categoria' => 2,
                'cod_produto' => 3333,
            ),
            54 => 
            array (
                'id' => 55,
                'cod_categoria' => 2,
                'cod_produto' => 3334,
            ),
            55 => 
            array (
                'id' => 56,
                'cod_categoria' => 2,
                'cod_produto' => 3335,
            ),
            56 => 
            array (
                'id' => 57,
                'cod_categoria' => 2,
                'cod_produto' => 3336,
            ),
            57 => 
            array (
                'id' => 58,
                'cod_categoria' => 2,
                'cod_produto' => 3337,
            ),
            58 => 
            array (
                'id' => 59,
                'cod_categoria' => 2,
                'cod_produto' => 3338,
            ),
            59 => 
            array (
                'id' => 60,
                'cod_categoria' => 2,
                'cod_produto' => 3339,
            ),
            60 => 
            array (
                'id' => 61,
                'cod_categoria' => 2,
                'cod_produto' => 3340,
            ),
            61 => 
            array (
                'id' => 62,
                'cod_categoria' => 2,
                'cod_produto' => 3341,
            ),
            62 => 
            array (
                'id' => 63,
                'cod_categoria' => 2,
                'cod_produto' => 3342,
            ),
            63 => 
            array (
                'id' => 64,
                'cod_categoria' => 2,
                'cod_produto' => 3343,
            ),
            64 => 
            array (
                'id' => 65,
                'cod_categoria' => 2,
                'cod_produto' => 3344,
            ),
            65 => 
            array (
                'id' => 66,
                'cod_categoria' => 2,
                'cod_produto' => 3345,
            ),
            66 => 
            array (
                'id' => 67,
                'cod_categoria' => 2,
                'cod_produto' => 3423,
            ),
            67 => 
            array (
                'id' => 68,
                'cod_categoria' => 2,
                'cod_produto' => 3646,
            ),
            68 => 
            array (
                'id' => 69,
                'cod_categoria' => 2,
                'cod_produto' => 3647,
            ),
            69 => 
            array (
                'id' => 70,
                'cod_categoria' => 3,
                'cod_produto' => 3379,
            ),
            70 => 
            array (
                'id' => 71,
                'cod_categoria' => 3,
                'cod_produto' => 3380,
            ),
            71 => 
            array (
                'id' => 72,
                'cod_categoria' => 3,
                'cod_produto' => 3381,
            ),
            72 => 
            array (
                'id' => 73,
                'cod_categoria' => 3,
                'cod_produto' => 3382,
            ),
            73 => 
            array (
                'id' => 74,
                'cod_categoria' => 3,
                'cod_produto' => 3383,
            ),
            74 => 
            array (
                'id' => 75,
                'cod_categoria' => 3,
                'cod_produto' => 3384,
            ),
            75 => 
            array (
                'id' => 76,
                'cod_categoria' => 3,
                'cod_produto' => 3385,
            ),
            76 => 
            array (
                'id' => 77,
                'cod_categoria' => 3,
                'cod_produto' => 3386,
            ),
            77 => 
            array (
                'id' => 78,
                'cod_categoria' => 3,
                'cod_produto' => 3387,
            ),
            78 => 
            array (
                'id' => 79,
                'cod_categoria' => 3,
                'cod_produto' => 3388,
            ),
            79 => 
            array (
                'id' => 80,
                'cod_categoria' => 3,
                'cod_produto' => 3389,
            ),
            80 => 
            array (
                'id' => 81,
                'cod_categoria' => 3,
                'cod_produto' => 3390,
            ),
            81 => 
            array (
                'id' => 82,
                'cod_categoria' => 3,
                'cod_produto' => 3391,
            ),
            82 => 
            array (
                'id' => 83,
                'cod_categoria' => 3,
                'cod_produto' => 3392,
            ),
            83 => 
            array (
                'id' => 84,
                'cod_categoria' => 3,
                'cod_produto' => 3393,
            ),
            84 => 
            array (
                'id' => 85,
                'cod_categoria' => 3,
                'cod_produto' => 3394,
            ),
            85 => 
            array (
                'id' => 86,
                'cod_categoria' => 3,
                'cod_produto' => 3395,
            ),
            86 => 
            array (
                'id' => 87,
                'cod_categoria' => 3,
                'cod_produto' => 3396,
            ),
            87 => 
            array (
                'id' => 88,
                'cod_categoria' => 3,
                'cod_produto' => 3397,
            ),
            88 => 
            array (
                'id' => 89,
                'cod_categoria' => 3,
                'cod_produto' => 3398,
            ),
            89 => 
            array (
                'id' => 90,
                'cod_categoria' => 3,
                'cod_produto' => 3399,
            ),
            90 => 
            array (
                'id' => 91,
                'cod_categoria' => 3,
                'cod_produto' => 3400,
            ),
            91 => 
            array (
                'id' => 92,
                'cod_categoria' => 3,
                'cod_produto' => 3401,
            ),
            92 => 
            array (
                'id' => 93,
                'cod_categoria' => 3,
                'cod_produto' => 3402,
            ),
            93 => 
            array (
                'id' => 94,
                'cod_categoria' => 3,
                'cod_produto' => 3403,
            ),
            94 => 
            array (
                'id' => 95,
                'cod_categoria' => 3,
                'cod_produto' => 3404,
            ),
            95 => 
            array (
                'id' => 96,
                'cod_categoria' => 3,
                'cod_produto' => 3405,
            ),
            96 => 
            array (
                'id' => 97,
                'cod_categoria' => 3,
                'cod_produto' => 3406,
            ),
            97 => 
            array (
                'id' => 98,
                'cod_categoria' => 3,
                'cod_produto' => 3407,
            ),
            98 => 
            array (
                'id' => 99,
                'cod_categoria' => 3,
                'cod_produto' => 3408,
            ),
            99 => 
            array (
                'id' => 100,
                'cod_categoria' => 3,
                'cod_produto' => 3409,
            ),
            100 => 
            array (
                'id' => 101,
                'cod_categoria' => 3,
                'cod_produto' => 3410,
            ),
            101 => 
            array (
                'id' => 102,
                'cod_categoria' => 3,
                'cod_produto' => 3411,
            ),
            102 => 
            array (
                'id' => 103,
                'cod_categoria' => 3,
                'cod_produto' => 3412,
            ),
            103 => 
            array (
                'id' => 104,
                'cod_categoria' => 3,
                'cod_produto' => 3413,
            ),
            104 => 
            array (
                'id' => 105,
                'cod_categoria' => 3,
                'cod_produto' => 3414,
            ),
            105 => 
            array (
                'id' => 106,
                'cod_categoria' => 3,
                'cod_produto' => 3415,
            ),
            106 => 
            array (
                'id' => 107,
                'cod_categoria' => 3,
                'cod_produto' => 3416,
            ),
            107 => 
            array (
                'id' => 108,
                'cod_categoria' => 3,
                'cod_produto' => 3417,
            ),
            108 => 
            array (
                'id' => 109,
                'cod_categoria' => 3,
                'cod_produto' => 3432,
            ),
            109 => 
            array (
                'id' => 110,
                'cod_categoria' => 3,
                'cod_produto' => 3433,
            ),
            110 => 
            array (
                'id' => 111,
                'cod_categoria' => 3,
                'cod_produto' => 3662,
            ),
            111 => 
            array (
                'id' => 112,
                'cod_categoria' => 3,
                'cod_produto' => 3663,
            ),
            112 => 
            array (
                'id' => 113,
                'cod_categoria' => 3,
                'cod_produto' => 3664,
            ),
            113 => 
            array (
                'id' => 114,
                'cod_categoria' => 3,
                'cod_produto' => 3665,
            ),
            114 => 
            array (
                'id' => 115,
                'cod_categoria' => 3,
                'cod_produto' => 3670,
            ),
            115 => 
            array (
                'id' => 116,
                'cod_categoria' => 4,
                'cod_produto' => 3626,
            ),
            116 => 
            array (
                'id' => 117,
                'cod_categoria' => 4,
                'cod_produto' => 3627,
            ),
            117 => 
            array (
                'id' => 118,
                'cod_categoria' => 4,
                'cod_produto' => 3628,
            ),
            118 => 
            array (
                'id' => 119,
                'cod_categoria' => 4,
                'cod_produto' => 3629,
            ),
            119 => 
            array (
                'id' => 120,
                'cod_categoria' => 4,
                'cod_produto' => 3630,
            ),
            120 => 
            array (
                'id' => 121,
                'cod_categoria' => 4,
                'cod_produto' => 3631,
            ),
            121 => 
            array (
                'id' => 122,
                'cod_categoria' => 4,
                'cod_produto' => 3632,
            ),
            122 => 
            array (
                'id' => 123,
                'cod_categoria' => 4,
                'cod_produto' => 3633,
            ),
            123 => 
            array (
                'id' => 124,
                'cod_categoria' => 4,
                'cod_produto' => 3634,
            ),
            124 => 
            array (
                'id' => 125,
                'cod_categoria' => 4,
                'cod_produto' => 3635,
            ),
            125 => 
            array (
                'id' => 126,
                'cod_categoria' => 4,
                'cod_produto' => 3636,
            ),
            126 => 
            array (
                'id' => 127,
                'cod_categoria' => 4,
                'cod_produto' => 3637,
            ),
            127 => 
            array (
                'id' => 128,
                'cod_categoria' => 4,
                'cod_produto' => 3638,
            ),
            128 => 
            array (
                'id' => 129,
                'cod_categoria' => 4,
                'cod_produto' => 3718,
            ),
            129 => 
            array (
                'id' => 130,
                'cod_categoria' => 4,
                'cod_produto' => 3719,
            ),
            130 => 
            array (
                'id' => 131,
                'cod_categoria' => 4,
                'cod_produto' => 3720,
            ),
            131 => 
            array (
                'id' => 132,
                'cod_categoria' => 4,
                'cod_produto' => 3724,
            ),
            132 => 
            array (
                'id' => 133,
                'cod_categoria' => 5,
                'cod_produto' => 3379,
            ),
            133 => 
            array (
                'id' => 134,
                'cod_categoria' => 5,
                'cod_produto' => 3380,
            ),
            134 => 
            array (
                'id' => 135,
                'cod_categoria' => 5,
                'cod_produto' => 3381,
            ),
            135 => 
            array (
                'id' => 136,
                'cod_categoria' => 5,
                'cod_produto' => 3415,
            ),
            136 => 
            array (
                'id' => 137,
                'cod_categoria' => 5,
                'cod_produto' => 3416,
            ),
            137 => 
            array (
                'id' => 138,
                'cod_categoria' => 5,
                'cod_produto' => 3417,
            ),
            138 => 
            array (
                'id' => 139,
                'cod_categoria' => 5,
                'cod_produto' => 3420,
            ),
            139 => 
            array (
                'id' => 140,
                'cod_categoria' => 5,
                'cod_produto' => 3421,
            ),
            140 => 
            array (
                'id' => 141,
                'cod_categoria' => 5,
                'cod_produto' => 3422,
            ),
            141 => 
            array (
                'id' => 142,
                'cod_categoria' => 5,
                'cod_produto' => 3424,
            ),
            142 => 
            array (
                'id' => 143,
                'cod_categoria' => 5,
                'cod_produto' => 3425,
            ),
            143 => 
            array (
                'id' => 144,
                'cod_categoria' => 5,
                'cod_produto' => 3426,
            ),
            144 => 
            array (
                'id' => 145,
                'cod_categoria' => 5,
                'cod_produto' => 3427,
            ),
            145 => 
            array (
                'id' => 146,
                'cod_categoria' => 5,
                'cod_produto' => 3428,
            ),
            146 => 
            array (
                'id' => 147,
                'cod_categoria' => 5,
                'cod_produto' => 3429,
            ),
            147 => 
            array (
                'id' => 148,
                'cod_categoria' => 5,
                'cod_produto' => 3430,
            ),
            148 => 
            array (
                'id' => 149,
                'cod_categoria' => 5,
                'cod_produto' => 3431,
            ),
            149 => 
            array (
                'id' => 150,
                'cod_categoria' => 5,
                'cod_produto' => 3432,
            ),
            150 => 
            array (
                'id' => 151,
                'cod_categoria' => 5,
                'cod_produto' => 3433,
            ),
            151 => 
            array (
                'id' => 152,
                'cod_categoria' => 5,
                'cod_produto' => 3434,
            ),
            152 => 
            array (
                'id' => 153,
                'cod_categoria' => 5,
                'cod_produto' => 3666,
            ),
            153 => 
            array (
                'id' => 154,
                'cod_categoria' => 5,
                'cod_produto' => 3667,
            ),
            154 => 
            array (
                'id' => 155,
                'cod_categoria' => 5,
                'cod_produto' => 3668,
            ),
            155 => 
            array (
                'id' => 156,
                'cod_categoria' => 5,
                'cod_produto' => 3669,
            ),
            156 => 
            array (
                'id' => 157,
                'cod_categoria' => 5,
                'cod_produto' => 3670,
            ),
            157 => 
            array (
                'id' => 158,
                'cod_categoria' => 6,
                'cod_produto' => 3435,
            ),
            158 => 
            array (
                'id' => 159,
                'cod_categoria' => 6,
                'cod_produto' => 3436,
            ),
            159 => 
            array (
                'id' => 160,
                'cod_categoria' => 6,
                'cod_produto' => 3437,
            ),
            160 => 
            array (
                'id' => 161,
                'cod_categoria' => 6,
                'cod_produto' => 3438,
            ),
            161 => 
            array (
                'id' => 162,
                'cod_categoria' => 6,
                'cod_produto' => 3439,
            ),
            162 => 
            array (
                'id' => 163,
                'cod_categoria' => 6,
                'cod_produto' => 3440,
            ),
            163 => 
            array (
                'id' => 164,
                'cod_categoria' => 6,
                'cod_produto' => 3441,
            ),
            164 => 
            array (
                'id' => 165,
                'cod_categoria' => 6,
                'cod_produto' => 3442,
            ),
            165 => 
            array (
                'id' => 166,
                'cod_categoria' => 6,
                'cod_produto' => 3443,
            ),
            166 => 
            array (
                'id' => 167,
                'cod_categoria' => 6,
                'cod_produto' => 3444,
            ),
            167 => 
            array (
                'id' => 168,
                'cod_categoria' => 6,
                'cod_produto' => 3445,
            ),
            168 => 
            array (
                'id' => 169,
                'cod_categoria' => 6,
                'cod_produto' => 3446,
            ),
            169 => 
            array (
                'id' => 170,
                'cod_categoria' => 6,
                'cod_produto' => 3447,
            ),
            170 => 
            array (
                'id' => 171,
                'cod_categoria' => 6,
                'cod_produto' => 3448,
            ),
            171 => 
            array (
                'id' => 172,
                'cod_categoria' => 6,
                'cod_produto' => 3449,
            ),
            172 => 
            array (
                'id' => 173,
                'cod_categoria' => 6,
                'cod_produto' => 3450,
            ),
            173 => 
            array (
                'id' => 174,
                'cod_categoria' => 6,
                'cod_produto' => 3451,
            ),
            174 => 
            array (
                'id' => 175,
                'cod_categoria' => 6,
                'cod_produto' => 3671,
            ),
            175 => 
            array (
                'id' => 176,
                'cod_categoria' => 6,
                'cod_produto' => 3672,
            ),
            176 => 
            array (
                'id' => 177,
                'cod_categoria' => 7,
                'cod_produto' => 3458,
            ),
            177 => 
            array (
                'id' => 178,
                'cod_categoria' => 7,
                'cod_produto' => 3459,
            ),
            178 => 
            array (
                'id' => 179,
                'cod_categoria' => 7,
                'cod_produto' => 3460,
            ),
            179 => 
            array (
                'id' => 180,
                'cod_categoria' => 7,
                'cod_produto' => 3461,
            ),
            180 => 
            array (
                'id' => 181,
                'cod_categoria' => 7,
                'cod_produto' => 3462,
            ),
            181 => 
            array (
                'id' => 182,
                'cod_categoria' => 7,
                'cod_produto' => 3463,
            ),
            182 => 
            array (
                'id' => 183,
                'cod_categoria' => 7,
                'cod_produto' => 3464,
            ),
            183 => 
            array (
                'id' => 184,
                'cod_categoria' => 7,
                'cod_produto' => 3465,
            ),
            184 => 
            array (
                'id' => 185,
                'cod_categoria' => 7,
                'cod_produto' => 3466,
            ),
            185 => 
            array (
                'id' => 186,
                'cod_categoria' => 7,
                'cod_produto' => 3467,
            ),
            186 => 
            array (
                'id' => 187,
                'cod_categoria' => 7,
                'cod_produto' => 3468,
            ),
            187 => 
            array (
                'id' => 188,
                'cod_categoria' => 7,
                'cod_produto' => 3469,
            ),
            188 => 
            array (
                'id' => 189,
                'cod_categoria' => 7,
                'cod_produto' => 3470,
            ),
            189 => 
            array (
                'id' => 190,
                'cod_categoria' => 7,
                'cod_produto' => 3471,
            ),
            190 => 
            array (
                'id' => 191,
                'cod_categoria' => 7,
                'cod_produto' => 3472,
            ),
            191 => 
            array (
                'id' => 192,
                'cod_categoria' => 7,
                'cod_produto' => 3473,
            ),
            192 => 
            array (
                'id' => 193,
                'cod_categoria' => 7,
                'cod_produto' => 3474,
            ),
            193 => 
            array (
                'id' => 194,
                'cod_categoria' => 7,
                'cod_produto' => 3475,
            ),
            194 => 
            array (
                'id' => 195,
                'cod_categoria' => 7,
                'cod_produto' => 3476,
            ),
            195 => 
            array (
                'id' => 196,
                'cod_categoria' => 7,
                'cod_produto' => 3477,
            ),
            196 => 
            array (
                'id' => 197,
                'cod_categoria' => 7,
                'cod_produto' => 3478,
            ),
            197 => 
            array (
                'id' => 198,
                'cod_categoria' => 7,
                'cod_produto' => 3479,
            ),
            198 => 
            array (
                'id' => 199,
                'cod_categoria' => 7,
                'cod_produto' => 3673,
            ),
            199 => 
            array (
                'id' => 200,
                'cod_categoria' => 7,
                'cod_produto' => 3674,
            ),
            200 => 
            array (
                'id' => 201,
                'cod_categoria' => 7,
                'cod_produto' => 3675,
            ),
            201 => 
            array (
                'id' => 202,
                'cod_categoria' => 7,
                'cod_produto' => 3676,
            ),
            202 => 
            array (
                'id' => 203,
                'cod_categoria' => 7,
                'cod_produto' => 3677,
            ),
            203 => 
            array (
                'id' => 204,
                'cod_categoria' => 7,
                'cod_produto' => 3678,
            ),
            204 => 
            array (
                'id' => 205,
                'cod_categoria' => 7,
                'cod_produto' => 3679,
            ),
            205 => 
            array (
                'id' => 206,
                'cod_categoria' => 7,
                'cod_produto' => 3680,
            ),
            206 => 
            array (
                'id' => 207,
                'cod_categoria' => 7,
                'cod_produto' => 3681,
            ),
            207 => 
            array (
                'id' => 208,
                'cod_categoria' => 7,
                'cod_produto' => 3682,
            ),
            208 => 
            array (
                'id' => 209,
                'cod_categoria' => 7,
                'cod_produto' => 3683,
            ),
            209 => 
            array (
                'id' => 210,
                'cod_categoria' => 7,
                'cod_produto' => 3684,
            ),
            210 => 
            array (
                'id' => 211,
                'cod_categoria' => 7,
                'cod_produto' => 3685,
            ),
            211 => 
            array (
                'id' => 212,
                'cod_categoria' => 7,
                'cod_produto' => 3686,
            ),
            212 => 
            array (
                'id' => 213,
                'cod_categoria' => 7,
                'cod_produto' => 3687,
            ),
            213 => 
            array (
                'id' => 214,
                'cod_categoria' => 7,
                'cod_produto' => 3688,
            ),
            214 => 
            array (
                'id' => 215,
                'cod_categoria' => 7,
                'cod_produto' => 3689,
            ),
            215 => 
            array (
                'id' => 216,
                'cod_categoria' => 7,
                'cod_produto' => 3690,
            ),
            216 => 
            array (
                'id' => 217,
                'cod_categoria' => 7,
                'cod_produto' => 3691,
            ),
            217 => 
            array (
                'id' => 218,
                'cod_categoria' => 7,
                'cod_produto' => 3692,
            ),
            218 => 
            array (
                'id' => 219,
                'cod_categoria' => 7,
                'cod_produto' => 3693,
            ),
            219 => 
            array (
                'id' => 220,
                'cod_categoria' => 7,
                'cod_produto' => 3694,
            ),
            220 => 
            array (
                'id' => 221,
                'cod_categoria' => 7,
                'cod_produto' => 3695,
            ),
            221 => 
            array (
                'id' => 222,
                'cod_categoria' => 7,
                'cod_produto' => 3696,
            ),
            222 => 
            array (
                'id' => 223,
                'cod_categoria' => 7,
                'cod_produto' => 3727,
            ),
            223 => 
            array (
                'id' => 224,
                'cod_categoria' => 7,
                'cod_produto' => 3733,
            ),
            224 => 
            array (
                'id' => 225,
                'cod_categoria' => 8,
                'cod_produto' => 3505,
            ),
            225 => 
            array (
                'id' => 226,
                'cod_categoria' => 8,
                'cod_produto' => 3506,
            ),
            226 => 
            array (
                'id' => 227,
                'cod_categoria' => 8,
                'cod_produto' => 3507,
            ),
            227 => 
            array (
                'id' => 228,
                'cod_categoria' => 8,
                'cod_produto' => 3508,
            ),
            228 => 
            array (
                'id' => 229,
                'cod_categoria' => 8,
                'cod_produto' => 3509,
            ),
            229 => 
            array (
                'id' => 230,
                'cod_categoria' => 8,
                'cod_produto' => 3510,
            ),
            230 => 
            array (
                'id' => 231,
                'cod_categoria' => 8,
                'cod_produto' => 3511,
            ),
            231 => 
            array (
                'id' => 232,
                'cod_categoria' => 8,
                'cod_produto' => 3512,
            ),
            232 => 
            array (
                'id' => 233,
                'cod_categoria' => 8,
                'cod_produto' => 3513,
            ),
            233 => 
            array (
                'id' => 234,
                'cod_categoria' => 8,
                'cod_produto' => 3514,
            ),
            234 => 
            array (
                'id' => 235,
                'cod_categoria' => 8,
                'cod_produto' => 3515,
            ),
            235 => 
            array (
                'id' => 236,
                'cod_categoria' => 8,
                'cod_produto' => 3516,
            ),
            236 => 
            array (
                'id' => 237,
                'cod_categoria' => 8,
                'cod_produto' => 3517,
            ),
            237 => 
            array (
                'id' => 238,
                'cod_categoria' => 8,
                'cod_produto' => 3518,
            ),
            238 => 
            array (
                'id' => 239,
                'cod_categoria' => 8,
                'cod_produto' => 3519,
            ),
            239 => 
            array (
                'id' => 240,
                'cod_categoria' => 8,
                'cod_produto' => 3520,
            ),
            240 => 
            array (
                'id' => 241,
                'cod_categoria' => 8,
                'cod_produto' => 3521,
            ),
            241 => 
            array (
                'id' => 242,
                'cod_categoria' => 8,
                'cod_produto' => 3522,
            ),
            242 => 
            array (
                'id' => 243,
                'cod_categoria' => 8,
                'cod_produto' => 3523,
            ),
            243 => 
            array (
                'id' => 244,
                'cod_categoria' => 8,
                'cod_produto' => 3524,
            ),
            244 => 
            array (
                'id' => 245,
                'cod_categoria' => 8,
                'cod_produto' => 3525,
            ),
            245 => 
            array (
                'id' => 246,
                'cod_categoria' => 8,
                'cod_produto' => 3526,
            ),
            246 => 
            array (
                'id' => 247,
                'cod_categoria' => 8,
                'cod_produto' => 3527,
            ),
            247 => 
            array (
                'id' => 248,
                'cod_categoria' => 8,
                'cod_produto' => 3528,
            ),
            248 => 
            array (
                'id' => 249,
                'cod_categoria' => 8,
                'cod_produto' => 3529,
            ),
            249 => 
            array (
                'id' => 250,
                'cod_categoria' => 8,
                'cod_produto' => 3530,
            ),
            250 => 
            array (
                'id' => 251,
                'cod_categoria' => 8,
                'cod_produto' => 3531,
            ),
            251 => 
            array (
                'id' => 252,
                'cod_categoria' => 8,
                'cod_produto' => 3532,
            ),
            252 => 
            array (
                'id' => 253,
                'cod_categoria' => 8,
                'cod_produto' => 3533,
            ),
            253 => 
            array (
                'id' => 254,
                'cod_categoria' => 8,
                'cod_produto' => 3534,
            ),
            254 => 
            array (
                'id' => 255,
                'cod_categoria' => 8,
                'cod_produto' => 3535,
            ),
            255 => 
            array (
                'id' => 256,
                'cod_categoria' => 8,
                'cod_produto' => 3536,
            ),
            256 => 
            array (
                'id' => 257,
                'cod_categoria' => 8,
                'cod_produto' => 3537,
            ),
            257 => 
            array (
                'id' => 258,
                'cod_categoria' => 8,
                'cod_produto' => 3538,
            ),
            258 => 
            array (
                'id' => 259,
                'cod_categoria' => 8,
                'cod_produto' => 3539,
            ),
            259 => 
            array (
                'id' => 260,
                'cod_categoria' => 8,
                'cod_produto' => 3540,
            ),
            260 => 
            array (
                'id' => 261,
                'cod_categoria' => 8,
                'cod_produto' => 3541,
            ),
            261 => 
            array (
                'id' => 262,
                'cod_categoria' => 8,
                'cod_produto' => 3542,
            ),
            262 => 
            array (
                'id' => 263,
                'cod_categoria' => 8,
                'cod_produto' => 3543,
            ),
            263 => 
            array (
                'id' => 264,
                'cod_categoria' => 8,
                'cod_produto' => 3697,
            ),
            264 => 
            array (
                'id' => 265,
                'cod_categoria' => 8,
                'cod_produto' => 3698,
            ),
            265 => 
            array (
                'id' => 266,
                'cod_categoria' => 8,
                'cod_produto' => 3699,
            ),
            266 => 
            array (
                'id' => 267,
                'cod_categoria' => 8,
                'cod_produto' => 3700,
            ),
            267 => 
            array (
                'id' => 268,
                'cod_categoria' => 8,
                'cod_produto' => 3701,
            ),
            268 => 
            array (
                'id' => 269,
                'cod_categoria' => 8,
                'cod_produto' => 3702,
            ),
            269 => 
            array (
                'id' => 270,
                'cod_categoria' => 8,
                'cod_produto' => 3703,
            ),
            270 => 
            array (
                'id' => 271,
                'cod_categoria' => 8,
                'cod_produto' => 3704,
            ),
            271 => 
            array (
                'id' => 272,
                'cod_categoria' => 8,
                'cod_produto' => 3728,
            ),
            272 => 
            array (
                'id' => 273,
                'cod_categoria' => 9,
                'cod_produto' => 3382,
            ),
            273 => 
            array (
                'id' => 274,
                'cod_categoria' => 9,
                'cod_produto' => 3434,
            ),
            274 => 
            array (
                'id' => 275,
                'cod_categoria' => 9,
                'cod_produto' => 3480,
            ),
            275 => 
            array (
                'id' => 276,
                'cod_categoria' => 9,
                'cod_produto' => 3481,
            ),
            276 => 
            array (
                'id' => 277,
                'cod_categoria' => 9,
                'cod_produto' => 3482,
            ),
            277 => 
            array (
                'id' => 278,
                'cod_categoria' => 9,
                'cod_produto' => 3483,
            ),
            278 => 
            array (
                'id' => 279,
                'cod_categoria' => 9,
                'cod_produto' => 3484,
            ),
            279 => 
            array (
                'id' => 280,
                'cod_categoria' => 9,
                'cod_produto' => 3485,
            ),
            280 => 
            array (
                'id' => 281,
                'cod_categoria' => 9,
                'cod_produto' => 3486,
            ),
            281 => 
            array (
                'id' => 282,
                'cod_categoria' => 9,
                'cod_produto' => 3487,
            ),
            282 => 
            array (
                'id' => 283,
                'cod_categoria' => 9,
                'cod_produto' => 3488,
            ),
            283 => 
            array (
                'id' => 284,
                'cod_categoria' => 9,
                'cod_produto' => 3489,
            ),
            284 => 
            array (
                'id' => 285,
                'cod_categoria' => 9,
                'cod_produto' => 3490,
            ),
            285 => 
            array (
                'id' => 286,
                'cod_categoria' => 9,
                'cod_produto' => 3491,
            ),
            286 => 
            array (
                'id' => 287,
                'cod_categoria' => 9,
                'cod_produto' => 3492,
            ),
            287 => 
            array (
                'id' => 288,
                'cod_categoria' => 9,
                'cod_produto' => 3493,
            ),
            288 => 
            array (
                'id' => 289,
                'cod_categoria' => 9,
                'cod_produto' => 3494,
            ),
            289 => 
            array (
                'id' => 290,
                'cod_categoria' => 9,
                'cod_produto' => 3495,
            ),
            290 => 
            array (
                'id' => 291,
                'cod_categoria' => 9,
                'cod_produto' => 3496,
            ),
            291 => 
            array (
                'id' => 292,
                'cod_categoria' => 9,
                'cod_produto' => 3497,
            ),
            292 => 
            array (
                'id' => 293,
                'cod_categoria' => 9,
                'cod_produto' => 3498,
            ),
            293 => 
            array (
                'id' => 294,
                'cod_categoria' => 9,
                'cod_produto' => 3499,
            ),
            294 => 
            array (
                'id' => 295,
                'cod_categoria' => 9,
                'cod_produto' => 3500,
            ),
            295 => 
            array (
                'id' => 296,
                'cod_categoria' => 9,
                'cod_produto' => 3501,
            ),
            296 => 
            array (
                'id' => 297,
                'cod_categoria' => 9,
                'cod_produto' => 3502,
            ),
            297 => 
            array (
                'id' => 298,
                'cod_categoria' => 9,
                'cod_produto' => 3503,
            ),
            298 => 
            array (
                'id' => 299,
                'cod_categoria' => 9,
                'cod_produto' => 3504,
            ),
            299 => 
            array (
                'id' => 300,
                'cod_categoria' => 10,
                'cod_produto' => 3548,
            ),
            300 => 
            array (
                'id' => 301,
                'cod_categoria' => 10,
                'cod_produto' => 3549,
            ),
            301 => 
            array (
                'id' => 302,
                'cod_categoria' => 10,
                'cod_produto' => 3550,
            ),
            302 => 
            array (
                'id' => 303,
                'cod_categoria' => 10,
                'cod_produto' => 3551,
            ),
            303 => 
            array (
                'id' => 304,
                'cod_categoria' => 10,
                'cod_produto' => 3552,
            ),
            304 => 
            array (
                'id' => 305,
                'cod_categoria' => 10,
                'cod_produto' => 3553,
            ),
            305 => 
            array (
                'id' => 306,
                'cod_categoria' => 10,
                'cod_produto' => 3554,
            ),
            306 => 
            array (
                'id' => 307,
                'cod_categoria' => 10,
                'cod_produto' => 3555,
            ),
            307 => 
            array (
                'id' => 308,
                'cod_categoria' => 10,
                'cod_produto' => 3556,
            ),
            308 => 
            array (
                'id' => 309,
                'cod_categoria' => 10,
                'cod_produto' => 3557,
            ),
            309 => 
            array (
                'id' => 310,
                'cod_categoria' => 10,
                'cod_produto' => 3558,
            ),
            310 => 
            array (
                'id' => 311,
                'cod_categoria' => 10,
                'cod_produto' => 3559,
            ),
            311 => 
            array (
                'id' => 312,
                'cod_categoria' => 10,
                'cod_produto' => 3560,
            ),
            312 => 
            array (
                'id' => 313,
                'cod_categoria' => 10,
                'cod_produto' => 3561,
            ),
            313 => 
            array (
                'id' => 314,
                'cod_categoria' => 10,
                'cod_produto' => 3562,
            ),
            314 => 
            array (
                'id' => 315,
                'cod_categoria' => 10,
                'cod_produto' => 3563,
            ),
            315 => 
            array (
                'id' => 316,
                'cod_categoria' => 10,
                'cod_produto' => 3564,
            ),
            316 => 
            array (
                'id' => 317,
                'cod_categoria' => 10,
                'cod_produto' => 3565,
            ),
            317 => 
            array (
                'id' => 318,
                'cod_categoria' => 10,
                'cod_produto' => 3566,
            ),
            318 => 
            array (
                'id' => 319,
                'cod_categoria' => 10,
                'cod_produto' => 3567,
            ),
            319 => 
            array (
                'id' => 320,
                'cod_categoria' => 10,
                'cod_produto' => 3568,
            ),
            320 => 
            array (
                'id' => 321,
                'cod_categoria' => 10,
                'cod_produto' => 3569,
            ),
            321 => 
            array (
                'id' => 322,
                'cod_categoria' => 10,
                'cod_produto' => 3570,
            ),
            322 => 
            array (
                'id' => 323,
                'cod_categoria' => 10,
                'cod_produto' => 3571,
            ),
            323 => 
            array (
                'id' => 324,
                'cod_categoria' => 10,
                'cod_produto' => 3572,
            ),
            324 => 
            array (
                'id' => 325,
                'cod_categoria' => 10,
                'cod_produto' => 3573,
            ),
            325 => 
            array (
                'id' => 326,
                'cod_categoria' => 10,
                'cod_produto' => 3574,
            ),
            326 => 
            array (
                'id' => 327,
                'cod_categoria' => 10,
                'cod_produto' => 3575,
            ),
            327 => 
            array (
                'id' => 328,
                'cod_categoria' => 10,
                'cod_produto' => 3576,
            ),
            328 => 
            array (
                'id' => 329,
                'cod_categoria' => 10,
                'cod_produto' => 3577,
            ),
            329 => 
            array (
                'id' => 330,
                'cod_categoria' => 10,
                'cod_produto' => 3578,
            ),
            330 => 
            array (
                'id' => 331,
                'cod_categoria' => 10,
                'cod_produto' => 3579,
            ),
            331 => 
            array (
                'id' => 332,
                'cod_categoria' => 10,
                'cod_produto' => 3580,
            ),
            332 => 
            array (
                'id' => 333,
                'cod_categoria' => 10,
                'cod_produto' => 3581,
            ),
            333 => 
            array (
                'id' => 334,
                'cod_categoria' => 10,
                'cod_produto' => 3582,
            ),
            334 => 
            array (
                'id' => 335,
                'cod_categoria' => 10,
                'cod_produto' => 3583,
            ),
            335 => 
            array (
                'id' => 336,
                'cod_categoria' => 10,
                'cod_produto' => 3584,
            ),
            336 => 
            array (
                'id' => 337,
                'cod_categoria' => 10,
                'cod_produto' => 3585,
            ),
            337 => 
            array (
                'id' => 338,
                'cod_categoria' => 10,
                'cod_produto' => 3586,
            ),
            338 => 
            array (
                'id' => 339,
                'cod_categoria' => 10,
                'cod_produto' => 3587,
            ),
            339 => 
            array (
                'id' => 340,
                'cod_categoria' => 10,
                'cod_produto' => 3588,
            ),
            340 => 
            array (
                'id' => 341,
                'cod_categoria' => 10,
                'cod_produto' => 3589,
            ),
            341 => 
            array (
                'id' => 342,
                'cod_categoria' => 10,
                'cod_produto' => 3590,
            ),
            342 => 
            array (
                'id' => 343,
                'cod_categoria' => 10,
                'cod_produto' => 3591,
            ),
            343 => 
            array (
                'id' => 344,
                'cod_categoria' => 10,
                'cod_produto' => 3592,
            ),
            344 => 
            array (
                'id' => 345,
                'cod_categoria' => 10,
                'cod_produto' => 3593,
            ),
            345 => 
            array (
                'id' => 346,
                'cod_categoria' => 10,
                'cod_produto' => 3594,
            ),
            346 => 
            array (
                'id' => 347,
                'cod_categoria' => 10,
                'cod_produto' => 3707,
            ),
            347 => 
            array (
                'id' => 348,
                'cod_categoria' => 10,
                'cod_produto' => 3708,
            ),
            348 => 
            array (
                'id' => 349,
                'cod_categoria' => 10,
                'cod_produto' => 3709,
            ),
            349 => 
            array (
                'id' => 350,
                'cod_categoria' => 10,
                'cod_produto' => 3710,
            ),
            350 => 
            array (
                'id' => 351,
                'cod_categoria' => 10,
                'cod_produto' => 3711,
            ),
            351 => 
            array (
                'id' => 352,
                'cod_categoria' => 10,
                'cod_produto' => 3721,
            ),
            352 => 
            array (
                'id' => 353,
                'cod_categoria' => 10,
                'cod_produto' => 3722,
            ),
            353 => 
            array (
                'id' => 354,
                'cod_categoria' => 10,
                'cod_produto' => 3723,
            ),
            354 => 
            array (
                'id' => 355,
                'cod_categoria' => 11,
                'cod_produto' => 3595,
            ),
            355 => 
            array (
                'id' => 356,
                'cod_categoria' => 11,
                'cod_produto' => 3596,
            ),
            356 => 
            array (
                'id' => 357,
                'cod_categoria' => 11,
                'cod_produto' => 3597,
            ),
            357 => 
            array (
                'id' => 358,
                'cod_categoria' => 11,
                'cod_produto' => 3598,
            ),
            358 => 
            array (
                'id' => 359,
                'cod_categoria' => 11,
                'cod_produto' => 3599,
            ),
            359 => 
            array (
                'id' => 360,
                'cod_categoria' => 11,
                'cod_produto' => 3600,
            ),
            360 => 
            array (
                'id' => 361,
                'cod_categoria' => 11,
                'cod_produto' => 3601,
            ),
            361 => 
            array (
                'id' => 362,
                'cod_categoria' => 11,
                'cod_produto' => 3602,
            ),
            362 => 
            array (
                'id' => 363,
                'cod_categoria' => 11,
                'cod_produto' => 3603,
            ),
            363 => 
            array (
                'id' => 364,
                'cod_categoria' => 11,
                'cod_produto' => 3604,
            ),
            364 => 
            array (
                'id' => 365,
                'cod_categoria' => 11,
                'cod_produto' => 3605,
            ),
            365 => 
            array (
                'id' => 366,
                'cod_categoria' => 11,
                'cod_produto' => 3606,
            ),
            366 => 
            array (
                'id' => 367,
                'cod_categoria' => 11,
                'cod_produto' => 3607,
            ),
            367 => 
            array (
                'id' => 368,
                'cod_categoria' => 11,
                'cod_produto' => 3608,
            ),
            368 => 
            array (
                'id' => 369,
                'cod_categoria' => 11,
                'cod_produto' => 3609,
            ),
            369 => 
            array (
                'id' => 370,
                'cod_categoria' => 11,
                'cod_produto' => 3610,
            ),
            370 => 
            array (
                'id' => 371,
                'cod_categoria' => 11,
                'cod_produto' => 3611,
            ),
            371 => 
            array (
                'id' => 372,
                'cod_categoria' => 11,
                'cod_produto' => 3612,
            ),
            372 => 
            array (
                'id' => 373,
                'cod_categoria' => 11,
                'cod_produto' => 3613,
            ),
            373 => 
            array (
                'id' => 374,
                'cod_categoria' => 11,
                'cod_produto' => 3614,
            ),
            374 => 
            array (
                'id' => 375,
                'cod_categoria' => 11,
                'cod_produto' => 3615,
            ),
            375 => 
            array (
                'id' => 376,
                'cod_categoria' => 11,
                'cod_produto' => 3616,
            ),
            376 => 
            array (
                'id' => 377,
                'cod_categoria' => 11,
                'cod_produto' => 3617,
            ),
            377 => 
            array (
                'id' => 378,
                'cod_categoria' => 11,
                'cod_produto' => 3618,
            ),
            378 => 
            array (
                'id' => 379,
                'cod_categoria' => 11,
                'cod_produto' => 3619,
            ),
            379 => 
            array (
                'id' => 380,
                'cod_categoria' => 11,
                'cod_produto' => 3620,
            ),
            380 => 
            array (
                'id' => 381,
                'cod_categoria' => 11,
                'cod_produto' => 3621,
            ),
            381 => 
            array (
                'id' => 382,
                'cod_categoria' => 11,
                'cod_produto' => 3622,
            ),
            382 => 
            array (
                'id' => 383,
                'cod_categoria' => 11,
                'cod_produto' => 3623,
            ),
            383 => 
            array (
                'id' => 384,
                'cod_categoria' => 11,
                'cod_produto' => 3624,
            ),
            384 => 
            array (
                'id' => 385,
                'cod_categoria' => 11,
                'cod_produto' => 3625,
            ),
            385 => 
            array (
                'id' => 386,
                'cod_categoria' => 11,
                'cod_produto' => 3712,
            ),
            386 => 
            array (
                'id' => 387,
                'cod_categoria' => 11,
                'cod_produto' => 3713,
            ),
            387 => 
            array (
                'id' => 388,
                'cod_categoria' => 11,
                'cod_produto' => 3714,
            ),
            388 => 
            array (
                'id' => 389,
                'cod_categoria' => 11,
                'cod_produto' => 3715,
            ),
            389 => 
            array (
                'id' => 390,
                'cod_categoria' => 11,
                'cod_produto' => 3716,
            ),
            390 => 
            array (
                'id' => 391,
                'cod_categoria' => 11,
                'cod_produto' => 3717,
            ),
            391 => 
            array (
                'id' => 392,
                'cod_categoria' => 13,
                'cod_produto' => 3346,
            ),
            392 => 
            array (
                'id' => 393,
                'cod_categoria' => 13,
                'cod_produto' => 3347,
            ),
            393 => 
            array (
                'id' => 394,
                'cod_categoria' => 13,
                'cod_produto' => 3348,
            ),
            394 => 
            array (
                'id' => 395,
                'cod_categoria' => 13,
                'cod_produto' => 3349,
            ),
            395 => 
            array (
                'id' => 396,
                'cod_categoria' => 13,
                'cod_produto' => 3350,
            ),
            396 => 
            array (
                'id' => 397,
                'cod_categoria' => 13,
                'cod_produto' => 3351,
            ),
            397 => 
            array (
                'id' => 398,
                'cod_categoria' => 13,
                'cod_produto' => 3356,
            ),
            398 => 
            array (
                'id' => 399,
                'cod_categoria' => 13,
                'cod_produto' => 3357,
            ),
            399 => 
            array (
                'id' => 400,
                'cod_categoria' => 13,
                'cod_produto' => 3358,
            ),
            400 => 
            array (
                'id' => 401,
                'cod_categoria' => 13,
                'cod_produto' => 3359,
            ),
            401 => 
            array (
                'id' => 402,
                'cod_categoria' => 13,
                'cod_produto' => 3360,
            ),
            402 => 
            array (
                'id' => 403,
                'cod_categoria' => 13,
                'cod_produto' => 3361,
            ),
            403 => 
            array (
                'id' => 404,
                'cod_categoria' => 13,
                'cod_produto' => 3362,
            ),
            404 => 
            array (
                'id' => 405,
                'cod_categoria' => 13,
                'cod_produto' => 3363,
            ),
            405 => 
            array (
                'id' => 406,
                'cod_categoria' => 13,
                'cod_produto' => 3364,
            ),
            406 => 
            array (
                'id' => 407,
                'cod_categoria' => 13,
                'cod_produto' => 3365,
            ),
            407 => 
            array (
                'id' => 408,
                'cod_categoria' => 13,
                'cod_produto' => 3366,
            ),
            408 => 
            array (
                'id' => 409,
                'cod_categoria' => 13,
                'cod_produto' => 3367,
            ),
            409 => 
            array (
                'id' => 410,
                'cod_categoria' => 13,
                'cod_produto' => 3368,
            ),
            410 => 
            array (
                'id' => 411,
                'cod_categoria' => 13,
                'cod_produto' => 3648,
            ),
            411 => 
            array (
                'id' => 412,
                'cod_categoria' => 13,
                'cod_produto' => 3649,
            ),
            412 => 
            array (
                'id' => 413,
                'cod_categoria' => 13,
                'cod_produto' => 3650,
            ),
            413 => 
            array (
                'id' => 414,
                'cod_categoria' => 13,
                'cod_produto' => 3651,
            ),
            414 => 
            array (
                'id' => 415,
                'cod_categoria' => 13,
                'cod_produto' => 3652,
            ),
            415 => 
            array (
                'id' => 416,
                'cod_categoria' => 13,
                'cod_produto' => 3653,
            ),
            416 => 
            array (
                'id' => 417,
                'cod_categoria' => 13,
                'cod_produto' => 3654,
            ),
            417 => 
            array (
                'id' => 418,
                'cod_categoria' => 13,
                'cod_produto' => 3655,
            ),
            418 => 
            array (
                'id' => 419,
                'cod_categoria' => 13,
                'cod_produto' => 3656,
            ),
            419 => 
            array (
                'id' => 420,
                'cod_categoria' => 14,
                'cod_produto' => 3639,
            ),
            420 => 
            array (
                'id' => 421,
                'cod_categoria' => 14,
                'cod_produto' => 3640,
            ),
            421 => 
            array (
                'id' => 422,
                'cod_categoria' => 14,
                'cod_produto' => 3641,
            ),
            422 => 
            array (
                'id' => 423,
                'cod_categoria' => 14,
                'cod_produto' => 3642,
            ),
            423 => 
            array (
                'id' => 424,
                'cod_categoria' => 14,
                'cod_produto' => 3643,
            ),
            424 => 
            array (
                'id' => 425,
                'cod_categoria' => 14,
                'cod_produto' => 3644,
            ),
            425 => 
            array (
                'id' => 426,
                'cod_categoria' => 14,
                'cod_produto' => 3645,
            ),
            426 => 
            array (
                'id' => 427,
                'cod_categoria' => 15,
                'cod_produto' => 3544,
            ),
            427 => 
            array (
                'id' => 428,
                'cod_categoria' => 15,
                'cod_produto' => 3545,
            ),
            428 => 
            array (
                'id' => 429,
                'cod_categoria' => 15,
                'cod_produto' => 3546,
            ),
            429 => 
            array (
                'id' => 430,
                'cod_categoria' => 15,
                'cod_produto' => 3547,
            ),
            430 => 
            array (
                'id' => 431,
                'cod_categoria' => 15,
                'cod_produto' => 3705,
            ),
            431 => 
            array (
                'id' => 432,
                'cod_categoria' => 15,
                'cod_produto' => 3706,
            ),
            432 => 
            array (
                'id' => 433,
                'cod_categoria' => 17,
                'cod_produto' => 3418,
            ),
            433 => 
            array (
                'id' => 434,
                'cod_categoria' => 17,
                'cod_produto' => 3419,
            ),
            434 => 
            array (
                'id' => 435,
                'cod_categoria' => 17,
                'cod_produto' => 3729,
            ),
            435 => 
            array (
                'id' => 436,
                'cod_categoria' => 17,
                'cod_produto' => 3730,
            ),
            436 => 
            array (
                'id' => 437,
                'cod_categoria' => 17,
                'cod_produto' => 3731,
            ),
            437 => 
            array (
                'id' => 438,
                'cod_categoria' => 17,
                'cod_produto' => 3732,
            ),
            438 => 
            array (
                'id' => 439,
                'cod_categoria' => 18,
                'cod_produto' => 3369,
            ),
            439 => 
            array (
                'id' => 440,
                'cod_categoria' => 18,
                'cod_produto' => 3370,
            ),
            440 => 
            array (
                'id' => 441,
                'cod_categoria' => 18,
                'cod_produto' => 3371,
            ),
            441 => 
            array (
                'id' => 442,
                'cod_categoria' => 18,
                'cod_produto' => 3372,
            ),
            442 => 
            array (
                'id' => 443,
                'cod_categoria' => 18,
                'cod_produto' => 3373,
            ),
            443 => 
            array (
                'id' => 444,
                'cod_categoria' => 18,
                'cod_produto' => 3374,
            ),
            444 => 
            array (
                'id' => 445,
                'cod_categoria' => 18,
                'cod_produto' => 3375,
            ),
            445 => 
            array (
                'id' => 446,
                'cod_categoria' => 18,
                'cod_produto' => 3376,
            ),
            446 => 
            array (
                'id' => 447,
                'cod_categoria' => 18,
                'cod_produto' => 3377,
            ),
            447 => 
            array (
                'id' => 448,
                'cod_categoria' => 18,
                'cod_produto' => 3378,
            ),
            448 => 
            array (
                'id' => 449,
                'cod_categoria' => 18,
                'cod_produto' => 3657,
            ),
            449 => 
            array (
                'id' => 450,
                'cod_categoria' => 18,
                'cod_produto' => 3658,
            ),
            450 => 
            array (
                'id' => 451,
                'cod_categoria' => 18,
                'cod_produto' => 3659,
            ),
            451 => 
            array (
                'id' => 452,
                'cod_categoria' => 18,
                'cod_produto' => 3725,
            ),
            452 => 
            array (
                'id' => 453,
                'cod_categoria' => 18,
                'cod_produto' => 3726,
            ),
        ));
        
        
    }
}