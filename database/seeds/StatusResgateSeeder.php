<?php

use Illuminate\Database\Seeder;
use App\Models\StatusResgate;

class StatusResgateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StatusResgate::insert([
            ['descricao_status' => 'Aguardando'],
            ['descricao_status' => 'Aprovado'],
            ['descricao_status' => 'A Caminho'],
            ['descricao_status' => 'Entregue'],
            ['descricao_status' => 'Cancelado']
        ]);
    }
}
