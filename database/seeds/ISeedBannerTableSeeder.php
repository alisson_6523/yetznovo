<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ISeedBannerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('banner')->delete();
        
        \DB::table('banner')->insert(array (
            0 => 
            array (
                'cod_banner' => 1,
                'link_banner' => 'banner-10-esportes.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'cod_banner' => 2,
                'link_banner' => 'banner-101-casa-decoracao.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'cod_banner' => 3,
                'link_banner' => 'banner-102-beleza.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'cod_banner' => 4,
                'link_banner' => 'banner-103-carro.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'cod_banner' => 5,
                'link_banner' => 'banner-104-cartoes-tudo.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'cod_banner' => 6,
                'link_banner' => 'banner-105-pascoa.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'cod_banner' => 7,
                'link_banner' => 'banner-107-lampada.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'cod_banner' => 8,
                'link_banner' => 'banner-108-eletronicos.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'cod_banner' => 9,
                'link_banner' => 'banner-109-maes.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'cod_banner' => 10,
                'link_banner' => 'banner-110-namorados.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'cod_banner' => 11,
                'link_banner' => 'banner-111-inverno.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'cod_banner' => 12,
                'link_banner' => 'banner-112-pet.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'cod_banner' => 13,
                'link_banner' => 'banner-113.png',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'cod_banner' => 14,
                'link_banner' => 'banner-71-natal.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'cod_banner' => 15,
                'link_banner' => 'banner-72-papelaria.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'cod_banner' => 16,
                'link_banner' => 'banner-75-portal-recompensas.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'cod_banner' => 17,
                'link_banner' => 'banner-76-app.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'cod_banner' => 18,
                'link_banner' => 'banner-80-natal.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'cod_banner' => 19,
                'link_banner' => 'banner-82-pipoca.jpg',
                'bannerable_type' => NULL,
                'bannerable_id' => NULL,
                'data_entrada' => NULL,
                'data_saida' => NULL,
                'created_at' => Carbon::now(),
                'updated_at' => NULL,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}