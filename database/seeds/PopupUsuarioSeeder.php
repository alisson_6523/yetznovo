<?php

use Illuminate\Database\Seeder;
use App\Models\PopupUsuario;

class PopupUsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $popupUsuario = [
            ['cod_usuario' => '1', 'cod_popup' => '24', 'visualizado' => '0'],
            ['cod_usuario' => '1', 'cod_popup' => '25', 'visualizado' => '0'],
        ];

        PopupUsuario::insert($popupUsuario);


    }
}
