<?php

use Illuminate\Database\Seeder;
use App\Models\Video;
use \Carbon\Carbon;

class VideoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $videos = array(
            array('cod_video' => '12', 'cod_campanha' => '1','titulo' => 'Seguro Crédito Protegido','descricao' => 'Características do Seguro Crédito Protegido, em história da era dos descobrimentos e dias atuais.','foto_capa' => 'credito_protegido_-_descobrimento_do_brasil.jpg','extensao' => 'mp4','link_video' => 'ccs_credito_protegido_alta_qualidade.mp4','data_entrada' => '2019-02-21 21:00:00','data_saida' => '2019-04-22 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '13', 'cod_campanha' => '1','titulo' => 'Previdência Privada','descricao' => 'Características da Previdência Privada, através de história do Velho Oeste e dias atuais.','foto_capa' => 'previdencia_privada_-_a_conquista_da_lua.jpg','extensao' => 'mp4','link_video' => 'ccs_previdencia_alta_qualidade.mp4','data_entrada' => '2019-02-21 21:00:00','data_saida' => '2019-04-22 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '14', 'cod_campanha' => '1','titulo' => 'Seguro Residencial','descricao' => 'Características do Seguro Residencial, através de história ambientada na Roma antiga e dias atuais.','foto_capa' => 'seguro_residencial_-_imperio_romano.jpg','extensao' => 'mp4','link_video' => 'ccs_residencial_alta_qualidade.mp4','data_entrada' => '2019-02-21 21:00:00','data_saida' => '2019-04-22 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '15', 'cod_campanha' => '1','titulo' => 'Seguro de Vida','descricao' => 'Características do Seguro de Vida, através de história ambientada na pré-história e dias atuais','foto_capa' => 'seguro_de_vida_-_pre_historia.jpg','extensao' => 'mp4','link_video' => 'ccs_vida_alta_qualidade.mp4','data_entrada' => '2019-02-21 21:00:00','data_saida' => '2019-04-22 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '20', 'cod_campanha' => '1','titulo' => 'Gugacast','descricao' => 'GugaCast','foto_capa' => 'img-medica.jpg','extensao' => 'mp3','link_video' => 'conversas_secretas_3_-_gugacast_-_s04e03.mp3','data_entrada' => '2019-03-17 21:00:00','data_saida' => '2019-05-09 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '21', 'cod_campanha' => '1','titulo' => 'Teste','descricao' => 'Teste','foto_capa' => '800x600.png','extensao' => 'mp4','link_video' => 'extrato.mp4','data_entrada' => '2019-03-17 21:00:00','data_saida' => '2019-03-17 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '22', 'cod_campanha' => '1','titulo' => 'Seguro de Vida','descricao' => 'Características do Seguro de Vida, através de história ambientada na pré-história e dias atuais.','foto_capa' => 'seguro_de_vida_-_pre_historia.jpg','extensao' => 'mp4','link_video' => 'ccs_vida_alta_qualidade.mp4','data_entrada' => '2019-05-27 21:00:00','data_saida' => '2019-06-27 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '23', 'cod_campanha' => '1','titulo' => 'Seguro Residencial','descricao' => 'Características do Seguro Residencial, através de história ambientada na Roma antiga e dias atuais.','foto_capa' => 'seguro_residencial_-_imperio_romano.jpg','extensao' => 'mp4','link_video' => 'ccs_residencial_alta_qualidade.mp4','data_entrada' => '2019-05-27 21:00:00','data_saida' => '2019-06-27 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '24', 'cod_campanha' => '1','titulo' => 'Seguro Créd Protegido','descricao' => 'Características do Seguro Crédito Protegido, em história da era dos descobrimentos e dias atuais.','foto_capa' => 'credito_protegido_-_descobrimento_do_brasil.jpg','extensao' => 'mp4','link_video' => 'ccs_credito_protegido_alta_qualidade.mp4','data_entrada' => '2019-05-27 21:00:00','data_saida' => '2019-06-27 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '25', 'cod_campanha' => '1','titulo' => 'Previdência Privada','descricao' => 'Características da Previdência Privada, através de história do Velho Oeste e dias atuais.','foto_capa' => 'previdencia_privada_-_a_conquista_da_lua.jpg','extensao' => 'mp4','link_video' => 'ccs_previdencia_alta_qualidade.mp4','data_entrada' => '2019-05-27 21:00:00','data_saida' => '2019-06-27 21:00:00', 'created_at' => Carbon::now()),
            array('cod_video' => '26', 'cod_campanha' => '1','titulo' => 'Capitalização','descricao' => 'Características da Capitalização, em história da época do Primeiro Homem na Lua e dias atuais.','foto_capa' => 'capitalizacao_-_velho_oeste.jpg','extensao' => 'mp4','link_video' => 'ccs_capitalizacao_alta_qualidade.mp4','data_entrada' => '2019-05-27 21:00:00','data_saida' => '2019-06-27 21:00:00', 'created_at' => Carbon::now())
          );
          

        Video::insert($videos);

    }
}
