<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('regulamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cod_campanha');
            $table->string('regulamento');
            $table->string('regulamento_hash');
            $table->string('ilha');
            $table->timestamps();

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('regulamentos');
    }
}
