<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCloumnBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banner', function (Blueprint $table) {
            $table->string('titulo')->nullable();
            $table->string('arquivo');
            $table->renameColumn('link_banner', 'hash_arquivo');
            $table->unsignedInteger('cod_campanha')->nullable();

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');

            $table->dropColumn(['bannerable_type', 'bannerable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banner', function (Blueprint $table) {
            $table->string('bannerable_type')->nullable();
            $table->renameColumn('hash_arquivo', 'link_banner');
            $table->bigInteger('bannerable_id')->nullable();

            $table->dropForeign(['cod_campanha']);
            $table->dropColumn(['titulo', 'arquivo', 'cod_campanha']);
        });
    }
}
