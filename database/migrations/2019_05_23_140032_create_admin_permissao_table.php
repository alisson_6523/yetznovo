<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminPermissaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_permissao', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('perfilId');
            $table->unsignedInteger('paginaId');
            $table->timestamps();

            $table->foreign('perfilId')
                ->references('id')->on('admin_perfil')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('paginaId')
                ->references('id')->on('admin_pagina')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_permissao');
    }
}
