<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'video';

    /**
     * Run the migrations.
     * @table video
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_video');
            $table->unsignedInteger('cod_campanha')->nullable(); // tive que deixar nulo por causa da migracao
            $table->string('titulo');
            $table->text('descricao');
            $table->string('foto_capa');
            $table->string('link_video');
            $table->string('extensao');
            $table->date('data_entrada');
            $table->date('data_saida');
            $table->timestamps();


            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
