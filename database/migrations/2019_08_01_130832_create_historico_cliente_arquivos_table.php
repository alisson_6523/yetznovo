<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoClienteArquivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historico_cliente_arquivos', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('arquivo_id');
            $table->unsignedInteger('adm_id');
            $table->string('operacao');
            $table->timestamps();

            $table->foreign('arquivo_id')
                    ->references('id')->on('cliente_arquivos');

            $table->foreign('adm_id')
                    ->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('historico_cliente_arquivos');
    }
}
