<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacaoGrupoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'notificacao_grupo';

    /**
     * Run the migrations.
     * @table notificacao
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_grupo');
            $table->unsignedInteger('adminId');
            $table->unsignedInteger('cod_lista')->nullable();
            $table->unsignedInteger('cod_categoria')->nullable();
            $table->unsignedInteger('cod_campanha')->nullable();
            $table->string('titulo');
            $table->text('mensagem');
            $table->text('pdvs');
            $table->integer('enviados')->default(0);
            $table->string('link')->nullable();
            $table->timestamps();

            $table->foreign('adminId')
                    ->references('id')->on('admins');

            $table->foreign('cod_categoria')
                    ->references('cod_categoria')->on('notificacao_categoria');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
