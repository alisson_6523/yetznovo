<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupom', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pedidoId');
            $table->unsignedInteger('userId')->nullable();
            $table->string('codigo', 100)->unique();
            $table->integer('pontos');
            $table->timestamp('usado_em')->nullable()->default(null);
            $table->timestamps();


            $table->foreign('userId')
                ->references('id')->on('users');

            $table->foreign('pedidoId')
                ->references('id')->on('cupom_pedido');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupom');
    }
}
