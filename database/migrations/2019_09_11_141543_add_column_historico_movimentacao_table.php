<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnHistoricoMovimentacaoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('historico_movimentacao', function (Blueprint $table) {
            $table->unsignedInteger('cod_campanha')->nullable();

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historico_movimentacao', function (Blueprint $table) {
            $table->dropForeign(['cod_campanha']);
            $table->dropColumn(['cod_campanha']);
        });
    }
}
