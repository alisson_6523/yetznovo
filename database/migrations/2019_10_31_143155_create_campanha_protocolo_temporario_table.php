<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaProtocoloTemporarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campanha_protocolo_temporario', function (Blueprint $table) {
            $table->increments('cod_protocolo');
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_adm_criador');
            $table->unsignedInteger('cod_adm_efetivador')->nullable();
            $table->unsignedInteger('tipo_protocolo');
            $table->string('protocolo');
            $table->integer('total_pontos');
            $table->string('arquivo');
            $table->string('hash_arquivo');
            $table->longText('dados');
            $table->timestamp('data_efetivacao')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cod_campanha')
                ->references('cod_campanha')->on('campanha');

            $table->foreign('tipo_protocolo')
                ->references('cod_tipo')->on('tipo_movimentacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campanha_protocolo_temporario');
    }
}
