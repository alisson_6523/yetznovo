<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaProdutoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'campanha_produto';

    /**
     * Run the migrations.
     * @table campanha_produto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_produto');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha')
                    ->onDelete('cascade');

            $table->foreign('cod_produto')
                    ->references('cod_produto')->on('produto')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
