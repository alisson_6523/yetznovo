<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTipoNotificacaoPopupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notificacao_popup', function (Blueprint $table) {
            $table->unsignedInteger('cod_campanha')->nullable();
            $table->integer('tipo_popup')->default(1)->comment('1:Cliente/2:Campanha/3:Ilha');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notificacao_popup', function (Blueprint $table) {
            $table->dropForeign(['cod_campanha']);
            $table->dropColumn(['cod_campanha', 'tipo_popup']);
        });
    }
}
