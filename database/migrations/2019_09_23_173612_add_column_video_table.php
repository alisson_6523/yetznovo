<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('video', function (Blueprint $table) {
            $table->unsignedInteger('cod_cliente')->nullable();
            $table->integer('tipo_streaming')->default(1)->comment('1:Geral/2:Cliente/3:Campanha/4:Ilha');
            $table->softDeletes();

            $table->foreign('cod_cliente')
                    ->references('id')->on('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('video', function (Blueprint $table) {
            $table->dropForeign(['cod_cliente']);
            $table->dropColumn(['cod_cliente', 'tipo_streaming']);
            $table->dropSoftDeletes();
        });
    }
}
