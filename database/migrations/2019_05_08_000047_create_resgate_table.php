<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResgateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'resgate';

    /**
     * Run the migrations.
     * @table resgate
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_resgate');
            $table->unsignedInteger('cod_usuario');
            $table->unsignedInteger('cod_carrinho');
            $table->unsignedInteger('cod_entrega')->nullable(); // nao achei de onde vem
            $table->unsignedInteger('cod_status');
            $table->unsignedInteger('cod_campanha')->nullable(); // campanha das importacoes
            $table->string('registro', 129)->unique()->nullable();
            $table->integer('valor_resgate')->default('0');
            $table->timestamp('data_entregue')->nullable();
            $table->timestamps();

            $table->foreign('cod_usuario')
                    ->references('id')->on('users');

            $table->foreign('cod_carrinho')
                    ->references('cod_carrinho')->on('carrinho');

            $table->foreign('cod_status')
                    ->references('cod_status')->on('status_resgate');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
