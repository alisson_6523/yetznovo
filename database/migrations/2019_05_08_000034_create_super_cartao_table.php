<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperCartaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_cartao';

    /**
     * Run the migrations.
     * @table super_cartao
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_cartao');
            $table->unsignedInteger('valor_inicial')->nullable();
            $table->unsignedInteger('saldo_cartao')->nullable();
            $table->unsignedInteger('cod_lote');
            $table->unsignedInteger('cod_pedido')->nullable();
            $table->unsignedInteger('status_cartao')->default('1');
            $table->string('codigo_controle', 6)->unique()->nullable();
            $table->string('numero_cartao')->nullable();
            $table->string('pin')->nullable();
            $table->string('checksum_cartao')->nullable();
            $table->timestamp('criacao_cartao')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cod_lote')
                    ->references('cod_lote')->on('super_lote');

            $table->foreign('cod_pedido')
                    ->references('cod_pedido')->on('super_pedido');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
