<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperLoteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_lote';

    /**
     * Run the migrations.
     * @table super_lote
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_lote');
            $table->unsignedInteger('status_lote'); // nao sei da onde vem
            $table->unsignedInteger('cod_adm_criador')->nullable(); // nao sei da onde vem
            $table->string('nome_lote')->nullable();
            $table->string('numero_lote', 6)->unique();
            $table->timestamp('inicio_lote')->nullable();
            $table->timestamp('encerramento_lote')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
