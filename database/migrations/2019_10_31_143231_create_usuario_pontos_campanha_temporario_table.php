<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuarioPontosCampanhaTemporarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuario_pontos_campanha_temporario', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('protocolo_id');
            $table->unsignedInteger('usuario_id');
            $table->unsignedInteger('campanha_id');
            $table->decimal('pontos', 10, 2)->default('0.00');
            $table->timestamps();

            $table->foreign('protocolo_id')
                ->references('cod_protocolo')->on('campanha_protocolo_temporario')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('usuario_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('campanha_id')
                ->references('cod_campanha')->on('campanha')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario_pontos_campanha_temporario');
    }
}
