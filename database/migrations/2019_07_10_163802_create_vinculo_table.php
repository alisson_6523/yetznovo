<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVinculoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vinculo', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('cod_video');
            $table->unsignedInteger('cod_pdv');
            $table->string('ilha');
            $table->timestamps();


            $table->foreign('cod_video')
                    ->references('cod_video')->on('video');

            $table->foreign('cod_pdv')
                    ->references('cod_pdv')->on('cliente_pdv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vinculo');
    }
}
