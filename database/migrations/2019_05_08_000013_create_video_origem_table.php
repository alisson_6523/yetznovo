<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoOrigemTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'video_origem';

    /**
     * Run the migrations.
     * @table video_origem
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('db_controle');
            $table->unsignedInteger('cod_video');
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_pdv');
            $table->text('ilha');


            $table->foreign('cod_video')
                    ->references('cod_video')->on('video');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');

            $table->foreign('cod_pdv')
                    ->references('cod_pdv')->on('cliente_pdv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
