<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupIlhaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'popup_ilha';

    /**
     * Run the migrations.
     * @table popup_ilha
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cod_popup');
            $table->unsignedInteger('cod_pdv');
            $table->string('ilha');
            $table->timestamps();


            $table->foreign('cod_popup')
                    ->references('cod_popup')->on('notificacao_popup');

            $table->foreign('cod_pdv')
                    ->references('cod_pdv')->on('cliente_pdv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
