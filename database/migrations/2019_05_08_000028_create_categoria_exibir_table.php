<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaExibirTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'categoria_exibir';

    /**
     * Run the migrations.
     * @table categoria_exibir
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cod_categoria');
            $table->unsignedInteger('cod_produto');

            $table->foreign('cod_categoria')
                    ->references('cod_categoria_produto')->on('categoria_produto')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('cod_produto')
                    ->references('cod_produto')->on('produto')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
