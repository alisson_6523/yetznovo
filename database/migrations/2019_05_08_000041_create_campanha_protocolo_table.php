<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaProtocoloTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'campanha_protocolo';

    /**
     * Run the migrations.
     * @table campanha_protocolo
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_protocolo');
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_adm_criador');
            $table->unsignedInteger('cod_adm_efetivador')->nullable();
            $table->unsignedInteger('tipo_protocolo');
            $table->string('protocolo');
            $table->integer('total_pontos');
            $table->string('arquivo');
            $table->string('hash_arquivo');
            $table->longText('dados');
            $table->timestamp('data_efetivacao')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');

            $table->foreign('tipo_protocolo')
                    ->references('cod_tipo')->on('tipo_movimentacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
