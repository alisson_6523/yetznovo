<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('popup_usuario', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('cod_usuario');
            $table->unsignedInteger('cod_popup');
            $table->boolean('visualizado');
            
            $table->foreign('cod_usuario')
            ->references('id')->on('users');

            $table->foreign('cod_popup')
            ->references('cod_popup')->on('notificacao_popup');


        });
    }
//  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('popup_usuario');
    }
}
