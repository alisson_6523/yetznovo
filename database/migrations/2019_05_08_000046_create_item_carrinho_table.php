<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemCarrinhoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'item_carrinho';

    /**
     * Run the migrations.
     * @table item_carrinho
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_item');
            $table->unsignedInteger('cod_produto');
            $table->unsignedInteger('cod_carrinho');
            $table->unsignedInteger('cod_status');
            $table->timestamps();

            $table->foreign('cod_produto')
                    ->references('cod_produto')->on('produto');

            $table->foreign('cod_carrinho')
                    ->references('cod_carrinho')->on('carrinho')
                    ->onDelete('cascade');

            $table->foreign('cod_status')
                    ->references('cod_status')->on('status_resgate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
