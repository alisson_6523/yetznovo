<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificacaoStreamingTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'notificacao_streaming';

    /**
     * Run the migrations.
     * @table notificacao_streaming
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('notificacao_id');
            $table->unsignedInteger('cod_streaming'); // nao sei da onde vem
            $table->unsignedInteger('cod_usuario');
            $table->unsignedInteger('visualizado')->default('0');
            $table->timestamps();

            $table->foreign('cod_usuario')
                    ->references('id')->on('users')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
