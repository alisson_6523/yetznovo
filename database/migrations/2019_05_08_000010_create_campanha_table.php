<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'campanha';

    /**
     * Run the migrations.
     * @table campanha
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_campanha');
            $table->unsignedInteger('cliente_id');
            $table->string('nome_campanha');
            $table->string('chave', 40)->unique();
            $table->string('analytics', 40)->nullable();
            $table->boolean('super')->default(FALSE);
            $table->string('logo')->nullable();
            $table->string('logo_hash')->nullable();
            $table->date('dt_inicio');
            $table->date('dt_fim');
            $table->integer('validade_pontos')->default(30);
            $table->string('regulamento')->nullable();
            $table->string('regulamento_hash')->nullable();
            $table->boolean('fale_conosco')->default(FALSE);
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('cliente_id')
                ->references('id')->on('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
