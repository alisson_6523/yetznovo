<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperResgateTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_resgate';

    /**
     * Run the migrations.
     * @table super_resgate
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_resgate');
            $table->unsignedInteger('cod_carrinho')->nullable();
            $table->unsignedInteger('cod_endereco')->nullable();
            $table->unsignedInteger('cod_status')->default('1');
            $table->string('nome_destinatario');
            $table->string('email_destinatario')->nullable();
            $table->string('celular_destinatario');
            $table->string('cpf_destinatario');
            $table->string('registro')->unique()->nullable();
            $table->decimal('valor_resgate', 10, 2)->nullable();
            $table->text('metadado_resgate')->nullable();
            $table->timestamps();


            $table->foreign('cod_carrinho')
                    ->references('cod_carrinho')->on('carrinho');

            $table->foreign('cod_endereco')
                    ->references('cod_endereco')->on('endereco');

            $table->foreign('cod_status')
                    ->references('cod_status')->on('status_resgate');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
