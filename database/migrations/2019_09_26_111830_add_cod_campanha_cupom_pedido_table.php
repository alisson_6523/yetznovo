<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodCampanhaCupomPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('cupom_pedido', function (Blueprint $table) {
            $table->unsignedInteger('cliente_id')->nullable()->change();
            $table->unsignedInteger('campanha_id')->nullable();

            $table->foreign('campanha_id')
                    ->references('cod_campanha')->on('campanha');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cupom_pedido', function (Blueprint $table) {
            $table->unsignedInteger('cliente_id')->nullable(false)->change();
            $table->dropForeign(['campanha_id']);
            $table->dropColumn(['campanha_id']);
        });
    }
}
