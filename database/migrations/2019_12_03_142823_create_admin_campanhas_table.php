<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminCampanhasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_campanhas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_id');
            $table->unsignedInteger('campanha_id');
            $table->timestamps();

            $table->foreign('admin_id')
                ->references('id')->on('admins')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('campanha_id')
                ->references('cod_campanha')->on('campanha')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins_campanhas');
    }
}
