<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLinkItemCarrinhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('item_carrinho', function (Blueprint $table) {
            $table->string('link_voucher')->nullable();
            $table->dateTime('email_enviado')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_carrinho', function (Blueprint $table) {
            $table->dropColumn(['link_voucher', 'email_enviado']);
        });
    }
}
