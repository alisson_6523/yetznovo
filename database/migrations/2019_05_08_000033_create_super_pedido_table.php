<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperPedidoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_pedido';

    /**
     * Run the migrations.
     * @table super_pedido
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_pedido');
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_usuario_atendente');
            $table->unsignedInteger('status_pedido')->default('1');
            $table->unsignedInteger('cod_area')->nullable();                // nao sei da onde vem
            $table->integer('quantidade_cartoes')->default('0');
            $table->string('nome_pedido')->nullable();
            $table->timestamp('data_ativacao')->nullable();
            $table->timestamp('validade')->nullable();
            $table->string('registro_lote')->unique()->nullable();
            $table->decimal('valor_total', 10, 2)->default('0.00');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
