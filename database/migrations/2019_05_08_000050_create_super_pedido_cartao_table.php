<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperPedidoCartaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_pedido_cartao';

    /**
     * Run the migrations.
     * @table super_pedido_cartao
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cod_pedido');
            $table->unsignedInteger('cod_cartao');

            $table->foreign('cod_pedido')
                    ->references('cod_pedido')->on('super_pedido');

            $table->foreign('cod_cartao')
                    ->references('cod_cartao')->on('super_cartao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
