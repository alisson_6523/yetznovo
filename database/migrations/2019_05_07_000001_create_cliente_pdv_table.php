<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientePdvTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'cliente_pdv';

    /**
     * Run the migrations.
     * @table cliente_pdv
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_pdv');
            $table->unsignedInteger('cliente_id');
            $table->unsignedInteger('cod_chave')->unique();
            $table->string('registro')->unique();
            $table->string('nome_pdv');
            $table->string('razao_social');
            $table->string('inscricao_estadual')->nullable();
            $table->string('inscricao_municipal')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('cep');
            $table->string('logradouro');
            $table->string('numero');
            $table->string('complemento')->nullable();
            $table->string('bairro');
            $table->string('cidade');
            $table->string('estado');
            $table->string('telefone');
            $table->string('email');
            $table->string('site')->nullable();
            $table->string('logo')->nullable();
            $table->string('logo_hash')->nullable();
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('estado')
                    ->references('sigla')->on('estado');

            $table->foreign('cliente_id')
                    ->references('id')->on('cliente');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
