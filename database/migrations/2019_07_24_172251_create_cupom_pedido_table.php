<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCupomPedidoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cupom_pedido', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('admin_id');
            $table->unsignedInteger('cliente_id');
            $table->string('nome');
            $table->integer('quantidade');
            $table->integer('pontos');
            $table->string('prefixo', 30);
            $table->timestamps();


            $table->foreign('admin_id')
                ->references('id')->on('admins');

            $table->foreign('cliente_id')
                ->references('id')->on('cliente');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cupom_pedido');
    }
}
