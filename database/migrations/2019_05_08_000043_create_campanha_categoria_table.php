<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaCategoriaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'campanha_categoria';

    /**
     * Run the migrations.
     * @table campanha_categoria
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_categoria');

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha')
                    ->onDelete('cascade');

            $table->foreign('cod_categoria')
                    ->references('cod_categoria_produto')->on('categoria_produto')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
