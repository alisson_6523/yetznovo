<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperCarrinhoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_carrinho';

    /**
     * Run the migrations.
     * @table super_carrinho
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_carrinho');
            $table->unsignedInteger('cod_cartao')->nullable();
            $table->unsignedInteger('status_carrinho')->default('1');

            $table->foreign('cod_cartao')
                    ->references('cod_cartao')->on('super_cartao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
