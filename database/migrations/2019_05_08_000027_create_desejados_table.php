<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesejadosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'desejados';

    /**
     * Run the migrations.
     * @table desejados
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cod_usuario');
            $table->unsignedInteger('cod_produto');

            $table->unique(["cod_usuario", "cod_produto"], 'cod_usuario');

            $table->foreign('cod_produto')
                    ->references('cod_produto')->on('produto')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('cod_usuario')
                    ->references('id')->on('users')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
