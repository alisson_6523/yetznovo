<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampanhaPdvTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'campanha_pdv';

    /**
     * Run the migrations.
     * @table campanha_pdv
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_campanha_pdv'); // O eloquent não deleta quando se tem chaves primarias compostas
            $table->unsignedInteger('cod_campanha');
            $table->unsignedInteger('cod_pdv');
            $table->timestamp('data_vinculo')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('cod_campanha')
                    ->references('cod_campanha')->on('campanha')
                    ->onDelete('cascade');

            $table->foreign('cod_pdv')
                    ->references('cod_pdv')->on('cliente_pdv')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
