<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoricoMovimentacaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'historico_movimentacao';

    /**
     * Run the migrations.
     * @table historico_movimentacao
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_movimentacao');
            $table->unsignedInteger('cod_usuario');
            $table->unsignedInteger('cod_tipo_movimentacao');
            $table->integer('valor'); // valor em yetz
            $table->string('descricao')->nullable();
            $table->string('protocolo')->nullable();
            $table->date('expiracao')->nullable();
            $table->string('referencia')->nullable();
            $table->timestamps();

            $table->foreign('cod_tipo_movimentacao')
                    ->references('cod_tipo')->on('tipo_movimentacao');

            $table->foreign('cod_usuario')
                    ->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
