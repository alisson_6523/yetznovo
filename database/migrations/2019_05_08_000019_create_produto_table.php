<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'produto';

    /**
     * Run the migrations.
     * @table produto
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_produto');
            $table->unsignedInteger('tipo');
            $table->unsignedInteger('cod_categoria_produto')->nullable()->default('0');
            $table->unsignedInteger('cod_bandeira')->nullable(); // nao sei da onde vem
            $table->unsignedInteger('disponibilidade')->default('3')->comment('Padrões: 0 => i 1 => o; 2 => f; 3 => of'); // nao sei da onde vem
            $table->integer('valor_produto');
            $table->string('nome_produto');
            $table->text('descricao_produto')->nullable();
            $table->text('detalhes_produto')->nullable();
            $table->text('informacoes_importantes')->nullable();
            $table->decimal('valor_reais', 10, 2)->default('0.00');
            $table->string('fotos')->nullable();
            $table->string('foto_exemplo')->nullable()->default('index');
            $table->string('video')->nullable();
            $table->string('validade_produto')->nullable();
            $table->tinyInteger('tipo_loja');
            $table->timestamps();
            $table->softDeletes();


            $table->foreign('cod_categoria_produto')
                ->references('cod_categoria_produto')->on('categoria_produto');


            $table->foreign('tipo')
                    ->references('cod_tipo')->on('produto_tipo');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
