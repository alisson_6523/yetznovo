<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('cod_pdv');
            $table->unsignedInteger('status')->default(0);
            $table->unsignedInteger('protocolo_insercao'); // Nao achei a coluna de referencia
            $table->unsignedInteger('nivel_usuario')->default('2'); // Nao achei a coluna de referencia
            $table->string('password')->nullable();
            $table->string('cpf_usuario')->nullable()->unique();
            $table->string('nome_usuario')->nullable();
            $table->date('data_nascimento')->nullable();
            $table->string('apelido')->nullable();
            $table->decimal('pontos_reais', 10, 2)->nullable()->default('0.00');
            $table->string('foto_usuario')->nullable();
            $table->string('email')->nullable();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('sexo')->nullable();
            $table->string('coordenador')->nullable();
            $table->string('supervisor')->nullable();
            $table->string('ilha')->nullable();
            $table->tinyInteger('notificacao_celular')->nullable()->default(0);
            $table->timestamp('data_cadastro')->nullable();
            $table->string('cargo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('cod_pdv')
                    ->references('cod_pdv')->on('cliente_pdv');

            $table->foreign('status')
                    ->references('cod_status')->on('status_usuario');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
