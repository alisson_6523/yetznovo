<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuperMovimentacaoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'super_movimentacao';

    /**
     * Run the migrations.
     * @table super_movimentacao
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('cod_movimentacao');
            $table->unsignedInteger('cod_cartao');
            $table->string('protocolo')->nullable();
            $table->unsignedInteger('tipo');
            $table->decimal('valor', 10, 2);
            $table->string('desc_tipo')->nullable();
            $table->string('descricao')->nullable();
            $table->timestamps();

            $table->foreign('cod_cartao')
                    ->references('cod_cartao')->on('super_cartao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
