let arquivos = {

    excluir:function(id){

        let arquivo = new FormData();

        arquivo.append('id', id);

        swal({
            title: 'Tem certeza?',
            text: "Você não será capaz de reverter isto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#F7A81D',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, deletar!',
            cancelButtonText: 'Cancelar',
            customClass: 'swal-custom'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/sistema/uploads/deletar-arquivo",
                    type: "POST",
                    data: arquivo,
                    async: true,
                    contentType: false,
                    processData: false,
                    timeout: 10000,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        window.location = "/sistema/uploads";
                    },
                    error: function (x, t, m) {
                        console.log("erro=> " + m);
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                });
            }
        });

    }

};

$("#form-cadastro-arquivo").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        cadastro_arquivo_titulo: {
            required: true
        }
    },
    errorPlacement: function (error, element) {
    },
    submitHandler: function (form) {

        if ($("#anexo")[0].files[0] == undefined){
            alert('Insira um arquivo!');
            return;
        }

        let arquivo = new FormData();

        arquivo.append('titulo', $("#cadastro-arquivo-titulo").val());
        arquivo.append('arquivo', $("#anexo")[0].files[0]);

        $.ajax({
            url: "/sistema/uploads/adicionar-arquivo",
            type: "POST",
            data: arquivo,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!arquivos.cadastroOnProgress){
                            arquivos.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-arquivo button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        arquivos.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-arquivo button[type='submit']");
                if (!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });

    }
});
