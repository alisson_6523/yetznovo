let itens = Array.from(document.querySelectorAll('.area-conteudo-modulo .topo .acoes .container-form ul li'))

let menus = Array.from(document.querySelectorAll('.area-conteudo-modulo .topo .acoes .container-form'))

let removeMenus = Array.from(document.querySelectorAll('.area-conteudo-modulo .topo .acoes .container-form ul'))

function removeMenu(){
      removeMenus.forEach(function(menu){
            
            if(menu.classList.value.includes('active')){
                  menu.classList.remove('active')
            }
      })
}

menus.forEach(function(menu){
      menu.addEventListener('click', function(){
            let id = document.querySelector('#'+menu.dataset.id) 
            
            id.classList.toggle('active')

            
      })
})

itens.forEach(function(item){ 
      item.addEventListener('click', function(){
            let id = item.parentElement.dataset.id;

            let text = item.querySelector('p').innerHTML

            item.parentElement.parentElement.querySelector('.container-text-img p').innerHTML = text
      
            document.querySelector('#'+id).value = text 
      })
})

