
let regulamento = {

    novoRegulamento:function(ilha){

        let dados = new FormData();

        dados.append('ilha', ilha);

        $.ajax({
            url: window.location.pathname + "/novo-regulamento",
            type: "POST",
            data: dados,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#campanha").html(data.html);
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo de errado aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });
    },

    excluir:function(id){

        swal({
            title: 'Tem certeza?',
            text: "Você não será capaz de reverter isto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#F7A81D',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, deletar!',
            cancelButtonText: 'Cancelar',
            customClass: 'swal-custom'
        }).then((result) => {
            if (result.value) {
                let dados = new FormData();

                dados.append('id', id);

                $('#loader-regulamento').show();
                $('.btn-excluir-carga').css('pointer-events', 'none');

                $.ajax({
                    url: window.location.pathname + "/excluir-regulamento",
                    type: "POST",
                    data: dados,
                    async: true,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    error: function (x, t, m) {
                        console.log("erro=> " + m);
                        swal({
                            type: 'error',
                            title: 'Ops! Algo de errado aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                }).done(function(data) {
                    $('#loader-regulamento').hide();
                    if(!data.erro){
                        swal({
                            type: 'success',
                            title: 'Regulamento Excluído!',
                        }).then(function () {
                            location.reload();
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: data.mensagem,
                        }).then(function () {
                            location.reload();
                        });
                    }
                });
            }
        });
    }

};


$("#form-cadastrar-regulamento").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_regulamento: 'required'
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let regulamento = new FormData();

        if($("#file-7")[0].files[0] == undefined) {
            swal({
                type: 'error',
                title: 'Espere! Insira o regulamento!',
            });
            return;
        }

        regulamento.append('titulo', $("#titulo").val());
        regulamento.append('ilha', $("#ilha").val());
        regulamento.append('cod_campanha', $("#cod-campanha").val());
        regulamento.append('arquivo', $("#file-7")[0].files[0]);

        $.ajax({
            url: window.location.pathname + "/regulamento/" + $("#ilha").val() + "/cadastrar",
            type: "POST",
            data: regulamento,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!regulamento.cadastroOnProgress){
                            regulamento.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastrar-regulamento button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        regulamento.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastrar-regulamento button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });
    }
});


$("#form-editar-regulamento").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_regulamento: 'required'
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let regulamento = new FormData();

        regulamento.append('id', $("#id-regulamento").val());
        regulamento.append('titulo', $("#titulo").val());

        if($("#file-7")[0].files[0] != undefined)
            regulamento.append('arquivo', $("#file-7")[0].files[0]);


        $.ajax({
            url: window.location.pathname + "/editar",
            type: "POST",
            data: regulamento,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!regulamento.cadastroOnProgress){
                            regulamento.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-editar-regulamento button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        regulamento.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-editar-regulamento button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });
    }
});


$("#form-editar-regulamento-geral").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_regulamento: 'required'
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let regulamento = new FormData();

        regulamento.append('cod_campanha', $("#id-campanha").val());
        regulamento.append('titulo_regulamento', $("#titulo").val());

        if($("#file-7")[0].files[0] != undefined)
            regulamento.append('arquivo', $("#file-7")[0].files[0]);

        $.ajax({
            url: window.location.pathname + "/editar",
            type: "POST",
            data: regulamento,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!regulamento.cadastroOnProgress){
                            regulamento.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-editar-regulamento-geral button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        regulamento.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-editar-regulamento-geral button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });
    }
});
