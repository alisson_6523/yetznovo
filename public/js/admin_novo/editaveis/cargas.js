let carga = {

    salvar:function(temporaria = null){

        let carga = new FormData();
        let url   = "/sistema/campanhas/salvar-carga-de-pontos";

        if(temporaria != null)
            url = "/sistema/campanhas/salvar-carga-de-pontos-temporaria";

        if($("#titulo-carga").val() == ''){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Insira um título para a carga'
            });
            $("#titulo-carga").focus();
            return;
        }

        carga.append('cod_campanha', $("#cod-campanha").val());
        carga.append('nome_arquivo', $("#nome-arquivo").val());
        carga.append('titulo', $("#titulo-carga").val());
        carga.append('dados', sessionStorage.getItem('dados'));

        $.ajax({
            url: url,
            type: "POST",
            data: carga,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                location.reload();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });

    },

    excluir:function(cod_protocolo, temporaria = null){

        let protocolo = new FormData();
        let url       = "/sistema/campanhas/excluir-carga-de-pontos";

        if(temporaria != null)
            url = "/sistema/campanhas/excluir-carga-de-pontos-temporaria";

        protocolo.append('cod_protocolo', cod_protocolo);

        $.ajax({
            url: url,
            type: "POST",
            data: protocolo,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('.loader').show();
            },
            success: function (data) {
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $('.loader').hide();
            location.reload();
        });

    },

    efetivar:function(cod_protocolo, temporaria = null){

        let protocolo = new FormData();
        let url       = "/sistema/campanhas/efetivar-carga-de-pontos";

        if(temporaria != null)
            url = "/sistema/campanhas/efetivar-carga-de-pontos-temporaria";

        $(".btn-efetivar-carga").attr('disabled', true);

        protocolo.append('cod_protocolo', cod_protocolo);

        $('#loader-carga'+(temporaria != null ? '-temporaria' : '')).show();

        $.ajax({
            url: url,
            type: "POST",
            data: protocolo,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $(".area-geral-campanha .cont-geral").css('overflow', 'hidden');
                $(".backdrop"+(temporaria != null ? '-temporaria' : '')).show();
            },
            success: function (data) {
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            if(!data.erro){
                console.log(data);
                $('#loader-carga'+(temporaria != null ? '-temporaria' : '')).hide();
                $(".backdrop"+(temporaria != null ? '-temporaria' : '')).hide();
                location.reload();
            } else {
                console.log(data);
                swal({
                    type: 'error',
                    title: 'Ops! Algo aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }

        });
    }

};

// Checa ext do arquivo
$("#carga-arquivo").change(function(){
    let ext_arquivo = $(this).val().split('.').pop();
    if($("#perfil-adm").val() == 1 || $("#perfil-adm").val() == 2){
        if(ext_arquivo == 'xlsx')
            $("#btn-enviar-planilha").removeClass('disabled');
        else
            $("#btn-enviar-planilha").addClass('disabled');
    }
});

$("#carga-arquivo-temporario").change(function(){
    let ext_arquivo = $(this).val().split('.').pop();
    if($("#perfil-adm").val() == 1 || $("#perfil-adm").val() == 2){
        if(ext_arquivo == 'xlsx')
            $("#btn-enviar-planilha-temporario").removeClass('disabled');
        else
            $("#btn-enviar-planilha-temporario").addClass('disabled');
    }
});


$('#campanha-enviar-planilha').submit(function(e) {
    e.preventDefault();
}).validate({
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        if($("#carga-arquivo")[0].files[0] == null){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Você precisa selecionar o arquivo!'
            });
            return;
        }

        let campanha = new FormData();
        let arquivo  = $("#carga-arquivo")[0].files[0];

        campanha.append('cod_campanha', $("#cod-campanha").val());
        campanha.append('arquivo', arquivo);

        $.ajax({
            url: "/sistema/campanhas/adicionar-carga-de-pontos",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#loader-carga').show();
                $("#btn-enviar-planilha").addClass('disabled');
            },
            success: function (data) {

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $('#loader-carga').hide();
            if(!data.erro){
                sessionStorage.setItem('dados', JSON.stringify(data.dados));
                $("#campanha").html(data.html);
            } else
                alert(data.mensagem);

            $("#btn-enviar-planilha").removeClass('disabled');
        });

    }
});

$('#campanha-enviar-planilha-temporaria').submit(function(e) {
    e.preventDefault();
}).validate({
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        if($("#carga-arquivo-temporario")[0].files[0] == null){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Você precisa selecionar o arquivo!'
            });
            return;
        }

        let campanha = new FormData();
        let arquivo = $("#carga-arquivo-temporario")[0].files[0];

        campanha.append('cod_campanha', $("#cod-campanha").val());
        campanha.append('arquivo', arquivo);

        $.ajax({
            url: "/sistema/campanhas/adicionar-carga-de-pontos-temporaria",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            beforeSend: function() {
                $('#loader-carga-temporaria').show();
                $("#btn-enviar-planilha-temporario").addClass('disabled');
            },
            success: function (data) {

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $('#loader-carga-temporaria').hide();
            if(!data.erro){
                sessionStorage.setItem('dados', JSON.stringify(data.dados));
                $("#campanha").html(data.html);
            } else
                alert(data.mensagem);

            $("#btn-enviar-planilha-temporario").removeClass('disabled');
        });

    }
});
