let itensdetalhesUser = Array.from(document.querySelectorAll('.container-line-user ul'))

function removeDetalheUsuario(){
      itensdetalhesUser.forEach(function(item){
          if(item.classList.value.includes('active')){
            item.classList.remove('active')
            item.style.height =  "55px"
          }
      })
}


let tamanho =  0
itensdetalhesUser.forEach(function(item){
      item.addEventListener('click', function(){
            
            if(item.classList.value.includes('active')){
                  item.classList.remove('active')
                  item.style.height =  "55px"
            }else{
                    removeDetalheUsuario()
                    item.classList.toggle('active')
                  
                    setTimeout(() => {
                        
                        item.querySelectorAll('.container-line-user ul li:nth-child(2) .container-item p').forEach(function(p){
                            tamanho += p.offsetHeight == 0 ? 50 :  p.offsetHeight
                        })
                        
                        tamanho += 150 + 43
                        item.style.height = tamanho+ "px" 
                    }, 300);
                    tamanho = 0
            }
      })
})


let modalAlterarSenha = document.querySelector('.modal-alterar-senha')
let modal = document.querySelector('.modal-alterar-senha')
if(modalAlterarSenha){
      let openModal = document.querySelector('.open-modal')

      openModal.addEventListener('click', function(){
            modalAlterarSenha.classList.toggle('active')
      })

      modal.addEventListener('click', function(e){
            if(e.target.classList.value.includes('modal-alterar-senha')){
                  modalAlterarSenha.classList.toggle('active')
            }
      })
}



$("#alterar-senha-detalhe-usuario").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        detalhe_usuario_nova_senha: 'required'
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let usuario = new FormData();

        usuario.append('id', $("#detalhe-usuario-id").val());
        usuario.append('nova_senha', $("#detalhe-usuario-nova-senha").val());

        $.ajax({
            url: "/sistema/usuarios/alterar-senha",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                modalAlterarSenha.classList.toggle('active');
                $("#detalhe-usuario-nova-senha").val("");
                swal({
                    type: 'success',
                    title: 'Senha Alterada!',
                    text: ''
                });
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });

    }
});
