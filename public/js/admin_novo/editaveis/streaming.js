var videos = videos || [];

let streaming = {
    original_list: videos,
    filtered_list: null,
    streamingTimeout: null,
    filtros: {
        id_cliente: "",
        data_entrada: "",
        data_saida: "",
        nome_midia: ""
    },
    filtrar:function(){
        clearTimeout(this.streamingTimeout);

        this.streamingTimeout = setTimeout(() => {
            if(this.filtros.id_cliente) {
                this.filtered_list = this.original_list.filter(midia => {
                    return midia.cod_cliente == this.filtros.id_cliente;
                })
            } else {
                this.filtered_list = this.original_list;
            }

            if(this.filtros.data_entrada) {
                this.filtered_list = this.filtered_list.filter(midia => {
                    return moment(midia.data_entrada).unix() >= this.filtros.data_entrada;
                })
            }

            if(this.filtros.data_saida) {
                this.filtered_list = this.filtered_list.filter(midia => {
                    return moment(midia.data_saida).unix() <= this.filtros.data_saida;
                })
            }

            if(this.filtros.nome_midia) {
                this.filtered_list = this.filtered_list.filter(midia => {
                    return midia.titulo.toUpperCase().includes(this.filtros.nome_midia.toUpperCase());
                })
            }

            this.renderVideos();
        }, 800)
    },
    filtrarPdvs:function(){

        let streaming = new FormData();

        streaming.append('cod_campanha', $("#cadastro-streaming-campanha").val());

        $.ajax({
            url: "/sistema/streaming/listar-ilhas",
            type: "POST",
            data: streaming,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {


                $("#cadastro-streaming-pdv-ilhas").html('');

                $.each(data.pdvs_ilhas, function(key, value){

                    let ilhas = "";


                    $.each(value.users, function(key_u, value_u){

                        ilhas += `
                            <div class="check" data-cod-ilha="${value_u.ilha}">
                                <div class="square"></div>
                                <span>${value_u.ilha}</span>
                            </div>
                        `;
                    });

                    $("#cadastro-streaming-pdv-ilhas").append(`
                        <div class="item" data-cod-pdv="${value.cod_pdv}">
                            <h3>${value.nome_pdv}</h3>
                            ${ilhas}
                        </div>
                    `);

                });

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });

    },
    renderVideos:function(){
        let area_videos = $(".todos-midias");

        area_videos.html("");

        this.filtered_list.map(video => {
            area_videos.append(
                `<div class="area-box">
                    <div class="box-midia">
                        <div class="info-midia">
                            <button type="button">
                                <img src="/img/icons/bar-cinza.svg" alt="">
                            </button>
                            <div class="dados">
                                <h3>${ video.titulo }</h3>
                                <div class="info">
                                    <div class="thumb">
                                        <div class="img">
                                            <img src="${ video.foto_capa }" alt="">
                                        </div>
                                        <div class="tipo">
                                            <img src="/img/icone-midia.svg" alt="">
                                            <i class="seta icone-arrow-right"></i>
                                            <span>${ video.tipo }</span>
                                        </div>
                                    </div>
                                    <ul>
                                        <li>Cliente: ${ video.cod_cliente != null ? video.cliente.nome_fantasia : 'Todos'}</li>
                                        <li>Campanha: ${ video.cod_campanha != null ? video.campanha.nome_campanha : 'Todas'}</li>
                                        <li>PDV: ${ video.pdvs != '' ? video.pdvs : 'Todos' }</li>
                                        <li>D.E: - ${ moment(video.data_entrada).format("DD/MM/YYYY") }</li>
                                        <li>D.F: - ${ moment(video.data_saida).format("DD/MM/YYYY") }</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="area-desc">
                            <div class="topo-desc" onclick="toggleInfoStreaming(this)">
                                <h3>Descrição</h3>
                                <i class="seta icone-arrow-down"></i>
                            </div>
                            <div class="info-desc">
                                <p>
                                    ${ video.descricao }
                                </p>
                                <div class="arquivo">
                                    <span>Arquivo</span>
                                    <a href="${ video.link }">${ video.link_video }</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`
            )
        });
    }
};

var arrPdvIlhasCadastrar = [];

$(document).on("click", "#cadastro-streaming-pdv-ilhas .check", function() {
    $(this).toggleClass("checked");
    let objPdvIlha  = {};
    objPdvIlha.pdv  = this.parentElement.dataset.codPdv;
    objPdvIlha.ilha = this.dataset.codIlha;

    if($(this).hasClass('checked')){
        arrPdvIlhasCadastrar.push(objPdvIlha);
    } else {
        arrPdvIlhasCadastrar.forEach(function(value, i){
            if(value.pdv == objPdvIlha.pdv && value.ilha == objPdvIlha.ilha){
                arrPdvIlhasCadastrar.splice(i, 1);
            }
        });
    }
});


$(document).on('click',"#cadastro-streaming-option-cliente", function(){

    if($("#cadastro-streaming-cliente").val() != ''){

        let cliente = new FormData();

        cliente.append('cod_cliente', $("#cadastro-streaming-cliente").val());

        $.ajax({
            url: "/sistema/streaming/listar-campanhas",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#cadastro-streaming-select-campanha").html('');

                    $.each(data.campanhas, function(key, value){

                        $("#cadastro-streaming-select-campanha").append(`
                            <li><a href="" id="cadastro-streaming-option-campanha" cod-opcao="${ value.cod_campanha }">${ value.nome_campanha }</a></li>
                        `);

                    });
                } else
                    console.log("erro=> " + data.mensagem);

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });


    }

});


$(document).on('click',"#cadastro-streaming-option-campanha", function(){
    streaming.filtrarPdvs();
});

$("#form-cadastro-streaming").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo: 'required',
        descricao: 'required',
        dt_disparo: {
            required: true,
            minlength: 10
        },
        dt_termino: {
            required: true,
            minlength: 10
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let streaming = new FormData();

        if($("#cadastro-streaming-midia")[0].files[0] == undefined) {
            swal({
                type: 'error',
                title: 'Espere! Insira a mídia!',
            });
            return;
        }

        if($("#cadastro-streaming-capa")[0].files[0] == undefined) {
            swal({
                type: 'error',
                title: 'Espere! Insira a capa!',
            });
            return;
        }

        streaming.append('titulo', $("#cadastro-streaming-titulo").val());
        streaming.append('descricao', $("#cadastro-streaming-descricao").val());
        streaming.append('dt_entrada', $("#cadastro-streaming-dt-disparo").val());
        streaming.append('dt_saida', $("#cadastro-streaming-dt-termino").val());
        streaming.append('video', $("#cadastro-streaming-midia")[0].files[0]);
        streaming.append('capa', $("#cadastro-streaming-capa")[0].files[0]);
        streaming.append('cod_cliente', $("#cadastro-streaming-cliente").val());
        streaming.append('cod_campanha', $("#cadastro-streaming-campanha").val());
        streaming.append('pdvs_ilhas', JSON.stringify(arrPdvIlhasCadastrar));

        $.ajax({
            url: "/sistema/streaming/adicionar-streaming",
            type: "POST",
            data: streaming,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!streaming.cadastroOnProgress){
                            streaming.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-streaming button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        streaming.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-streaming button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });

    }
});

$("#filtro-streaming-cliente").on("change", function(){
    streaming.filtros.id_cliente = this.value;
    streaming.filtrar();
});

$("#filtro-streaming-dt-inicio").on("keyup", function(){
    if(helpers.checkDateBr(this.value)){
        streaming.filtros.data_entrada = helpers.brDateToUTC(this.value);
    } else {
        streaming.filtros.data_entrada = "";
    }

    streaming.filtrar();
});

$("#filtro-streaming-dt-fim").on("keyup", function(){

    if(helpers.checkDateBr(this.value)){
        streaming.filtros.data_saida = helpers.brDateToUTC(this.value);
    } else {
        streaming.filtros.data_saida = "";
    }

    streaming.filtrar();
});

$("#filtro-streaming-busca").on("keyup", function(){
    streaming.filtros.nome_midia = this.value;

    streaming.filtrar();
});
