function limpaString(string){
    let pattern = /[\/\n\r\s\t]+/g;

    string = string.toLowerCase().toString();
    string = string.replace('/[áàãâä]/ui', 'a');
    string = string.replace('/[éèêë]/ui', 'e');
    string = string.replace('/[íìîï]/ui', 'i');
    string = string.replace('/[óòõôö]/ui', 'o');
    string = string.replace('/[úùûü]/ui', 'u');
    string = string.replace('/[ç]/ui', 'c');
    string = string.replace('/\//g', '-');
    string = string.replace(pattern, '-');

    return string;
}

var slide_home = new Swiper('.slide-home', {
    pagination: {
        el: '.s-banner .area-box-ctrl .ctrl .swiper-pagination',
    },
    navigation: {
        nextEl: '.s-banner .area-box-ctrl .ctrl .btn-next',
        prevEl: '.s-banner .area-box-ctrl .ctrl .btn-prev',
    },
});

var slide_central = new Swiper('.slide-central', {
    pagination: {
        el: '.s-central .slide-central .ctrl-slide .swiper-pagination',
    },
    navigation: {
        nextEl: '.s-central .slide-central .ctrl-slide .btn-next',
        prevEl: '.s-central .slide-central .ctrl-slide .btn-prev',
    },
    slidesPerView: 3,
    spaceBetween: 79,
});

var slide_vocabulario = new Swiper('.slide-vocabulario', {
    slidesPerView: 2,
    slidesPerColumn: 2,
    spaceBetween: 17,
    pagination: {
        el: '.s-vocabulario .topo .ctrl-slide .swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.s-vocabulario .topo .ctrl-slide .btn-next',
        prevEl: '.s-vocabulario .topo .ctrl-slide .btn-prev',
    },
});

var slide_planos = new Swiper('.slide-planos', {
    slidesPerView: 3,
    spaceBetween: 22,
    pagination: {
        el: '.s-planos-uniodonto .topo .ctrl-slide .swiper-pagination',
        clickable: true,
    },
    navigation: {
        nextEl: '.s-planos-uniodonto .topo .ctrl-slide .btn-next',
        prevEl: '.s-planos-uniodonto .topo .ctrl-slide .btn-prev',
    },
});

function toggleInfoStreaming(el) {
    let box_midia = $(el).parents(".box-midia");
    let hasClass = box_midia.hasClass("box-expand");

    $(".todos-midias .area-box .box-midia").removeClass("box-expand");

    if(!hasClass) {
        box_midia.addClass("box-expand");
    }
}

$("#modal-erro .btn-voltar").on("click", function() {
    $(this).parents(".modal").fadeOut();
    return false;
})

$(".check-disponivel").on("click", function() {
    $(this).toggleClass("checked");
})

$(".check-temporaria").on("click", function() {
    $(this).toggleClass("checked");
})

$("input").focusin(function() {
    $(this).parents(".form-group").addClass("focus");
})

$("input").focusout(function() {
    $(this).parents(".form-group").removeClass("focus");
})

$("[mask='cnpj']").mask("00.000.000/0000-00");

$("[mask='cep']").mask("00000-000");

$("[mask='telefone']").mask("(00) 0000-0000");

$("[mask='date']").mask("00/00/0000");

$(document).on('click',".select-custom .item-selected", function(){
    $(this).parents(".select-custom").toggleClass("open-dropdown");
    $(this).parents(".form-group").addClass("focus");
    return false;
})

$(document).on('click',".select-custom .dropdown ul li a", function(){

    var valorSelecionado = $(this).text();
    var codOpcao = $(this).attr("cod-opcao");
    $(this).parents(".select-custom").removeClass("open-dropdown");
    $(this).parents(".select-custom").find(".item-selected span").text(valorSelecionado);
    $(this).parents(".select-custom").find("input[type=hidden]").val(codOpcao);
    $(this).parents(".select-custom").find("input[type=hidden]").trigger('change');
    $(this).parents(".form-group").removeClass("focus");
    return false;
})

$(".btn-recolher-sidebar").on("click", function() {
    $(".area-geral-campanha").toggleClass("close-sidebar");
    $('.status-ativo').toggleClass('active')
    return false;
})

$(".btn-excluir-carga").on("click", function() {
    $(this).parents(".item").remove();
    return false;
})

$(".box-pdv").click(function() {
    $(this).toggleClass("expand-box");
    $(this).parent().toggleClass('active')
})

$(".area-box .btn-acoes").on("click", function() {
    $(this).parents(".area-box").find(".dropdown").toggleClass("active");
})

$(".area-box .dropdown ul li .btn-excluir").on("click", function() {
    $(this).parents(".area-box").remove();
    return false;
})

$(".nav-tabs li a").click(function(e) {

    if(e.target.href){
        if(e.target.href !== ''){
            window.location.href = e.target.href
        }
    }

    $(this).parents(".nav-tabs").find("li").removeClass("active");
    $(this).parents("li").addClass("active");
    return false;
})

let links = document.querySelectorAll('.s-area-portal .tab-pane#cupons .item')

links.forEach(function(link){
    link.addEventListener('click', function(e){
        e.preventDefault();
        const redirect = ['A', 'STRONG'];

        if(redirect.includes(e.target.tagName)){

            window.location.href= link.getAttribute('href');
        }else{
            // e.target.parentElement.parentElement.querySelector('.dropdown').classList.add('active')
            let item = e.target.parentElement.parentElement.parentElement.parentElement
            item.querySelector('.dropdown').classList.toggle('active')
        }
    })
})

$(".nav-tabs li span").click(function() {
    $(this).parents(".nav-tabs").find("li").removeClass("active");
    $(this).parents("li").addClass("active");
})

$("#tab-pdv").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#pdvs").fadeIn();
    $(".btn-add").show();
})

$("#tab-campanha").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#campanhas").fadeIn();
    $(".btn-add").hide();
})

$("#tab-pts").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#geral-pts").fadeIn();
})

$("#tab-temporarias").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#cargas-temporarias").fadeIn();
})

$("#tab-regulamento").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#regulamentos").fadeIn();
})

$("#tab-banner").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#banners").fadeIn();
})

$("#tab-categoria").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#categorias").fadeIn();
})

$("#tab-cartoes").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#cartoes").fadeIn();
})

$("#tab-lote").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#lote").fadeIn();
})

$("#file-7").change(function() {
    $(this).parents("form").find("button").removeClass("disabled");
})


$(".area-geral-campanha .cont-geral .tab-pane .dados-resgates-gerais .item .item-head").on("click", function() {
    $(this).parents(".dados-resgates-gerais").find(".item").removeClass("item-expand");
    $(this).parents(".item").addClass("item-expand");
})

$(".box-loja .cards .btn-expandir").on("click", function() {
    $(this).parents(".box-loja").toggleClass("expand-store");
})




$(".btn-switch").on("click", function() {
    $(this).toggleClass("active");
})

$(".todos-usuarios .item .item-head").on("click", function() {
    $(".todos-usuarios .item").removeClass("expand-item");
    $(this).parents(".item").addClass("expand-item");
//     console.log('olá')
})

$(".s-area-portal .dados-cartoes .item .item-head").on("click", function() {
    $(".s-area-portal .dados-cartoes .item").removeClass("item-expand");
    $(this).parents(".item").addClass("item-expand");
})

$(".area-conteudo-modulo .area-todas-notificacoes .box-notificacao .area-mensagem .topo-msg").on("click", function() {
    $(".area-conteudo-modulo .area-todas-notificacoes .box-notificacao").removeClass("expand-box");
    $(this).parents(".box-notificacao").addClass("expand-box");
})

$(".area-geral-campanha aside .title button").on("click",function() {
    $(this).parents(".title").find(".dropdown").toggleClass("active");
})
