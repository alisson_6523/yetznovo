let usuarios = {

    bloquear:function(id){
        let usuario = new FormData();

        usuario.append('id', id);

        $.ajax({
            url: "/sistema/usuarios/bloquear-usuario",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });

    },

    ativar:function(id){
        let usuario = new FormData();

        usuario.append('id', id);

        $.ajax({
            url: "/sistema/usuarios/ativar-usuario",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                console.log(data);
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });
    },

    filtrarUsuarios:function(){

        let filtros = new FormData();

        if($("#filtro-usuarios-pdv").val() != '')
            filtros.append('pdv', $("#filtro-usuarios-pdv").val());

        if($("#filtro-usuarios-ilha").val() != '')
            filtros.append('ilha', $("#filtro-usuarios-ilha").val());

        if($("#filtro-usuarios-status").val() != '')
            filtros.append('status', $("#filtro-usuarios-status").val());

        if($("#filtro-usuarios-cadastro").val() != '')
            filtros.append('cadastro', $("#filtro-usuarios-cadastro").val());

        if($("#filtro-usuarios-busca").val() != '')
            filtros.append('busca', $("#filtro-usuarios-busca").val());


        $.ajax({
            url: "/sistema/usuarios/filtrar-usuarios",
            type: "POST",
            data: filtros,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

            },
            beforeSend: function() {
                $('#todos-usuarios-filtrado').empty();
                $('.loader').show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $.each(data, function(key, value){

                let dt_cadastro = new Date(value.data_cadastro);
                let nome_usuario = value.nome_usuario;
                nome_usuario = nome_usuario.replace(/_/g, ' ');

                $("#todos-usuarios-filtrado").append(`
                        <div class="item" status="${ value.status ? 'ativo' : 'bloqueado' }">
                            <ul class="item-head">
                                <li>
                                    <a id="open-dropdown" style="cursor: pointer">
                                        <img src="/img/icons-adm/bar-cinza.svg" alt="">
                                    </a>
                                    <h3>${ nome_usuario }</h3>
                                    <div class="dropdown">
                                        <ul>
                                            <li>
                                                <a href="${ window.location.pathname + '/' + value.id + '/' + limpaString(value.nome_usuario) }">
                                                    <i class="icone-lapis"></i>
                                                    <span>Detalhes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <span>RE: ${ value.login }</span>
                                </li>
                                <li>
                                    <span>${value.pdv.nome_pdv}/${ value.ilha }</span>
                                </li>

                                <li>

                                    <div class="status">
                                        <div class="square"></div>
                                        <span>${ value.status ? 'Concluído' : 'Não Concluído' }</span>
                                    </div>

                                    <label for="">status</label>
                                    <div id="status-usuario-campanha" class="btn-switch ${ value.deleted_at ? '' : 'active' }">
                                        <input type="hidden" id="id-usuario" value="${ value.id }">
                                        <div class="square"></div>
                                    </div>

                                    <i class="icone-arrow-down"></i>
                                </li>


                            </ul>
                            <ul class="item-body">

                                <li>
                                ${value.status == 1 ? `
                                    <label for="">email</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/envelope.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ value.email }</span>
                                    </div>
                                ` : ''}
                                </li>
                                <li>
                                ${value.status == 1 ? `
                                    <label for="">telefone</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/celular.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ value.telefone != '' ? value.celular : value.telefone }</span>
                                    </div>
                                ` : ''}
                                </li>

                                <li>
                                    <label for="">Campanha</label>
                                    <div class="info">
                                        <i class="icone-campanha" style="color: #d0456e;"></i>
                                        <i class="seta icone-arrow-right"></i>
                                        <span>teste</span>
                                    </div>
                                </li>

                                <li>
                                ${value.status == 1 ? `
                                    <label for="">CONCLUSÃO DE CADASTRO</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/circulo-estrela.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ dt_cadastro.getDate() + '.' + (dt_cadastro.getUTCMonth() + 1)  + '.' + dt_cadastro.getFullYear() }</span>
                                    </div>
                                ` : ''}
                                </li>
                            </ul>
                        </div>
                    `);

            });

            $('.loader').hide();
            usuarios.recuperarReferencia();
        });

    },

    recuperarReferencia:function(){
        $(".btn-switch").on("click", function() {
            $(this).toggleClass("active");
        })

        let dropdown = Array.from(document.querySelectorAll('#open-dropdown'));

        dropdown.forEach(function(drop){
            drop.addEventListener('click',function(){
                drop.parentElement.querySelector('.dropdown').classList.toggle('active')
            })
        })

	  let itemsHead = document.querySelectorAll('#todos-usuarios-filtrado .item .item-head')

        function remoItensHead(){
            itemsHead.forEach(function(item){
                item.parentElement.classList.remove('expand-item')
            })
        }
        itemsHead.forEach(function(item){
		item.addEventListener('click', function(){

                if(item.parentElement.classList.value.includes('expand-item')){
                    remoItensHead()
                    item.parentElement.classList.remove('expand-item')
                }else{
                    remoItensHead()
                    item.parentElement.classList.add('expand-item')
                }
            })
        })
    }

}

$("#filtro-usuarios-pdv").trigger('change');
$("#filtro-usuarios-ilha").trigger('change');
$("#filtro-usuarios-status").trigger('change');
$("#filtro-usuarios-cadastro").trigger('change');
$(document).on("change", "#filtro-usuarios-pdv, #filtro-usuarios-ilha, #filtro-usuarios-status, #filtro-usuarios-cadastro", function(){
    usuarios.filtrarUsuarios();
});
$(document).on("keyup", "#filtro-usuarios-busca", function () {
    clearTimeout(usuarios.filtrarUsuariosTimeout);

    usuarios.filtrarUsuariosTimeout = setTimeout(() => {
        usuarios.filtrarUsuarios()
    }, 800);
});


let titles = Array.from(document.querySelectorAll('#title-toggle'))

titles.forEach(function(title){
    title.addEventListener('click', function(){
        let user = title.dataset.user
        let todosUsuariosSite = document.querySelector('.todos-usuarios-sites')
        let todosUsuariosAdm = document.querySelector('.todos-usuarios')
        if(user == 'usu'){
            todosUsuariosAdm.classList.remove('active')
            todosUsuariosSite.classList.add('active')
            usuarios.filtrarUsuarios();
        }else{
            todosUsuariosSite.classList.remove('active')
            todosUsuariosAdm.classList.add('active')
        }
    })
})

let itensUsuarios = Array.from(document.querySelectorAll('.todos-usuarios-sites .item'))

function remoteItensUsuario (){
    itensUsuarios.forEach(function(item){
        item.classList.remove('expand-item')
    })
}

itensUsuarios.forEach(function(item){

    item.addEventListener('click', function(){


        if(item.classList.value.includes('expand-item')){
            item.classList.remove('expand-item')
        }else{
            remoteItensUsuario()
            item.classList.toggle('expand-item')
        }

    })
})

$('#clientes li a').click(function() {
    $('#cadastro-usuario-cliente').val(($(this).attr('cod-opcao')));
});

$('#perfis li a').click(function() {
    $('#cadastro-usuario-perfil').val(($(this).attr('cod-opcao')));
});

$("#cadastro-administrador-perfil").trigger('change');
$("#editar-administrador-perfil").trigger('change');
$(document).on("change", "#cadastro-administrador-perfil, editar-administrador-perfil", function(){
    if($(this).val() == 4){
        if($("#cadastro-administrador-cliente").val() != ''){
            usuarios.listarCampanhas($("#cadastro-administrador-cliente").val());

            $("#cadastro-administrador-div-campanhas").show();
        }
    } else {
        $("#cadastro-administrador-div-campanhas").hide();
    }
});


$("#form-cadastro-administrador").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        nome: 'required',
        email: 'required',
        login: 'required',
        senha: 'required',
        confirmar_senha: {
            required: true,
            equalTo: '#cadastro-administrador-senha'
        }
    },
    errorPlacement: function(error, element){
    },
    submitHandler: function(form) {

        let usuario = new FormData();

        if($("#cadastro-administrador-cliente").val() == ''){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Defina um cliente para este administrador.'
            });
            return;
        }

        if($("#cadastro-administrador-perfil").val() == ''){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Defina um perfil para este administrador.'
            });
            return;
        }

        usuario.append('nome', $("#cadastro-administrador-nome").val());
        usuario.append('email', $("#cadastro-administrador-email").val());
        usuario.append('login', $("#cadastro-administrador-login").val());
        usuario.append('password', $("#cadastro-administrador-senha").val());
        usuario.append('cliente_id', $("#cadastro-administrador-cliente").val());
        usuario.append('perfil_id', $("#cadastro-administrador-perfil").val());

        $.ajax({
            url: "/sistema/usuarios/adicionar-administrador",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!usuario.cadastroOnProgress){
                            usuario.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-administrador button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        usuario.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-administrador button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.login) {
                    swal({
                        type: 'error',
                        title: 'Espere!',
                        text: 'Este Login já existe!'
                    }).then(function () {
                        $("#cadastro-administrador-login").focus();
                        btnHelpers.setBtnCadastroDefault("#form-cadastro-administrador button[type='submit']");
                    });
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});


$("#form-editar-administrador").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        nome: 'required',
        email: 'required',
        confirmar_senha: {
            equalTo: '#editar-administrador-senha'
        }
    },
    errorPlacement: function(error, element){
    },
    submitHandler: function(form) {

        let usuario = new FormData();

        if($("#editar-administrador-cliente").val() == ''){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Defina um cliente para este administrador.'
            });
            return;
        }

        if($("#editar-administrador-perfil").val() == ''){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Defina um perfil para este administrador.'
            });
            return;
        }

        usuario.append('id', $("#editar-administrador-id").val());
        usuario.append('nome', $("#editar-administrador-nome").val());
        usuario.append('email', $("#editar-administrador-email").val());
        usuario.append('login', $("#editar-administrador-login").val());
        usuario.append('cliente_id', $("#editar-administrador-cliente").val());
        usuario.append('perfil_id', $("#editar-administrador-perfil").val());

        if($("#editar-administrador-senha").val() != '' && $("#editar-administrador-confirmar-senha").val() != ''  && $("#editar-administrador-senha").val() == $("#editar-administrador-confirmar-senha").val())
            usuario.append('password', $("#editar-administrador-senha").val());

        $.ajax({
            url: "/sistema/usuarios/editar-administrador",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!usuario.cadastroOnProgress){
                            usuario.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-editar-administrador button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        usuario.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-editar-administrador button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else{
                    if(data.tipo == 'login')
                        swal({
                            type: 'error',
                            title: 'Espere!',
                            text: 'Este Login já existe!'
                        }).then(function () {
                            $("#editar-administrador-login").focus();
                        });
                    else
                        $("#modal-erro").show();
                }

            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.login) {
                    swal({
                        type: 'error',
                        title: 'Espere!',
                        text: 'Este Login já existe!'
                    }).then(function () {
                        $("#editar-administrador-login").focus();
                        btnHelpers.setBtnCadastroDefault("#form-editar-administrador button[type='submit']");
                    });
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});
