var helpers = {
    brDateToUTC: function(date) {
        let splitedData = date.split("/");

        return moment(new Date(splitedData[1] + "/" + splitedData[0] + "/" + splitedData[2])).unix();
    },
    checkDateBr: function(date) {
        let data_regex = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/

        return data_regex.test(date)
    }
}

var previa = {
    originalPrevia(input, target ){
        let title = document.querySelector(input)
        let titlePrevia = document.querySelector(target)
        if(title){
            title.addEventListener('keyup', function(e){
                titlePrevia.innerHTML = title.value
            })
        }
    },

    setImgPrevia(targetItens, targetImg){
        let itens = document.querySelectorAll(targetItens)
        itens.forEach(function(item){
            item.addEventListener('click', function(){
                let opcao = item.getAttribute('cod-opcao')

                switch(opcao){
                    case '1':
                            document.querySelector(targetImg).setAttribute('src', '/img/icones-notificacoes/novo_cartao.svg')
                            document.querySelector(targetImg).style.backgroundColor = "#00a651"
                    break

                    case '2':
                            document.querySelector(targetImg).setAttribute('src', '/img/icones-notificacoes/fique_atento.svg')
                            document.querySelector(targetImg).style.backgroundColor = "#971134"
                    break

                    case '3':
                            document.querySelector(targetImg).setAttribute('src', '/img/icones-notificacoes/novo_cartao.svg')
                            document.querySelector(targetImg).style.backgroundColor = "#ff6f00"
                    break

                    case '4':
                            document.querySelector(targetImg).setAttribute('src', '/img/icones-notificacoes/dicas.svg')
                            document.querySelector(targetImg).style.backgroundColor = "#ecb730"
                    break

                }
            })
        })
    }
}



var btnHelpers = {
    setBtnCadastroDefault: function(btn_selector) {
        let btn = $(btn_selector);
        let iconBtn = btn.find("i");

        btn.attr("disabled", false)
        iconBtn.addClass("icone-arrow-right");
        iconBtn.removeClass("fa fa-spinner fa-spin");
    },
    setBtnCadastroLoading: function(btn_selector) {
        let btn = $(btn_selector);
        let iconBtn = btn.find("i");

        btn.attr("disabled", true)
        iconBtn.removeClass("icone-arrow-right");
        iconBtn.addClass("fa fa-spinner fa-spin");
    }
}
