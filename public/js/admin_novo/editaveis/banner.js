let openDropdownBanner = document.querySelectorAll('.s-area-portal .todos-banners  button');

openDropdownBanner.forEach(function(btn){
    btn.addEventListener('click', function(){
        btn.querySelector('.dropdown').classList.toggle('active')
    })
});


var banners = banners || [];

let banner = {
    original_list: banners,
    filtered_list: null,
    bannerTimeout: null,
    filtros: {
        titulo: "",
    },
    filtrar:function(){
        clearTimeout(this.bannerTimeout);

        this.bannerTimeout = setTimeout(() => {
            if(this.filtros.titulo) {
                this.filtered_list = this.original_list.filter(midia => {
                    return midia.titulo.toUpperCase().includes(this.filtros.titulo.toUpperCase());
                })
            } else {
                this.filtered_list = this.original_list;
            }

            this.renderBanners();
        }, 800)
    },
    renderBanners:function(){
        let area_banners = $(".todos-banners");

        area_banners.html('');

        this.filtered_list.map(banner => {
            area_banners.append(`
                <div class="box-banner">
                      <button type="button" class="">
                            <img src="/img/icons/bar-cinza.svg" alt="">
                            <div class="dropdown">
                                <ul>
                                    <li>
                                        <a href="/sistema/plataforma/editar-banner/${ banner.cod_banner }/${ banner.titulo != null ? limpaString(banner.titulo) : 'sem-titulo' }">
                                            <i class="icone-lapis"></i>
                                            <span>editar</span>
                                        </a>
                                    </li>
                                    ${ banner.deleted_at == null ? `
                                        <li>
                                            <a onclick="banner.inativar(${banner.cod_banner})" class="btn-excluir"
                                               style="cursor:pointer;">
                                                <img src="/img/icons-adm/excluir.svg" alt="">
                                                <span>desativar</span>
                                            </a>
                                        </li>                                   
                                    ` : `
                                        <li>
                                            <a onclick="banner.ativar(${ banner.cod_banner })" class="btn-excluir"
                                               style="cursor:pointer;">
                                                <i class="icone-lapis"></i>
                                                <span>ativar</span>
                                            </a>
                                        </li>
                                    `}
                                </ul>
                            </div>
                      </button>
                      <div class="info">
                            <div class="thumb">
                                  <img src="https://larayetz.s3.sa-east-1.amazonaws.com/banners/${ banner.hash_arquivo }" alt="">
                            </div>
                            <ul>
                                  ${banner.titulo != '' ? `<li><span>Título: ${ banner.titulo }</span></li>` : ''}
                                  ${banner.cod_campanha != null ? `<li><span>Campanha: ${ banner.campanha.nome_campanha }</span></li>` : ''}
                                  <li><span>D.E- ${ moment(banner.data_entrada).format("DD.MM.YYYY") }</span></li>
                                  <li><span>D.S- ${ banner.data_saida != null ? moment(banner.data_saida).format("DD.MM.YYYY") : 'Indefinido' }</span></li>
                                  <li><span>${ banner.arquivo }</span></li>
                                  <li status="${ banner.deleted_at == null ? 'ativo' : '' }"><span>${ banner.deleted_at == null ? 'Ativo' : 'Inativo' }</span></li>
                            </ul>
                      </div>
                </div>
            `);
        });

        let openDropdownBanner = document.querySelectorAll('.s-area-portal .todos-banners  button');

        openDropdownBanner.forEach(function(btn){
            btn.addEventListener('click', function(){
                btn.querySelector('.dropdown').classList.toggle('active')
            })
        });
    },
    ativar:function(id){
        let banner = new FormData();

        banner.append('cod_banner', id);

        $.ajax({
            url: window.location.pathname + "/ativar-banner",
            type: "POST",
            data: banner,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    swal({
                        type: 'success',
                        title: 'Banner Ativado!',
                    }).then(function () {
                        location.reload();
                    });
                }
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo de errado aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });
    },

    inativar:function(id){
        let banner = new FormData();

        banner.append('cod_banner', id);

        $.ajax({
            url: window.location.pathname + "/inativar-banner",
            type: "POST",
            data: banner,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    swal({
                        type: 'success',
                        title: 'Banner Desativado!',
                    }).then(function () {
                        location.reload();
                    });
                }
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo de errado aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });
    },

};


$(".check-banner-geral").on("click", function() {
    $(this).toggleClass("checked");

    if($(this).hasClass('checked')){
        $("#cadastro-banner-div-cliente").hide();
        $("#cadastro-banner-div-campanha").hide();
    } else {
        $("#cadastro-banner-div-cliente").show();
        $("#cadastro-banner-div-campanha").show();
    }
})


$(document).on('click',"#cadastro-banner-option-cliente", function(){

    if($("#cadastro-banner-cliente").val() != ''){

        let cliente = new FormData();

        cliente.append('cod_cliente', $("#cadastro-banner-cliente").val());

        $.ajax({
            url: "/sistema/plataforma/listar-campanhas",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#cadastro-banner-select-campanha").html('');

                    $.each(data.campanhas, function(key, value){
                        $("#cadastro-banner-select-campanha").append(`
                            <li><a href="" id="cadastro-banner-option-campanha" cod-opcao="${ value.cod_campanha }">${ value.nome_campanha }</a></li>
                        `);
                    });
                } else
                    console.log("erro=> " + data.mensagem);

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });
    }

});

$("#form-cadastro-banner").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_banner: 'required',
        descricao: 'required',
        entrada_banner: {
            required: true,
            minlength: 10
        },
        saida_banner: {
            minlength: 10
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let banner = new FormData();

        if($("#file-7")[0].files[0] == undefined) {
            swal({
                type: 'error',
                title: 'Espere! Insira a imagem!',
            });
            return;
        }

        if(!$(".check-banner-geral").hasClass('checked') && $("#cadastro-banner-campanha").val() == '') {
            swal({
                type: 'error',
                title: 'Espere! Escolha uma campanha!',
            });
            return;
        }

        banner.append('titulo', $("#titulo").val());
        banner.append('entrada', $("#dt-entrada").val());
        banner.append('saida', $("#dt-saida").val());
        banner.append('foto', $("#file-7")[0].files[0]);

        if(!$(".check-banner-geral").hasClass('checked'))
            banner.append('cod_campanha', $("#cadastro-banner-campanha").val());

        $.ajax({
            url: "/sistema/plataforma/cadastrar-banner",
            type: "POST",
            data: banner,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!banner.cadastroOnProgress){
                            banner.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-banner button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        banner.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-banner button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });
    }
});


$("#form-editar-banner").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_banner: 'required',
        descricao: 'required',
        entrada_banner: {
            required: true,
            minlength: 10
        },
        saida_banner: {
            minlength: 10
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let banner = new FormData();

        if(!$(".check-banner-geral").hasClass('checked') && $("#cadastro-banner-campanha").val() == '') {
            swal({
                type: 'error',
                title: 'Espere! Escolha uma campanha!',
            });
            return;
        }

        banner.append('cod_banner', $("#cod-banner").val());
        banner.append('titulo', $("#titulo").val());
        banner.append('entrada', $("#dt-entrada").val());
        banner.append('cod_campanha', $("#cadastro-banner-campanha").val());
        banner.append('saida', $("#dt-saida").val());

        if($("#file-7")[0].files[0] != null)
            banner.append('foto', $("#file-7")[0].files[0]);

        $.ajax({
            url: "/sistema/plataforma/editar-banner",
            type: "POST",
            data: banner,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!banner.cadastroOnProgress){
                            banner.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-banner button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        banner.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-banner button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });


    }
});

$("#filtro-busca-banner").on("keyup", function(){
    banner.filtros.titulo = this.value;
    banner.filtrar();
});
