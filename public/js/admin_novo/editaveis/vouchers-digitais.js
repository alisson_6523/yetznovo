let vouchers = {

    filtrar:function(){

        let dataInicial = $('#form-filtro-vouchers').find('input[name="data_inicial"]').val();
        let dataFinal   = $('#form-filtro-vouchers').find('input[name="data_final"]').val();
        let codCliente  = $('#form-filtro-vouchers').find('input[name="cod_cliente"]').val();
        let codCampanha = $('#form-filtro-vouchers').find('input[name="cod_campanha"]').val();

        let arrStatus = new Array();

        document.querySelectorAll("#todosStatus").forEach(function(pdv){
            pdv.querySelectorAll('.checked[data-cod]').forEach(function(e){
                arrStatus.push(e.dataset.cod);
            });
        });

        let arrPdvs = new Array();

        if(codCampanha != ""){
            document.querySelectorAll("#todosPdvs").forEach(function(pdv){
                pdv.querySelectorAll('.checked[data-cod]').forEach(function(e){
                    arrPdvs.push(e.dataset.cod);
                });
            });
        }

        let filtros = new FormData();

        filtros.append('tipo_produto', 2);

        if(dataInicial != "")    filtros.append('data_inicial', dataInicial);
        if(dataFinal != "")      filtros.append('data_final', dataFinal);
        if(arrStatus.length > 0) filtros.append('arr_status', JSON.stringify(arrStatus));
        if(codCliente != "")     filtros.append('cod_cliente', codCliente);
        if(codCampanha != "")    filtros.append('cod_campanha', codCampanha);
        if(arrPdvs.length > 0)   filtros.append('arr_pdvs', JSON.stringify(arrPdvs));


        $.ajax({
            url: "/sistema/vouchers-digitais/filtrar",
            type: "POST",
            dataType: "html",
            data: filtros,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data) {

            },
            beforeSend: function() {
                $('#linhas-resgates').empty();
                $('.loader').show();
            },
            error: function(x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $('#linhas-resgates').empty();
            $('.loader').hide();
            $('#linhas-resgates').append(data);
        });
    }
}

$(document).on('click',"#btn-filtrar-vouchers", function(){
    vouchers.filtrar();
});

$("#pesquisa-vouchers").on('input', function(){
    var pesquisa = $(this).val();

    $(".item-table").not(":contains('"+ pesquisa +"')").hide();
    $(".item-table:contains('"+ pesquisa +"')").show();
});

$(".vouchers-option-cliente").on("click", function () {
    var idCliente = $(this).attr('cod-opcao');

    if(idCliente == ""){
        $("#campanhas-cliente").html('');
        return;
    }

    $.ajax({
        url: "/sistema/vouchers-digitais/listar-campanhas",
        type: "POST",
        dataType: "json",
        data: {
            cod_cliente: idCliente
        },
        async: true,
        timeout: 90000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $("#campanhas-cliente").html('');

            $("#campanhas-cliente").append(`
                <li>
                    <a class="vouchers-option-campanha" cod-opcao="">
                        Todas
                    </a>
                </li>
            `);

            $.each(data.campanhas, function (key, value) {
                $("#campanhas-cliente").append(`
                    <li>
                        <a class="vouchers-option-campanha" cod-opcao="${value.cod_campanha}">
                            ${value.nome_campanha}
                        </a>
                    </li>
                `);
            });

        },
        error: function (x, t, m) {
            if (t === "timeout") {
                alert('Conexão está lenta ou é inexistente, tente novamente.');
            } else {
                alert("Erro #001");
            }
        }
    });

});

$(document).on('click', ".vouchers-option-campanha", function () {
    var codCampanha = $(this).attr('cod-opcao');
    var valorSelecionado = $(this).text();

    if(codCampanha == ""){
        $("#todos-pdvs").hide();
        return;
    }

    $(this).parents(".select-custom").find(".item-selected span").text(valorSelecionado);
    $('#hidden-cod-campanha').val(codCampanha);
    $("#todosPdvs").html('');

    $.ajax({
        url: "/sistema/vouchers-digitais/pdvs",
        type: "POST",
        dataType: "json",
        data: {
            cod_campanha: codCampanha
        },
        async: true,
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            $("#todos-pdvs").fadeIn();
            if (!data.erro) {
                if (data.pdvs.length > 0) {
                    $.each(data.pdvs, function (key, value) {
                        $("#todosPdvs").append(`
                            <div class="check"  data-cod="${value.cod_pdv}">
                            <div class="square"></div>
                            <span>${value.nome_pdv}</span>
                            </div>
                        `);
                    });
                } else {
                    $(".todosPdvs").append(`
                        <small>Nenhum PDV encontrado</small>
                    `);
                }
            }

        },
        error: function (x, t, m) {
            console.log("erro=> " + m);
        }
    });

});

$(document).on('click',".s-resgates #tabela-vouchers .item-table .dados .check-square", function(e){
    $("#form-pesquisa-inicial").hide();
    $("#status-lote").fadeIn();

    if($(this).hasClass("checked")) {
        $(this).removeClass("checked");
        var qtdChecked = $(this).parents(".body").find(".checked").length;

        if(qtdChecked == 0) {
            $("#form-pesquisa-inicial").fadeIn();
            $("#status-lote").hide();
        }
    } else {
        var qtdChecked = $(this).parents(".body").find(".checked").length + 1;
        $(this).addClass("checked");
    }
    $(".s-resgates .sidebar-resgate #status-lote h4 span").text("0"+qtdChecked);
    e.stopPropagation();
})

$(".s-resgates #tabela-vouchers .head li .check-all").on("click", function() {
    let itens = document.querySelectorAll('#linhas-resgates .item-resgate:not([style="display: none;"])');
    let qtdChecked = itens.length;

    if($(this).hasClass("checked")) {
        $(this).removeClass("checked");
        $(this).parents(".tabela-dados-resgate").find(".body .item-resgate:not([style='display: none;']) .check-square").removeClass("checked");
        $("#form-pesquisa-inicial").fadeIn();
        $("#status-lote").hide();
        $(".s-resgates .sidebar-resgate #status-lote h4 span").text("00");
    } else {
        $(this).addClass("checked");
        $(this).parents(".tabela-dados-resgate").find(".body .item-resgate:not([style='display: none;']) .check-square").addClass("checked");
        $("#form-pesquisa-inicial").hide();
        $("#status-lote").fadeIn();
        $(".s-resgates .sidebar-resgate #status-lote h4 span").text(qtdChecked);
    }
})

$(document).on('click', '.btn-salvar-voucher', function(e){
    swal({
        title: 'Tem certeza?',
        text: "Este link será vinculado a este voucher digital. Você poderá editar somente se a notificação não foi enviada.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#F7A81D',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, salvar!',
        cancelButtonText: 'Cancelar',
        customClass: 'swal-custom'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "/sistema/vouchers-digitais/salvar-link",
                type: "POST",
                dataType: "json",
                data: {
                    cod_item: e.target.dataset.resgate,
                    link: e.target.parentElement.parentElement.querySelector('input').value,
                },
                async: true,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    swal({
                        type: (data.erro ? 'error' : 'success'),
                        title: data.mensagem,
                    });
                },
                error: function (x, t, m) {
                    swal({
                        type: 'error',
                        title: 'Ops! Algo aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });
        }
    });

});

$(document).on('click', '#btn-enviar-notificacao', function(e){
    let itens = Array.from(document.querySelectorAll(".s-resgates .tabela-dados-resgate .item-table .dados .check-square.checked"))
                .map((item) => {
                    return item.dataset.cod_resgate
                });

    swal({
        title: 'Tem certeza?',
        text: "Após enviar a notificação, o item será alterado para 'Entregue' e não será mais possível mudar o link do voucher!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#F7A81D',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, enviar!',
        cancelButtonText: 'Cancelar',
        customClass: 'swal-custom'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "/sistema/vouchers-digitais/enviar-notificacao",
                type: "POST",
                dataType: "json",
                data: {
                    itens
                },
                async: true,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    swal({
                        type: (data.erro ? 'error' : 'success'),
                        title: data.mensagem,
                    }).then(function (){
                        location.reload();
                    });
                },
                error: function (x, t, m) {
                    swal({
                        type: 'error',
                        title: 'Ops! Algo aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });
        }
    });

});