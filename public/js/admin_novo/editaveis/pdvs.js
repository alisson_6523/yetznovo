let cad_pdvs = {

    // funcoes do pdv aqui dentro (listar, etc)

    excluir:function(id){

        let pdv = new FormData();

        pdv.append('cod_pdv', id);

        $.ajax({
            url: "/sistema/clientes/pdv/excluir-pdv",
            type: "POST",
            data: pdv,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                location.reload();
            },
            error: function (x, t, m) {
                location.reload();
            }
        });

    },


    ativar:function(id){

        let pdv = new FormData();

        pdv.append('cod_pdv', id);

        $.ajax({
            url: "/sistema/clientes/pdv/ativar-pdv",
            type: "POST",
            data: pdv,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                location.reload();
            },
            error: function (x, t, m) {
                console.log('erro=> '+ m);
            }
        });

    }

};


$('#cadastro-pdv').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        pdv_registro: {
            required: true,
            maxlength: 30
        },
        pdv_nome: {
            required: true
        },
        pdv_cnpj: {
            required: true,
            minlength: 18
        },
        pdv_cep: {
            required: true,
            minlength: 9
        },
        pdv_logradouro: {
            required: true
        },
        pdv_numero: {
            required: true
        },
        pdv_bairro: {
            required: true
        },
        pdv_cidade: {
            required: true
        },
        pdv_estado: {
            required: true
        },
        pdv_telefone: {
            required: true,
            minlength: 14
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let pdv = new FormData();

        pdv.append('cliente_id', $("#cliente-id").val());
        pdv.append('registro', $("#pdv-registro").val());
        pdv.append('nome_pdv', $("#pdv-nome").val());
        pdv.append('razao_social', $("#pdv-razao").val());
        pdv.append('cnpj', $("#pdv-cnpj").val());
        pdv.append('cep', $("#pdv-cep").val());
        pdv.append('logradouro', $("#pdv-logradouro").val());
        pdv.append('numero', $("#pdv-numero").val());
        pdv.append('bairro', $("#pdv-bairro").val());
        pdv.append('cidade', $("#pdv-cidade").val());
        pdv.append('estado', $("#pdv-estado").val());
        pdv.append('telefone', $("#pdv-telefone").val());
        pdv.append('email', $("#pdv-email").val());


        if($("#pdv-estadual").val() != '')
            pdv.append('inscricao_estadual', $("#pdv-estadual").val());

        if($("#pdv-municipal").val() != '')
            pdv.append('inscricao_municipal', $("#pdv-municipal").val());

        if($("#pdv-complemento").val() != '')
            pdv.append('complemento', $("#pdv-complemento").val());

        if($("#pdv-site").val() != '')
            pdv.append('site', $("#pdv-site").val());

        if($("#file-7")[0].files[0] != undefined)
            pdv.append('arquivo', $("#file-7")[0].files[0]);


        $.ajax({
            url: "/sistema/clientes/pdv/adicionar-pdv",
            type: "POST",
            data: pdv,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.cnpj) {
                    alert('Este CNPJ já existe!');
                    $("#pdv-cnpj").focus();
                } else if(x.responseJSON.errors.registro) {
                    alert('Este Código de PDV já existe!');
                    $("#pdv-registro").focus();
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});

$('#editar-pdv').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        pdv_nome: {
            required: true
        },
        pdv_cnpj: {
            required: true,
            minlength: 18
        },
        pdv_cep: {
            required: true,
            minlength: 9
        },
        pdv_logradouro: {
            required: true
        },
        pdv_numero: {
            required: true
        },
        pdv_bairro: {
            required: true
        },
        pdv_cidade: {
            required: true
        },
        pdv_estado: {
            required: true
        },
        pdv_telefone: {
            required: true,
            minlength: 14
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let pdv = new FormData();

        pdv.append('cod_pdv', $("#pdv-id").val());
        pdv.append('nome_pdv', $("#pdv-nome").val());
        pdv.append('razao_social', $("#pdv-razao").val());
        pdv.append('cnpj', $("#pdv-cnpj").val());
        pdv.append('cep', $("#pdv-cep").val());
        pdv.append('logradouro', $("#pdv-logradouro").val());
        pdv.append('numero', $("#pdv-numero").val());
        pdv.append('bairro', $("#pdv-bairro").val());
        pdv.append('cidade', $("#pdv-cidade").val());
        pdv.append('estado', $("#pdv-estado").val());
        pdv.append('telefone', $("#pdv-telefone").val());
        pdv.append('email', $("#pdv-email").val());


        if($("#pdv-estadual").val() != '')
            pdv.append('inscricao_estadual', $("#pdv-estadual").val());

        if($("#pdv-municipal").val() != '')
            pdv.append('inscricao_municipal', $("#pdv-municipal").val());

        if($("#pdv-complemento").val() != '')
            pdv.append('complemento', $("#pdv-complemento").val());

        if($("#pdv-site").val() != '')
            pdv.append('site', $("#pdv-site").val());

        if($("#file-7")[0].files[0] != undefined)
            pdv.append('arquivo', $("#file-7")[0].files[0]);


        $.ajax({
            url: "/sistema/clientes/pdv/editar-pdv",
            type: "POST",
            data: pdv,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.cnpj) {
                    alert('Este CNPJ já existe!');
                    $("#pdv-cnpj").focus();
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});
