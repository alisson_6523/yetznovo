let cupons = {

    bloquearLote:function(id){
        let lote = new FormData();
        lote.append('id', id);

        swal({
            title: 'Tem certeza?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#F7A81D',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, bloquear!',
            cancelButtonText: 'Cancelar',
            customClass: 'swal-custom'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/sistema/cupons/bloquear-lote",
                    type: "POST",
                    data: lote,
                    async: true,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                    },
                    beforeSend: function() {
                        $('.loader').show();
                    },
                    error: function (x, t, m) {
                        console.log("erro=> " + m);
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                }).done(function(data) {
                    if(!data.error){
                        $('.loader').hide();
                        swal({
                            type: 'success',
                            title: 'Lote Bloqueado!',
                        }).then(function () {
                            location.reload();
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                });
            }
        });
    },

    ativarLote:function(id){
        let lote = new FormData();
        lote.append('id', id);

        swal({
            title: 'Tem certeza?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#F7A81D',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, ativar!',
            cancelButtonText: 'Cancelar',
            customClass: 'swal-custom'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "/sistema/cupons/ativar-lote",
                    type: "POST",
                    data: lote,
                    async: true,
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {

                    },
                    beforeSend: function() {
                        $('.loader').show();
                    },
                    error: function (x, t, m) {
                        console.log("erro=> " + m);
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                }).done(function(data) {
                    if(!data.error){
                        $('.loader').hide();
                        swal({
                            type: 'success',
                            title: 'Lote Ativo!',
                        }).then(function () {
                            location.reload();
                        });
                    } else {
                        swal({
                            type: 'error',
                            title: 'Ops! Algo aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                });
            }
        });
    },

    exportar:function(lote_id){

        let lote = new FormData();
        lote.append('id', lote_id);

        $.ajax({
            url: "/sistema/cupons/exportar-lote",
            type: "POST",
            data: lote,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

            },
            beforeSend: function() {
                $('.loader').show();
            },
            error: function (x, t, m) {
                $('.loader').hide();
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        }).done(function(data) {
            $('.loader').hide();
            if(!data.erro)
                window.location = "/sistema/cupons/baixar-lote-exportado";
            else {
                swal({
                    type: 'error',
                    title: 'Ops! Algo aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });
    },

};


$(document).on('click',"#cadastro-cupom-option-cliente", function(){

    if($("#cadastro-cupom-cliente").val() != ''){

        console.log($("#cadastro-cupom-cliente").val());

        let cliente = new FormData();

        cliente.append('cod_cliente', $("#cadastro-cupom-cliente").val());

        $.ajax({
            url: "/sistema/cupons/listar-campanhas",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#cadastro-cupom-select-campanha").html('');

                    $("#cadastro-cupom-select-campanha").append(`
                            <li><a href="" id="cadastro-cupom-option-campanha" cod-opcao="">Todas</a></li>
                        `);

                    $.each(data.campanhas, function(key, value){

                        $("#cadastro-cupom-select-campanha").append(`
                            <li><a href="" id="cadastro-cupom-option-campanha" cod-opcao="${ value.cod_campanha }">${ value.nome_campanha }</a></li>
                        `);

                    });
                } else
                    console.log("erro=> " + data.mensagem);

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });
    }

});


$('#form-cadastro-lote-cupom').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        nome: {
            required: true
        },
        prefixo: {
            required: true
        },
        quantidade: {
            required: true
        },
        pontos: {
            required: true
        }
    },
    errorPlacement: function(error, element){ },
    submitHandler: function(form) {

        $("#btn-cadastro-lote-cupom").attr('disabled', 'disabled');

        let cupom = new FormData();

        cupom.append('nome', $("#nome").val());
        cupom.append('cliente_id', $("#cadastro-cupom-cliente").val());
        cupom.append('campanha_id', $("#cadastro-cupom-campanha").val());
        cupom.append('prefixo', $("#prefixo").val());
        cupom.append('quantidade', $("#quantidade").val());
        cupom.append('pontos', $("#pontos").val());

        btnHelpers.setBtnCadastroLoading("#form-cadastro-lote-cupom button[type='submit']");

        $.ajax({
            url: "/sistema/cupons/cadastrar",
            type: "POST",
            data: cupom,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#modal-sucesso").show();
                    btnHelpers.setBtnCadastroDefault("#form-cadastro-lote-cupom button[type='submit']");
                } else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });

    }
});


$(document).on('click', '#check-all-cupons', function(){
    if($(this).hasClass("checked")) {
        $(this).removeClass("checked");
        $("#form-group-cod-status-cupom").fadeOut();
        $(this).parents(".tabela-cupons").find("#linhas-resgates .check-square").removeClass("checked");
    } else {
        $(this).addClass("checked");
        $(this).parents(".tabela-cupons").find("#linhas-resgates .check-square").addClass("checked");
        $("#form-group-cod-status-cupom").fadeIn();
    }
});


document.querySelectorAll("#checkbox-cupom-unitario").forEach(function(item){
    item.addEventListener('click', function(){
        item.classList.toggle('checked')

        let qtdTotal = Array.from(document.querySelectorAll("#checkbox-cupom-unitario.checked")).length;
        if(qtdTotal == 0)
            $("#form-group-cod-status-cupom").fadeOut();
        else
            $("#form-group-cod-status-cupom").fadeIn();

    })
})


$("#cod-status-cupom").change(function(event){
    let cod_status = this.value;
    let itens      = Array.from(
        document.querySelectorAll("#checkbox-cupom-unitario.checked"))
        .map((item) => {
            return item.dataset.cupom_id
        });

    let cupons = new FormData();
    cupons.append('cupons_id', JSON.stringify(itens));

    swal({
        title: 'Espere!',
        text: "Cupons já usados não serão alterados.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#F7A81D',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, continuar!',
        cancelButtonText: 'Cancelar',
        customClass: 'swal-custom'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "/sistema/cupons/" + (cod_status == 1 ? 'ativar' : 'bloquear') + "-cupom-lote",
                type: "POST",
                dataType: "json",
                data: cupons,
                contentType: false,
                processData: false,
                async: true,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                },
                beforeSend: function() {
                    $('.loader').show();
                },
                error: function (x, t, m) {
                    swal({
                        type: 'error',
                        title: 'Ops! Algo aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            }).done(function(data) {
                if(!data.error){
                    $('.loader').hide();
                    swal({
                        type: 'success',
                        title: 'Sucesso!',
                    }).then(function () {
                        location.reload();
                    });
                } else {
                    swal({
                        type: 'error',
                        title: 'Ops! Algo aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });

        }
    });
})
