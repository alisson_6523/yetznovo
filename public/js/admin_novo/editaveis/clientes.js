let clientes = {

    excluir:function(id){

        let cliente = new FormData();

        cliente.append('id', id);

        $.ajax({
            url: "/sistema/clientes/excluir-cliente",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                window.location = "/sistema/clientes";
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });

    },

    ativar:function(id){

        let cliente = new FormData();

        cliente.append('id', id);

        $.ajax({
            url: "/sistema/clientes/ativar-cliente",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                window.location = "/sistema/clientes";
            },
            error: function (x, t, m) {
                location.reload();
            }
        });

    },
    buscaClientes: function(){        
        let cliente = new FormData();

        cliente.append('busca', $("#cliente-busca").val());

        $.ajax({
            url: "/sistema/clientes/filtrar",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                $(".area-todos-clientes").html('');

                $.each(data, function(key, value){

                    let created_at = new Date(value.created_at);

                    $(".area-todos-clientes").append(`
                        <a href="/sistema/clientes/${ value.id }/${ limpaString(value.nome_fantasia != null ? value.nome_fantasia : value.razao_social) }" class="box-cliente">
                            <div class="info">
                                <h3>${ value.nome_fantasia != null ? value.nome_fantasia : value.razao_social }</h3>
                                <ul>
                                    <li>
                                        <div class="square"></div>
                                        <span>CNPJ- ${ value.cnpj }</span>
                                    </li>
                                    <li>
                                        <div class="square"></div>
                                        <span>${ (created_at.getUTCMonth() + 1)  + '/' + created_at.getFullYear() }</span>
                                    </li>
                                    <li>
                                        <div class="square ${ value.deleted_at == null ? 'green' : 'yellow' }"></div>
                                        <span>${ value.deleted_at == null ? 'ATIVO' : 'INATIVO' }</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="geral">
                                <div class="item">
                                    <i class="icone icone-campanha"></i>
                                    <i class="icone-arrow-down"></i>
                                    <span>${ value.campanhas.length }</span>
                                </div>
                                <div class="item">
                                    <i class="icone icone-pdv"></i>
                                    <i class="icone-arrow-down"></i>
                                    <span>${ value.cliente_pdvs.length }</span>
                                </div>
                            </div>
                        </a>
                    `);

                });

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });
    }
};


$('#cadastro-cliente').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        cliente_razao: {
            required: true
        },
        cliente_cnpj: {
            required: true,
            minlength: 18
        },
        cliente_cep: {
            required: true,
            minlength: 9
        },
        cliente_logradouro: {
            required: true
        },
        cliente_numero: {
            required: true
        },
        cliente_bairro: {
            required: true
        },
        cliente_cidade: {
            required: true
        },
        cliente_estado: {
            required: true
        },
        cliente_telefone: {
            required: true,
            minlength: 14
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let cliente = new FormData();

        cliente.append('razao_social', $("#cliente-razao").val());
        cliente.append('cnpj', $("#cliente-cnpj").val());
        cliente.append('cep', $("#cliente-cep").val());
        cliente.append('logradouro', $("#cliente-logradouro").val());
        cliente.append('numero', $("#cliente-numero").val());
        cliente.append('bairro', $("#cliente-bairro").val());
        cliente.append('cidade', $("#cliente-cidade").val());
        cliente.append('estado', $("#cliente-estado").val());
        cliente.append('telefone', $("#cliente-telefone").val());
        cliente.append('email', $("#cliente-email").val());

        if($("#cliente-fantasia").val() != '')
            cliente.append('nome_fantasia', $("#cliente-fantasia").val());

        if($("#cliente-estadual").val() != '')
            cliente.append('inscricao_estadual', $("#cliente-estadual").val());

        if($("#cliente-municipal").val() != '')
            cliente.append('inscricao_municipal', $("#cliente-municipal").val());

        if($("#cliente-complemento").val() != '')
            cliente.append('complemento', $("#cliente-complemento").val());

        if($("#cliente-site").val() != '')
            cliente.append('site', $("#cliente-site").val());

        if($("#file-7")[0].files[0] != undefined)
            cliente.append('arquivo', $("#file-7")[0].files[0]);


        $.ajax({
            url: "/sistema/clientes/adicionar-cliente",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {                        
                        if(!clientes.cadastroOnProgress){                            
                            clientes.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#cadastro-cliente button[type='submit']");
                        }                                        
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        clientes.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.cnpj) {
                    alert('Este CNPJ já existe!');
                    $("#cliente-cnpj").focus();

                    btnHelpers.setBtnCadastroDefault("#cadastro-cliente button[type='submit']");
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});

$('#editar-cliente').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        cliente_razao: {
            required: true
        },
        cliente_cnpj: {
            required: true,
            minlength: 18
        },
        cliente_cep: {
            required: true,
            minlength: 9
        },
        cliente_logradouro: {
            required: true
        },
        cliente_numero: {
            required: true
        },
        cliente_bairro: {
            required: true
        },
        cliente_cidade: {
            required: true
        },
        cliente_estado: {
            required: true
        },
        cliente_telefone: {
            required: true,
            minlength: 14
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let cliente = new FormData();

        cliente.append('id', $("#cliente-id").val());
        cliente.append('razao_social', $("#cliente-razao").val());
        cliente.append('cnpj', $("#cliente-cnpj").val());
        cliente.append('cep', $("#cliente-cep").val());
        cliente.append('logradouro', $("#cliente-logradouro").val());
        cliente.append('numero', $("#cliente-numero").val());
        cliente.append('bairro', $("#cliente-bairro").val());
        cliente.append('cidade', $("#cliente-cidade").val());
        cliente.append('estado', $("#cliente-estado").val());
        cliente.append('telefone', $("#cliente-telefone").val());
        cliente.append('email', $("#cliente-email").val());

        if($("#cliente-fantasia").val() != '')
            cliente.append('nome_fantasia', $("#cliente-fantasia").val());

        if($("#cliente-estadual").val() != '')
            cliente.append('inscricao_estadual', $("#cliente-estadual").val());

        if($("#cliente-municipal").val() != '')
            cliente.append('inscricao_municipal', $("#cliente-municipal").val());

        if($("#cliente-complemento").val() != '')
            cliente.append('complemento', $("#cliente-complemento").val());

        if($("#cliente-site").val() != '')
            cliente.append('site', $("#cliente-site").val());

        if($("#file-7")[0].files[0] != undefined)
            cliente.append('arquivo', $("#file-7")[0].files[0]);


        $.ajax({
            url: "/sistema/clientes/editar-cliente",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {                        
                        if(!clientes.cadastroOnProgress){                            
                            clientes.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#editar-cliente button[type='submit']");
                        }                                        
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        clientes.cadastroOnProgress = false
                    }, false);                    
                }
                return myXhr;
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.cnpj) {
                    alert('Este CNPJ já existe!');
                    $("#cliente-cnpj").focus();

                    btnHelpers.setBtnCadastroDefault("#editar-cliente button[type='submit']");
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});

$("#cliente-busca").keyup(function(){
    clearTimeout(clientes.buscaClientesTimeout);

    clientes.buscaClientesTimeout = setTimeout(() => {
        clientes.buscaClientes()
    }, 800);    
});