let cadastroPopups = {

    filtrarPdvs:function(){

        let popups = new FormData();

        popups.append('cod_campanha', $("#cadastro-popup-campanha").val());

        $.ajax({
            url: "/sistema/popups/listar-ilhas",
            type: "POST",
            data: popups,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {


                $("#cadastro-popup-pdv-ilhas").html('');

                $.each(data.pdvs_ilhas, function(key, value){

                    let ilhas = "";

                    $.each(value.users, function(key_u, value_u){

                        ilhas += `
                            <div class="check" data-cod-ilha="${value_u.ilha}">
                                <div class="square"></div>
                                <span>${value_u.ilha}</span>
                            </div>
                        `;
                    });

                    $("#cadastro-popup-pdv-ilhas").append(`
                        <div class="item" data-cod-pdv="${value.cod_pdv}">
                            <h3>${value.nome_pdv}</h3>
                            ${ilhas}
                        </div>
                    `);

                });

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });

    },

    limpar:function(){
        swal({
            title: 'Tem certeza?',
            text: "Você não será capaz de reverter isto!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#F7A81D',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim, limpar!',
            cancelButtonText: 'Cancelar',
            customClass: 'swal-custom'
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    url: "/sistema/popups/limpar",
                    type: "GET",
                    dataType: "json",
                    async: true,
                    timeout: 10000,
                    success: function (data) {
                        if(!data.erro) {
                            swal({
                                type: 'success',
                                title: 'Sucesso!',
                            }).then(function () {
                                location.reload();
                            });
                        } else {
                            swal({
                                type: 'error',
                                title: 'Ops! Algo de errado aconteceu!',
                            }).then(function () {
                                location.reload();
                            });
                        }
                    },
                    error: function (x, t, m) {
                        console.log("erro=> " + m);
                        swal({
                            type: 'error',
                            title: 'Ops! Algo de errado aconteceu!',
                        }).then(function () {
                            location.reload();
                        });
                    }
                });


            }
        });

    },
}


var arrPopupPdvIlhasCadastrar = [];

$(document).on("click", "#cadastro-popup-pdv-ilhas .check", function() {
    $(this).toggleClass("checked");
    let objPdvIlha = {};
    objPdvIlha.pdv = this.parentElement.dataset.codPdv;
    objPdvIlha.ilha = this.dataset.codIlha;

    if($(this).hasClass('checked')){
        arrPopupPdvIlhasCadastrar.push(objPdvIlha);
    } else {
        arrPopupPdvIlhasCadastrar.forEach(function(value, i){
            if(value.pdv == objPdvIlha.pdv && value.ilha == objPdvIlha.ilha){
                arrPopupPdvIlhasCadastrar.splice(i, 1);
            }
        });
    }
});

$(document).on('click',"#cadastro-popup-option-cliente", function(){

    if($("#cadastro-popup-cliente").val() != ''){

        let cliente = new FormData();

        cliente.append('cod_cliente', $("#cadastro-popup-cliente").val());

        $.ajax({
            url: "/sistema/popups/listar-campanhas",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#cadastro-popup-select-campanha").html('');

                    $.each(data.campanhas, function(key, value){

                        $("#cadastro-popup-select-campanha").append(`
                            <li><a href="" id="cadastro-popup-option-campanha" cod-opcao="${ value.cod_campanha }">${ value.nome_campanha }</a></li>
                        `);

                    });
                } else
                    console.log("erro=> " + data.mensagem);

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });
    }

});


$(document).on('click',"#cadastro-popup-option-campanha", function(){
    cadastroPopups.filtrarPdvs();
});


$("#form-cadastro-popup").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_popup: 'required',
        descricao: 'required',
        entrada_popup: {
            required: true,
            minlength: 10
        },
        saida_popup: {
            required: true,
            minlength: 10
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        if($("#cadastro-popup-cliente").val() != null){
            let popup = new FormData();

            popup.append('titulo_popup', $("#titulo").val());
            popup.append('entrada_popup', $("#dt-entrada").val());
            popup.append('saida_popup', $("#dt-saida").val());
            popup.append('foto', $("#file-7")[0].files[0]);
            popup.append('cod_cliente', $("#cadastro-popup-cliente").val());
            popup.append('cod_campanha', $("#cadastro-popup-campanha").val());
            popup.append('pdvs_ilhas', JSON.stringify(arrPopupPdvIlhasCadastrar));

            $.ajax({
                url: "/sistema/popups/adicionar-popup",
                type: "POST",
                data: popup,
                async: true,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                xhr: function() {
                    var myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){
                        myXhr.upload.addEventListener('progress', (e) => {
                            if(!popup.cadastroOnProgress){
                                popup.cadastroOnProgress = true;

                                btnHelpers.setBtnCadastroLoading("#form-cadastro-popup button[type='submit']");
                            }
                        }, false);
                        myXhr.upload.addEventListener("load", (e) => {
                            popup.cadastroOnProgress = false
                        }, false);
                    }
                    return myXhr;
                },
                success: function (data) {
                    btnHelpers.setBtnCadastroDefault("#form-cadastro-popup button[type='submit']");
                    if(!data.erro)
                        $("#modal-sucesso").show();
                    else
                        $("#modal-erro").show();
                },
                error: function (x, t, m) {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            });
        } else {
            swal({
                type: 'error',
                title: 'Espere! Escolha um cliente!',
            });
        }
    }
});


$("#form-editar-popup").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo_popup: 'required',
        descricao: 'required',
        entrada_popup: {
            required: true,
            minlength: 10
        },
        saida_popup: {
            required: true,
            minlength: 10
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let popup = new FormData();

        popup.append('cod_popup', $("#cod-popup").val());

        popup.append('titulo_popup', $("#titulo").val());
        popup.append('entrada_popup', $("#dt-entrada").val());
        popup.append('saida_popup', $("#dt-saida").val());
        popup.append('cod_cliente', $("#cadastro-popup-cliente").val());
        popup.append('cod_campanha', $("#cadastro-popup-campanha").val());

        if($("#file-7")[0].files[0] != null)
            popup.append('foto', $("#file-7")[0].files[0]);

        popup.append('pdvs_ilhas', JSON.stringify(arrPopupPdvIlhasCadastrar));

        $.ajax({
            url: "/sistema/popups/editar-popup",
            type: "POST",
            data: popup,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!popup.cadastroOnProgress){
                            popup.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#form-cadastro-popup button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        popup.cadastroOnProgress = false
                    }, false);
                }
                return myXhr;
            },
            success: function (data) {
                btnHelpers.setBtnCadastroDefault("#form-cadastro-popup button[type='submit']");
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });

    }
});
