let arrPdvsCadastrar = []; // array de pdvs para inserir na campanha
let arrPdvsRemover = [];
var check_produtos = {};

let campanha = {
    cadastroOnProgress: false,
    excluir:function(id){

        let campanha = new FormData();

        campanha.append('cod_campanha', id);

        $.ajax({
            url: "/sistema/campanhas/excluir-campanha",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                window.location = "/sistema/campanhas";
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });

    },

    ativar:function(id){

        let campanha = new FormData();

        campanha.append('cod_campanha', id);

        $.ajax({
            url: "/sistema/campanhas/ativar-campanha",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                window.location = "/sistema/campanhas";
            },
            error: function (x, t, m) {
                location.reload();
            }
        });

    },

    buscaCampanhas:function(){
        let campanha = new FormData();

        campanha.append('busca', $("#campanha-busca").val());

        $.ajax({
            url: "/sistema/campanhas/filtrar",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

                $(".area-todos-clientes").html('');

                $.each(data, function(key, value){

                    let dt_inicio    = new Date(value.dt_inicio);
                    let dt_fim       = new Date(value.dt_fim);
                    let total_pontos = 0;

                    $.each(value.campanha_protocolos, function(key, value){
                        total_pontos += value.total_pontos;
                    });

                    $(".area-todos-clientes").append(`
                        <a href="/sistema/campanhas/${ value.cod_campanha }/${ limpaString(value.nome_campanha) }" class="box-cliente box-campanha">
                            <div class="info">
                                <h3>${ value.nome_campanha }</h3>
                                <ul>
                                    <li>
                                        <div class="square"></div>
                                        <span>${ value.cliente.nome_fantasia != null ? value.cliente.nome_fantasia : value.cliente.razao_social }</span>
                                    </li>
                                    <li>
                                        <div class="square"></div>
                                        <span>Início: ${ dt_inicio.getDay() + '/' + (dt_inicio.getUTCMonth() + 1)  + '/' + dt_inicio.getFullYear() }</span>
                                    </li>
                                    <li>
                                        <div class="square"></div>
                                        <span>Fim: ${ dt_fim.getDay() + '/' + (dt_fim.getUTCMonth() + 1)  + '/' + dt_fim.getFullYear() }</span>
                                    </li>
                                    <li>
                                        <div class="square ${ value.deleted_at != null ? 'yellow' : 'green' }"></div>
                                        <span>${ value.deleted_at != null ? 'Inativa' : 'Ativa' }</span>
                                    </li>
                                </ul>
                            </div>
                            <div class="geral">
                                <div class="item">
                                    <i class="icone icone-user"></i>
                                    <i class="icone-arrow-down"></i>
                                    <span>08</span>
                                </div>
                                <div class="item">
                                    <i class="icone icone-circulo-estrela"></i>
                                    <i class="icone-arrow-down"></i>
                                    <span>${ total_pontos }</span>
                                </div>
                            </div>
                        </a>
                    `);

                });

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        });
    },

    filtrarUsuarios:function(){

        let filtros = new FormData();

        filtros.append('cod_campanha', $("#cod-campanha").val());

        if($("#filtro-usuarios-campanha-pdv").val() != '')
            filtros.append('pdv', $("#filtro-usuarios-campanha-pdv").val());

        if($("#filtro-usuarios-campanha-ilha").val() != '')
            filtros.append('ilha', $("#filtro-usuarios-campanha-ilha").val());

        if($("#filtro-usuarios-campanha-status").val() != '')
            filtros.append('status', $("#filtro-usuarios-campanha-status").val());

        if($("#filtro-usuarios-campanha-cadastro").val() != '')
            filtros.append('cadastro', $("#filtro-usuarios-campanha-cadastro").val());

        if($("#filtro-usuarios-campanha-busca").val() != '')
            filtros.append('busca', $("#filtro-usuarios-campanha-busca").val());


        $.ajax({
            url: "/sistema/campanhas/filtrar-usuarios-campanha",
            type: "POST",
            data: filtros,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

            },
            beforeSend: function() {
                $('#todos-usuarios-filtrado').empty();
                $('.loader').show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $.each(data, function(key, value){

                let dt_cadastro = new Date(value.data_cadastro);
                let nome_usuario = value.nome_usuario;
                nome_usuario = nome_usuario.replace(/_/g, ' ');



                $("#todos-usuarios-filtrado").append(`
                        <div class="item" status="${ value.status ? 'ativo' : 'bloqueado' }">
                            <ul class="item-head">
                                <li>
                                    <a id="open-dropdown" style="cursor: pointer">
                                        <img src="/img/icons-adm/bar-cinza.svg" alt="">
                                    </a>
                                    <h3>${ nome_usuario }</h3>
                                    <div class="dropdown">
                                        <ul>
                                            <li>
                                                <a href="${ window.location.pathname + '/usuario/' + value.id + '/' + limpaString(value.nome_usuario) }">
                                                    <i class="icone-lapis"></i>
                                                    <span>Detalhes</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <span>RE: ${ value.login }</span>
                                </li>
                                <li>
                                    <span>${value.pdv.nome_pdv}/${ value.ilha.replace(/_/g, ' ') }</span>
                                </li>
                                <li class="status-conclusao">
                                    <div class="status">
                                        <div class="square"></div>
                                        <span>${ value.status ? 'Concluído' : 'Não Concluído' }</span>
                                    </div>
                                    <i class="icone-arrow-down"></i>
					  </li>
					  <li class="status-ativo">
					  	<label for="">status</label>
						<div id="status-usuario-campanha" class="btn-switch ${ value.deleted_at ? '' : 'active' }">
							<input type="hidden" id="id-usuario" value="${ value.id }">
							<div class="square"></div>
						</div>
					  </li>
                            </ul>
                            <ul class="item-body">
                                <li>

                                </li>
                                <li>
                                ${value.status == 1 ? `
                                    <label for="">email</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/envelope.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ value.email }</span>
                                    </div>
                                ` : ''}
                                </li>
                                <li>
                                ${value.status == 1 ? `
                                    <label for="">telefone</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/celular.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ value.telefone != '' ? value.celular : value.telefone }</span>
                                    </div>
                                ` : ''}
                                </li>
                                <li>
                                ${value.status == 1 ? `
                                    <label for="">CONCLUSÃO DE CADASTRO asasd</label>
                                    <div class="info">
                                        <img src="/img/icons-adm/circulo-estrela.svg" alt="">
                                        <i class="seta icone-arrow-right"></i>
                                        <span>${ dt_cadastro.getDate() + '.' + (dt_cadastro.getUTCMonth() + 1)  + '.' + dt_cadastro.getFullYear() }</span>
                                    </div>
                                ` : ''}
                                </li>
                            </ul>
                        </div>
                    `);

            });

            $('.loader').hide();
            campanha.recuperarReferencia();
        });

    },

    filtrarResgates:function(){
        let filtros = new FormData();

        filtros.append('cod_campanha', $("#cod-campanha").val());

        if($("#filtro-resgates-campanha-status").val() != '')
            filtros.append('status', $("#filtro-resgates-campanha-status").val());

        if($("#filtro-resgates-campanha-dt-inicio").val() != '')
            filtros.append('dt_inicio', $("#filtro-resgates-campanha-dt-inicio").val());

        if($("#filtro-resgates-campanha-dt-fim").val() != '')
            filtros.append('dt_fim', $("#filtro-resgates-campanha-dt-fim").val());

        if($("#filtro-resgates-campanha-busca").val() != '')
            filtros.append('busca', $("#filtro-resgates-campanha-busca").val());


        $.ajax({
            url: "/sistema/campanhas/filtrar-resgates-campanha",
            type: "POST",
            data: filtros,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {

            },
            beforeSend: function() {
                $("#todos-resgates-filtrado").empty();
                $('.loader').show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
            }
        }).done(function(data) {
            $.each(data, function(key, value) {

                let created_at = new Date(value.created_at);
                let itens = "";

                $.each(value.carrinho.itens, function(key_i, value_i){
                    itens += `<span class="info-span">
                                   <i class="icone-arrow-right"></i>
                                   ${ value_i.produto ? value_i.produto.nome_produto : '' } - R$${ value_i.produto ? value_i.produto.valor_reais : '' }
                              </span> `;
                    });


                $("#todos-resgates-filtrado").append(`
                        <div class="item">
                            <ul class="item-head">
                                <li>
                                    <span>${ value.registro }</span>
                                </li>
                                <li>
                                    <span>RE - ${ value.user.login }</span>
                                </li>
                                <li>
                                    <span>Data - ${ created_at.getDate() + '/' + (created_at.getUTCMonth() + 1)  + '/' + created_at.getFullYear() }</span>
                                </li>
                                <li>
                                    <span>PDV - ${ value.user.pdv.nome_pdv }</span>
                                </li>
                                <li>
                                    <i class="icone icone-circulo-estrela"></i>
                                    <i class="seta icone-arrow-right"></i>
                                    <h4>${ value.valor_resgate }</h4>

                                    <i class="icone-arrow-down"></i>
                                </li>
                            </ul>
                            <ul class="item-body">
                                <li>
                                    <div class="title-list">
                                        <i class="icone-user"></i>
                                        <span>INF. DO USUÁRIOS</span>
                                    </div>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.nome_usuario }
                                    </span>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.celular != '' ? value.user.celular : value.user.telefone }
                                    </span>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.email }
                                    </span>
                                </li>
                                <li>
                                    <div class="title-list">
                                        <i class="icone-user"></i>
                                        <span>INF. DA ENTREGA</span>
                                    </div>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        CEP - ${ value.user.pdv.cep }
                                    </span>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.pdv.cidade } - ${ value.user.pdv.estado }
                                    </span>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.pdv.bairro }
                                    </span>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.user.pdv.logradouro }, ${ value.user.pdv.numero } ${ value.user.pdv.complemento }
                                    </span>
                                </li>
                                <li>
                                    <div class="title-list">
                                        <i class="icone-resgate"></i>
                                        <span>INF. D0 RESGATES</span>
                                    </div>
                                    `+itens+`
                                </li>
                                <li>
                                    <div class="title-list">
                                        <i class="icone-cartao"></i>
                                        <span>STATUS DA ENTREGA</span>
                                    </div>
                                    <span class="info-span">
                                        <i class="icone-arrow-right"></i>
                                        ${ value.status_carrinho.descricao_status }
                                    </span>
                                </li>
                            </ul>
                        </div>
                    `);


            });

            $('.loader').hide();
            campanha.recuperarReferencia();
        });

    },


    adicionarRemoverProduto:function(checkbox){
        let produto = new FormData();
        produto.append('cod_produto', checkbox.getAttribute('id'));

        if(checkbox.classList.value.includes('checked')){ // adicionar
            $.ajax({
                url: window.location.pathname + "/adicionar-produto",
                type: "POST",
                data: produto,
                async: true,
                contentType: false,
                processData: false,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                },
                error: function (x, t, m) {
                    console.log("erro=> " + m);
                    swal({
                        type: 'error',
                        title: 'Ops! Algo de errado aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });

        } else { // remover
            $.ajax({
                url: window.location.pathname + "/remover-produto",
                type: "POST",
                data: produto,
                async: true,
                contentType: false,
                processData: false,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                },
                error: function (x, t, m) {
                    console.log("erro=> " + m);
                    swal({
                        type: 'error',
                        title: 'Ops! Algo de errado aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });
        }
    },


    adicionarRemoverProdutoLote:function(checkbox){
        let categoria = new FormData();
        categoria.append('cod_categoria', checkbox.getAttribute('id'));

        if(checkbox.classList.value.includes('checked')){ // adicionar
            $.ajax({
                url: window.location.pathname + "/adicionar-produto-lote",
                type: "POST",
                data: categoria,
                async: true,
                contentType: false,
                processData: false,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                },
                error: function (x, t, m) {
                    console.log("erro=> " + m);
                    swal({
                        type: 'error',
                        title: 'Ops! Algo de errado aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });

        } else { // remover
            $.ajax({
                url: window.location.pathname + "/remover-produto-lote",
                type: "POST",
                data: categoria,
                async: true,
                contentType: false,
                processData: false,
                timeout: 10000,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {

                },
                error: function (x, t, m) {
                    console.log("erro=> " + m);
                    swal({
                        type: 'error',
                        title: 'Ops! Algo de errado aconteceu!',
                    }).then(function () {
                        location.reload();
                    });
                }
            });
        }

    },

    recuperarReferencia:function(){
        $(".btn-switch").on("click", function() {
            $(this).toggleClass("active");
        })

        let dropdown = Array.from(document.querySelectorAll('#open-dropdown'));

        dropdown.forEach(function(drop){
            drop.addEventListener('click',function(){
                drop.parentElement.querySelector('.dropdown').classList.toggle('active')
            })
        })

	$(".todos-usuarios .item .item-head").on("click", function() {
		if($(this).parent().hasClass('expand-item')){
			$(".todos-usuarios .item").removeClass("expand-item");
		}else{
			$(".todos-usuarios .item").removeClass("expand-item");
			$(this).parents(".item").addClass("expand-item");
		}

	})

	$(".area-geral-campanha .cont-geral .tab-pane .dados-resgates-gerais .item .item-head").on("click", function() {
		console.log($(this).parents('.item'))
		console.log($(this).parents('.item').hasClass('item-expand'))
		if($(this).parents('.item').hasClass('item-expand')){

			$(this).parents(".dados-resgates-gerais").find(".item").removeClass("item-expand");
		}else{
			$(this).parents(".dados-resgates-gerais").find(".item").removeClass("item-expand");
			$(this).parents(".item").addClass("item-expand");
		}
		// $(this).parents(".dados-resgates-gerais").find(".item").removeClass("item-expand");
		// $(this).parents(".item").addClass("item-expand");
	})
    }

};


$("#tab-usuarios").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#usuarios").fadeIn();
    campanha.filtrarUsuarios();
});

$("#tab-resgates").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#resgates").fadeIn();
    campanha.filtrarResgates();
});

$("#tab-loja").click(function() {
    $(".tab-pane").hide();
    $(".tab-pane#loja").fadeIn();
});

$(document).on("click", "#campanha-pdvs .check", function() {
    $(this).toggleClass("checked");

    if($(this).hasClass("checked")){
        arrPdvsCadastrar.push(this.dataset.cod);
        arrPdvsRemover.splice( $.inArray(this.dataset.cod, arrPdvsRemover), 1);
    } else {
        arrPdvsCadastrar.splice( $.inArray(this.dataset.cod, arrPdvsCadastrar), 1);
        arrPdvsRemover.push(this.dataset.cod);
    }
});


$(".check-produtos").click(function(){
    if($("#perfil-adm").val() == 1 || $("#perfil-adm").val() == 2){
        let valorTotal = $(this).parents(".box-loja").find(".total").text();
        $(this).toggleClass("checked");

        campanha.adicionarRemoverProdutoLote(this); // chama função para adicionar ou remover em lote

        if($(this).hasClass("checked")) {
            $(this).parents(".cards").find(".todos-cards .check").addClass("checked");
            $(this).parents(".box-loja").find(".qtd-marcados").text(valorTotal);
        } else {
            $(this).parents(".cards").find(".todos-cards .check").removeClass("checked");
            $(this).parents(".box-loja").find(".qtd-marcados").text("0");
        }
    }
})

$(".box-loja .cards .todos-cards ul li .check").on("click", function() {
    if($("#perfil-adm").val() == 1 || $("#perfil-adm").val() == 2){
        $(this).toggleClass("checked");
        var qtdMarcados = $(this).parents(".box-loja .todos-cards").find(".checked").length;
        let qtdProdutos = $(this).parents(".box-loja .todos-cards").find(".check").length;

        $(this).parents(".box-loja").find(".qtd-marcados").text(qtdMarcados);

        if(qtdMarcados == qtdProdutos)
            $(this).parent().parent().parent().parent().find(".title .check-all").addClass("checked")
        else
            $(this).parent().parent().parent().parent().find(".title .check-all").removeClass("checked");

        campanha.adicionarRemoverProduto(this); // chama função para adicionar ou remover o produto
    }
})

$("#filtro-usuarios-campanha-pdv").trigger('change');
$("#filtro-usuarios-campanha-ilha").trigger('change');
$("#filtro-usuarios-campanha-status").trigger('change');
$("#filtro-usuarios-campanha-cadastro").trigger('change');
$(document).on("change", "#filtro-usuarios-campanha-pdv, #filtro-usuarios-campanha-ilha, #filtro-usuarios-campanha-status, #filtro-usuarios-campanha-cadastro", function(){
    campanha.filtrarUsuarios();
});
$(document).on("keyup", "#filtro-usuarios-campanha-busca", function () {
    clearTimeout(campanha.filtrarUsuariosTimeout);

    campanha.filtrarUsuariosTimeout = setTimeout(() => {
        campanha.filtrarUsuarios()
    }, 800);
});


$("#filtro-resgates-campanha-status").trigger('change');
$(document).on("change", "#filtro-resgates-campanha-status", function(){
    campanha.filtrarResgates();
});
$(document).on("keyup", "#filtro-resgates-campanha-dt-inicio", function(){
    if($("#filtro-resgates-campanha-dt-inicio") != null && $("#filtro-resgates-campanha-dt-inicio").length >= 10)
        campanha.filtrarResgates();
});
$(document).on("keyup", "#filtro-resgates-campanha-dt-fim", function(){
    if($("#filtro-resgates-campanha-dt-fim") != null && $("#filtro-resgates-campanha-dt-fim").length >= 10)
        campanha.filtrarResgates();
});
$(document).on("keyup", "#filtro-resgates-campanha-busca", function(){
    clearTimeout(campanha.filtrarResgatesTimeout);

    campanha.filtrarResgatesTimeout = setTimeout(() => {
        campanha.filtrarResgates
    }, 800);
});


$(document).on("click", "#status-usuario-campanha", function() {
    if($(this).hasClass("active"))
        usuarios.ativar(this.querySelector('input').value);

    else
        usuarios.bloquear(this.querySelector('input').value);

});


$(document).on('click',".option-cod-clinte", function(){
    let cliente = new FormData();

    cliente.append('id', $("#campanha-cliente-id").val());

    $(".todos-pdvs").html('');

    campanha.recuperarReferencia();

    $.ajax({
        url: "/sistema/campanhas/listar-pdvs",
        type: "POST",
        data: cliente,
        async: true,
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {

            if(!data.erro){
                if(data.pdvs.length > 0){

                    $.each(data.pdvs, function(key, value){
                        $(".todos-pdvs").append(`
                            <div class="check" data-cod="${value.cod_pdv}">
                                <div class="square"></div>
                                <span>${value.nome_pdv}</span>
                            </div>

                        `);
                    });
                } else {
                    $(".todos-pdvs").append(`
                        <small>Nenhum PDV encontrado</small>
                    `);
                }
            }

        },
        error: function (x, t, m) {
            console.log("erro=> " + m);
        }
    });

});

$('#cadastro-campanha').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        campanha_nome: {
            required: true
        },
        campanha_chave: {
            required: true
        },
        campanha_inicio: {
            required: true,
            minlength: 10
        },
        campanha_fim: {
            required: true,
            minlength: 10
        },
        titulo_regulamento: {
            required: true
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {
        if(arrPdvsCadastrar.length == 0){
            alert('Adicione pelo menos 1 pdv');
            return;
        }

        if($("#campanha-regulamento")[0].files[0] == undefined){
            alert('Adicione o regulamento!');
            $("#campanha-regulamento").focus();
            return;
        }

        let campanha = new FormData();

        campanha.append('cliente_id', $("#campanha-cliente-id").val());
        campanha.append('pdvs', JSON.stringify(arrPdvsCadastrar));
        campanha.append('nome_campanha', $("#campanha-nome").val());
        campanha.append('super', $("#campanha-tipo").val());
        campanha.append('chave', $("#campanha-chave").val());
        campanha.append('dt_inicio', $("#campanha-inicio").val());
        campanha.append('dt_fim', $("#campanha-fim").val());
        campanha.append('validade_pontos', $("#validade-pontos").val());
        campanha.append('arquivo', $("#campanha-regulamento")[0].files[0]);
        campanha.append('valor_pontos', $("#valor-pontos").maskMoney('unmasked')[0]);
        campanha.append('titulo_regulamento', $("#titulo-regulamento").val());

        if($("#campanha-logo")[0].files[0] != undefined)
            campanha.append('imagem', $("#campanha-logo")[0].files[0]);

        if($(".check-disponivel").hasClass('checked'))
            campanha.append('fale_conosco', 1);

        if($(".check-temporaria").hasClass('checked'))
            campanha.append('pontos_temporarios', 1);


        $.ajax({
            url: "/sistema/campanhas/adicionar-campanha",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!campanha.cadastroOnProgress){
                            campanha.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#cadastro-campanha button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        campanha.cadastroOnProgress = false
                    }, false);
                    // myXhr.upload.addEventListener("error", onErrorUpload, false);
                }
                return myXhr;
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.chave) {
                    alert('Esta chave já existe!');
                    $("#campanha-chave").focus();

                    btnHelpers.setBtnCadastroDefault("#cadastro-campanha button[type='submit']");
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});


$('#editar-campanha').submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        campanha_nome: {
            required: true
        },
        campanha_chave: {
            required: true
        },
        campanha_inicio: {
            required: true,
            minlength: 10
        },
        campanha_fim: {
            required: true,
            minlength: 10
        },
        titulo_regulamento: {
            required: true
        }
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let campanha = new FormData();

        campanha.append('cod_campanha', $("#cod-campanha").val());
        campanha.append('adicionarPdvs', JSON.stringify(arrPdvsCadastrar));
        campanha.append('removerPdvs', JSON.stringify(arrPdvsRemover));
        campanha.append('nome_campanha', $("#campanha-nome").val());
        campanha.append('super', $("#campanha-tipo").val());
        campanha.append('chave', $("#campanha-chave").val());
        campanha.append('dt_inicio', $("#campanha-inicio").val());
        campanha.append('dt_fim', $("#campanha-fim").val());
        campanha.append('validade_pontos', $("#validade-pontos").val());
        campanha.append('valor_pontos', $("#valor-pontos").maskMoney('unmasked')[0]);
        campanha.append('titulo_regulamento', $("#titulo-regulamento").val());

        if($("#campanha-regulamento")[0].files[0] != undefined)
            campanha.append('arquivo', $("#campanha-regulamento")[0].files[0]);

        if($("#campanha-logo")[0].files[0] != undefined)
            campanha.append('imagem', $("#campanha-logo")[0].files[0]);

        if($(".check-disponivel").hasClass('checked'))
            campanha.append('fale_conosco', 1);
        else
            campanha.append('fale_conosco', 0);

        if($(".check-temporaria").hasClass('checked'))
            campanha.append('pontos_temporarios', 1);
        else
            campanha.append('pontos_temporarios', 0);

        $.ajax({
            url: "/sistema/campanhas/editar-campanha",
            type: "POST",
            data: campanha,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            xhr: function() {
                var myXhr = $.ajaxSettings.xhr();
                if(myXhr.upload){
                    myXhr.upload.addEventListener('progress', (e) => {
                        if(!campanha.cadastroOnProgress){
                            campanha.cadastroOnProgress = true;

                            btnHelpers.setBtnCadastroLoading("#editar-campanha button[type='submit']");
                        }
                    }, false);
                    myXhr.upload.addEventListener("load", (e) => {
                        campanha.cadastroOnProgress = false
                    }, false);
                    // myXhr.upload.addEventListener("error", onErrorUpload, false);
                }
                return myXhr;
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                if(x.responseJSON.errors.chave) {
                    alert('Esta chave já existe!');
                    $("#campanha-chave").focus();

                    btnHelpers.setBtnCadastroDefault("#editar-campanha button[type='submit']");
                } else {
                    console.log("erro=> " + m);
                    $("#modal-erro").show();
                }
            }
        });

    }
});

$("#campanha-busca").keyup(function(){
    clearTimeout(campanha.buscaCampanhaTimeout);

    campanha.buscaCampanhaTimeout = setTimeout(() => {
        campanha.buscaCampanhas()
    }, 800);
});


 /** Alisson */

 function filterTipo(filtro, id){
    let listUl = Array.from(document.querySelectorAll(filtro))
    listUl.forEach(function(list){

        let ulId = parseInt(list.dataset.cod_movimentacao)

        if(ulId == id){
            list.style.display = "flex"
        }else{
            id == 0 ? list.style.display = "flex" : list.style.display = "none"
        }
    })
}

let selectTipo = Array.from(document.querySelectorAll('.select-custom .dropdown li'))
let filtroDetalheUsuario = document.querySelector('#filtro-campanhas-detalhes-usuario-busca')

if(filtroDetalheUsuario != null){
    filtroDetalheUsuario.addEventListener('keyup', function(){
        let valorDigitado = filtroDetalheUsuario.value.toString()

        let listUl = Array.from(document.querySelectorAll('.container-line-user ul'))
        let keys = ['referencia', 'data_efetivacao', 'nome_arquivo']
        listUl.forEach(function(list){

            let listObjs = Array.from(list.querySelectorAll('li')).map(function(itemList, index){
                let itens = Array.from(itemList.querySelectorAll('.container-item p')).map(function(text){
                    let el = text.textContent.trim().toString().toLowerCase();
                    let item = keys[index]

                    let obj = { [item]: [el] }

                    return obj
                })
                return itens
            })

            if(JSON.stringify(listObjs).includes(valorDigitado)){
                list.style.display.includes('none') ? null : list.style.display = "flex";
            }else{
                list.style.display = "none"
            }

            if(valorDigitado == ''){
                filterTipo('.container-line-user ul', 0)
            }
        })
    })
}


selectTipo.forEach(function(select){
    select.addEventListener('click', function(){
        let id = parseInt(select.dataset.cod_opcao)
        filterTipo('.container-line-user ul', id)
    })
})


$("#alterar-senha-detalhe-usuario-campanha").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        detalhe_usuario_nova_senha: 'required'
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        let usuario = new FormData();

        usuario.append('id', $("#detalhe-usuario-id").val());
        usuario.append('nova_senha', $("#detalhe-usuario-nova-senha").val());

        $.ajax({
            url: "/sistema/campanhas/usuario/alterar-senha",
            type: "POST",
            data: usuario,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                let modal = document.querySelector('.modal-alterar-senha');
                modal.classList.toggle('active');
                $("#detalhe-usuario-nova-senha").val("");
                swal({
                    type: 'success',
                    title: 'Senha Alterada!',
                    text: ''
                });
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                swal({
                    type: 'error',
                    title: 'Ops! Algo aconteceu!',
                }).then(function () {
                    location.reload();
                });
            }
        });

    }
});
