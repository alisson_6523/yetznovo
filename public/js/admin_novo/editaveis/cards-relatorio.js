var dados = {};
var query = {};

function geral(filtro) {
    query = Object.assign({}, filtro);

    carregarMovimentacao(filtro);

    let toggleResumo = document.querySelector('.toggle-resumo');
    let resumo = document.querySelector('.card-resumo');
    let main = document.querySelector('.area-conteudo-modulo');
    let topo = document.querySelector('.topo');
    let todosCards = document.querySelector('.todos-cards')

    if (toggleResumo) {

        toggleResumo.addEventListener('click', function () {
            resumo.classList.toggle('active')
            if (resumo.classList.value.includes('active')) {
                main.style.overflow = 'hidden'
                topo.style.width = '91vw'
                todosCards.style.width = ' 5019px'
                window.screen.availWidth <= 1366 ? document.querySelector('.card-usuarios').style.marginLeft = '870px' : null
                window.screen.availWidth <= 1366 ? document.querySelector('.card-permissao').style.marginLeft = '870px' : null
            } else {
                window.screen.availWidth <= 1366 ? document.querySelector('.card-usuarios').style.marginLeft = '0px' : null
                window.screen.availWidth <= 1366 ? document.querySelector('.card-permissao').style.marginLeft = '0px' : null
                setTimeout(function () {
                    todosCards.style.width = "100%"
                }, 200)
            }
        });

        let toggleUsuario = document.querySelector('.toggle-usuario');

        toggleUsuario.addEventListener('click', function () {
            let usuario = document.querySelector('.card-usuarios');
            let arraySpan = Array.from(document.querySelectorAll('.container-card-resgatados .card-usuarios .container-card-ilha .loader-user .container-width .span-width'))
            let arrayPrcentagem = Array.from(document.querySelectorAll('.container-card-resgatados .card-usuarios .container-card-ilha .loader-user .container-width .porcentagem-user'))
            main.style.overflow = 'hidden'
            topo.style.width = '91vw'
            usuario.classList.toggle('active')
            toggleUsuario.classList.toggle('active')

            if (toggleUsuario.classList.value.includes('active')) {
                todosCards.style.width = ' 5019px'
            } else {
                setTimeout(function () {
                    todosCards.style.width = "100%"
                }, 200)
            }

            arraySpan.forEach(function (span) {
                let spanWidth = span.dataset.width

                setTimeout(function () {
                    span.style.width = spanWidth + "%"
                }, 400)
            })


            if (document.querySelector('.container-card-ilha').dataset.start == "true") {
                arrayPrcentagem.forEach(function (porcentagem) {
                    let total = parseInt(porcentagem.dataset.porc)

                    var animationLength = 1000; // 1 second
                    var counter = 0;
                    var counterEnd = total;

                    function animate() {
                        porcentagem.innerHTML = counter++ + "%"
                        animationLength *= 0.3; // not exponential - but speed will half on each iteration
                        if (counter <= counterEnd) { // Stopping condition
                            setTimeout(animate, animationLength);
                        }
                    }

                    animate()
                })
            }

            document.querySelector('.container-card-ilha').dataset.start = "false"

        })


        let toggleResgates = document.querySelector('.toggle-resgates')


        toggleResgates.addEventListener('click', function () {
            let resgates = document.querySelector('.card-resgates')
            let arraySpanResgates = Array.from(document.querySelectorAll('.container-card-resgatados .card-resgates .percentual-men-women .loader-user .container-width .span-width'))
            let arrayPrcentagemResgates = Array.from(document.querySelectorAll('.container-card-resgatados .card-resgates .percentual-men-women .loader-user .container-width .porcentagem-user'))
            resgates.classList.toggle('active')

            toggleResgates.classList.toggle('active')
            if (toggleResgates.classList.value.includes('active')) {
                document.querySelector('.card-permissao').style.boxShadow = '0px 0px 0 rgba(255,255,255,0.0)'
                document.querySelector('.card-ativos').style.boxShadow = '0px 0px 0 rgba(255,255,255,0.0)'
                window.screen.availWidth <= 1366 ? document.querySelector('.card-ativos').style.left = '900px' : null

                setTimeout(function () {
                    document.querySelector('.container-wonem').style.left = '331px'
                    document.querySelector('.container-men').style.left = '650px'
                }, 200)
            } else {
                document.querySelector('.card-permissao').style.boxShadow = '4px 4px 0 rgba(109,109,109,0.09)'
                document.querySelector('.card-ativos').style.boxShadow = '4px 4px 0 rgba(109,109,109,0.09)'
                document.querySelector('.container-wonem').style.left = '0px'
                document.querySelector('.container-men').style.left = '0px'
                window.screen.availWidth <= 1366 ? document.querySelector('.card-ativos').style.left = '0px' : null
            }
            arraySpanResgates.forEach(function (span) {
                let spanWidth = span.dataset.width

                setTimeout(function () {
                    span.style.width = spanWidth + "%"
                }, 400)
            })


            if (document.querySelector('.percentual-men-women').dataset.start == "true") {
                arrayPrcentagemResgates.forEach(function (porcentagem) {
                    let total = parseInt(porcentagem.dataset.porc)

                    var animationLength = 1000; // 1 second
                    var counter = 0;
                    var counterEnd = total;

                    function animate() {
                        porcentagem.innerHTML = counter++ + "%"
                        animationLength *= 0.4; // not exponential - but speed will half on each iteration
                        if (counter <= counterEnd) { // Stopping condition
                            setTimeout(animate, animationLength);
                        }
                    }

                    animate()
                })
            }

            document.querySelector('.percentual-men-women').dataset.start = "false"
        })

        let togglePaginas = document.querySelector('.todos-relatorios .container-relatorio-acesso .relatorio-graficos-acesso .container-dir')

        window.onscroll = function () {

            if (window.scrollY > 158) {
                if (document.querySelector('.container-paginas-acesso').dataset.start == 'true') {
                    let arraySpanAcessos = Array.from(document.querySelectorAll('.loader-acessos .container-width  .span-width'))
                    let acesso = Array.from(document.querySelectorAll('.loader-acessos .container-width .porcentagem-acessos'))


                    arraySpanAcessos.forEach(function (span) {
                        let spanWidth = span.dataset.width
                        setTimeout(function () {
                            span.style.width = spanWidth + "%"
                        }, 400)
                    })

                    acesso.forEach(function (porcentagem) {
                        let total = parseInt(porcentagem.dataset.porc)

                        var animationLength = 1000; // 1 second
                        var counter = 0;
                        var counterEnd = total;

                        function animate() {
                            porcentagem.innerHTML = counter++ + "%"
                            animationLength *= 0.5; // not exponential - but speed will half on each iteration
                            if (counter <= counterEnd) { // Stopping condition
                                setTimeout(animate, animationLength);
                            }
                        }

                        animate()
                    })
                }

                document.querySelector('.container-paginas-acesso').dataset.start = 'false'
            }
        }


    }
}

function gerarFiltros() {
    let cliente = $("#cliente-selecionado").attr('data-cliente-id');

    let campanha = $("#campanha-selecionada").attr('data-campanha-id');

    $("#campanhas li").each(function () {

        if ($(this).attr('data-cliente-id') == cliente || $(this).attr('data-campanha-id') == 0) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });

    $("#pdv li").each(function () {

        let campanhas = JSON.parse($(this).attr('data-campanhas-id'));

        if (campanhas.includes(parseInt(campanha)) || $(this).attr('data-pdv-id') == 0) {
            $(this).css('display', 'block');
        } else {
            $(this).css('display', 'none');
        }
    });
}

function carregarMovimentacao(filtro) {
    $.post('/sistema/relatorios/carregar-movimentacao', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['cargas'] = data.cargas;
            dados['resgates'] = data.resgates;

            carregarUsuarios(filtro);
        }
    });
}

function carregarUsuarios(filtro) {
    $.post('/sistema/relatorios/carregar-usuarios', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['usuarios'] = data.usuarios;

            carregarProdutosResgatados(filtro)
        }
    });
}

function carregarProdutosResgatados(filtro) {
    $.post('/sistema/relatorios/carregar-produtos-resgatados', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['produtosResgatados'] = data.produtosResgatados;

            carregarUsuariosMensal(filtro);
        }
    });
}

function carregarUsuariosMensal(filtro) {
    filtro['data.formatted'] = {
        '$gte': gerarData(new Date(), -30)
    };

    $.post('/sistema/relatorios/carregar-usuarios-mensal', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['usuariosMensal'] = data.usuariosMensal;

            carregarUsuariosSemanal(filtro);
        }
    });
}

function carregarUsuariosSemanal(filtro) {
    filtro['data.formatted'] = {
        '$gte': gerarData(new Date(), -7)
    };

    $.post('/sistema/relatorios/carregar-usuarios-semanal', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['usuariosSemanal'] = data.usuariosSemanal;

            carregarUsuariosDiario(filtro);
        }
    });
}

function carregarUsuariosDiario(filtro) {
    filtro['data.formatted'] = {
        '$gte': gerarData(new Date(), -1)
    };

    $.post('/sistema/relatorios/carregar-usuarios-diario', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['usuariosDiario'] = data.usuariosDiario;

            filtro['data.formatted'] = {
                '$gte': $('#range').data('daterangepicker').startDate._d,
                '$lt': $('#range').data('daterangepicker').endDate._d
            };

            carregarLogins(filtro);
        }
    });
}

function carregarLogins(filtro) {
    $.post('/sistema/relatorios/carregar-logins', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['logins'] = data.logins;
            dados['logouts'] = data.logouts;

		  carregarPaginasAcessadas(filtro);
        }
    });
}

function carregarPaginasAcessadas(filtro) {
    $.post('/sistema/relatorios/carregar-paginas-acessadas', {
        _token: $("#csrf_token").val(),
        filtro: JSON.stringify(filtro)
    }, function (data) {
        if (data.error) {
            console.log(data);
        } else {
            dados['paginas'] = data.paginas;

		  carregarCategoriasAcessadas(filtro);
        }
    });
}

function carregarCategoriasAcessadas(filtro) {
	$.post('/sistema/relatorios/carregar-categorias-acessadas', {
		_token: $("#csrf_token").val(),
		filtro: JSON.stringify(filtro)
	}, function (data) {
		if (data.error) {
			console.log(data);
		} else {
			dados['categorias'] = data.categorias;

			carregarProdutosAcessados(filtro);
		}
	});
}

function carregarProdutosAcessados(filtro) {
	$.post('/sistema/relatorios/carregar-produtos-acessados', {
		_token: $("#csrf_token").val(),
		filtro: JSON.stringify(filtro)
	}, function (data) {
		if (data.error) {
			console.log(data);
		} else {
			dados['produtos'] = data.produtos;

			preecherDados(dados);
		}
	});
}

function preencherMovimantacao(movimentacoes) {
    let pontosDistribuidos = totalPontosDistribuidos(movimentacoes.cargas);
    let pontosResgatados = totalPontosResgatados(movimentacoes.resgates);
    let pontosDisponiveis = pontosDistribuidos - pontosResgatados;

    $("#pts-distribuidos").html(pontosDistribuidos.toLocaleString('pt-BR'));
    $("#pts-resgatados").html(pontosResgatados.toLocaleString('pt-BR'));
    $("#pts-disponiveis").html((pontosDisponiveis > 0 ? pontosDisponiveis : 0).toLocaleString('pt-BR'));
    pontosDistribuidosMensalmente(movimentacoes.cargas);
    pontosResgatadosMensalmente(movimentacoes.resgates);
}

function preencherUsuarios(usuarios) {
    let completos = usuariosCompletos(usuarios);

    $("#total-usuarios").html(usuarios.length.toLocaleString('pt-BR'));
    $("#usuarios-completos").html(completos.toLocaleString('pt-BR'));
    $("#usuarios-incompletos").html((usuarios.length - completos).toLocaleString('pt-BR'));
    usuariosPorIlha(usuarios);

    let notificacoesPermetidas = permissaoNotificacoes(usuarios);

    $("#notificacao-autorizada").html(notificacoesPermetidas.toLocaleString('pt-BR'));
    $("#notificacao-nao-autorizada").html((completos - notificacoesPermetidas).toLocaleString('pt-BR'));
}

function preencherProduosResgatados(produtosResgatados) {
    $("#total-resgates").html(produtosResgatados.length.toLocaleString('pt-BR'));
    rankingProdutosResgatados(produtosResgatados);
}

function preencherUsuariosAtivos(usuariosAtivos) {
    $("#usuarios-ativos-mensal").html(_.size(_.groupBy(usuariosAtivos.usuariosMensal, function (usuario) {
        return usuario.usuario.id
    })));
    $("#usuarios-ativos-semanal").html(_.size(_.groupBy(usuariosAtivos.usuariosSemanal, function (usuario) {
        return usuario.usuario.id
    })));
    $("#usuarios-ativos-diario").html(_.size(_.groupBy(usuariosAtivos.usuariosDiario, function (usuario) {
        return usuario.usuario.id
    })));
}

function preencherLogins(logins) {
    let usuariosQueLogaram = usuariosRealizaramLogin(logins);

    $("#usuarios-que-logaram").html(usuariosQueLogaram).toLocaleString('pt-BR');
    $("#usuarios-recorrentes").html(usuariosRecorrentes(logins).toLocaleString('pt-BR'));
    $("#total-logins").html(logins.length.toLocaleString('pt-BR'));
    $("#sessoes-por-usuario").html(logins.length > 0 ? (logins.length / usuariosQueLogaram).toFixed(2).toLocaleString('pt-BR') : 0);
}

function preencherAcessos(acessos) {
    $("#paginas-por-sessao").html(acessos.logins.length > 0 ? (acessos.paginas.length / acessos.logins.length).toFixed(2).toLocaleString('pt-BR') : 0);

    $("#paginas-visitadas").html(acessos.paginas.length.toLocaleString('pt-BR'));

    paginasMaisAcessadas(acessos.paginas);
    categoriasMaisAcessadas(acessos.categorias);
    produtosMaisAcessados(acessos.produtos);

    acessosPorDispositivo(acessos.logins)

    google.charts.load("current", {packages: ["corechart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        let logins = _.reject(acessos.logins, function (login) {
            return !login.hasOwnProperty('os') || login.os == null
        });
        let loginsAgrupados = _.groupBy(logins, function (login) {
            return login.os
        });

        let ios = 0;
        let android = 0;

        if (loginsAgrupados.hasOwnProperty('ios')) {
            ios = loginsAgrupados.ios.length;
        }

        if (loginsAgrupados.hasOwnProperty('android')) {
            android = loginsAgrupados.android.length;
        }

        $("#acessos-ios").html(ios + " ACESSOS");
        $("#acessos-android").html(android + " ACESSOS");
        $("#total-acessos-app").html(_.size(logins));
        $("#total-usuarios-app").html(_.size(_.groupBy(logins, function (login) {
            return login.usuario.id
        })));

        var data = google.visualization.arrayToDataTable([
            ['Sistema Operacional', 'Logins'],
            ['Android', android],
            ['iOS', ios],
        ]);


        var options = {
            title: '',
            legend: 'none',
            pieHole: 0.8,
            colors: ['#aac148', '#f77e0b'],
            fontSize: 14,
            legend: {position: 'none'},
            pieSliceText: 'none',
            tooltip: {trigger: 'none'},
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));

        chart.draw(data, options);
    }

    google.charts.load('current', {
        'packages': ['corechart']
    });

    google.charts.setOnLoadCallback(drawSeriesChart);

    function drawSeriesChart() {

        let loginsAgrupadosPorDia = _.groupBy(acessos.logins, function (login) {
            return login.data.dayOfWeek
        });

        let valores = [
            ['ID', 'Hora', 'Dia', '', 'Acessos']
        ]

        Object.keys(loginsAgrupadosPorDia).map(function (dia) {
            let loginsAgrupadosPorHora = _.groupBy(loginsAgrupadosPorDia[dia], function (login) {
                return login.data.hour
            });

            Object.keys(loginsAgrupadosPorHora).map(function (hora) {
                valores.push(['', parseInt(hora), parseInt(dia), "item", loginsAgrupadosPorHora[hora].length])
            });
        });

        if (valores.length == 1) {
            valores.push(['', 0, 0, 'item', 0]);
        }

        var data = google.visualization.arrayToDataTable(valores);
        let widthGraficoBall = window.screen.availWidth <= 1366 ? 1000 : 580
        let options = {
            title: '',
            hAxis: {
                title: '',
                ticks: [
                    {
                        v: 0,
                        f: '0AM'
                    },
                    {
                        v: 3,
                        f: '3AM'
                    },
                    {
                        v: 6,
                        f: '6AM'
                    },
                    {
                        v: 9,
                        f: '9AM'
                    },
                    {
                        v: 12,
                        f: '12PM'
                    },
                    {
                        v: 15,
                        f: '3PM'
                    },
                    {
                        v: 18,
                        f: '6PM'
                    },
                    {
                        v: 21,
                        f: '9PM'
                    },
                    {
                        v: 24,
                        f: '0PM'
                    },
                ]

            },

            vAxis: {
                title: '',
                ticks: [
                    {
                        v: 6,
                        f: 'SAB'
                    },
                    {
                        v: 5,
                        f: 'SEX'
                    },
                    {
                        v: 4,
                        f: 'QUI'
                    },
                    {
                        v: 3,
                        f: 'QUA'
                    },
                    {
                        v: 2,
                        f: 'TER'
                    },
                    {
                        v: 1,
                        f: 'SEG'
                    },
                    {
                        v: 0,
                        f: 'DOM'
                    }
                ]
            },

            series: {
                'item': {color: '#f7a81d'}
            },

            tooltip: {trigger: 'none'},

            bubble: {
                textStyle: 'none'
            },

            legend: 'none',

            colorAxis: {
                colors: "#000"
            },


            backgroundColor: {
                fill: "#fff"
            },

            bubble: {
                stroke: "#f7a81d"
            },

            chartArea: {
                left: 85,
                width: widthGraficoBall
            }
        };

        var chart = new google.visualization.BubbleChart(document.getElementById('series_chart_div'));
        chart.draw(data, options);
    }

    google.charts.load('current', {packages: ['corechart', 'line']});
    google.charts.setOnLoadCallback(drawBasicLine);

    function drawBasicLine() {

        var data = new google.visualization.DataTable();

        data.addColumn('string', 'Data');
        data.addColumn('number', 'Paginas');

        let paginasAgrupadas = _.groupBy(acessos.paginas, function (pagina) {
            return pagina.data.formatted.split(' ')[0]
        });

        let x = [];
        let valores = [];

        Object.keys(paginasAgrupadas).map(function (key) {
            x.push(key);
            valores.push([new Date(key).toLocaleDateString(), paginasAgrupadas[key].length]);
        });

        let diaComMaisVisitas = _.size(paginasAgrupadas) == 0 ? 0 : _.max(paginasAgrupadas, function (pagina) {
            return pagina.length;
        }).length;

        let valorBase = (diaComMaisVisitas / 100 * 20).toFixed(0);

        let y = [
            0,
            valorBase * 1,
            valorBase * 2,
            valorBase * 3,
            valorBase * 4,
            diaComMaisVisitas
        ]

        data.addRows(valores);
        let widthGrafico = window.screen.availWidth <= 1366 ? 1000 : 580
        var options = {

            legend: 'none',

            vAxis: {
                ticks: y,
            },

            hAxis: {
                ticks: x
            },

            colors: ['#e3547f',],

            chartArea: {

                left: 75,
                width: widthGrafico
            }
        };

        var chart = new google.visualization.LineChart(document.getElementById('line_top_x'));

        chart.draw(data, options);
    }

    let paginasAgrupadas = _.groupBy(acessos.paginas, function (pagina) {
        return pagina.sessao
    });
    let tempoTotal = 0;
    let numeroPaginas = 1;

    Object.keys(paginasAgrupadas).map(function (key) {
        let logout = _.find(dados.logouts, function (logout) {
            return logout.sessao == key
        });

        if (logout != undefined && logout != null) {
            tempoTotal += logout.tempo;
            numeroPaginas += _.size(paginasAgrupadas[key]);
        }
    });

    $('#tempo-medio-pagina').html((tempoTotal / numeroPaginas).toLocaleString('pt-BR'));
}

function preecherDados(elementos) {
    gerarFiltros();

    document.querySelector('.container-card-ilha').dataset.start = "true"
    document.querySelector('.percentual-men-women').dataset.start = "true"
    document.querySelector('.container-paginas-acesso').dataset.start = 'true'

    preencherMovimantacao(elementos);
    preencherUsuarios(elementos.usuarios);
    preencherProduosResgatados(elementos.produtosResgatados);
    preencherUsuariosAtivos(elementos);
    preencherLogins(elementos.logins);
    preencherAcessos(elementos);

    $("main .area-conteudo-modulo .topo").css('display', 'flex');
    $(".todos-cards").css('display', 'block');
    $(".todos-relatorios").css('display', 'block');
    $(".area-conteudo-modulo .loader").css('display', 'none');
}

function totalPontosDistribuidos(cargas) {
    let total = 0;

    if (cargas.length > 0) {
        cargas.map(carga => {
            total += parseInt(carga.carga.valor);
        })
    }

    return total;
}

function totalPontosResgatados(resgates) {
    let total = 0;

    if (resgates.length > 0) {
        resgates.map(resgate => {
            total += parseInt(resgate.resgate.valor);
        })
    }

    return total;
}

function pontosDistribuidosMensalmente(cargas) {
    let cargasAgrupadas = _.groupBy(cargas, function (carga) {
        return carga.data.month
    });

    let colunas = $(".todos-cards .container-card-resgatados .card-resumo .container-table table .table-row td");

    $(colunas).each(function () {
        $(this).html("-");
    })

    Object.keys(cargasAgrupadas).map(function (key) {
        $(colunas[key - 1]).html(totalPontosDistribuidos(cargasAgrupadas[key]).toLocaleString('pt-BR'));
    });
}

function pontosResgatadosMensalmente(resgates) {
    let resgatesAgrupados = _.groupBy(resgates, function (resgate) {
        return resgate.data.month
    });

    let colunas = $(".todos-cards .container-card-resgatados .card-resumo .container-table table .table-resgatados td");

    $(colunas).each(function () {
        $(this).html("-");
    })

    Object.keys(resgatesAgrupados).map(function (key) {
        $(colunas[key - 1]).html(totalPontosResgatados(resgatesAgrupados[key]).toLocaleString('pt-BR'));
    });
}

function usuariosCompletos(usuarios) {
    let completos = 0;

    if (usuarios.length > 0) {
        usuarios.map(usuario => {
            if (usuario.usuario.status != null && usuario.usuario.status == 1) {
                completos++;
            }
        })
    }

    return completos;
}

function usuariosPorIlha(usuarios) {
    let usuariosAgrupados = _.groupBy(usuarios, function (usuario) {
        return usuario.usuario.ilha
    });

    let ilhas = ""

    Object.keys(usuariosAgrupados).map(function (key) {
        ilhas += "<div class='loader-user'><span>" + key.replace(/\_/g, ' ') + "</span><p> " + usuariosCompletos(usuariosAgrupados[key]).toLocaleString('pt-BR') + "/" + usuariosAgrupados[key].length.toLocaleString('pt-BR') + " usuários</p><div class='container-width'><span></span><span class='span-width' data-width=" + (usuariosCompletos(usuariosAgrupados[key]) / usuariosAgrupados[key].length * 100).toFixed(2) + "></span><p class='porcentagem-user' data-porc=" + (usuariosCompletos(usuariosAgrupados[key]) / usuariosAgrupados[key].length * 100).toFixed(2) + ">%</p></div></div>"
    });

    $("#usuarios-ilhas").html(ilhas);
}

function permissaoNotificacoes(usuarios) {
    let total = 0;

    if (usuarios.length > 0) {
        usuarios.map(usuario => {
            if (usuario.usuario.notificacao_celular == 1) {
                total++;
            }
        })
    }

    return total;
}

function rankingProdutosResgatados(produtosResgatados) {
    let rankingGeral = $(".todos-cards .container-card-resgatados .card-resgates .container-card-resgates .mais-resg");

    if (produtosResgatados.length > 0) {
        let produtosResgatadosAgrupados = _.groupBy(produtosResgatados, function (produto) {
            return produto.produto.id
        });
        let arrayProdutosResgatadosAgrupados = _.toArray(produtosResgatadosAgrupados);

        arrayProdutosResgatadosAgrupados.sort(function (a, b) {
            return b.length - a.length;
        });

        $(rankingGeral).hide();

        for (var i = 0; i < (arrayProdutosResgatadosAgrupados.length > 3 ? 3 : arrayProdutosResgatadosAgrupados.length); i++) {
            let elemento = rankingGeral[i];
            let produto = arrayProdutosResgatadosAgrupados[i];

            $(elemento).find(".container-img-mais img").attr('src', produto[produto.length - 1].produto.imagem);
            $(elemento).find(".container-img-mais span").html(produto.length.toLocaleString('pt-BR'));
            $(elemento).find(".text-mais h4").html(produto[0].produto.nome + " - R$ " + produto[0].produto.valor_real.toFixed(2).toLocaleString('pt-BR'));
            $(elemento).css('display', 'flex');
        }
    } else {
        $(rankingGeral).each(function () {
            $(this).css('display', 'none');
        });
    }

    let produtosResgatadosMulheres = _.filter(produtosResgatados, function (p) {
        return p.usuario.sexo == "f";
    });

    $("#total-produtos-resgatados-mulheres").html(produtosResgatadosMulheres.length + '<span> resgates</span>');

    if (produtosResgatadosMulheres.length > 0) {
        let produtosResgatadosAgrupados = _.groupBy(produtosResgatadosMulheres, function (produto) {
            return produto.produto.id
        });
        let arrayProdutosResgatadosAgrupados = _.toArray(produtosResgatadosAgrupados);

        arrayProdutosResgatadosAgrupados.sort(function (a, b) {
            return b.length - a.length;
        });

        let resgatesMulheres = "";

        for (var i = 0; i < (arrayProdutosResgatadosAgrupados.length > 10 ? 10 : arrayProdutosResgatadosAgrupados.length); i++) {
            let produto = arrayProdutosResgatadosAgrupados[i];

            resgatesMulheres += "<div class='loader-user'><span>" + produto[0].produto.nome + " - R$ " + produto[0].produto.valor_real.toFixed(2).toLocaleString('pt-BR') + "</span><p> " + arrayProdutosResgatadosAgrupados[i].length + "/" + produtosResgatadosMulheres.length + " resgates</p><div class='container-width'><span></span><span class='span-width' data-width=" + (arrayProdutosResgatadosAgrupados[i].length / produtosResgatadosMulheres.length * 100).toFixed(2) + "></span><p class='porcentagem-user' data-porc=" + (arrayProdutosResgatadosAgrupados[i].length / produtosResgatadosMulheres.length * 100).toFixed(2) + ">%</p></div></div>"
        }

        $("#produtos-resgatados-mulheres").html(resgatesMulheres);
    } else {
        $("#produtos-resgatados-mulheres").html('');
    }

    let produtosResgatadosHomens = _.filter(produtosResgatados, function (p) {
        return p.usuario.sexo == "m";
    });

    $("#total-produtos-resgatados-homens").html(produtosResgatadosHomens.length + '<span> resgates</span>');

    if (produtosResgatadosHomens.length > 0) {
        let produtosResgatadosAgrupados = _.groupBy(produtosResgatadosHomens, function (produto) {
            return produto.produto.id
        });
        let arrayProdutosResgatadosAgrupados = _.toArray(produtosResgatadosAgrupados);

        arrayProdutosResgatadosAgrupados.sort(function (a, b) {
            return b.length - a.length;
        });

        let resgatesHomens = "";

        for (var i = 0; i < (arrayProdutosResgatadosAgrupados.length > 10 ? 10 : arrayProdutosResgatadosAgrupados.length); i++) {
            let produto = arrayProdutosResgatadosAgrupados[i];

            resgatesHomens += "<div class='loader-user'><span>" + produto[0].produto.nome + " - R$ " + produto[0].produto.valor_real.toFixed(2).toLocaleString('pt-BR') + "</span><p> " + arrayProdutosResgatadosAgrupados[i].length + "/" + produtosResgatadosHomens.length + " resgates</p><div class='container-width'><span></span><span class='span-width' data-width=" + (arrayProdutosResgatadosAgrupados[i].length / produtosResgatadosHomens.length * 100).toFixed(2) + "></span><p class='porcentagem-user' data-porc=" + (arrayProdutosResgatadosAgrupados[i].length / produtosResgatadosHomens.length * 100).toFixed(2) + ">%</p></div></div>"
        }

        $("#produtos-resgatados-homens").html(resgatesHomens);
    } else {
        $("#produtos-resgatados-homens").html('');
    }
}

function usuariosRealizaramLogin(logins) {
    let loginsAgrupados = _.groupBy(logins, function (login) {
        return login.usuario.id
    });

    return _.size(loginsAgrupados);
}

function usuariosRecorrentes(logins) {
    let loginsAgrupados = _.groupBy(logins, function (login) {
        return login.usuario.id
    });

    let total = 0;

    Object.keys(loginsAgrupados).map(function (key) {
        if (loginsAgrupados[key].length > 1) {
            total++;
        }
    });

    return total;
}

function paginasMaisAcessadas(paginas) {
    let paginasAgrupadas = _.groupBy(paginas, function (pagina) {
        return pagina.rota
    });
    let arrayPaginasAgrupadas = _.toArray(paginasAgrupadas);

    arrayPaginasAgrupadas.sort(function (a, b) {
        return b.length - a.length;
    });

    let paginasAcessadas = "";

    for (var i = 0; i < (arrayPaginasAgrupadas.length > 10 ? 10 : arrayPaginasAgrupadas.length); i++) {
        let pagina = arrayPaginasAgrupadas[i];

        paginasAcessadas += '<div class="loader-acessos"><h3>' + (i + 1) + '-' + pagina[0].titulo + '</h3><p>' + pagina.length.toLocaleString('pt-BR') + (pagina.length > 1 ? " acessos" : " acesso") + '</p><div class="container-width"><span></span><span class="span-width" data-width="' + (pagina.length / paginas.length * 100).toFixed(2) + '"></span><p class="porcentagem-acessos" data-porc="' + (pagina.length / paginas.length * 100).toFixed(2) + '">0%</p></div></div>';
    }

    $("#ranking-paginas").html(paginasAcessadas);
}

function categoriasMaisAcessadas(categorias) {
    let categoriasAgrupadas = _.groupBy(categorias, function (categoria) {
        return categoria.rota
    });
    let arrayCategoriassAgrupadas = _.toArray(categoriasAgrupadas);

    arrayCategoriassAgrupadas.sort(function (a, b) {
        return b.length - a.length;
    });

    let categoriasAcessadas = "";

    for (var i = 0; i < (arrayCategoriassAgrupadas.length > 10 ? 10 : arrayCategoriassAgrupadas.length); i++) {
        let categoria = arrayCategoriassAgrupadas[i];

        categoriasAcessadas += '<div class="loader-acessos"><h3>' + (i + 1) + '-' + categoria[0].titulo + '</h3><p>' + categoria.length.toLocaleString('pt-BR') + (categoria.length > 1 ? " acessos" : " acesso") + '</p><div class="container-width"><span></span><span class="span-width" data-width="' + (categoria.length / categorias.length * 100).toFixed(2) + '"></span><p class="porcentagem-acessos" data-porc="' + (categoria.length / categorias.length * 100).toFixed(2) + '">0%</p></div></div>';
    }

    $("#ranking-categorias").html(categoriasAcessadas);
}

function produtosMaisAcessados(produtos) {
    let produtosAgrupados = _.groupBy(produtos, function (produto) {
        return produto.rota
    });
    let arrayProdutosAgrupados = _.toArray(produtosAgrupados);

    arrayProdutosAgrupados.sort(function (a, b) {
        return b.length - a.length;
    });

    let produtosAcessados = "";

    for (var i = 0; i < (arrayProdutosAgrupados.length > 10 ? 10 : arrayProdutosAgrupados.length); i++) {
        let produto = arrayProdutosAgrupados[i];

        produtosAcessados += '<div class="loader-acessos"><h3>' + (i + 1) + '-' + produto[0].titulo + '</h3><p>' + produto.length.toLocaleString('pt-BR') + (produto.length > 1 ? " acessos" : " acesso") + '</p><div class="container-width"><span></span><span class="span-width" data-width="' + (produto.length / produtos.length * 100).toFixed(2) + '"></span><p class="porcentagem-acessos" data-porc="' + (produto.length / produtos.length * 100).toFixed(2) + '">0%</p></div></div>';
    }

    $("#ranking-produtos").html(produtosAcessados);
}

function acessosPorDispositivo(logins) {
    if (logins.length > 0) {
        let celular = _.reject(logins, function (login) {
            return login.dispositivo != "celular"
        });
        let computador = _.reject(logins, function (login) {
            return login.dispositivo != "computador"
        });
        let tablet = _.reject(logins, function (login) {
            return login.dispositivo != "tablet"
        });

        $("#acessos-celular").html(((celular.length / logins.length * 100).toFixed(2)) + '%');
        $("#acessos-computador").html(((computador.length / logins.length * 100).toFixed(2)) + '%');
        $("#acessos-tablet").html(((tablet.length / logins.length * 100).toFixed(2)) + '%');
    } else {
        $("#acessos-celular").html('0%');
        $("#acessos-computador").html('0%');
        $("#acessos-tablet").html('0%');
    }
}

$("#cliente li").click(function () {
    $(this).parent().parent().find(".container-text-img p").attr('data-cliente-id', $(this).attr('data-cliente-id'));

    $("#campanha-selecionada").attr('data-campanha-id', 0);
    $("#campanha-selecionada").html('Todos');

    $("#pdv-selecionado").attr('data-pdv-id', 0);
    $("#pdv-selecionado").html('Todos');

    preecherDados(filtrarDados())
});

$("#campanhas li").click(function () {
    $(this).parent().parent().find(".container-text-img p").attr('data-campanha-id', $(this).attr('data-campanha-id'));

    $("#pdv-selecionado").attr('data-pdv-id', 0);
    $("#pdv-selecionado").html('Todos');

    preecherDados(filtrarDados())
});

$("#pdv li").click(function () {
    $(this).parent().parent().find(".container-text-img p").attr('data-pdv-id', $(this).attr('data-pdv-id'));
    preecherDados(filtrarDados())

});

function filtrarDados() {
    let cliente = $("#cliente-selecionado").attr('data-cliente-id');
    let campanha = $("#campanha-selecionada").attr('data-campanha-id');
    let pdv = $("#pdv-selecionado").attr('data-pdv-id');

    let dadosFiltrados = Object.assign({}, dados);

    // if(cliente != 0) {
    //     Object.keys(dadosFiltrados).map(function(key) {
    //         dadosFiltrados[key] = _.reject(dadosFiltrados[key], function(x) { return x.usuario.pdv.cliente.id != parseInt(cliente) });
    //     });
    // }

    // if(campanha != 0) {
    //       Object.keys(dadosFiltrados).map(function(key) {
    //           let objetos = [];

    //             $(dadosFiltrados[key]).each(function() {
    //                   let objeto = $(this)[0];

    //                   if(objeto.usuario.pdv.cliente.hasOwnProperty('campanha')) {
    //                         if(Array.isArray(objeto.usuario.pdv.cliente.campanha)) {
    //                               if(objeto.usuario.pdv.cliente.campanha.some(x => x.id === parseInt(campanha))) {
    //                                     objetos.push(objeto);
    //                               }
    //                         } else {
    //                               if(objeto.usuario.pdv.cliente.campanha.id == parseInt(campanha)) {
    //                                     objetos.push(objeto);
    //                               }
    //                         }
    //                   } else if(objeto.usuario.pdv.cliente.hasOwnProperty('campanhas')) {
    //                         if(objeto.usuario.pdv.cliente.campanhas.some(x => x.id === parseInt(campanha))) {
    //                               objetos.push(objeto);
    //                         }
    //                   }
    //             });

    //             dadosFiltrados[key] = objetos;
    //       });
    // }

    if (pdv != 0) {
        Object.keys(dadosFiltrados).map(function (key) {
            dadosFiltrados[key] = _.reject(dadosFiltrados[key], function (x) {
                return x.usuario.pdv.id != parseInt(pdv)
            });
        });
    } else if (campanha != 0) {
        Object.keys(dadosFiltrados).map(function (key) {
            let objetos = [];

            $(dadosFiltrados[key]).each(function () {
                let objeto = $(this)[0];

                if (objeto.usuario.pdv.cliente.hasOwnProperty('campanha')) {
                    if (Array.isArray(objeto.usuario.pdv.cliente.campanha)) {
                        if (objeto.usuario.pdv.cliente.campanha.some(x => x.id === parseInt(campanha))) {
                            objetos.push(objeto);
                        }
                    } else {
                        if (objeto.usuario.pdv.cliente.campanha.id == parseInt(campanha)) {
                            objetos.push(objeto);
                        }
                    }
                } else if (objeto.usuario.pdv.cliente.hasOwnProperty('campanhas')) {
                    if (objeto.usuario.pdv.cliente.campanhas.some(x => x.id === parseInt(campanha))) {
                        objetos.push(objeto);
                    }
                }
            });

            dadosFiltrados[key] = objetos;
        });
    } else if (cliente != 0) {
        Object.keys(dadosFiltrados).map(function (key) {
            dadosFiltrados[key] = _.reject(dadosFiltrados[key], function (x) {
                return x.usuario.pdv.cliente.id != parseInt(cliente)
            });
        });
    }

    return dadosFiltrados;
}

function gerarData(data, dias) {
    let resultado = new Date(data);

    resultado.setDate(resultado.getDate() + dias);

    return resultado;
}
