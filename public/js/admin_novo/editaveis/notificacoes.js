let cadastroNotificacoes = {

    filtrarPdvs:function(){

        let notificacao = new FormData();

        notificacao.append('cod_campanha', $("#cadastro-notificacao-campanha").val());

        $.ajax({
            url: "/sistema/notificacoes/listar-ilhas",
            type: "POST",
            data: notificacao,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {


                $("#cadastro-notificacao-pdv-ilhas").html('');

                $.each(data.pdvs_ilhas, function(key, value){

                    let ilhas = "";


                    $.each(value.users, function(key_u, value_u){

                        ilhas += `
                            <div class="check" data-cod-ilha="${value_u.ilha}">
                                <div class="square"></div>
                                <span>${value_u.ilha}</span>
                            </div>
                        `;
                    });

                    $("#cadastro-notificacao-pdv-ilhas").append(`
                        <div class="item" data-cod-pdv="${value.cod_pdv}">
                            <h3>${value.nome_pdv}</h3>
                            ${ilhas}
                        </div>
                    `);

                });

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });

    },
}


var arrNotificacaoPdvIlhasCadastrar = [];

$(document).on("click", "#cadastro-notificacao-pdv-ilhas .check", function() {
    $(this).toggleClass("checked");
    let ilha = this.dataset.codIlha;

    if($(this).hasClass('checked')){
        arrNotificacaoPdvIlhasCadastrar.push(this.dataset.codIlha);
    } else {
        arrNotificacaoPdvIlhasCadastrar.forEach(function(value, i){
            if(value == ilha){
                arrNotificacaoPdvIlhasCadastrar.splice(i, 1);
            }
        });
    }
});

$(document).on('click',"#cadastro-notificacao-option-cliente", function(){

    if($("#cadastro-notificacao-cliente").val() != ''){

        let cliente = new FormData();

        cliente.append('cod_cliente', $("#cadastro-notificacao-cliente").val());

        $.ajax({
            url: "/sistema/notificacoes/listar-campanhas",
            type: "POST",
            data: cliente,
            async: true,
            contentType: false,
            processData: false,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro){
                    $("#cadastro-notificacao-select-campanha").html('');

                    $.each(data.campanhas, function(key, value){

                        $("#cadastro-notificacao-select-campanha").append(`
                            <li><a href="" id="cadastro-notificacao-option-campanha" cod-opcao="${ value.cod_campanha }">${ value.nome_campanha }</a></li>
                        `);

                    });
                } else
                    console.log("erro=> " + data.mensagem);

            },
            error: function (x, t, m) {
                console.log("erro=> " + m);

            }
        });
    }

});


$(document).on('click',"#cadastro-notificacao-option-campanha", function(){
    cadastroNotificacoes.filtrarPdvs();
});

previa.setImgPrevia('.option-categoria', '.previa-notificacao .container-img img')
previa.originalPrevia("#titulo", '.previa-notificacao .title-descricao h3')
previa.originalPrevia("#mensagem", '.previa-notificacao .title-descricao p')

$("#form-cadastro-notificacao").submit(function(e) {
    e.preventDefault();
}).validate({
    rules: {
        titulo: 'required',
        mensagem: 'required',
    },
    errorPlacement: function(error, element){},
    submitHandler: function(form) {

        if($("#hidden-cod-categoria").val() == ""){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Escolha um tipo de notificação!'
            });
            return;
        }

        if($("#cadastro-notificacao-cliente").val() == ""){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Escolha um cliente!'
            });
            return;
        }

        if($("#cadastro-notificacao-campanha").val() == ""){
            swal({
                type: 'error',
                title: 'Espere!',
                text: 'Escolha uma campanha!'
            });
            return;
        }

        $("#btn-finalizar-notificacao").attr('disabled', 'disabled');

        let notificacao = new FormData();

        notificacao.append('titulo', $("#titulo").val());
        notificacao.append('mensagem', $("#mensagem").val());
        notificacao.append('cod_cliente', $("#cadastro-notificacao-cliente").val());
        notificacao.append('cod_campanha', $("#cadastro-notificacao-campanha").val());
        notificacao.append('cod_categoria', $("#hidden-cod-categoria").val());
        notificacao.append('pdvs_ilhas', JSON.stringify(arrNotificacaoPdvIlhasCadastrar));

        $.ajax({
            url: "/sistema/notificacoes/adicionar-notificacao",
            type: "POST",
            data: notificacao,
            async: true,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro)
                    $("#modal-sucesso").show();
                else
                    $("#modal-erro").show();
            },
            error: function (x, t, m) {
                console.log("erro=> " + m);
                $("#modal-erro").show();
            }
        });

    }
});
