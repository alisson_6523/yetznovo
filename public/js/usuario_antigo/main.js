var audios = [];

$(document).ready(function () {
	atualizaNotificacoes();
})

function getCookie(name) {
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else
	{
		begin += 2;
		var end = document.cookie.indexOf(";", begin);
		if (end == -1) {
			end = dc.length;
		}
	}
	return decodeURI(dc.substring(begin + prefix.length, end));
}

function direcionamento(codProduto) {

	var url = "../../view/resgate/detalhes-produto.php?produto=" + codProduto;

	location.href = url;
}

var swiper = new Swiper('.banner-area.swiper-container', {
	autoplay: {
		delay:5000
	},
	navigation: {
		nextEl: '.setas .seta-dir1',
		prevEl: '.setas .seta-esq1',
	},
	loop:true
});

$(".setas .seta-dir1, .setas .seta-esq1").click(function(){
	swiper.autoplay.start();
});

$("#btn-categoria").click(function () {
	$(".dropdown-todas-categorias").slideToggle(function () {
		$(this).removeClass("sumir");
		$(this).addClass("aparecer");
	})
	if ($(this).find("#seta-categoria").hasClass("normal")) {
		$(this).find("#seta-categoria").removeClass("normal");
		$(this).find("#seta-categoria").addClass("girar");
	} else {
		$(this).find("#seta-categoria").removeClass("girar");
		$(this).find("#seta-categoria").addClass("normal");
	}
})

console.log('aqui')

$(document).on("click", ".btn-excluir", function () {
	var codProduto = $(this).parents(".produto-carrinho").attr('cod-produto');
	var produto = $(".resumo-carrinho .produto-carrinho[cod-produto='" + codProduto + "']");
	var produto_responsivo = $(".resumo-carrinho-mobile .produto-carrinho[cod-produto='" + codProduto + "']");

	deletaProduto(codProduto);

	produto.fadeOut('slow', function () {
		produto.remove();
		if ($('#produtosCarrinho .produto-carrinho').length == 0) {
			$('#produtosCarrinho').append('<h1 class="aviso-vazio">Carrinho vazio!</h1>');

			// window.location.href = "/"
		}

		calcCarrinho();
	});

	produto_responsivo.fadeOut('slow', function () {
		produto_responsivo.remove();

		calcCarrinho();

		let itens = document.querySelectorAll('.resumo-carrinho-mobile #produtosCarrinho .produto-carrinho[cod-produto]')

		if(itens.length == 0){
			$('.resumo-carrinho-mobile .area-total').empty()
			document.querySelector('.resumo-carrinho-mobile #produtosCarrinho').innerHTML = `<h1 class="aviso-vazio">Carrinho vazio!</h1>`
		}

	});

})

$(document).click(function (e) {
	if (!$(e.target).closest('.btn-categorias').length && !$(e.target).closest('.dropdown-todas-categorias').length) {
		$('.dropdown-todas-categorias').slideUp();
	}
});

$(".saldo-carrinho .btn-carrinho").click(function () {

	$(".resumo-carrinho").slideToggle(function () {
		$(this).removeClass("sumir");
		$(this).addClass("aparecer");

		if ($(this).parents(".saldo-carrinho").find(".saldo-novo").hasClass("sem-borda-verde")) {
			$(".saldo-novo").removeClass("sem-borda-verde");
			$(".saldo-novo").addClass("borda-verde");
		} else {
			$(".saldo-novo").addClass("sem-borda-verde");
			$(".saldo-novo").removeClass("borda-verde");
		}
	})

	checkCarrinho();
});

$(".saldo-carrinho-responsivo .btn-carrinho").click(function () {
	let item = parseInt($('.saldo-carrinho-responsivo .bloco-carrinho .btn-carrinho .qtd-itens').text())

	if(item){
		if(!document.querySelector('.resumo-carrinho-mobile .area-total .btn-resgatar-carrinho')){
			$('.resumo-carrinho-mobile .area-total').append(`<a href="/loja/carrinho" class="btn-resgatar-carrinho">Visualizar Carrinho</a>`)
		}
	}


	if($(".resumo-carrinho-mobile").hasClass('active')){
		$(".resumo-carrinho-mobile").removeClass('active');
		$("html").removeClass('no-scroll');
	} else {
		$(".resumo-carrinho-mobile").addClass('active');
		$("html").addClass('no-scroll');
	}
});

function checkCarrinho(){
	if($(".resumo-carrinho").find(".produto-carrinho").length == 0){
		$(".resumo-carrinho .area-total").css("display", "none");
	}else{
		$(".resumo-carrinho .area-total").css("display", "block");
	}
}

var menu_categorias = new Swiper('.menu-categorias', {
	pagination:{
		clickable: true,
	},
	navigation: {
		nextEl: '.seta-dir-slide-categoria',
		prevEl: '.seta-esq-slide-categoria',
	},
	slidesPerView: 14,
	simulateTouch: false,
	spaceBetween: 0,
	breakpoints: {
		1200: {
			slidesPerView: 7
		}
	}
});

$(function () {
	$(window).on("scroll", function () {
		if ($(window).scrollTop() > 420) {
			$(".texto-duvidas-frequentes").addClass("fixar-conteudo-duvidas");
		} else {
			$(".texto-duvidas-frequentes").removeClass("fixar-conteudo-duvidas");
		}
	});
});

$(".btn-menu-responsivo").click(function () {
	$(".menu-resp").slideToggle();
});

$(".btn-menu-usuario").click(function () {
	$(".menu-usuario").slideToggle();
});

var swiper_responsivo = new Swiper('.swiper-categorias-produtos', {
	pagination:{
		el: '.swiper-pagination',
		clickable: true,
		dynamicBullets: true,
		dynamicMainBullets: 5
	},
	slidesPerView: 3,
	breakpoints: {
		991: {
			slidesPerView: 7
		},
		650: {
			slidesPerView: 6
		},
		480: {
			slidesPerView: 4
		}
	}
});

$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function (event) {
	// On-page links
	if (
		location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
		&&
		location.hostname == this.hostname
		) {
			// Figure out element to scroll to
			var target = $(this.hash);
			target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
			// Does a scroll target exist?
			if (target.length) {
				// Only prevent default if animation is actually gonna happen
				event.preventDefault();
				$('html, body').animate({
					scrollTop: target.offset().top
				}, 1000, function () {
					// Callback after animation
					// Must change focus!
					var $target = $(target);
					$target.focus();
					if ($target.is(":focus")) { // Checking if the target was focused
						return false;
					} else {
						$target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
						$target.focus(); // Set focus again
					}
					;
				});
			}
		}
	});

	//Menu Lateral Notificações
	function openMenuNotificacoes() {
		closeMenu();
		$('.menu-lateral-notificacoes').addClass('active');
	}

	function closeMenuNotificacoes() {
		$('.menu-lateral-notificacoes').removeClass('active');
		atualizaNotificacoes();
	}

	$(".btn-alerta").click(function (event) {
		openMenuNotificacoes();
	});

	$(".menu-lateral-notificacoes").click(function (event) {
		if (event.target.classList.contains("background")) {
			closeMenuNotificacoes(event)
		}
	});

	$(".menu-lateral-notificacoes .btn-fechar").click(function (event) {
		$(".fundo-escuro").hide();
		$(".conteudo-alertas").removeClass('abrir-conteudo');
		$(".item_notificacao .new").remove();
		atualizaNotificacoes();
	});

	$(".fundo-escuro").click(function (event) {
		$(".fundo-escuro").hide();
		$(".conteudo-alertas").removeClass('abrir-conteudo');
		$(".conteudo-agenda").removeClass('abrir-conteudo');
		$(".item_notificacao .new").remove();
		atualizaNotificacoes();
	});

	$(".btn-excluir-item").click(function () {
		var notificacao_id = $(this)[0].getAttribute("notificacao_id");
		var cod_usuario = $(this)[0].getAttribute("cod_usuario");
		marcaNotificacao(notificacao_id, cod_usuario, $(this));
		$(this).parents(".item_notificacao").remove();
	});

	function marcaNotificacao(notificacao_id, cod_usuario) {
		$.ajax({
			url: "/notification/visualizar/" + notificacao_id,
			type: "GET",
			dataType: "json",
			async: true,
			timeout: 10000,
			success: function (data) {
				$(".todas-notificacoes .item_notificacao[notificacao_id='" + notificacao_id + "']")[0].remove();
			},
			error: function (x, t, m) {
				console.log(x.responseText);
			}
		});
	}

	function atualizaNotificacoes() {
		var qtdItem = $(".item_notificacao").length;

		if (qtdItem == 0) {
			$(".notificacao-vazio").show();
			$(".todas-notificacoes").hide();
			$(".num-notificacoes").parent(".num").hide();
		} else {
			$(".num-notificacoes").parent(".num").show();
			$(".num-notificacoes").text(qtdItem);
		}
	}

	$('.abre-modal-notificacao').click(function () {
		var notificacao_id = $(this)[0].getAttribute("notificacao_id");
		var notificacao = $(".btn-excluir-item[notificacao_id='" + notificacao_id + "']")
		var cod_usuario = notificacao.attr("cod_usuario");

		$.ajax({
			url: SITE + "/json/busca-detalhes-notificacao.php",
			type: "POST",
			dataType: "json",
			data: {notificacao_id: notificacao_id},
			async: true,
			timeout: 10000,
			success: function (data) {
				$("#titulo-notificacao").text(data.dados.titulo);
				$("#conteudo-notificacao").text(data.dados.mensagem);
				if (notificacao.length != "0") {
					marcaNotificacao(notificacao_id, cod_usuario);
					notificacao.parents(".item_notificacao").remove();
				}
			},
			error: function (x, t, m) {
				console.log("erro");
			}
		});
	});

	$(".todas-notificacoes .item_notificacao").click(function (event) {
		var notificacao_id = $(this)[0].getAttribute("notificacao_id");
		var notificacao = $(".btn-excluir-item[notificacao_id='" + notificacao_id + "']")
		var cod_usuario = $(".todas-notificacoes").attr("cod_usuario");

		if(event.target.parentElement.classList.contains("btn-visualizar-mensagem")){
			marcaNotificacao(notificacao_id, cod_usuario);
			atualizaNotificacoes();
		} else {
			$.ajax({
				url: "/notification/visualizar/" + notificacao_id,
				type: "GET",
				dataType: "json",
				async: true,
				timeout: 10000,
				success: function (resp) {
					$("#tipo-notificacao").text(resp.data.tipo);
					$("#titulo-notificacao").text(resp.data.titulo);
					$("#conteudo-notificacao").text(resp.data.mensagem);
					$(".cod-notificacao").val(notificacao_id);

					if (notificacao.length != "0") {
						notificacao.parents(".item_notificacao").remove();
					}
				},
				error: function (x, t, m) {
					console.log("erro");
				}
			});

			$("#modalNotificacao").modal('show');
			$("#modalNotificacao").attr("notificacao_nova", 1);
		}
	});

	$(".todas-notificacoes-all .box-notificacao").click(function (event) {
		var notificacao_id = $(this)[0].getAttribute("notificacao_id");
		var notificacao = $(".btn-excluir-item[notificacao_id='" + notificacao_id + "']")
		var cod_usuario = $(".todas-notificacoes").attr("cod_usuario");

		$.ajax({
			url: "/notification/visualizar/" + notificacao_id,
			type: "GET",
			dataType: "json",
			async: true,
			timeout: 10000,
			success: function (resp) {
				$("#tipo-notificacao").text(resp.data.tipo);
				$("#titulo-notificacao").text(resp.data.titulo);
				$("#conteudo-notificacao").text(resp.data.mensagem);
				$(".cod-notificacao").val(notificacao_id);

			},
			error: function (x, t, m) {
				console.log("erro");
			}
		});
		$("#modalNotificacao").modal('show');
		$("#modalNotificacao").attr("notificacao_nova", "");
	});


	$("#modalNotificacao").on('hidden.bs.modal', function(){
		var notificacao_id = this.querySelector(".cod-notificacao").value;
		var cod_usuario = this.querySelector(".cod-usuario").value;

		if ($(this).attr("notificacao_nova")){
			marcaNotificacao(notificacao_id, cod_usuario);
		}
	});

	function formatTime(seconds) {
		minutes = Math.floor(seconds / 60);
		minutes = (minutes >= 10) ? minutes : "0" + minutes;
		seconds = Math.floor(seconds % 60);
		seconds = (seconds >= 10) ? seconds : "0" + seconds;
		return minutes + ":" + seconds;
	}

	// Novo Player Audio
	var Audio = function(elem) {
		this.media = elem.querySelector('.media');
		this.playButton = elem.querySelector('.pButton');
		this.playHead = elem.querySelector('.playhead');
		this.timeline = elem.querySelector('.timeline')
		this.spanTempoCorrente = elem.querySelector('.tempo-corrente');

		this.status = false;
		this.currentTime = this.media.currentTime.toString();
		this.timelineWidth = this.timeline.offsetWidth * .95;
		this.timeLinePosition = this.timeline.getBoundingClientRect().left;
		this.duration = this.media.duration;
		this.playHeadPosition = 0;

		this.playButton.addEventListener('click', toogleAudio.bind(this));
		this.media.ontimeupdate = updateTime.bind(this);
		this.media.onended = resetAudio.bind(this);
		this.playHead.ondrag = dragPlayHead.bind(this);
		this.playHead.ondragend = dragPlayHead.bind(this);
	}

	setTimeout(function(){
		Array.from(document.querySelectorAll('.card-video[data-tipo="audio"]')).map(function(audio){
			audios.push(new Audio(audio));
		})
	}, 500);

	function toogleAudio() {
		if(this.status){
			pauseAudio.call(this);
		} else {
			pauseAllAudio();
			playAudio.call(this);
		}
	}

	function playAudio() {
		this.status = true;
		this.playButton.classList.remove('play');
		this.playButton.classList.add('pause');
		this.media.play();
	}

	function pauseAudio() {
		this.status = false;
		this.playButton.classList.remove('pause');
		this.playButton.classList.add('play');
		this.media.pause();
	}

	function pauseAllAudio() {
		audios.map(function(audio) {
			pauseAudio.call(audio);
		})
	}

	function updateTime() {
		this.currentTime = this.media.currentTime;
		this.spanTempoCorrente.innerText = formatTime(this.currentTime);
		this.playHead.style.marginLeft = getPositionTimeHandler(this) + "px";
	}

	function getPositionTimeHandler(elem) {
		let result = (elem.timelineWidth * (elem.currentTime / elem.duration));

		return result.toFixed(1);
	}

	function resetAudio() {
		this.media.currentTime = 0;
		pauseAudio.call(this);
	}

	function dragPlayHead(e){
		let mousePosition = e.clientX;
		let marginLeft = (((mousePosition - this.timeLinePosition) / this.timelineWidth) * 100).toFixed(1);

		if(mousePosition != 0){
			if(marginLeft < 0){
				setPlayHeadPosition.call(this, 0);
			} else if (marginLeft > 0 && marginLeft < 100) {
				setPlayHeadPosition.call(this, marginLeft);
			} else {
				setPlayHeadPosition.call(this, 99.5);
			}
		}
	}

	function setPlayHeadPosition(margin) {
		this.playHead.style.marginLeft = margin + '%';
		this.playHeadPosition = margin;
		this.media.currentTime = Math.floor((margin / 100) * this.duration);
	}



	// //Player Audio
	// var media = [];
	// var duration = []; // Duration of audio clip, calculated here for embedding purposes
	// var pButton = []; // play button
	// var playhead = []; // playhead
	// var timeline = []; // timeline
	// var onplayhead = [];
	// var timelineWidth = [];
	// var currentTimeText = [];
	// var spanTempoCorrente = [];
	//
	//
	//
	// var timeUpdateHandler = function (event) {
	//     index = event.data.i;
	//     var playPercent = timelineWidth[index] * (media[index].currentTime / duration[index]);
	//     //currentTimeText[index].text(media[index].currentTime);
	//     var tempoCorrenteVideo = formatTime(media[index].currentTime);
	//     spanTempoCorrente[index].innerHTML = tempoCorrenteVideo;
	//
	//     playhead[index].style.marginLeft = playPercent + "px";
	//     if (media[index].currentTime == duration[index]) {
	//         //pButton[index].className = "";
	//         //pButton[index].className = "pButton play";
	//         media[index].currentTime = 0;
	//         onMediaPause(index);
	//     }
	// };
	//
	//
	// [].forEach.call(document.getElementsByClassName('media'), function (v, i, a) {
	//     getMedia(i);
	// });
	// [].forEach.call(document.getElementsByClassName('playhead'), function (v, i, a) {
	//     getPlayhead(i);
	// });
	// //[].forEach.call(document.getElementsByClassName('timeline'), function(v,i,a) {
	// //gettimeline(i);
	// //});
	//
	// function getMedia(index) {
	//     pButton[index] = document.getElementsByClassName('pButton')[index];
	//     spanTempoCorrente[index] = document.getElementsByClassName('tempo-corrente')[index];
	//     media[index] = document.getElementsByClassName('media')[index];
	//     playhead[index] = document.getElementsByClassName('playhead')[index];
	//     timeline[index] = document.getElementsByClassName('timeline')[index];
	//
	//     if(navigator.userAgent.indexOf("Firefox") != -1){
	//         duration[index] = document.getElementsByClassName('media')[index].duration;
	//     }else{
	//         media[index].addEventListener("canplaythrough", function () {
	//             duration[index] = document.getElementsByClassName('media')[index].duration;
	//         });
	//     }
	//
	//     $(pButton[index]).bind("click", function () {
	//         media.forEach(function (v, i, a) {
	//             if (i != index) {
	//                 if (!media[i].paused) {
	//                     //remove
	//                     media[i].pause();
	//                     //setElementOnPause(pButton[i]);
	//                     onMediaPause(i);
	//                 }
	//             }
	//         });
	//         if (media[index].paused) {
	//             //adiciona
	//             media[index].play();
	//             // remove play, add pause
	//             //setElementOnPlay(pButton[index]);
	//             onMediaPlay(index);
	//
	//         } else {
	//             media[index].pause();
	//             //setElementOnPause(pButton[index]);
	//             onMediaPause(index);
	//         }
	//     });
	//     gettimeline(index);
	// }
	//
	// function getPlayhead(index) {
	//     var playheadMoveHandler = function (event) {
	//         var posX = 0;
	//         if (event.type == 'mousemove')
	//             posX = event.clientX;
	//         else if (event.type == 'touchmove')
	//             posX = event.originalEvent.changedTouches[0].clientX;
	//         var newMargLeft = posX - getPosition(timeline[index]);
	//
	//         if (newMargLeft >= 0 && newMargLeft <= timelineWidth[index]) {
	//             playhead[index].style.marginLeft = newMargLeft + "px";
	//         }
	//         if (newMargLeft < 0) {
	//             playhead[index].style.marginLeft = "0px";
	//         }
	//         if (newMargLeft > timelineWidth[index]) {
	//             playhead[index].style.marginLeft = timelineWidth[index] + "px";
	//         }
	//     };
	//
	//     var playheadReleaseHandler = function (event) {
	//         console.log(event);
	//         var posX = 0;
	//         if (event.type == 'mouseup')
	//             posX = event.clientX;
	//         else if (event.type == 'touchend')
	//             posX = event.originalEvent.changedTouches[0].clientX;
	//         $(window).unbind('touchmove mousemove', playheadMoveHandler);
	//
	//         console.log("index: " + index);
	//         console.log("duration: " +  duration[index]);
	//         console.log("posX: " +  posX);
	//         console.log("timeline: " +  timeline[index]);
	//         console.log("timelineWidth: " +  timelineWidth[index]);
	//
	//         media[index].currentTime = duration[index] * ((posX - getPosition(timeline[index])) / timelineWidth[index]);
	//         $(media[index]).bind("timeupdate", {i: index}, timeUpdateHandler);
	//         $(window).unbind('touchend mouseup', playheadReleaseHandler);
	//     };
	//
	//
	//     var playheadHandler = function (event) {
	//         var index = event.data.i;
	//         onplayhead[index] = true;
	//
	//         $(window).bind('touchmove mousemove', playheadMoveHandler);
	//         $(window).bind('touchend mouseup', playheadReleaseHandler);
	//         $(media[index]).unbind('timeupdate', timeUpdateHandler);
	//     };
	//
	//
	//     $(playhead[index]).bind('touchstart mousedown', {i: index}, playheadHandler);
	//     onplayhead[index] = false;
	// }
	// function gettimeline(index) {
	//     var timelineHandler = function (event) {
	//         console.log(event);
	//         var index = event.data.i;
	//         var posX = 0;
	//         if (event.clientX != undefined)
	//             posX = event.clientX;
	//         else if (event.originalEvent.changedTouches[0] != undefined)
	//             posX = event.originalEvent.changedTouches[0].clientX;
	//         console.log(duration[index]);
	//         media[index].currentTime = parseFloat(duration[index] * ((posX - getPosition(timeline[index])) / timelineWidth[index]));
	//     }
	//
	//     $(timeline[index]).bind("click", {i: index}, timelineHandler);
	//     timelineWidth[index] = timeline[index].offsetWidth - playhead[index].offsetWidth;
	// }
	//
	//
	// // getPosition
	// // Returns elements left position relative to top-left of viewport
	// function getPosition(el) {
	//     return el.getBoundingClientRect().left;
	// }
	//
	// //For Firefox we have to handle it in JavaScript
	// var vids = $("video");
	// $.each(vids, function () {
	//     this.controls = false;
	// });
	//
	//
	//
	// function setElementOnPause(el) {
	//     el.className = "";
	//     el.className = "pButton play";
	// }
	//
	// function setElementOnPlay(el) {
	//     el.className = "";
	//     el.className = "pButton pause";
	// }
	//
	// function onMediaPause(index){
	//     pButton[index].className = "";
	//     pButton[index].className = "pButton play";
	// }
	//
	// function onMediaPlay(index){
	//     pButton[index].className = "";
	//     pButton[index].className = "pButton pause";
	// }

	function toggleMenu() {
		var menu = $("#menu");

		if(menu.hasClass('active')){
			$("#menu").removeClass("active");
			$("html").removeClass("no-scroll")
		} else {
			$("#menu").addClass("active");
			$("html").addClass("no-scroll")
		}
	}

	function closeMenu() {
		$("#menu").removeClass("active");
		$("html").addClass("no-scroll")
	}

	function closeDuvidas() {
		document.querySelectorAll(".area-contato .content .box-conteudo .conteudo .bloco-duvida").forEach(function(duvida) {
			duvida.classList.remove('active');
		})
	}

	$(".area-contato .content .box-conteudo .topo").click(function() {
		$(this.parentNode).toggleClass('active');
	});

	$(".area-contato .content .box-conteudo .conteudo .bloco-duvida").click(function() {
		if (this.classList.contains("active")) {
			closeDuvidas();
		} else {
			closeDuvidas();
			$(this).toggleClass('active');
		}
	});

	function procuraDuvida(frase) {
		frase = frase.toLowerCase();

		document.querySelectorAll(".bloco-duvida").forEach(function(duvida) {
			if (duvida.querySelector(".duvida").textContent.toLowerCase().includes(frase)) {
				duvida.classList.remove("hide");
			} else {
				duvida.classList.add("hide");
			}
		});
	}

	$(".area-contato .content .box-conteudo.duvidas-frequentes .bloco-busca input").keyup(function() {
		procuraDuvida(this.value);
	});

	$(".btn-detalhes-extrato").click(function(){

		var detalhes_row = $(this).parents('tr')[0].nextElementSibling;

		this.classList.toggle("active");

		if (this.classList.contains("active")) {
			this.querySelector("span").textContent = "ocultar detalhes";
			this.querySelector("i").classList = "fa fa-caret-up";
			detalhes_row.classList.add("opened");
		} else {
			this.querySelector("span").textContent = "ver detalhes";
			this.querySelector("i").classList = "fa fa-caret-down";
			detalhes_row.classList.remove("opened");
		};
	})

	$(".btn-voucher").click(function() {

		if(this.parentElement.classList.value.includes('btn-mobile-voucher')){
			document.querySelector('#menu').classList.remove('active')
		}
		$(".modal-voucher").fadeIn();
	})

	$(".btn-close-modal").click(function() {
		$(".modal-voucher").fadeOut();
	})

	$(".box-erro .btn-voltar").on("click", function() {
		$(".modal-voucher .box-erro").hide();
		$(".modal-voucher .box-white").fadeIn();
		$(".modal-voucher .box-white .texto .formulario input[type=text]").val("").focus();
	})

	$(".modal-voucher .box-sucesso .btn-finalizar").on("click", function() {
		location.reload();
	})


	let categoriasMobile = document.querySelectorAll('#home-mobile .cards-mobile .container-cards a');
	categoriasMobile.forEach(function(categoria){
		categoria.addEventListener('click', function(){
			document.querySelector('body').classList.toggle('loader-toggle')
		})
	})

	//  DowLoad

	let target = '#area-campanha-mobile .container-cards .container-card-mobile .card-mobile .card-text span'

	let links = document.querySelectorAll(target)
	let origin = window.location.origin

	let modal = document.querySelector('.modal-iframe');
	let iframe = document.querySelector('.modal-iframe iframe');
	let ios = document.querySelector('.modal-iframe');
	let closeModal = document.querySelector('.container-close-modal')
	let html = document.querySelector('html')

	if(closeModal){
		closeModal.addEventListener('click', function(){
			document.querySelector('.modal-iframe').classList.toggle('active')
		})
	}

	modal.addEventListener('click', function(){
		document.querySelector('.modal-iframe').classList.remove('active')
	})

	closeModal.addEventListener('click', function(){
	ios.classList.remove('active')
	html.classList.remove('active')
	})

	links.forEach(function(link){
		link.addEventListener('click', function(){
			let linkComplete = link.dataset.link

			if(ios.classList.value.includes('ios')){
				linkComplete = link.dataset.link
			}

			document.querySelector('.modal-iframe').classList.remove('active')

			// link.innerHTML =linkComplete

			document.querySelector('.modal-iframe').classList.toggle('active')
			iframe.setAttribute('src', linkComplete)

		})
	})
