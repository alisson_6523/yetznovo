$(function () {
    var valorData = "";
    $("#dropdown-data").change(function () {
        valorData = $("#dropdown-data option:selected").text().toUpperCase();
        if (valorData == 'TODOS') {
            valorData = '';
        }
        searchTable(valorData);
    });

    var valorTipo = "";
    $("#dropdown-tipo").change(function () {
        valorTipo = $("#dropdown-tipo option:selected").text().toUpperCase();
        if (valorTipo == 'TODOS') {
            valorTipo = '';
        }
        searchTipo(valorTipo);
    });

    function searchTipo(valorD) {
        var nthTipo = "#tabela td:nth-child(2)";

        console.log(valorD)
        if(valorD == "") {
            $('#tabela tbody tr').show();
        } else {
            $('#tabela tbody tr').hide();
            $('#tabela tbody tr[data-tipo="'+ valorD +'"]').show();
            
            let length = $('#tabela tbody tr[data-tipo="'+ valorD +'"]').show().length
            if(length == 0){
                if(valorD != ""){
                    alerta('awesome yellow', 'Atenção', 'Não existem registro para este filtro', '', 'fa fa-check');    
                } 
            }
        }
        
        $('#tabela tbody tr.detalhes-resgate').hide()
        $('#tabela tbody tr.detalhes-resgate').each(function(index, elem){
            if(elem.classList.contains("opened")){
                cod_elem = elem.dataset.cod
                $("tr[data-cod=" + cod_elem +"] .btn-detalhes-extrato").click()
            }            
        });

        $(nthTipo).each(function () {
            if ($(this).text().toUpperCase().indexOf(valorD) < 0) {

                $(this).parent().hide();
            }
        });
    };

    function searchTable(valorD) {
        var nthData = "#tabela td:nth-child(1)";
        $("#tabela tbody tr").show();
        $('#tabela tbody tr.detalhes-resgate').hide()
        $('#tabela tbody tr.detalhes-resgate').each(function(index, elem){
            if(elem.classList.contains("opened")){
                cod_elem = elem.dataset.cod
                $("tr[data-cod=" + cod_elem +"] .btn-detalhes-extrato").click()
            }
        });        

        console.log(valorD)

        $(nthData).each(function () {
            if ($(this).text().toUpperCase().indexOf(valorD) < 0) {
                $(this).parent().hide();
            }
        });
    }
    ;
});