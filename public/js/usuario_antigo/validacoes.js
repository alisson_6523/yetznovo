var v_obj, v_fun;

function moeda(v, elem) {
    v = v.replace(/\D/g, "");//Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{9})$/, "$1.$2");//coloca o ponto dos milhões
    v = v.replace(/(\d)(\d{6})$/, "$1.$2");//coloca o ponto dos milhares
    v = v.replace(/(\d)(\d{3})$/, "$1.$2");//coloca o ponto dos milhares
    return v;
}

function porcentagem(v, elem) {
    if (v.length < 4) {
        v = v.replace(/\D/g, "");//Remove tudo o que não é dígito
        return v;
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
}

function moedaComCentavos(v, elem) {
    v = v.replace(/\D/g, "");//Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{12})$/, "$1.$2");//coloca o ponto dos milhões
    v = v.replace(/(\d)(\d{8})$/, "$1.$2");//coloca o ponto dos milhares
    v = v.replace(/(\d)(\d{5})$/, "$1.$2");//coloca o ponto dos milhares
    v = v.replace(/(\d)(\d{2})$/, "$1,$2");//coloca a virgula antes dos 2 últimos dígitos
    return v;
}

function validaCep(v, elem) {
    if (v.length < 10) {
        v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
        v = v.replace(/^(\d{5})(\d)/, "$1-$2");         //Esse é tão fácil que não merece explicações
        return v;
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
}
function tel(v, elem) {
    if (v.length < 15) {
        v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
}

function cel(v, elem) {
    if (v.length < 16) {
        v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v = v.replace(/(\d)(\d{5})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
}

function organizaNumeroDeContato(v, elem) {
    if (v.length < 15) {
        v = v.replace(/\D/g, "");             //Remove tudo o que não é dígito
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    }
    return v;

}

function verificaCampoEmail(v, elem) {
    var expr = /\w+@\w+\./ig;
    if (expr.test(v)) {
        $(elem).removeClass('error-input');
    } else {
        $(elem).addClass('error-input');
    }
    return v;
}


function cnpj(v, elem) {
    v = v.replace(/\D/g, "")                           //Remove tudo o que não é dígito
    v = v.replace(/^(\d{2})(\d)/, "$1.$2")             //Coloca ponto entre o segundo e o terceiro dígitos
    v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3") //Coloca ponto entre o quinto e o sexto dígitos
    v = v.replace(/\.(\d{3})(\d)/, ".$1/$2")           //Coloca uma barra entre o oitavo e o nono dígitos
    v = v.replace(/(\d{4})(\d)/, "$1-$2")              //Coloca um hífen depois do bloco de quatro dígitos
    return v
}
function validaCpf(v, elem) {
    if (v.length < 15) {
        v = v.replace(/\D/g, "")                    //Remove tudo o que não é dígito
        v = v.replace(/(\d{3})(\d)/, "$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace(/(\d{3})(\d)/, "$1.$2")       //Coloca um ponto entre o terceiro e o quarto dígitos
        //de novo (para o segundo bloco de números)
        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2") //Coloca um hífen entre o terceiro e o quarto dígitos
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
    return v;
}
function dataDMA(v, elem) {

    if (v.length < 11) {
        v = v.replace(/\D/g, "");                    //Remove tudo o que não é dígito
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d)/, "$1/$2");
        v = v.replace(/(\d{2})(\d{2})$/, "$1$2");
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
    return v;
}
function tempo(v, elem) {
    if (v.length < 6) {
        v = v.replace(/\D/g, "");                    //Remove tudo o que não é dígito
        v = v.replace(/(\d{2})(\d)/, "$1:$2");
    } else {
        var str = new String(v);
        return str.substring(0, (str.length - 1));
    }
    return v;
}
function hora(v, elem) {
    v = v.replace(/\D/g, "");                    //Remove tudo o que não é dígito
    v = v.replace(/(\d{2})(\d)/, "$1h$2");
    return v;
}
function rg(v, elem) {
    v = v.replace(/\D/g, "");                                      //Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{7})$/, "$1.$2");    //Coloca o . antes dos últimos 3 dígitos, e antes do verificador
    v = v.replace(/(\d)(\d{4})$/, "$1.$2");    //Coloca o . antes dos últimos 3 dígitos, e antes do verificador
    v = v.replace(/(\d)(\d)$/, "$1-$2");               //Coloca o - antes do último dígito
    return v;
}

function apenasNumeros(v, elem) {
    v = v.replace(/\D/g, "");                                      //Remove tudo o que não é dígito
    return v;
}

function mascara(o, f) {
    v_obj = o;
    v_fun = f;
    setTimeout("execmascara()", 1);
}

function execmascara() {
    v_obj.value = v_fun(v_obj.value, v_obj);
}

function removeErro(elem) {
    if ($(elem).val() !== "")
        $(elem).removeClass('error-input');
    else
        $(elem).addClass('error-input');
}

function ValidaEmail() {
    var email = $("#email");
    var emailConfirmado = $("#confirmarEmail");

    if (email.val() == emailConfirmado.val()) {
        emailConfirmado.removeClass('error-input');
        $('.emailIncorreto').css('visibility', 'hidden');
    }
    else {
        emailConfirmado.addClass('error-input');
        $('.emailIncorreto').css('visibility', 'visible');
    }

}
;



function ValidaSenhas() {
    var senha = $("#senha");
    var senhaConfirmada = $("#confirmarSenha");
    var textoSenhaErro = $("#senhaIncorreta");

    if (senha.val() === senhaConfirmada.val()) {
        senhaConfirmada.removeClass('error-input');
        textoSenhaErro.css("visibility", "hidden");
    }
    else {
        senhaConfirmada.addClass('error-input');
        textoSenhaErro.css("visibility", "visible");
    }

}
;