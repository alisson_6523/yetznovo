/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {

//    var swiper = new Swiper('.swiper-produtos', {
//        slidesPerView: 4,
//        paginationClickable: true,
//        nextButton: '.swiper-button-next',
//        prevButton: '.swiper-button-prev',
//        spaceBetween: 30
//    });

    $('.slide-produtos').each(function () {
        new Swiper($(this), {
            nextButton: $(this).find('.swiper-button-next'),
            prevButton: $(this).find('.swiper-button-prev'),
            paginationClickable: true,
            slidesPerView: 4,
            spaceBetween: 30,
            loop: true
        });
    });
});