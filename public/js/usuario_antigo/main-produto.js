$(document).ready(function () {
    var swiper = new Swiper('.slide-produtos', {
        slidesPerView: 4,
        paginationClickable: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });
});
