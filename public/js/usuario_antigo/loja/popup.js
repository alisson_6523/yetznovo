$(document).ready(function(){
    if(popups.lista_popups.length){
        changePopup(popups.lista_popups[popups.index_atual].foto_popup)
    }

    $("#modal-popup-ilha").on("hidden.bs.modal", function(){
        if(popups.proximo_popup()){
            changePopup(popups.proximo_popup().foto_popup);
            popups.index_atual+=1;
        }
    });
});

function changePopup(link_foto_popup){
    $("#modal-popup-ilha").find(".modal-body > img").attr('src', 'https://larayetz.s3.sa-east-1.amazonaws.com/popup/' + link_foto_popup);
    $("#modal-popup-ilha").modal("show");
}
