$('.quantity-btn').click(function () {
    let inputQtd = $(this).siblings('input');
    let valorQtd = parseInt(inputQtd.val());
    let valorSemFrete = $('#totalSemFrete');
    let valorComFrete = parseInt($('#totalComFrete').text().replace('.', ""));
    let codProduto = $(this).parents('tr').attr('cod-item');
    let textValorTotal = $(this).parents('td').siblings('.item-total-col').find('.valor-multiplicado');
    let textValorUnitario = $(this).parents('td').siblings('.item-price-col').find('.valor-unitario').text().replace('.', "");
    let valorUnitario = parseInt(textValorUnitario.replace('.', ""));
    let sld = $("#saldo-real").val();
    let soma = false;


    if ($(this).hasClass('quantity-input-up')) {
        if (sld >= (valorComFrete + valorUnitario)) {
            soma = true;

            if (!$(this).hasClass('disabled')) {

                valorQtd = valorQtd + 1;
                inputQtd.val(valorQtd);
                textValorTotal.text(mvalor(valorUnitario * valorQtd));
                var valorTotalSemFrete = parseInt(valorSemFrete.text().replace('.', "")) + valorUnitario;
                valorSemFrete.text(mvalor(valorTotalSemFrete));
                valorComFrete = valorComFrete + valorUnitario;
                $('#totalComFrete').text(mvalor(valorComFrete));
            }
            atualizaQtd(codProduto, soma);

        } else {
            document.querySelector('.card-produto .container-card-qtd .produto-qtd span:nth-child(3)').setAttribute('disabled',"disabled")
            alerta('awesome error', 'Saldo insuficiente', 'Você não possui saldo suficiente para realizar esse resgate.', '', 'fa fa-times');
            // $('#geraForm').attr('disabled', 'disabled');
            $('.quantity-input-up').addClass('disabled');
            $('#textoSaldo').removeClass('verde-saldo');
            $('#textoSaldo').addClass('vermelho-saldo');

            return;
        }
    } else {
        if (valorQtd <= 1) {
            inputQtd.val(1);
            textValorTotal.text(mvalor(valorUnitario * 1));
        } else {
            valorQtd = valorQtd - 1;
            inputQtd.val(valorQtd);
            textValorTotal.text(mvalor(valorUnitario * valorQtd));
            valorComFrete = valorComFrete - valorUnitario;
            $('#totalComFrete').text(mvalor(valorComFrete));
            valorSemFrete.text(mvalor(parseInt(valorSemFrete.text().replace('.', "")) - valorUnitario));

            $('#geraForm').prop('disabled', false);
            $('.quantity-input-up').removeClass('disabled');
            $('#textoSaldo').removeClass('vermelho-saldo');
            $('#textoSaldo').addClass('verde-saldo');

            atualizaQtd(codProduto, soma);
        }
    }

});

function format2(n, currency) {
    return currency + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1.');
}

function totalFreteMobile(){
   let itens = document.querySelectorAll('.card-produto .container-card-qtd .produto-sub-total p span');
   let total  = 0;
   itens.forEach(function(item) {
        let valorStr = item.innerHTML.replace(/[\.|\,]/g, '')
        let valor = parseInt(valorStr.substr(0, valorStr.length -2))
        total += valor
   })
   document.querySelector("#totalComFrete").innerHTML = format2(total, '')
}

function valorTotalPontos(){
    // mvalor(parseFloat(document.querySelector('.container-card-produto .text-lista-produto h3 span').innerHTML.replace(/[\.]/g, '')))
    let pontos = document.querySelectorAll('.lista-produtos-mobile .card-produto .container-card-qtd .produto-sub-total p span')
    let totalPontos = 0;
    pontos.forEach(function(ponto){
        let pontoString = ponto.innerHTML.replace(/[\.|\,]/g, '')
        let valor = parseInt(pontoString.substr(0, pontoString.length -2))
        totalPontos += valor
    })
    return totalPontos
}

let cards = Array.from(document.querySelectorAll('.lista-produtos-mobile .card-produto'))
let addItem = document.querySelector('.quantity-input-add')
let subItem = document.querySelector('.quantity-input-sub')
let totalFrete = document.querySelector('#totalComFrete')
let qtdItens = document.querySelector('.qtd-itens')
let pontuacaoAtual = parseInt(document.querySelector('#textoSaldo[data-saldo]').dataset.saldo.replace(/\./g, ''))
let verificaResult = 0;

cards.forEach(function(card){
    let pontosAtual =  mvalor(parseFloat(card.querySelector('.container-card-produto .text-lista-produto h3 span').innerHTML.replace(/[\.]/g, '')))
    let verificaSaldo = document.querySelector('.card-produto .container-card-qtd .produto-qtd span:nth-child(3)')
    let subTotal = card.querySelector('.container-card-qtd .produto-sub-total p span')
    let qtd = card.querySelector('.container-card-qtd .produto-qtd span:nth-child(2)')
    let multiplicador = parseInt(card.querySelector('.container-card-qtd .produto-qtd span:nth-child(2)').innerHTML)
    let subItem = card.querySelector('.container-card-qtd .produto-qtd span:nth-child(1)')
    let addItem = card.querySelector('.container-card-qtd .produto-qtd span:nth-child(3)')


    addItem.addEventListener('click', function(){

        if(multiplicador >= 0){
            multiplicador += 1

            verificaResult = valorTotalPontos()

            if(verificaResult >= pontuacaoAtual){
                console.log('aqui')
                multiplicador -= 1
                alerta('awesome error', 'Saldo Insuficiente', 'Você não possui saldo suficiente para realizar esse resgate.', '', 'fa fa-times');
            }else{
                let result = mvalor((parseInt(pontosAtual.replace(/\./g, '')) * multiplicador),' ')
                result = format2(parseInt(result.replace(/\./g, '')), '')
                subTotal.innerHTML = result
                qtd.innerHTML = multiplicador
                totalFreteMobile()
                qtdItens.innerHTML = multiplicador
                atualizaQtd(card.dataset.code, true);
            }
        }
    })

    subItem.addEventListener('click', function(){

        if(multiplicador > 1){
            multiplicador -= 1
            let result = mvalor((parseInt(pontosAtual.replace(/\./g, '')) * multiplicador),' ')
            result = format2(parseInt(result.replace(/\./g, '')), '')
            subTotal.innerHTML = result
            qtd.innerHTML = multiplicador
            totalFreteMobile()
            qtdItens.innerHTML = multiplicador
            atualizaQtd(card.dataset.code, false);
        }
    })
})


function atualizaQtd(codProduto, soma) {
    if(soma){
        $.ajax({
            url: "/loja/adicionar-item-carrinho",
            type: "POST",
            dataType: "json",
            data: { cod_produto: codProduto },
            async: true,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if(!data.erro)
                    alerta('awesome ok', 'Prêmio adicionado', 'Este prêmio foi adicionado ao seu carrinho!', '', 'fa fa-check');
                else
                    // document.querySelector('.card-produto .container-card-qtd .produto-qtd span:nth-child(3)').setAttribute('disabled',"disabled")
                    alerta('awesome error', data.mensagem, 'Você não possui saldo suficiente para realizar esse resgate.', '', 'fa fa-times');
            },
            error: function (x, t, m) {
                if (t === "timeout") {
                    alert('Conexão está lenta ou é inexistente, tente novamente.');
                } else {
                    // alert("Erro #001");
                }
            }
        });
    } else {
        $.ajax({
            url: "/loja/remover-item-carrinho",
            type: "POST",
            dataType: "json",
            data: { cod_produto: codProduto, unitario: true },
            async: true,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                alerta('awesome yellow', 'Prêmio removido', 'Prêmio removido do carrinho com sucesso!', '', 'fa fa-check');
            }, error: function (x, t, m) {
                if (t === "timeout") {

                    alert('Conexão está lenta ou é inexistente, tente novamente.');
                } else {
                    console.log(t)
                    alert("Erro #001");
                }
            }
        });
    }
};


$('.close-button').click(function (e) {
    let resp = confirm('Tem certeza que deseja remover este produto?');

    if (resp) {
        let codProduto = $(this).parents('tr').attr('cod-item');

        $.ajax({
            url: "/loja/remover-item-carrinho",
            type: "POST",
            dataType: "json",
            data: { cod_produto: codProduto },
            async: true,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                alerta('awesome yellow', 'Prêmio removido', 'Prêmio removido do carrinho com sucesso!', '', 'fa fa-check');
                location.reload();
            }, error: function (x, t, m) {
                if (t === "timeout") {
                    alert('Conexão está lenta ou é inexistente, tente novamente.');
                } else {
                    alert("Erro #001");
                }
            }
        });
    }
});

$('.btn-close-mobile').click(function (e) {
    let resp = confirm('Tem certeza que deseja remover este produto?');


    if (resp) {
        let cod_produto = $(this).parents('.card-produto').attr('data-code')

        $.ajax({
            url: "/loja/remover-item-carrinho",
            type: "POST",
            dataType: "json",
            data: { cod_produto },
            async: true,
            timeout: 10000,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                alerta('awesome yellow', 'Prêmio removido', 'Prêmio removido do carrinho com sucesso!', '', 'fa fa-check');
                location.reload();
            }, error: function (x, t, m) {
                if (t === "timeout") {
                    alert('Conexão está lenta ou é inexistente, tente novamente.');
                } else {
                    alert("Erro #001");
                }
            }
        });
    }
});


$('#geraForm').click(function (e) {
    if (!$('#cep').is(':disabled') && validate_form()) {
        if ($('#codEndereco').val() == 0) {
            $('#cepHidden').val($('#cep').val());
            $('#estadoHidden').val($('#uf').val());
            $('#cidadeHidden').val($('#localidade').val());
            $('#bairroHidden').val($('#bairro').val());
            $('#ruaHidden').val($('#logradouro').val());
            $('#numHidden').val($('#numero').val());
            $('#complementoHidden').val($('#complmento').val());
            $('#destinatarioHidden').val($('#destinatario').val());
            $('#emailDestinatarioHidden').val($('#email').val());
            $('#telefoneDestinatarioHidden').val($('#telefone').val());
            $('#cpfDestinatarioHidden').val($('#cpf').val());
            $("#modalConfirmaResgate").modal("show");
        }
        $('#resultSaldo').text($('#textoSaldo').text() + ' yetz');
        $('#resultValorTotal').text('-' + $('#totalComFrete').text() + ' yetz');
        var conta = parseInt($('#textoSaldo').text().split('.').join("")) - parseInt($('#totalComFrete').text().split(".").join(""));
        $('#resultSaldoFinal').text(mvalor(conta) + ' yetz');
    } else {
        alerta('awesome yellow', 'Endereço', 'Complete seu endereço para entrega dos produtos!', '', 'fa fa-check');
        return false;
    }
});


function validate_form() {
    inputs_endereco = $(".campos-endereco").find("input");
    check = true;

    for (i in inputs_endereco) {
        if (inputs_endereco[i].value == "") {
            $("#" + inputs_endereco[i].id).focus();
            check = false;
            break
        }
    }

    return(check);
};


$("#modalConfirmaResgate .modal-body .confirmacao .opcao").on("click", function() {
    $(this).toggleClass("checked");

    if($(this).hasClass("checked"))
        $("#modalConfirmaResgate .btn-resgatar").attr('disabled', false);
    else
        $("#modalConfirmaResgate .btn-resgatar").attr('disabled', true);

});


$('#formResgate').submit(function(e) {
    e.preventDefault();
    $("#modalConfirmaResgate .btn-resgatar").attr('disabled', true);
    $.ajax({
        url: "/loja/resgatar",
        type: "POST",
        dataType: "json",
        data: { },
        async: true,
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if(!data.erro)
                window.location.href = "/loja/agradecimentos";
            else{
			alerta('awesome error', 'Desculpe!', data.mensagem, '', 'fa fa-times');

            }

        }, error: function (x, t, m) {
            if (t === "timeout") {
                alert('Conexão está lenta ou é inexistente, tente novamente.');
            } else {
                alert("Erro #001");
            }
            // location.reload();
        }
    });
});
