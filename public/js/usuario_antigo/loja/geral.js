$(document).ready(function () {
    let dados = {
        titulo: $(document).find("title").text(),
        rota: window.location.pathname,
        sessao: '',
        data: ''
    };

    $.ajax({
        url: "/loja/salvar-dados",
        type: "POST",
        dataType: "json",
        data: dados,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if (data.erro == false) {
                console.log('Dados salvos com sucesso.');
            }
        },
        error: function (x, t, m) {
            alert(x.responseText);
        }
    });
});

function atualizaNotificacaoStreaming() {
    $.ajax({
        url: "/controller/atualiza-notificacao-streaming.php",
        type: "POST",
        dataType: "json",
        async: true,
        timeout: 10000,
        success: function (data) {
            window.location.href = "/view/videos/index.php";
        },
        error: function (x, t, m) {
            console.log(x.responseText);
        }
    });
}

$(document).on('click', '.menu-categorias .swiper-slide', function () {
    var url = $(this).find('a').attr('href');
    location.href = url;
});




$(document).on('click', '.btn-validar-cupom', function () {
    var valorCupom = $(".input-cupom").val().toUpperCase();

    $.ajax({
     url: "/loja/aplica-cupom",
     type: "POST",
     dataType: "json",
     data: {cupom: valorCupom},
     headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     },
     async: true,
     timeout: 10000,
     success: function (data) {
         if(!data.erro) {
             $(".modal-voucher .box-white").hide();
             $(".modal-voucher .box-sucesso").fadeIn();
             $(".modal-voucher .box-sucesso p").text(data.mensagem);
         } else {
             $(".modal-voucher .box-white").hide();
             $(".modal-voucher .box-erro").fadeIn();
             $(".modal-voucher .box-erro p").text(data.mensagem);
         }
     },
     error: function (x, t, m) {
         console.log("erro");
     }
    });
 })











// === funções js =======================

function favorito(CodProduto, elem) {
    if ($(elem).hasClass("icon-like")) {
        $(elem).removeClass("icon-like");
        $(elem).addClass("icon-unlike").attr({
            "title": "Remover da Lista de Desejos"
        });
        atualizaLike(CodProduto, 1);
    } else {
        $(elem).removeClass("icon-unlike");
        $(elem).addClass("icon-like").attr({
            "title": "Adicionar à Lista de Desejos"
        });
        atualizaLike(CodProduto, 2);
    }
};

function removeFavorito(CodProduto) {
    var produtoEstilo = $("#produto" + CodProduto);
    produtoEstilo.fadeToggle("slow", "linear");
    atualizaLike(CodProduto, 1);
};

function atualizaLike(CodProduto, status) {
    var url = "/loja/desejados";
    var dados = {
        cod_produto: CodProduto
    };

    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        async: true,
        timeout: 10000,
        data: dados,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            console.log(data);
            if (data.erro) {
                if (status == 1)
                    alerta('awesome ok', 'Prêmio adicionado', 'Lista atualizada com sucesso!', '', 'fa fa-check');
                else
                    alerta('awesome yellow', 'Prêmio removido', 'Prêmio removido com sucesso!', '', 'fa fa-check');
            }
        },
        error: function (x, t, m) {
            if (t === "timeout") {
                alert('Conexão está lenta ou é inexistente, tente novamente.');
            } else {
                //alert("Erro #001");
                console.log(x.responseText);
                console.log(t);
                console.log(m);
                alerta('awesome error', 'Erro', 'Falha de atualizar os favoritos', 'Informe este erro ao seu gestor.', 'fa fa-times');
            }
        }
    });
}

function alerta(tema, titulo, menssagem, info, icon) {
    $.amaran({
        'theme': tema,
        'content': {
            title: titulo,
            message: menssagem,
            info: info,
            icon: icon
        },
        'position': 'bottom right',
        'cssanimationIn': 'tada',
        'cssanimationOut': 'bounceOut',
        'resetTimeout': true
    });
}

function deletaProduto(cod) {
    $.ajax({
        url: "/loja/remover-item-carrinho",
        type: "POST",
        dataType: "json",
        data: {
            cod_produto: cod
        },
        async: true,
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            alerta('awesome yellow', 'Prêmio removido', 'Prêmio removido do carrinho com sucesso!', '', 'fa fa-check');
            calcCarrinho();

            if (window.location.pathname == '/loja/carrinho')
                location.reload();

        },
        error: function (x, t, m) {
            if (t === "timeout") {
                alert('Conexão está lenta ou é inexistente, tente novamente.');
            } else {
                alert("Erro #001");
            }
        }
    })
}

function addProduto(cod) {
    $.ajax({
        url: "/loja/adicionar-item-carrinho",
        type: "POST",
        dataType: "json",
        data: {
            cod_produto: cod
        },
        async: true,
        timeout: 10000,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            if (!data.erro) {
                $('#produtosCarrinho h1').remove();
                var elem = $(".carrinho .produto-carrinho[cod-produto='" + data.dados.cod_produto + "']");
                var elem_responsivo = $(".resumo-carrinho-mobile .produto-carrinho[cod-produto='" + data.dados.cod_produto + "']");
                if (elem.length == 0) {
                    var img = '<img class="foto-produto-carrinho" src="https://larayetz.s3.sa-east-1.amazonaws.com/produtos/' + data.dados.foto_exemplo + '">';
                    var desc = '<div class="texto-carrinho"><p>' + data.dados.categoria.categoria + '</p><p>' + data.dados.nome_produto + ' - ' + data.dados.valor_reais.toLocaleString("pt-BR", {
                        style: "currency",
                        currency: "BRL"
                    }) + '</p></div>';
                    var btn = '<div class="btns"><span><i class="fa fa-shopping-basket" aria-hidden="true"></i><span class="qtd-produto">1</span></span><span><i class="fa fa-trash-o btn-excluir" aria-hidden="true"></i></span></div>';
                    var valor = '<div class="valor pull-right"><span>' + mvalor(data.dados.valor) + ' YETZ</span></div>';
                    $(".resumo-carrinho #produtosCarrinho").append('<div class="produto-carrinho" cod-item="' + data.dados.cod_item + '" cod-produto="' + data.dados.cod_produto + '"><div class="valor-produto">' + img + desc + btn + valor + '</div></div>');
                    $(".resumo-carrinho-mobile #produtosCarrinho").append('<div class="produto-carrinho" cod-item="' + data.dados.cod_item + '" cod-produto="' + data.dados.cod_produto + '"><div class="valor-produto">' + img + '<div class="bloco-informacoes"><div class="bloco-texto">' + desc + btn + valor + '</div></div></div></div>');
                } else {
                    var qtd = parseInt(elem.find('.qtd-produto').text());

                    elem.find('.qtd-produto').text(qtd + 1);
                    elem.find('.valor').text(mvalor(parseInt(data.dados.valor)) + ' YETZ');

                    elem_responsivo.find('.qtd-produto').text(qtd + 1);
                    elem_responsivo.find('.valor span').text(mvalor(parseInt(data.dados.valor)) + ' YETZ');
                }
                alerta('awesome ok', 'Prêmio adicionado', 'Este prêmio foi adicionado ao seu carrinho!', '', 'fa fa-check');
            }
            calcCarrinho();
        },
        error: function (x, t, m) {
            if (t === "timeout") {
                alert('Conexão está lenta ou é inexistente, tente novamente.');
            } else {
                //alert("Erro #001");
                console.log(x.responseText);
                console.log(t);
                console.log(m);
            }
        }
    });
}

function calcCarrinho() {
    var qtd_itens_responsivo = document.querySelector(".saldo-carrinho-responsivo .btn-carrinho .qtd-itens");
    var valUnidades = $('.resumo-carrinho .produto-carrinho');
    var valProdPag = $('.box-produto, .descricao-produto');
    var saldo = parseInt($('#saldoCabecalho').text().replace(/\D/g, ''));
    var totalCarrinho = 0;
    $.each(valUnidades, function (nome, produto) {
        var qtd = parseInt(produto.querySelector(".qtd-produto").textContent)
        totalCarrinho += (parseInt(produto.querySelector(".valor").textContent.replace(/\D/g, '')) * qtd);
    });
    var saldoFalso = saldo - totalCarrinho;
    var txtQtdItens = $('.resumo-carrinho #produtosCarrinho .produto-carrinho');
    var qtdItens = 0;
    $.each(txtQtdItens, function (nome, valor) {
        qtdItens += parseInt(valor.querySelector('.qtd-produto').textContent);
    });
    if (qtdItens < 10) {
        if (qtdItens < 1)
            $('.num-itens-carrinho').text('');
        else
            $('.num-itens-carrinho').text(' - 0' + qtdItens);
    } else {
        $('.num-itens-carrinho').text(' - ' + qtdItens);
    }

    $('.valor-total span').text('Total: ' + mvalor(totalCarrinho) + ' YETZ');
    $.each(valProdPag, function (index, prod) {
        var valProd = parseInt($(prod.querySelector(".saldo")).text().replace(/\D/g, ''));
        var porcentagem = (saldoFalso / valProd) * 100;
        var progress = prod.querySelector(".progress-bar")
        var box_produto = prod.querySelector(".area-valores")

        if (!box_produto) {
            box_produto = prod
            box_produto.classList = "descricao-produto"
        } else {
            box_produto.classList = "area-valores"
        }

        if (porcentagem <= 0) {
            box_produto.classList.add("vermelho")
            porcentagem = 0;
        } else if (porcentagem < 60) {
            box_produto.classList.add("vermelho")
        } else if (porcentagem >= 60 && porcentagem < 100) {
            box_produto.classList.add("amarelo")
        } else {
            box_produto.classList.add("verde")
            porcentagem = 100;
        }
        $(progress).css('width', porcentagem + "%");
    });

    qtd_itens_responsivo.textContent = qtdItens;

    checkCarrinho();
}

function mvalor(v) {
    v = v.toString();
    v = v.replace(/\D/g, ""); //Remove tudo o que não é dígito
    v = v.replace(/(\d)(\d{6})$/, "$1.$2"); //coloca o ponto dos milhões
    v = v.replace(/(\d)(\d{3})$/, "$1.$2"); //coloca o ponto dos milhares

    //v = v.replace(/(\d)(\d{2})$/, "$1,$2");//coloca a virgula antes dos 2 últimos dígitos
    return v;
}

var status_usuario = 1;

if (status_usuario === 2) {
    $('#modalTrocaSenha').modal('show');
}

function verificaCapsCadastro(elem, event) {
    if (event.getModifierState('CapsLock')) {
        $(elem).parents(".form-group")[0].querySelector(".alerta-caps").classList.add("active");
    } else {
        $(elem).parents(".form-group")[0].querySelector(".alerta-caps").classList.remove("active");
    }
}


