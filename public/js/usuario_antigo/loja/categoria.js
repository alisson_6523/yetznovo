$(document).ready(function(){
    $("#input-menor-preco input").val(mascaraFloat("<?=$v1?>"));
    $("#input-maior-preco input").val(mascaraFloat("<?=$v2?>"));

    alinhaProdutos();
});

$(".dropdown-menu").click(function(e){
    e.stopPropagation();
});

$(".input-pesquisa").keyup(function () {
    var valorBusca = $(this).val().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    var nomeProdutos = $('.informacoes-produto h1');
    $('.prod-destaques .box-produto').show();

    $.each(nomeProdutos, function (index, nome) {
        if ($(nome).text().normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().indexOf(valorBusca.toLowerCase()) < 0) {
            $(nome).parents('.box-produto').hide();
        }
    });

    alinhaProdutos();
});

$(".btn-adiciona-produto").on("click", function () {
    var area_valores = $(this).parents(".area-valores")

    if(area_valores.hasClass("verde")){
        addProduto($(this).attr("cod-item"));
    } else {
        alerta('awesome yellow', 'Saldo insuficiente', 'Acumule mais pontos para resgatar este prêmio!', '', 'fa fa-check');
    }
});

$("#lista-menores-valores > a").click(function(){
    $("#input-menor-preco input").val(mascaraFloat(this.text));
    $("#input-maior-preco input").focus()
});

let cardsGlobal = Array.from(document.querySelectorAll('.box-produto[data-preco]'))


function filtroValores(){
    $('.prod-destaques').append(cardsGlobal)

    let menorValor = parseInt(document.querySelector('#input-menor-preco input').value.replace(/\./g, ""))
    let maiorValor = parseInt(document.querySelector('#input-maior-preco input').value.replace(/\./g, ""))
    let boxs = Array.from(document.querySelectorAll('.prod-destaques .box-produto[data-preco]'))
    let cards = [];

    boxs.forEach(function(box){
        if((menorValor <= parseInt(box.dataset.preco.replace(/\./g, ""))) &&  (maiorValor >= parseInt(box.dataset.preco.replace(/\./g, "")))){
            cards.push(box)
        }
    })

    cards.sort(function(a, b){
        return parseInt(a.dataset.preco.replace(/\./g, "")) - parseInt(b.dataset.preco.replace(/\./g, ""))
    })

    $('.prod-destaques').empty()
    $('.prod-destaques').append(cards)

    $(".btn-adiciona-produto").on("click", function () {
        var area_valores = $(this).parents(".area-valores")

        if(area_valores.hasClass("verde")){
            addProduto($(this).attr("cod-item"));
        } else {
            alerta('awesome yellow', 'Saldo insuficiente', 'Acumule mais pontos para resgatar este prêmio!', '', 'fa fa-check');
        }
    });
}


$("#lista-maiores-valores > a").click(function(){
    if(parseInt(this.text)){
        $("#input-maior-preco input").val(mascaraFloat(this.text));

        if($('#input-menor-preco input').val()){
            $('.prod-destaques').empty()
            $('.prod-destaques').append(cardsGlobal)

            filtroValores()
        }

    }else{
        $("#input-maior-preco input").val(mascaraFloat("<?=$rang->maximo?>"));
    }
});

$("#input-menor-preco input").keyup(function(e){
    if(e.keyCode == 13){
        filtroValores()
        $("#input-maior-preco input").focus();
    }else{
        this.value = mascaraFloat(this.value) ;

    }
});

$("#input-menor-preco input").focus(function(e){
    $("#lista-menores-valores").css("display", "block");
    $("#lista-maiores-valores").css("display", "none");
});

$("#input-maior-preco input").focus(function(e){
    $("#lista-menores-valores").css("display", "none");
    $("#lista-maiores-valores").css("display", "block");
});


$("#input-maior-preco input").keyup(function(e){
    if(e.keyCode == 13){
        filtroValores()
    }else{
        this.value = mascaraFloat(this.value) ;
    }
});

function atualizaIconeProduto() {
    document.querySelectorAll(".box-produto").forEach(function(box){
        var botao = box.querySelector(".btn-adiciona-produto");
        var status_produto = box.querySelector(".progress").classList[1];

        botao.className = "btn-adiciona-produto " + status_produto.replace("progress-", "carrinho-");
    });
};

function mascaraFloat(input){
    val = input.match(/\d/g);

    if(val){
        return val.join("").split(/(?=(?:...)*$)/).join('.');
    }else{
        return "";
    }
}

function alinhaProdutos() {
    if(window.screen.width > 991){
        var produtos = document.querySelectorAll(".box-produto");
        var produtos_visiveis = Array.from(produtos.values()).filter(function(produto){
            return produto.style.display != 'none';
        });

        produtos_visiveis.forEach(function(produto) {
            produto.classList.remove('second-child');
        });

        for (i=1; i < produtos_visiveis.length; i+=3) {
            produtos_visiveis[i].classList.add("second-child");
        }
    }
}

/*** Alisson  */

let cartaoTipo = document.querySelector('#cartao-tipo');
let ordernar = document.querySelector('#ordernar');

cartaoTipo.addEventListener('click', function(e){
        let itens = Array.from(cartaoTipo.querySelectorAll('option'))

        let optionSelect = itens.find(e => e.selected ? e.value : null).value

        // 1 fisico
        // 2 voucher
        let fisico = Array.from(document.querySelectorAll('.box-produto[data-tipo="1"]'));
        let voucher = Array.from(document.querySelectorAll('.box-produto[data-tipo="2"]'));

        switch(optionSelect){
            case 'fisico':
                fisico.forEach(function(item){
                    item.style.display = "block";
                })

                voucher.forEach(function(item){
                    item.style.display = "none";
                })
                break;

            case 'voucher':
                fisico.forEach(function(item){
                    item.style.display = "none";
                })

                voucher.forEach(function(item){
                    item.style.display = "block";
                })
                break;

                default :
                    Array.from(document.querySelectorAll('.box-produto')).forEach(function(item){
                        item.style.display = "block";
                    })
        }
})



ordernar.addEventListener('click', function(){
    let itens = Array.from(ordernar.querySelectorAll('option'))

    let optionSelect = itens.find(e => e.selected ? e.value : null) .value

    let cards = Array.from(document.querySelectorAll('.box-produto[data-preco]'))

    switch(optionSelect){
        case 'maiorpreco':
                cards.sort(function(a, b){
                    return parseFloat(b.dataset.preco.replace(/\./g, "")) - parseFloat(a.dataset.preco.replace(/\./g, ""))
                })
                $('.prod-destaques').empty()
                $('.prod-destaques').append(cards)
        break;

        case 'menorpreco':
            cards.sort(function(a, b){
                return parseFloat(a.dataset.preco.replace(/\./g, "")) - parseFloat(b.dataset.preco.replace(/\./g, ""))
            })

            $('.prod-destaques').empty()
            $('.prod-destaques').append(cards)
        break

        default:
            $('.prod-destaques').empty()
            $('.prod-destaques').append(cardsGlobal)
    }

    $(".btn-adiciona-produto").on("click", function () {
        var area_valores = $(this).parents(".area-valores")

        if(area_valores.hasClass("verde")){
            addProduto($(this).attr("cod-item"));
        } else {
            alerta('awesome yellow', 'Saldo insuficiente', 'Acumule mais pontos para resgatar este prêmio!', '', 'fa fa-check');
        }
    });

})
