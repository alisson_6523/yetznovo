    //var musica_yetz = document.querySelector("#musica-yetz");

    $(document).ready(function(){

        $.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
    });

    let cod_campanha_esconde_campos = 0;


    var progress_bar = document.querySelector("#progress-login .progress-bar");
    var formSubmitting = false;
    $(".check").click(function () {
        $(this).toggleClass("active-check");
    })
    var chave_campanha;
    var tipo_login;
    function Trim(str) {
        return str.replace(/^\s+|\s+$/g, "");
    }
    function buscarChave() {
        var valChave = $("#chave-cliente").val();
        if (!formSubmitting) {
            if (valChave != '') {
                var dados = {
                    chave: valChave
                };
                formSubmitting = true;
                $.ajax({
                    url: "/login/busca-chave",
                    type: "POST",
                    dataType: "json",
                    data: dados,
                    async: true,
                    timeout: 10000,
                    success: function (data) {

                        var inputRE = '<div class="form-group"><input type="text" name="login" class="form-control input-lg" id="matricula-primeiro-acesso" placeholder="IDENTIFICAÇÃO PESSOAL"></div>';
                        var botaoVerificar = '<div class="form-group"> <input type="button" id="btn-verificar-cadastro" value="VERIFICAR"></div>';
                        chave_campanha = valChave;
                        tipo_login = data.tipo_autenticacao;
                        $(".conteudo-login .conteudo-form form").append(inputRE + botaoVerificar);
                        $("#chave-cliente").attr("disabled", true);
                        $("#btn-buscar-chave").hide();
                        
                        formSubmitting = false;
                    },
                    error: function (x, t, m) {
                        if (t === "timeout") {
                            alert('Conexão está lenta ou é inexistente, tente novamente.');
                        } else {
                            alerta('awesome yellow', 'Chave Inválida.', 'Confira a chave informada.', '', 'fa fa-check');
                        }
                        formSubmitting = false;
                    }
                });
            } else {
                alerta('awesome yellow', 'Chave Inválida.', 'Confira a chave informada.', '', 'fa fa-check');
            }
        }
    };
    function refreshFormLogin() {
        let template = "<div class=\"form-group\">\n  <div class=\"input-group\">\n      <input type=\"text\" name=\"chave\" class=\"form-control input-lg\" id=\"chave-cliente\" onkeyup=\"this.value = Trim(this.value)\" placeholder=\"Chave do cliente\"> \n <span class=\"input-group-addon\" id=\"btn-refresh-chave\" onclick=\"refreshFormLogin()\"><img src=\"/img/icon-refresh.png\" onclick=\"refreshFormLogin()\"></span>\n  </div>\n</div>\n<div class=\"form-group\">\n  <input type=\"button\" id=\"btn-buscar-chave\" value=\"BUSCAR\" onclick=\"buscarChave()\">\n    </div>"
        $(".conteudo-login .conteudo-form form").html(template);
    };
    function recaptchaCallback() {
        $('#btn-acessar-cadastro').removeAttr('disabled');
    }
    ;
    $('.conteudo-login .conteudo-form form').on('keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            if ($("#btn-buscar-chave")[0].style.display == "") {
                $("#btn-buscar-chave").click();
                e.preventDefault();
            } else if ($("#btn-verificar-cadastro")[0].style.display == "") {
                $("#btn-verificar-cadastro").click();
                e.preventDefault();
            }
        }
    });
    $(document).on('click', '#btn-verificar-cadastro', function (e) {
        var valChave = $('#chave-cliente').val();
        var valCpr = $('#cpf-primeiro-acesso').val();
        var valRe = $('#matricula-primeiro-acesso').val();
        var valSuper = $('#super-primeiro-acesso').val();

        if (valRe != null) {
            var dados = {
                login: valRe.toUpperCase(),
                chave: valChave
            };
        } else if (valSuper != null) {
            var dados = {
                cartao: valSuper,
                chave: valChave
            };
        }
        if (!formSubmitting) {
            formSubmitting = true;
            $.ajax({
                url: "/login/primeiro-acesso",
                type: "POST",
                dataType: "json",
                data: dados,
                async: true,
                timeout: 10000,
                success: function (data) {
                    if (data.usuario.status == 1) {
                        $("#cpf-primeiro-acesso").attr("readonly", true);
                        var inputSenha = '<div class="form-group"><input type="password" name="password" class="form-control input-lg" onkeydown="verificaCaps(event)" placeholder="Digite sua senha"></div>';
                        var msgCaps = '<p id="msgCaps" style="display:none">Caps Lock Ativado</p>'
                        var botaoLogar = '<div class="form-group"> <input type="submit" id="btn-entrar" value="ENTRAR"></div>';
                        var esqueciMinhaSenha = '<div class="form-group"><a id="esqueci-minha-senha">Esqueci minha senha</a></div>';
                        $(".conteudo-login .conteudo-form form").append(inputSenha + msgCaps + botaoLogar + esqueciMinhaSenha);
                        $("#btn-verificar-cadastro").hide();
                        
                    } else if (data.usuario.status == 0) {
                        if(data.cod_campanha == 64){
                            cod_campanha_esconde_campos = 64;
                            $("#form-email").remove();
                            $("#form-email-confirmar").remove();
                            $("#form-celular").remove();
                            $("#form-telefone").remove();
                            $("#checkboxDiv").remove();
                        }
                        $('#modalCompletarCadastro').modal('hide');
                        $('#modalFinalizarCadastro').modal('show');
                        $('#acNome').val(data.usuario.nome_usuario);
                        $('#cpf-finalizar-cadastro').val(data.usuario.login);
                        $('#acEmail').focus();
                        
                    };
                    formSubmitting = false;
                },
                error: function (x, t, m) {
                    if (t === "timeout") {
                        alert('Conexão está lenta ou é inexistente, tente novamente.');
                    } else {
                        alerta('awesome error', 'Falha de autenticação.', 'Confira os dados informados', '', 'fa fa-check');
                    }
                    formSubmitting = false;
                }
            });
        }
    });
    $(document).on('click', '#esqueci-minha-senha', function (e) {
        $("#formAcesso").hide()
        $("#formEsqueciSenha").show();
        var campoChave = $(this).parents(".conteudo-form").find("#chave-cliente").val();
        $("#chave-acesso").val(campoChave);
        if (tipo_login == "cpf") {
            input_cpf = '<input type="text" class="form-control input-lg " placeholder="IDENTIFICAÇÃO PESSOAL"/>';
            $(this).parents(".conteudo-form").find("#cpf-esqueci-senha").append(input_cpf);
            $(this).parents(".conteudo-form").find("#cpf-esqueci-senha").find("input").mask("999.999.999-99");
        } else {
            input_cpf = '<input type="text" class="form-control input-lg" placeholder="IDENTIFICAÇÃO PESSOAL"/>';
            $(this).parents(".conteudo-form").find("#cpf-esqueci-senha").append(input_cpf);
        }
    });
    $(document).ready(function () {
        document.getElementById('form-email-confirmar').onpaste = function (e) {
            e.preventDefault();
        };
        jQuery(".mask-cpf").mask("999.999.999-99");
        jQuery(".mask-data").mask("99/99/9999");
        jQuery(".mask-super").mask("9999-9999-9999-9999");
        //jQuery("#inputTelefoneFixo").mask("(99)9999-9999");
        jQuery(".mask-celular")
            .mask("(99)9 9999-9999")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                phone = target.value.replace(/\D/g, '');
                element = $(target);
                element.mask("(99)9 9999-9999");
            });
        jQuery(".mask-telefone")
            .mask("(99) 9999-9999")
            .focusout(function (event) {
                var target, phone, element;
                target = (event.currentTarget) ? event.currentTarget : event.srcElement;
                phone = target.value.replace(/\D/g, '');
                element = $(target);
                element.mask("(99) 9999-9999");
            });
    });
    function alerta(tema, titulo, menssagem, info, icon) {
        $.amaran({
            'theme': tema,
            'content': {
                title: titulo,
                message: menssagem,
                info: info,
                icon: icon
            },
            'position': 'bottom right',
            'cssanimationIn': 'tada',
            'cssanimationOut': 'bounceOut',
        });
    }
    $(".btn-cancelar-esqueci-senha").on("click", function () {
        $("#formAcesso").show()
        $("#formEsqueciSenha").hide();
        $(this).parents(".conteudo-form").find("#cpf-esqueci-senha").find("input").remove();
    });
    $("#btn-troca-senha").click(function () {
        valLogin = $('#formEsqueciSenha').find("#cpf-esqueci-senha").find("input").val();
        var dados = {
            chave: chave_campanha,
            login: valLogin
        };
        $.ajax({
            url: "/recuperar-senha",
            type: "POST",
            dataType: "json",
            data: dados,
            async: true,
            timeout: 10000,
            success: function (data) {
                console.log(data);
                if (!data.erro) {
                    alerta('awesome ok', 'Sucesso!', 'Confira o seu email para definir uma nova senha!', '', 'fa fa-check');
                } else{
                    alerta('awesome error', 'Erro de acesso!', data.mensagem, '', 'fa fa-times');
                }
                $("#formAcesso").show();
                $("#formEsqueciSenha").hide();
            },
            error: function (x, t, m) {
                if (t === "timeout") {
                    alert('Conexão está lenta ou é inexistente, tente novamente.');
                } else {
                    alerta('awesome yellow', '','Erro de acesso!', 'Verifique os dados informados', '', 'fa fa-times');
                }
            }
        });
    });
    $("#modalTermosUso").on("show.bs.modal", function () {
        $("#modalFinalizarCadastro").modal("hide");
    });
    $("#modalTermosUso").on("hide.bs.modal", function () {
        $("#modalFinalizarCadastro").modal("show");
    });
    $("#concluiPrimeiroAcesso").click(function () {
        if(cod_campanha_esconde_campos == 64){
            informacoes_acesso = {
                nome: $('#acNome').val(),
                login: $("#cpf-finalizar-cadastro").val(),
                apelido: $('#acApelido').val(),
                password: $('#acSenha').val(),
                senha_confirmacao: $('#acConfirmaSenha').val(),
                data_nascimento: $('#dataNasc').val(),
                check_termos: $("#checkboxDiv2 .check").hasClass("active-check") ? 1 : 0,
                sexo: $("#acSexo").val(),
                chave: $("#chave-cliente").val()
            }
        } else {
            console.log('passou else');
            informacoes_acesso = {
                nome: $('#acNome').val(),
                login: $("#cpf-finalizar-cadastro").val(),
                email: $('#acEmail').val(),
                email_confirmacao: $('#acConfirmarEmail').val(),
                apelido: $('#acApelido').val(),
                telefone: $('#inputTelefoneFixo').val(),
                celular: $('#inputCelular').val(),
                password: $('#acSenha').val(),
                senha_confirmacao: $('#acConfirmaSenha').val(),
                data_nascimento: $('#dataNasc').val(),
                notificacao_celular: $("#checkboxDiv .check").hasClass("active-check") ? 1 : 0,
                check_termos: $("#checkboxDiv2 .check").hasClass("active-check") ? 1 : 0,
                sexo: $("#acSexo").val(),
                chave: $("#chave-cliente").val()
            }
        }
        Object.freeze(informacoes_acesso);
        inputs = document.querySelectorAll("#modalFinalizarCadastro input:not([type='button'])");
        inputs.forEach(function (input) {
            validaInput(input);
        });
        campos_invalidos = document.querySelectorAll("#modalFinalizarCadastro .campo-invalido");
        if (campos_invalidos.length == 0) {
            if (informacoes_acesso.check_termos) {
                $("#modalFinalizarCadastro").modal("hide");
                $("#modalConfirmarInformacoes").modal("show");
                fillModalInformacoes("#informacoes-confirma-cadastro", informacoes_acesso);
            } else {
                alerta('awesome yellow', 'Termos de Uso não aceitos', 'Leia os termos de uso e marque a caixa de aceitação.', '', 'fa fa-check');
            }
        } else if (campos_invalidos[0].type == 'email') {
            alerta('awesome yellow', 'Erro no Email!', 'Os emails não correspondem ou são inválidos!', '', 'fa fa-check');
        } else if (campos_invalidos[0].type == 'password') {
            alerta('awesome yellow', 'Erro na Senha!', 'As senhas não correspondem ou não obedecem o critério de conter no minimo 6 caracteres!', '', 'fa fa-check');
        } else {
            alerta('awesome yellow', 'Erro no cadastro!', 'Preencha os campos obrigatórios!', '', 'fa fa-check');
        }
    });
    function validaInput(input) {
        if (input.checkValidity()) {
            input.parentElement.classList.remove("campo-invalido");
            if (input.id == "acEmail" || input.id == "acConfirmarEmail") {
                if (!input.checkValidity() || (informacoes_acesso.email !== informacoes_acesso.email_confirmacao)) {
                    input.parentElement.classList.add("campo-invalido");
                }
            } else if (input.id == "acSenha" || input.id == "acConfirmaSenha") {
                if (!input.checkValidity() || (informacoes_acesso.password !== informacoes_acesso.senha_confirmacao)) {
                    input.parentElement.classList.add("campo-invalido");
                }
            }
        } else {
            input.parentElement.classList.add("campo-invalido");
        };
    }
    $("#modalConfirmarInformacoes").on("hidden.bs.modal", function () {
        $("#modalFinalizarCadastro").modal("show");
    });
    $("#cancelarCadastroPrimeiroAcesso").click(function () {
        $("#modalConfirmarInformacoes").modal("hide");
    });
    $("#confirmarCadastroPrimeiroAcesso").click(function () {
        $.ajax({
            url: "/login/cadastro",
            type: "POST",
            dataType: "json",
            data: informacoes_acesso,
            async: true,
            timeout: 10000,
            success: function (data) {
                // console.log(data);
                location.href = "/loja";
            },
            error: function (x, t, m) {
                console.log(x.responseText);
            }
        });
    });
    function fillModalInformacoes(elementSelector, informacoes_acesso) {
        element = document.querySelector(elementSelector);
        element.innerHTML = "";
        p = document.createElement("p");
        element.appendChild(setText(p.cloneNode(), "<strong>Nome:</strong> " + informacoes_acesso.nome));
        element.appendChild(setText(p.cloneNode(), "<strong>Matricula/RE:</strong> " + informacoes_acesso.login));
        element.appendChild(setText(p.cloneNode(), "<strong>Apelido:</strong> " + informacoes_acesso.apelido));
        element.appendChild(setText(p.cloneNode(), "<strong>Data Nascimento:</strong> " + informacoes_acesso.data_nascimento));
        if (informacoes_acesso.sexo == 'm') {
            element.appendChild(setText(p.cloneNode(), "<strong>Sexo:</strong> Masculino"));
        } else {
            element.appendChild(setText(p.cloneNode(), "<strong>Sexo:</strong> Feminino"));
        }
        if(cod_campanha_esconde_campos != 64){
            element.appendChild(setText(p.cloneNode(), "<strong>Email:</strong> " + informacoes_acesso.email));
            element.appendChild(setText(p.cloneNode(), "<strong>Celular:</strong> " + informacoes_acesso.celular));
            element.appendChild(setText(p.cloneNode(), "<strong>Telefone:</strong> " + informacoes_acesso.telefone));
            if (informacoes_acesso.notificacao_celular) {
                element.appendChild(setText(p.cloneNode(), "<strong>Aceita Receber Notificações:</strong> Sim"));
            } else {
                element.appendChild(setText(p.cloneNode(), "<strong>Aceita Receber Notificações:</strong> Não"));
            }
        }
    };
    function setText(element, text) {
        element.innerHTML = text;
        return element;
    }
    function verificaCaps(e) {
        e.getModifierState('CapsLock') ? $("#msgCaps").show() : $("#msgCaps").hide();
    }
    $("#formAcesso").submit(function (e) {
        e.preventDefault();
        e.stopPropagation();
        var data = {
            password: this.password.value,
            chave: this.chave.value,
            login: this.login.value,
        }

        $.ajax({
            url: "/login",
            type: "POST",
            dataType: "json",
            data: data,
            async: true,
            timeout: 10000,
            success: function (data) {
                if (data.usuario) {

                    // alert(data.usuario.nome_usuario);
                    //musica_yetz.play();
                   location.href = "/loja";
//                    $("#formAcesso").usuario_antigo("display", "none");
//                    $("#progress-login").usuario_antigo("display", "block");
//                    $("#esqueci-minha-senha").usuario_antigo("display", "none");
                } else {
                    alerta('awesome error', 'Senha inválida', 'Verifique os dados informados', '', 'fa fa-times');     
                }
            },
            error: function (x, t, m) {
                console.log(x.responseText);
                alerta('awesome error', 'Senha inválida', 'Verifique os dados informados', '', 'fa fa-times');
            }
        });
    });
    function verificaCapsCadastro(elem, event) {
        if (event.getModifierState('CapsLock')) {
            $(elem).parents(".form-group")[0].querySelector(".alerta-caps").classList.add("active");
        } else {
            $(elem).parents(".form-group")[0].querySelector(".alerta-caps").classList.remove("active");
        }
    }
    $(".video-sobre .thumb-video").click(function () {
        $("#modal-media").modal("show");
    });
    $("#modal-media").on("shown.bs.modal", function () {
        $("#modal-media video")[0].play();
    });
    $("#modal-media").on("hide.bs.modal", function () {
        $("#modal-media video")[0].pause();
    });
