$(".icone-carrinho").click(function () {
    if ($(this).hasClass("carrinho-nao-selecionado")) {
        $(this).removeClass("carrinho-nao-selecionado");
        $(this).addClass("carrinho-selecionado");
        addProduto($(this).attr("cod-item"));
    } else {
        $(this).removeClass("carrinho-selecionado");
        $(this).addClass("carrinho-nao-selecionado");
        deletaProduto($(this).attr("cod-item"));
    }
})
var cod_end_padrao = 0;

var swiper = new Swiper('.slide-produtos-relacionados', {
    slidesPerView: 4,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    spaceBetween: 30
});

$(function () {

    $('.botao-bivolt').change(function () {
        $('#voltagem').val($(this).val());
    });

    $('.btn-collapsed').click(function () {
        var cod = $('#cod_endereco').val();
        limpaCamposEnd();
        $("#cep").val("");
        if (cod == "") {
            $('#cod_endereco').val(cod_end_padrao);
            $(this).text("Desejo receber no endereço cadastrado");
            $(".centraliza-btn-endereco").css("width", "340px");
            $("#collapseExample").addClass("in");
            $('#confirmaCompraProduto').attr("disabled", "disabled");

        } else {
            $('#cod_endereco').val("");
            $(this).text("Desejo receber em outro endereço");
            $(".centraliza-btn-endereco").css("width", "295px");
            $("#collapseExample").removeClass("in");
            $('#confirmaCompraProduto').removeAttr("disabled", "disabled");
        }
    });

    $("button.btnProcuraCep").click(function (evento) {
        evento.preventDefault();
        carregarCepNovo();
    });

    function carregarCepNovo() {
        $("#cep").addClass("success-input");
        $("#carregando").css("visibility", "visible");

        var cep = $("#cep").val();
        var url = "../cadastro/cep.php?cep=" + cep;
        $.getJSON(url, function (dados) {
            $("#carregando").css("visibility", "hidden");
            limpaCamposEnd();
            $('#confirmaCompraProduto').removeAttr("disabled", "disabled");

            if (dados == "false") {
                $("#cep").removeClass("success-input");
                $("#cep").addClass("error-input");
                alert("CEP está errado!");
                return false;
            }

            $('#estado').val(dados.uf);
            $('#cidade').val(dados.cidade);

            if (!(dados.bairro) == "") {
                $('#bairro').val(dados.bairro);
            } else {
                $('#bairro').removeAttr("readonly");
            }

            if (!(dados.logradouro) == "") {
                var logradouro = dados.logradouro;
                retorno = logradouro.split(' - ');
                $('#logradouro').val(retorno[0]);
            } else {
                $('#logradouro').removeAttr("readonly");
            }

            if (dados.bairro == "" || dados.logradouro == "")
                alert("Por favor complete os campos vazios");
            else
                $("#numero").focus();
        });
    }

    function limpaCamposEnd() {
        $("#estado").val("");
        $("#cidade").val("");
        $("#bairro").val("");
        $("#logradouro").val("");
        $("#numero").val("");
        $("#complemento").val("");

        $("#bairro").attr('readonly', 'true');
        $("#logradouro").attr('readonly', 'true');
    }

});

function atualizaIconeProduto() {
    document.querySelectorAll(".box-produto").forEach(function(box){
        var botao = box.querySelector(".btn-adiciona-produto");
        var status_produto = box.querySelector(".progress").classList[1];

        botao.className = "btn-adiciona-produto " + status_produto.replace("progress-", "carrinho-");
    });
};
$(".btn-adiciona-produto").on("click", function () {
    var area_valores = $(this).parents(".area-valores")

    if(!area_valores[0]) {
        area_valores = $(this).parents(".descricao-produto")
    }

    if(area_valores.hasClass("verde")){
        addProduto($(this).attr("cod-item"));
    } else {
        alerta('awesome yellow', 'Saldo insuficiente', 'Acumule mais pontos para resgatar este prêmio!', '', 'fa fa-check');
    }
});