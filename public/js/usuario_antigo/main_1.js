function direcionamento(codProduto) {

    var url = "../../view/resgate/detalhes-produto.php?produto=" + codProduto;

    location.href = url;
}


$("#btn-categoria").click(function () {
    $(".dropdown-todas-categorias").slideToggle(function () {
        $(this).removeClass("sumir");
        $(this).addClass("aparecer");
    })
    if ($(this).find("#seta-categoria").hasClass("normal")) {
        $(this).find("#seta-categoria").removeClass("normal");
        $(this).find("#seta-categoria").addClass("girar");
    } else {
        $(this).find("#seta-categoria").removeClass("girar");
        $(this).find("#seta-categoria").addClass("normal");
    }
})

$(document).on("click", ".btn-excluir", function () {
    var codProduto = $(this).parents(".produto-carrinho").attr('cod-item');
    deletaProduto(codProduto);
    $(this).parents(".produto-carrinho").fadeOut('slow', function () {
        $(this).remove();
        if ($('#produtosCarrinho .produto-carrinho').length == 0) {
            $('#produtosCarrinho').append('<h1 class="aviso-vazio">Carrinho vazio!</h1>');
        }
        calcCarrinho();
    });
})

$(document).click(function (e) {
    if (!$(e.target).closest('.btn-categorias').length && !$(e.target).closest('.dropdown-todas-categorias').length) {
        $('.dropdown-todas-categorias').slideUp();
    }
});

$("#btn-carrinho").click(function () {
    $(".resumo-carrinho").slideToggle(function () {
        $(this).removeClass("sumir");
        $(this).addClass("aparecer");

        if ($(this).parents(".saldo-carrinho").find(".saldo-novo").hasClass("sem-borda-verde")) {
            $(".saldo-novo").removeClass("sem-borda-verde");
            $(".saldo-novo").addClass("borda-verde");
        } else {
            $(".saldo-novo").addClass("sem-borda-verde");
            $(".saldo-novo").removeClass("borda-verde");
        }
    })
})


var menu_categorias = new Swiper('.menu-categorias', {
    pagination: '.swiper-pagination',
    paginationClickable: true,
    slidesPerView: 11,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    width: 930,
    simulateTouch: false,
    spaceBetween: 0
});

$(function () {
    $(window).on("scroll", function () {
        if ($(window).scrollTop() > 420) {
            $(".texto-duvidas-frequentes").addClass("fixar-conteudo-duvidas");
        } else {
            $(".texto-duvidas-frequentes").removeClass("fixar-conteudo-duvidas");
        }
    });
});