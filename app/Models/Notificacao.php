<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 24 Jun 2019 19:57:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Notificacao
 * 
 * @property int $cod_notificacao
 * @property int $cod_categoria
 * @property string $titulo
 * @property string $mensagem
 * @property string $origem
 * @property string $link
 * @property \Carbon\Carbon $entrada_notificacao
 * @property \Carbon\Carbon $saida_notificacao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $cod_usuario
 * 
 * @property \App\Models\NotificacaoCategorium $notificacao_categorium
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_mmkts
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_popups
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_pushes
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_sms
 *
 * @package App\Models
 */
class Notificacao extends Eloquent
{
	protected $table = 'notificacao';
	protected $primaryKey = 'cod_notificacao';

	protected $casts = [
		'cod_categoria' => 'int',
		'cod_usuario' => 'int'
	];

	protected $dates = [
		'entrada_notificacao',
		'saida_notificacao'
	];

	protected $fillable = [
		'cod_categoria',
		'titulo',
		'mensagem',
		'origem',
		'link',
		'entrada_notificacao',
		'saida_notificacao',
		'cod_usuario'
	];

	public function notificacao_categorium()
	{
		return $this->belongsTo(\App\Models\NotificacaoCategorium::class, 'cod_categoria');
	}

	public function usuario()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}

}
