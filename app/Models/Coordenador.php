<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Coordenador
 * 
 * @property int $id
 * @property string $nome
 * @property string $ilha
 * @property int $pdv
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Coordenador extends Eloquent
{
	protected $table = 'coordenador';

	protected $casts = [
		'pdv' => 'int'
	];

	protected $fillable = [
		'nome',
		'ilha',
		'pdv'
	];
}
