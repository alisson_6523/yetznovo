<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminPagina
 * 
 * @property int $id
 * @property string $nome
 * @property string $link
 * @property string $icone
 * @property string $grupo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_permissaos
 *
 * @package App\Models
 */
class AdminPagina extends Eloquent
{
	protected $table = 'admin_pagina';

	protected $fillable = [
		'nome',
		'link',
		'icone',
		'grupo'
	];

	public function admin_permissaos()
	{
		return $this->hasMany(\App\Models\AdminPermissao::class, 'paginaId');
	}
}
