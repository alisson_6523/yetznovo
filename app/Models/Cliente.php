<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 27 Jun 2019 18:31:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Cliente
 * 
 * @property int $id
 * @property string $razao_social
 * @property string $nome_fantasia
 * @property string $inscricao_estadual
 * @property string $inscricao_municipal
 * @property string $cnpj
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $telefone
 * @property string $email
 * @property string $site
 * @property string $logo
 * @property string $logo_hash
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admins
 * @property \Illuminate\Database\Eloquent\Collection $campanhas
 * @property \Illuminate\Database\Eloquent\Collection $cliente_pdvs
 * @property \Illuminate\Database\Eloquent\Collection $ilhas
 *
 * @package App\Models
 */
class Cliente extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'cliente';

	protected $fillable = [
		'razao_social',
		'nome_fantasia',
		'inscricao_estadual',
		'inscricao_municipal',
		'cnpj',
		'cep',
		'logradouro',
		'numero',
		'complemento',
		'bairro',
		'cidade',
		'estado',
		'telefone',
		'email',
		'site',
		'logo',
		'logo_hash'
	];

	public function admins()
	{
		return $this->hasMany(\App\Models\Admin::class);
	}

	public function campanhas()
	{
		return $this->hasMany(\App\Models\Campanha::class);
	}

	public function cliente_pdvs()
	{
		return $this->hasMany(\App\Models\ClientePdv::class);
	}

	public function ilhas()
	{
		return $this->hasMany(\App\Models\Ilha::class);
	}

	public function getLogoHashAttribute($value){
	    return 'logo-cliente/' . $value;
    }

    public function getNomeAttribute(){
	    return ($this->nome_fantasia != null ? $this->nome_fantasia : $this->razao_social);
    }
}
