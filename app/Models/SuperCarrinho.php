<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:52:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperCarrinho
 *
 * @property int $cod_carrinho
 * @property int $cod_cartao
 * @property int $status_carrinho
 *
 * @property \App\Models\SuperCartao $super_cartao
 *
 * @package App\Models
 */
class SuperCarrinho extends Eloquent
{
	protected $table = 'super_carrinho';
	protected $primaryKey = 'cod_carrinho';
	public $timestamps = false;

	protected $casts = [
		'cod_cartao' => 'int',
		'status_carrinho' => 'int'
	];

	protected $fillable = [
		'cod_cartao',
		'status_carrinho'
	];

	public function cartao()
	{
		return $this->belongsTo(\App\Models\SuperCartao::class, 'cod_cartao');
	}
}
