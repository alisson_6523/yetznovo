<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 25 Jun 2019 14:17:14 +0000.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Admin
 *
 * @property int $id
 * @property int $cliente_id
 * @property int $cod_campanha
 * @property int $perfil_id
 * @property string $nome
 * @property string $login
 * @property string $email
 * @property string $password
 * @property string $foto
 * @property string $ip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Cliente $cliente
 * @property \App\Models\AdminPerfil $admin_perfil
 *
 * @package App\Models
 */
class Admin extends Authenticatable
{

    use SoftDeletes;

	protected $casts = [
		'cliente_id' => 'int',
		'perfil_id' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'cliente_id',
        'cod_campanha',
		'perfil_id',
        'ip',
		'nome',
		'login',
		'email',
		'password',
		'foto'
	];

	public $timestamps = true;

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class);
	}

	public function campanhas()
    {
	    return $this->hasMany(\App\Models\AdminCampanha::class);
    }

	public function perfil()
	{
		return $this->belongsTo(\App\Models\AdminPerfil::class, 'perfil_id');
	}

	public function protocolos_criados(){
	    return $this->hasMany(CampanhaProtocolo::class, 'cod_adm_criador');
    }

    public function protocolos_efetivados(){
        return $this->hasMany(CampanhaProtocolo::class, 'cod_adm_efetivador');
    }

    public function getFotoUsuarioAttribute(){
        return '/img/foto-usuario-responsivo.png';
	}

	public function getCriacaoAttribute(){
		return $this->created_at->format('d.m.Y');
	}

	public function getStatusAttribute(){
		return (!$this->trashed()) ? 'ativo' : 'inativo';

	}

	public function setPasswordAttribute($p)
	{
		$this->attributes['password'] = sha1($p);
	}
}
