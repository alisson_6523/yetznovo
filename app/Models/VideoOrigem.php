<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:29:18 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class VideoOrigem
 * 
 * @property int $db_controle
 * @property int $cod_video
 * @property int $cod_campanha
 * @property int $cod_pdv
 * @property string $ilha
 * 
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\ClientePdv $cliente_pdv
 * @property \App\Models\Video $video
 *
 * @package App\Models
 */
class VideoOrigem extends Eloquent
{
	protected $table = 'video_origem';
	protected $primaryKey = 'db_controle';
	public $timestamps = false;

	protected $casts = [
		'cod_video' => 'int',
		'cod_campanha' => 'int',
		'cod_pdv' => 'int'
	];

	protected $fillable = [
		'cod_video',
		'cod_campanha',
		'cod_pdv',
		'ilha'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function cliente_pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}

	public function video()
	{
		return $this->belongsTo(\App\Models\Video::class, 'cod_video');
	}
}
