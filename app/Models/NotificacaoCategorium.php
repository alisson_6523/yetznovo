<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:51:26 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoCategorium
 * 
 * @property int $cod_categoria
 * @property string $nome_categoria
 * 
 * @property \Illuminate\Database\Eloquent\Collection $notificacoes
 *
 * @package App\Models
 */
class NotificacaoCategorium extends Eloquent
{
	protected $primaryKey = 'cod_categoria';
	public $timestamps = false;

	protected $fillable = [
		'nome_categoria'
	];

	public function notificacoes()
	{
		return $this->hasMany(\App\Models\Notificacao::class, 'cod_categoria');
	}
}
