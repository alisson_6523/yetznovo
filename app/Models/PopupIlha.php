<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:52:55 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PopupIlha
 * 
 * @property int $db_controle
 * @property int $cod_popup
 * @property int $cod_pdv
 * @property string $ilha
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\ClientePdv $cliente_pdv
 * @property \App\Models\NotificacaoPopup $notificacao_popup
 *
 * @package App\Models
 */
class PopupIlha extends Eloquent
{
	protected $table = 'popup_ilha';
	protected $primaryKey = 'id';

	protected $casts = [
		'cod_popup' => 'int',
		'cod_pdv' => 'int'
	];

	protected $fillable = [
		'cod_popup',
		'cod_pdv',
		'ilha'
	];

	public function pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}

	public function notificacao()
	{
		return $this->belongsTo(\App\Models\NotificacaoPopup::class, 'cod_popup');
	}
}
