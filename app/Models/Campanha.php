<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Jul 2019 21:21:41 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class Campanha
 *
 * @property int $cod_campanha
 * @property int $cliente_id
 * @property string $nome_campanha
 * @property string $chave
 * @property bool $super
 * @property string $logo
 * @property string $logo_hash
 * @property \Carbon\Carbon $dt_inicio
 * @property \Carbon\Carbon $dt_fim
 * @property int $validade_pontos
 * @property string $regulamento
 * @property string $regulamento_hash
 * @property bool $fale_conosco
 * @property bool $pontos_temporarios
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $autenticacao
 * @property \App\Models\Cliente $cliente
 * @property \App\Models\CampanhaCategorium $campanha_categorium
 * @property \App\Models\CampanhaPdv $campanha_pdv
 * @property \Illuminate\Database\Eloquent\Collection $produtos
 * @property \Illuminate\Database\Eloquent\Collection $campanha_protocolos
 * @property \Illuminate\Database\Eloquent\Collection $super_pedidos
 * @property \App\Models\UsuarioCampanha $usuario_campanha
 * @property \Illuminate\Database\Eloquent\Collection $video_origems
 *
 * @package App\Models
 */
class Campanha extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'campanha';
	protected $primaryKey = 'cod_campanha';

	protected $casts = [
		'cliente_id' => 'int',
		'super' => 'bool',
		'validade_pontos' => 'int',
		'fale_conosco' => 'bool',
		'autenticacao' => 'int',
        'valor_pontos' => 'float',
        'pontos_temporarios' => 'bool'
	];

	protected $dates = [
		'dt_inicio',
		'dt_fim'
	];

	protected $fillable = [
		'cliente_id',
		'nome_campanha',
		'chave',
		'super',
		'logo',
		'logo_hash',
		'dt_inicio',
		'dt_fim',
		'validade_pontos',
		'regulamento',
		'regulamento_hash',
        'titulo_regulamento',
		'fale_conosco',
		'autenticacao',
		'valor_pontos',
        'pontos_temporarios'
	];

    public function autenticacao()
    {
        return $this->belongsTo(\App\Models\Autenticacao::class, 'autenticacao');
    }

    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class);
    }

    public function campanha_categorias()
    {
        return $this->hasMany(\App\Models\CampanhaCategorium::class, 'cod_campanha');
    }

    public function campanha_pdvs()
    {
        return $this->belongsToMany(\App\Models\ClientePdv::class, 'campanha_pdv', 'cod_campanha', 'cod_pdv');
    }

    public function produtos()
    {
        return $this->belongsToMany(\App\Models\Produto::class, 'campanha_produto', 'cod_campanha', 'cod_produto')->distinct('cod_produto');
    }

    public function categorias()
    {
        return $this->belongsToMany(\App\Models\CategoriaProduto::class, 'campanha_categoria', 'cod_campanha', 'cod_categoria')
        ->orderBy('categoria')
        ->distinct('categoria');
    }

    public function campanha_protocolos()
    {
        return $this->hasMany(\App\Models\CampanhaProtocolo::class, 'cod_campanha');
    }

    public function campanha_protocolos_temporarios()
    {
        return $this->hasMany(\App\Models\CampanhaProtocoloTemporario::class, 'cod_campanha');
    }

    public function super_pedidos()
    {
        return $this->hasMany(\App\Models\SuperPedido::class, 'cod_campanha');
    }

    public function usuarios_campanha()
    {
        return $this->belongsToMany(\App\Models\User::class, 'usuario_campanha', 'cod_campanha', 'cod_usuario')->withTrashed();
    }

    public function video_origem()
    {
        return $this->hasMany(\App\Models\VideoOrigem::class, 'cod_campanha');
    }

    public function resgates()
    {
        return $this->hasMany(\App\Models\Resgate::class, 'cod_campanha');
    }

    public function banners()
    {
        return $this->hasMany(\App\Models\Banner::class, 'cod_campanha');
    }

    public function regulamentos()
    {
        return $this->hasMany(\App\Models\Regulamento::class, 'cod_campanha');
    }

    public function admins()
    {
        return $this->hasMany(\App\Models\Admin::class);
    }

    public function getRegulamentoHashAttribute($value){
        return 'regulamento-campanha/' . $value;
    }

    public function getLogoHashAttribute($value){
        return 'logo-campanha/' . $value;
    }


}
