<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jul 2019 19:01:35 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CupomPedido
 *
 * @property int $id
 * @property int $admin_id
 * @property int $cliente_id
 * @property int $campanha_id
 * @property string $nome
 * @property int $numero_cartoes
 * @property string $prefixo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Admin $admin
 * @property \App\Models\Cliente $cliente
 * @property \Illuminate\Database\Eloquent\Collection $cupoms
 *
 * @package App\Models
 */
class CupomPedido extends Eloquent {

    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'cupom_pedido';

	protected $casts = [
		'admin_id' => 'int',
		'cliente_id' => 'int',
		'quantidade' => 'int'
	];

	protected $fillable = [
		'admin_id',
		'cliente_id',
        'campanha_id',
		'nome',
		'quantidade',
		'pontos',
		'prefixo'
	];

	public function admin()
	{
		return $this->belongsTo(\App\Models\Admin::class);
	}

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id');
	}

    public function campanha()
    {
        return $this->belongsTo(\App\Models\Campanha::class, 'campanha_id');
    }

	public function cupons()
	{
		return $this->hasMany(\App\Models\Cupom::class, 'pedidoId');
	}

	public function getUsadosAttribute()
	{
		return $this->cupons()->where('usado_em', '!=', null)->count();
	}

	public function getSolicitacaoAttribute()
	{
		return $this->created_at->format('d.m.Y');
	}
}
