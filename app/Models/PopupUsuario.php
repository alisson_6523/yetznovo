<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Jun 2019 17:11:47 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PopupUsuario
 * 
 * @property int $cod_usuario
 * @property int $cod_popup
 * @property bool $visualizado
 * @property int $id
 * 
 * @property \App\Models\NotificacaoPopup $notificacao_popup
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class PopupUsuario extends Eloquent
{
	protected $table = 'popup_usuario';
	public $timestamps = false;

	protected $casts = [
		'cod_usuario' => 'int',
		'cod_popup' => 'int',
		'visualizado' => 'bool'
	];

	protected $fillable = [
		'cod_usuario',
		'cod_popup',
		'visualizado'
	];

	public function notificacao_popup()
	{
		return $this->belongsTo(\App\Models\NotificacaoPopup::class, 'cod_popup');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}
}
