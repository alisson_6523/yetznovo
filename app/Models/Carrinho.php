<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:51:15 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Carrinho
 * 
 * @property int $cod_carrinho
 * @property int $cod_usuario
 * @property int $status_carrinho
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $item_carrinhos
 * @property \Illuminate\Database\Eloquent\Collection $resgates
 * @property \Illuminate\Database\Eloquent\Collection $super_item_carrinhos
 * @property \Illuminate\Database\Eloquent\Collection $super_resgates
 *
 * @package App\Models
 */
class Carrinho extends Eloquent
{
	protected $table = 'carrinho';
	protected $primaryKey = 'cod_carrinho';

    protected $casts = [
        'cod_usuario' => 'int',
        'status_carrinho' => 'bool'
    ];

	protected $fillable = [
		'cod_usuario',
		'status_carrinho'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}

	public function itens()
	{
		return $this->hasMany(\App\Models\ItemCarrinho::class, 'cod_carrinho');
	}

	public function resgates()
	{
		return $this->hasMany(\App\Models\Resgate::class, 'cod_carrinho');
	}

	public function super_item_carrinhos()
	{
		return $this->hasMany(\App\Models\SuperItemCarrinho::class, 'cod_carrinho');
	}

	public function super_resgates()
	{
		return $this->hasMany(\App\Models\SuperResgate::class, 'cod_carrinho');
	}

    public function produtos()
    {
        return $this->belongsToMany(\App\Models\Produto::class, 'item_carrinho', 'cod_carrinho', 'cod_produto')->withPivot(['cod_status', 'cod_item']);
    }
	

	public function getValorFinalAttribute(){
		$soma = $this->produtos->sum('valor_produto');
		return number_format($soma, 0, '', '.');
	}

	public function getDescStatusAttribute(){
        if($this->status_carrinho)
            return "Aberto";
        return "Fechado";
    }


}
