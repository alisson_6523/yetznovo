<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 01 Aug 2019 14:03:50 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ClienteArquivo
 * 
 * @property int $id
 * @property int $cliente_id
 * @property string $titulo
 * @property string $arquivo
 * @property string $hash_arquivo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\ClientePdv $cliente_pdv
 * @property \Illuminate\Database\Eloquent\Collection $historico_cliente_arquivos
 *
 * @package App\Models
 */
class ClienteArquivo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $casts = [
		'cliente_id' => 'int'
	];

	protected $fillable = [
		'cliente_id',
		'titulo',
		'arquivo',
		'hash_arquivo'
	];

	public function cliente_pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cliente_id', 'cliente_id');
	}

	public function historico()
	{
		return $this->hasMany(\App\Models\HistoricoClienteArquivo::class, 'arquivo_id');
	}
}
