<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:58:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusResgate
 * 
 * @property int $cod_status
 * @property string $descricao_status
 *
 * @package App\Models
 */
class StatusResgate extends Eloquent
{

	const AGUARDANDO = 1;
	const APROVADO = 2;
	const ACAMINHO = 3;
	const ENTREGUE = 4;
	const CANCELADO = 5;


	const lista = [
					'1' => 'Aguardando',
					'2' => 'Aprovado',
					'3' => 'A Caminho',
					'4' => 'Entregue',
					'5' => 'Cancelado',
					];

	protected $table = 'status_resgate';
	protected $primaryKey = 'cod_status';
	public $timestamps = false;

	protected $fillable = [
		'descricao_status'
	];
}
