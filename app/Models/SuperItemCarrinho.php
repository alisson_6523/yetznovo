<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 18:11:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperItemCarrinho
 * 
 * @property int $cod_item
 * @property int $cod_produto
 * @property int $cod_carrinho
 * @property int $quantidade
 * @property \Carbon\Carbon $modificado
 * 
 * @property \App\Models\Carrinho $carrinho
 * @property \App\Models\Produto $produto
 *
 * @package App\Models
 */
class SuperItemCarrinho extends Eloquent
{
	protected $table = 'super_item_carrinho';
	protected $primaryKey = 'cod_item';
	public $timestamps = false;

	protected $casts = [
		'cod_produto' => 'int',
		'cod_carrinho' => 'int',
		'quantidade' => 'int'
	];

	protected $dates = [
		'modificado'
	];

	protected $fillable = [
		'cod_produto',
		'cod_carrinho',
		'quantidade',
		'modificado'
	];

	public function carrinho()
	{
		return $this->belongsTo(\App\Models\Carrinho::class, 'cod_carrinho');
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}
}
