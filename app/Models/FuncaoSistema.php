<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FuncaoSistema
 * 
 * @property int $id
 * @property string $nome_funcao
 * @property string $link
 *
 * @package App\Models
 */
class FuncaoSistema extends Eloquent
{
	protected $table = 'funcao_sistema';
	public $timestamps = false;

	protected $fillable = [
		'nome_funcao',
		'link'
	];
}
