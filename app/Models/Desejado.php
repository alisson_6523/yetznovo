<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:46:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Desejado
 * 
 * @property int $cod_usuario
 * @property int $cod_produto
 * 
 * @property \App\Models\Produto $produto
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Desejado extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $fillable = [
		'cod_usuario',
		'cod_produto'
	];

	protected $casts = [
		'cod_usuario' => 'int',
		'cod_produto' => 'int'
	];

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}
}
