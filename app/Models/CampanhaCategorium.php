<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CampanhaCategorium
 * 
 * @property int $cod_campanha
 * @property int $cod_categoria
 * 
 * @property \App\Models\Campanha $campanha
 *
 * @package App\Models
 */
class CampanhaCategorium extends Eloquent
{

	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'cod_campanha' => 'int',
		'cod_categoria' => 'int'
	];

	protected $fillable = [
		'cod_campanha',
		'cod_categoria',
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}
	public function categoria()
	{
		return $this->belongsTo(\App\Models\CategoriaProduto::class, 'cod_categoria');
	}
}
