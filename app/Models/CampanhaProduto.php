<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CampanhaProduto
 *
 * @property int $cod_campanha
 * @property int $cod_produto
 *
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\Produto $produto
 *
 * @package App\Models
 */
class CampanhaProduto extends Eloquent
{
	protected $table = 'campanha_produto';
    protected $primaryKey = null;
    public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
        'cod_campanha' => 'int',
		'cod_produto' => 'int'
	];

	protected $fillable = [
		'cod_campanha',
		'cod_produto'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}
}
