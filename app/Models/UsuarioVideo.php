<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 23 Sep 2019 17:44:43 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsuarioVideo
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $video_id
 * @property \Carbon\Carbon $read_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\User $user
 * @property \App\Models\Video $video
 *
 * @package App\Models
 */
class UsuarioVideo extends Eloquent
{
	protected $casts = [
		'usuario_id' => 'int',
		'video_id' => 'int'
	];

	protected $dates = [
		'read_at'
	];

	protected $fillable = [
		'usuario_id',
		'video_id',
		'read_at'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'usuario_id');
	}

	public function video()
	{
		return $this->belongsTo(\App\Models\Video::class, 'video_id');
	}
}
