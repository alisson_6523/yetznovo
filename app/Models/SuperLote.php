<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:35:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperLote
 * 
 * @property int $cod_lote
 * @property int $status_lote
 * @property int $cod_adm_criador
 * @property string $nome_lote
 * @property string $numero_lote
 * @property \Carbon\Carbon $inicio_lote
 * @property \Carbon\Carbon $encerramento_lote
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $super_cartaos
 *
 * @package App\Models
 */
class SuperLote extends Eloquent
{
	protected $table = 'super_lote';
	protected $primaryKey = 'cod_lote';

	protected $casts = [
		'status_lote' => 'int',
		'cod_adm_criador' => 'int'
	];

	protected $dates = [
		'inicio_lote',
		'encerramento_lote'
	];

	protected $fillable = [
		'status_lote',
		'cod_adm_criador',
		'nome_lote',
		'numero_lote',
		'inicio_lote',
		'encerramento_lote'
	];

	public function super_cartoes()
	{
		return $this->hasMany(\App\Models\SuperCartao::class, 'cod_lote');
	}
}
