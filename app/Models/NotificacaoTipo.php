<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:54:21 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoTipo
 * 
 * @property int $cod_tipo
 * @property string $descricao
 * 
 * @property \Illuminate\Database\Eloquent\Collection $notificacaos
 *
 * @package App\Models
 */
class NotificacaoTipo extends Eloquent
{
	protected $table = 'notificacao_tipo';
	protected $primaryKey = 'cod_tipo';
	public $timestamps = false;

	protected $fillable = [
		'descricao'
	];

	public function notificacoes()
	{
		return $this->hasMany(\App\Models\Notificacao::class, 'tipo_notificacao');
	}
}
