<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 03 Dec 2019 14:35:49 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminCampanha
 * 
 * @property int $id
 * @property int $admin_id
 * @property int $campanha_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Admin $admin
 * @property \App\Models\Campanha $campanha
 *
 * @package App\Models
 */
class AdminCampanha extends Eloquent
{
	protected $casts = [
		'admin_id' => 'int',
		'campanha_id' => 'int'
	];

	protected $fillable = [
		'admin_id',
		'campanha_id'
	];

	public function admin()
	{
		return $this->belongsTo(\App\Models\Admin::class);
	}

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class);
	}
}
