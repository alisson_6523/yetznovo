<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:49:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperPedido
 * 
 * @property int $cod_pedido
 * @property int $cod_campanha
 * @property int $cod_usuario_atendente
 * @property int $status_pedido
 * @property int $cod_area
 * @property int $quantidade_cartoes
 * @property string $nome_pedido
 * @property \Carbon\Carbon $data_ativacao
 * @property \Carbon\Carbon $validade
 * @property string $registro_lote
 * @property float $valor_total
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Campanha $campanha
 * @property \Illuminate\Database\Eloquent\Collection $super_cartaos
 * @property \Illuminate\Database\Eloquent\Collection $super_pedido_cartaos
 *
 * @package App\Models
 */
class SuperPedido extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'super_pedido';
	protected $primaryKey = 'cod_pedido';

	protected $casts = [
		'cod_campanha' => 'int',
		'cod_usuario_atendente' => 'int',
		'status_pedido' => 'int',
		'cod_area' => 'int',
		'quantidade_cartoes' => 'int',
		'valor_total' => 'float'
	];

	protected $dates = [
		'data_ativacao',
		'validade'
	];

	protected $fillable = [
		'cod_campanha',
		'cod_usuario_atendente',
		'status_pedido',
		'cod_area',
		'quantidade_cartoes',
		'nome_pedido',
		'data_ativacao',
		'validade',
		'registro_lote',
		'valor_total'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function super_cartoes()
	{
		return $this->hasMany(\App\Models\SuperCartao::class, 'cod_pedido');
	}

	public function super_pedido_cartoes()
	{
		return $this->hasMany(\App\Models\SuperPedidoCartao::class, 'cod_pedido');
	}
}
