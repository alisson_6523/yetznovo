<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 24 Jun 2019 13:19:46 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HistoricoMovimentacao
 *
 * @property int $cod_movimentacao
 * @property int $cod_usuario
 * @property int $cod_tipo_movimentacao
 * @property int $valor
 * @property string $descricao
 * @property string $protocolo
 * @property \Carbon\Carbon $expiracao
 * @property string $referencia
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\TipoMovimentacao $tipo_movimentacao
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class HistoricoMovimentacao extends Eloquent
{
	protected $table = 'historico_movimentacao';
	protected $primaryKey = 'cod_movimentacao';

	protected $casts = [
		'cod_usuario' => 'int',
		'cod_tipo_movimentacao' => 'int',
		'valor' => 'int',
        'cod_campanha' => 'int'
	];

	protected $dates = [
		'expiracao'
	];

	protected $fillable = [
		'cod_usuario',
		'cod_tipo_movimentacao',
		'valor',
		'descricao',
		'protocolo',
		'expiracao',
		'referencia',
        'cod_campanha'
	];

	public function tipo_movimentacao()
	{
		return $this->belongsTo(\App\Models\TipoMovimentacao::class, 'cod_tipo_movimentacao');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}

	public function getValorAttribute()
	{
		return number_format($this->attributes['valor'], 0, ',', '.');
	}

	public function premio()
	{
		return $this->hasOne(\App\Models\Resgate::class, 'registro', 'protocolo');
	}

}
