<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 18:12:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperPedidoCartao
 * 
 * @property int $id
 * @property int $cod_pedido
 * @property int $cod_cartao
 * 
 * @property \App\Models\SuperCartao $super_cartao
 * @property \App\Models\SuperPedido $super_pedido
 *
 * @package App\Models
 */
class SuperPedidoCartao extends Eloquent
{
	protected $table = 'super_pedido_cartao';
	public $timestamps = false;

	protected $casts = [
		'cod_pedido' => 'int',
		'cod_cartao' => 'int'
	];

	protected $fillable = [
		'cod_pedido',
		'cod_cartao'
	];

	public function cartao()
	{
		return $this->belongsTo(\App\Models\SuperCartao::class, 'cod_cartao');
	}

	public function pedido()
	{
		return $this->belongsTo(\App\Models\SuperPedido::class, 'cod_pedido');
	}
}
