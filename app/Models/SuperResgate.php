<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 17 Jun 2019 14:33:43 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperResgate
 * 
 * @property int $cod_resgate
 * @property int $cod_carrinho
 * @property int $cod_endereco
 * @property int $cod_status
 * @property string $nome_destinatario
 * @property string $email_destinatario
 * @property string $celular_destinatario
 * @property string $cpf_destinatario
 * @property string $registro
 * @property float $valor_resgate
 * @property string $metadado_resgate
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Carrinho $carrinho
 * @property \App\Models\Endereco $endereco
 * @property \App\Models\StatusResgate $status_resgate
 *
 * @package App\Models
 */
class SuperResgate extends Eloquent
{
	protected $table = 'super_resgate';
	protected $primaryKey = 'cod_resgate';

	protected $casts = [
		'cod_carrinho' => 'int',
		'cod_endereco' => 'int',
		'cod_status' => 'int',
		'valor_resgate' => 'float'
	];

	protected $fillable = [
		'cod_carrinho',
		'cod_endereco',
		'cod_status',
		'nome_destinatario',
		'email_destinatario',
		'celular_destinatario',
		'cpf_destinatario',
		'registro',
		'valor_resgate',
		'metadado_resgate'
	];

	public function carrinho()
	{
		return $this->belongsTo(\App\Models\Carrinho::class, 'cod_carrinho');
	}

	public function endereco()
	{
		return $this->belongsTo(\App\Models\Endereco::class, 'cod_endereco');
	}

	public function status_carrinho()
	{
		return $this->belongsTo(\App\Models\StatusResgate::class, 'cod_status');
	}
}
