<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 18:13:34 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Ilha
 * 
 * @property int $id
 * @property int $cliente_id
 * @property string $ilha
 * @property string $grupo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Cliente $cliente
 *
 * @package App\Models
 */
class Ilha extends Eloquent
{
	protected $table = 'ilha';

	protected $casts = [
		'cliente_id' => 'int'
	];

	protected $fillable = [
		'cliente_id',
		'ilha',
		'grupo'
	];

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class);
	}
}
