<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:53:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperDesejado
 * 
 * @property int $cod_produto
 * @property int $cod_cartao
 * 
 * @property \App\Models\SuperCartao $super_cartao
 * @property \App\Models\Produto $produto
 *
 * @package App\Models
 */
class SuperDesejado extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'cod_produto' => 'int',
		'cod_cartao' => 'int'
	];

	public function super_cartao()
	{
		return $this->belongsTo(\App\Models\SuperCartao::class, 'cod_cartao');
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}
}
