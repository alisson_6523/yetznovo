<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 01 Jul 2019 18:36:10 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use function GuzzleHttp\json_encode;
use function GuzzleHttp\json_decode;

/**
 * Class NotificacaoGrupo
 * 
 * @property int $cod_grupo
 * @property int $cod_categoria
 * @property string $titulo
 * @property string $mensagem
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\NotificacaoCategorium $notificacao_categorium
 *
 * @package App\Models
 */
class NotificacaoGrupo extends Eloquent
{
	protected $table = 'notificacao_grupo';
	protected $primaryKey = 'cod_grupo';

	protected $casts = [
		'cod_categoria' => 'int'
	];

	protected $fillable = [
		'cod_categoria',
		'cod_campanha',
		'adminId',
		'titulo',
		'pdvs',
		'enviados',
		'mensagem'
	];

	public function setPdvsAttribute($pdvs)
	{
		$this->attributes['pdvs'] = json_encode($pdvs);
	}

	public function getPdvsAttribute()
	{
		return json_decode($this->attributes['pdvs']);
	}

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
		
	}
	public function notificacao_categorium()
	{
		return $this->belongsTo(\App\Models\NotificacaoCategorium::class, 'cod_categoria');
		
	}

	public function getVisualizadosAttribute()
	{
		return Notification::where([['data->grupo', $this->attributes['cod_grupo']], ['read_at', '<>', null]])->get();

	}

	public function getDataEnvioAttribute()
	{
		return $this->created_at->format('d.m.Y');
	}

	public function getClass()
	{
		switch($this->cod_categoria)
		{
			case 1: 
				$class = 'pontos_creditados';
				break;
			case 2: 
				$class = 'fique-atento';
				break;
			case 3: 
				$class = 'novo-cartao';
				break;
			case 4: 
				$class = 'dicas';
				break;
			default:
				$class = '';
				break;
		}

		return $class;
	}
}
