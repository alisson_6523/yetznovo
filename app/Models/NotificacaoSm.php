<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:48:52 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoSm
 * 
 * @property int $cod_sms
 * @property int $cod_notificacao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Notificacao $notificacao
 *
 * @package App\Models
 */
class NotificacaoSm extends Eloquent
{
	protected $primaryKey = 'cod_sms';

	protected $casts = [
		'cod_notificacao' => 'int'
	];

	protected $fillable = [
		'cod_notificacao'
	];

	public function notificacao()
	{
		return $this->belongsTo(\App\Models\Notificacao::class, 'cod_notificacao');
	}
}
