<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 24 Jul 2019 19:01:31 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
/**
 * Class Cupom
 *
 * @property int $id
 * @property int $pedidoId
 * @property int $userId
 * @property string $codigo
 * @property int $pontos
 * @property \Carbon\Carbon $usado_em
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\CupomPedido $cupom_pedido
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Cupom extends Eloquent
{

	use SoftDeletes;

	protected $table = 'cupom';

	protected $casts = [
		'pedidoId' => 'int',
		'userId' => 'int',
		'pontos' => 'int'
	];

	protected $dates = [
		'usado_em',
		'deleted_at'
	];

	protected $fillable = [
		'pedidoId',
		'userId',
		'codigo',
		'pontos',
		'usado_em'
	];

	public function pedido()
	{
		return $this->belongsTo(\App\Models\CupomPedido::class, 'pedidoId');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'userId');
	}

	public function getUsadoAttribute()
	{
		if($this->usado_em == null):
			return null;
		endif;

		return $this->usado_em->format('d.m.Y - H:i:s');
	}

	public function getSequencialAttribute(){
	    $letras     = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];
	    $indice     = intval($this->id/10000);
	    $sequencial = $letras[$indice] . '-' . str_pad(substr($this->id, -4), 4, "0", STR_PAD_LEFT); // Ex: A-9999, B-0000

	    return $sequencial;
    }
}
