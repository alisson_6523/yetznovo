<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:44:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Estado
 * 
 * @property string $sigla
 * @property string $nome_estado
 * @property int $cod_ibge
 * 
 * @property \Illuminate\Database\Eloquent\Collection $cliente_pdvs
 * @property \Illuminate\Database\Eloquent\Collection $enderecos
 *
 * @package App\Models
 */
class Estado extends Eloquent
{
	protected $table = 'estado';
	protected $primaryKey = 'sigla';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'cod_ibge' => 'int'
	];

	protected $fillable = [
		'nome_estado',
		'cod_ibge'
	];

	public function cliente_pdvs()
	{
		return $this->hasMany(\App\Models\ClientePdv::class, 'estado');
	}

	public function enderecos()
	{
		return $this->hasMany(\App\Models\Endereco::class, 'sigla_estado');
	}
}
