<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminPerfil
 * 
 * @property int $id
 * @property string $nome
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admin_permissaos
 *
 * @package App\Models
 */
class AdminPerfil extends Eloquent
{
	protected $table = 'admin_perfil';

	protected $fillable = [
		'nome'
	];

	public function permissoes()
	{
		return $this->hasMany(\App\Models\AdminPermissao::class, 'perfilId');
	}
}
