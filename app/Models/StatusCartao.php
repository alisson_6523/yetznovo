<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:56:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusCartao
 * 
 * @property int $cod_status
 * @property string $descricao_status
 *
 * @package App\Models
 */
class StatusCartao extends Eloquent
{
	protected $table = 'status_cartao';
	protected $primaryKey = 'cod_status';
	public $timestamps = false;

	protected $fillable = [
		'descricao_status'
	];
}
