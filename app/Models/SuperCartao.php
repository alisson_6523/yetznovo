<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:50:23 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperCartao
 * 
 * @property int $cod_cartao
 * @property int $valor_inicial
 * @property int $saldo_cartao
 * @property int $cod_lote
 * @property int $cod_pedido
 * @property int $status_cartao
 * @property string $codigo_controle
 * @property string $numero_cartao
 * @property string $pin
 * @property string $checksum_cartao
 * @property \Carbon\Carbon $criacao_cartao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\SuperLote $super_lote
 * @property \App\Models\SuperPedido $super_pedido
 * @property \Illuminate\Database\Eloquent\Collection $super_carrinhos
 * @property \Illuminate\Database\Eloquent\Collection $super_desejados
 * @property \Illuminate\Database\Eloquent\Collection $super_movimentacaos
 * @property \Illuminate\Database\Eloquent\Collection $super_pedido_cartaos
 *
 * @package App\Models
 */
class SuperCartao extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'super_cartao';
	protected $primaryKey = 'cod_cartao';

	protected $casts = [
		'valor_inicial' => 'int',
		'saldo_cartao' => 'int',
		'cod_lote' => 'int',
		'cod_pedido' => 'int',
		'status_cartao' => 'int'
	];

	protected $dates = [
		'criacao_cartao'
	];

	protected $fillable = [
		'valor_inicial',
		'saldo_cartao',
		'cod_lote',
		'cod_pedido',
		'status_cartao',
		'codigo_controle',
		'numero_cartao',
		'pin',
		'checksum_cartao',
		'criacao_cartao'
	];

	public function super_lote()
	{
		return $this->belongsTo(\App\Models\SuperLote::class, 'cod_lote');
	}

	public function super_pedido()
	{
		return $this->belongsTo(\App\Models\SuperPedido::class, 'cod_pedido');
	}

	public function super_carrinhos()
	{
		return $this->hasMany(\App\Models\SuperCarrinho::class, 'cod_cartao');
	}

	public function super_desejados()
	{
		return $this->hasMany(\App\Models\SuperDesejado::class, 'cod_cartao');
	}

	public function super_movimentacoes()
	{
		return $this->hasMany(\App\Models\SuperMovimentacao::class, 'cod_cartao');
	}

	public function super_pedido_cartoes()
	{
		return $this->hasMany(\App\Models\SuperPedidoCartao::class, 'cod_cartao');
	}
}
