<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 27 Jun 2019 19:05:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ClientePdv
 * 
 * @property int $cod_pdv
 * @property int $cliente_id
 * @property string $registro
 * @property string $nome_pdv
 * @property string $razao_social
 * @property string $inscricao_estadual
 * @property string $inscricao_municipal
 * @property string $cnpj
 * @property string $cep
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cidade
 * @property string $estado
 * @property string $telefone
 * @property string $email
 * @property string $site
 * @property string $logo
 * @property string $logo_hash
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Cliente $cliente
 * @property \App\Models\CampanhaPdv $campanha_pdv
 * @property \Illuminate\Database\Eloquent\Collection $popup_ilhas
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @property \Illuminate\Database\Eloquent\Collection $video_origems
 *
 * @package App\Models
 */
class ClientePdv extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'cliente_pdv';
	protected $primaryKey = 'cod_pdv';

	protected $casts = [
		'cliente_id' => 'int'
	];

	protected $fillable = [
		'cliente_id',
		'registro',
		'nome_pdv',
		'razao_social',
		'inscricao_estadual',
		'inscricao_municipal',
		'cnpj',
		'cep',
		'logradouro',
		'numero',
		'complemento',
		'bairro',
		'cidade',
		'estado',
		'telefone',
		'email',
		'site',
		'logo',
		'logo_hash'
	];

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class);
	}

	public function estado()
	{
		return $this->belongsTo(\App\Models\Estado::class, 'estado');
	}

	public function campanhas()
	{
        return $this->belongsToMany(\App\Models\Campanha::class, 'campanha_pdv', 'cod_pdv', 'cod_campanha');
	}

	public function popup_ilhas()
	{
		return $this->hasMany(\App\Models\PopupIlha::class, 'cod_pdv');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'cod_pdv');
	}

	public function videos()
	{
		return $this->hasMany(\App\Models\VideoOrigem::class, 'cod_pdv');
	}

	public function getIlhasAttribute(){

		$ilhas = $this->users->groupBy('ilha')->keys();
		return $ilhas;
	}

    public function getLogoHashAttribute($value){
        return 'logo-pdv/' . $value;
	}
	
	public function getEnderecoAttribute(){

		return "$this->logradouro, $this->numero - $this->complemento - $this->bairro";
	}
}
