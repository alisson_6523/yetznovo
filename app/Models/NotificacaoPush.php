<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:49:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoPush
 * 
 * @property int $cod_push
 * @property int $cod_notificacao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Notificacao $notificacao
 *
 * @package App\Models
 */
class NotificacaoPush extends Eloquent
{
	protected $table = 'notificacao_push';
	protected $primaryKey = 'cod_push';

	protected $casts = [
		'cod_notificacao' => 'int'
	];

	protected $fillable = [
		'cod_notificacao'
	];

	public function notificacao()
	{
		return $this->belongsTo(\App\Models\Notificacao::class, 'cod_notificacao');
	}
}
