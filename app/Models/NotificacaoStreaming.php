<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:45:30 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoStreaming
 * 
 * @property int $notificacao_id
 * @property int $cod_streaming
 * @property int $cod_usuario
 * @property int $visualizado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class NotificacaoStreaming extends Eloquent
{
	protected $table = 'notificacao_streaming';
	protected $primaryKey = 'notificacao_id';

	protected $casts = [
		'cod_streaming' => 'int',
		'cod_usuario' => 'int',
		'visualizado' => 'int'
	];

	protected $fillable = [
		'cod_streaming',
		'cod_usuario',
		'visualizado'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}
}
