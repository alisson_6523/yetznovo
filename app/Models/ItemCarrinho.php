<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 03 Jan 2020 11:14:26 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ItemCarrinho
 * 
 * @property int $cod_item
 * @property int $cod_produto
 * @property int $cod_carrinho
 * @property int $cod_status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $data_entregue
 * @property string $link_voucher
 * @property \Carbon\Carbon $email_enviado
 * 
 * @property \App\Models\Carrinho $carrinho
 * @property \App\Models\Produto $produto
 * @property \App\Models\StatusResgate $status_resgate
 *
 * @package App\Models
 */
class ItemCarrinho extends Eloquent
{
	protected $table = 'item_carrinho';
	protected $primaryKey = 'cod_item';

	protected $casts = [
		'cod_produto' => 'int',
		'cod_carrinho' => 'int',
		'cod_status' => 'int'
	];

	protected $dates = [
		'data_entregue',
		'email_enviado'
	];

	protected $fillable = [
		'cod_produto',
		'cod_carrinho',
		'cod_status',
		'data_entregue',
		'link_voucher',
		'email_enviado'
	];

	public function carrinho()
	{
		return $this->belongsTo(\App\Models\Carrinho::class, 'cod_carrinho');
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}

	public function status_resgate()
	{
		return $this->belongsTo(\App\Models\StatusResgate::class, 'cod_status');
	}
}
