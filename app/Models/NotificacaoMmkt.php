<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:49:24 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoMmkt
 * 
 * @property int $cod_mmkt
 * @property int $cod_notificacao
 * @property string $blade
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Notificacao $notificacao
 *
 * @package App\Models
 */
class NotificacaoMmkt extends Eloquent
{
	protected $table = 'notificacao_mmkt';
	protected $primaryKey = 'cod_mmkt';

	protected $casts = [
		'cod_notificacao' => 'int'
	];

	protected $fillable = [
		'cod_notificacao',
		'blade',
		'params'
	];

	public function notificacao()
	{
		return $this->belongsTo(\App\Models\Notificacao::class, 'cod_notificacao');
	}
}
