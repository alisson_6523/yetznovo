<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:59:31 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TipoMovimentacao
 * 
 * @property int $cod_tipo
 * @property string $descricao_tipo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $campanha_protocolos
 * @property \Illuminate\Database\Eloquent\Collection $historico_movimentacaos
 *
 * @package App\Models
 */
class TipoMovimentacao extends Eloquent
{
	protected $table = 'tipo_movimentacao';
	protected $primaryKey = 'cod_tipo';
	public $timestamps = false;


	const RESGATE = 1;
	const CREDITO = 2;
	const ESTORNO = 3;
	const EXPIRACAO = 4;
	
	protected $fillable = [
		'descricao_tipo'
	];

	public function campanha_protocolos()
	{
		return $this->hasMany(\App\Models\CampanhaProtocolo::class, 'tipo_protocolo');
	}

	public function historico_movimentacaos()
	{
		return $this->hasMany(\App\Models\HistoricoMovimentacao::class, 'cod_tipo_movimentacao');
	}
}
