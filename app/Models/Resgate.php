<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 17 Jun 2019 14:33:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Resgate
 *
 * @property int $cod_resgate
 * @property int $cod_usuario
 * @property int $cod_carrinho
 * @property int $cod_entrega
 * @property int $cod_status
 * @property string $registro
 * @property float $valor_resgate
 * @property float $valor_pontos
 * @property \Carbon\Carbon $data_entregue
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Carrinho $carrinho
 * @property \App\Models\StatusResgate $status_resgate
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class Resgate extends Eloquent
{
	protected $table = 'resgate';
	protected $primaryKey = 'cod_resgate';

	protected $casts = [
		'cod_usuario' => 'int',
		'cod_carrinho' => 'int',
		'cod_entrega' => 'int',
		'cod_status' => 'int',
		'valor_resgate' => 'float',
        'valor_pontos' => 'float'
	];

	protected $dates = [
		'data_entregue'
	];

	protected $fillable = [
		'cod_usuario',
		'cod_carrinho',
		'cod_entrega',
		'cod_campanha',
		'cod_status',
		'registro',
		'valor_resgate',
		'data_entregue',
        'valor_pontos'
	];

	protected $appends = ['mes'];

	public function carrinho()
	{
		return $this->belongsTo(\App\Models\Carrinho::class, 'cod_carrinho');
	}

	public function status_carrinho()
	{
		return $this->belongsTo(\App\Models\StatusResgate::class, 'cod_status');
	}

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha')->withTrashed();
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario')->withTrashed();
	}

	public function getValorFAttribute(){

		return number_format($this->attributes['valor_resgate'], 0, ',', '.');
	}

	public function getMesAttribute(){
		return $this->created_at->format('M/Y');
	}

	public function getDataResgateAttribute(){
		return $this->created_at->format('d/M/Y . H:i:s');
	}

	public function getPrevisaoEntregaAttribute(){
		return $this->created_at->addDays(35)->format('d/M/Y');
	}




}
