<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 26 Jul 2019 14:44:20 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Vinculo
 * 
 * @property int $id
 * @property int $cod_video
 * @property int $cod_pdv
 * @property string $ilha
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\ClientePdv $cliente_pdv
 * @property \App\Models\Video $video
 *
 * @package App\Models
 */
class Vinculo extends Eloquent
{
	protected $table = 'vinculo';

	protected $casts = [
		'cod_video' => 'int',
		'cod_pdv' => 'int'
	];

	protected $fillable = [
		'cod_video',
		'cod_pdv',
		'ilha'
	];

	public function pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}

	public function video()
	{
		return $this->belongsTo(\App\Models\Video::class, 'cod_video');
	}
}
