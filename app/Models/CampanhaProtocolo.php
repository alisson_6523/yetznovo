<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Jul 2019 20:16:09 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CampanhaProtocolo
 * 
 * @property int $cod_protocolo
 * @property int $cod_campanha
 * @property int $cod_adm_criador
 * @property int $cod_adm_efetivador
 * @property int $tipo_protocolo
 * @property string $protocolo
 * @property string $titulo
 * @property int $total_pontos
 * @property string $arquivo
 * @property string $hash_arquivo
 * @property string $dados
 * @property \Carbon\Carbon $data_efetivacao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\TipoMovimentacao $tipo_movimentacao
 *
 * @package App\Models
 */
class CampanhaProtocolo extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'campanha_protocolo';
	protected $primaryKey = 'cod_protocolo';

	protected $casts = [
		'cod_campanha' => 'int',
		'cod_adm_criador' => 'int',
		'cod_adm_efetivador' => 'int',
		'tipo_protocolo' => 'int',
		'total_pontos' => 'int'
	];

	protected $dates = [
		'data_efetivacao'
	];

	protected $fillable = [
		'cod_campanha',
		'cod_adm_criador',
		'cod_adm_efetivador',
		'tipo_protocolo',
		'protocolo',
        'titulo',
		'total_pontos',
		'arquivo',
		'hash_arquivo',
		'dados',
		'data_efetivacao'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function tipo_movimentacao()
	{
		return $this->belongsTo(\App\Models\TipoMovimentacao::class, 'tipo_protocolo');
	}

	public function adm_criador(){
	    return $this->belongsTo(Admin::class, 'cod_adm_criador');
    }

    public function adm_efetivador(){
	    return $this->belongsTo(Admin::class, 'cod_adm_efetivador');
    }
}
