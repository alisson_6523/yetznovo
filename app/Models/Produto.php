<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:31:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;

/**
 * Class Produto
 *
 * @property int $cod_produto
 * @property int $tipo
 * @property int $cod_categoria_produto
 * @property int $cod_bandeira
 * @property int $disponibilidade
 * @property int $valor_produto
 * @property string $nome_produto
 * @property string $descricao_produto
 * @property string $detalhes_produto
 * @property string $informacoes_importantes
 * @property float $valor_reais
 * @property string $fotos
 * @property string $foto_exemplo
 * @property string $video
 * @property string $validade_produto
 * @property bool $selo
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\CategoriaProduto $categoria_produto
 * @property \App\Models\ProdutoTipo $produto_tipo
 * @property \Illuminate\Database\Eloquent\Collection $campanhas
 * @property \Illuminate\Database\Eloquent\Collection $categoria_exibirs
 * @property \Illuminate\Database\Eloquent\Collection $desejados
 * @property \Illuminate\Database\Eloquent\Collection $item_carrinhos
 * @property \Illuminate\Database\Eloquent\Collection $super_desejados
 * @property \Illuminate\Database\Eloquent\Collection $super_item_carrinhos
 *
 * @package App\Models
 */
class Produto extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'produto';
	protected $primaryKey = 'cod_produto';

	protected $casts = [
		'tipo' => 'int',
		'cod_categoria_produto' => 'int',
		'cod_bandeira' => 'int',
		'disponibilidade' => 'int',
		'valor_produto' => 'int',
		'valor_reais' => 'float',
		'selo' => 'bool'
	];

	protected $fillable = [
		'tipo',
		'cod_categoria_produto',
		'cod_bandeira',
		'disponibilidade',
		'valor_produto',
		'nome_produto',
		'descricao_produto',
		'detalhes_produto',
		'informacoes_importantes',
		'valor_reais',
		'fotos',
		'foto_exemplo',
		'video',
		'validade_produto',
		'selo'
	];

	public function categoria()
	{
		return $this->belongsTo(\App\Models\CategoriaProduto::class, 'cod_categoria_produto');
	}

	public function produto_tipo()
	{
		return $this->belongsTo(\App\Models\ProdutoTipo::class, 'tipo');
	}

	public function campanhas()
	{
		return $this->belongsToMany(\App\Models\Campanha::class, 'campanha_produto', 'cod_produto', 'cod_campanha');
	}

	public function categorias()
	{
		return $this->belongsToMany(\App\Models\CategoriaProduto::class, 'categoria_exibir', 'cod_produto', 'cod_categoria');
	}

	public function exibir_categoria()
	{
		return $this->hasMany(\App\Models\CategoriaExibir::class, 'cod_produto');
	}

	public function desejados()
	{
		return $this->hasMany(\App\Models\Desejado::class, 'cod_produto');
	}

	public function itens_carrinho()
	{
		return $this->hasMany(\App\Models\ItemCarrinho::class, 'cod_produto');
	}

	public function super_desejados()
	{
		return $this->hasMany(\App\Models\SuperDesejado::class, 'cod_produto');
	}

	public function super_itens_carrinho()
	{
		return $this->hasMany(\App\Models\SuperItemCarrinho::class, 'cod_produto');
	}


	public function getLinkFotoAttribute(){
		return Storage::cloud()->url('produtos/' . $this->attributes['foto_exemplo']);
	}

	public function getNomeCompletoAttribute(){
		return $this->attributes['nome_produto'].' R$'.number_format($this->attributes['valor_reais'], 0, '', '.')." | " . $this->produto_tipo->nome_tipo;
	}

	public function getPontosVisualAttribute(){
		return number_format($this->attributes['valor_produto'], 0, '', '.');
	}

	public function getValorAttribute(){
        $campanha = Campanha::find(session('campanha')->cod_campanha);
	    return round(($campanha->valor_pontos != 0 ? $this->valor_reais / $campanha->valor_pontos : $this->valor_produto));
    }


	public function getSelosAttribute(){

		$selos = [];
		$fisica = ['class' => 'selo-loja-fisica', 'icone' => '/img/icone-loja-fisica.svg', 'descricao' => 'LOJA FÍSICA'];
		$ecommerce = ['class' => 'selo-e-commerce', 'icone' => '/img/icone-e-commerce.svg', 'descricao' => 'E-COMMERCE'];

		$selos[0] = [$fisica];
		$selos[1] = [$ecommerce];
		$selos[2] = [$ecommerce, $fisica];

		return $selos[$this->attributes['selo']];
	}

	public function getSelosAdmAttribute(){

		$selos = [];
		$fisica = ['class' => 'selo-roxo', 'icone' => asset('img/loja-fisica.svg'), 'descricao' => 'LOJA FÍSICA'];
		$ecommerce = ['class' => 'selo-laranja', 'icone' => asset('img/loja-virtual.svg'), 'descricao' => 'E-COMMERCE'];

		$selos[0] = [$fisica];
		$selos[1] = [$ecommerce];
		$selos[2] = [$fisica, $ecommerce];

		return $selos[$this->attributes['selo']];
	}


	public function styleDesejados($desejados){

		$resultado = [];

		if ($desejados->contains($this)):
			$resultado['like'] = 'icon-unlike';
			$resultado['txt'] = 'Remover da lista de desejos';
		else:
			$resultado['like'] = 'icon-like';
			$resultado['txt'] = 'Adicionar a lista de desejos';
		endif;

		return $resultado;

	}



	public function termometro($saldoUsuario, $valorCarrinho){

		$saldoFalso = $saldoUsuario - $valorCarrinho;

		$result = ($saldoFalso <= 0) ? 0 : ($saldoFalso / $this->valor) * 100;

		$resposta = [];

		if ($result < 60):

			$resposta['classe'] = "saldo-vermelho";
            $resposta['classe_nova'] = "vermelho";
			$resposta['progresso'] = "progress-vermelho";
			$resposta['corCarrinho'] = 'carrinho-vermelho';
			$resposta['classe_box'] = "texto-alternativo texto-vermelho";
			$resposta['icone'] = "/img/icone-erro.png";
			$resposta['titulo_texto'] = "PONTOS INSUFICIENTES";
			$resposta['texto'] = "Continue acumulando pontos para trocá-los por este item!!";
			$resposta['result'] = $result;

		elseif ($result >= 60 && $result < 100):

			$resposta['classe'] = "saldo-amarelo";
            $resposta['classe_nova'] = "amarelo";
			$resposta['progresso'] = "progress-amarelo";
			$resposta['corCarrinho'] = 'carrinho-amarelo';
			$resposta['classe_box'] = "texto-alternativo texto-amarelo";
			$resposta['icone'] = "/img/icone-erro-laranja.png";
			$resposta['titulo_texto'] = "PONTOS INSUFICIENTES";
			$resposta['texto'] = "Continue acumulando pontos para trocá-los por este item!!";
			$resposta['result'] = $result;

		elseif ($result >= 100):

			$resposta['classe'] = "saldo-verde";
            $resposta['classe_nova'] = "verde";
			$resposta['corCarrinho'] = 'carrinho-verde';
			$resposta['progresso'] = "progress-verde";
			$resposta['classe_box'] = "texto-alternativo";
			$resposta['icone'] = "/img/icone-sucesso.png";
			$resposta['titulo_texto'] = "PARABÉNS";
			$resposta['texto'] = "Você atingiu os pontos necessários para esse item!";
			$result = 100;
			$resposta['result'] = $result;

		endif;

		return $resposta;

	}

}
