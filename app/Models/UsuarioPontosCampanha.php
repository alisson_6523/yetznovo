<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 10 Sep 2019 14:00:23 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsuarioPontosCampanha
 *
 *
 * INSERT INTO yetz_novo.usuario_pontos_campanha (usuario_id, campanha_id, pontos, created_at, updated_at) (SELECT cod_usuario as usuario_id, cod_campanha as campanha_id, pontos_reais as pontos, NOW() as created_at, NOW() as updated_at FROM yetz_novo.usuario_campanha INNER JOIN yetz_novo.users ON id = cod_usuario GROUP BY cod_usuario, cod_campanha HAVING COUNT(*) = 1);
 *
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $campanha_id
 * @property float $pontos
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsuarioPontosCampanha extends Eloquent
{
	protected $table = 'usuario_pontos_campanha';

	protected $casts = [
		'usuario_id' => 'int',
		'campanha_id' => 'int',
		'pontos' => 'float'
	];

	protected $fillable = [
		'usuario_id',
		'campanha_id',
		'pontos'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'usuario_id');
	}
}
