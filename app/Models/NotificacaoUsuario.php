<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:45:58 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoUsuario
 * 
 * @property int $notificacao_id
 * @property int $cod_notificacao
 * @property int $cod_usuario
 * @property bool $visualizado
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class NotificacaoUsuario extends Eloquent
{
	protected $table = 'notificacao_usuario';
	protected $primaryKey = 'notificacao_id';

	protected $casts = [
		'cod_notificacao' => 'int',
		'cod_usuario' => 'int',
		'visualizado' => 'bool'
	];

	protected $fillable = [
		'cod_notificacao',
		'cod_usuario',
		'visualizado'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}
}
