<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 24 Sep 2019 15:23:22 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Storage;

/**
 * Class Video
 *
 * @property int $cod_video
 * @property int $cod_campanha
 * @property string $titulo
 * @property string $descricao
 * @property string $foto_capa
 * @property string $link_video
 * @property string $extensao
 * @property \Carbon\Carbon $data_entrada
 * @property \Carbon\Carbon $data_saida
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property int $cod_cliente
 * @property int $tipo_streaming
 * @property string $deleted_at
 *
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\Cliente $cliente
 * @property \Illuminate\Database\Eloquent\Collection $usuario_videos
 * @property \Illuminate\Database\Eloquent\Collection $video_origems
 * @property \Illuminate\Database\Eloquent\Collection $vinculos
 *
 * @package App\Models
 */
class Video extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'video';
	protected $primaryKey = 'cod_video';

	const STREAMING_GERAL    = 1;
	const STREAMING_CLIENTE  = 2;
	const STREAMING_CAMPANHA = 3;
	const STREAMING_ILHA     = 4;

	protected $casts = [
		'cod_campanha' => 'int',
		'cod_cliente' => 'int',
		'tipo_streaming' => 'int'
	];

	protected $dates = [
		'data_entrada',
		'data_saida'
	];

	protected $fillable = [
		'cod_campanha',
		'titulo',
		'descricao',
		'foto_capa',
		'link_video',
		'extensao',
		'data_entrada',
		'data_saida',
		'cod_cliente',
		'tipo_streaming'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function cliente()
	{
		return $this->belongsTo(\App\Models\Cliente::class, 'cod_cliente');
	}

	public function usuario_videos()
	{
		return $this->hasMany(\App\Models\UsuarioVideo::class);
	}

	public function origem()
	{
		return $this->hasMany(\App\Models\VideoOrigem::class, 'cod_video');
	}

	public function vinculo()
	{
		return $this->hasMany(\App\Models\Vinculo::class, 'cod_video');
	}

    public function getTipoAttribute()
    {
        switch ($this->extensao):
            case 'mp3':
                return 'audio';
                break;
            case 'mp4':
                return 'video';
                break;

        endswitch;
    }


    public function getLinkAttribute()
    {
        switch ($this->extensao):
            case 'mp3':
                return Storage::disk('s3')->url('audio/' . $this->link_video);
                break;
            case 'mp4':
                return Storage::disk('s3')->url('videos/' . $this->link_video);
                break;

        endswitch;
    }

    public function getFotoCapaAttribute()
    {

        return Storage::disk('s3')->url('video-capa/' . $this->attributes['foto_capa']);
    }
}
