<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Slide
 * 
 * @property int $id
 * @property string $link
 * @property string $imagem
 * @property int $ordem
 * @property \Carbon\Carbon $data_entrada
 * @property \Carbon\Carbon $data_saida
 * @property int $permanente
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Slide extends Eloquent
{
	protected $table = 'slide';

	protected $casts = [
		'ordem' => 'int',
		'permanente' => 'int'
	];

	protected $dates = [
		'data_entrada',
		'data_saida'
	];

	protected $fillable = [
		'link',
		'imagem',
		'ordem',
		'data_entrada',
		'data_saida',
		'permanente'
	];
}
