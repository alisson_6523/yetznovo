<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 26 Jun 2019 18:59:51 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * Class NotificacaoPopup
 *
 * @property int $cod_popup
 * @property int $cod_notificacao
 * @property string $titulo_popup
 * @property string $foto_popup
 * @property \Carbon\Carbon $entrada_popup
 * @property \Carbon\Carbon $saida_popup
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Notificacao $notificacao
 * @property \Illuminate\Database\Eloquent\Collection $popup_ilhas
 * @property \Illuminate\Database\Eloquent\Collection $popup_usuarios
 *
 * @package App\Models
 */
class NotificacaoPopup extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'notificacao_popup';
	protected $primaryKey = 'cod_popup';

    const POPUP_CLIENTE  = 1;
    const POPUP_CAMPANHA = 2;
    const POPUP_ILHA     = 3;

	// protected $appends = ['foto_popup'];

	protected $casts = [
		'cod_notificacao' => 'int'
	];

	protected $dates = [
		'entrada_popup',
		'saida_popup'
	];

	protected $fillable = [
		'titulo_popup',
		'foto_popup',
		'cod_cliente',
        'cod_campanha',
        'tipo_popup',
		'entrada_popup',
		'saida_popup'
	];


	public function popup_ilhas()
	{
		return $this->hasMany(\App\Models\PopupIlha::class, 'cod_popup');
	}

	public function popup_usuarios()
	{
		return $this->hasMany(\App\Models\PopupUsuario::class, 'cod_popup');
	}

	public function cliente(){
	    return $this->belongsTo(\App\Models\Cliente::class, 'cod_cliente');
    }

    public function campanha(){
        return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
    }

	public function setEntradaPopupAttribute($data)
	{
		$this->attributes['entrada_popup'] = Carbon::createFromFormat('d/m/Y', $data);
	}

	public function setSaidaPopupAttribute($data)
	{
		$this->attributes['saida_popup'] = Carbon::createFromFormat('d/m/Y', $data);
	}

	public function getLinkFotoPopupAttribute(){
		return Storage::cloud()->temporaryUrl('popup/' . $this->attributes['foto_popup'], \Carbon\Carbon::now()->addMinutes(5));
	}

}
