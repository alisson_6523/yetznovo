<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 02 Jul 2019 18:09:13 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CampanhaPdv
 * 
 * @property int $cod_campanha_pdv
 * @property int $cod_campanha
 * @property int $cod_pdv
 * @property \Carbon\Carbon $data_vinculo
 * 
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\ClientePdv $cliente_pdv
 *
 * @package App\Models
 */
class CampanhaPdv extends Eloquent
{
	protected $table = 'campanha_pdv';
	protected $primaryKey = 'cod_campanha_pdv';
	public $timestamps = false;

	protected $casts = [
		'cod_campanha' => 'int',
		'cod_pdv' => 'int'
	];

	protected $dates = [
		'data_vinculo'
	];

	protected $fillable = [
		'cod_campanha',
		'cod_pdv',
		'data_vinculo'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function cliente_pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}
}
