<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 29 Aug 2019 11:05:18 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoGeral
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $notificacao_id
 * @property int $tipo_notificacao
 * @property \Carbon\Carbon $data_leitura
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class NotificacaoGeral extends Eloquent {
    use \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table = 'notificacao_geral';

    protected $casts = [
        'usuario_id' => 'int',
        'notificacao_id' => 'int',
        'tipo_notificacao' => 'int'
    ];

    protected $dates = [
        'data_leitura'
    ];

    protected $fillable = [
        'usuario_id',
        'notificacao_id',
        'tipo_notificacao',
        'data_leitura'
    ];

    public function user() {
        return $this->belongsTo(\App\Models\User::class, 'usuario_id');
    }

    public function notificacao() {
        $objeto = NULL;

//        var_dump($this->notificacao_id);
//        return;

        switch($this->tipo_notificacao) {
            case NOTIFICACAO_GRUPO:
                $notificacao = NotificacaoGrupo::where('cod_grupo', $this->notificacao_id)->first();

                $objeto['tipo'] = NotificacaoCategorium::find($notificacao->cod_categoria)->nome_categoria;
                $objeto['titulo'] = $notificacao->titulo;
                $objeto['mensagem'] = $notificacao->mensagem;
                $objeto['class'] = $notificacao->getClass();
                break;
        }

        return $objeto;
    }
}
