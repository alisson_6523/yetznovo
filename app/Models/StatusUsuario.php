<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:09:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusUsuario
 * 
 * @property int $cod_status
 * @property string $nome_status
 * 
 * @property \Illuminate\Database\Eloquent\Collection $users
 *
 * @package App\Models
 */
class StatusUsuario extends Eloquent
{
	protected $table = 'status_usuario';
	protected $primaryKey = 'cod_status';
	public $timestamps = false;

	protected $fillable = [
		'nome_status'
	];

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'status');
	}
}
