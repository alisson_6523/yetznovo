<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:55:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ProdutoTipo
 * 
 * @property int $cod_tipo
 * @property string $nome_tipo
 * 
 * @property \Illuminate\Database\Eloquent\Collection $produtos
 *
 * @package App\Models
 */
class ProdutoTipo extends Eloquent
{
	protected $table = 'produto_tipo';
	protected $primaryKey = 'cod_tipo';
	public $timestamps = false;

	protected $fillable = [
		'nome_tipo'
	];

	public function produtos()
	{
		return $this->hasMany(\App\Models\Produto::class, 'tipo');
	}
}
