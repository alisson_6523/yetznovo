<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:58:36 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StatusPedido
 * 
 * @property int $cod_status
 * @property string $descricao_status
 *
 * @package App\Models
 */
class StatusPedido extends Eloquent
{
	protected $table = 'status_pedido';
	protected $primaryKey = 'cod_status';
	public $timestamps = false;

	protected $fillable = [
		'descricao_status'
	];
}
