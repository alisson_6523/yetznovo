<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:10:44 +0000.
 */

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Reliese\Database\Eloquent\Model as Eloquent;
use Hash;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @property int $id
 * @property int $cod_pdv
 * @property int $status
 * @property int $protocolo_insercao
 * @property int $nivel_usuario
 * @property string $password
 * @property string $login
 * @property string $nome_usuario
 * @property \Carbon\Carbon $data_nascimento
 * @property string $apelido
 * @property float $pontos_reais
 * @property string $foto_usuario
 * @property string $email
 * @property string $telefone
 * @property string $celular
 * @property string $sexo
 * @property string $coordenador
 * @property string $supervisor
 * @property string $ilha
 * @property int $notificacao_celular
 * @property string $cargo
 * @property \Carbon\Carbon $email_verified_at
 * @property string $remember_token
 * @property string $deleted_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\ClientePdv $cliente_pdv
 * @property \App\Models\StatusUsuario $status_usuario
 * @property \Illuminate\Database\Eloquent\Collection $carrinhos
 * @property \Illuminate\Database\Eloquent\Collection $desejados
 * @property \Illuminate\Database\Eloquent\Collection $historico_movimentacaos
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_streamings
 * @property \Illuminate\Database\Eloquent\Collection $notificacao_usuarios
 * @property \Illuminate\Database\Eloquent\Collection $resgates
 * @property \App\Models\UsuarioCampanha $usuario_campanha
 *
 * @package App\Models
 */
class User extends Authenticatable implements JWTSubject
{
	use \Illuminate\Database\Eloquent\SoftDeletes;

	use Notifiable;

	protected $casts = [
		'cod_pdv' => 'int',
		'status' => 'int',
		'protocolo_insercao' => 'int',
		'nivel_usuario' => 'int',
		'pontos_reais' => 'float',
		'notificacao_celular' => 'int'
	];

	protected $dates = [
		'data_nascimento',
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'cod_pdv',
		'status',
		'protocolo_insercao',
		'nivel_usuario',
		'password',
		'login',
		'nome_usuario',
		'data_nascimento',
		'apelido',
		'pontos_reais',
		'foto_usuario',
		'email',
		'telefone',
		'celular',
		'sexo',
		'coordenador',
        'email_coordenador',
		'supervisor',
        'email_supervisor',
		'ilha',
		'notificacao_celular',
		'cargo',
		'data_cadastro',
		'email_verified_at',
		'remember_token'
	];

	public function pdv()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}

	public function status_usuario()
	{
		return $this->belongsTo(\App\Models\StatusUsuario::class, 'status');
	}

	public function carrinhos()
	{
		return $this->hasMany(\App\Models\Carrinho::class, 'cod_usuario');
	}

	public function carrinhoAtual()
	{
		$carrinho = $this->hasOne(\App\Models\Carrinho::class, 'cod_usuario')->latest();

		if ($carrinho) :
			return $carrinho;
		else :
			return new \App\Models\Carrinho();
		endif;
	}

	public function desejados()
	{
		return $this->belongsToMany(\App\Models\Produto::class, 'desejados', 'cod_usuario', 'cod_produto');
	}

	public function campanhas()
	{
		return $this->belongsToMany(\App\Models\Campanha::class, 'usuario_campanha', 'cod_usuario', 'cod_campanha');
	}

	public function movimentacoes()
	{
		return $this->hasMany(\App\Models\HistoricoMovimentacao::class, 'cod_usuario');
	}

	public function vinculos()
	{
		return $this->hasMany(\App\Models\Vinculo::class, 'cod_pdv')->where('ilha', '=', $this->ilha);
	}

	public function notificacoes()
	{
		return $this->belongsTo(\App\Models\ClientePdv::class, 'cod_pdv');
	}

	public function pontos(){
	    return $this->hasMany(\App\Models\UsuarioPontosCampanha::class, 'usuario_id');
    }

    public function pontos_temporarios(){
        return $this->hasMany(\App\Models\UsuarioPontosCampanhaTemporario::class, 'usuario_id');
    }

	public function getVideosAttribute() {
        $campanha = Campanha::find(session('campanha')->cod_campanha);
        $videos   = [];

	    $videos_geral = Video::where('tipo_streaming', Video::STREAMING_GERAL)->where('data_entrada', '<=', date('Y-m-d'))->where('data_saida', '>=', date('Y-m-d H:i:s'))->get();
	    foreach($videos_geral as $video_geral)
	        array_push($videos, $video_geral);

	    $videos_cliente = Video::where('tipo_streaming', Video::STREAMING_CLIENTE)->where('cod_cliente', $campanha->cliente_id)->where('data_entrada', '<=', date('Y-m-d H:i:s'))->where('data_saida', '>=', date('Y-m-d H:i:s'))->get();
        foreach($videos_cliente as $video_cliente)
            array_push($videos, $video_cliente);

        $videos_campanha = Video::where('tipo_streaming', Video::STREAMING_CAMPANHA)->where('cod_campanha', $campanha->cod_campanha)->where('data_entrada', '<=', date('Y-m-d H:i:s'))->where('data_saida', '>=', date('Y-m-d H:i:s'))->get();
        foreach($videos_campanha as $video_campanha)
            array_push($videos, $video_campanha);

		$videos_ilha = Video::select(['video.*'])->join('vinculo', 'video.cod_video', '=', 'vinculo.cod_video')
			->join('users', [['vinculo.cod_pdv', '=', 'users.cod_pdv'], ['vinculo.ilha', '=', 'users.ilha']])
            ->where('video.cod_campanha', $campanha->cod_campanha)
			->where('users.id', '=', $this->id)
            ->where('video.data_entrada', '<=', date('Y-m-d H:i:s'))->where('video.data_saida', '>=', date('Y-m-d H:i:s'))->get();
		foreach($videos_ilha as $video_ilha)
		    array_push($videos, $video_ilha);

		return collect($videos);
	}

	public function videosVistos(){
	    return $this->hasMany(\App\Models\UsuarioVideo::class, 'usuario_id');
    }

    public function getCountVideosVistosAttribute(){
	    $count = 0;
	    foreach($this->videosVistos()->with('video')->get() as $video){
	        if($this->videos->contains($video->video))
                $count += 1;
        }

	    return $count;
    }

	public function getPopupsAttribute() {
	    $campanha = Campanha::find(session('campanha')->cod_campanha);
	    $popups   = [];

	    $popups_cliente = NotificacaoPopup::where('tipo_popup', NotificacaoPopup::POPUP_CLIENTE)->where('cod_cliente', $campanha->cliente_id)->where('entrada_popup', '<=', date('Y-m-d H:i:s'))->where('saida_popup', '>=', date('Y-m-d H:i:s'))->get();
        foreach($popups_cliente as $popup_cliente)
            array_push($popups, $popup_cliente);


        $popups_campanha = NotificacaoPopup::where('tipo_popup', NotificacaoPopup::POPUP_CAMPANHA)->where('cod_campanha', $campanha->cod_campanha)->where('entrada_popup', '<=', date('Y-m-d H:i:s'))->where('saida_popup', '>=', date('Y-m-d H:i:s'))->get();
        foreach($popups_campanha as $popup_campanha)
            array_push($popups, $popup_campanha);

		$popups_ilha = NotificacaoPopup::select(['notificacao_popup.*'])->join('popup_ilha', 'notificacao_popup.cod_popup', '=', 'popup_ilha.cod_popup')
            ->join('users', [['popup_ilha.cod_pdv', '=', 'users.cod_pdv'], ['popup_ilha.ilha', '=', 'users.ilha']])
            ->where('notificacao_popup.tipo_popup', NotificacaoPopup::POPUP_ILHA)
            ->where('notificacao_popup.cod_campanha', $campanha->cod_campanha)
            ->where('users.id', '=', $this->id)
            ->where('notificacao_popup.entrada_popup', '<=', date('Y-m-d H:i:s'))->where('notificacao_popup.saida_popup', '>=', date('Y-m-d H:i:s'))->get();
		foreach($popups_ilha as $popup_ilha)
		    array_push($popups, $popup_ilha);

		return collect($popups);
	}

	public function popups_visualizados()
	{
		return $this->belongsToMany(\App\Models\NotificacaoPopup::class, 'popup_usuario', 'cod_usuario', 'cod_popup');
	}

	public function notificacoes_gerais()
	{
		return $this->hasMany(\App\Models\NotificacaoGeral::class, 'usuario_id');
	}

	public function resgates()
	{
		return $this->hasMany(\App\Models\Resgate::class, 'cod_usuario');
	}

	// Modifiers
	public function setPasswordAttribute($p)
	{
		$this->attributes['password'] = sha1($p);
	}

	public function setDataNascimentoAttribute($data)
	{
		$data_br = \Carbon\Carbon::createFromFormat('d/m/Y', $data);

		if ($data_br !== false) :
			$this->attributes['data_nascimento'] = $data_br->format('Y-m-d');
		else :
			$this->attributes['data_nascimento'] = '1991-01-01';
		endif;
	}


	public function getNascimentoAttribute(){
		return $this->data_nascimento->format('d/m/Y');
	}


	public function getSaldoUsuarioAttribute(){
		$campanha = Campanha::find(session('campanha')->cod_campanha);

		$pontos = $this->pontos()->where('campanha_id', $campanha->cod_campanha)->first();

		return $pontos == null ? 0 : $pontos->pontos;
	}

    public function getSaldoTemporarioUsuarioAttribute(){
        $campanha = Campanha::find(session('campanha')->cod_campanha);

        $pontos = $this->pontos_temporarios()->where('campanha_id', $campanha->cod_campanha)->sum('pontos');

        return $pontos == null ? 0 : $pontos;
    }

	public function getAbreviacaoAttribute()
	{
		$array = explode(' ', $this->attributes['apelido']);

		return $array[0];
	}

	public function getFotoUsuarioAttribute()
	{
		if($this->attributes['foto_usuario']):
			return \Storage::cloud()->temporaryUrl('usuarios/' . $this->attributes['foto_usuario'], \Carbon\Carbon::now()->addMinutes(1));
		endif;

		return '/img/foto-usuario-responsivo.png';

	}

    public function getEmailForPasswordReset() {
        // Há possibilidade de haver emails duplicados e logins tambem. Foi necessário colocar o id do usuário aqui
        // Também não consegui alterar o nome da coluna da tabela "password resets" porque o framework tenta procurar a coluna 'email' no notification
        return $this->id;
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }


    public function getJWTCustomClaims()
    {
        return [];
    }
}
