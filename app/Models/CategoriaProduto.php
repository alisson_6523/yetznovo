<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 16:38:19 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoriaProduto
 *
 * @property int $cod_categoria_produto
 * @property string $categoria
 * @property string $descricao
 * @property string $link_categoria
 * @property string $icone_categoria
 * @property int $ordem
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property \App\Models\CampanhaCategorium $campanha_categorium
 * @property \Illuminate\Database\Eloquent\Collection $categoria_exibirs
 * @property \Illuminate\Database\Eloquent\Collection $produtos
 *
 * @package App\Models
 */
class CategoriaProduto extends Eloquent
{
    use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table = 'categoria_produto';
	protected $primaryKey = 'cod_categoria_produto';
	public $timestamps = false;

	protected $casts = [
		'ordem' => 'int'
	];

	protected $fillable = [
		'categoria',
		'descricao',
		'link_categoria',
		'icone_categoria',
		'ordem'
	];

	public function campanha_categoria()
	{
		return $this->hasOne(\App\Models\CampanhaCategorium::class, 'cod_categoria');
	}

	public function produtos()
	{
        return $this->belongsToMany(\App\Models\Produto::class, 'categoria_exibir', 'cod_categoria', 'cod_produto');
    }

    public function categoria_unica_produtos(){
	    return $this->hasMany(\App\Models\Produto::class, 'cod_categoria_produto');
    }
}
