<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 08 Jul 2019 14:43:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class NotificacaoListum
 * 
 * @property int $id
 * @property string $nome
 * @property int $criador
 * @property int $cliente
 * @property string $criterios
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class NotificacaoListum extends Eloquent
{
	protected $casts = [
		'criador' => 'int',
		'cliente' => 'int'
	];

	protected $fillable = [
		'nome',
		'criador',
		'cliente',
		'criterios'
	];
}
