<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:47:46 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class CategoriaExibir
 * 
 * @property int $id
 * @property int $cod_categoria
 * @property int $cod_produto
 * 
 * @property \App\Models\CategoriaProduto $categoria_produto
 * @property \App\Models\Produto $produto
 *
 * @package App\Models
 */
class CategoriaExibir extends Eloquent
{
	protected $table = 'categoria_exibir';
	public $timestamps = false;

	protected $casts = [
		'cod_categoria' => 'int',
		'cod_produto' => 'int'
	];

	protected $fillable = [
		'cod_categoria',
		'cod_produto'
	];

	public function categoria_produto()
	{
		return $this->belongsTo(\App\Models\CategoriaProduto::class, 'cod_categoria');
	}

	public function produto()
	{
		return $this->belongsTo(\App\Models\Produto::class, 'cod_produto');
	}
}
