<?php

/**
 * Created by Reliese Model.
 * Date: Tue, 06 Aug 2019 17:39:02 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Regulamento
 *
 * @property int $id
 * @property int $cod_campanha
 * @property string $regulamento
 * @property string $regulamento_hash
 * @property string $ilha
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Campanha $campanha
 *
 * @package App\Models
 */
class Regulamento extends Eloquent
{
	protected $casts = [
		'cod_campanha' => 'int'
	];

	protected $fillable = [
		'cod_campanha',
        'titulo',
		'regulamento',
		'regulamento_hash',
		'ilha'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}
}
