<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:01:59 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Autenticacao
 * 
 * @property int $cod_autenticacao
 * @property string $tipo_autenticacao
 * @property string $mascara
 * 
 * @property \Illuminate\Database\Eloquent\Collection $campanhas
 *
 * @package App\Models
 */
class Autenticacao extends Eloquent
{
	protected $table = 'autenticacao';
	protected $primaryKey = 'cod_autenticacao';
	public $timestamps = false;

	protected $fillable = [
		'tipo_autenticacao',
		'mascara'
	];

	public function campanhas()
	{
		return $this->hasMany(\App\Models\Campanha::class, 'autenticacao');
	}
}
