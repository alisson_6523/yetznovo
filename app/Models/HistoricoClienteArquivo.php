<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 01 Aug 2019 14:03:54 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class HistoricoClienteArquivo
 * 
 * @property int $id
 * @property int $arquivo_id
 * @property int $adm_id
 * @property string $operacao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Admin $admin
 * @property \App\Models\ClienteArquivo $cliente_arquivo
 *
 * @package App\Models
 */
class HistoricoClienteArquivo extends Eloquent
{
	protected $casts = [
		'arquivo_id' => 'int',
		'adm_id' => 'int'
	];

	protected $fillable = [
		'arquivo_id',
		'adm_id',
		'operacao'
	];

	public function admin()
	{
		return $this->belongsTo(\App\Models\Admin::class, 'adm_id');
	}

	public function cliente_arquivo()
	{
		return $this->belongsTo(\App\Models\ClienteArquivo::class, 'arquivo_id');
	}
}
