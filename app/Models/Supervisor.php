<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Supervisor
 * 
 * @property int $id
 * @property int $ilha
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Supervisor extends Eloquent
{
	protected $table = 'supervisor';

	protected $casts = [
		'ilha' => 'int'
	];

	protected $fillable = [
		'ilha'
	];
}
