<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 18 Nov 2019 16:03:59 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsuarioPontosCampanhaTemporario
 * 
 * @property int $id
 * @property int $protocolo_id
 * @property int $usuario_id
 * @property int $campanha_id
 * @property float $pontos
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\CampanhaProtocoloTemporario $campanha_protocolo_temporario
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsuarioPontosCampanhaTemporario extends Eloquent
{
	protected $table = 'usuario_pontos_campanha_temporario';

	protected $casts = [
		'protocolo_id' => 'int',
		'usuario_id' => 'int',
		'campanha_id' => 'int',
		'pontos' => 'float'
	];

	protected $fillable = [
		'protocolo_id',
		'usuario_id',
		'campanha_id',
		'pontos'
	];

	public function campanha()
	{
		return $this->belongsTo(\App\Models\Campanha::class);
	}

	public function campanha_protocolo_temporario()
	{
		return $this->belongsTo(\App\Models\CampanhaProtocoloTemporario::class, 'protocolo_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'usuario_id');
	}
}
