<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 18 Sep 2019 10:18:58 -0300.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Banner
 *
 * @property int $cod_banner
 * @property string $link_banner
 * @property \Carbon\Carbon $data_entrada
 * @property \Carbon\Carbon $data_saida
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $titulo
 * @property int $cod_campanha
 *
 * @property \App\Models\Campanha $campanha
 *
 * @package App\Models
 */
class Banner extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'banner';
	protected $primaryKey = 'cod_banner';

	protected $casts = [
		'cod_campanha' => 'int'
	];

	protected $dates = [
		'data_entrada',
		'data_saida'
	];

	protected $fillable = [
	    'arquivo',
		'hash_arquivo',
		'data_entrada',
		'data_saida',
		'titulo',
		'cod_campanha'
	];

	public function campanha(){
		return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

    public function getEntradaAttribute(){
        if($this->data_entrada)
            return $this->data_entrada->format('d.m.Y');

        return $this->created_at->format('d.m.Y');
    }

    public function getSaidaAttribute(){
        if($this->attributes['data_saida'])
            return $this->data_saida->format('d.m.Y');

        return 'Indefinido';
    }

    public function getLinkAttribute(){
        return \Storage::disk('s3')->url('banners/' . $this->hash_arquivo);
    }
}
