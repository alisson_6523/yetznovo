<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 30 May 2019 20:25:22 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AdminPermissao
 * 
 * @property int $id
 * @property int $perfilId
 * @property int $paginaId
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\AdminPagina $admin_pagina
 * @property \App\Models\AdminPerfil $admin_perfil
 *
 * @package App\Models
 */
class AdminPermissao extends Eloquent
{
	protected $table = 'admin_permissao';

	protected $casts = [
		'perfilId' => 'int',
		'paginaId' => 'int'
	];

	protected $fillable = [
		'perfilId',
		'paginaId'
	];

	public function pagina()
	{
		return $this->belongsTo(\App\Models\AdminPagina::class, 'paginaId');
	}

	public function perfil()
	{
		return $this->belongsTo(\App\Models\AdminPerfil::class, 'perfilId');
	}
}
