<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 18:09:46 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SuperMovimentacao
 * 
 * @property int $cod_movimentacao
 * @property int $cod_cartao
 * @property string $protocolo
 * @property int $tipo
 * @property float $valor
 * @property string $desc_tipo
 * @property string $descricao
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\SuperCartao $super_cartao
 *
 * @package App\Models
 */
class SuperMovimentacao extends Eloquent
{
	protected $table = 'super_movimentacao';
	protected $primaryKey = 'cod_movimentacao';

	protected $casts = [
		'cod_cartao' => 'int',
		'tipo' => 'int',
		'valor' => 'float'
	];

	protected $fillable = [
		'cod_cartao',
		'protocolo',
		'tipo',
		'valor',
		'desc_tipo',
		'descricao'
	];

	public function super_cartao()
	{
		return $this->belongsTo(\App\Models\SuperCartao::class, 'cod_cartao');
	}
}
