<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:44:48 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Endereco
 * 
 * @property int $cod_endereco
 * @property string $logradouro
 * @property string $numero
 * @property string $complemento
 * @property string $bairro
 * @property string $cep
 * @property string $cidade
 * @property string $sigla_estado
 * @property int $primario
 * @property int $ddd
 * 
 * @property \App\Models\Estado $estado
 * @property \Illuminate\Database\Eloquent\Collection $super_resgates
 *
 * @package App\Models
 */
class Endereco extends Eloquent
{
	protected $table = 'endereco';
	protected $primaryKey = 'cod_endereco';
	public $timestamps = false;

	protected $casts = [
		'primario' => 'int',
		'ddd' => 'int'
	];

	protected $fillable = [
		'logradouro',
		'numero',
		'complemento',
		'bairro',
		'cep',
		'cidade',
		'sigla_estado',
		'primario',
		'ddd'
	];

	public function estado()
	{
		return $this->belongsTo(\App\Models\Estado::class, 'sigla_estado');
	}

	public function super_resgates()
	{
		return $this->hasMany(\App\Models\SuperResgate::class, 'cod_endereco');
	}
}
