<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 12 Jun 2019 17:56:44 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UsuarioCampanha
 *
 * @property int $cod_usuario
 * @property int $cod_campanha
 * 
 * @property \App\Models\Campanha $campanha
 * @property \App\Models\User $user
 *
 * @package App\Models
 */
class UsuarioCampanha extends Eloquent
{
	protected $table = 'usuario_campanha';
    public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'cod_usuario' => 'int',
		'cod_campanha' => 'int'
	];

	protected $fillable = [
		'cod_usuario',
		'cod_campanha'
	];

	public function campanha()
	{
        return $this->belongsTo(\App\Models\Campanha::class, 'cod_campanha');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'cod_usuario');
	}
}
