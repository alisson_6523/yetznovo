<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Resgate;
use Carbon\Carbon;
use App\Models\StatusResgate;
use Notification;
use App\Notifications\StatusResgateNotification;
class AtualizaStatusResgate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resgate:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza automaticamente os status dos resgates de acordo com os itens do resgate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $resgates = Resgate::whereIn('cod_status', [1,2,3])->where('created_at', '>', '2019-08-07')->get();

        $this->info(Carbon::now());

        foreach($resgates as $resgate){

            if($resgate->carrinho->itens()->distinct('cod_status')->count('cod_status') == 1){
                if($resgate->cod_status != $resgate->carrinho->itens()->groupBy('cod_status')->distinct()->first()->cod_status){
                    $this->info("Resgate " . $resgate->cod_resgate . ": Status alterado de '". $resgate->status_carrinho->descricao_status ."' para => '" . $resgate->carrinho->itens()->groupBy('cod_status')->distinct()->first()->status_resgate->descricao_status . "'");
                    $resgate->cod_status = $resgate->carrinho->itens()->groupBy('cod_status')->distinct()->first()->cod_status;
                    $resgate->update();
                }
            } else {

                if($resgate->carrinho->itens()->distinct('cod_status')->count('cod_status') == 2){

                    if($resgate->carrinho->itens()->distinct('cod_status')->select('cod_status')->get()->contains('cod_status', '5')){
                        $this->info("Resgate " . $resgate->cod_resgate . ": Status alterado de '". $resgate->status_carrinho->descricao_status ."' para => '" . $resgate->carrinho->itens()->where('cod_status', '!=', 5)->get()->first()->status_resgate->descricao_status. "'");
                        $resgate->cod_status = $resgate->carrinho->itens()->where('cod_status', '!=', 5)->get()->first()->cod_status;
                        $resgate->update();
                    }

                }

            }

        }

        $this->info('------------------------------------------------------------------------------------');



//        foreach ($resgates as $r) :
//
//            $itens = $r->carrinho->itens()->where('cod_status', '<', '4')->get();
//            $sendMail = false;
//
//            $alteracoes = [];
//
//            $this->info("$r->cod_resgate: " . $itens->count() . ' itens encontrados');
//
//            foreach ($itens as $i) :
//
//
//
//                $dias = $r->created_at->diffInDays();
//
//                $cod_status = $i->cod_status;
//
//                if (($dias > 15 && $dias < 30) && $i->cod_status < StatusResgate::APROVADO) :
//
//                    $cod_status = StatusResgate::APROVADO;
//
//                elseif ($dias >= 30 && $i->cod_status < StatusResgate::APROVADO) :
//
//                    $cod_status = StatusResgate::ACAMINHO;
//
//                endif;
//
//                if ($i->cod_status != $cod_status) :
//
//
//                    $respString = "$i->cod_produto/$i->cod_carrinho"
//                        . ' trocado de '
//                        . StatusResgate::lista[$i->cod_status]
//                        . ' para '
//                        . StatusResgate::lista[$cod_status]
//                        . " ($dias)";
//
//                    $i->cod_status = $cod_status;
//                    $alteracoes[$i->cod_produto] = 'alterado para "' . StatusResgate::lista[$i->cod_status]. '"';
//                    // $i->update();
//
//                    $sendMail = true;
//
//                else:
//                    $alteracoes[$i->cod_produto] = StatusResgate::lista[$i->cod_status];
//
//                endif;
//
//            endforeach;
//
//            if ($sendMail) :
//                // envia mensagem de status resgate
//                Notification::send($r->user, new StatusResgateNotification($r, $alteracoes));
//                $this->info('email enviado para '. $r->user->email);
//                // envia email
//
//
//            endif;
//        endforeach;
    }
}
