<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use function GuzzleHttp\json_decode;
use App\Models\Estado;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Url;
use phpDocumentor\Reflection\DocBlock\Tags\Link;
use File;
use Mockery\Exception;

class ImportaEstado extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'importa:estado';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        echo base_path('imports/estado.json');
        echo "\n";
        echo app_path('imports/estado.json');
        echo "\n";
        echo public_path('imports/estado.json');
        echo "\n";
        echo storage_path('imports/estado.json');
        echo "\n";

        $json = File::get(public_path('imports/estado.json'));

        $estados = json_decode($json, true);

        foreach ($estados as $e) :

            $estado = new Estado($e);
            try {
                $estado->save();
            } catch (\Illuminate\Database\QueryException $e) {

                echo "Já temos este regustro ($estado->sigla)\n";

            }
        endforeach;


        echo "acabei por aqui\n";
    }
}
