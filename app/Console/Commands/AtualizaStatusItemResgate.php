<?php

namespace App\Console\Commands;

use App\Mail\StatusItemResgateMail;
use App\Models\ItemCarrinho;
use App\Models\StatusResgate;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Notification;
use App\Notifications\StatusResgateNotification;

class AtualizaStatusItemResgate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'itemresgate:status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza o status dos itens do resgate a partir da data do resgate e seu respectivo tipo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $itens = ItemCarrinho::whereIn('cod_status', [1,2])->where('created_at', '>', '2019-08-07')->get();

        $this->info(Carbon::now());

        foreach($itens as $index => $item){

            if($item->carrinho->status_carrinho == 0){
                $atualizou = false;

                if($item->cod_status == 1){ // Aguardando

                    $diferenca = Carbon::now()->diffInDays($item->updated_at);

                    if($diferenca >= 2){
                        $this->info('Item ' . $item->cod_item . ' Alterado de "' . $item->status_resgate->descricao_status . '" para "Aprovado"');
                        $item->cod_status = 2; // Aprovado
                        $item->update();
                        $atualizou = true;
                        $alteracoes[$item->cod_item] = 'Alterado para "' . StatusResgate::lista[$item->cod_status]. '"';
                    }

                } else { // cod_status == 2 (Aprovado)

                    $diferenca = Carbon::now()->diffInDays($item->updated_at);

                    if($item->produto->tipo == 1){ // (Cartão Físico)

                        if($diferenca >= 25){ // 25 dias ou mais
                            $this->info('Item ' . $item->cod_item . ' Alterado de "' . $item->status_resgate->descricao_status . '" para "A Caminho"');
                            $item->cod_status = 3;
                            $item->update();
                            $atualizou = true;
                            $alteracoes[$item->cod_item] = 'Alterado para "' . StatusResgate::lista[$item->cod_status]. '"';
                        }

                    } else { // item->produto->tipo == 2 (Voucher)

                        if($diferenca >= 12) { // 12 dias ou mais
                            $this->info('Item ' . $item->cod_item . ' Alterado de "' . $item->status_resgate->descricao_status . '" para "A Caminho"');
                            $item->cod_status = 3;
                            $item->update();
                            $atualizou = true;
                            $alteracoes[$item->cod_item] = 'Alterado para "' . StatusResgate::lista[$item->cod_status]. '"';
                        }

                    }

                }

                if($atualizou){
                    if((isset($itens[$index+1]) && $itens[$index+1]->cod_carrinho != $item->cod_carrinho) || (!isset($itens[$index+1]))){
                        try{
                            Mail::to($item->carrinho->resgates->first()->user->email)->send(new StatusItemResgateMail($item->carrinho->resgates->first(), $alteracoes));
                        } catch (\Exception $e){
                            $this->info('Item =>  '. $item->cod_item .'| ERRO: => ' . $e->getMessage());
                        }

                    }


////                Notification::send($item->carrinho->resgates->first()->user, new StatusResgateNotification($item->carrinho->resgates->first(), $mensagem)); dava erro pq nao tava achando o theme:default
                }

            }

        }

        $this->info('------------------------------------------------------------------------------------');

    }
}
