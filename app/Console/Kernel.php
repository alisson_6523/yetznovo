<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

        \App\Console\Commands\ImportaEstado::class,
        \App\Console\Commands\AtualizaStatusResgate::class,
        \App\Console\Commands\AtualizaStatusItemResgate::class,

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->command('itemresgate:status')
            ->twiceDaily(11, 23)
            ->appendOutputTo(storage_path('logs/resgates/itemResgateStatus-'.date('Y-m-d').'.log'));

        $schedule->command('resgate:status')
            ->twiceDaily(11, 23)
            ->appendOutputTo(storage_path('logs/resgates/resgateStatus-'.date('Y-m-d').'.log'));

//            ->dailyAt('16:00')
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
