<?php

namespace App\Search;

use App\Models\ItemCarrinho;
use Illuminate\Http\Request;


class ResgateSearch
{
    public static function apply(Request $filters) {

//        $resgates = Resgate::select(['item_carrinho.*', 'campanha.cod_campanha', 'users.cod_pdv'])
//                            ->join('campanha', 'resgate.cod_campanha', '=', 'campanha.cod_campanha')
//                            ->join('users', 'resgate.cod_usuario', '=', 'users.id')
//                            ->join('resgate', 'resgate.cod_carrinho', '=', 'item_carrinho.cod_carrinho');

        // O certo é filtrar por itens do carrinho
        $resgates = ItemCarrinho::select(['item_carrinho.*', 'campanha.cod_campanha', 'users.cod_pdv'])
                                ->join('carrinho', 'carrinho.cod_carrinho', '=', 'item_carrinho.cod_carrinho')
                                ->join('resgate', 'resgate.cod_carrinho', '=', 'carrinho.cod_carrinho')
                                ->join('campanha', 'campanha.cod_campanha', '=', 'resgate.cod_campanha')
                                ->join('users', 'users.id', '=', 'resgate.cod_usuario')
                                ->join('produto', 'item_carrinho.cod_produto', '=', 'produto.cod_produto');

        if($filters->has('tipo_produto')){
            $resgates->where('produto.tipo', $filters->tipo_produto);
        }

        if ($filters->has('cod_cliente')) {
            $resgates->where('campanha.cliente_id', $filters->input('cod_cliente'));
        }

        if ($filters->has('cod_campanha')) {
            $resgates->where('resgate.cod_campanha', $filters->input('cod_campanha'));
        }

        if ($filters->has('arr_status')) {
            $resgates->whereIn('item_carrinho.cod_status', json_decode($filters->arr_status));
        }

        if($filters->has('data_inicial') && !$filters->has('data_final')){
            $inicio = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $filters->input('data_inicial') . '00:00:01');
            $resgates->where('resgate.created_at', '>=', $inicio);
        }

        if(!$filters->has('data_inicial') && $filters->has('data_final')){
            $fim = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $filters->input('data_final') . '23:59:59');
            $resgates->where('resgate.created_at', '<=', $fim);
        }

        if($filters->has('data_inicial') && $filters->has('data_final')){
            $inicio = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $filters->input('data_inicial') . '00:00:01');
            $fim = \Carbon\Carbon::createFromFormat('d/m/Y H:i:s', $filters->input('data_final') . '23:59:59');

            $resgates->whereBetween('resgate.created_at', [$inicio, $fim]);

        }

        if($filters->has('arr_pdvs')){
            $resgates->whereIn('users.cod_pdv', json_decode($filters->arr_pdvs));
        }

//        dd($resgates->get());

        return $resgates->orderBy('resgate.created_at', 'DESC')->get();
    }
}
