<?php

namespace App\Search;

use App\Models\User;
use Illuminate\Http\Request;


class UserSearch
{
    public static function apply(Request $filters)
    {

        $usuarios = User::select('users.*')
                        ->join('cliente_pdv', 'users.cod_pdv', '=', 'cliente_pdv.cod_pdv')
                        ->join('campanha_pdv', 'cliente_pdv.cod_pdv', '=', 'campanha_pdv.cod_pdv');



        if ($filters->has('cod_cliente') && $filters->input('cod_cliente') != '') {
            $usuarios->where('cliente_pdv.cliente_id', $filters->input('cod_cliente'));
        }

        if ($filters->has('cod_campanha') && $filters->input('cod_campanha') != '') {
            $usuarios->where('campanha_pdv.cod_campanha', $filters->input('cod_cliente'));
        }





        if ($filters->has('pdvs') && !empty($filters->input('pdvs'))) {

            $pdvs = $filters->input('pdvs');
            $usuarios->where(function($query) use ($pdvs){
                            foreach($pdvs as $p => $ilhas):
                                foreach($ilhas as $i):                    

                                    $w = [['users.cod_pdv', '=', $p], ['users.ilha', '=', $i]];
                                    
                                    $query->orWhere($w);

                                endforeach;
                            endforeach;
                });

        }

        return $usuarios;
        

    }
}
