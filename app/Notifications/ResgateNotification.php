<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Mail\ConfirmacaoResgateMail;

class ResgateNotification extends Notification
{
    use Queueable;

    private $details;

    protected $cod_categoria;

    /**
     * Create a new notification instance.
     * @param  mixed  $d
     * @return void
     */
    public function __construct($d)
    {
        //
        $this->details = $d;
        $this->cod_campanha = $d['cod_campanha'];

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Você efetuou um novo resgate!")
            ->from('atendimento@yetzcards.com.br', 'Atendimento YetzCards')
            ->view('sistema.emails.confirmacao-resgate', [ 'dados' => $this->details ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->details;
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
                'titulo' => 'Resgate Realizado',
                'mensagem' => 'Parabéns, você efetuou um resgate na plataforma YETZCARDS. Confira o status'
                            .' da entrega no menu "ACOMPANHAR RESGATES".',
                'link' => action('LojaController@resgates'),
                'icone' => 'novo_cartao.svg',
                'class' => 'pontos_creditados',
                'cod_campanha' => $this->cod_campanha
               ];
    }
}
