<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class GrupoNotification extends Notification
{
    use Queueable;
    private $titulo;
    private $mensagem;
    private $grupo;
    private $link;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($titulo, $mensagem, $grupo = null, $link = '')
    {
        //
        $this->titulo = $titulo;
        $this->mensagem = $mensagem;
        $this->grupo = $grupo;
        $this->link = $link;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
                'titulo' => $this->titulo,
                'mensagem' => $this->mensagem,
                'link' => $this->link,
                'grupo' => $this->grupo,
                'link' => '',
                'icone' => 'novo_cartao.svg',
                'topo' => '',
                'class' => 'pontos_creditados'

               ];
    }


}
