<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VoucherNotification extends Notification
{
    use Queueable;

    private $details;

    protected $cod_categoria;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($dados)
    {
        $this->details = $dados;
        $this->cod_campanha = $dados['cod_campanha'];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("Seu voucher digital está disponível!")
            ->from('atendimento@yetzcards.com.br', 'Atendimento YetzCards')
            ->view('sistema.emails.voucher-digital', [ 'dados' => $this->details ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->details;
    }


    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'titulo' => 'Voucher Digital Disponível',
            'mensagem' => 'Olá! O seu voucher digital já está disponível!',
            'link' => action('LojaController@vouchers'),
            'icone' => 'novo_cartao.svg',
            'class' => 'pontos_creditados',
            'cod_campanha' => $this->cod_campanha
        ];
    }
}
