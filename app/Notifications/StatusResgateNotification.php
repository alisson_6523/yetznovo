<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Carrinho;
use App\Models\Resgate;

class StatusResgateNotification extends Notification
{
    use Queueable;

    private $resgate;
    private $alteracoes;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Resgate $r, $alteracoes = [])
    {
        //
        $this->resgate = $r;
        $this->alteracoes = $alteracoes;


    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        return (new MailMessage)->subject('Yetz Cards: Atualização de status de resgate')
                                ->markdown('sistema.emails.status-resgate', [
                                    'resgate' => $this->resgate,
                                    'alteracoes' => $this->alteracoes
                                    ]
                                );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'titulo' => 'Atualização de status de resgate', 
            'mensagem' => 'Um dos itens de seu resgate teve o status de entrega atualizado. Confira sua página de pedidos!', 
            'link' => action('LojaController@resgates'),
            'icone' => 'novo_cartao.svg',
            'class' => 'pontos_creditados'
           ];
    }
    
    public function toDatabase($notifiable)
    {
        return [
            'titulo' => 'Atualização de status', 
            'mensagem' => 'Um dos itens de seu resgate teve o status de entrega atualizado. Confira sua página de pedidos!', 
            'link' => action('LojaController@resgates'),
            'icone' => 'novo_cartao.svg',
            'class' => 'pontos_creditados'
           ];
    }
}
