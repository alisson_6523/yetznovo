<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\HtmlString;

class ResetPassword extends Notification
{
    use Queueable;

    private $token;
    private $usuario;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario, $token)
    {
        //
        $this->usuario = $usuario;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $urlRecuperacao = "/nova-senha/". $this->token;

        return (new MailMessage)
                    ->subject('Recuperação de senha')
                    ->line('Olá, '. $this->usuario->apelido)
                    ->line('Você recebeu este e-mail porque houve um pedido de redefinição de senha para sua conta.')
                    ->action('Resetar Senha', url($urlRecuperacao))
                    ->salutation('Equipe '. config('app.name'))
                    ->line('Se você não solicitou uma alteração da senha, nenhuma ação adicional é necessária.');




    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
