<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AvisoDePontosMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        return $this->view('sistema.emails.aviso-de-pontos')
            ->from('atendimento@yetzcards.com.br', 'Atendimento YetzCards')
            ->subject('Seus pontos estão disponíveis!')
            ->with([ 'dados' => $this->data ]);

    }
}
