<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContatoMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('sistema.emails.contato')
            ->from('cards@yetzcards.com.br', 'Fale Conosco Yetz Cards')
            ->subject('Contato Fale Conosco')
            ->with([ 'dados' => $this->data ]);
    }
}
