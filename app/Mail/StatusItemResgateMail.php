<?php

namespace App\Mail;

use App\Models\Resgate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StatusItemResgateMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resgate, $alteracoes;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resgate $r, $alteracoes = []) {
        $this->resgate = $r;
        $this->alteracoes = $alteracoes;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('sistema.emails.status-resgate')
            ->from('atendimento@yetzcards.com.br', 'Atendimento Yetz Cards')
            ->subject('Yetz Cards: Atualização de status de resgate')
            ->with(['resgate' => $this->resgate, 'alteracoes' => $this->alteracoes]);
    }
}
