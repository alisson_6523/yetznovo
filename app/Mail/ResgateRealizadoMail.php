<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResgateRealizadoMail extends Mailable {

    use Queueable, SerializesModels;

    public $data;

    public function __construct($data) {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {

        return $this->view('sistema.emails.resgate-realizado')
                    ->from('atendimento@yetzcards.com.br', 'Atendimento YetzCards')
                    ->subject('[YETZCARDS] Resgate (' . $this->data['resgate']['cod_resgate'] . ') Realizado')
                    ->with([ 'dados' => $this->data ]);

    }
}
