<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Desejado;

class DesejadosController extends Controller
{
    //

    public function atualiza(Request $request)
    {
        $usuario = \Auth::guard('user')->user();
        $cod_produto = $request->input('cod_produto');


        $desejado = Desejado::where([
            ['cod_usuario', '=', $usuario->id],
            ['cod_produto', '=', $cod_produto]
        ])->first();

        if (!$desejado) :

            try {
                Desejado::create([
                    'cod_usuario' => $usuario->id,
                    'cod_produto' => $cod_produto
                ]);
                return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
            } catch (Exception $e) {

                return response()->json(['erro' => true, 'mensagem' => 'Houve algum erro']);
            }

        else:

            $desejado->delete();
            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);

        endif;

    }
}
