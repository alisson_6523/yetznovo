<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\CampanhaProtocoloTemporario;
use App\Models\ClientePdv;
use App\Models\UsuarioPontosCampanhaTemporario;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CargaDePontosTemporariosController extends Controller {

    public function recebeCarga(Request $request) {
        try {

            $dados = $this->lerArquivo($request->file('arquivo'), $request->cod_campanha); // envia para função ler o arquivo, tratar os erros e retornar estatisticas

            if (!$dados)
                return response()->json(['erro' => true, 'mensagem' => 'Arquivo incorreto!']);

            $view = view('sistema.cargas.carga')->with([
                'campanha'     => Campanha::find($request->cod_campanha),
                'estatistica'  => $dados['estatistica'],
                'usuarios'     => $dados['arrUsuarios'],
                'nome_arquivo' => $request->file('arquivo')->getClientOriginalName(),
                'temporaria'   => true
            ])->render();

            // Salva o arquivo no storage para recuperar quando salvar
            if ($dados['estatistica']->erros == 0)
                \Storage::disk('local')->putFileAs('arquivos', $request->file('arquivo'), $request->file('arquivo')->getClientOriginalName());

            return response()->json(['html' => $view, 'dados' => $dados]);
        } catch (\Exception $e) {
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function downloadCarga(Request $request) {
        $assetPath = Storage::cloud()->temporaryUrl('cargas-temporarias/' . $request->arquivo, \Carbon\Carbon::now()->addMinutes(1));

        $header = [
            'Content-Type' => Storage::cloud()->mimeType('cargas-temporarias/' . $request->arquivo),
            'Content-Disposition' => 'inline; filename="'.$request->nome.'"',
            'Content-Length' => Storage::cloud()->size('cargas-temporarias/' . $request->arquivo)
        ];

        return response()->make(file_get_contents($assetPath), 200, $header);
    }


    public function salvarCarga(Request $request) {

        $estatistica = json_decode($request->dados)->estatistica;

        $nome_unico = uniqid(date('HisYmd')) . '.xlsx'; // Monta nome único para o arquivo

        // Busca o arquivo no storage local, move pro s3 e deleta do local
        $arquivo = \Storage::disk('local')->get('arquivos/' . $request->nome_arquivo);
        \Storage::disk('s3')->put('cargas-temporarias/' . $nome_unico, $arquivo, 'private');
        \Storage::disk('local')->delete('arquivos/' . $request->nome_arquivo);

        try {

            CampanhaProtocoloTemporario::create([
                'cod_campanha'    => $request->cod_campanha,
                'cod_adm_criador' => \Auth::user()->id,
                'tipo_protocolo'  => 2, // credito
                'protocolo'       => date('Ymdhis') . 'A' . str_pad(\Auth::user()->id, 3, "0", STR_PAD_LEFT),
                'total_pontos'    => $estatistica->total_pontos,
                'arquivo'         => $request->nome_arquivo,
                'hash_arquivo'    => $nome_unico,
                'dados'           => $request->dados,
                'titulo'          => $request->titulo
            ]);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e) {
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function excluirCarga(Request $request) {
        try {
            CampanhaProtocoloTemporario::find($request->cod_protocolo)->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e) {
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function efetivarCarga(Request $request) {
        try {
            ini_set('max_execution_time', 180); //3 minutes
            $protocolo = CampanhaProtocoloTemporario::find($request->cod_protocolo);
            $campanha = Campanha::find($protocolo->cod_campanha);

            if(empty($protocolo->data_efetivacao)) {
                DB::beginTransaction();

                $dados = json_decode($protocolo->dados)->arrUsuarios;

                foreach ($dados as $dado) {

                    $usuario = $campanha->usuarios_campanha->where('id', $dado->id)->first();

                    if (!empty($usuario)) {
                        $usuario->cod_pdv = ClientePdv::where('registro', $dado->registro)->first()->cod_pdv;
                        $usuario->ilha = $dado->ilha;
                        $usuario->cargo = $dado->cargo;
                        $usuario->coordenador = $dado->coordenador;
                        $usuario->email_coordenador = $dado->email_coordenador;
                        $usuario->supervisor = $dado->supervisor;
                        $usuario->email_supervisor = $dado->email_supervisor;

                        UsuarioPontosCampanhaTemporario::create([
                            'protocolo_id' => $protocolo->cod_protocolo,
                            'usuario_id'   => $usuario->id,
                            'campanha_id'  => $campanha->cod_campanha,
                            'pontos'       => floatval($dado->carga)
                        ]);

                        $usuario->save();
                    }


                }

                $protocolo->data_efetivacao = Carbon::now();
                $protocolo->cod_adm_efetivador = \Auth::user()->id;
                $protocolo->save();

                DB::commit();
                return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
            }

            return response()->json(['erro' => true, 'mensagem' => 'Carga já efetivada']);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage(), 'linha' => $e->getLine()]);
        }
    }


    private function lerArquivo($arquivo, $cod_campanha) {
        try {
            $reader    = IOFactory::createReader(ucfirst($arquivo->getClientOriginalExtension()));
            $planilha  = $reader->load($arquivo);
            $aba_ativa = $planilha->getActiveSheet();
            $campanha  = Campanha::find($cod_campanha);
//            $campanha_1  = Campanha::find(50);
//            $arr_ids   = [];

            if ($aba_ativa->getCellByColumnAndRow(1, 1)->getValue() != 1)
                return false;

            $arrUsuarios = [];
            $estatistica = new \stdClass();

            $estatistica->tipo_carga          = $aba_ativa->getCellByColumnAndRow(1, 1)->getValue();
            $estatistica->erros               = 0;
            $estatistica->atualizados         = 0;
            $estatistica->nao_encontrados     = 0;
            $estatistica->mudanca_cargo       = 0;
            $estatistica->mudanca_pdv         = 0;
            $estatistica->mudanca_ilha        = 0;
            $estatistica->mudanca_coordenador = 0;
            $estatistica->mudanca_supervisor  = 0;
            $estatistica->total_pontos        = 0;

            $num_maior_linha = $aba_ativa->getHighestDataRow() + 1;

            ini_set('max_execution_time', 0);

            for ($i = 5; $i < $num_maior_linha; $i++) {

                if ($aba_ativa->getCellByColumnAndRow(2, $i)->getValue() == null && ($i + 1 == $num_maior_linha))
                    break;

                $usuario = new \stdClass();

                $usuario->registro          = trim($aba_ativa->getCellByColumnAndRow(2, $i)->getValue());
                $usuario->id                = trim($aba_ativa->getCellByColumnAndRow(3, $i)->getValue()); // id
                $usuario->nome              = trim(preg_replace('/_/', ' ', $aba_ativa->getCellByColumnAndRow(4, $i)->getValue()));
                $usuario->cargo             = trim($aba_ativa->getCellByColumnAndRow(5, $i)->getValue()); // avisar alteração de cargo
                $usuario->coordenador       = trim(preg_replace('/_/', ' ', $aba_ativa->getCellByColumnAndRow(6, $i)->getValue()));
                $usuario->email_coordenador = trim($aba_ativa->getCellByColumnAndRow(7, $i)->getValue());
                $usuario->supervisor        = trim(preg_replace('/_/', ' ', $aba_ativa->getCellByColumnAndRow(8, $i)->getValue()));
                $usuario->email_supervisor  = trim($aba_ativa->getCellByColumnAndRow(9, $i)->getValue());
                $usuario->ilha              = trim($aba_ativa->getCellByColumnAndRow(10, $i)->getValue());
                $usuario->carga             = $aba_ativa->getCellByColumnAndRow(11, $i)->getValue();
                $usuario->referencia        = trim($aba_ativa->getCellByColumnAndRow(12, $i)->getValue());
                $usuario->erro              = FALSE;
                $usuario->mudanca_pdv       = FALSE;
                $usuario->msg_erros         = "";

                $dados = $this->verificaUsuario($usuario, $estatistica, $campanha); // recebe o usuario verificado com a estatistica atualizada

                $usuario     = $dados['usuario'];
                $estatistica = $dados['estatistica'];

                array_push($arrUsuarios, $usuario);

                //Buscar os ids do povo da 30h para 30h2
//                array_push($arr_ids, $campanha->usuarios_campanha->where('login', $campanha_1->usuarios_campanha->where('id', $usuario->id)->first()->login)->first()->id);

            }

//            print_r($arr_ids);
//            die();

            ini_set('max_execution_time', 60);

            return ['estatistica' => $estatistica, 'arrUsuarios' => $arrUsuarios];
        } catch (\Exception $e) {
            return response()->json([
                "error" => $e->getCode(),
                "message" => $e->getMessage(),
                "file" => $e->getFile(),
                "line" => $e->getLine()
            ]);
        }

    }


    private function verificaUsuario($usuario_novo, $estatistica, Campanha $campanha) {
        $pdv = $campanha->campanha_pdvs()->where('registro', $usuario_novo->registro)->get()->first();
        $usuarios_campanha = $campanha->usuarios_campanha;
        $estatistica->total_pontos += $usuario_novo->carga;

        if (empty($pdv)) {
            $usuario_novo->erro = TRUE;
            $estatistica->erros++;
        } else {

            // verifica se existe
            $usuario = $usuarios_campanha->where('id', $usuario_novo->id)->first();

            if (!empty($usuario)) { // atualiza usuario
                $estatistica->atualizados++;

                $usuario_novo->nome = $usuario->nome_usuario;

                if ($usuario->cargo != $usuario_novo->cargo) // Verifica se vai mudar o cargo
                    $estatistica->mudanca_cargo++;

                if ($usuario->cod_pdv != $pdv->cod_pdv) {      // Verifica se vai mudar o pdv
                    $estatistica->mudanca_pdv++;
                    $usuario_novo->mudanca_pdv = TRUE;
                }

                if ($usuario->ilha != $usuario_novo->ilha)   // Verifica se vai mudar a ilha
                    $estatistica->mudanca_ilha++;

                if ($usuario->email_coordenador != $usuario_novo->email_coordenador)
                    $estatistica->mudanca_coordenador++;

                if ($usuario->email_supervisor != $usuario_novo->email_supervisor)
                    $estatistica->mudanca_supervisor++;

            } else { // cria novo
                $estatistica->nao_encontrados++;
                $usuario_novo->erro = TRUE;
            }

        }

        return ['estatistica' => $estatistica, 'usuario' => $usuario_novo];
    }

}
