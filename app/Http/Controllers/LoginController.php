<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Models\Campanha;
use App\Models\User;
use App\Http\MongoDb;
use Auth;
use Carbon\Carbon;
use App\Notifications\ResetPassword;
use Illuminate\Support\Facades\Redirect;
use Hash;

class LoginController extends Controller
{
    public function buscaChave()
    {
        $c = Request::input('chave', '##-######');

        $chave = Campanha::where('chave', '=', $c)->first();


        if ($chave) :

            return Response::json(['chave' => $chave], 200);

        else :
            return Response::json(['erro' => 'chave não encontrada'], 401);

        endif;
    }

    public function primeiroAcesso()
    {
        $chave    = Request::input('chave', '##-######');
        $campanha = Campanha::where('chave', '=', $chave)->first();

        $user_campanha = $campanha->usuarios_campanha->where('login', Request::input('login'))->first();

        if (!empty($user_campanha)) {
            $usuario = User::find($user_campanha->id);

            if (empty($usuario))
                return Response::json(['erro' => 'Usuário não encontrado.'], 401);

            return Response::json(['usuario' => $usuario, 'cod_campanha' => $campanha->cod_campanha], 200);

        } else
            return Response::json(['erro' => 'envie novamente a chave do cliente.'], 401);

    }

    public function login()
    {
        $chave = Request::input('chave');
//        $params = Request::only('login', 'password');
        $campanha = Campanha::where('chave', '=', $chave)->first();

        if(empty($campanha->usuarios_campanha->where('login', strtoupper(Request::input('login')))->where('password', sha1(Request::input('password')))->first()))
            return Response::json(['erro' => 'Usuário não encontrado.'], 403);

        $usuario = User::find($campanha->usuarios_campanha->where('login', strtoupper(Request::input('login')))->where('password', sha1(Request::input('password')))->first()->id);

        if(!empty($usuario)){
            Auth::guard('user')->login($usuario);

            if(Auth::check()){
                Request::session()->put('campanha', $campanha);
                \Session::put('sessao', \Ramsey\Uuid\Uuid::uuid4()->toString());

                $mongo = new MongoDb();

                $detect = new \Mobile_Detect;

                $os = null;

                if($detect->isTablet()) {
                    $dispositivo = 'tablet';
                } else if($detect->isMobile()) {
                    $dispositivo = 'celular';

                    if($detect->isiOS() && $detect->isSafari() && $detect->isChrome()) {
                        $os = 'ios';
                    } else if($detect->isAndroidOS() && $detect->isSafari() && $detect->isChrome()) {
                        $os = 'android';
                    }
                } else {
                    $dispositivo = "computador";
                }

                $documento = [
                    'dispositivo' => $dispositivo,
                    'os'          => $os
                ];

                $mongo->salvarDados($documento, '.logins');

                return Response::json(['usuario' => Auth::guard('user')->user()], 200);
            }

            return Response::json(['erro' => 'As credenciais não conferem.'], 403);
        }

        return Response::json(['erro' => 'Usuário não encontrado.'], 403);
    }

    public function logout()
    {

        $mongo = new MongoDb();

        $mongo->salvarDadosLogout();

        Auth::guard('user')->logout();

        return redirect('/');
    }

    public function cadastro()
    {
        $params = Request::all();
        $chave = Request::input('chave');
        $campanha = Campanha::where('chave', '=', $chave)->first();

        $usuario = User::find($campanha->usuarios_campanha->where('login', Request::input('login'))->first()->id);

        if ($usuario) :

            $usuario->status = 1;
            $usuario->fill($params);

            try {

                $usuario->data_cadastro = Carbon::now();
                $usuario->save();

                \Auth::loginUsingId($usuario->id);

                \Session::put('sessao', \Ramsey\Uuid\Uuid::uuid4()->toString());
                Request::session()->put('campanha', $campanha);

                $mongo = new MongoDb();

                $mongo->atualizarUsuario($usuario);

                $detect = new \Mobile_Detect;
                $dispositivo = "";
                $os          = "";

                if($detect->isTablet()) {
                    $dispositivo = 'tablet';
                } else if($detect->isMobile()) {
                    $dispositivo = 'celular';

                    if($detect->isiOS() && $detect->isSafari() && $detect->isChrome()) {
                        $os = 'ios';
                    } else if($detect->isAndroidOS() && $detect->isSafari() && $detect->isChrome()) {
                        $os = 'android';
                    }
                } else {
                    $dispositivo = "computador";
                }

                $documento = [
                    'dispositivo' => $dispositivo,
                    'os'          => $os
                ];

                $mongo->salvarDados($documento, '.logins');

                return Response::json(['usuario' => $usuario], 200);
            } catch (\Exception $e) {
                return Response::json(['erro' => 'Erro ao atualizar os dados do usuário'], 500);
            }


        endif;
    }


    public function recuperaSenha()
    {
        // verifica login/campanha
        $chave = strtoupper(Request::input('chave'));
        $login = strtoupper(Request::input('login'));

        $campanha = Campanha::where('chave', '=', $chave)->first();

        if (!$campanha) :
            return Response::json(['erro' => true, 'msg' => 'Campanha não encontrada'], 404);

        endif;

        $usuario = $campanha->usuarios_campanha()->where('login', '=', $login)->first();

        if (!$usuario) :
            return Response::json(['erro' => true, 'msg' => 'Usuário não pertence à campanha'], 404);

        endif;

        $notification = new ResetPassword($usuario, app('auth.password.broker')->createToken($usuario));

        $usuario->notify($notification);

        return Response::json(['erro' => false, 'msg' => 'Sucesso'], 201);
    }

    public function novaSenha($token)
    {
        $reset = \DB::table('password_resets')
            ->where('token', '=', sha1($token))
            ->where('created_at', '>', Carbon::now()->subHours(2))
            ->first();

        if($reset):

            return view('vendor.mail.resetar-senha')->with(['token' => $token]);


        else:
            return redirect('/');

        endif;


    }

    public function confirmaNovaSenha()
    {
        $token = Request::input('token');
        $senha = Request::input('password');
        $confirma_senha = Request::input('password_confirm');

        if($senha != $confirma_senha)
            return Redirect::back()->withErrors(['msg' => 'As senhas não são iguais']);

        $reset = \DB::table('password_resets')
            ->where('token', '=', sha1($token))
            ->where('created_at', '>', Carbon::now()->subHours(2))
            ->first();

        if(!empty($reset)){
            $usuario = User::find($reset->email);

            if(!empty($usuario)){
                $usuario->password = $senha;
                $usuario->save();

//                Auth::guard('user')->loginUsingId($usuario->id);

                \DB::table('password_resets')
                    ->where('token', '=', sha1($token))->delete();

                return redirect()->route('login')->with('senha-reset', true);
            }

            return Redirect::back()->withErrors(['msg' => 'Usuário não encontrado: o token informado é inválido']);

        } else {
            return Redirect::back()->withErrors(['msg' => 'O token informado não existe ou é inválido']);
        }

    }


    public function editaUsuario()
    {
        $cod_usuario = Request::input('cod_usuario');

        $usuario = Auth::guard('user')->user();

        $usuario->fill(Request::all());

        $usuario->notificacao_celular = Request::has('notificacao_celular');


        if(Request::has('foto_perfil') && Request::input('foto_perfil') != null):
            $usuario->foto_usuario = StoreBase64(Request::input('foto_perfil'), 600, '/usuarios');

        endif;

        $usuario->update();

        $mongo = new MongoDb();
        $mongo->atualizarUsuario($usuario);

        return Redirect::back()->with(['msg' => 'sucesso']);

    }


    public function mudarSenha(){
        if(Request::input('senha') != Request::input('confirma_senha'))
            return Redirect::back()->withErrors(['msg' => 'As senhas não são iguais']);

        try{
            $usuario = \Auth::user();

            $usuario->password = Request::input('senha');
            $usuario->save();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }
}
