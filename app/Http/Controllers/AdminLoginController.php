<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/sistema';

    // Mudando a coluna de autenticação para a 'login'
    public function username() {
        return 'login';
    }

    public function login(Request $request){
        $this->validateLogin($request);

        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }

        $credentials = $request->only('login', 'password');

        $usuario = Admin::where('login', $request->login)->first();

        if(!empty($usuario->ip) && getMeuIp() != $usuario->ip)
            return $this->sendFailedLoginResponse($request);

        if(!Auth::guard('admin')->attempt($credentials))
            return $this->sendFailedLoginResponse($request);

        $this->incrementLoginAttempts($request);

        return redirect()->route('sistema');
    }

}
