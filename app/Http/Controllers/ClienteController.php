<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ClienteController extends Controller {


    public function index(){

        $clientes = (!empty(\Auth::user()->cliente_id) ? Cliente::where('id', \Auth::user()->cliente_id)->orderBy('nome_fantasia', 'ASC')->get() : Cliente::withTrashed()->orderBy('nome_fantasia', 'ASC')->get());

        return view('sistema.clientes.clientes')->with('clientes', $clientes);
    }


    public function cadastrar(Request $request){

        $this->validarCadastro($request->all())->validate();

        try{

            Cliente::create(array_merge($request->input(), [
                'logo'      => ($request->hasFile('arquivo') ? limpaString($request->file('arquivo')->getClientOriginalName(), '.') : null),
                'logo_hash' => ($request->hasFile('arquivo') ? StoreFile($request->file('arquivo'), 'logo-cliente') : null)
            ]));

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function editar(Request $request){
        $cliente = Cliente::withTrashed()->find($request->id);

        $this->validarEdicao($request->all(), $cliente)->validate();

        try{
            if($request->hasFile('arquivo')){
                if ($cliente->logo_hash != null)
                    \Storage::disk('s3')->delete($cliente->logo_hash);
                $request->request->add([
                    'logo'      => limpaString($request->file('arquivo')->getClientOriginalName(), '.'),
                    'logo_hash' => StoreFile($request->file('arquivo'), 'logo-cliente')
                ]);
            }

            $cliente->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function excluir(Request $request){
        try{
            $cliente = Cliente::find($request->id);

            $cliente->cliente_pdvs()->delete(); // exclui logicamente os pdvs
            $cliente->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function ativar(Request $request){
        try{
            $cliente = Cliente::withTrashed()->find($request->id);

            $cliente->cliente_pdvs()->restore(); // restaura logicamente os pdvs
            $cliente->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function filtrar(Request $request, Cliente $cliente){

        $cliente = $cliente->newQuery();

        $cliente->where('razao_social', 'LIKE', '%' . $request->busca . '%')
                ->where('nome_fantasia', 'LIKE', '%' . $request->busca . '%');

        $cliente->with('campanhas')->with('cliente_pdvs');

        return $cliente->orderBy('razao_social', 'ASC')->withTrashed()->get();

    }


    public function listarCampanhas(Request $request){
        try{

            $cliente = Cliente::findOrFail($request->cod_cliente);

            return response()->json(['erro' => false, 'campanhas' => $cliente->campanhas]);

        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function listarPdvs(Request $request){
        try{

            $cliente = Cliente::findOrFail($request->id);

            $pdvs = $cliente->cliente_pdvs;


            foreach($pdvs as $p):
                $p->ilhas = $p->users->groupBy('ilha')->keys();
            endforeach;

            return response()->json([
                'erro' => false,
                'pdvs' => $pdvs,
                'campanhas' => $cliente->campanhas
                ]);

        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    private function validarCadastro(array $data){
        return Validator::make($data, [
            'cnpj'         => ['unique:cliente', 'string']
        ]);
    }


    private function validarEdicao(array $data, Cliente $cliente) {
        return Validator::make($data, [
            'cnpj'         => [Rule::unique('cliente')->ignore($cliente->id), 'string']
        ]);
    }


}
