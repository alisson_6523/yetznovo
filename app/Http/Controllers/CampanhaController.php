<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\CampanhaCategorium;
use App\Models\CampanhaPdv;
use App\Models\CampanhaProduto;
use App\Models\CategoriaProduto;
use App\Models\Produto;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class CampanhaController extends Controller {

    public function index(){
        switch (\Auth::user()->perfil_id){
            case 1:
                $campanhas = Campanha::withTrashed()->orderBy('nome_campanha', 'ASC')->get();
                break;
            case 2:
            case 3:
                $campanhas = \Auth::user()->cliente->campanhas()->whereNotIn('cod_campanha', [63, 64])->orderBy('nome_campanha', 'ASC')->get();
                break;
            case 4:
                $campanhas = Campanha::whereIn('cod_campanha', \Auth::user()->campanhas->pluck('campanha_id'))->orderBy('nome_campanha', 'ASC')->get();
                break;
            default:
                $campanhas = Campanha::withTrashed()->orderBy('nome_campanha', 'ASC')->get();
        }

        return view('sistema.campanhas.campanhas')->with('campanhas', $campanhas);
    }


    public function cadastrar(Request $request){

        $this->validarCadastro($request->all())->validate();

        try{

            $campanha = Campanha::create(array_merge($request->all(), [
                'dt_inicio'        => Carbon::createFromFormat('d/m/Y', $request->dt_inicio),
                'dt_fim'           => Carbon::createFromFormat('d/m/Y', $request->dt_fim),
                'regulamento'      => limpaString($request->file('arquivo')->getClientOriginalName(), '.'),
                'regulamento_hash' => StoreFile($request->file('arquivo'), 'regulamento-campanha', 'private'),
                'logo'             => ($request->hasFile('imagem') ? limpaString($request->file('imagem')->getClientOriginalName()) : null),
                'logo_hash'        => ($request->hasFile('imagem') ? StoreFile($request->file('imagem'), 'logo-campanha') : null),
                'autenticacao'     => 1
            ]));

            $pdvs = json_decode($request->pdvs);

            foreach($pdvs as $cod_pdv){
                CampanhaPdv::create([
                    'cod_campanha' => $campanha->cod_campanha,
                    'cod_pdv'      => $cod_pdv,
                    'data_vinculo' => Carbon::now()
                ]);
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function listarIlhas(Request $request){
        $campanha = Campanha::find($request->input('cod_campanha'));

        $pdv_ilhas = $campanha->campanha_pdvs()->with(['users' => function($q){ $q->groupBy('cod_pdv', 'ilha'); }])->get();

        return response()->json(['erro' => false, 'pdvs_ilhas' => $pdv_ilhas]);
    }


    public function pdvs(Request $request)
    {
        $campanha = Campanha::find($request->input('cod_campanha'));

        return response()->json(['erro' => false, 'campanha' => $campanha, 'pdvs' => $campanha->campanha_pdvs]);
    }


    public function editar(Request $request){
        $campanha = Campanha::withTrashed()->find($request->cod_campanha);

        $this->validarEdicao($request->all(), $campanha)->validate();

        try{

            if($request->hasFile('arquivo')){
                if(\Storage::disk('s3')->delete($campanha->regulamento_hash)){
                    $request->request->add([
                        'regulamento'      => limpaString($request->file('arquivo')->getClientOriginalName(), '.'),
                        'regulamento_hash' => StoreFile($request->file('arquivo'), 'regulamento-campanha')
                    ]);
                }
            }

            if($request->hasFile('imagem')){
                if(!empty($campanha->logo_hash) && \Storage::disk('s3')->delete($campanha->logo_hash)){
                    $request->request->add([
                        'logo'      => limpaString($request->file('imagem')->getClientOriginalName(), '.'),
                        'logo_hash' => StoreFile($request->file('imagem'), 'logo-campanha')
                    ]);
                } else {
                    $request->request->add([
                        'logo'      => limpaString($request->file('imagem')->getClientOriginalName(), '.'),
                        'logo_hash' => StoreFile($request->file('imagem'), 'logo-campanha')
                    ]);
                }
            }

            $campanha->update(array_merge($request->all(), [
                'dt_inicio' => Carbon::createFromFormat('d/m/Y', $request->dt_inicio),
                'dt_fim'    => Carbon::createFromFormat('d/m/Y', $request->dt_fim)
            ]));

            // Deletar PDV's
            foreach(json_decode($request->removerPdvs) as $removePdv){
                $campanha_pdv = CampanhaPdv::where('cod_campanha', $campanha->cod_campanha)->where('cod_pdv', $removePdv)->first();
                $campanha_pdv->delete();
            }

            // Adicionar novos PDV's
            foreach(json_decode($request->adicionarPdvs) as $addPdv){
                CampanhaPdv::create([
                    'cod_campanha' => $campanha->cod_campanha,
                    'cod_pdv'      => intval($addPdv)
                ]);
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function excluir(Request $request){
        try{

            Campanha::find($request->cod_campanha)->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function ativar(Request $request){
        try{

            Campanha::withTrashed()->find($request->cod_campanha)->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function adicionarProdutoLote(Request $request){
        try{
            $campanha  = Campanha::find($request->id);
            $categoria = CategoriaProduto::find($request->cod_categoria);

            if(empty($campanha->campanha_categorias()->where('cod_categoria', $categoria->cod_categoria_produto)->first())) {
                CampanhaCategorium::create([
                    'cod_campanha'  => $campanha->cod_campanha,
                    'cod_categoria' => $categoria->cod_categoria_produto
                ]);
            }

            foreach($categoria->categoria_unica_produtos as $produto){
                $item = CampanhaProduto::firstOrNew([
                    'cod_campanha' => $campanha->cod_campanha,
                    'cod_produto'  => $produto->cod_produto
                ]);
                $item->save();
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function adicionarProduto(Request $request){
        try{
            $campanha  = Campanha::find($request->id);
            $produto   = Produto::find($request->cod_produto);

            if(empty($campanha->campanha_categorias()->where('cod_categoria', $produto->cod_categoria_produto)->first())) {
                CampanhaCategorium::create([
                    'cod_campanha'  => $campanha->cod_campanha,
                    'cod_categoria' => $produto->cod_categoria_produto
                ]);
            }

            CampanhaProduto::create([
                'cod_campanha' => $campanha->cod_campanha,
                'cod_produto'  => $produto->cod_produto
            ]);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function removerProdutoLote(Request $request){
        try{
            $campanha  = Campanha::find($request->id);
            $categoria = CategoriaProduto::find($request->cod_categoria);

            foreach($categoria->categoria_unica_produtos as $produto){
                CampanhaProduto::where('cod_campanha', $campanha->cod_campanha)->where('cod_produto', $produto->cod_produto)->delete();
            }

            CampanhaCategorium::where('cod_campanha', $campanha->cod_campanha)->where('cod_categoria', $categoria->cod_categoria_produto)->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function removerProduto(Request $request){
        try{
            $campanha  = Campanha::find($request->id);
            $produto   = Produto::find($request->cod_produto);

            CampanhaProduto::where('cod_campanha', $campanha->cod_campanha)->where('cod_produto', $produto->cod_produto)->delete();

            if($campanha->produtos()->where('produto.cod_categoria_produto', $produto->cod_categoria_produto)->count() == 0)
                CampanhaCategorium::where('cod_campanha', $campanha->cod_campanha)->where('cod_categoria', $produto->cod_categoria_produto)->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function baixarRegulamento(Request $request){
        $campanha = Campanha::find($request->id);
        $regulamento_hash = $campanha->regulamento_hash;

        $assetPath = Storage::cloud()->temporaryUrl($regulamento_hash, \Carbon\Carbon::now()->addMinutes(1));

        $header = [
            'Content-Type' => Storage::cloud()->mimeType($regulamento_hash),
            'Content-Disposition' => 'inline; filename="'.limpaString($campanha->nome_campanha) . substr($regulamento_hash, -4).'"',
            'Content-Length' => Storage::cloud()->size($regulamento_hash)
        ];

        return response()->make(file_get_contents($assetPath), 200, $header);
    }


    public function filtrar(Request $request, Campanha $campanha){

        $campanha = $campanha->newQuery();

        $campanha->where('nome_campanha', 'LIKE', '%' . $request->busca . '%')->with('cliente', 'campanha_protocolos');

        return $campanha->orderBy('nome_campanha', 'ASC')->get();

    }


    public function filtrarUsuarios(Request $request){

        $campanha          = Campanha::findOrFail($request->cod_campanha);
        $usuarios_campanha = $campanha->usuarios_campanha()->with('pdv')->newQuery();
        $busca             = preg_replace('/[_]/', '\_', $request->busca);

        if($request->has('pdv'))
            $usuarios_campanha->where('cod_pdv', $request->pdv);

        if($request->has('ilha'))
            $usuarios_campanha->where('ilha', 'LIKE', '%' . $request->ilha . '%');

        if($request->has('status')){
            if($request->status == 0)
                $usuarios_campanha->onlyTrashed();
        }else{
            $usuarios_campanha->withTrashed();
        }

        if($request->has('cadastro')){
            if($request->cadastro == 1)
                $usuarios_campanha->where('status', 1);
            else
                $usuarios_campanha->where('status', 0);
        }

        if($request->has('busca'))
            $usuarios_campanha->where('nome_usuario', 'LIKE', '%' . $busca . '%');

        if(!$request->has('pdv') && !$request->has('ilha') && !$request->has('status') && !$request->has('cadastro') && !$request->has('busca'))
            $usuarios_campanha->limit(100);

        return $usuarios_campanha->orderBy('nome_usuario', 'ASC')->get();

    }


    public function filtrarResgates(Request $request){

        $campanha = Campanha::findOrFail($request->cod_campanha);
        $resgates = $campanha->resgates()->with('user.pdv', 'carrinho.itens.produto', 'status_carrinho')->newQuery();
        $busca    = preg_replace('/[_]/', '\_', $request->busca);

        if($request->has('status'))
            $resgates->where('cod_status', $request->status);

        if($request->has('dt_inicio') && !$request->has('dt_fim'))
            $resgates->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $request->dt_inicio));

        if($request->has('dt_fim') && !$request->has('dt_inicio'))
            $resgates->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $request->dt_fim));

        if($request->has('dt_fim') && $request->has('dt_inicio'))
            $resgates->whereBetween('created_at', array(Carbon::createFromFormat('d/m/Y', $request->dt_inicio), Carbon::createFromFormat('d/m/Y', $request->dt_fim)));

        if($request->has('busca'))
            $resgates->where('registro', 'LIKE', '%' . $busca . '%')->orWhere('resgates.nome_usuario', 'LIKE', '%' . $busca . '%');

        return $resgates->orderBy('created_at', 'DESC')->get();

    }


    private function validarCadastro(array $data){
        return Validator::make($data, [
            'chave' => ['unique:campanha', 'string']
        ]);
    }

    private function validarEdicao(array $data, Campanha $campanha){
        return Validator::make($data, [
            'chave' => [Rule::unique('campanha')->ignore($campanha->cod_campanha, 'cod_campanha'), 'string']
        ]);
    }
}
