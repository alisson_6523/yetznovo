<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notifications\TesteYetzNotification;
use Illuminate\Support\Facades\Auth;
use App\Models\Produto;
use App\Models\Cliente;
use App\Models\NotificacaoListum;
use Notification;
use App\Notifications\GrupoNotification;
use App\Models\NotificacaoGrupo;
use App\Search\UserSearch;
use App\Models\ClientePdv;
use App\Models\Campanha;
use App\Models\User;
use App\Models\NotificacaoGeral;

class NotificationController extends Controller
{

    public function index()
    {
        $usuario = Auth::guard('user')->user();

        $variaveis = [
            'usuario' => $usuario

        ];
        return view('notifications.index')->with($variaveis);
    }

    public function visualiza($id)
    {
        $user = Auth::guard('user')->user();

        $notification = $user->notifications->find($id);

        if(empty($notification))
        {
            $notificacaoGeral = NotificacaoGeral::find($id);
            $notificacaoGeral->data_leitura = \Carbon\Carbon::now();
            $notificacaoGeral->save();
            return \response()->json(['data' => $notificacaoGeral->notificacao()]);
        }

        $notification->read_at = \Carbon\Carbon::now();

        $notification->save();

        return $notification;
    }

    public function novaNotificacao()
    {

        $variaveis = [
            'clientes' => Cliente::all()
        ];

        return view('sistema.notificacoes.cadastrar-notificacao')->with($variaveis);
    }

    public function listarCampanhas(Request $request) {
        try{

            $cliente = Cliente::findOrFail($request->id);

            return response()->json(['erro' => false, 'campanhas' => $cliente->campanhas]);

        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function listarIlhas(Request $request) {
        $campanha = Campanha::find($request->input('cod_campanha'));

        $pdv_ilhas = $campanha->campanha_pdvs()->with(['users' => function($q){ $q->groupBy('ilha'); }])->get();

        return response()->json(['erro' => false, 'pdvs_ilhas' => $pdv_ilhas]);
    }

    public function listaNotificacoesGrupo()
    {
        $variaveis = [
            'notificacoes' => (!empty(\Auth::user()->cliente_id) ? NotificacaoGrupo::whereHas('campanha', function($query){ $query->where('cliente_id', \Auth::user()->cliente_id); })->get() : NotificacaoGrupo::all()),
            'clientes'     => Cliente::all()
        ];

        return view('sistema.notificacoes.notificacoes')->with($variaveis);
    }

    public function enviaNotificacao(Request $request, User $usuarios) {
        try {
            $usuarios = $usuarios->newQuery();
            $usuarios->whereIn('ilha', json_decode($request->pdvs_ilhas));

            $grupo = NotificacaoGrupo::create([
                'cod_categoria' => $request->cod_categoria,
                'cod_campanha'  => $request->cod_campanha,
                'adminId'       => Auth::guard('admin')->user()->id,
                'titulo'        => $request->titulo,
                'mensagem'      => $request->mensagem,
                'pdvs'          => $request->pdvs_ilhas,
                'enviados'      => $usuarios->get()->count(),
                'link'          => '' // nao sei o que é isso
            ]);

            // envia a notificação para os usuários
            //Notification::send($usuarios, new GrupoNotification($grupo->titulo, $grupo->mensagem, $grupo->cod_grupo, $grupo->link));


            foreach($usuarios->get() as $u) {
                NotificacaoGeral::create([
                    'usuario_id'        => $u->id,
                    'notificacao_id'    => $grupo->cod_grupo,
                    'tipo_notificacao'  => NOTIFICACAO_GRUPO,
                ]);
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }
}
