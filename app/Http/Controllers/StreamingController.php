<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\UsuarioVideo;
use App\Models\Video;
use App\Models\Vinculo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class StreamingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar() {

        $variaveis = [
            'videos' => (!empty(\Auth::user()->cliente_id) ? Video::whereHas('campanha', function($query){ $query->where('cliente_id', \Auth::user()->cliente_id); })->get() : Video::all()),
            'clientes' => Cliente::all()
        ];

        return view('sistema.streaming.streaming')->with($variaveis);
    }

    public function cadastrar(Request $request) {
        try{

            $tipo = 1;

            if(count(json_decode($request->pdvs_ilhas)) > 0)
                $tipo = 4;
            elseif(!empty($request->cod_campanha))
                $tipo = 3;
            elseif(!empty($request->cod_cliente))
                $tipo = 2;

            $video = Video::create(array_merge($request->all(), [
                'link_video'     => StoreFile($request->file('video'), ($request->file('video')->getClientOriginalExtension() == 'mp3' ? 'audio' : 'videos')),
                'extensao'       => $request->file('video')->getClientOriginalExtension(),
                'foto_capa'      => StoreFile($request->file('capa'), 'video-capa'),
                'data_entrada'   => Carbon::createFromFormat('d/m/Y', $request->dt_entrada),
                'data_saida'     => Carbon::createFromFormat('d/m/Y', $request->dt_saida),
                'tipo_streaming' => $tipo
            ]));


            $pdvs_ilhas = json_decode($request->pdvs_ilhas);
            foreach($pdvs_ilhas as $ilha){
                Vinculo::create([
                    'cod_video' => $video->cod_video,
                    'cod_pdv'   => $ilha->pdv,
                    'ilha'      => $ilha->ilha
                ]);
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function marcarVisto(Request $request){
        try{

            $usuario = UsuarioVideo::firstOrNew(
                ['usuario_id' => \Auth::user()->id, 'video_id'   => $request->video_id],
                ['read_at'    => Carbon::now()]
            );

            $usuario->save();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


}
