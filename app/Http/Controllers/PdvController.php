<?php

namespace App\Http\Controllers;

use App\Models\ClientePdv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PdvController extends Controller {


    public function cadastrar(Request $request){

        $this->validarCadastro($request->all())->validate();

        try{

            ClientePdv::create(array_merge($request->all(), [
                'logo'      => ($request->hasFile('arquivo') ? limpaString($request->file('arquivo')->getClientOriginalName(), '.') : null),
                'logo_hash' => ($request->hasFile('arquivo') ? StoreFile($request->file('arquivo'), 'logo-pdv') : null)
            ]));

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function editar(Request $request){
        $pdv = ClientePdv::find($request->cod_pdv);

        $this->validarEdicao($request->all(), $pdv)->validate();

        try{

            if($request->hasFile('arquivo')){
                if ($pdv->logo_hash != null)
                    \Storage::disk('s3')->delete($pdv->logo_hash);
                $request->request->add([
                    'logo'      => limpaString($request->file('arquivo')->getClientOriginalName(), '.'),
                    'logo_hash' => StoreFile($request->file('arquivo'), 'logo-pdv')
                ]);
            }

            $pdv->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function excluir(Request $request){
        $pdv = ClientePdv::find($request->cod_pdv);

        try{

            $pdv->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function ativar(Request $request){
        $pdv = ClientePdv::withTrashed()->find($request->cod_pdv);

        try{

            $pdv->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function validarCadastro(array $data){
        return Validator::make($data, [
            'registro' => ['unique:cliente_pdv', 'string:30'],
            'cnpj'     => ['unique:cliente_pdv', 'string']
        ]);
    }


    protected function validarEdicao(array $data, ClientePdv $pdv) {
        return Validator::make($data, [
            'cnpj' => [Rule::unique('cliente_pdv')->ignore($pdv->cod_pdv, 'cod_pdv'), 'string']
        ]);
    }

}
