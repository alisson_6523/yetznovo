<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Admin;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use function PHPSTORM_META\type;

class UsuarioController extends Controller
{
    public function adicionarAdministrador(Request $request){
        $this->validarCadastro($request->all())->validate();
        try{
            Admin::create($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function editarAdministrador(Request $request){
        try{
            $admin = Admin::withTrashed()->find($request->id);

//            $this->validarEdicao($request->input(), $admin)->validate(); // nao sei pq da dado inválido
            if(!empty(Admin::withTrashed()->where('login', $request->login)->where('id', '!=', $admin->id)->first()))
                return response()->json(['erro' => true, 'tipo' => 'login', 'mensagem' => 'Este Login já existe!']);

                $admin->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function bloquear(Request $request){
        try{
            User::find($request->id)->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function ativar(Request $request){
        try{
            User::withTrashed()->find($request->id)->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function alterarSenha(Request $request){
        try{
            $usuario = User::withTrashed()->find($request->id);

            if(!empty($usuario)){
                $usuario->password = $request->nova_senha;
                $usuario->save();

                return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
            }

            return response()->json(['erro' => true, 'mensagem' => 'Usuário não encontrado']);

        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function filtrarUsuarios(Request $request, User $usuarios){
        $usuarios = $usuarios->with('pdv')->newQuery();
        $busca    = preg_replace('/[_]/', '\_', $request->busca);

        if($request->has('pdv'))
            $usuarios->where('cod_pdv', $request->pdv);

        if($request->has('ilha'))
            $usuarios->where('ilha', 'LIKE', '%' . $request->ilha . '%');

        if($request->has('status')){
            if($request->status == 0)
                $usuarios->onlyTrashed();
        }else{
            $usuarios->withTrashed();
        }

        if($request->has('cadastro')){
            if($request->cadastro == 1)
                $usuarios->where('status', 1);
            else
                $usuarios->where('status', 0);
        }

        if($request->has('busca'))
            $usuarios->where('nome_usuario', 'LIKE', '%' . $busca . '%')
                     ->orWhere('login', 'LIKE', '%'. $busca .'%');

        if(!$request->has('pdv') && !$request->has('ilha') && !$request->has('status') && !$request->has('cadastro') && !$request->has('busca'))
            $usuarios->limit(100);

        return $usuarios->orderBy('nome_usuario', 'ASC')->get();

    }


    private function validarCadastro(array $data){
        return Validator::make($data, [
            'login' => ['unique:admins', 'string']
        ]);
    }

    private function validarEdicao(array $data, Admin $admin){
        return Validator::make($data, [
            'login' => [Rule::unique('admins')->ignore($admin->id), 'string']
        ]);
    }
}
