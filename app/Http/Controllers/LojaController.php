<?php

namespace App\Http\Controllers;

use App\Mail\ContatoMail;
use App\Models\Banner;
use App\Models\HistoricoMovimentacao;
use Illuminate\Http\Request;
use Auth;
use Cookie;
use App\Models\Carrinho;
use App\Models\Campanha;
use App\Models\CategoriaProduto;
use App\Models\Produto;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Mail;
use function GuzzleHttp\json_decode;
use App\Http\MongoDb;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class LojaController extends Controller
{
    //

    public function index()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');
        // atualizando informações de campanha
        $banners = [];
        session()->put('campanha', Campanha::find($campanha->cod_campanha));


        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        foreach(Banner::where('data_entrada', '<=', Carbon::now())->whereNull('cod_campanha')->orWhere('cod_campanha', $campanha->cod_campanha)->get() as $banner){
            if(!empty($banner->data_saida)){
                if($banner->data_saida >= Carbon::now())
                    array_push($banners, $banner);
            } else
                array_push($banners, $banner);
        }

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'banners' => collect($banners),

        ];

        return view('loja.home')->with($variaveis);
    }

    public function conheca()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');


        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;


        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],

        ];
        return view('loja.conheca')->with($variaveis);
    }

    public function contato()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');


        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;



        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],

        ];
        return view('loja.contato')->with($variaveis);
    }

    public function enviarContato(Request $request){
        try {
            Mail::to('cards@yetz.com.br')->send(new ContatoMail(array_merge($request->input(), [
                'login'    => Auth::user()->login,
                'nome'     => Auth::user()->nome_usuario,
                'pdv'      => Auth::user()->pdv->nome_pdv,
                'campanha' => session('campanha')->nome_campanha,
                'ilha'     => Auth::user()->ilha,
                'email'    => Auth::user()->email,
                'telefone' => Auth::user()->telefone
            ])));

            return ['erro' => false, 'mensagem' => 'Contato enviado!'];
        } catch (\Exception $e) {
            return ['erro' => true, 'mensagem' => $e->getMessage()];
        }
    }

    public function perfil()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');


        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;



        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],

        ];
        return view('loja.perfil')->with($variaveis);
    }

    public function salvarDados(Request $request)
    {
        $documento = $request->all();

        $mongo = new MongoDb();

        $mongo->salvarDados($documento, '.paginas_visitadas');

        return response()->json(['erro' => false, 'documento' => $documento]);

        return response()->json(['erro' => false, 'documento' => []]);
    }

    public function resgates()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        $meses = $usuario->resgates->pluck('mes')->unique();

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],
            'meses' => $meses

        ];
        return view('loja.resgates')->with($variaveis);
    }

    public function vouchers() {
        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos()->where('status_carrinho', '=', 1)->first();

        if (empty($carrinho)) {
            Carrinho::create([
                'cod_usuario' => $usuario->id,
                'status_carrinho' => 1
            ]);
        }

        $meses = $usuario->resgates->pluck('mes')->unique();

//        $resgates = $usuario->resgates()->when()

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
//            'resgates' => ,
            'slides' => [],
            'meses' => $meses

        ];
        return view('loja.voucher')->with($variaveis);
    }

    public function termosCondicoes()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho):
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        $meses = $usuario->resgates->pluck('mes')->unique();

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],
            'meses' => $meses

        ];
        return view('loja.termos-e-condicoes')->with($variaveis);
    }


    public function regulamento() {

        $usuario  = Auth::guard('user')->user();
        $campanha = Campanha::find(session('campanha')->cod_campanha);

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        $meses = $usuario->resgates->pluck('mes')->unique();

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],
            'meses' => $meses

        ];
        return view('loja.regulamento')->with($variaveis);
    }



    public function notificacoes()
    {
        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;


        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],

        ];
        return view('loja.notificacoes')->with($variaveis);
    }


    public function videos()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;


        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],

        ];
        return view('loja.videos')->with($variaveis);
    }



    public function listaDesejos()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        $meses = $usuario->resgates->pluck('mes')->unique();

        $mongo = new MongoDb();

        $p_visitados = $mongo->ultimasCategoriasVisitadas($usuario);

        $visitados = Produto::whereIn('cod_produto', $p_visitados->pluck('id'))->get();


        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'visitados' => $visitados,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],
            'meses' => $meses

        ];
        return view('loja.lista-desejos')->with($variaveis);
    }

    public function movimentacao()
    {

        $usuario = Auth::guard('user')->user();
        $campanha = session('campanha');


        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;


        $datas_filtro = HistoricoMovimentacao::where('cod_usuario', $usuario->id)->orderBy('created_at')->get()->groupBy(function ($item) {
            return $item->created_at->format('m/Y');
        });


        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => null,
            'slides' => [],
            'datas_filtro' => $datas_filtro

        ];
        return view('loja.movimentacao')->with($variaveis);
    }



    public function categoria($link) {

        $campanha = Campanha::find(session('campanha')->cod_campanha);

        if ($link == 'todos') :

            $categoria = new CategoriaProduto();
            $produtos = $campanha->produtos()->whereIn('disponibilidade', [1, 3])->orderBy('nome_produto')->orderBy('valor_produto')->get();

        else :
            $categoria = CategoriaProduto::where('link_categoria', '=', $link)->first();

            $produtos = [];

            foreach($categoria->produtos()->whereIn('disponibilidade', [1, 3])->orderBy('nome_produto')->orderBy('valor_produto')->get() as $produto){
                if(!empty($campanha->produtos()->where('produto.cod_produto', $produto->cod_produto)->first())){
                    array_push($produtos, $produto);
                }
            }

            if (!$categoria) :
                return redirect()->action('LojaController@index');
            endif;
        endif;

        $usuario = Auth::guard('user')->user();

        $usuario->notificacao_streaming = new \Illuminate\Database\Eloquent\Collection();
        $usuario->notificacoes = new \Illuminate\Database\Eloquent\Collection();


        $carrinho = $usuario->carrinhos()->where('status_carrinho', '=', 1)->first();

        if (empty($carrinho)) {
            Carrinho::create([
                'cod_usuario' => $usuario->id,
                'status_carrinho' => 1
            ]);
        }

        $carrinho->valorCarrinho = round($carrinho->produtos->sum('valor'));

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => $categoria,
            'slides' => [],
            'produtos' => collect($produtos)
        ];

        $mongo = new MongoDb();

        $documento = [
            'rota' => \Request::path(),
            'titulo' => $categoria->categoria,
        ];

        $mongo->salvarDados($documento, '.categorias_visitadas');

        return view('loja.categoria')->with($variaveis);
    }

    public function produto($id)
    {
        $campanha     = Campanha::find(session('campanha')->cod_campanha);
        $usuario      = Auth::guard('user')->user();
        $produto      = Produto::find($id);
        $relacionados = [];

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();
        if (!$carrinho) :
            $carrinho = new Carrinho();
            $carrinho->cod_usuario = $usuario->id;
            $carrinho->status_carrinho = 1;
            $carrinho->save();

        endif;

        $carrinho->valorCarrinho = round($carrinho->produtos->sum('valor'));

        foreach($produto->categoria->produtos()->whereIn('disponibilidade', [1,3])->get() as $similar){
            if(!empty($campanha->produtos()->where('produto.cod_produto', $similar->cod_produto)->where('produto.cod_produto', '!=', $produto->cod_produto)->first())){
                array_push($relacionados, $similar);
            }
        }

        $variaveis = [
            'campanha' => $campanha,
            'usuario' => $usuario,
            'carrinho' => $carrinho,
            'popups' => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria' => $produto->categoria,
            'produto' => $produto,
            'slides' => [],
            'relacionados' => collect($relacionados)->take(3)
        ];

        $mongo = new MongoDb();

        $documento = [
            'rota' => \Request::path(),
            'titulo' => $produto->nome_produto . " - R$" . $produto->valor_reais
        ];

        $mongo->salvarDados($documento, '.produtos_visitados');

        return view('loja.produto')->with($variaveis);
    }

    public function visitados($produto)
    {

        $json = Cookie::get('visitados');
        if ($json) :
            $visitados = new Collection(json_decode($json, true));
        else :
            $visitados = new Collection();
        endif;
        $minutos = 60 * 24 * 7;


        // if(!$visitados):
        //     $visitados = collect();
        // endif;
        $find = $visitados->where('cod_produto', $produto->cod_produto)->first();

        if ($find) :
            dd($find);

            return false;

        endif;

        $visitados->push($produto);

        response('visitados')->cookie('visitados', $visitados, $minutos);

        Cookie::queue(Cookie::make('visitados', $visitados, $minutos));
    }

    public function baixarRegulamentoCampanha(Request $request) {
        $campanha         = Campanha::find($request->cod_campanha);
        $regulamento_hash = $campanha->regulamento_hash;

        if (!empty($campanha->regulamentos->firstWhere('ilha', \Auth::user()->ilha)))
            $regulamento_hash = 'regulamento-campanha/' . $campanha->regulamentos->firstWhere('ilha', \Auth::user()->ilha)->regulamento_hash;

        $assetPath = Storage::cloud()->temporaryUrl($regulamento_hash, \Carbon\Carbon::now()->addMinutes(1));

        $header = [
            'Content-Type' => Storage::cloud()->mimeType($regulamento_hash),
            'Content-Disposition' => 'inline; filename="'.limpaString($campanha->nome_campanha) . substr($regulamento_hash, -4).'"',
            'Content-Length' => Storage::cloud()->size($regulamento_hash)
        ];

        return response()->make(file_get_contents($assetPath), 200, $header);
    }
}
