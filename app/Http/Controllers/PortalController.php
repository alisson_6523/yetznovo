<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slide;
use App\Models\CategoriaProduto;
use App\Models\Produto;
use App\Models\Banner;

class PortalController extends Controller
{
    public function index() {

        $campanhaIds = [];

        if(!empty(\Auth::user()->cliente_id)){
            foreach(\Auth::user()->cliente->campanhas as $campanha){
                array_push($campanhaIds, $campanha->cod_campanha);
            }
        }


        $variaveis = [
            'banners'    => (!empty(\Auth::user()->cliente_id) ? Banner::withTrashed()->whereIn('cod_campanha', $campanhaIds)->get() : Banner::withTrashed()->get()),
            'categorias' => CategoriaProduto::withTrashed()->get(),
            'produtos'   => Produto::withTrashed()->get()
        ];

        return view('sistema.portal.portal')->with($variaveis);
    }
}
