<?php

namespace App\Http\Controllers;

use App\Http\MongoDb;
use App\Models\Campanha;
use Illuminate\Http\Request;

class RelatorioController extends Controller
{
	private $mongo;

	public function __construct()
	{
		$this->mongo = new MongoDb();
	}

	public function index()
	{
		return $this->gerarFiltros();
	}

	public function carregarMovimentacao(request $request)
	{
		try {
			return $this->mongo->carregarMovimentacao(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarUsuarios(request $request)
	{
		try {
			return $this->mongo->carregarUsuarios(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarProdutosResgatados(request $request)
	{
		try {
			return $this->mongo->carregarProdutosResgatados(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarUsuariosMensal(request $request)
	{
		try {
			return $this->mongo->carregarUsuariosMensal(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarUsuariosSemanal(request $request)
	{
		try {
			return $this->mongo->carregarUsuariosSemanal(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarUsuariosDiario(request $request)
	{
		try {
			return $this->mongo->carregarUsuariosDiario(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarLogins(request $request)
	{
		try {
			return $this->mongo->carregarLogins(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarPaginasAcessadas(request $request)
	{
		try {
			return $this->mongo->carregarPaginasAcessadas(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarCategoriasAcessadas(request $request)
	{
		try {
			return $this->mongo->carregarCategoriasAcessadas(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function carregarProdutosAcessados(request $request)
	{
		try {
			return $this->mongo->carregarProdutosAcessados(json_decode($request->filtro));
		} catch (\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}

	}

	public function status()
	{
		return $this->mongo->statusMongoDb();
	}

	private function gerarFiltros()
	{
		$cliente = \Auth::guard('admin')->user()->cliente;

		if ($cliente == null) {
			$clientes = \App\Models\Cliente::all();
			$campanhas = \App\Models\Campanha::all();
			$pdvs = \App\Models\ClientePdv::all();
			$filtro = new \stdClass;
		} else {
			$clientes = [$cliente];

            switch (\Auth::user()->perfil_id){
                case 1:
                    $campanhas = Campanha::withTrashed()->orderBy('nome_campanha', 'ASC')->get();
                    break;
                case 2:
                case 3:
                    $campanhas = \Auth::user()->cliente->campanhas()->whereNotIn('cod_campanha', [63, 64])->orderBy('nome_campanha', 'ASC')->get();
                    break;
                case 4:
                    $campanhas = Campanha::whereIn('cod_campanha', \Auth::user()->campanhas->pluck('campanha_id'))->orderBy('nome_campanha', 'ASC')->get();
                    break;
                default:
                    $campanhas = Campanha::withTrashed()->orderBy('nome_campanha', 'ASC')->get();
            }

//			$campanhas = \App\Models\Campanha::where('cliente_id', $cliente->id)->get();
			$pdvs = \App\Models\ClientePdv::where('cliente_id', $cliente->id)->get();
			$filtro = [
			    'usuario.pdv.cliente.id' => $cliente->id,
                'usuario.pdv.cliente.campanhas.id' => [
                    '$in' => $campanhas->pluck("cod_campanha")
                ]
            ];
		}

		return view('sistema.grafico.grafico')->with([
			'clientes' => $clientes,
			'pdvs' => $pdvs,
			'campanhas' => $campanhas,
			'filtro' => json_encode($filtro)
		]);
	}
}
