<?php

namespace App\Http\Controllers;

use App\Mail\ConfirmacaoResgateMail;
use App\Mail\ResgateRealizadoMail;
use App\Models\Campanha;
use App\Models\Carrinho;
use App\Models\HistoricoMovimentacao;
use App\Models\Resgate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Notifications\ResgateNotification;
use App\Notifications\StatusResgateNotification;
use Illuminate\Support\Facades\Storage;
use Notification;
use App\Http\MongoDb;
use App\Search\ResgateSearch;
use App\Models\ItemCarrinho;
use App\Models\StatusResgate;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class ResgateController extends Controller {

    public function resgatar(Request $request){
        try{
            $usuario  = \Auth::user();
            $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->first();
            $campanha = Campanha::find(session('campanha')->cod_campanha);
            $dt_maior = Carbon::now();

            if($campanha->dt_fim <= date('Y-m-d', strtotime(Carbon::now())))
		        return response()->json(['erro' => true, 'mensagem' => 'Esta campanha já foi encerrada!']);

            if($campanha->cod_campanha == 61)
                return response()->json(['erro' => true, 'mensagem' => 'Esta campanha não está efetuando resgates!']);

            if($carrinho->produtos->sum('valor') > $usuario->saldo_usuario)
                return response()->json(['erro' => true, 'mensagem' => 'Pontos insuficientes!']);

            if(count($carrinho->itens) == 0)
                return response()->json(['erro' => true, 'mensagem' => 'Não há produtos no carrinho!']);

            foreach($carrinho->itens as $item){
                $item->data_entregue = ($item->produto->tipo == 1 ? Carbon::now()->addDays(30) : Carbon::now()->addDays(15));
                $item->save();

                if($dt_maior < $item->data_entregue)
                    $dt_maior = $item->data_entregue;

            }

            $resgate = Resgate::create([
                'cod_usuario'   => $usuario->id,
                'cod_carrinho'  => $carrinho->cod_carrinho,
                'cod_status'    => 1, //aguardando
                'cod_campanha'  => $campanha->cod_campanha,
                'registro'      => ((DB::table('resgate')->max('cod_resgate') + 1) . 'c' . substr(md5($usuario->id . Carbon::now()), 0, 8)), // hash modelo antigo
                'valor_resgate' => $carrinho->produtos->sum('valor'),
                'data_entregue' => $dt_maior,
                'valor_pontos'  => $campanha->valor_pontos
            ]);


            HistoricoMovimentacao::create([
                'cod_usuario'           => $usuario->id,
                'cod_tipo_movimentacao' => 1, // RESGATE
                'valor'                 => $carrinho->produtos->sum('valor'),
                'descricao'             => 'Resgate de cartão(ões)',
                'protocolo'             => $resgate->registro,
                'cod_campanha'          => $campanha->cod_campanha
            ]); // referencia e expiracao não tem para tipo 'RESGATE' de acordo com o banco antigo


            // retira credito do usuario e fecha o carrinho
            $pontos_antigo = $usuario->pontos()->where('campanha_id', $campanha->cod_campanha)->first()->pontos;
            $usuario->pontos()->where('campanha_id', $campanha->cod_campanha)->first()->update(['pontos' => $pontos_antigo - $carrinho->produtos->sum('valor')]);
            $carrinho->update(['status_carrinho' => 0]);

            // dados do email
            $data = [
                'usuario'      => $usuario,
                'pdv'          => $usuario->pdv,
                'resgate'      => $resgate,
                'itens'        => $carrinho->produtos->groupBy('cod_produto'),
                'cod_campanha' => $campanha->cod_campanha
            ];

            // Notificação de resgate
            Notification::send($usuario, new ResgateNotification($data));
            Mail::to('resgates@yetzcards.com.br')->send(new ResgateRealizadoMail($data));

            $mongo = new MongoDb();
            $mongo->salvarDadosResgate($resgate, $carrinho->itens);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);

        }catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function listaResgates() {
        // Eu filtro pelo front utilizando o que ta nos inputs
        return view('sistema.resgates.resgates');
    }

    public function filtrar(Request $request)
    {

        $resgates = ResgateSearch::apply($request);

        $variaveis = [
            'resgates' => $resgates
        ];

        return view('sistema.resgates.lista-resgates')->with($variaveis);

    }

    public function statusIndividual(Request $request){
        try{
            $cod_item   = $request->input('cod_item');
            $cod_status = $request->input('cod_status');

            $item         = ItemCarrinho::find($cod_item);
            $cod_campanha = $item->carrinho->resgates->first()->cod_campanha;
            $campanha     = Campanha::find($cod_campanha);

            if($item->cod_status != 4 && $item->cod_status != 5){
                $item->cod_status = $cod_status;
                $item->update();

                if($cod_status == '5'){ // Estorno

                    $user          = $item->carrinho->user;
                    $pontos_antigo = $user->pontos()->where('campanha_id', $cod_campanha)->first()->pontos;
                    $valor_produto = round(($campanha->valor_pontos != 0 ? ($item->produto->valor_reais / $campanha->valor_pontos) : $item->produto->valor_produto));
                    $user->pontos()->where('campanha_id', $cod_campanha)->first()->update(['pontos' => $pontos_antigo + $valor_produto]);

                    HistoricoMovimentacao::create([
                        'cod_usuario'           => $user->id,
                        'cod_tipo_movimentacao' => 3, // ESTORNO,
                        'valor'                 => $valor_produto,
                        'descricao'             => 'Estorno referente ao cancelamento de item do resgate (reg. ' . $item->carrinho->resgates->first()->registro . ')',
                        'protocolo'             => ((DB::table('historico_movimentacao')->max('cod_movimentacao') + 1) . 'e' . substr(md5($user->id . Carbon::now()), 0, 8)),
                        'referencia'            => $item->cod_item, // item que foi estornado
                        'cod_campanha'          => $cod_campanha
                    ]);
                }

                $mongo = new MongoDb();
                $mongo->alterarStatusResgate([$item], $cod_status);

                return response()->json(['erro' => false, 'mensagem' => 'Status Alterado!']);
            }

            return response()->json(['erro' => true, 'mensagem' => 'O Status não foi alterado.']);

        } catch (\Exception $e){
            return ['erro' => true, 'mensagem' => $e->getMessage()];
        }
    }

    public function statusLote(Request $request) {
        $ids = $request->input('itens');
        $cod_status = $request->input('cod_status');

        try {
            $query = ItemCarrinho::whereIn('cod_item', $ids)->whereNotIn('cod_status', [4, 5]);
            $itens = $query->get();

            $query->update(['cod_status' => $cod_status]);

            if($cod_status == '5'){ // Estorno

                foreach($itens as $item){
                    $user          = $item->carrinho->user;
                    $cod_campanha  = $item->carrinho->resgates->first()->cod_campanha;
                    $campanha      = Campanha::find($cod_campanha);
                    $pontos_antigo = $user->pontos()->where('campanha_id', $cod_campanha)->first()->pontos;
                    $valor_produto = round(($campanha->valor_pontos != 0 ? ($item->produto->valor_reais / $campanha->valor_pontos) : $item->produto->valor_produto));
                    $user->pontos()->where('campanha_id', $cod_campanha)->first()->update(['pontos' => $pontos_antigo + $valor_produto]);

                    HistoricoMovimentacao::create([
                        'cod_usuario'           => $user->id,
                        'cod_tipo_movimentacao' => 3, // ESTORNO,
                        'valor'                 => $valor_produto,
                        'descricao'             => 'Estorno referente ao cancelamento de item do resgate (reg. ' . $item->carrinho->resgates->first()->registro . ')',
                        'protocolo'             => ((DB::table('historico_movimentacao')->max('cod_movimentacao') + 1) . 'e' . substr(md5($user->id . Carbon::now()), 0, 8)),
                        'referencia'            => $item->cod_item, // item que foi estornado
                        'cod_campanha'          => $cod_campanha
                    ]);

                }
            }

            $mongo = new MongoDb();
            $mongo->alterarStatusResgate($itens, $cod_status);

            return ['erro' => false, 'mensagem' => $itens->count(). ' pedidos foram atualizados para "'. StatusResgate::lista[$cod_status].'"'];
        } catch (\Exception $e) {
            return ['erro' => true, 'mensagem' => $e->getMessage()];
        }

    }

    public function exportarResgates(Request $request){

        try{

            $resgates = ResgateSearch::apply($request); // filtro

            $linha_inicial = 4;

            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);

            $style = array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ffffff')),
                'fill' => array('fillType' => Fill::FILL_SOLID, 'color' => array('rgb' => 'ED7D31')),
//            'allBorders' => array('borderStyle' => Border::BORDER_MEDIUM, 'color' => array('rgb' => '000000'))
            );

            $agora = date('d-m-Y \á\s H:i:s');

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->mergeCells('B1:O3');
            $spreadsheet->getActiveSheet()->getStyle('B'.$linha_inicial.':Q'.$linha_inicial)->applyFromArray($style);
            $spreadsheet->getActiveSheet()->getStyle('B'.$linha_inicial.':Q'.$linha_inicial)->getBorders()->applyFromArray(['allBorders' => ['borderStyle' => Border::BORDER_MEDIUM, 'color' => ['rgb' => '000000']]]);

            $style = array(
                'alignment' => array(
                    'vertical' => Alignment::VERTICAL_CENTER,
                )
            );

            $spreadsheet->getActiveSheet()->getStyle("B1:Q3")->applyFromArray($style);
            $spreadsheet->getActiveSheet()->setCellValue('B1', 'Arquivo gerado no dia '. $agora);
            $spreadsheet->getActiveSheet()->setCellValue('B' . $linha_inicial, 'CIDADE');
            $spreadsheet->getActiveSheet()->setCellValue('C' . $linha_inicial, 'CAMPANHA');
            $spreadsheet->getActiveSheet()->setCellValue('D' . $linha_inicial, 'PDV');
            $spreadsheet->getActiveSheet()->setCellValue('E' . $linha_inicial, 'ILHA');
            $spreadsheet->getActiveSheet()->setCellValue('F' . $linha_inicial, 'DATA DO RESGATE');
            $spreadsheet->getActiveSheet()->setCellValue('G' . $linha_inicial, 'LOGIN');
            $spreadsheet->getActiveSheet()->setCellValue('H' . $linha_inicial, 'NOME DO USUÁRIO');
            $spreadsheet->getActiveSheet()->setCellValue('I' . $linha_inicial, 'EMAIL DO USUÁRIO');
            $spreadsheet->getActiveSheet()->setCellValue('J' . $linha_inicial, 'COORDENADOR');
            $spreadsheet->getActiveSheet()->setCellValue('K' . $linha_inicial, 'TIPO');
            $spreadsheet->getActiveSheet()->setCellValue('L' . $linha_inicial, 'PRODUTOS');
            $spreadsheet->getActiveSheet()->setCellValue('M' . $linha_inicial, 'REAIS');
            $spreadsheet->getActiveSheet()->setCellValue('N' . $linha_inicial, 'PONTOS');
            $spreadsheet->getActiveSheet()->setCellValue('O' . $linha_inicial, 'REGISTRO');
            $spreadsheet->getActiveSheet()->setCellValue('P' . $linha_inicial, 'Data de entrega');
            $spreadsheet->getActiveSheet()->setCellValue('Q' . $linha_inicial, 'STATUS');

            $cont = $linha_inicial + 1;

            foreach($resgates as $resgate){

                $spreadsheet->getActiveSheet()->setCellValue("B$cont", $resgate->carrinho->resgates->first()->user->pdv->cidade);   //CIDADE
                $spreadsheet->getActiveSheet()->setCellValue("C$cont", $resgate->carrinho->resgates->first()->campanha->nome_campanha);    //CAMPANHA
                $spreadsheet->getActiveSheet()->setCellValue("D$cont", $resgate->carrinho->resgates->first()->user->pdv->nome_pdv); //PDV
                $spreadsheet->getActiveSheet()->setCellValue("E$cont", $resgate->carrinho->resgates->first()->user->ilha); //ILHA
                $spreadsheet->getActiveSheet()->setCellValue("F$cont", date('d/m/Y', strtotime($resgate->carrinho->resgates->first()->created_at)));  //DATA DO RESGATE
                $spreadsheet->getActiveSheet()->setCellValue("G$cont", $resgate->carrinho->resgates->first()->user->login);  //LOGIN
                $spreadsheet->getActiveSheet()->setCellValue("H$cont", $resgate->carrinho->resgates->first()->user->nome_usuario); //NOME DO USUÁRIO
                $spreadsheet->getActiveSheet()->setCellValue("I$cont", $resgate->carrinho->resgates->first()->user->email); //EMAIL DO USUÁRIO
                $spreadsheet->getActiveSheet()->setCellValue("J$cont", $resgate->carrinho->resgates->first()->user->coordenador); //COORDENADOR
                $spreadsheet->getActiveSheet()->setCellValue("K$cont", $resgate->produto->produto_tipo->nome_tipo); //TIPO
                $spreadsheet->getActiveSheet()->setCellValue("L$cont", $resgate->produto->nome_produto);    //PRODUTOS
                $spreadsheet->getActiveSheet()->setCellValue("M$cont", $resgate->produto->valor_reais); //REAIS
                $spreadsheet->getActiveSheet()->setCellValue("N$cont", $resgate->produto->valor_produto); //PONTOS
                $spreadsheet->getActiveSheet()->setCellValue("O$cont", $resgate->carrinho->resgates->first()->registro); //REGISTRO
                $spreadsheet->getActiveSheet()->setCellValue("P$cont", date('d/m/Y', strtotime($resgate->carrinho->resgates->first()->data_entregue)));    //Data de entrega
                $spreadsheet->getActiveSheet()->setCellValue("Q$cont", $resgate->status_resgate->descricao_status);    //STATUS

                $spreadsheet->getActiveSheet()->getStyle("B$cont:Q$cont")->getBorders()->applyFromArray(['allborders' => ['style' => Border::BORDER_THIN, 'color' => ['rgb' => 'DDDDDD']]]);

                $cont++;

            }

            foreach (range('B', 'Q') as $col) {
                $spreadsheet->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);

            ob_start();
            $writer->save('php://output');
            $content = ob_get_contents();
            ob_end_clean();

            Storage::put('filtro_resgates/resgates.xlsx', $content);

            return response()->json(['erro' => false, 'mensagem' => 'Resgates Exportados!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }

    public function baixarResgatesExportados(){
        return Storage::download('filtro_resgates/resgates.xlsx', 'Resgates-'. date('d-m-Y_H-i') . '.xlsx');
    }
}
