<?php

namespace App\Http\Controllers;

use App\Models\PopupIlha;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\NotificacaoPopup;
use App\Models\Cliente;

class PopupController extends Controller {

    public function index() {
        $popups = NotificacaoPopup::all();

        if(!empty(\Auth::user()->cliente_id))
            $popups = NotificacaoPopup::where('cod_cliente', \Auth::user()->cliente_id)->get();

        $variaveis = [
            'popups' =>$popups
        ];

        return view('sistema.popups.popups')->with($variaveis);
    }

    public function cadastrar(Request $request) {
        try {
            $tipo = 1;

            if(count(json_decode($request->pdvs_ilhas)) > 0)
                $tipo = 3;
            elseif(!empty($request->cod_campanha))
                $tipo = 2;

            $popup = NotificacaoPopup::create(array_merge($request->all(), [
                'foto_popup' => StoreFile($request->file('foto'), 'popup'),
                'tipo_popup' => $tipo
            ]));

            $pdvs_ilhas = json_decode($request->pdvs_ilhas);
            foreach($pdvs_ilhas as $ilha){
                PopupIlha::create([
                    'cod_popup' => $popup->cod_popup,
                    'cod_pdv'   => $ilha->pdv,
                    'ilha'      => $ilha->ilha
                ]);
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function editar(Request $request) {
        try {
            $tipo = 1;

            if(count(json_decode($request->pdvs_ilhas)) > 0)
                $tipo = 3;
            elseif(!empty($request->cod_campanha))
                $tipo = 2;

            $popup = NotificacaoPopup::find($request->cod_popup);

            if($request->hasFile('foto') && \Storage::cloud()->delete('popup/' . $popup->foto_popup)){
                $request->request->add([
                    'foto_popup' => StoreFile($request->file('foto'), 'popup'),
                    'tipo_popup' => $tipo
                ]);
            }

            $popup->update($request->all());

            if(count(json_decode($request->pdvs_ilhas)) > 0){
                $pdvs_ilhas = json_decode($request->pdvs_ilhas);

                foreach($popup->popup_ilhas as $remove_ilha){
                    $remove_ilha->delete();
                }

                foreach($pdvs_ilhas as $ilha){
                    PopupIlha::create([
                        'cod_popup' => $popup->cod_popup,
                        'cod_pdv'   => $ilha->pdv,
                        'ilha'      => $ilha->ilha
                    ]);
                }
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function limparPopups(){
        try {
            $popups = NotificacaoPopup::all();

            if(!empty(\Auth::user()->cliente_id))
                $popups = NotificacaoPopup::where('cod_cliente', \Auth::user()->cliente_id)->get();

            foreach($popups as $popup){
                foreach($popup->popup_ilhas as $remove_ilha){
                    $remove_ilha->delete();
                }

                $popup->delete();
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

}
