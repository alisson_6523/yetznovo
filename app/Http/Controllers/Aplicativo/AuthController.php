<?php

namespace App\Http\Controllers\Aplicativo;

use App\Http\MongoDb;
use App\Models\Campanha;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Validator;

class AuthController extends Controller {

    public function cadastrar(Request $request, Route $route){
        $usuario  = User::where('login', '=', $request->login)->first();
        $campanha = Campanha::where('chave', '=', $request->chave)->first();

        if (!empty($usuario)){

            $usuario->status = 1;
            $usuario->fill($request->input());

            try {

                $usuario->data_cadastro = Carbon::now();
                $usuario->save();

                $token = Auth::guard('api')->attempt(['login' => $usuario->login, 'password' => $request->password]);

                Session::put('campanha', $campanha); // Necessário para o Mongo
                Session::put('sessao', \Ramsey\Uuid\Uuid::uuid4()->toString());

                $mongo = new MongoDb();

                $mongo->atualizarUsuario($usuario);

                $documento = [
                    'dispositivo' => 'celular',
                    'os'          => strtolower($request->os)
                ];

                $mongo->salvarDados($documento, '.logins');

                return $this->respondWithToken($token);
            } catch (\Exception $e) {
                Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
                return response()->json(['erro' => 'Erro ao atualizar os dados do usuário'], 500);
            }
        }

        return response()->json(['erro' => 'Erro ao encontrar usuário'], 500);
    }


    public function me(){
        return response()->json(['key' => Auth::guard('api')->user()->getJWTIdentifier()]);
    }


    public function login(Request $request, Route $route){

        if($request->chave == null)
            return response()->json(['error' => 'Informe a chave da campanha'], 401);

        if(!$token = Auth::guard('api')->attempt(['login' => $request->login, 'password' => $request->password]))
            return response()->json(['error' => 'Não Autorizado'], 401);

        try{
            Session::put('campanha', Campanha::where('chave', '=', $request->chave)->first());
            Session::put('sessao', \Ramsey\Uuid\Uuid::uuid4()->toString());

            $mongo = new MongoDb();

            $documento = [
                'dispositivo' => 'celular',
                'os'          => strtolower($request->os)
            ];

            $mongo->salvarDados($documento, '.logins');

            return $this->respondWithToken($token);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['error' => 'Erro ao efetuar o login'], 500);
        }

    }


    protected function respondWithToken($token) {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => Auth::guard('api')->factory()->getTTL() * 60,
            'campanha'     => Session::get('campanha')->cod_campanha,
            'session_id'   => Session::get('sessao')
        ]);
    }



    public function logout(Route $route) {
        $sessao   = $route->parameter('session_id');
        $campanha = Campanha::find($route->parameter('campanha'));

        Session::put('sessao', $sessao); // Necessário para o mongo
        Session::put('campanha', $campanha); // Necessário para o mongo

        try{
            $mongo = new MongoDb();
            $mongo->salvarDadosLogout();

            Auth::guard('api')->logout();

            return response()->json(['mensagem' => 'Deslogado com sucesso!']);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['error' => 'Erro ao efetuar o logout'], 500);
        }
    }



    public function refresh() {
        return $this->respondWithToken(Auth::guard('api')->refresh());
    }


    public function buscaChave(Request $request, Route $route) {
        try{
            $campanha = Campanha::where('chave', '=', $request->chave)->first();

            if(!empty($campanha))
                return response()->json(['campanha' => $campanha], 200);
            else
                return response()->json(['erro' => 'Campanha nao encontrada'], 401);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['error' => 'Erro ao buscar campanha'], 500);
        }
    }



    public function primeiroAcesso(Request $request, Route $route) {
        try{
            $campanha = Campanha::where('chave', '=', $request->chave)->first();

            if(!empty($campanha)) {
                $login   = $request->login;
                $usuario = User::where('login', '=', $login)->first();

                $campanhas = $usuario->pdv->campanhas;

                if($campanhas->contains($campanha))
                    return response()->json(['usuario' => $usuario], 200);
                else
                    return response()->json(['erro' => 'Usuário não pertence a esta campanha.'], 401);

            } else {
                return response()->json(['erro' => 'Envie novamente a chave do cliente.'], 401);
            }
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['error' => 'Erro ao verificar primeiro acesso'], 500);
        }

    }

}
