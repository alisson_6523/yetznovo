<?php

namespace App\Http\Controllers\Aplicativo;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class UsuarioController extends Controller {

    public function atualizar(Request $request, Route $route){
        $usuario = Auth::guard('api')->user();

        try{
            $usuario->fill($request->input());
            $usuario->notificacao_celular = $request->has('notificacao_celular');

            if($request->foto_perfil != null)
                $usuario->foto_usuario = StoreBase64($request->input('foto_perfil'), 600, '/usuarios');

            $usuario->update();

            return response()->json(['mensagem' => "Usuário Atualizado!"], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao atualizar usuário'], 500);
        }
    }


    public function listarCampanhas(Route $route){
        try{
            $usuario = Auth::guard('api')->user();

            return response()->json(['campanhas' => $usuario->campanhas], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao listar campanhas do usuário'], 500);
        }

    }

}
