<?php

namespace App\Http\Controllers\Aplicativo;

use App\Http\MongoDb;
use App\Models\Campanha;
use App\Models\Carrinho;
use App\Models\CategoriaProduto;
use App\Models\Desejado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class ProdutoController extends Controller {

    public function index(Route $route){
        $usuario  = Auth::guard('api')->user();
        $campanha = Campanha::find($route->parameter('campanha'));

        try{

            $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

            if(empty($carrinho))
                $carrinho = Carrinho::create(['cod_usuario' => $usuario->id]);

            return response()->json(['categorias' => $campanha->categorias, 'carrinho' => $carrinho], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao listar categorias da campanha'], 500);
        }

    }


    public function produtosPorCategoria(Route $route){
        $usuario  = Auth::guard('api')->user();
        $campanha = Campanha::find($route->parameter('campanha'));

        try{

            if($route->parameter('categoria') == 'todos'){
                $produtos = $campanha->produtos()->orderBy('nome_produto')->orderBy('valor_produto')->get();
            } else {
                $categoria = CategoriaProduto::where('link_categoria', $route->parameter('categoria'))->first();
                $produtos  = $campanha->produtos()->where('cod_categoria_produto', $categoria->cod_categoria_produto)->whereIn('disponibilidade', [1, 3])->orderBy('nome_produto')->orderBy('valor_produto')->get();
            }

            $carrinho = $usuario->carrinhos()->where('status_carrinho', '=', 1)->first();

            if(empty($carrinho))
                $carrinho = Carrinho::create(['cod_usuario' => $usuario->id]);

            $carrinho->valorCarrinho = $carrinho->produtos->sum('valor_produto');
            Session::put('campanha', $campanha); // Necessário para o Mongo

            $mongo = new MongoDb();

            $documento = [
                'rota' => \Request::path(),
                'titulo' => $categoria->categoria
            ];

            $mongo->salvarDados($documento, '.categorias_visitadas');

            return response()->json(['produtos' => $produtos, 'carrinho' => $carrinho], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao listar produtos dessa categoria'], 500);
        }
    }


    public function atualizarDesejado(Request $request, Route $route){
        $usuario  = Auth::guard('api')->user();

        try{
            $desejado = Desejado::where([['cod_usuario', $usuario->id], ['cod_produto', $request->cod_produto]])->first();

            if(empty($desejado)){
                Desejado::create(['cod_usuario' => $usuario->id, 'cod_produto' => $request->cod_produto]);
                return response()->json(['mensagem' => "Produto Adicionado!"], 200);
            } else {
                $desejado->delete();
                return response()->json(['mensagem' => "Produto Removido!"], 200);
            }
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao listar produtos dessa categoria'], 500);
        }
    }

}
