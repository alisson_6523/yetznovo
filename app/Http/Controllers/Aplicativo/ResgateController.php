<?php

namespace App\Http\Controllers\Aplicativo;

use App\Http\MongoDb;
use App\Mail\ConfirmacaoResgateMail;
use App\Mail\ResgateRealizadoMail;
use App\Models\Campanha;
use App\Models\HistoricoMovimentacao;
use App\Models\Resgate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class ResgateController extends Controller {

    public function resgatar(Route $route){
        $usuario  = Auth::guard('api')->user();
        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->first(); // Carrinho aberto daquele usuario logado
        $campanha = Campanha::find($route->parameter('campanha'));

        if(empty($campanha))
            return response()->json(['erro' => 'Campanha não encontrada!'], 404);

        if(empty($carrinho))
            return response()->json(['erro' => 'Carrinho já resgatado!'], 403);

        if(count($carrinho->itens) == 0) // Quando usa a relação where() em vez do first(), a função empty não identifica que está vazio
            return response()->json(['erro' => 'Não há produto(s) no carrinho!'], 404);

        try{

            $resgate = Resgate::create([
                'cod_usuario'   => $usuario->id,
                'cod_carrinho'  => $carrinho->cod_carrinho,
                'cod_status'    => 1, //aguardando
                'cod_campanha'  => $campanha->cod_campanha,
                'registro'      => ((DB::table('resgate')->max('cod_resgate') + 1) . 'c' . substr(md5($usuario->id . Carbon::now()), 0, 8)), // Hash modelo antigo
                'valor_resgate' => $carrinho->produtos->sum('valor_produto'),
                'data_entregue' => Carbon::now()->addDays(30)
            ]);


            HistoricoMovimentacao::create([
                'cod_usuario'           => $usuario->id,
                'cod_tipo_movimentacao' => 1, // RESGATE,
                'valor'                 => $carrinho->produtos->sum('valor_produto'),
                'descricao'             => 'Resgate de cartão(ões)',
                'protocolo'             => $resgate->registro
            ]); // Referência e expiração não tem para o tipo 'RESGATE' de acordo com o banco antigo


            // Retira o crédito do usuário e fecha o carrinho
            $pontos_antigo = $usuario->pontos_reais;

            $usuario->update(['pontos_reais' => $pontos_antigo - $carrinho->produtos->sum('valor_produto')]);
            $carrinho->update(['status_carrinho' => 0]);


            // Dados do email
            $data = [
                'usuario' => $usuario,
                'pdv'     => $usuario->pdv,
                'resgate' => $resgate,
                'itens'   => $carrinho->produtos->groupBy('cod_produto'),
            ];


            // Notificação de resgate
            Mail::to($usuario->email)->send(new ConfirmacaoResgateMail($data)); // Envia para o usuario
//            Mail::to('resgates@yetzcards.com.br')->send(new ResgateRealizadoMail($data));


            Session::put('campanha', $campanha); // Necessário para o Mongo
            $mongo = new MongoDb();
            $mongo->salvarDadosResgate($resgate, $carrinho->produtos);

            return response()->json(['mensagem' => "Resgate realizado com sucesso!"], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao efetuar resgate'], 500);
        }

    }

}
