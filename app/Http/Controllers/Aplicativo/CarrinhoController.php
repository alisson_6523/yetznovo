<?php

namespace App\Http\Controllers\Aplicativo;

use App\Models\Carrinho;
use App\Models\ItemCarrinho;
use App\Models\Produto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class CarrinhoController extends Controller {

    public function index(Route $route){
        $usuario  = Auth::guard('api')->user();

        try{
            $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->first();

            if(empty($carrinho))
                $carrinho = Carrinho::create(['cod_usuario' => $usuario->id]);

            return response()->json(['frete' => 0, 'itens_carrinho' => $carrinho->itens()->with('produto', 'produto.categoria')->get()], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Carrinho não encontrado'], 500);
        }

    }


    public function adicionar(Request $request, Route $route){
        $usuario  = Auth::guard('api')->user();
        $produto  = Produto::find($request->cod_produto);
        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->first(); // Carrinho aberto daquele usuario logado

        $valor_final = 0; // Soma de valores dos produtos

        if(empty($produto))
            return response()->json(['erro' => 'Produto não encontrado!'], 404);

        try{

            if(empty($carrinho))
                $carrinho = Carrinho::create(['cod_usuario' => \Auth::user()->id]);

            foreach($carrinho->itens as $item){
                $valor_final += $item->produto->valor_produto;
            }

            if(\Auth::user()->pontos_reais < ($valor_final + $produto->valor_produto))
                return response()->json(['erro' => 'Saldo Insuficiente!'], 403);

            $produto->categoria;

            ItemCarrinho::create([
                'cod_produto'  => $produto->cod_produto,
                'cod_carrinho' => $carrinho->cod_carrinho,
                'cod_status'   => 1
            ]);

            return response()->json(['produto' => $produto], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao adicionar produto no carrinho'], 500);
        }

    }


    public function remover(Request $request, Route $route){
        $usuario  = Auth::guard('api')->user();
        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->first(); // Carrinho aberto daquele usuario logado
        $itens    = $carrinho->itens->where('cod_produto', $request->cod_produto);

        if(count($itens) == 0) // Quando usa a relação where() em vez do first(), a função empty não identifica que está vazio
            return response()->json(['erro' => 'Produto(s) não encontrado(s) no carrinho!'], 404);

        try{

            if($request->has('unitario')){
                $itens->last()->delete();
            } else {
                foreach($itens as $item)
                    $item->delete();
            }

            return response()->json(['mensagem' => "Produto(s) Removido(s)!"], 200);
        } catch (\Exception $e){
            Log::channel('yetz-api')->error(stripslashes(explode('Aplicativo', $route->getActionName())[1]) . ":" . $e->getLine() . " => " . $e->getMessage());
            return response()->json(['erro' => 'Erro ao remover produto do carrinho'], 500);
        }
    }

}
