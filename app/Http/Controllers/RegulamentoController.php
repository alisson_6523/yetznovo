<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\Regulamento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RegulamentoController extends Controller {


    public function index(Request $request){
        try{
            $view =  view('sistema.regulamentos.cadastrar-regulamento')->with([
                'ilha'     =>  $request->ilha,
                'campanha' => \App\Models\Campanha::find($request->id)
            ])->render();

            return response()->json(['html'=>$view]);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function cadastrar(Request $request){
        try{
            $campanha = Campanha::findOrFail($request->cod_campanha);

            Regulamento::create([
                'titulo'           => $request->titulo,
                'cod_campanha'     => $campanha->cod_campanha,
                'ilha'             => $request->ilha,
                'regulamento'      => $request->file('arquivo')->getClientOriginalName(),
                'regulamento_hash' => StoreFile($request->file('arquivo'), 'regulamento-campanha')
            ]);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function editar(Request $request){
        try{
            $regulamento = Regulamento::findOrFail($request->id);

            if($request->has('arquivo')){
                \Storage::disk('s3')->delete('regulamento-campanha/' . $regulamento->regulamento_hash);
                $request->request->add([
                    'regulamento'      => $request->file('arquivo')->getClientOriginalName(),
                    'regulamento_hash' => StoreFile($request->file('arquivo'), 'regulamento-campanha')
                ]);
            }

            $regulamento->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function editarRegulamentoGeral(Request $request){
        try{
            $campanha = Campanha::findOrFail($request->cod_campanha);

            if($request->has('arquivo')){
                \Storage::disk('s3')->delete('regulamento-campanha/' . $campanha->regulamento_hash);
                $request->request->add([
                    'regulamento'      => $request->file('arquivo')->getClientOriginalName(),
                    'regulamento_hash' => StoreFile($request->file('arquivo'), 'regulamento-campanha')
                ]);
            }

            $campanha->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function excluir(Request $request){
        try{
            $regulamento = Regulamento::findOrFail($request->id);

            if(\Storage::disk('s3')->delete('regulamento-campanha/' . $regulamento->regulamento_hash)){
                $regulamento->delete();
                return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
            }

            return response()->json(['erro' => true, 'mensagem' => 'Não foi possível excluir o regulamento!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function baixar(Request $request){
        $regulamento = Regulamento::find($request->id_regulamento);

        $assetPath = Storage::cloud()->temporaryUrl('regulamento-campanha/' . $regulamento->regulamento_hash, \Carbon\Carbon::now()->addMinutes(5));

        $header = [
            'Content-Type' => Storage::cloud()->mimeType('regulamento-campanha/' . $regulamento->regulamento_hash),
            'Content-Disposition' => 'inline; filename="'.limpaString($regulamento->titulo) . '_' . substr($regulamento->regulamento_hash, -4).'"',
            'Content-Length' => Storage::cloud()->size('regulamento-campanha/' . $regulamento->regulamento_hash)
        ];

        return response()->make(file_get_contents($assetPath), 200, $header);
    }

}
