<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Models\Carrinho;
use App\Models\ItemCarrinho;
use Illuminate\Http\Request;
use App\Models\Produto;

class CarrinhoController extends Controller {

    public function index(){
        $usuario  = \Auth::user();
        $campanha = Campanha::find(session('campanha')->cod_campanha);

        $carrinho = $usuario->carrinhos->where('status_carrinho', '=', 1)->last();

        if(!$carrinho)
            $carrinho = Carrinho::create(['cod_usuario' => $usuario->id]);

        if($carrinho->produtos->sum('valor') > $usuario->pontos()->where('campanha_id', $campanha->cod_campanha)->first()->pontos){
            $carrinho->update(['status_carrinho' => 0]);
            return redirect()->action('LojaController@index');
        }

        $variaveis = [
            'campanha'    => $campanha,
            'usuario'    => $usuario,
            'carrinho'   => $carrinho,
            'popups'     => new \Illuminate\Database\Eloquent\Collection(),
            'categorias' => $campanha->categorias,
            'categoria'  => null,
        ];

        return view('carrinho.detalhes-carrinho')->with($variaveis);
    }


    public function adicionar(Request $request){

        $valor_final = 0; // soma de valores dos produtos

        try{

            $produto  = Produto::find($request->cod_produto);
            $carrinho = Carrinho::where('cod_usuario', \Auth::user()->id)->where('status_carrinho', 1)->first(); // carrinho aberto daquele usuario logado
            $campanha = Campanha::find(session('campanha')->cod_campanha);

            $produto->categoria; // para aparecer o nome da categoria no append no JS
            $produto->valor = round(($campanha->valor_pontos != 0 ? $produto->valor_reais / $campanha->valor_pontos : $produto->valor_produto));

            if(empty($produto))
                return response()->json(['erro' => true, 'mensagem' => 'Produto não encontrado!']);


            if(empty($carrinho))
                $carrinho = Carrinho::create(['cod_usuario' => \Auth::user()->id]);


            foreach($carrinho->itens as $item){
                $valor_final += ($campanha->valor_pontos != 0 ? $item->produto->valor_reais / $campanha->valor_pontos : $item->produto->valor_produto);
            }

            if(\Auth::user()->pontos()->where('campanha_id', $campanha->cod_campanha)->first()->pontos < ($valor_final + ($campanha->valor_pontos != 0 ? $produto->valor_reais / $campanha->valor_pontos : $produto->valor_produto)))
                return response()->json(['erro' => true, 'mensagem' => 'Saldo Insuficiente!']);


            ItemCarrinho::create([
                'cod_produto'  => $produto->cod_produto,
                'cod_carrinho' => $carrinho->cod_carrinho,
                'cod_status'   => 1
            ]);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!', 'dados' => $produto]);

        }catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage(), 'request' => $request]);
        }
    }


    public function remover(Request $request){
        try{

            $carrinho = Carrinho::where('cod_usuario', \Auth::user()->id)->where('status_carrinho', 1)->first(); // carrinho aberto daquele usuario logado
            $itens    = ItemCarrinho::where('cod_carrinho', $carrinho->cod_carrinho)->where('cod_produto', $request->cod_produto)->get();

            if($request->has('unitario')){
                $itens->last()->delete();
            } else {
                foreach($itens as $item)
                    $item->delete();
            }

            return response()->json(['erro' => false, 'mensagem' => 'Prêmio Removido!']);

        }catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

}
