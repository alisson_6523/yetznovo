<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Campanha;
use App\Models\Banner;

class BannerController extends Controller {

    public function cadastrar(Request $request){
        try{
            Banner::create(array_merge($request->all(), [
                'arquivo'      => limpaString($request->file('foto')->getClientOriginalName(), '.'),
                'hash_arquivo' => StoreFile($request->file('foto'), 'banners'),
                'data_entrada' => Carbon::createFromFormat('d/m/Y', $request->entrada),
                'data_saida'   => (!empty($request->saida) ? Carbon::createFromFormat('d/m/Y', $request->saida) : null)
            ]));

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function editar(Request $request){
        try{
            $banner = Banner::findOrFail($request->cod_banner);

            if($request->has('foto')){
                \Storage::disk('s3')->delete('banners/' . $banner->hash_arquivo);
                $request->request->add([
                    'arquivo'       => limpaString($request->file('foto')->getClientOriginalName(), '.'),
                    'hash_arquivo' => StoreFile($request->file('foto'), 'banners')
                ]);
            }

            if($request->has('entrada'))
                $request->request->add(['data_entrada' => Carbon::createFromFormat('d/m/Y', $request->entrada)]);

            $request->request->add(['data_saida' => ($request->saida != "" ? Carbon::createFromFormat('d/m/Y', $request->saida) : null)]);

            $banner->update($request->all());

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function ativar(Request $request){
        try{
            $banner = Banner::withTrashed()->findOrFail($request->cod_banner);
            $banner->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function inativar(Request $request){
        try{
            $banner = Banner::findOrFail($request->cod_banner);
            $banner->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }
}
