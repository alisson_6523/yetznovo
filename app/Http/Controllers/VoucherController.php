<?php

namespace App\Http\Controllers;

use App\Http\MongoDb;
use App\Models\ItemCarrinho;
use Notification;
use App\Notifications\VoucherNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Search\ResgateSearch;
use Illuminate\Support\Facades\DB;

class VoucherController extends Controller {

    public function index(){
        return view('sistema.vouchers.vouchers-digitais');
    }

    public function filtrar(Request $request){
        $resgates = ResgateSearch::apply($request);

        return view('sistema.vouchers.lista-vouchers')->with([
            'resgates' => $resgates
        ]);
    }

    public function salvarLink(Request $request){
        try{
            ItemCarrinho::findOrFail($request->cod_item)->update(['link_voucher' => $request->link]);

            return response()->json(['erro' => false, 'mensagem' => 'Link salvo com sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

    public function enviarNotificacao(Request $request){
        try{
            $ids = $request->input('itens');

            $query = ItemCarrinho::whereIn('cod_item', $ids)->whereNotIn('cod_status', [4, 5])->whereNull('email_enviado')->whereNotNull('link_voucher')->where('link_voucher', '!=', '');
            $itens = $query->get();

            foreach($itens as $item){
                $usuario = $item->carrinho->user;

                $item->update(['cod_status' => 4, 'email_enviado' => Carbon::now()]);

                // dados do email
                $data = [
                    'usuario' => $usuario,
                    'item'  => $item,
                    'cod_campanha' => $item->carrinho->resgates->first()->campanha->cod_campanha
                ];

                // Notificação na área do usuário e e-mail
                Notification::send($usuario, new VoucherNotification($data));

            }

            $mongo = new MongoDb();
            $mongo->alterarStatusResgate($itens, 4);

            return response()->json(['erro' => false, 'mensagem' => 'Notificação enviada com sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

}
