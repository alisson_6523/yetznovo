<?php

namespace App\Http\Controllers;

use App\Models\ClienteArquivo;
use App\Models\HistoricoClienteArquivo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClienteArquivoController extends Controller
{

    public function cadastrar(Request $request){
        try{

            $arquivo = ClienteArquivo::create(array_merge($request->input(), [
                'cliente_id'   => \Auth::user()->cliente_id,
                'arquivo'      => ($request->hasFile('arquivo') ? limpaString($request->file('arquivo')->getClientOriginalName(), '.') : null),
                'hash_arquivo' => ($request->hasFile('arquivo') ? StoreFile($request->file('arquivo'), 'cliente-arquivo', 'private') : null)
            ]));

            HistoricoClienteArquivo::create([
                'arquivo_id' => $arquivo->id,
                'adm_id'     => \Auth::user()->id,
                'operacao'   => 'Enviou'
            ]);

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function baixar(Request $request){
        try{

            $arquivo = ClienteArquivo::where('hash_arquivo', $request->arquivo)->first();

            $assetPath = Storage::cloud()->temporaryUrl('cliente-arquivo/' . $arquivo->hash_arquivo, \Carbon\Carbon::now()->addMinutes(1));

            HistoricoClienteArquivo::create([
                'arquivo_id' => $arquivo->id,
                'adm_id'     => \Auth::user()->id,
                'operacao'   => 'Baixou'
            ]);

            $nome_arquivo = $request->titulo . '.' . explode('.', $arquivo->hash_arquivo)[1];

            header("Content-Description: File Transfer");
            header("Content-Length: " . Storage::cloud()->size('cliente-arquivo/' . $arquivo->hash_arquivo));
            header("Content-Disposition: attachment; filename=" . $nome_arquivo);
            header("Content-Type: " . Storage::cloud()->mimeType('cliente-arquivo/' . $arquivo->hash_arquivo));

            return readfile($assetPath);

        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }

    }


    public function deletar(Request $request){
        try{

            $arquivo = ClienteArquivo::find($request->id);

            HistoricoClienteArquivo::create([
                'arquivo_id' => $arquivo->id,
                'adm_id'     => \Auth::user()->id,
                'operacao'   => 'Deletou'
            ]);

            $arquivo->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }

}
