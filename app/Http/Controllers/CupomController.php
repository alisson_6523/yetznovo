<?php

namespace App\Http\Controllers;

use App\Models\Campanha;
use App\Search\ResgateSearch;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\CupomPedido;
use Auth;
use App\Models\Cupom;
use App\Models\HistoricoMovimentacao;
use Carbon\Carbon;
use App\Models\TipoMovimentacao;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Http\MongoDb;

class CupomController extends Controller {

    public function index() {
        $cupons = (!empty(\Auth::user()->cliente_id) ? CupomPedido::withTrashed()->where('cliente_id', \Auth::user()->cliente_id)->orderBy('created_at', 'DESC')->get() : CupomPedido::withTrashed()->orderBy('created_at', 'DESC')->get());

        return view('sistema.cupons.cupons')->with(['cupons' => $cupons]);
    }

    public function cadastra(Request $request) {
        $adm = Auth::guard('admin')->user();

        try{
            $pedido = CupomPedido::create(array_merge($request->all(), [
                'admin_id' => $adm->id
            ]));

            for ($i = 0; $i < $pedido->quantidade; $i++) {
                // cria um código único
                do {
                    $codigo = strtoupper($pedido->prefixo . '-' . Str::random(8));
                } while (Cupom::where('codigo', $codigo)->count() > 0);

                Cupom::create([
                    'pedidoId' => $pedido->id,
                    'codigo'   => $codigo,
                    'pontos'   => $pedido->pontos,
                ]);
            }

            return redirect('/sistema/cupons/cadastra-lote')->with(['erro' => false, 'msg' => 'Lote cadastrado com sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function show($id) {
        $pedido = CupomPedido::withTrashed()->find($id);

        return view('sistema.cupons.detalhes-pedido')->with([
            'pedido' => $pedido
        ]);

        return redirect()->action('CupomController@index');
    }

    public function aplicar(Request $request) {
        $usuario  = Auth::guard('user')->user();
        $codigo   = $request->input('cupom');
        $campanha = Campanha::findOrFail(session('campanha')->cod_campanha);
        $pontos   = $usuario->pontos()->where('campanha_id', $campanha->cod_campanha)->first();
        $cupom    = Cupom::withTrashed()->where('codigo', '=', $codigo)->first();

        if (empty($cupom))
            return response()->json(['erro' => true, 'mensagem' => 'CUPOM YETZ CARDS não encontrado, verifique os dados digitados e tente novamente']);

        if($cupom->pedido()->withTrashed()->first()->trashed())
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS não está mais ativo. Verifique se digitou os dados corretamente ou comunique seu supervisor.']);

        if($cupom->trashed())
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS não está mais ativo. Verifique se digitou os dados corretamente ou comunique seu supervisor.']);

        if (!empty($usuario->campanhas->where('cod_campanha', 61)->first()) && $cupom->pedido->campanha_id != 61) // Campanha ITAU2 (Refazer depois)
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS não é válido para a campanha que você participa.']);

        if (!empty($cupom->pedido->cliente_id) && $cupom->pedido->cliente_id != $campanha->cliente->id)
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS não é válido para a campanha que você participa.']);

        if (!empty($cupom->pedido->campanha_id) && $cupom->pedido->campanha_id != $campanha->cod_campanha)
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS não é válido para a campanha que você participa.']);

        if (!empty($cupom->usado_em))
            return response()->json(['erro' => true, 'mensagem' => 'Esse CUPOM YETZ CARDS já foi utilizado. Verifique se digitou os dados corretamente ou comunique seu supervisor.']);

        $pontos->pontos += $cupom->pontos;

        try {
            $pontos->update();
            $usuario->update();

            HistoricoMovimentacao::create([
                'cod_usuario'           => $usuario->id,
                'cod_tipo_movimentacao' => TipoMovimentacao::CREDITO,
                'valor'                 => $cupom->pontos,
                'protocolo'             => $cupom->codigo,
                'descricao'             => "Pontos creditados pelo cupom promocional",
                'referencia'            => "Cupom Yetz Cards",
                'cod_campanha'          => $campanha->cod_campanha
            ]);

            $cupom->usado_em = Carbon::now();
            $cupom->userId   = $usuario->id;
            $cupom->update();

            $carga = [
                'usuario' => [
                    'id' => $usuario->id,
                    'pdv' => [
                        'id' => $usuario->cod_pdv,
                        'cliente' => [
                            'id' => $usuario->pdv->cliente->id,
                            'campanhas' => [
                                [
                                    'id' => $campanha->cod_campanha
                                ]
                            ]
                        ]
                    ],
                    'sexo' => $usuario->sexo,
                    'coordenador' => $usuario->coordenador,
                    'email_coordenador' => $usuario->email_coordenador,
                    'supervisor' => $usuario->supervisor,
                    'email_supervisor' => $usuario->email_supervisor,
                    'ilha' => $usuario->ilha,
                    'cargo' => $usuario->cargo
                ],
                'carga' => [
                    'valor' => $cupom->pontos,
                ],
                'data' => Carbon::now()->toArray()
            ];

            $mongo = new MongoDb();
            $mongo->salvarCargas([], [$carga]);

            return response()->json(['erro' => false, 'mensagem' => "Parabéns, você carregou $cupom->pontos pontos na sua conta YETZ CARDS"]);
        } catch (Exception $e) {
            return response()->json(['erro' => true, 'mensagem' => 'Algo deu errado! entre em contato com o suporte técnico']);
        }
    }



    public function bloquearLote(Request $request){
        try{
            $lote = CupomPedido::findOrFail($request->id);
            $lote->delete();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function ativarLote(Request $request){
        try{
            $lote = CupomPedido::withTrashed()->findOrFail($request->id);
            $lote->restore();

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function bloquearCupomLote(Request $request){
        try{
            foreach(json_decode($request->cupons_id) as $id) {
                $cupom = Cupom::withTrashed()->findOrFail($id);

                if(empty($cupom->usado_em) && !$cupom->trashed())
                    $cupom->delete();
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function ativarCupomLote(Request $request){
        try{
            foreach(json_decode($request->cupons_id) as $id) {
                $cupom = Cupom::withTrashed()->findOrFail($id);

                if($cupom->trashed() && empty($cupom->usado_em))
                    $cupom->restore();
            }

            return response()->json(['erro' => false, 'mensagem' => 'Sucesso!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function exportarLote(Request $request){
        try{
            $lote = CupomPedido::withTrashed()->find($request->id);

            $linha_inicial = 6;

            $spreadsheet = new Spreadsheet();
            $spreadsheet->setActiveSheetIndex(0);

            $style = array('font' => array('size' => 12, 'bold' => true, 'color' => array('rgb' => 'ffffff')),
                'fill' => array('fillType' => Fill::FILL_SOLID, 'color' => array('rgb' => 'ED7D31')),
//            'allBorders' => array('borderStyle' => Border::BORDER_MEDIUM, 'color' => array('rgb' => '000000'))
            );

            $agora = date('d-m-Y \á\s H:i:s');

            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(4);
            $spreadsheet->getActiveSheet()->mergeCells('B1:H3');
            $spreadsheet->getActiveSheet()->mergeCells('B4:H5');
            $spreadsheet->getActiveSheet()->getStyle('B'.$linha_inicial.':H'.$linha_inicial)->applyFromArray($style);
            $spreadsheet->getActiveSheet()->getStyle('B'.$linha_inicial.':H'.$linha_inicial)->getBorders()->applyFromArray(['allBorders' => ['borderStyle' => Border::BORDER_MEDIUM, 'color' => ['rgb' => '000000']]]);

            $style = array(
                'alignment' => array(
                    'vertical' => Alignment::VERTICAL_CENTER,
                )
            );

            $spreadsheet->getActiveSheet()->getStyle("B1:G3")->applyFromArray($style);
            $spreadsheet->getActiveSheet()->getStyle("B4:G5")->applyFromArray($style);
            $spreadsheet->getActiveSheet()->setCellValue('B1', 'Arquivo gerado no dia '. $agora . ' / ' . $lote->nome);
            $spreadsheet->getActiveSheet()->setCellValue('B4', 'Lote: '. $lote->nome .' / '. (!empty($lote->cliente_id) ? $lote->cliente->nome_fantasia : 'Todos os Clientes') .' / '. (!empty($lote->campanha_id) ? $lote->campanha->nome_campanha : 'Todas as Campanhas' ) .' / '. $lote->quantidade .' cartões / '. $lote->usados  .' usados '. ($lote->trashed() ? '(BLOQUEADO)' : ''));
            $spreadsheet->getActiveSheet()->setCellValue('B' . $linha_inicial, 'SEQUENCIAL');
            $spreadsheet->getActiveSheet()->setCellValue('C' . $linha_inicial, 'CÓDIGO');
            $spreadsheet->getActiveSheet()->setCellValue('D' . $linha_inicial, 'VALOR');
            $spreadsheet->getActiveSheet()->setCellValue('E' . $linha_inicial, 'USADO POR');
            $spreadsheet->getActiveSheet()->setCellValue('F' . $linha_inicial, 'PDV');
            $spreadsheet->getActiveSheet()->setCellValue('G' . $linha_inicial, 'USADO EM');
            $spreadsheet->getActiveSheet()->setCellValue('H' . $linha_inicial, 'BLOQUEADO');

            $cont = $linha_inicial + 1;

            foreach($lote->cupons()->withTrashed()->get() as $cupom){

                $spreadsheet->getActiveSheet()->setCellValue("B$cont", $cupom->sequencial);   //SEQUENCIAL
                $spreadsheet->getActiveSheet()->setCellValue("C$cont", $cupom->codigo);    //CÓDIGO
                $spreadsheet->getActiveSheet()->setCellValue("D$cont", $cupom->pontos); //VALOR
                $spreadsheet->getActiveSheet()->setCellValue("E$cont", ($cupom->user->nome_usuario ?? '-')); //USADO POR
                $spreadsheet->getActiveSheet()->setCellValue("F$cont", ($cupom->user->pdv->nome_pdv ?? '-'));  //PDV
                $spreadsheet->getActiveSheet()->setCellValue("G$cont", ($cupom->usado ?? '-'));  //USADO EM
                $spreadsheet->getActiveSheet()->setCellValue("H$cont", ($cupom->trashed() ? 'Sim' : '-'));  //USADO EM

                $spreadsheet->getActiveSheet()->getStyle("B$cont:H$cont")->getBorders()->applyFromArray(['allborders' => ['style' => Border::BORDER_THIN, 'color' => ['rgb' => 'DDDDDD']]]);

                $cont++;

            }

            foreach (range('B', 'H') as $col) {
                $spreadsheet->getActiveSheet()
                    ->getColumnDimension($col)
                    ->setAutoSize(true);
            }

            $writer = new Xlsx($spreadsheet);

            ob_start();
            $writer->save('php://output');
            $content = ob_get_contents();
            ob_end_clean();

            Storage::put('cupons/cupons.xlsx', $content);

            return response()->json(['erro' => false, 'mensagem' => 'Lote de cupons exportado!']);
        } catch (\Exception $e){
            return response()->json(['erro' => true, 'mensagem' => $e->getMessage()]);
        }
    }


    public function baixarCuponsExportados(){
        return Storage::download('cupons/cupons.xlsx', 'Cupons-'. date('d-m-Y_H-i') . '.xlsx');
    }

}
