<?php

define('NOTIFICACAO_GRUPO', 1);

function StoreImage($file, $maxWidth, $diretorio, $visibilidade = 'public', $png = null){

    $nome_unico = uniqid(date('HisYmd'));

    $photo = \Image::make($file)->widen($maxWidth);
    $photo->interlace();
    $photo->stream();


    if($png != null){
        \Storage::disk('s3')->put($diretorio . '/' . $nome_unico . '.png', $photo->__toString(), $visibilidade);
        $photo->encode('png');
        return $nome_unico . '.png';
    } else {
        $photo->encode('jpg');
        \Storage::disk('s3')->put($diretorio . '/' . $nome_unico . '.jpg', $photo->__toString(), $visibilidade);
        return $nome_unico . '.jpg';
    }

    return $nome_unico;
}

function StoreFile($file, $diretorio, $visibilidade = 'public'){

    $nome_unico = uniqid(date('HisYmd')) . '.' . $file->getClientOriginalExtension();

    \Storage::disk('s3')->put($diretorio . '/' . limpaString($nome_unico, '.'), file_get_contents($file), $visibilidade);

    return $nome_unico;
}

function StoreBase64($file, $maxWidth, $diretorio, $visibilidade = 'public', $png = null){

    $nome_unico = uniqid(date('HisYmd'));
    $photo = Image::make(base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $file)))->widen($maxWidth);
    $photo->interlace();
    $photo->stream();


    if($png != null){
        \Storage::disk('s3')->put($diretorio . '/' . $nome_unico . '.png', $photo->__toString(), $visibilidade);
        $photo->encode('png');
        return $nome_unico . '.png';
    } else {
        $photo->encode('jpg');
        \Storage::disk('s3')->put($diretorio . '/' . $nome_unico . '.jpg', $photo->__toString(), $visibilidade);
        return $nome_unico . '.jpg';
    }
}


function limpaString($str, $arquivo = null) {

    $str = preg_replace('/[áàãâä]/ui', 'a', $str);
    $str = preg_replace('/[éèêë]/ui', 'e', $str);
    $str = preg_replace('/[íìîï]/ui', 'i', $str);
    $str = preg_replace('/[óòõôö]/ui', 'o', $str);
    $str = preg_replace('/[úùûü]/ui', 'u', $str);
    $str = preg_replace('/[ç]/ui', 'c', $str);
    $str = strtolower($str);
    if($arquivo == null){
        $str = preg_replace('/_+/', '-', $str);
        $str = preg_replace('/[^a-z0-9]/i', '-', $str);
    } else {
        $str = preg_replace('/ /', '-', $str);
    }


    return $str;
}

function formataEndereco($endereco){

    $endereco = preg_replace('/[áàãâä]/ui', 'a', $endereco);
    $endereco = preg_replace('/[éèêë]/ui', 'e', $endereco);
    $endereco = preg_replace('/[íìîï]/ui', 'i', $endereco);
    $endereco = preg_replace('/[óòõôö]/ui', 'o', $endereco);
    $endereco = preg_replace('/[úùûü]/ui', 'u', $endereco);
    $endereco = preg_replace('/[ç]/ui', 'c', $endereco);
    $endereco = preg_replace('/ +/', '+', $endereco);

    return $endereco;
}


function tiposNotificacao($cod, $lugar = null){
    $string = "";

    switch ($cod){
        case 0:
            $string = "Prontos Creditados";
        break;
        case 1:
            $string = "Fique Atento";
        break;
        case 2:
            $string = "Novo Cartão";
        break;
        case 3:
            $string = "Dicas";
        break;
    }

    if($lugar == 'class')
        return limpaString($string);

    return $string;
}


/**
 * Classe responsavel por limitar a quantidade de dados digitados.
 * @access public
 * @param mixed $texto Armazena o texto digitado
 * @param mixed $limite Armazena o limite de texto a ser digitado
 * @return mixed Retorna o texto digitado e limitado
 * @author Edney Paulo Gonçalves <desenvolvimento@cryptos.eti.br>
 * @version  Estável
 * @copyright Copyright © 2016, Caravela Comunicações.
 */
function limitarTexto($texto, $limite) {

    if (strlen($texto) > $limite) {

        $subTexto = substr($texto, 0, $limite);
        $texto = substr($subTexto, 0, strrpos($subTexto, ' ')) . '...';
    }

    return $texto;
}


function getMeuIp(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
}