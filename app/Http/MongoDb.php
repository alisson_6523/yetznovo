<?php

namespace App\Http;

use MongoDB\Driver\Manager;
use MongoDB\Driver\Command;
use MongoDB\Driver\BulkWrite;
use MongoDB\Driver\Query;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class MongoDb
{

	private $manager;
	private $banco;

	public function __construct()
	{
		$this->manager = new Manager('mongodb+srv://yetzcards-user:DMab2dVn2phTlh4K@yetzcards-drpvi.mongodb.net/test?retryWrites=true&w=majority');
//        $this->manager = new Manager();
		$this->banco = "yetzcards";
	}

	function statusMongoDb()
	{
		try {
			$status = new Command(["dbstats" => 1]);
			$resposta = $this->manager->executeCommand($this->banco, $status);
			$status = current($resposta->toArray());

			return \response()->json($status);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function salvarDados($documento, $collection)
	{
		try {
			$bulk = new BulkWrite;

			$bulk->insert($this->finalizarDocumento($documento));

			$this->manager->executeBulkWrite($this->banco . $collection, $bulk);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function salvarCargas($usuarios, $cargas)
	{
		try {
			if (count($usuarios) > 0) {
				$bulkUsuarios = new BulkWrite;

				foreach ($usuarios as $usuario) {
					$bulkUsuarios->insert($usuario);
				}

				$this->manager->executeBulkWrite($this->banco . '.usuarios', $bulkUsuarios);
			}

			if (count($cargas) > 0) {
				$bulkCargas = new BulkWrite;

				foreach ($cargas as $carga) {
					$bulkCargas->insert($carga);
				}

				$this->manager->executeBulkWrite($this->banco . '.cargas', $bulkCargas);
			}
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function atualizarUsuario($usuario, $cod_campanha = null)
	{
		try {
			$documento = [
				'id' => $usuario->id,
				'pdv' => [
					'id' => $usuario->cod_pdv,
					'cliente' => [
						'id' => $usuario->pdv->cliente->id,
						'campanhas' => [
							[
								'id' => (!empty($cod_campanha) ? $cod_campanha : \Session::get('campanha')->cod_campanha)
							]
						]
					]
				],
				'status' => $usuario->status,
				'login' => $usuario->login,
				'sexo' => $usuario->sexo,
				'coordenador' => $usuario->coordenador,
				'supervisor' => $usuario->supervisor,
				'ilha' => $usuario->ilha,
				'notificacao_celular' => $usuario->notificacao_celular,
				'cargo' => $usuario->cargo
			];

			$bulk = new BulkWrite;
			$bulk->update(['usuario.id' => $usuario->id], ['$set' => ['usuario' => $documento]]);

			$this->manager->executeBulkWrite($this->banco . '.usuarios', $bulk);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function salvarDadosResgate($resgate, $itens)
	{
		try {
			$bulkResgate = new BulkWrite;
			$bulkProdutosResgatdos = new BulkWrite;

			$documentoResgate = [
				'resgate' => [
					'id' => $resgate->cod_resgate,
					'valor' => $resgate->valor_resgate,
					'quantidade_itens' => count($itens)
				]
			];

			$bulkResgate->insert($this->finalizarDocumento($documentoResgate));

			foreach ($itens as $item) {
				$documentoProdutosResgatados = [
					'item' => [
						'id' => $item->cod_item,
						'status' => $item->cod_status
					],
					'produto' => [
						'id' => $item->produto->cod_produto,
						'nome' => $item->produto->nome_produto,
						'valor_yetz' => $item->produto->valor_produto,
						'valor_real' => $item->produto->valor_reais,
						'imagem' => $item->produto->link_foto
					],
					'categoria' => [
						'id' => $item->produto->categoria->cod_categoria_produto,
						'nome' => $item->produto->categoria->categoria,
					],
					'tipo' => $item->produto->produto_tipo->toArray(),
					'resgate' => [
						'id' => $resgate->cod_resgate
					]
				];

				$bulkProdutosResgatdos->insert($this->finalizarDocumento($documentoProdutosResgatados));
			}

			$this->manager->executeBulkWrite($this->banco . '.resgates', $bulkResgate);
			$this->manager->executeBulkWrite($this->banco . '.produtos_resgatados', $bulkProdutosResgatdos);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function alterarStatusResgate($itens, $cod_status)
	{
		try {
			$bulk = new BulkWrite;

			foreach ($itens as $item) {
				$bulk->update(['item.id' => $item->cod_item], ['$set' => ['item.status' => (int)$cod_status]]);

				if ($cod_status == 5) {
					$resgate_id = $item->carrinho->resgates[0]->cod_resgate;

					$filtro = ['resgate.id' => $resgate_id];
					$query = new Query($filtro);
					$resgate = $this->manager->executeQuery($this->banco . ".resgates", $query)->toArray();

					if (count($resgate) > 0) {
						$documento = [
							'id' => $resgate[0]->resgate->id,
							'valor' => $resgate[0]->resgate->valor -= $item->produto->valor_produto,
							'quantidade_itens' => $resgate[0]->resgate->quantidade_itens - 1
						];

						$bulkResgate = new BulkWrite;
						$bulkResgate->update(['resgate.id' => $resgate_id], ['$set' => ['resgate' => $documento]]);
						$this->manager->executeBulkWrite($this->banco . '.resgates', $bulkResgate);
					}
				}
			}

			$this->manager->executeBulkWrite($this->banco . '.produtos_resgatados', $bulk);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function salvarDadosLogout()
	{
		try {
			$sessionId = \Session::get('sessao');

			$filtro = ['sessao' => $sessionId];
			$query = new Query($filtro);
			$login = $this->manager->executeQuery($this->banco . ".logins", $query);

			if (!empty($login)) {
				$data = Carbon::parse($login->toArray()[0]->data->formatted);

				$documento = [
					'tempo' => Carbon::now()->diffInMinutes($data)
				];

				$this->salvarDados($documento, '.logouts');
			}
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarMovimentacao($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.email_coordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.email_supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0,
					'data.day' => 0,
					'data.dayOfWeek' => 0,
					'data.dayOfYear' => 0,
					'data.hour' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.formatted' => 0,
					'data.timezone' => 0
				]
			];

			$query = new Query($filtro, $opcoes);
			$cargas = $this->manager->executeQuery($this->banco . ".cargas", $query)->toArray();

			$opcoes = [
				"projection" => [
					'_id' => 0,
					'resgate.id' => 0,
					'resgate.quantidade_itens' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.cordenador' => 0,
					'usuario.email_coordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.email_supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0,
					'sessao' => 0,
					'data.day' => 0,
					'data.dayOfWeek' => 0,
					'data.dayOfYear' => 0,
					'data.hour' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.formatted' => 0,
					'data.timezone' => 0
				]
			];

			$filtro = [
				'$and' => [
					$filtro,
					[
						'resgate.quantidade_itens' => [
							'$gt' => 0,
						]
					]
				],
			];

			$query = new Query($filtro, $opcoes);
			$resgates = $this->manager->executeQuery($this->banco . ".resgates", $query)->toArray();

			return \response()->json(['cargas' => $cargas, 'resgates' => $resgates]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarUsuarios($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'usuario.id' => 0,
					'usuario.login' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.email_coordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.email_supervisor' => 0,
					'data' => 0
				]
			];
			$query = new Query($filtro, $opcoes);
			$usuarios = $this->manager->executeQuery($this->banco . ".usuarios", $query)->toArray();

			return \response()->json(['usuarios' => $usuarios]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarProdutosResgatados($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'produto.valor_yetz' => 0,
					'categoria' => 0,
					'tipo' => 0,
					'resgate' => 0,
					'usuario.id' => 0,
					'usuario.login' => 0,
					'usuario.coordenador' => 0,
					'usuario.supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0,
					'sessao' => 0,
					'data' => 0
				]
			];

			$filtro = [
				'$and' => [
					$filtro, [
						'item.status' => [
							'$ne' => 5,
						]
					]
				]
			];

			$query = new Query($filtro, $opcoes);
			$produtosResgatados = $this->manager->executeQuery($this->banco . ".produtos_resgatados", $query)->toArray();

			return \response()->json(['produtosResgatados' => $produtosResgatados]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarUsuariosMensal($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'dispositivo' => 0,
					'data' => 0,
					'sessao' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0
				]
			];

			$query = new Query($filtro, $opcoes);
			$usuariosMensal = $this->manager->executeQuery($this->banco . '.logins', $query)->toArray();

			return \response()->json(['usuariosMensal' => $usuariosMensal]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarUsuariosSemanal($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'dispositivo' => 0,
					'data' => 0,
					'sessao' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0
				]
			];

			$query = new Query($filtro, $opcoes);
			$usuariosSemanal = $this->manager->executeQuery($this->banco . '.logins', $query)->toArray();

			return \response()->json(['usuariosSemanal' => $usuariosSemanal]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarUsuariosDiario($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'dispositivo' => 0,
					'data' => 0,
					'sessao' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0
				]
			];

			$query = new Query($filtro, $opcoes);
			$usuariosDiario = $this->manager->executeQuery($this->banco . '.logins', $query)->toArray();

			return \response()->json(['usuariosDiario' => $usuariosDiario]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarLogins($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'usuario.sexo' => 0,
					'usuario.coordenador' => 0,
					'usuario.supervisor' => 0,
					//'usuario.ilha' => 0,
					'usuario.cargo' => 0,
					'data.year' => 0,
					'data.month' => 0,
					'data.day' => 0,
					'data.dayOfYear' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.timezone' => 0
				]
			];
			$query = new Query($filtro, $opcoes);
			$logins = $this->manager->executeQuery($this->banco . ".logins", $query)->toArray();

			$opcoes = [
				"projection" => [
					'_id' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.cordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.cargo' => 0,
					//'usuario.ilha' => 0,
					'data' => 0
				]
			];
			$query = new Query($filtro, $opcoes);
			$logouts = $this->manager->executeQuery($this->banco . ".logouts", $query)->toArray();

			return \response()->json(['logins' => $logins, 'logouts' => $logouts]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarPaginasAcessadas($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'data.year' => 0,
					'data.month' => 0,
					'data.day' => 0,
					'data.dayOfWeek' => 0,
					'data.dayOfYear' => 0,
					'data.hour' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.timezone' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.cordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.cargo' => 0,
					//'usuario.ilha' => 0,
				]
			];

			$query = new Query($filtro, $opcoes);

			$paginas = $this->manager->executeQuery($this->banco . ".paginas_visitadas", $query)->toArray();

			return \response()->json(['paginas' => $paginas]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarCategoriasAcessadas($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'data.year' => 0,
					'data.month' => 0,
					'data.day' => 0,
					'data.dayOfWeek' => 0,
					'data.dayOfYear' => 0,
					'data.hour' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.timezone' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.cordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.cargo' => 0,
					//'usuario.ilha' => 0,
				]
			];

			$query = new Query($filtro, $opcoes);

			$categorias = $this->manager->executeQuery($this->banco . ".categorias_visitadas", $query)->toArray();

			return \response()->json(['categorias' => $categorias]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function carregarProdutosAcessados($filtro)
	{
		try {
			$opcoes = [
				"projection" => [
					'_id' => 0,
					'data.year' => 0,
					'data.month' => 0,
					'data.day' => 0,
					'data.dayOfWeek' => 0,
					'data.dayOfYear' => 0,
					'data.hour' => 0,
					'data.minute' => 0,
					'data.second' => 0,
					'data.micro' => 0,
					'data.timestamp' => 0,
					'data.timezone' => 0,
					'usuario.id' => 0,
					'usuario.sexo' => 0,
					'usuario.cordenador' => 0,
					'usuario.supervisor' => 0,
					'usuario.cargo' => 0,
					//'usuario.ilha' => 0,
				]
			];

			$query = new Query($filtro, $opcoes);

			$produtos = $this->manager->executeQuery($this->banco . ".produtos_visitados", $query)->toArray();

			return \response()->json(['produtos' => $produtos]);
		} catch (\MongoDB\Driver\Exception\Exception $e) {
			return response()->json([
				"error" => $e->getCode(),
				"message" => $e->getMessage(),
				"file" => $e->getFile(),
				"line" => $e->getLine()
			]);
		}
	}

	function ultimasCategoriasVisitadas($usuario)
	{
		$query = new Query([
			'usuario.id' => $usuario->id
		]);

		$produtos = collect();

		foreach (array_slice($this->manager->executeQuery($this->banco . ".produtos_visitados", $query)->toArray(), -3) as $produto) {
			$produtos->push(['id' => explode('/', $produto->rota)[2]]);
		}


		return $produtos;
	}

	private function finalizarDocumento($documento)
	{
		$usuario = Auth::guard('user')->user();

		if ($usuario == null)
			$usuario = Auth::guard('api')->user();

		$documento['usuario'] = [
			'id' => $usuario->id,
			'pdv' => [
				'id' => $usuario->cod_pdv,
				'cliente' => [
					'id' => $usuario->pdv->cliente->id,
					'campanhas' => [
						[
							'id' => \Session::get('campanha')->cod_campanha
						]
					]
				]
			],
			'sexo' => $usuario->sexo,
			'cordenador' => $usuario->cordenador,
			'supervisor' => $usuario->supervisor,
			'ilha' => $usuario->ilha,
			'cargo' => $usuario->cargo
		];
		$documento['sessao'] = \Session::get('sessao');
		$documento['data'] = Carbon::now()->toArray();

		return $documento;
	}
}
