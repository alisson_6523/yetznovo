<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    protected function redirectTo($request) {

        $rota = explode('/', $request->path());

        if (!$request->expectsJson()) {

            if(!empty($rota[0]) && $rota[0] == 'sistema')
                return route('login-admin');

            return route('login');

        }

    }
}
