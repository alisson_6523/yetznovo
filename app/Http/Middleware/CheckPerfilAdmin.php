<?php

namespace App\Http\Middleware;

use App\Models\AdminPagina;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckPerfilAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        // Checa se existe a página requisitada nas permissoes do admin
        if($request->route()->getPrefix() != '/sistema' && Auth::user()->perfil->permissoes()->where('paginaId', AdminPagina::where('link', $request->route()->getPrefix())->first()->id)->count() > 0)
            return $next($request);

        // Se nao existir, redireciona para a primeira permissao que ele tem
        return redirect(Auth::user()->perfil->permissoes->first()->pagina->link);
    }
}
