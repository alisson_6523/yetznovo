<?php

Route::post('registrar', 'Aplicativo\AuthController@cadastrar');
Route::post('login', 'Aplicativo\AuthController@login');
Route::post('buscar-chave', 'Aplicativo\AuthController@buscaChave');
Route::post('primeiro-acesso', 'Aplicativo\AuthController@primeiroAcesso');


Route::get('/', function () {
    return response()->json(['message' => 'Entrou na API']);
});


Route::middleware(['auth:api'])->group(function () {

    Route::get('/user-logado', 'Aplicativo\AuthController@me');
    Route::post('{campanha}/{session_id}/logout', 'Aplicativo\AuthController@logout');


    // Pastas no yetz antigo
    Route::prefix('{campanha}/institucional')->group(function () {
        Route::get('/', 'Aplicativo\CarrinhoController@index');

        Route::post('/adicionar-produto', 'Aplicativo\CarrinhoController@adicionar');

        Route::post('/remover-produto', 'Aplicativo\CarrinhoController@remover');

        Route::post('/efetuar-resgate', 'Aplicativo\ResgateController@resgatar');
    });


    Route::prefix('{campanha}/outros')->group(function () {
        Route::post('/adicionar-desejado', 'Aplicativo\ProdutoController@atualizarDesejado');
    });


    Route::prefix('promo')->group(function () {

    });


    Route::prefix('{campanha}/produtos')->group(function () {
        Route::get('/', 'Aplicativo\ProdutoController@index');

        Route::get('/{categoria}', 'Aplicativo\ProdutoController@produtosPorCategoria');
    });


    Route::prefix('resgates')->group(function () {

    });


    Route::prefix('super')->group(function () {

    });


    Route::prefix('usuario')->group(function () {
        Route::post('/atualizar-usuario', 'Aplicativo\UsuarioController@atualizar');

        Route::get('/listar-campanhas', 'Aplicativo\UsuarioController@listarCampanhas');
    });



    Route::prefix('videos')->group(function () {

    });

});
