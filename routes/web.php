<?php

Route::view('/', 'login')->name('login');
Route::get('/logout', 'LoginController@logout');
Route::post('/recuperar-senha', 'LoginController@recuperaSenha');
Route::get('/nova-senha/{token}', 'LoginController@novaSenha');

Route::post('/confirma-nova-senha', 'LoginController@confirmaNovaSenha');

Route::get('/card', function () {
	return Redirect::to('/');
});


Route::get('/meu-ip', function () {
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    dd($ip);
                }
            }
        }
    }
});


Route::prefix('login')->group(function () {
	Route::post('/', 'LoginController@login');
	Route::post('/busca-chave', 'LoginController@buscaChave');
	Route::post('/primeiro-acesso', 'LoginController@primeiroAcesso');
	Route::post('/cadastro', 'LoginController@cadastro');
});


Route::group(['middleware' => ['auth']], function () {

	// Rotas da Loja
	Route::prefix('loja')->group(function () {

		Route::get('/', 'LojaController@index');
		Route::get('/conheca', 'LojaController@conheca');
		Route::get('/contato', 'LojaController@contato');
		Route::post('/contato/enviar', 'LojaController@enviarContato');
		Route::get('/movimentacao', 'LojaController@movimentacao');
		Route::get('/resgates', 'LojaController@resgates');
		Route::get('/perfil', 'LojaController@perfil');
		Route::get('/regulamento', 'LojaController@regulamento');
		Route::get('/termos-condicoes', 'LojaController@termosCondicoes');
		Route::get('/baixar-regulamento/{cod_campanha}/{nome_campanha}', 'LojaController@baixarRegulamentoCampanha');
		Route::get('/notificacoes', 'LojaController@notificacoes');
		Route::get('/videos', 'LojaController@videos');
		Route::post('/videos/marcar-visto', 'StreamingController@marcarVisto');
		Route::get('/lista-desejos', 'LojaController@listaDesejos');
		Route::get('/vouchers-digitais', 'LojaController@vouchers');

		Route::get('/produto/{id}', 'LojaController@produto');

		Route::get('/carrinho', 'CarrinhoController@index');
		Route::post('/adicionar-item-carrinho', 'CarrinhoController@adicionar');
		Route::post('/remover-item-carrinho', 'CarrinhoController@remover');

		Route::post('/edita-perfil', 'LoginController@editaUsuario');
		Route::post('/mudar-senha', 'LoginController@mudarSenha');

		Route::post('/desejados', 'DesejadosController@atualiza');


		Route::post('/resgatar', 'ResgateController@resgatar');
		Route::view('/agradecimentos', 'loja.agradecimentos');

		Route::post('/aplica-cupom', 'CupomController@aplicar');

		Route::post('/salvar-dados', 'LojaController@salvarDados');

		Route::get('/{link}', 'LojaController@categoria'); // Precisa ser a ultima senão outras rotas da loja caem aqui
	});

	// Notification
	Route::prefix('notification')->group(function () {

		Route::get('visualizar/{id}', 'NotificationController@visualiza');

	});


	// Rotas do Super
	Route::prefix('super')->group(function () {
	});

});


// Rotas do sistema
Route::prefix('sistema')->group(function () {

	// Authentication Routes...
	Route::view('/login', 'sistema.login')->name('login-admin');
	Route::post('/efetuar-login', 'AdminLoginController@login');
	Route::post('logout', 'AdminLoginController@logout')->name('logout');

	// Password Reset Routes...
	// Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
	// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
	// Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
	// Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');

	// Email Verification Routes...
	// Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
	// Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
	// Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');


	// Admin autenticado
	Route::group(['middleware' => ['auth:admin', 'perfil_admin']], function () {

		Route::get('/', function () {
			return Redirect::to('/sistema/campanhas');
		})->name('sistema');

		Route::prefix('helpdesk')->group(function () {
			Route::view('/', 'sistema.helpdesk.helpdesk');
		});



		// Clientes
		Route::prefix('clientes')->group(function () {
			Route::get('/', 'ClienteController@index');

			Route::view('/adicionar', 'sistema.clientes.cadastrar-cliente');

			Route::post('/adicionar-cliente', 'ClienteController@cadastrar');

			Route::get('/{id}/{nome_cliente}', function ($id) {
				return view('sistema.clientes.detalhes-cliente')->with([
					'cliente' => \App\Models\Cliente::withTrashed()->find($id)
				]);
			});

			Route::get('/editar/{id}/{nome_cliente}', function ($id) {
				return view('sistema.clientes.editar-cliente')->with([
					'cliente' => \App\Models\Cliente::withTrashed()->find($id)
				]);
			});

			Route::post('/editar-cliente', 'ClienteController@editar');

			Route::post('/excluir-cliente', 'ClienteController@excluir');

			Route::post('/ativar-cliente', 'ClienteController@ativar');

			Route::post('/filtrar', 'ClienteController@filtrar');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/listar-pdvs', 'ClienteController@listarPdvs');


			Route::get('/{id}/{nome_cliente}/pdv/adicionar', function ($id) {
				return view('sistema.pdvs.cadastrar-pdv')->with([
					'cliente' => \App\Models\Cliente::find($id)
				]);
			});

			Route::post('/pdv/adicionar-pdv', 'PdvController@cadastrar');

			Route::get('/{id_cliente}/{nome_cliente}/pdv/{id_pdv}/{nome_pdv}/editar', function ($id_cliente, $nome_cliente, $id_pdv) {
				return view('sistema.pdvs.editar-pdv')->with([
					'pdv' => \App\Models\ClientePdv::find($id_pdv)
				]);
			});

			Route::post('/pdv/editar-pdv', 'PdvController@editar');

			Route::post('/pdv/excluir-pdv', 'PdvController@excluir');

			Route::post('/pdv/ativar-pdv', 'PdvController@ativar');

		});


		// Campanhas
		Route::prefix('campanhas')->group(function () {
			Route::get('/', 'CampanhaController@index');

			Route::get('/adicionar', function () {
				return view('sistema.campanhas.cadastrar-campanha')->with([
					'clientes' => \App\Models\Cliente::all()
				]);
			});

			Route::post('/adicionar-campanha', 'CampanhaController@cadastrar');

			Route::get('/{id}/{titulo_campanha}', function ($id) {
				return view('sistema.campanhas.detalhes-campanha')->with([
					'campanha' => \App\Models\Campanha::withTrashed()->find($id),
					'categorias' => \App\Models\CategoriaProduto::with(['produtos' => function ($q) {
						$q->where('disponibilidade', [1, 3]);
					}])->orderBy('categoria', 'ASC')->get()
				]);
			});

			Route::get('/editar/{id}/{nome_campanha}', function ($id) {
				return view('sistema.campanhas.editar-campanha')->with([
					'campanha' => \App\Models\Campanha::withTrashed()->find($id)
				]);
			});

			Route::post('/filtrar', 'CampanhaController@filtrar');

			Route::post('/filtrar-usuarios-campanha', 'CampanhaController@filtrarUsuarios');

			Route::post('/filtrar-resgates-campanha', 'CampanhaController@filtrarResgates');

			Route::post('/listar-ilhas', 'CampanhaController@listarIlhas');

			Route::post('/listar-pdvs', 'ClienteController@listarPdvs');

			Route::post('/pdvs', 'CampanhaController@pdvs');

			Route::post('/editar-campanha', 'CampanhaController@editar');

			Route::post('/excluir-campanha', 'CampanhaController@excluir');

			Route::post('/ativar-campanha', 'CampanhaController@ativar');

			// Carga de pontos normal
			Route::post('/adicionar-carga-de-pontos', 'CargaDePontosController@recebeCarga');
			Route::post('/salvar-carga-de-pontos', 'CargaDePontosController@salvarCarga');
			Route::post('/excluir-carga-de-pontos', 'CargaDePontosController@excluirCarga');
			Route::post('/efetivar-carga-de-pontos', 'CargaDePontosController@efetivarCarga');
			Route::get('/download-carga-de-pontos/{arquivo}/{nome}', 'CargaDePontosController@downloadCarga');
			Route::get('/{id_campanha}/{titulo_campanha}/protocolo/{id_protocolo}', function ($id_campanha, $titulo_campanha, $id_protocolo) {
				$protocolo = \App\Models\CampanhaProtocolo::find($id_protocolo);
				return view('sistema.cargas.carga')->with([
					'campanha' => \App\Models\Campanha::find($id_campanha),
					'nome_arquivo' => $protocolo->arquivo,
					'estatistica' => json_decode($protocolo->dados)->estatistica,
					'usuarios' => json_decode($protocolo->dados)->arrUsuarios,
					'protocolo' => $protocolo
				]);
			});
			Route::get('/{id}/{titulo_campanha}/carga-de-pontos', function ($id) {
				return view('sistema.cargas.carga')->with('campanha', \App\Models\Campanha::find($id));
			})->name('carga-de-pontos');

			// Carga de pontos temporária
			Route::post('/adicionar-carga-de-pontos-temporaria', 'CargaDePontosTemporariosController@recebeCarga');
			Route::post('/salvar-carga-de-pontos-temporaria', 'CargaDePontosTemporariosController@salvarCarga');
			Route::post('/excluir-carga-de-pontos-temporaria', 'CargaDePontosTemporariosController@excluirCarga');
			Route::post('/efetivar-carga-de-pontos-temporaria', 'CargaDePontosTemporariosController@efetivarCarga');
			Route::get('/download-carga-de-pontos-temporaria/{arquivo}/{nome}', 'CargaDePontosTemporariosController@downloadCarga');
			Route::get('/{id_campanha}/{titulo_campanha}/protocolo-temporario/{id_protocolo}', function ($id_campanha, $titulo_campanha, $id_protocolo) {
				$protocolo = \App\Models\CampanhaProtocoloTemporario::find($id_protocolo);
				return view('sistema.cargas.carga')->with([
					'campanha' => \App\Models\Campanha::find($id_campanha),
					'nome_arquivo' => $protocolo->arquivo,
					'estatistica' => json_decode($protocolo->dados)->estatistica,
					'usuarios' => json_decode($protocolo->dados)->arrUsuarios,
					'protocolo' => $protocolo,
					'temporaria' => true
				]);
			});
			Route::get('/{id}/{titulo_campanha}/carga-de-pontos-temporaria', function ($id) {
				return view('sistema.cargas.carga')->with('campanha', \App\Models\Campanha::find($id));
			})->name('carga-de-pontos-temporaria');


			Route::get('/{id}/{titulo_campanha}/usuario/{id_usuario}/{nome}', function ($id, $titulo_campanha, $id_usuario) {
				return view('sistema.detalhe-usuario.detalhe-usuario')->with([
					'usuario' => \App\Models\User::withTrashed()->find($id_usuario),
					'campanha' => \App\Models\Campanha::find($id)
				]);
			});

			Route::post('/usuario/alterar-senha', 'UsuarioController@alterarSenha');

			Route::get('/{id}/{titulo_campanha}/listar-categorias-e-produtos', 'CampanhaController@listarCategoriasProdutos');

			Route::get('/{id}/{titulo_campanha}/listar-produtos', 'CampanhaController@listarProdutos');

			Route::get('/{id}/{titulo_campanha}/baixar-regulamento', 'CampanhaController@baixarRegulamento');

			Route::post('/{id}/{titulo_campanha}/adicionar-produto-lote', 'CampanhaController@adicionarProdutoLote');

			Route::post('/{id}/{titulo_campanha}/adicionar-produto', 'CampanhaController@adicionarProduto');

			Route::post('/{id}/{titulo_campanha}/remover-produto-lote', 'CampanhaController@removerProdutoLote');

			Route::post('/{id}/{titulo_campanha}/remover-produto', 'CampanhaController@removerProduto');

			Route::post('/{id}/{titulo_campanha}/novo-regulamento', 'RegulamentoController@index'); // cadastro

			Route::post('/{id}/{titulo_campanha}/excluir-regulamento', 'RegulamentoController@excluir');

			Route::get('/{id}/{titulo_campanha}/regulamento/{ilha}/{regulamento_id}', function ($id, $titulo, $ilha, $regulamento_id) { // edição
				return view('sistema.regulamentos.editar-regulamento')->with([
					'regulamento' => \App\Models\Regulamento::find($regulamento_id),
					'campanha' => \App\Models\Campanha::find($id)
				]);
			});

			Route::get('/{id}/{titulo_campanha}/regulamento-geral', function ($id, $titulo) { // edição regulamento geral
				return view('sistema.regulamentos.editar-regulamento-geral')->with([
					'campanha' => \App\Models\Campanha::find($id)
				]);
			});

			Route::post('/{id}/{titulo_campanha}/regulamento/{ilha}/cadastrar', 'RegulamentoController@cadastrar');

			Route::post('/{id}/{titulo_campanha}/regulamento/{ilha}/{regulamento_id}/editar', 'RegulamentoController@editar');

			Route::post('/{id}/{titulo_campanha}/regulamento-geral/editar', 'RegulamentoController@editarRegulamentoGeral');

			Route::get('/{id_regulamento}/{ilha}/baixar-regulamento-ilha', 'RegulamentoController@baixar');
		});


		// Portal
		Route::prefix('plataforma')->group(function () {
			Route::get('/', 'PortalController@index');

			Route::get('/adicionar-banner', function () {
				return view('sistema.banners.cadastrar-banner')->with([
					'clientes' => \App\Models\Cliente::all()
				]);
			});

			Route::post('/cadastrar-banner', 'BannerController@cadastrar');

			Route::get('/editar-banner/{id}/{nome}', function ($id) {
				return view('sistema.banners.editar-banner')->with([
					'clientes' => \App\Models\Cliente::all(),
					'banner' => \App\Models\Banner::withTrashed()->find($id)
				]);
			});

			Route::post('/editar-banner', 'BannerController@editar');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/ativar-banner', 'BannerController@ativar');

			Route::post('/inativar-banner', 'BannerController@inativar');

		});


		// Notificações
		Route::prefix('notificacoes')->group(function () {
			Route::get('/', 'NotificationController@listaNotificacoesGrupo');

			Route::get('/adicionar', function () {
				return view('sistema.notificacoes.cadastrar-notificacao')->with([
					'clientes' => \App\Models\Cliente::all()
				]);
			});

			Route::post('/adicionar-notificacao', 'NotificationController@enviaNotificacao');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/listar-ilhas', 'CampanhaController@listarIlhas');

		});


		// Popups
		Route::prefix('popups')->group(function () {
			Route::get('/', 'PopupController@index');

			Route::get('/adicionar', function () {
				return view('sistema.popups.cadastrar-popup')->with([
					'clientes' => \App\Models\Cliente::all()
				]);
			});

			Route::post('/adicionar-popup', 'PopupController@cadastrar');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/listar-ilhas', 'CampanhaController@listarIlhas');

			Route::get('/limpar', 'PopupController@limparPopups');

			Route::get('/editar/{id}/{titulo}', function ($id) {
				return view('sistema.popups.editar-popup')->with([
					'popup' => \App\Models\NotificacaoPopup::find($id),
					'clientes' => \App\Models\Cliente::all()
				]);
			});

			Route::post('/editar-popup', 'PopupController@editar');

		});


		// Relatórios
		Route::prefix('relatorios')->group(function () {
			Route::get('/', 'RelatorioController@index');
			Route::post('carregar-movimentacao', 'RelatorioController@carregarMovimentacao');
			Route::post('carregar-usuarios', 'RelatorioController@carregarUsuarios');
			Route::post('carregar-produtos-resgatados', 'RelatorioController@carregarProdutosResgatados');
			Route::post('carregar-usuarios-mensal', 'RelatorioController@carregarUsuariosMensal');
			Route::post('carregar-usuarios-semanal', 'RelatorioController@carregarUsuariosSemanal');
			Route::post('carregar-usuarios-diario', 'RelatorioController@carregarUsuariosDiario');
			Route::post('carregar-logins', 'RelatorioController@carregarLogins');
			Route::post('carregar-paginas-acessadas', 'RelatorioController@carregarPaginasAcessadas');
			Route::post('carregar-categorias-acessadas', 'RelatorioController@carregarCategoriasAcessadas');
			Route::post('carregar-produtos-acessados', 'RelatorioController@carregarProdutosAcessados');
			Route::get('status', 'RelatorioController@status');
		});


		// Streaming
		Route::prefix('streaming')->group(function () {
			Route::get('/', 'StreamingController@listar');

			Route::get('/adicionar', function () {
				return view('sistema.streaming.cadastrar-streaming')->with([
					'clientes' => \App\Models\Cliente::all(),
					'campanhas' => \App\Models\Campanha::all()
				]);
			});

			Route::post('/adicionar-streaming', 'StreamingController@cadastrar');

			Route::post('/listar-ilhas', 'CampanhaController@listarIlhas');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

		});


		// Resgates
		Route::prefix('resgates')->group(function () {
			Route::get('/', 'ResgateController@listaResgates');

			Route::post('filtrar', 'ResgateController@filtrar');

			Route::post('status-individual', 'ResgateController@statusIndividual');

			Route::post('status-lote', 'ResgateController@statusLote');

            Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/pdvs', 'CampanhaController@pdvs');

			Route::post('/gerar-arquivo', 'ResgateController@exportarResgates');

			Route::get('/baixar-arquivo', 'ResgateController@baixarResgatesExportados');

		});


		// Cupons
		Route::prefix('cupons')->group(function () {

			Route::get('/', 'CupomController@index');

			Route::view('/cadastra-lote', 'sistema.cupons.cadastra-lote');

			Route::post('/cadastrar', 'CupomController@cadastra');

			Route::post('/ativar-lote', 'CupomController@ativarLote');

			Route::post('/bloquear-lote', 'CupomController@bloquearLote');

			Route::post('/ativar-cupom-lote', 'CupomController@ativarCupomLote');

			Route::post('/bloquear-cupom-lote', 'CupomController@bloquearCupomLote');

			Route::post('/listar-campanhas', 'ClienteController@listarCampanhas');

			Route::post('/exportar-lote', 'CupomController@exportarLote');

			Route::get('/baixar-lote-exportado', 'CupomController@baixarCuponsExportados');

			Route::get('/{id}', 'CupomController@show'); // precisa ser o ultimo senao outras rotas caem aqui
		});


		// Usuários
		Route::prefix('usuarios')->group(function () {
			Route::get('/', function () {
				return view('sistema.usuarios.usuarios')->with([
					'pdvs' => \App\Models\ClientePdv::orderBy('nome_pdv', 'ASC')->get(),
					'ilhas' => \App\Models\User::select('ilha')->groupBy('ilha')->withTrashed()->get(),
					'admins' => (!empty(\Auth::user()->cliente_id) ? \App\Models\Admin::withTrashed()->where('cliente_id', \Auth::user()->cliente_id)->orderBy('nome', 'ASC')->get() : \App\Models\Admin::withTrashed()->orderBy('nome', 'ASC')->get())
				]);
			});

			Route::get('/novo-administrador', function () {
				return view('sistema.usuarios.cadastro-administrador')->with([
					'clientes' => \App\Models\Cliente::all(),
					'perfis' => \App\Models\AdminPerfil::all()
				]);
			});

			Route::post('adicionar-administrador', 'UsuarioController@adicionarAdministrador');

			Route::get('editar-administrador/{id}/{nome}', function ($id) {
				return view('sistema.usuarios.editar-administrador')->with([
					'admin' => \App\Models\Admin::withTrashed()->find($id),
					'clientes' => \App\Models\Cliente::all(),
					'perfis' => \App\Models\AdminPerfil::all()
				]);
			});

			Route::post('editar-administrador', 'UsuarioController@editarAdministrador');

			Route::post('bloquear-usuario', 'UsuarioController@bloquear');

			Route::post('ativar-usuario', 'UsuarioController@ativar');

			Route::post('/alterar-senha', 'UsuarioController@alterarSenha');

			Route::post('/filtrar-usuarios', 'UsuarioController@filtrarUsuarios');

			Route::get('/{id}/{nome}', function ($id) {
				return view('sistema.usuarios.detalhe-usuario')->with([
					'usuario' => \App\Models\User::find($id)
				]);
			});

			Route::get('/{id_usuario}/{nome_usuario}/{id_campanha}/{nome_campanha}', function ($id, $nome_usuario, $id_campanha) {
				return view('sistema.usuarios.detalhe-usuario')->with([
					'usuario' => \App\Models\User::find($id),
					'campanha' => \App\Models\Campanha::find($id_campanha)
				]);
			});
		});


		// Arquivos
		Route::prefix('uploads')->group(function () {
			Route::get('/', function () {
				return view('sistema.arquivos.arquivos')->with([
					'arquivos' => (!empty(\Auth::user()->cliente_id) ? \App\Models\ClienteArquivo::where('cliente_id', \Auth::user()->cliente_id)->get() : \App\Models\ClienteArquivo::all())
				]);
			});

			Route::view('/adicionar', 'sistema.arquivos.adicionar-arquivos');

			Route::post('/adicionar-arquivo', 'ClienteArquivoController@cadastrar');

			Route::post('/deletar-arquivo', 'ClienteArquivoController@deletar');

			Route::get('/baixar-arquivo/{arquivo}/{titulo}', 'ClienteArquivoController@baixar');
		});


		// Vouchers Digitais
        Route::prefix('vouchers-digitais')->group(function(){
            Route::get('/', 'VoucherController@index');

            Route::post('filtrar', 'VoucherController@filtrar');

            Route::post('salvar-link', 'VoucherController@salvarLink');

            Route::post('listar-campanhas', 'ClienteController@listarCampanhas');

            Route::post('pdvs', 'CampanhaController@pdvs');

            Route::post('enviar-notificacao', 'VoucherController@enviarNotificacao');
        });


	});


});
