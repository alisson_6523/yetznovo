const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');


function compilaSass() {
    return gulp.src('public/scss/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css/admin_novo'))
        .pipe(browserSync.stream());
}

function compilaUsuarioSass() {
    return gulp.src('public/mobile-scss/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('public/css/mobile'))
        .pipe(browserSync.stream());
}

gulp.task('sassUsuarios', compilaUsuarioSass);
gulp.task('sass', compilaSass);

function gulpJS() {
    return gulp
        .src(['public/js/admin_novo/editaveis/*.js'])
        .pipe(sourcemaps.init())
            .pipe(concat('admin_novo/admin.min.js'))
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/js/'))
        .pipe(browserSync.stream());
}

gulp.task('mainjs', gulpJS);

function pluginJS() {
    return gulp
        .src([
            'public/lib/jquery/jquery.min.js',
            'public/lib/input-mask/jquery.mask.min.js',
            'public/lib/swiper-slide/swiper.min.js',
            'public/lib/file-custom/custom-file-input.js',
            'public/lib/validate/jquery.validate.js',
            'public/lib/underscore/underscore-min.js',
            'public/lib/moment/moment.js',
            'public/lib/sweetalert2/sweetalert2.all.js',
            'public/lib/jquery-maskmoney/jquery.maskMoney.js'
        ])
        .pipe(concat('plugins.js'))
        .pipe(gulp.dest('public/js/'))
        .pipe(browserSync.stream());
}

function pluginCSS() {
    return gulp
        .src([
            'public/lib/swiper-slide/swiper.min.css',
            'public/lib/fontawesome/css/font-awesome.min.css',
            'public/lib/font-yetz/css/yetz-adm.css',
        ])
        .pipe(concat('plugin.min.css'))
        .pipe(gulp.dest('public/css/'))
        .pipe(browserSync.stream());
}

gulp.task('pluginjs', pluginJS);
gulp.task('plugincss', pluginCSS);

function browser() {
    browserSync.init({
        proxy: 'localhost:8000'
    })
}

gulp.task('browser-sync', browser);

function watch() {
    gulp.watch('public/scss/*.scss', compilaSass);
    gulp.watch('public/mobile-scss/*.scss', compilaUsuarioSass);
    gulp.watch('public/js/admin_novo/editaveis/*.js' , gulpJS);
    gulp.watch('public/lib/*.js' , pluginJS);
    gulp.watch('public/lib/*.css' , pluginCSS);
    gulp.watch(['resources/views/sistema/**/*.blade.php']).on('change', browserSync.reload);
}

gulp.task('watch', watch);

gulp.task('default', gulp.parallel('watch', 'browser-sync', 'sass', 'mainjs', 'pluginjs', 'plugincss', 'sassUsuarios'));
